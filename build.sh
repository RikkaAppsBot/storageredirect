#!/bin/sh
./gradlew :server:assembleRelease
./gradlew :inject:assembleRelease
./gradlew :native:generateJsonModelRelease
./gradlew :native:buildArmRelease
./gradlew :native:buildArm64Release
./gradlew :native:buildX86Release
./gradlew :native:buildX64Release
./gradlew :app:assembleRelease