package android.util;

public abstract class Singleton<T> {

    public Singleton() {
        throw new RuntimeException("STUB");
    }

    protected abstract T create();

    public final T get() {
        throw new RuntimeException("STUB");
    }
}
