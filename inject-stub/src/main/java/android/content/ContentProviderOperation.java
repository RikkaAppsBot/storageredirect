package android.content;

public class ContentProviderOperation {

    public static final int TYPE_INSERT = 1;
    public static final int TYPE_UPDATE = 2;
    public static final int TYPE_DELETE = 3;
    public static final int TYPE_ASSERT = 4;

    public int getType() {
        throw new RuntimeException("STUB");
    }

    public ContentValues resolveValueBackReferences(ContentProviderResult[] backRefs, int numBackRefs) {
        throw new RuntimeException("STUB");
    }
}
