package android.content;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ICancellationSignal;
import android.os.IInterface;
import android.os.RemoteException;

public interface IContentProvider extends IInterface {

    default Cursor query(String callingPkg, Uri url, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal)
            throws RemoteException {
        throw new RuntimeException();
    }

    // 26
    default Cursor query(String callingPkg, Uri url, String[] projection,
                         Bundle queryArgs, ICancellationSignal cancellationSignal)
            throws RemoteException {
        throw new RuntimeException();
    }

    // 30
    default Cursor query(String callingPkg, String featureId, Uri url, String[] projection,
                         Bundle queryArgs, ICancellationSignal cancellationSignal)
            throws RemoteException {
        throw new RuntimeException();
    }

    default Uri insert(String callingPkg, Uri url, ContentValues initialValues)
            throws RemoteException {
        throw new RuntimeException();
    }

    // 30
    default Uri insert(String callingPkg, String featureId, Uri url, ContentValues values, Bundle extras)
            throws RemoteException {
        throw new RuntimeException();
    }

    default int bulkInsert(String callingPkg, Uri url, ContentValues[] initialValues)
            throws RemoteException {
        throw new RuntimeException();
    }

    // 30
    default int bulkInsert(String callingPkg, String featureId, Uri url, ContentValues[] initialValues)
            throws RemoteException {
        throw new RuntimeException();
    }

    default int delete(String callingPkg, Uri url, String selection, String[] selectionArgs)
            throws RemoteException {
        throw new RuntimeException();
    }

    // 30
    default int delete(String callingPkg, String featureId, Uri url, Bundle selectionArgs)
            throws RemoteException {
        throw new RuntimeException();
    }
}
