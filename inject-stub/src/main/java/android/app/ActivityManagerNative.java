package android.app;

import android.os.Binder;
import android.os.IBinder;

public abstract class ActivityManagerNative extends Binder implements IActivityManager {

    public static IActivityManager asInterface(IBinder binder) {
        throw new RuntimeException("STUB");
    }

    public static IActivityManager getDefault() {
        throw new RuntimeException("STUB");
    }

    @Override
    public IBinder asBinder() {
        throw new RuntimeException("STUB");
    }
}
