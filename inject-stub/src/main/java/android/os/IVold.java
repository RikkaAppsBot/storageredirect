package android.os;

public interface IVold extends IInterface {

    void remountUid(int uid, int remountMode);

    abstract class Stub extends Binder implements IVold {

        public Stub() {
            throw new RuntimeException("STUB");
        }

        public static IVold asInterface(IBinder binder) {
            throw new RuntimeException("STUB");
        }

        @Override
        public IBinder asBinder() {
            throw new RuntimeException("STUB");
        }
    }
}
