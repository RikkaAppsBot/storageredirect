package android.os;

import android.annotation.NonNull;
import android.annotation.Nullable;

import java.io.FileDescriptor;

public interface IBinder {

    @Nullable
    String getInterfaceDescriptor() throws RemoteException;

    boolean pingBinder();

    boolean isBinderAlive();

    @Nullable
    IInterface queryLocalInterface(@NonNull String descriptor);

    void dump(@NonNull FileDescriptor fd, @Nullable String[] args) throws RemoteException;

    void dumpAsync(@NonNull FileDescriptor fd, @Nullable String[] args)
            throws RemoteException;

    boolean transact(int code, @NonNull Parcel data, @Nullable Parcel reply, int flags)
            throws RemoteException;

    interface DeathRecipient {
        void binderDied();
    }

    void linkToDeath(@NonNull DeathRecipient recipient, int flags)
            throws RemoteException;

    boolean unlinkToDeath(@NonNull DeathRecipient recipient, int flags);
}
