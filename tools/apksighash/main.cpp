#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <cassert>

void print_usage(const char *exe) {
    printf("Usage:\n"
           "\t%s [apk path]\n",
           exe);
}

#define UNSIGNED32(x) static_cast<uint32_t>(x[0] | (x[1] << 8) | (x[2] << 16) | (x[3] << 24))
#define UNSIGNED64(x) static_cast<uint64_t>(x[0] | (x[1] << 8) | (x[2] << 16) | (x[3] << 24) | ((uint64_t)x[4] << 32) | ((uint64_t)x[5] << 40) | ((uint64_t)x[6] << 48) | ((uint64_t)x[7] << 56))

const unsigned char SIG_BLOCK_MAGIC[] = {'A', 'P', 'K', ' ', 'S', 'i', 'g', ' ',
                                         'B', 'l', 'o', 'c', 'k', ' ', '4', '2'};

int main(int argc, char *argv[]) {
    if (argc != 2) {
        print_usage(argv[0]);
        exit(1);
    }

    uint32_t res_size = 0, res_hash = 0, res_has_v3 = 0;

    char *path = argv[1];
    FILE *f = fopen(path, "rb");
    if (!f) {
        printf("can't open input %s", path);
        exit(1);
    }

    unsigned char buffer[16];
    uint32_t offset;
    uint64_t size;

    // See https://users.cs.jmu.edu/buchhofp/forensics/formats/pkzip.htm for zip format
    // See https://source.android.com/security/apksigning/v2 for sign block format

    // TODO: if zip has comment
    fseek(f, -6, SEEK_END);

    // offset of central directory
    fread(&offset, 4, 1, f);

    // read magic
    fseek(f, static_cast<long>(offset - 16), SEEK_SET);
    fread(&buffer, 1, 16, f);
    fseek(f, -16, SEEK_CUR);

    if (memcmp(buffer, SIG_BLOCK_MAGIC, 16) != 0) {
        printf("can't find APK Signing Block magic");
        exit(1);
    }

    // read size of sig block
    fseek(f, -8, SEEK_CUR);
    fread(&size, 8, 1, f);

    // back to end of sig block
    fseek(f, 16, SEEK_CUR);

    // go to begining of sig block
    fseek(f, static_cast<long>(-size - 8), SEEK_CUR);
    fread(&buffer, 8, 1, f);

    if (UNSIGNED64(buffer) != size) {
        printf("bad sig block");
        exit(1);
    }

    uint64_t length;
    uint32_t id;

    // size, magic
    while (size > (8 + 16)) {
        fread(&length, 8, 1, f);
        fread(&id, 4, 1, f);

        printf("length: %llu id: 0x%u\n", length, id);

        switch (id) {
            case 0x7109871a: {
                uint32_t signers_size, digests_length, additional_attributes_length, __size;

                printf("APK Signature Scheme v2\n");

                fread(&signers_size, 4, 1, f); // signer[] length
                printf("signer[] length: %u\n", signers_size);

                while (signers_size) {
                    fread(&__size, 4, 1, f); // signer length
                    printf("signer length: %u\n", __size);
                    signers_size -= (__size + 4);

                    fread(&__size, 4, 1, f); // signed data length
                    printf("signed data length: %u\n", __size);

                    {
                        fread(&digests_length, 4, 1, f); // digest[] length
                        printf("digest[] length: %u\n", digests_length);

                        while (digests_length) {
                            fread(&__size, 4, 1, f); // digest length
                            printf("length: %u\n", __size);

                            fread(&__size, 4, 1, f); // signature algorithm ID
                            printf("id: 0x%x\n", UNSIGNED32(buffer));

                            fread(&__size, 4, 1, f); // digest
                            printf("digest length: %u\n", __size);
                            fseek(f, __size, SEEK_CUR);

                            digests_length -= (4 + 4 + 4 + __size);
                        }

                        fread(&__size, 4, 1, f); // certificates length
                        printf("certificates length: %u\n", __size);

                        fread(&__size, 4, 1, f); // certificate length
                        printf("certificate length: %u\n", __size);

                        res_size = __size;

                        while (__size) {
                            //printf("0x%02x ", fgetc(f));
                            res_hash = 31 * res_hash + fgetc(f);
                            __size -= 1;
                        }

                        fread(&additional_attributes_length, 4, 1, f); // additional attributes[]
                        printf("additional attributes length: %u\n", additional_attributes_length);

                        while (additional_attributes_length) {
                            fread(&__size, 4, 1, f); // additional attribute length
                            printf("length: %u\n", __size);
                            fread(&__size, 4, 1, f); // additional attributes
                            printf("ID: 0x%x\n", __size);
                            fread(&__size, 4, 1, f); // additional attributes
                            printf("additional attribute length: %u\n", __size);
                            fseek(f, static_cast<long>(__size), SEEK_CUR);

                            additional_attributes_length -= (4 + 4 + 4 + __size);
                        }
                    }

                    {
                        fread(&__size, 4, 1, f); // signatures length
                        printf("signatures length: %u\n", __size);
                        fseek(f, static_cast<long>(__size), SEEK_CUR);
                    }

                    {
                        fread(&__size, 4, 1, f); // public key length
                        printf("public key length: %u\n", __size);
                        fseek(f, static_cast<long>(__size), SEEK_CUR); // skip public key
                    }
                }
                break;
            }
            case 0xf05368c0:
                printf("APK Signature Scheme v3\n");
                fseek(f, static_cast<long>(length - 4), SEEK_CUR);
                res_has_v3 = 1;
                break;
            default:
                fseek(f, static_cast<long>(length - 4), SEEK_CUR);
                break;
        }

        size -= (length + 8 + 4 - 4);
    }

    fclose(f);

    assert(res_size != 0);

    printf("\n");
    printf("#define SIG_CHECK_LENGTH %d\n", res_size);
    printf("#define SIG_CHECK_HASH 0x%08x\n", res_hash);
    printf("#define SIG_CHECK_HAS_V3 %d\n", res_has_v3);
    return 0;
}