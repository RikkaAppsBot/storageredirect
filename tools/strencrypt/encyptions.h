#ifndef RIIKAENCRYPTION_ENCYPTION_H
#define RIIKAENCRYPTION_ENCYPTION_H

#include <stdio.h>

const int ENCRYPTIONS_LEN = 2;
const int METHOD_NAME_MAX_LEN = 10;
const int BUFFER_SIZE = 1024;

typedef char *(*encryption_t)(char *data, size_t len);

char *e1(char *data, size_t len)
{
    for (int i = 0; i < len; i++)
    {
        data[i] ^= (char)0x23;
    }
    return data;
}

char *e2(char *data, size_t len)
{
    for (int i = 0; i < len; i++)
    {
        data[i] ^= ((char) (len) << 1) % 0x4;
    }
    return data;
}

encryption_t encryption[] = {e1, e2};

char *encryption_name[] = {"e1_d", "e2_d"};

void write_dec_methods(FILE *f, const char *search_path);

void write_dec_methods(FILE *f, const char *e_file)
{
    FILE *e_f = fopen(e_file, "r");
    if (e_f == NULL)
    {
        printf("error reading %s\n", e_file);
        exit(1);
    }

    char *buffer = malloc(BUFFER_SIZE);

    size_t ret;

    while ((ret = fread(buffer, sizeof(char), BUFFER_SIZE, e_f)) != 0)
        fwrite(buffer, sizeof(char), ret, f);
    
    fclose(e_f);

    free(buffer);
}

#endif
