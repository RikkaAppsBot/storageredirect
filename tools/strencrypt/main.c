#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "encyptions.h"

#ifndef HAVE_STRSEP

char *strsep(char **sp, char *sep)
{
    char *start = *sp;
    char *p;

    p = (start != NULL) ? strpbrk(start, sep) : NULL;

    if (p == NULL)
    {
        *sp = NULL;
    }
    else
    {
        *p = '\0';
        *sp = p + 1;
    }

    return start;
}

#endif

void write_head(FILE *in);

void write_part(FILE *in, const char *name, const char *data, char *e);

void write_end(FILE *in);

char *read_line(FILE *in);

const static int LINE_LEN = 512;

void print_usage(const char *exe)
{
    printf("Usage:\n"
           "\t%s [input] [output] [d path]\n",
           exe);
}

int main(int argc, char *argv[])
{
    srand((unsigned int)time(NULL));

    if (argc != 4)
    {
        print_usage(argv[0]);
        exit(1);
    }

    char *in_path = argv[1];
    char *out_path = argv[2];
    char *search_path = argv[3];

    char *line;

    FILE *in = fopen(in_path, "r");
    if (!in)
    {
        printf("can't open input %s", in_path);
        exit(1);
    }
        
    FILE *out = fopen(out_path, "w");
    if (!out) {
        printf("can't open output %s", out_path);
        exit(1);
    }

    write_head(out);
    write_dec_methods(out, search_path);

    while ((line = read_line(in)) != NULL)
    {
        if (*line == 0 || *line == '#' || (*line == '/' && *(line + 1) == '/'))
        { //skip comment, start with `#` or `//`
            free(line);
            continue;
        }
        char *value = line;
        char *key = strsep(&value, "=");
        if (key)
        {
            int offset = rand() % ENCRYPTIONS_LEN;
            value = encryption[offset](value, strlen(value));
            write_part(out, key, value, encryption_name[offset]);
        }
        free(line);
    }

    write_end(out);

    fclose(in);
    fclose(out);
    return 0;
}

void write_head(FILE *in)
{
    fprintf(in,
            "#ifndef ENCYPTED_STRINGS\n"
            "#define ENCYPTED_STRINGS\n"
            "\n"
            "#include <stdlib.h>\n\n");
}

void write_part(FILE *in, const char *name, const char *data, char *e)
{
    size_t data_len = strlen(data);
    fprintf(in,
            "\n\n"
            "inline static void fill_%s(char *s) {\n",
            name);
    for (int i = 0; i < data_len; i++)
    {
        fprintf(in,
                "    s[%d] = 0x%02x;\n",
                i, data[i]);
    }
    fprintf(in,
            "    s[%zu] = 0;\n"
            "    %s(s, %zu);\n"
            "}\n",
            data_len, e, data_len);

    fprintf(in,
            "\n"
            "#define import_string_%s()\\\n"
            "    char %s[%zu + 1];\\\n"
            "    fill_%s(%s);",
            name, name, data_len, name, name);
}

void write_end(FILE *in)
{
    fprintf(in,
            "\n\n"
            "#endif\n");
}

char *read_line(FILE *in)
{
    char *line = malloc(sizeof(char) * LINE_LEN);
    int len = 0;
    char ch = 0;
    while (len < LINE_LEN && (ch = (char)fgetc(in)) != '\n' && ch != EOF)
    {
        *(line + len) = ch;
        len++;
    }
    *(line + len) = 0;
    if (ch == '\n' || len)
    {
        return line;
    }
    else
    {
        free(line);
        return NULL;
    }
}
