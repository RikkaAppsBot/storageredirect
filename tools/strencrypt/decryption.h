inline static char *e1_d(char *data, size_t len) {
    for (int i = 0; i < len; i++) {
        data[i] ^= (char) 0x23;
    }
    return data;
}

inline static char *e2_d(char *data, size_t len) {
    for (int i = 0; i < len; i++) {
        data[i] ^= ((char) (len) << 1) % 0x4;
    }
    return data;
}