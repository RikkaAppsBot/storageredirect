#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdint.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("%s", "Invalid arguments");
        exit(1);
    }

    char *name = argv[1];
    char *in_file_name = argv[2];
    char *out_file_name = argv[3];

    /*printf("%s\n", name);
    printf("%s\n", in_file_name);
    printf("%s\n", out_file_name);*/

    FILE *in_file = fopen(in_file_name, "rb");
    if (!in_file) {
        printf("Can't open %s", in_file_name);
        exit(1);
    }

    FILE *out_file = fopen(out_file_name, "w");
    if (!out_file) {
        printf("Can't open %s", out_file_name);
        exit(1);
    }

    fprintf(out_file, "static uint8_t %s_array[] = {\n", name);

    uint8_t c[8192];
    uint32_t n = 0;
    size_t size;
    while ((size = fread(c, 1, 8192, in_file)) > 0) {
        for (int i = 0; i < size; i++) {
            fprintf(out_file, "0x%02X,", c[i] ^ ((uint8_t)((n % 33) + n)));

            ++n;
            if (n % 16 == 0)
                fprintf(out_file, "\n");
        }
    }

    fseek(out_file, -1, SEEK_CUR);

    fprintf(out_file, "};\n");
    fprintf(out_file, "size_t %s_length = %d;\n", name, n);

    fclose(in_file);
    fclose(out_file);

    return 0;
}