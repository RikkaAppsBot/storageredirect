package moe.shizuku.redirectstorage.server;

import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class PackageHelper {

    public static void forceStopPackage(String packageName, int userId) {
        if ("com.android.systemui".equals(packageName)) {
            LOGGER.w("don't force stop system ui", packageName, userId);
            return;
        }
        try {
            SystemService.forceStopPackage(packageName, userId);
            LOGGER.i("forceStopPackage: package=%s, user=%d", packageName, userId);
        } catch (Throwable tr) {
            LOGGER.w(tr, "forceStopPackage failed: package=%s, user=%d", packageName, userId);
        }
    }

    public static void killUid(int uid) {
        if (!UserHandleUtils.isRegularApp(uid)) {
            LOGGER.w("killUid: kill %d may be dangerous, skip :(", uid);
            return;
        }
        LOGGER.i("killUid: uid=%d", uid);
        NativeHelper.killProcessByUid(uid);
    }

    public static void killPackage(String packageName, int userId, int uid) {
        forceStopPackage(packageName, userId);
        if (uid != -1) {
            killUid(uid);
        }
    }
}
