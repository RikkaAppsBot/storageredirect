package moe.shizuku.redirectstorage.server;

import android.annotation.SuppressLint;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

@SuppressLint("UnsafeDynamicallyLoadedCode")
@SuppressWarnings("JavaJniMissingFunction")
class c {

    static {
        System.load("/data/adb/storage-isolation/bin/libhelper.so");
    }

    static native int a(int uid);

    static native String a0(int type);

    static native boolean a1(int pid);

    static native void a2(Object listener, boolean hasModule, boolean fromApp);

    static native void a3(int type, Object value);

    static native void b(Object[] args);

    static native Object c(Object arg);

    static native void d(Object arg);
}

@Keep
abstract class c1 {

    abstract void a(int pid, int uid, String packageName);

    abstract void a0(boolean hasLog);
}

public class NativeHelper extends c {

    public interface Listener {

        void onProcessStarted(int pid, int uid, String packageName);

        void onLogcatStatusChanged(boolean hasLog);
    }

    private static class ListenerAdapter extends c1 {

        private final Listener listener;

        protected ListenerAdapter(Listener listener) {
            this.listener = listener;
        }

        @Override
        final void a(int pid, int uid, String packageName) {
            listener.onProcessStarted(pid, uid, packageName);
        }

        @Override
        final void a0(boolean hasLog) {
            listener.onLogcatStatusChanged(hasLog);
        }
    }

    public static boolean isNativeProcessAlive() {
        return c.a1(0);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static int killProcessByUid(int uid) {
        return a(uid);
    }

    public static String getMediaPath() {
        return a0(0);
    }

    public static void startMonitors(@NonNull Listener listener, boolean hasModule, boolean fromApp) {
        a2(new ListenerAdapter(listener), hasModule, fromApp);
    }

    public static void updateLicense(int type, String value) {
        a3(type, value);
    }

    public static boolean isNoValidLicense() {
        return a1(-1);
    }

    public static boolean isSdcardfsUsed() {
        return a1(1);
    }

    public static void main(String[] args) {
        b(args);
    }

    public static String getSocketToken() {
        return a0(1);
    }

    public static String getSocketAddress() {
        return a0(2);
    }

    public static String getRiruVersion() {
        return a0(3);
    }

    public static String getRiruVersionName() {
        return a0(4);
    }

    public static String getModuleVersion() {
        return a0(5);
    }

    public static String getModuleVersionName() {
        return a0(6);
    }

    public static ByteBuffer requestHandleProcess(ByteBuffer data) {
        return (ByteBuffer) c(data);
    }

    public static void freeDirectByteBuffer(ByteBuffer data) {
        d(data);
    }
}