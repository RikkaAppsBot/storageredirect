package moe.shizuku.redirectstorage.server

import android.os.Handler
import android.os.Looper
import moe.shizuku.redirectstorage.Constants
import moe.shizuku.redirectstorage.server.api.SystemService
import moe.shizuku.redirectstorage.server.ktx.mainHandler
import moe.shizuku.redirectstorage.server.legacy.LegacyCleaner
import moe.shizuku.redirectstorage.server.util.Debug
import moe.shizuku.redirectstorage.server.util.FileUtils
import moe.shizuku.redirectstorage.server.util.Logger.LOGGER
import java.io.File
import java.util.*

object Starter {

    @JvmStatic
    fun main(args: Array<String>?) {
        LOGGER.d("args=%s", Arrays.toString(args))

        if (args == null) return

        Looper.prepare()
        mainHandler = Handler(Looper.myLooper()!!)

        try {
            waitSystemServices()
        } catch (e: Throwable) {
            LOGGER.w(e, "waitSystemServices")
        }

        for (arg in args) {
            when (arg) {
                "--run-as-legacy-cleaner" -> {
                    LegacyCleaner.main()
                    return
                }
                "--debug" -> {
                    Debug.setName(null)
                }
            }
        }

        clearFiles()
        Service.main(args)

        Looper.loop()
    }

    private fun clearFiles() {
        val name = String.format(Locale.ENGLISH, "server-v%d", Constants.SERVER_VERSION)
        for (parent in FileUtils.listFilesOrEmpty(File("/data/adb/storage-isolation/bin/oat"))) {
            for (child in FileUtils.listFilesOrEmpty(parent)) {
                if (!child.name.startsWith(name)) {
                    if (child.delete()) LOGGER.i("deleted %s", child.absolutePath) else LOGGER.w("can't deleted %s", child.absolutePath)
                }
            }
        }
    }

    private fun waitSystemServices() {
        while (SystemService.packageManager == null) {
            LOGGER.i("Service package is not started, wait 1s...")
            Thread.sleep(1000)
        }
        LOGGER.i("Service package has started")

        while (SystemService.activityManager == null) {
            LOGGER.i("Service activity is not started, wait 1s...")
            Thread.sleep(1000)
        }
        LOGGER.i("Service activity has started")

        while (SystemService.userManager == null) {
            LOGGER.i("Service user is not started, wait 1s...")
            Thread.sleep(1000)
        }
        LOGGER.i("Service user has started")

        while (SystemService.appOpsService == null) {
            LOGGER.i("Service appops is not started, wait 1s...")
            Thread.sleep(1000)
        }
        LOGGER.i("Service appops has started")
    }
}