package moe.shizuku.redirectstorage.server.api

import android.app.IActivityManager
import android.app.IProcessObserver
import android.app.IUidObserver
import android.content.IContentProvider
import android.content.IIntentReceiver
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.*
import android.os.IBinder
import android.os.IUserManager
import android.os.RemoteException
import android.permission.IPermissionManager
import androidx.annotation.RequiresApi
import com.android.internal.app.IAppOpsService
import hidden.HiddenApiBridge
import hidden.HiddenApiBridgeV23
import moe.shizuku.redirectstorage.server.util.Logger.LOGGER
import moe.shizuku.redirectstorage.utils.BuildUtils

object SystemService {

    private val activityManagerBinder by lazy {
        SystemServiceBinder<IActivityManager>("activity") {
            if (BuildUtils.atLeast26()) {
                IActivityManager.Stub.asInterface(it)
            } else {
                HiddenApiBridgeV23.ActivityManagerNative_asInterface(it)
            }
        }
    }

    private val packageManagerBinder by lazy {
        SystemServiceBinder<IPackageManager>("package") {
            IPackageManager.Stub.asInterface(it)
        }
    }

    private val userManagerBinder by lazy {
        SystemServiceBinder<IUserManager>("user") {
            IUserManager.Stub.asInterface(it)
        }

    }

    private val appOpsServiceBinder by lazy {
        SystemServiceBinder<IAppOpsService>("appops") {
            IAppOpsService.Stub.asInterface(it)
        }
    }

    private val launcherAppsBinder by lazy {
        SystemServiceBinder<ILauncherApps>("launcherapps") {
            ILauncherApps.Stub.asInterface(it)
        }
    }

    @delegate:RequiresApi(30)
    private val permissionManagerBinder by lazy {
        SystemServiceBinder<IPermissionManager>("permissionmgr") {
            IPermissionManager.Stub.asInterface(it)
        }
    }

    val activityManager get() = activityManagerBinder.service
    val packageManager get() = packageManagerBinder.service
    val userManager get() = userManagerBinder.service
    val appOpsService get() = appOpsServiceBinder.service
    val launcherApps get() = launcherAppsBinder.service
    val permissionManager get() = permissionManagerBinder.service

    @JvmStatic
    @Throws(RemoteException::class)
    fun getContentProviderExternal(name: String?, userId: Int, token: IBinder?, tag: String?): IContentProvider? {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        return when {
            BuildUtils.atLeast29() -> {
                am.getContentProviderExternal(name, userId, token, tag)?.provider
            }
            BuildUtils.atLeast26() -> {
                am.getContentProviderExternal(name, userId, token)?.provider
            }
            else -> {
                HiddenApiBridgeV23.getContentProviderExternal_provider(am, name, userId, token)
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun removeContentProviderExternal(name: String?, token: IBinder?) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        am.removeContentProviderExternal(name, token)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun broadcastIntent(intent: Intent, userId: Int) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        val sticky = intent.component == null
        am.broadcastIntent(null, intent, null, IntentReceiver(), 0, null, null,
                null, -1, null, true, sticky, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun forceStopPackage(packageName: String?, userId: Int) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        am.forceStopPackage(packageName, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun checkPermission(permission: String?, pid: Int, uid: Int): Int {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        return am.checkPermission(permission, pid, uid)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getPackageProcessState(pkg: String?, userId: Int, callingPackage: String?): Int {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        return if (BuildUtils.atLeast26()) {
            am.getUidProcessState(getPackageUid(pkg, 0, userId), callingPackage)
        } else {
            if (userId != 0) {
                HiddenApiBridge.getActivityManager_PROCESS_STATE_TOP()
            } else {
                am.getPackageProcessState(pkg, callingPackage)
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun startActivity(intent: Intent?, mimeType: String?, userId: Int) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        am.startActivityAsUser(null, null, intent, mimeType,
                null, null, 0, 0, null, null, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun registerReceiver(receiver: IIntentReceiver?, intentFilter: IntentFilter?, requiredPermission: String?, userId: Int): Intent? {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        return if (BuildUtils.atLeast26()) {
            am.registerReceiver(null, "android", receiver, intentFilter,
                    requiredPermission, userId, 0)
        } else {
            am.registerReceiver(null, "android", receiver, intentFilter,
                    requiredPermission, userId)
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getApplicationInfo(packageName: String?, flags: Int, userId: Int): ApplicationInfo? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
        return pm.getApplicationInfo(packageName, flags, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getPackageInfo(packageName: String?, flags: Int, userId: Int): PackageInfo? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
        return pm.getPackageInfo(packageName, flags, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getPackageUid(packageName: String?, flags: Int, userId: Int): Int {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
        return if (BuildUtils.atLeast24()) {
            pm.getPackageUid(packageName, flags, userId)
        } else {
            pm.getPackageUid(packageName, userId)
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getPackagesForUid(uid: Int): Array<String?>? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
        return pm.getPackagesForUid(uid)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun grantRuntimePermission(packageName: String?, permissionName: String?, userId: Int) {
        if (BuildUtils.atLeast30()) {
            val pm = permissionManager ?: throw RemoteException("can't get IPermissionManager")
            pm.grantRuntimePermission(packageName, permissionName, userId)
        } else {
            val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
            pm.grantRuntimePermission(packageName, permissionName, userId)
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun revokeRuntimePermission(packageName: String?, permissionName: String?, userId: Int) {
        if (BuildUtils.atLeast30()) {
            val pm = permissionManager ?: throw RemoteException("can't get IPermissionManager")
            try {
                pm.revokeRuntimePermission(packageName, permissionName, userId, null)
            } catch (e: NoSuchMethodError) {
                pm.revokeRuntimePermission(packageName, permissionName, userId)
            }
        } else {
            val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
            pm.revokeRuntimePermission(packageName, permissionName, userId)
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getPermissionFlags(permissionName: String?, packageName: String?, userId: Int): Int {
        return if (BuildUtils.atLeast30()) {
            val pm = permissionManager ?: throw RemoteException("can't get IPermissionManager")
            pm.getPermissionFlags(permissionName, packageName, userId)
        } else {
            val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
            pm.getPermissionFlags(permissionName, packageName, userId)
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun updatePermissionFlags(permissionName: String?, packageName: String?, flagMask: Int, flagValues: Int, userId: Int) {
        return when {
            BuildUtils.atLeast30() -> {
                val pm = permissionManager ?: throw RemoteException("IPermissionManager't get IPackageManger")
                pm.updatePermissionFlags(permissionName, packageName, flagMask, flagValues, false, userId)
            }
            BuildUtils.atLeast29() -> {
                val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
                pm.updatePermissionFlags(permissionName, packageName, flagMask, flagValues, false, userId)
            }
            else -> {
                val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
                pm.updatePermissionFlags(permissionName, packageName, flagMask, flagValues, userId)
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun checkPermission(permName: String?, pkgName: String?, userId: Int): Int {
        return when {
            BuildUtils.atLeast31() -> {
                val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
                pm.checkPermission(permName, pkgName, userId)
            }
            BuildUtils.atLeast30() -> {
                val pm = permissionManager ?: throw RemoteException("can't get IPermissionManager")
                pm.checkPermission(permName, pkgName, userId)
            }
            else -> {
                val pm = packageManager ?: throw RemoteException("can't get IPackageManger")
                pm.checkPermission(permName, pkgName, userId)
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getInstalledPackages(flags: Int, userId: Int): ParceledListSlice<PackageInfo>? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManager")
        @Suppress("UNCHECKED_CAST")
        return pm.getInstalledPackages(flags, userId) as ParceledListSlice<PackageInfo>?
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getInstalledApplications(flags: Int, userId: Int): ParceledListSlice<ApplicationInfo>? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManager")
        @Suppress("UNCHECKED_CAST")
        return pm.getInstalledApplications(flags, userId) as ParceledListSlice<ApplicationInfo>?
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getUidForSharedUser(sharedUserName: String): Int {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManager")
        @Suppress("UNCHECKED_CAST")
        return pm.getUidForSharedUser(sharedUserName)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getApplicationHiddenSettingAsUser(packageName: String, userId: Int): Boolean {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManager")
        return pm.getApplicationHiddenSettingAsUser(packageName, userId)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun isUserUnlocked(userId: Int): Boolean {
        return when {
            BuildUtils.atLeast26() -> {
                val um = userManager ?: throw RemoteException("can't get IUserManger")
                um.isUserUnlocked(userId)
            }
            BuildUtils.atLeast24() -> {
                val am = activityManager ?: throw RemoteException("can't get IActivityManager")
                am.isUserRunning(userId, HiddenApiBridge.getActivityManager_FLAG_AND_UNLOCKED())
            }
            else -> {
                true
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun isUserStorageAvailable(userId: Int): Boolean {
        return when {
            BuildUtils.atLeast26() -> {
                val um = userManager ?: throw RemoteException("can't get IUserManger")
                um.isUserRunning(userId) && um.isUserUnlocked(userId)
            }
            BuildUtils.atLeast24() -> {
                val am = activityManager ?: throw RemoteException("can't get IActivityManager")
                am.isUserRunning(userId, HiddenApiBridge.getActivityManager_FLAG_AND_UNLOCKED())
            }
            else -> {
                true
            }
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getUsers(excludePartial: Boolean, excludeDying: Boolean, excludePreCreated: Boolean): List<UserInfo> {
        val um = userManager ?: throw RemoteException("can't get IUserManger")
        return if (BuildUtils.atLeast30()) {
            um.getUsers(excludePartial, excludeDying, excludePreCreated)
        } else {
            try {
                um.getUsers(excludeDying)
            } catch (e: NoSuchMethodError) {
                um.getUsers(excludePartial, excludeDying, excludePreCreated)
            }
        }
    }

    fun getUsersNoThrow(excludePartial: Boolean, excludeDying: Boolean, excludePreCreated: Boolean): List<UserInfo> {
        return try {
            getUsers(excludePartial, excludeDying, excludePreCreated)
        } catch (e: Exception) {
            emptyList()
        }
    }


    @JvmStatic
    @Throws(RemoteException::class)
    fun getOpsForPackage(uid: Int, packageName: String?, ops: IntArray?): List<Any?>? {
        val appOps = appOpsService ?: throw RemoteException("can't get IAppOpsService")
        return HiddenApiBridge.getOpsForPackage(appOps, uid, packageName, ops)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun getUidOps(uid: Int, ops: IntArray?): List<Any?>? {
        val appOps = appOpsService ?: throw RemoteException("can't get IAppOpsService")
        return if (BuildUtils.atLeast26()) {
            HiddenApiBridge.getUidOps(appOps, uid, ops)
        } else {
            null
        }
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun setMode(code: Int, uid: Int, packageName: String?, mode: Int) {
        val appOps = appOpsService ?: throw RemoteException("can't get IAppOpsService")
        appOps.setMode(code, uid, packageName, mode)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun setUidMode(code: Int, uid: Int, mode: Int) {
        val appOps = appOpsService ?: throw RemoteException("can't get IAppOpsService")
        appOps.setUidMode(code, uid, mode)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun addOnAppsChangedListener(callingPackage: String?, listener: IOnAppsChangedListener?) {
        val la = launcherApps ?: throw RemoteException("can't get ILauncherApps")
        if (BuildUtils.atLeast24()) {
            la.addOnAppsChangedListener(callingPackage, listener)
        } else {
            la.addOnAppsChangedListener(listener)
        }
    }

    @JvmStatic
    fun getInstalledPackagesNoThrow(flags: Int, userId: Int): List<PackageInfo> {
        return try {
            getInstalledPackages(flags, userId)?.list ?: emptyList()
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getInstalledPackages failed: flags=%d, user=%d", flags, userId)
            emptyList()
        }
    }

    @JvmStatic
    fun getInstalledApplicationsNoThrow(flags: Int, userId: Int): List<ApplicationInfo> {
        return try {
            getInstalledApplications(flags, userId)?.list ?: emptyList()
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getInstalledApplications failed: flags=%d, user=%d", flags, userId)
            emptyList()
        }
    }

    @JvmStatic
    fun getPackageInfoNoThrow(packageName: String?, flags: Int, userId: Int): PackageInfo? {
        return try {
            getPackageInfo(packageName, flags, userId)
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getPackageInfo failed: packageName=%s, flags=%d, user=%d", packageName, flags, userId)
            null
        }
    }

    @JvmStatic
    fun getApplicationInfoNoThrow(packageName: String?, flags: Int, userId: Int): ApplicationInfo? {
        return try {
            getApplicationInfo(packageName, flags, userId)
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getApplicationInfo failed: packageName=%s, flags=%d, user=%d", packageName, flags, userId)
            null
        }
    }

    @JvmStatic
    fun getUserIdListNoThrow(): List<Int> {
        val users = ArrayList<Int>()
        try {
            for (ui in getUsers(excludePartial = true, excludeDying = true, excludePreCreated = true)) {
                users.add(ui.id)
            }
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getUsers failed")
            users.clear()
            users.add(0)
        }
        return users
    }

    @JvmStatic
    fun getPackagesForUidNoThrow(uid: Int): List<String> {
        val packages = ArrayList<String>()
        try {
            packages.addAll(getPackagesForUid(uid)?.filterNotNull().orEmpty())
        } catch (tr: Throwable) {
        }
        return packages
    }

    @JvmStatic
    fun getUidForSharedUserNoThrow(sharedUserName: String): Int {
        try {
            return getUidForSharedUser(sharedUserName)
        } catch (tr: Throwable) {
        }
        return -1
    }

    @JvmStatic
    fun startActivityNoThrow(intent: Intent, mimeType: String?, userId: Int) {
        try {
            startActivity(intent, mimeType, userId)
        } catch (tr: Throwable) {
            LOGGER.w(tr, "startActivity failed: action=%s, comp=%s", intent.action, intent.component)
        }
    }

    @JvmStatic
    fun getApplicationHiddenSettingAsUserNoThrow(packageName: String, userId: Int): Boolean {
        try {
            return getApplicationHiddenSettingAsUser(packageName, userId)
        } catch (tr: Throwable) {
            LOGGER.w(tr, "getApplicationHiddenSettingAsUser failed: packageName=%s, userId=%d", packageName, userId)
        }
        return true
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun registerProcessObserver(processObserver: IProcessObserver?) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        am.registerProcessObserver(processObserver)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun registerUidObserver(observer: IUidObserver?, which: Int, cutpoint: Int, callingPackage: String?) {
        val am = activityManager ?: throw RemoteException("can't get IActivityManager")
        am.registerUidObserver(observer, which, cutpoint, callingPackage)
    }

    @JvmStatic
    @Throws(RemoteException::class)
    fun resolveContentProvider(name: String?, flags: Int, userId: Int): ProviderInfo? {
        val pm = packageManager ?: throw RemoteException("can't get IPackageManager")
        return pm.resolveContentProvider(name, flags, userId)
    }
}