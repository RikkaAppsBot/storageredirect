package moe.shizuku.redirectstorage.server.util;

import android.os.SystemProperties;

import java.util.Objects;

public class VoldProperties {

    private static final String STATE;
    private static final String TYPE;

    static {
        String state, type;
        try {
            state = SystemProperties.get("ro.crypto.state", "");
            type = SystemProperties.get("ro.crypto.type", "");
        } catch (Throwable e) {
            state = "";
            type = "";
        }
        STATE = state;
        TYPE = type;
    }

    public static String getState() {
        return STATE;
    }

    public static String getType() {
        return TYPE;
    }

    public static boolean isFileBasedEncryption() {
        return Objects.equals("encrypted", STATE) && Objects.equals("file", TYPE);
    }
}
