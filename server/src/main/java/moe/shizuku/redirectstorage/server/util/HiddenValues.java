package moe.shizuku.redirectstorage.server.util;

import android.app.AppOpsManager;
import android.content.pm.PackageManager;

import java.lang.reflect.Field;

public final class HiddenValues {

    public static final int MODE_ALLOWED;
    public static final int MODE_DEFAULT;
    public static final int OP_LEGACY_STORAGE;
    public static final int OP_REQUEST_INSTALL_PACKAGES;

    public static final int FLAG_PERMISSION_USER_FIXED;
    public static final int FLAG_PERMISSION_POLICY_FIXED;
    public static final int FLAG_PERMISSION_SYSTEM_FIXED;
    public static final int FLAG_PERMISSION_GRANTED_BY_DEFAULT;

    public static final int MATCH_UNINSTALLED_PACKAGES = 0x00002000;

    static {
        MODE_ALLOWED = getStaticIntFieldValue(AppOpsManager.class, "MODE_ALLOWED", 0);
        MODE_DEFAULT = getStaticIntFieldValue(AppOpsManager.class, "MODE_DEFAULT", 0);

        OP_LEGACY_STORAGE = getStaticIntFieldValue(AppOpsManager.class, "OP_LEGACY_STORAGE", -1);
        OP_REQUEST_INSTALL_PACKAGES = getStaticIntFieldValue(AppOpsManager.class, "OP_REQUEST_INSTALL_PACKAGES", -1);

        FLAG_PERMISSION_USER_FIXED = getStaticIntFieldValue(PackageManager.class, "FLAG_PERMISSION_USER_FIXED", 0);
        FLAG_PERMISSION_POLICY_FIXED = getStaticIntFieldValue(PackageManager.class, "FLAG_PERMISSION_POLICY_FIXED", 0);
        FLAG_PERMISSION_SYSTEM_FIXED = getStaticIntFieldValue(PackageManager.class, "FLAG_PERMISSION_SYSTEM_FIXED", 0);
        FLAG_PERMISSION_GRANTED_BY_DEFAULT = getStaticIntFieldValue(PackageManager.class, "FLAG_PERMISSION_GRANTED_BY_DEFAULT", 0);
    }

    private static int getStaticIntFieldValue(Class<?> clazz, String name, int defaultValue) {
        Field field = ReflectionUtils.findField(clazz, name, int.class);
        if (field == null)
            return defaultValue;
        ReflectionUtils.makeAccessible(field);
        return (int) ReflectionUtils.getField(field, null);
    }
}
