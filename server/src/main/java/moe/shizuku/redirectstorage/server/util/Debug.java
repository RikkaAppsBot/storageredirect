package moe.shizuku.redirectstorage.server.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class Debug {

    private static void setAppName(String name) {
        try {
            Class<?> cDdmHandleAppName = Class.forName("android.ddm.DdmHandleAppName");
            Method m = cDdmHandleAppName.getDeclaredMethod("setAppName", String.class, int.class);
            m.invoke(null, name, 0);
        } catch (Exception e) {
            LOGGER.e(e, "setAppName");
        }
    }

    public static void setName(String name) {
        if (name == null) {
            final File cmdline = new File("/proc/" + android.os.Process.myPid() + "/cmdline");
            try (BufferedReader reader = new BufferedReader(new FileReader(cmdline))) {
                name = reader.readLine();
                if (name.indexOf(' ') > 0) name = name.substring(0, name.indexOf(' '));
                if (name.indexOf('\0') > 0) name = name.substring(0, name.indexOf('\0'));
            } catch (IOException e) {
                name = "unknown";
            }
        }
        setAppName(name);
    }
}
