package moe.shizuku.redirectstorage.server;

import android.Manifest;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import java.util.List;

import hidden.HiddenApiBridge;
import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.enhance.EnhanceModule;
import moe.shizuku.redirectstorage.server.util.ArrayUtils;
import moe.shizuku.redirectstorage.server.util.HiddenValues;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

class PermissionHelper {

    private static final int PERMISSION_FIXED_FLAG = HiddenValues.FLAG_PERMISSION_SYSTEM_FIXED;

    public static void revokePermissions(@NonNull PackageInfo pi, String packageName, int userId, int uid) {
        if (!UserHandleUtils.isRegularApp(uid)) {
            LOGGER.w("revokePermissions: %s (uid %d) is not regular app, skip", packageName, uid);
            return;
        }

        if (pi.applicationInfo == null) {
            LOGGER.w("revokePermissions: pi.applicationInfo is null");
            return;
        }

        String[] requestedPermissions = pi.requestedPermissions;
        if (requestedPermissions == null) {
            LOGGER.w("revokePermissions: requestedPermissions of %s is null", packageName);
            return;
        }

        int appId = uid % 100000;
        boolean isRegularApp = appId >= 10000 && appId <= 19999;
        if (!isRegularApp) {
            LOGGER.w("revokePermissions: %s (appId=%d) is not regular app, skip", packageName, appId);
            return;
        }

        String permission;
        boolean storagePermission = false;

        permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ArrayUtils.contains(requestedPermissions, permission)) {
            revokeRuntimePermission(permission, packageName, userId, pi.applicationInfo.targetSdkVersion);
            storagePermission = true;
        }

        permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ArrayUtils.contains(requestedPermissions, permission)) {
            revokeRuntimePermission(permission, packageName, userId, pi.applicationInfo.targetSdkVersion);
            storagePermission = true;
        }

        permission = "android.permission.ACCESS_MEDIA_LOCATION";
        if (BuildUtils.atLeast29() &&
                (storagePermission && pi.applicationInfo.targetSdkVersion < 29) || ArrayUtils.contains(requestedPermissions, permission)) {
            revokeRuntimePermission(permission, packageName, userId, pi.applicationInfo.targetSdkVersion);
        }
    }

    private static void revokeRuntimePermission(String permission, String packageName, int userId, int targetSdkVersion) {
        if (targetSdkVersion < 23) {
            LOGGER.i("revokePermissions: targetSdkVersion of %s is lower than 23", packageName);
            return;
        }

        try {
            if (SystemService.checkPermission(permission, packageName, userId) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            int permFlags = SystemService.getPermissionFlags(permission, packageName, userId);
            int systemFixedFlags = HiddenValues.FLAG_PERMISSION_SYSTEM_FIXED | HiddenValues.FLAG_PERMISSION_GRANTED_BY_DEFAULT;
            if ((permFlags & systemFixedFlags) == systemFixedFlags) {
                LOGGER.v("don't revoke system fixed permission: package=%s, user=%d, perm=%s", packageName, userId, permission);
                return;
            }

            int flags = PERMISSION_FIXED_FLAG;
            if (flags != 0 && (permFlags & flags) == flags) {
                return;
            }

            SystemService.revokeRuntimePermission(packageName, permission, userId);
        } catch (Throwable tr) {
            LOGGER.w(tr, "revokePermission failed: package=%s, user=%d, perm=%s", packageName, userId, permission);
        }
    }

    public static void grantPermissions(PackageInfo pi, String packageName, int userId, int uid) {
        if (!UserHandleUtils.isRegularApp(uid)) {
            LOGGER.w("grantPermissions: %s (uid %d) is not regular app, skip", packageName, uid);
            return;
        }

        if (pi.applicationInfo == null) {
            LOGGER.w("grantPermissions: applicationInfo of %s is null", packageName);
            return;
        }

        String[] requestedPermissions = pi.requestedPermissions;
        if (requestedPermissions == null)
            return;

        String permission;

        permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ArrayUtils.contains(requestedPermissions, permission)) {
            grantRuntimePermission(permission, packageName, userId, pi.applicationInfo.targetSdkVersion);
            allowAppOps(permission, uid, packageName);
        }
        permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ArrayUtils.contains(requestedPermissions, permission)) {
            grantRuntimePermission(permission, packageName, userId, pi.applicationInfo.targetSdkVersion);
            allowAppOps(permission, uid, packageName);
        }

        if (BuildUtils.atLeast29() && EnhanceModule.status().getVersionCode() < EnhanceModule.V23_0) {
            try {
                allowAppOps(HiddenValues.OP_REQUEST_INSTALL_PACKAGES, uid, packageName);
            } catch (Throwable tr) {
                LOGGER.w(tr, "grantPermissions failed: package=%s, user=%d", packageName, userId);
            }
        }
    }

    private static void grantRuntimePermission(String permission, String packageName, int userId, int targetSdkVersion) {
        if (targetSdkVersion < 23) {
            LOGGER.i("grantPermissions: targetSdkVersion of %s is lower than 23", packageName);
            return;
        }

        try {
            int permFlags = SystemService.getPermissionFlags(permission, packageName, userId);
            int systemFixedByDefaultFlags = HiddenValues.FLAG_PERMISSION_SYSTEM_FIXED | HiddenValues.FLAG_PERMISSION_GRANTED_BY_DEFAULT;
            boolean systemFixedByDefault = (permFlags & systemFixedByDefaultFlags) == systemFixedByDefaultFlags;
            if (systemFixedByDefault) {
                if (SystemService.checkPermission(permission, packageName, userId) == PackageManager.PERMISSION_GRANTED) {
                    LOGGER.v("system fixed permission already granted: package=%s, user=%d, perm=%s", packageName, userId, permission);
                } else {
                    LOGGER.e("system fixed permission not granted: package=%s, user=%d, perm=%s", packageName, userId, permission);
                }
            } else {
                boolean granted = SystemService.checkPermission(permission, packageName, userId) == PackageManager.PERMISSION_GRANTED;
                if (!granted) {
                    SystemService.grantRuntimePermission(packageName, permission, userId);
                }
            }
        } catch (Throwable tr) {
            LOGGER.w(tr, "grantPermission failed: package=%s, user=%d", packageName, userId);
        }
    }

    private static void allowAppOps(String permission, int uid, String packageName) {
        try {
            allowAppOps(HiddenApiBridge.permissionToOpCode(permission), uid, packageName);
        } catch (Throwable tr) {
            LOGGER.w(tr, "setAppOps failed: package=%s, uid=%d, permission=%s", packageName, uid, permission);
        }
    }

    private static void allowAppOps(int code, int uid, String packageName) throws RemoteException {
        if (code != -1) {
            setAppOpsPackage(code, uid, packageName, HiddenValues.MODE_ALLOWED);
            setAppOpsUid(code, uid, HiddenValues.MODE_ALLOWED);
        }
    }

    private static void setAppOpsPackage(int op, int uid, String packageName, int mode) throws RemoteException {
        List<?> list = SystemService.getOpsForPackage(uid, packageName, new int[]{op});
        if (list != null && !list.isEmpty()) {
            Object packageOps = list.get(0);
            List<?> ops = HiddenApiBridge.PackageOps_getOps(packageOps);
            if (ops != null && !ops.isEmpty() && HiddenApiBridge.OpEntry_getMode(ops.get(0)) != mode) {
                SystemService.setMode(op, uid, packageName, mode);
            }
        }
    }

    private static void setAppOpsUid(int op, int uid, int mode) throws RemoteException {
        if (Build.VERSION.SDK_INT < 29) {
            return;
        }
        List<?> list = SystemService.getUidOps(uid, new int[]{op});
        if (list != null && !list.isEmpty()) {
            Object packageOps = list.get(0);
            List<?> ops = HiddenApiBridge.PackageOps_getOps(packageOps);
            if (ops != null && !ops.isEmpty() && HiddenApiBridge.OpEntry_getMode(ops.get(0)) != mode) {
                SystemService.setUidMode(op, uid, mode);
            }
        }
    }
}
