package moe.shizuku.redirectstorage.server.util;

import android.content.IContentProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.ArrayMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hidden.HiddenApiBridge;
import hidden.ProcessObserverAdapter;
import hidden.UidObserverAdapter;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.provider.BinderProvider;
import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.ktx.HandlerKt;
import moe.shizuku.redirectstorage.server.ktx.IContentProviderKt;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class BinderSender {

    private static final String PACKAGE_NAME = Constants.APPLICATION_ID;

    private static final Map<Integer, Runnable> CALLBACKS = new ArrayMap<>();

    private static void scheduleSendBinder(int userId) {
        Handler handler = HandlerKt.getWorkerHandler();

        Runnable cb = CALLBACKS.get(userId);
        if (cb == null) {
            cb = new Runnable() {
                @Override
                public void run() {
                    if (!sendBinder(userId)) {
                        handler.postDelayed(this, 1000);
                    }
                }
            };
            CALLBACKS.put(userId, cb);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && handler.hasCallbacks(cb)) {
            return;
        } else {
            handler.removeCallbacks(cb);
        }
        handler.post(cb);
    }

    private static boolean sendBinder(int userId) {
        String name = PACKAGE_NAME + ".binder";
        IContentProvider provider = null;

        /*
          When we pass IBinder through binder (and really crossed process), the receive side (here is system_server process)
          will always get a new instance of android.os.BinderProxy.

          In the implementation of getContentProviderExternal and removeContentProviderExternal, received
          IBinder is used as the key of a HashMap. But hashCode() is not implemented by BinderProxy, so
          removeContentProviderExternal will never work.

          Luckily, we can pass null. When token is token, count will be used.
        */
        IBinder token = null;

        try {
            provider = SystemService.getContentProviderExternal(name, userId, token, name);
            if (provider == null) {
                LOGGER.e("provider is null %s %d", name, userId);
                return false;
            }

            Bundle extra = new Bundle();
            extra.putBinder(BinderProvider.EXTRA_BINDER, binder);

            Bundle reply = IContentProviderKt.callCompat(provider, null, null, name, BinderProvider.METHOD_SEND_BINDER, null, extra);
            if (reply != null) {
                LOGGER.i("send binder to user app %s in user %d", BinderSender.PACKAGE_NAME, userId);
                return true;
            } else {
                LOGGER.w("failed to send binder to user app %s in user %d", BinderSender.PACKAGE_NAME, userId);
                return false;
            }
        } catch (Throwable tr) {
            LOGGER.e(tr, "failed send binder to user app %s in user %d", BinderSender.PACKAGE_NAME, userId);
            return false;
        } finally {
            if (provider != null) {
                try {
                    SystemService.removeContentProviderExternal(name, token);
                } catch (Throwable tr) {
                    LOGGER.w(tr, "removeContentProviderExternal");
                }
            }
        }
    }

    private static final List<Integer> PID_LIST = new ArrayList<>();

    private static class ProcessObserver extends ProcessObserverAdapter {

        @Override
        public void onForegroundActivitiesChanged(int pid, int uid, boolean foregroundActivities) throws RemoteException {
            LOGGER.d("onForegroundActivitiesChanged: pid=%d, uid=%d, foregroundActivities=%s", pid, uid, foregroundActivities ? "true" : "false");

            if (PID_LIST.contains(pid) || !foregroundActivities) {
                return;
            }
            PID_LIST.add(pid);

            onActive(uid, pid);
        }

        @Override
        public void onProcessDied(int pid, int uid) {
            LOGGER.d("onProcessDied: pid=%d, uid=%d", pid, uid);

            int index = PID_LIST.indexOf(pid);
            if (index != -1) {
                PID_LIST.remove(index);
            }
        }

        @Override
        public void onProcessStateChanged(int pid, int uid, int procState) throws RemoteException {
            LOGGER.d("onProcessStateChanged: pid=%d, uid=%d, procState=%d", pid, uid, procState);

            if (PID_LIST.contains(pid)) {
                return;
            }
            PID_LIST.add(pid);

            onActive(uid, pid);
        }
    }

    private static class UidObserver extends UidObserverAdapter {

        @Override
        public void onUidActive(int uid) throws RemoteException {
            LOGGER.d("onUidActive: uid=%d", uid);

            onActive(uid);
        }
    }

    private static void onActive(int uid) throws RemoteException {
        onActive(uid, -1);
    }

    private static void onActive(int uid, int pid) throws RemoteException {
        List<String> packages = SystemService.getPackagesForUidNoThrow(uid);
        if (packages.isEmpty())
            return;

        LOGGER.d("onActive: uid=%d, packages=%s", uid, TextUtils.join(", ", packages));

        int userId = uid / 100000;
        for (String packageName : packages) {
            if (packageName.equals(PACKAGE_NAME)) {
                scheduleSendBinder(userId);
                break;
            }
        }
    }

    private static IBinder binder;

    public static void start(IBinder binder, boolean delay) {
        BinderSender.binder = binder;

        try {
            SystemService.registerProcessObserver(new ProcessObserver());
        } catch (Throwable tr) {
            LOGGER.e(tr, "registerProcessObserver");
        }

        if (Build.VERSION.SDK_INT >= 26) {
            try {
                SystemService.registerUidObserver(new UidObserver(),
                        HiddenApiBridge.getActivityManager_UID_OBSERVER_ACTIVE(),
                        HiddenApiBridge.getActivityManager_PROCESS_STATE_UNKNOWN(),
                        null);
            } catch (Throwable tr) {
                LOGGER.e(tr, "registerUidObserver");
            }
        }

        if (!delay) {
            for (int userId : SystemService.getUserIdListNoThrow()) {
                if (SystemService.getPackageInfoNoThrow(PACKAGE_NAME, 0, userId) == null) continue;

                scheduleSendBinder(userId);
            }
        }
    }
}
