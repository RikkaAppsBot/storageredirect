package moe.shizuku.redirectstorage.server;

import android.content.ComponentName;
import android.content.ContentProviderNative;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.system.ErrnoException;
import android.system.Os;
import android.system.StructStat;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import kotlin.Pair;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.IFileCalculatorListener;
import moe.shizuku.redirectstorage.IFileMoveListener;
import moe.shizuku.redirectstorage.IService;
import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SRFileManager;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.collection.MountDirsSet;
import moe.shizuku.redirectstorage.collection.MountDirsTemplateList;
import moe.shizuku.redirectstorage.collection.ObserverInfoList;
import moe.shizuku.redirectstorage.collection.RedirectPackageInfoList;
import moe.shizuku.redirectstorage.collection.ServerFileSet;
import moe.shizuku.redirectstorage.collection.SimpleMountInfoList;
import moe.shizuku.redirectstorage.model.DebugInfo;
import moe.shizuku.redirectstorage.model.FileMonitorRecord;
import moe.shizuku.redirectstorage.model.FolderSizeEntry;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.model.ParcelStructStat;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.provider.IRemoteProvider;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.api.SystemServiceUtils;
import moe.shizuku.redirectstorage.server.core.CoreConfig;
import moe.shizuku.redirectstorage.server.core.CoreService;
import moe.shizuku.redirectstorage.server.core.BridgeServiceClient;
import moe.shizuku.redirectstorage.server.enhance.EnhanceModule;
import moe.shizuku.redirectstorage.server.enhance.FileMonitor;
import moe.shizuku.redirectstorage.server.enhance.Riru;
import moe.shizuku.redirectstorage.server.ktx.HandlerKt;
import moe.shizuku.redirectstorage.server.legacy.LegacyUidFileWriter;
import moe.shizuku.redirectstorage.server.observer.ExportFolders;
import moe.shizuku.redirectstorage.server.observer.FileObserverHolder;
import moe.shizuku.redirectstorage.server.provider.RemoteProviderHolder;
import moe.shizuku.redirectstorage.server.util.AtomicFile;
import moe.shizuku.redirectstorage.server.util.BinderSender;
import moe.shizuku.redirectstorage.server.util.DownloadManager;
import moe.shizuku.redirectstorage.server.util.EnvironmentCompat;
import moe.shizuku.redirectstorage.server.util.FileUtils;
import moe.shizuku.redirectstorage.server.util.HiddenValues;
import moe.shizuku.redirectstorage.server.util.PackageInfoUtils;
import moe.shizuku.redirectstorage.server.util.ReentrantLock;
import moe.shizuku.redirectstorage.server.util.VoldProperties;
import moe.shizuku.redirectstorage.utils.DataVerifier;
import moe.shizuku.redirectstorage.utils.ModelVersion;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;
import moe.shizuku.redirectstorage.utils.RandomId;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.RedirectPackageInfo.SHARED_USER_PREFIX;
import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class Service extends IService.Stub implements BridgeServiceClient.Listener, NativeHelper.Listener {

    static {
        ModelVersion.setServer(true);
    }

    private static final int EXIT_ALL = 54;
    private static final int EXIT_JAVA_SERVER_ONLY = 55;
    private static final int EXIT_AND_CLEAR_FILES = 56;

    public static void main(String[] args) {
        NativeHelper.main(args);

        LOGGER.i("starting service...");

        LOGGER.i("uid: " + android.os.Process.myUid());
        LOGGER.i("api: " + Build.VERSION.SDK_INT);
        LOGGER.i("device: " + Build.DEVICE);
        LOGGER.i("model: " + Build.MODEL);
        LOGGER.i("manufacturer: " + Build.MANUFACTURER);
        LOGGER.i("encryption: " + VoldProperties.getState() + " " + VoldProperties.getType());

        new Service().start();
        LOGGER.i("service version " + Constants.SERVER_VERSION + " started.");
    }

    private static final boolean DEBUG = false;

    private static final ReentrantLock LOCK = new ReentrantLock();

    private static final long WRITE_DELAY = DEBUG ? 1000 : 10 * 1000;

    private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();
    private static final ExecutorService REQUEST_EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private final String mSocketToken;

    private final Config mConfig = new Config();
    private final RedirectPackageInfoList mPackages = mConfig.packages;
    private final ObserverInfoList mObservers = mConfig.observers;
    private final SimpleMountInfoList mSimpleMounts = mConfig.simpleMounts;
    private final MountDirsTemplateList mMountDirsTemplates = mConfig.mountDirsTemplates;

    private final Map<Integer, FileObserverHolder> mObserverHolders = new ArrayMap<>();
    private final List<FileMoveListenerHolder> mFileMoveListeners = new ArrayList<>();

    private final AtomicFile mFile;

    private final DebugInfo mDebugInfo = new DebugInfo();
    private long mRedirectProcessCount = 0;

    private boolean mWriteScheduled;

    private int mLicenseStatus = 0;

    private boolean mNoLogDetected = false;

    private final Runnable mWriteBackgroundRunner = new Runnable() {

        @Override
        public void run() {
            LOCK.lock();
            try {
                mWriteScheduled = false;
                ConfigProvider.getWorkerHandler().post(mWriteRunner);
            } finally {
                LOCK.unlock();
            }
        }
    };

    private boolean mUnlocked;

    private final Runnable mWriteRunner = this::writeState;

    private abstract class WaitForEmulatedStorageRunner implements Runnable {

        @Override
        public final void run() {
            try {
                mUnlocked = SystemService.isUserUnlocked(0);
            } catch (Throwable tr) {
                LOGGER.w("isUserUnlocked failed", tr);
            }

            if (!mUnlocked) {
                HandlerKt.getMainHandler().postDelayed(this, 1000);

                LOGGER.i("not unlocked, wait 1s...");
                return;
            }

            if (!FileUtils.buildPathFromEmulated(0, "Android").canWrite()) {
                HandlerKt.getMainHandler().postDelayed(this, 1000);

                LOGGER.i("not decrypted, wait 1s...");
                return;
            }

            onUnlocked();
        }

        public abstract void onUnlocked();
    }

    private final Runnable mAddObserverRunner = new WaitForEmulatedStorageRunner() {

        @Override
        public void onUnlocked() {
            if (!TextUtils.isEmpty(mConfig.mediaPath)) {
                LOGGER.i("saved media path is %s", EnvironmentCompat.getMediaDirectory());

                setSavedMediaPathInternal(mConfig.mediaPath);
            }

            LOGGER.i("media path is %s", EnvironmentCompat.getMediaDirectory());

            onUnlock();

            if (mConfig.observers != null) {
                LOGGER.i("add observers...");

                mWriteScheduled = true;
                for (ObserverInfo info : mConfig.observers) {
                    // we only need to really "enable" enabled observers
                    if (!info.enabled) {
                        continue;
                    }

                    // only add observer when redirect enabled
                    if (!mConfig.packages.contains(new RedirectPackageInfo(info.packageName, info.userId))) {
                        continue;
                    }

                    try {
                        ObserverInfo.Result result = updateObserver(info, 0);
                        if (result.conflict != DataVerifier.ObserverInfoConflict.NONE) {
                            LOGGER.w("conflict " + info.toString());
                        }
                    } catch (Throwable e) {
                        LOGGER.w("updateObserver", e);
                    }
                }
                mWriteScheduled = false;

                LOGGER.i("Observers added.");
            } else {
                LOGGER.i("Unlocked.");
            }
        }
    };

    private FileMoveRunnable mFileMoveRunnable;

    private boolean isStarting;

    private static Service instance;

    public static Service getInstance() {
        return instance;
    }

    private Service() {
        instance = this;

        mFile = new AtomicFile(ConfigProvider.getFile(true));
        mSocketToken = NativeHelper.getSocketToken();
    }

    @Override
    public void onSystemServerRestarted() {
        LOGGER.w("system restarted...");
        PackageManagerApi.clearCache();

        HandlerKt.getMainHandler().post(new WaitForEmulatedStorageRunner() {
            @Override
            public void onUnlocked() {
                onUnlock();
            }
        });
    }

    @Override
    public void onResponseFromBridgeService(boolean response) {
        if (response) {
            LOGGER.i("send service to bridge");
        } else {
            LOGGER.w("no response from bridge, downgrade to content provider");

            // downgrade to content provider
            BinderSender.start(this, EnhanceModule.status().getVersionCode() >= 0);
            NativeHelper.startMonitors(Service.this, EnhanceModule.status().getVersionCode() >= 17, true);
        }
    }

    @Override
    public void onProcessStarted(int pid, int uid, String packageName) {
        try {
            requestCheckProcessImpl(pid, uid, packageName).toArray(new String[0]);
        } catch (Throwable e) {
            LOGGER.w(Log.getStackTraceString(e));
        }
    }

    @Override
    public void onLogcatStatusChanged(boolean hasLog) {
        mNoLogDetected = !hasLog;
    }

    private void onUnlock() {
        LOGGER.i("unlocked");
    }

    private void onPackageAddedLocked(UidPackageInfo uidPackageInfo) {
        final PackageInfo pi = uidPackageInfo.getPackageInfo();
        final String packageName = uidPackageInfo.getPackageName();
        final int userId = uidPackageInfo.getUserId();
        final String sharedUserId = uidPackageInfo.getSharedUserId();

        LOGGER.v("onPackageAdded: %d:%s", userId, packageName);

        if (sharedUserId != null) {
            PackageManagerApi.clearCacheForPackage(packageName, userId);
        }

        RedirectPackageInfo info = getRedirectPackageInfoLocked(uidPackageInfo, true, false, true, true, true);

        if (info == null) {
            LOGGER.i("package %d:%s does not request storage permissions", userId, packageName);
            return;
        }

        if (info.enabled) {
            addRedirectPackageLocked(info, uidPackageInfo, true, false, true, false, true, false);

            LOGGER.i("configuration found for %d:%s", userId, pi.packageName);
        }

        Intent intent = new Intent(Constants.APPLICATION_ID + ".action.PACKAGE_ADDED")
                .setComponent(ComponentName.createRelative(Constants.APPLICATION_ID, ".ReceiverActivity"))
                .setPackage(Constants.APPLICATION_ID)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
                .putExtra(Constants.EXTRA_PACKAGE_INFO, info.packageInfo)
                .putExtra(Constants.EXTRA_ENABLE_NOTIFICATION, isNewAppNotificationEnabledInternal())
                .putExtra(Constants.EXTRA_MIN_COMPATIBLE_VERSION, Constants.MIN_COMPATIBLE_VERSION);

        SystemService.startActivityNoThrow(intent, null, 0 /* notify app in main user */);
    }

    private void onPackageRemovedLocked(String packageName, int uid) {
        int userId = UserHandleUtils.getUserId(uid);

        CoreConfig.removeUid(uid);
        BridgeServiceClient.setUidIsolated(uid, false);

        LOGGER.v("onPackageRemoved: %d:%s", userId, packageName);

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);

        if (uidPackageInfo != null && uidPackageInfo.isHidden()) {
            LOGGER.d("onPackageRemoved: %d:%s is hidden", userId, packageName);

            getObserverHolderLocked(userId).removeRules(uidPackageInfo.getName());
        } else {
            // check shared
            List<UidPackageInfo> uidPackages = UidPackageInfo.list(userId);
            if (uidPackages.isEmpty()) {
                LOGGER.w("getInstalledPackages failed");
            } else {
                for (RedirectPackageInfo info : mPackages) {
                    if (!info.enabled) {
                        continue;
                    }

                    boolean remove = true;
                    String name = info.packageName;
                    for (UidPackageInfo upi : uidPackages) {
                        if (name.equals(upi.getName())) {
                            LOGGER.d("onPackageRemoved: don't remove %d:%s", userId, name);
                            remove = false;
                            break;
                        }
                    }
                    if (remove) {
                        LOGGER.i("onPackageRemoved: remove %d:%s", userId, packageName);
                        removeRedirectPackageLocked(null, name, userId);
                    }
                }
            }

            Intent intent = new Intent(Constants.APPLICATION_ID + ".action.PACKAGE_REMOVED")
                    .setComponent(ComponentName.createRelative(Constants.APPLICATION_ID, ".ReceiverActivity"))
                    .setPackage(Constants.APPLICATION_ID)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
                    .putExtra(Constants.APPLICATION_ID + ".extra.PACKAGE_NAME", packageName)
                    .putExtra(Constants.APPLICATION_ID + ".extra.USER_ID", userId)
                    .putExtra(Constants.EXTRA_MIN_COMPATIBLE_VERSION, Constants.MIN_COMPATIBLE_VERSION);

            SystemService.startActivityNoThrow(intent, null, 0 /* notify app in main user */);

            PackageManagerApi.clearCacheForPackage(packageName, userId);
        }
    }

    private void start() {
        isStarting = true;

        BridgeServiceClient.send(this);

        SystemServiceUtils.waitForSystemService("mount");
        SystemServiceUtils.waitForSystemService("input");
        SystemServiceUtils.waitForSystemService("launcherapps");

        PackageManagerApi.enableCache();

        // do not write
        mWriteScheduled = true;

        Config config = readState();

        LOGGER.i("config=" + config.toString());

        // copy to service
        mConfig.mountDirsTemplates.addAll(config.mountDirsTemplates);
        mConfig.defaultTarget = config.defaultTarget;
        mConfig.mediaPath = config.mediaPath;
        mConfig.killMediaStorageOnStart = config.killMediaStorageOnStart;
        mConfig.moduleConfig = config.moduleConfig;
        mConfig.version = config.version;

        DownloadManager.setIsNotificationEnabled(EnhanceModule.status().getVersionCode() <= 0 || !mConfig.moduleConfig.disableExportNotification);

        // write for native
        LegacyUidFileWriter.initializeDir();
        LegacyUidFileWriter.writeDefault(mConfig);

        for (ObserverInfo it : config.observers) {
            UidPackageInfo info = UidPackageInfo.get(it.packageName, it.userId);
            if (info == null) {
                // not installed, add as is
                mConfig.observers.add(it);
                continue;
            }
            it.packageName = info.getName();
            mConfig.observers.add(it);
        }

        for (SimpleMountInfo it : config.simpleMounts) {
            UidPackageInfo source = UidPackageInfo.get(it.sourcePackage, it.userId);
            UidPackageInfo target = UidPackageInfo.get(it.targetPackage, it.userId);
            if (source == null || target == null) {
                // not installed, add as is
                mConfig.simpleMounts.add(it);
                continue;
            }
            it.sourcePackage = source.getName();
            it.targetPackage = target.getName();
            mConfig.simpleMounts.add(it);
        }

        for (RedirectPackageInfo info : config.packages) {
            if (info.packageName == null) {
                continue;
            }

            UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId, PackageManager.GET_PERMISSIONS);

            addRedirectPackageLocked(info, uidPackageInfo, false, false, false, true, false, true);
        }

        // write native config
        LegacyUidFileWriter.rewriteAll(mConfig, Service.this);

        HandlerKt.getMainHandler().post(mAddObserverRunner);
        mWriteScheduled = false;

        PackageManagerApi.disableCache();

        BridgeServiceClient.notifyStarted();
        BridgeServiceClient.addIsolatedUids(CoreConfig.getIsolatedUids());

        isStarting = false;
    }

    @NonNull
    private Config readState() {
        FileInputStream stream;
        try {
            stream = mFile.openRead();
        } catch (FileNotFoundException e) {
            LOGGER.i("no existing config file " + mFile.getBaseFile() + "; starting empty");

            return ConfigProvider.getDefault();
        }

        Config config = null;
        try {
            config = ConfigProvider.load(stream);
            mDebugInfo.configFromFile = true;
        } catch (Throwable tr) {
            LOGGER.w(tr, "failed to read config");
            mDebugInfo.addException(tr);

            try {
                File dir = new File(ConfigProvider.getConfigDir(), "log");
                if (!dir.exists()) {
                    //noinspection ResultOfMethodCallIgnored
                    dir.mkdirs();
                }
                File file = new File(dir, String.format(Locale.ENGLISH, "exception-%d.txt", System.currentTimeMillis() / 1000));

                FileOutputStream fos = new FileOutputStream(file, true);
                PrintStream ps = new PrintStream(fos);
                tr.printStackTrace(ps);
            } catch (Throwable ignored) {
            }
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                LOGGER.w("failed to close: " + e);
            }
        }

        if (config == null) {
            config = ConfigProvider.getDefault();
        }
        return config;
    }

    private void writeState() {
        if (!mUnlocked) {
            LOGGER.w("not unlocked, skip write state");
            return;
        }

        synchronized (mFile) {
            FileOutputStream stream;
            try {
                stream = mFile.startWrite();
            } catch (IOException e) {
                LOGGER.w("failed to write state: " + e);
                return;
            }

            try {
                ConfigProvider.write(stream, mConfig);
                mFile.finishWrite(stream);

                mDebugInfo.lastConfigSaved = System.currentTimeMillis();
                LOGGER.v("config saved");
            } catch (Throwable tr) {
                LOGGER.w(tr, "can't save %s, restoring backup.", mFile.getBaseFile());
                mDebugInfo.addException(tr);

                mFile.failWrite(stream);
            }
        }
    }

    private void scheduleWriteLocked() {
        if (!mWriteScheduled) {
            mWriteScheduled = true;
            HandlerKt.getMainHandler().postDelayed(mWriteBackgroundRunner, WRITE_DELAY);
        }
    }

    private int checkCallingPermission(String permission) {
        try {
            return SystemService.checkPermission(permission,
                    Binder.getCallingPid(),
                    Binder.getCallingUid());
        } catch (Throwable tr) {
            LOGGER.w(tr, "checkCallingPermission");
            return PackageManager.PERMISSION_DENIED;
        }
    }

    private void enforceManagerPermission() {
        if (Binder.getCallingPid() == Os.getpid()) {
            return;
        }

        if (checkCallingPermission(Constants.MANAGER_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String msg = "Permission Denial: call from pid="
                + Binder.getCallingPid()
                + ", uid=" + Binder.getCallingUid()
                + " requires " + Constants.MANAGER_PERMISSION;
        LOGGER.w(msg);
        throw new SecurityException(msg);
    }

    /**
     * @return from sdk client
     */
    private boolean enforceApiClientPermission() {
        if (Binder.getCallingPid() == Os.getpid()) {
            return false;
        }

        if (checkCallingPermission(Constants.SDK_GET_CONFIG_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (checkCallingPermission(Constants.MANAGER_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        String msg = "Permission Denial: call from pid="
                + Binder.getCallingPid()
                + ", uid=" + Binder.getCallingUid()
                + " requires at least " + Constants.SDK_GET_CONFIG_PERMISSION;
        LOGGER.w(msg);
        throw new SecurityException(msg);
    }

    @Override
    public int getVersion() {
        if (!NativeHelper.isNativeProcessAlive()) {
            LOGGER.w("Native process died?");
            return -1;
        }

        return Constants.SERVER_VERSION;
    }

    private RedirectPackageInfoList getRedirectPackagesLocked(
            int userId,
            boolean expandShared, boolean requiresEnabled, boolean allowAllPackages, boolean requiresPackageInfo, boolean requiresPermission) {

        List<Integer> users = new ArrayList<>();
        if (userId == SRManager.USER_ALL) {
            users.addAll(SystemService.getUserIdListNoThrow());
        } else {
            users.add(userId);
        }

        RedirectPackageInfoList res = new RedirectPackageInfoList();

        int pmFlags = PackageManager.GET_PERMISSIONS;

        for (int user : users) {
            final List<UidPackageInfo> packages = UidPackageInfo.list(user, pmFlags);

            // generate a sharedUserId -> requestForPermission map
            final Map<String, Boolean> isPermissionRequestedForShared = new HashMap<>();
            for (UidPackageInfo uidPackageInfo : packages) {
                if (uidPackageInfo.getSharedUserId() == null) {
                    continue;
                }
                Boolean writePermission = isPermissionRequestedForShared.get(uidPackageInfo.getPackageName());
                if (Boolean.TRUE.equals(writePermission)) {
                    continue;
                }
                if (PackageInfoUtils.containsWriteStoragePermission(uidPackageInfo.getPackageInfo())) {
                    isPermissionRequestedForShared.put(uidPackageInfo.getSharedUserId(), true);
                }
            }

            for (UidPackageInfo uidPackageInfo : packages) {
                boolean permission = false;
                RedirectPackageInfo info = mPackages.get(uidPackageInfo.getName(), user);

                if (uidPackageInfo.getSharedUserId() != null) {
                    if (!expandShared && info != null && res.contains(info.packageName, user)) {
                        continue;
                    }
                    if (Boolean.TRUE.equals(isPermissionRequestedForShared.get(uidPackageInfo.getSharedUserId()))) {
                        permission = true;
                    }
                }

                if (info != null) {
                    info = new RedirectPackageInfo(info);
                    if (info.enabled) {
                        permission = true;
                    }
                } else if (allowAllPackages) {
                    info = new RedirectPackageInfo(uidPackageInfo.getPackageName(), user);
                } else {
                    continue;
                }

                if (expandShared) {
                    info.packageName = uidPackageInfo.getPackageName();
                } else {
                    info.packageName = uidPackageInfo.getName();
                }

                if (requiresEnabled && !info.enabled) {
                    continue;
                }
                if (requiresPackageInfo) {
                    info.packageInfo = uidPackageInfo.getPackageInfo();
                    info.systemApp = (info.packageInfo.applicationInfo.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0;
                    info.storagePermission |= PackageInfoUtils.containsReadStoragePermission(info.packageInfo) ? RedirectPackageInfo.PERMISSION_READ : 0;
                    info.storagePermission |= PackageInfoUtils.containsWriteStoragePermission(info.packageInfo) ? RedirectPackageInfo.PERMISSION_WRITE : 0;
                }
                if (requiresPermission && !permission) {
                    if (!PackageInfoUtils.containsWriteStoragePermission(info.packageInfo)) {
                        continue;
                    }
                }
                res.add(info);
            }

            // TODO add non-exist packages
        }

        if (!requiresPackageInfo) {
            for (RedirectPackageInfo info : res) {
                info.packageInfo = null;
            }
        }

        return res;
    }

    @Override
    public ParceledListSlice<RedirectPackageInfo> getRedirectPackages(int flags, int userId) {
        final boolean fromSdk = enforceApiClientPermission();
        if (fromSdk) {
            if ((flags & SRManager.MATCH_ENABLED_ONLY) == 0) {
                flags |= SRManager.MATCH_ENABLED_ONLY;
                LOGGER.w("add MATCH_ENABLED_ONLY for sdk app call");
            }
            if ((flags & SRManager.MATCH_ALL_PACKAGES) > 0) {
                flags &= ~SRManager.MATCH_ALL_PACKAGES;
                LOGGER.w("remove MATCH_ALL_PACKAGES for sdk app call");
            }
        }

        boolean requiresEnabled = (flags & SRManager.MATCH_ENABLED_ONLY) > 0;
        boolean allowAllPackages = (flags & SRManager.MATCH_ALL_PACKAGES) > 0;
        boolean requiresPackageInfo = (flags & SRManager.GET_PACKAGE_INFO) > 0;
        boolean requiresPermission = (flags & SRManager.FLAG_NO_PERMISSION_CHECK) == 0;

        LOCK.lock();
        try {
            return new ParceledListSlice<>(getRedirectPackagesLocked(userId, true, requiresEnabled, allowAllPackages, requiresPackageInfo, requiresPermission));
        } finally {
            LOCK.unlock();
        }
    }

    public RedirectPackageInfo getRedirectPackageInfoLocked(
            final UidPackageInfo uidPackageInfo,
            boolean expandShared,
            boolean requiresEnabled, boolean allowAllPackages, boolean requiresPackageInfo, boolean requiresPermission) {
        if (uidPackageInfo == null) {
            LOGGER.w("getRedirectPackageInfoLocked: UidPackageInfo is null");
            return null;
        }

        final String packageName = uidPackageInfo.getPackageName();
        final String name = uidPackageInfo.getName();
        final int userId = uidPackageInfo.getUserId();
        final PackageInfo pi = uidPackageInfo.getPackageInfo();

        if (requiresPackageInfo && pi == null) {
            return null;
        }
        if (PackageInfoUtils.isOverlay(pi)) {
            return null;
        }

        RedirectPackageInfo info = mPackages.get(name, userId);
        if (info == null && allowAllPackages) {
            info = new RedirectPackageInfo(name, userId);
        }
        if (info == null) {
            return null;
        } else {
            info = new RedirectPackageInfo(info);
        }
        if (requiresEnabled && !info.enabled) {
            return null;
        }
        if (!expandShared) {
            info.packageName = name;
        } else {
            info.packageName = packageName;
        }
        if (requiresPackageInfo) {
            info.packageInfo = pi;
            info.systemApp = (info.packageInfo.applicationInfo.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0;
            info.storagePermission |= PackageInfoUtils.containsReadStoragePermission(info.packageInfo) ? RedirectPackageInfo.PERMISSION_READ : 0;
            info.storagePermission |= PackageInfoUtils.containsWriteStoragePermission(info.packageInfo) ? RedirectPackageInfo.PERMISSION_WRITE : 0;
        }
        if (requiresPermission) {
            if (pi.requestedPermissions == null) {
                pi.requestedPermissions = new String[0];
            }
            boolean permission = PackageInfoUtils.containsWriteStoragePermission(pi);
            if (!permission && uidPackageInfo.getSharedUserId() != null) {
                for (String sharedPackage : PackageManagerApi.getPackagesForUid(uidPackageInfo.getUid())) {
                    if (Objects.equals(sharedPackage, packageName)) {
                        continue;
                    }
                    UidPackageInfo sharedUidPackageInfo = UidPackageInfo.get(sharedPackage, userId, PackageManager.GET_PERMISSIONS);
                    if (sharedUidPackageInfo == null) {
                        continue;
                    }
                    if (PackageInfoUtils.containsWriteStoragePermission(sharedUidPackageInfo.getPackageInfo())) {
                        permission = true;
                        break;
                    }
                }
            }
            if (!permission) {
                return null;
            }
        }
        return info;
    }

    @Override
    public RedirectPackageInfo getRedirectPackageInfo(String packageName, int flags, int userId) {
        boolean fromSdk = enforceApiClientPermission();
        if (fromSdk) {
            if ((flags & SRManager.MATCH_ENABLED_ONLY) == 0) {
                flags |= SRManager.MATCH_ENABLED_ONLY;
                LOGGER.w("add MATCH_ENABLED_ONLY for sdk app call");
            }
            if ((flags & SRManager.MATCH_ALL_PACKAGES) > 0) {
                flags &= ~SRManager.MATCH_ALL_PACKAGES;
                LOGGER.w("remove MATCH_ALL_PACKAGES for sdk app call");
            }
        }

        if (packageName == null) {
            return null;
        }

        boolean expandShared = true;
        boolean requiresEnabled = (flags & SRManager.MATCH_ENABLED_ONLY) > 0;
        boolean allowAllPackages = (flags & SRManager.MATCH_ALL_PACKAGES) > 0;
        boolean requiresPackageInfo = (flags & SRManager.GET_PACKAGE_INFO) > 0;
        boolean requiresPermission = (flags & SRManager.FLAG_NO_PERMISSION_CHECK) == 0;
        int pmFlags = 0;
        if (requiresPackageInfo || requiresPermission) {
            pmFlags |= PackageManager.GET_PERMISSIONS;
        }
        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId, pmFlags);
        if (uidPackageInfo == null) {
            return null;
        }

        LOCK.lock();
        try {
            return getRedirectPackageInfoLocked(uidPackageInfo, expandShared, requiresEnabled, allowAllPackages, requiresPackageInfo, requiresPermission);
        } finally {
            LOCK.unlock();
        }
    }

    public String getRedirectTargetForPackageLocked(String redirectTarget, String packageName) {
        if (DataVerifier.isValidRedirectTarget(redirectTarget)) {
            return String.format(redirectTarget, packageName);
        }
        return String.format(getDefaultRedirectTargetLocked(), packageName);
    }

    private String getRedirectTargetForPackageLocked(@NonNull RedirectPackageInfo info) {
        return getRedirectTargetForPackageLocked(info.redirectTarget, info.packageName);
    }

    private String getRedirectTargetForPackageLocked(String packageName, int userId, boolean expandShared) {
        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);
        if (uidPackageInfo != null) {
            RedirectPackageInfo info = getRedirectPackageInfoLocked(uidPackageInfo, expandShared, false, false, false, false);
            if (info != null) {
                return getRedirectTargetForPackageLocked(info.redirectTarget, info.packageName);
            }
        }
        return String.format(getDefaultRedirectTargetLocked(), packageName);
    }

    public String getRedirectTargetForPackage(String packageName, int userId, boolean expandShared) {
        return getRedirectTargetForPackageLocked(packageName, userId, expandShared);
    }

    private void addRedirectPackageLocked(final RedirectPackageInfo info, final UidPackageInfo uidPackageInfo, boolean writeNativeConfig,
                                          boolean kill, boolean enableObservers, boolean combineConfig, boolean toggleEnableOnly, boolean noPermissionCheck) {
        if (info.packageName == null) {
            return;
        }

        LOGGER.v("request add package %d:%s", info.userId, info.packageName);

        // verify data
        if (info.redirectTarget != null && !DataVerifier.isValidRedirectTarget(info.redirectTarget)) {
            info.redirectTarget = null;
        }
        if (info.mountDirsConfig == null) {
            info.mountDirsConfig = new MountDirsConfig();
        } else if (info.mountDirsConfig.templatesIds != null) {
            for (String id : new HashSet<>(info.mountDirsConfig.templatesIds)) {
                if (!mMountDirsTemplates.contains(id)) {
                    info.mountDirsConfig.templatesIds.remove(id);
                    LOGGER.d("remove invalid template %s", id);
                }
            }
        }
        if (info.mountDirsConfig.customDirs != null
                && !DataVerifier.isValidMountDirs(info.mountDirsConfig.customDirs)) {
            info.mountDirsConfig.customDirs.clear();
            LOGGER.d("clear invalid custom");
        }

        final String name = info.packageName;
        final String packageName = uidPackageInfo != null ? uidPackageInfo.getPackageName() : name;
        final int userId = info.userId;
        final boolean forceUpdateCache = uidPackageInfo != null && UserHandleUtils.isRegularApp(uidPackageInfo.getUid());

        // check storage permission
        final PackageInfo pi = info.packageInfo = uidPackageInfo != null ? uidPackageInfo.getPackageInfo() : null;
        final ApplicationInfo ai = pi != null ? pi.applicationInfo : null;
        final boolean installed = pi != null && pi.applicationInfo != null;
        final String sharedUserId = installed ? pi.sharedUserId : null;
        final List<String> sharedPackages = sharedUserId != null ? PackageManagerApi.getPackagesForUid(pi.applicationInfo.uid, forceUpdateCache) : null;

        if (installed) {
            if (!noPermissionCheck && !PackageInfoUtils.containsWriteStoragePermission(pi)) {
                if (sharedPackages == null) {
                    LOGGER.w("package %d:%s not requests for write storage permission, ignore", userId, packageName);
                    return;
                }

                LOGGER.i("package %d:%s not requests for write storage permission, check other packages with sharedUserId '%s'", userId, packageName, sharedUserId);

                boolean requestedPermission = false;
                for (String sharedPackage : sharedPackages) {
                    PackageInfo pi1 = PackageManagerApi.getPackageInfo(sharedPackage, PackageManager.GET_PERMISSIONS | HiddenValues.MATCH_UNINSTALLED_PACKAGES, userId);
                    if (pi1 != null && PackageInfoUtils.containsWriteStoragePermission(pi1)) {
                        requestedPermission = true;
                        LOGGER.i("package %d:%s not request for write storage permission, but package %d:%s with sharedUserId '%s' do", userId, packageName, userId, sharedPackage, sharedUserId);
                        break;
                    }
                }

                if (!requestedPermission) {
                    LOGGER.w("package %d:%s and all other packages with sharedUserId '%s' not request for write storage permission, ignore", userId, packageName, sharedUserId);
                    return;
                }
            }
        }

        // change package name for shared
        if (sharedUserId != null) {
            info.packageName = SHARED_USER_PREFIX + sharedUserId;
        }

        RedirectPackageInfo current = mPackages.get(info.packageName, userId);
        if (current != null) {
            if (combineConfig) {
                current.enabled |= info.enabled;
                if (!toggleEnableOnly) {
                    if (current.mountDirsConfig != null && info.mountDirsConfig != null) {
                        current.mountDirsConfig.plus(info.mountDirsConfig);
                    }
                    if (info.redirectTarget != null) {
                        if (current.redirectTarget == null
                                || (Constants.REDIRECT_TARGET_CACHE.equals(current.redirectTarget) && Constants.REDIRECT_TARGET_DATA.equals(info.redirectTarget))) {
                            // current=default, new=any / current=cache, new = data
                            current.redirectTarget = info.redirectTarget;
                        }
                    }
                }
            } else {
                current.enabled = info.enabled;
                if (!toggleEnableOnly) {
                    current.redirectTarget = info.redirectTarget;
                    current.mountDirsConfig = info.mountDirsConfig;
                }
            }
        } else {
            mPackages.add(info);
            current = info;
        }

        if (installed && current.enabled) {
            int uid = ai.uid;

            CoreConfig.addUid(uid);

            if (!isStarting) {
                BridgeServiceClient.setUidIsolated(uid, true);

                if (UserHandleUtils.isRegularApp(uid)) {
                    if (sharedPackages != null) {
                        for (String sharedPackage : sharedPackages) {
                            PackageInfo sharedPackageInfo = PackageManagerApi.getPackageInfo(sharedPackage, PackageManager.GET_PERMISSIONS, userId);
                            if (sharedPackageInfo != null) {
                                PermissionHelper.grantPermissions(sharedPackageInfo, sharedPackage, userId, uid);
                            }
                        }
                    } else {
                        PermissionHelper.grantPermissions(pi, name, userId, uid);
                    }
                }
            }

            if (enableObservers && mUnlocked) {
                for (ObserverInfo oi : getObserversLocked(uidPackageInfo, userId, false)) {
                    try {
                        updateObserverLocked(oi, uidPackageInfo);
                    } catch (Exception e) {
                        LOGGER.w("addRedirectPackageLocked: updateObserverLocked", e);
                    }
                }
            }

            CoreConfig.update(current, uidPackageInfo, sharedPackages);

            if (writeNativeConfig) {
                LegacyUidFileWriter.scheduleWriteLocked(current, pi, this, kill);
                if (kill) {
                    PackageHelper.killUid(uid);
                }
            }
        }

        scheduleWriteLocked();

        LOGGER.i("added package %d:%s, target=%s, mount=%s, enabled=%s%s",
                userId, current.packageName,
                current.redirectTarget == null ? "[default]" : current.redirectTarget,
                current.mountDirsConfig.toString(),
                current.enabled ? "true" : "false",
                installed ? "" : ", not installed");
    }

    @Override
    public void addRedirectPackage(RedirectPackageInfo info, int flags) {
        enforceManagerPermission();

        boolean kill = (flags & SRManager.FLAG_KILL_PACKAGE) > 0;
        boolean toggleEnableOnly = (flags & SRManager.FLAG_TOGGLE_ENABLED_ONLY) > 0;
        boolean noPermissionCheck = (flags & SRManager.FLAG_NO_PERMISSION_CHECK) > 0;
        UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId, PackageManager.GET_PERMISSIONS);

        LOCK.lock();
        try {
            if (info.enabled && NativeHelper.isNoValidLicense()) {
                if (getRedirectPackagesLocked(SRManager.USER_ALL, false, true, false, true, false).size() >= 3) {
                    throw new IllegalStateException("");
                }
            }

            addRedirectPackageLocked(info, uidPackageInfo, true, kill, true, false, toggleEnableOnly, noPermissionCheck);
        } finally {
            LOCK.unlock();
        }
    }

    private MountDirsSet getMountDirsForConfigLocked(@Nullable MountDirsConfig config) {
        MountDirsSet mountDirs = new MountDirsSet();

        if (config == null) {
            return mountDirs;
        }
        if (config.isTemplatesAllowed()) {
            if (config.templatesIds != null) {
                for (String id : config.templatesIds) {
                    MountDirsTemplate template;
                    if ((template = mConfig.mountDirsTemplates.get(id)) != null) {
                        mountDirs.addAll(template.list);
                    }
                }
            }
        }
        if (config.isCustomAllowed()) {
            if (config.customDirs != null) {
                mountDirs.addAll(config.customDirs);
            }
        }
        return mountDirs;
    }

    @Override
    public List<String> getMountDirsForConfig(MountDirsConfig config) {
        enforceApiClientPermission();

        Objects.requireNonNull(config);
        LOCK.lock();
        try {
            return new ArrayList<>(getMountDirsForConfigLocked(config));
        } finally {
            LOCK.unlock();
        }
    }

    @NonNull
    public MountDirsSet getMountDirsForPackageLocked(@Nullable UidPackageInfo uidPackageInfo, boolean includeDefault) {
        if (uidPackageInfo == null) {
            return new MountDirsSet();
        }
        RedirectPackageInfo info;
        String name = uidPackageInfo.getName();
        String packageName = uidPackageInfo.getPackageName();
        int userId = uidPackageInfo.getUserId();
        PackageInfo pi = uidPackageInfo.getPackageInfo();
        MountDirsSet res;

        info = mPackages.get(name, userId);
        if (info == null) {
            return new MountDirsSet();
        }

        if (pi.sharedUserId != null) {
            int uid = pi.applicationInfo != null ? pi.applicationInfo.uid : -1;
            res = getMountDirsForConfigLocked(info.mountDirsConfig);
            if (includeDefault && uid != -1) {
                for (String sharedPackage : PackageManagerApi.getPackagesForUid(uid)) {
                    res.addDefaultDirs(sharedPackage, true, true, true);
                }
            }
            /*if (includeDefault) {
                res.addDefaultDirs(name, true, false, false);
            }*/
        } else {
            res = getMountDirsForConfigLocked(info.mountDirsConfig);
            if (includeDefault) {
                res.addDefaultDirs(packageName, true, true, true);
            }
        }

        return res;
    }

    @Override
    public List<String> getMountDirsForPackage(String packageName, int userId, boolean includeDefault) {
        enforceApiClientPermission();

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);
        LOCK.lock();
        try {
            return new ArrayList<>(getMountDirsForPackageLocked(uidPackageInfo, includeDefault));
        } finally {
            LOCK.unlock();
        }
    }

    private boolean setMountDirsForPackageLocked(MountDirsConfig config, UidPackageInfo uidPackageInfo) {
        if (uidPackageInfo == null) {
            return false;
        }

        String packageName = uidPackageInfo.getPackageName();
        int userId = uidPackageInfo.getUserId();

        RedirectPackageInfo current = getRedirectPackageInfoLocked(uidPackageInfo, true, false, true, false, false);
        if (current == null) {
            current = new RedirectPackageInfo(packageName, userId);
        }

        boolean update = !Objects.equals(current.mountDirsConfig, config);
        if (update) {
            current.mountDirsConfig = config;
        }

        if (update) {
            addRedirectPackageLocked(current, uidPackageInfo, true, true, false, false, false, false);

            scheduleWriteLocked();
        }

        return update;
    }

    @Override
    public boolean setMountDirsForPackage(MountDirsConfig config, String packageName, int userId) {
        enforceManagerPermission();

        if (config == null) {
            throw new NullPointerException("config is null");
        }
        if (config.customDirs != null && !DataVerifier.isValidMountDirs(config.customDirs)) {
            throw new IllegalArgumentException("customDirs is not valid");
        }
        if (config.templatesIds != null) {
            for (String id : config.templatesIds) {
                if (getMountDirsTemplate(id) == null) {
                    throw new IllegalArgumentException("bad template id " + id);
                }
            }
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId, PackageManager.GET_PERMISSIONS);
        LOCK.lock();
        try {
            return setMountDirsForPackageLocked(config, uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean setRedirectTargetForPackageLocked(String target, UidPackageInfo uidPackageInfo) {
        if (uidPackageInfo == null) {
            return false;
        }

        final String name = uidPackageInfo.getName();
        final String packageName = uidPackageInfo.getPackageName();
        final int userId = uidPackageInfo.getUserId();
        final PackageInfo pi = uidPackageInfo.getPackageInfo();

        RedirectPackageInfo current = getRedirectPackageInfoLocked(uidPackageInfo, true, false, false, false, false);
        if (current == null) {
            current = new RedirectPackageInfo(packageName, userId);
        }

        String oldFmt = current.redirectTarget;
        String newFmt = target;

        boolean update = !Objects.equals(oldFmt, newFmt);
        if (update) {
            // empty = use default
            if (TextUtils.isEmpty(oldFmt)) {
                oldFmt = getDefaultRedirectTarget();
            }
            if (TextUtils.isEmpty(newFmt)) {
                newFmt = getDefaultRedirectTarget();
            }

            // require move files
            if (!Objects.equals(oldFmt, newFmt)) {
                if (mFileMoveRunnable != null) {
                    throw new IllegalStateException("a file move action is running");
                }

                List<UidPackageInfo> packages = new ArrayList<>();
                packages.add(uidPackageInfo);

                mFileMoveRunnable = new FileMoveRunnable(packages, oldFmt, newFmt);
                mFileMoveRunnable.prepareLocked();
                mFileMoveRunnable.execute(EXECUTOR);
            }

            current.redirectTarget = target;
        }

        if (update) {
            addRedirectPackageLocked(current, uidPackageInfo, true, true, false, false, false, false);

            scheduleWriteLocked();
        }

        return update;
    }

    @Override
    public boolean setRedirectTarget(String target, String packageName, int userId) {
        enforceManagerPermission();

        if (!DataVerifier.isValidRedirectTarget(target) && target != null) {
            throw new IllegalArgumentException(target + " is not valid");
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId, PackageManager.GET_PERMISSIONS);
        LOCK.lock();
        try {
            return setRedirectTargetForPackageLocked(target, uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private void removeRedirectPackageLocked(UidPackageInfo uidPackageInfo, String name, int userId) {
        int uid = uidPackageInfo != null ? uidPackageInfo.getUid() : -1;

        if (uid != -1) {
            CoreConfig.removeUid(uid);
            BridgeServiceClient.setUidIsolated(uid, false);
        }

        if (uidPackageInfo != null) {
            if (uidPackageInfo.getSharedUserId() != null) {
                LOGGER.i("removing shared user %s", uidPackageInfo.getSharedUserId());
                for (String sharedPackageName : PackageManagerApi.getPackagesForUid(uid)) {
                    PackageInfo pi = PackageManagerApi.getPackageInfo(sharedPackageName, PackageManager.GET_PERMISSIONS, userId);
                    if (pi != null) {
                        PackageHelper.forceStopPackage(sharedPackageName, userId);
                        PermissionHelper.revokePermissions(pi, sharedPackageName, userId, uid);
                    }
                }
                PackageHelper.killUid(uid);
            } else {
                String packageName = uidPackageInfo.getPackageName();
                LOGGER.i("removing package %d:%s", userId, packageName);
                PackageInfo pi = uidPackageInfo.getPackageInfo();
                PackageHelper.killPackage(packageName, userId, uid);
                PermissionHelper.revokePermissions(pi, packageName, userId, uid);
            }
        }

        RedirectPackageInfo info = mPackages.get(name, userId);
        if (info != null) {
            if (TextUtils.isEmpty(info.redirectTarget) && info.mountDirsConfig.isNone()) {
                mPackages.remove(name, userId);

                LOGGER.i("removed %d:%s", userId, name);
            } else {
                info.enabled = false;

                LOGGER.i("removed %d:%s (keep config)", userId, name);
            }

            LegacyUidFileWriter.deletePackage(name, userId, uid);
            scheduleWriteLocked();
        }

        getObserverHolderLocked(userId).removeRules(name);
    }

    @Override
    public void removeRedirectPackage(String packageName, int userId) {
        enforceManagerPermission();

        if (packageName == null) {
            return;
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId, PackageManager.GET_PERMISSIONS);
        if (uidPackageInfo == null) {
            LOGGER.w("can't find package %d:%s", userId, packageName);
        }

        LOCK.lock();
        try {
            removeRedirectPackageLocked(uidPackageInfo, (uidPackageInfo != null) ? uidPackageInfo.getName() : packageName, userId);
        } finally {
            LOCK.unlock();
        }
    }

    public ObserverInfoList getObserversLocked(UidPackageInfo uidPackageInfo, int userId, boolean expandShared) {
        boolean all = uidPackageInfo == null;
        String name = all ? null : uidPackageInfo.getName();

        ObserverInfoList res = new ObserverInfoList();
        for (ObserverInfo oi : mObservers) {
            if (!((all || (Objects.equals(oi.packageName, name))) && (userId == SRManager.USER_ALL || userId == oi.userId))) {
                continue;
            }

            if (!all && expandShared && RedirectPackageInfo.isSharedUserId(oi.packageName)) {
                oi = new ObserverInfo(oi);
                oi.packageName = uidPackageInfo.getPackageName();
            }
            res.add(oi);
        }
        return res;
    }

    @Override
    public ParceledListSlice<ObserverInfo> getObservers(String packageName, int userId) {
        enforceApiClientPermission();

        UidPackageInfo uidPackageInfo = null;
        if (packageName != null) {
            uidPackageInfo = UidPackageInfo.get(packageName, userId);
            if (uidPackageInfo == null) {
                LOGGER.w("can't find package %d:%s", userId, packageName);
                return ParceledListSlice.emptyList();
            }
        }
        boolean expandShared = true;
        LOCK.lock();
        try {
            return new ParceledListSlice<>(getObserversLocked(uidPackageInfo, userId, expandShared));
        } finally {
            LOCK.unlock();
        }
    }

    private FileObserverHolder getObserverHolderLocked(int userId) {
        LOCK.lock();
        try {
            FileObserverHolder holder = mObserverHolders.get(userId);
            if (holder == null) {
                holder = ExportFolders.createFileObserverHolder(userId, HandlerKt.getMainHandler());
                mObserverHolders.put(userId, holder);
            }
            return holder;
        } finally {
            LOCK.unlock();
        }
    }

    private ObserverInfo.Result addObserverLocked(ObserverInfo info, @Nullable UidPackageInfo uidPackageInfo) {
        boolean installed = uidPackageInfo != null;
        if (!installed) {
            LOGGER.w("adding observer for uninstalled package: %d:%s %s -> %s", info.userId, info.packageName, info.source, info.target);
        }

        if (installed) {
            info.packageName = uidPackageInfo.getName();
        }

        if (info.enabled) {
            // do not allow conflict rules to be added
            for (ObserverInfo oi : mObservers) {
                if (oi.userId != info.userId)
                    continue;

                if (!oi.enabled)
                    continue;

                final int conflict = DataVerifier.checkIfObserverInfoConflict(oi, info);
                if (conflict != DataVerifier.ObserverInfoConflict.NONE) {
                    if (UidPackageInfo.get(oi.packageName, oi.userId) != null) {
                        return new ObserverInfo.Result(conflict, oi);
                    }
                    oi.enabled = false;
                    LOGGER.i("disable uninstalled observer %d:%s %s -> %s", oi.userId, oi.packageName, oi.source, oi.target);
                }
            }
        }

        if (TextUtils.isEmpty(info.id)) {
            info.id = RandomId.next();
        }

        mObservers.add(info);

        scheduleWriteLocked();

        if (installed && info.enabled) {
            RedirectPackageInfo rpi = mPackages.get(uidPackageInfo.getName(), uidPackageInfo.getUserId());
            if (rpi != null) {
                CoreConfig.update(rpi, uidPackageInfo, null);

                if (!ExportFolders.isFileObserverSupported()) {
                    if (uidPackageInfo.getSharedUserId() != null) {
                        PackageHelper.killUid(uidPackageInfo.getUid());
                    } else {
                        PackageHelper.killPackage(uidPackageInfo.getPackageName(), uidPackageInfo.getUserId(), uidPackageInfo.getUid());
                    }
                }
            }

            FileObserverHolder holder = getObserverHolderLocked(info.userId);
            if (!holder.containsRule(info) && holder.addRule(info)) {
                LOGGER.i("observer %s for package %s (%d) added", info.source, info.packageName, info.userId);
            }
        }

        return new ObserverInfo.Result(true);
    }

    @Override
    public ObserverInfo.Result addObserver(ObserverInfo info) {
        enforceManagerPermission();

        if (!mUnlocked) {
            throw new IllegalStateException("Not unlocked");
        }

        Objects.requireNonNull(info);
        Objects.requireNonNull(info.source, "source can't be null");
        Objects.requireNonNull(info.target, "target can't be null");

        // check mask now
        if (!TextUtils.isEmpty(info.mask)) {
            try {
                info.pattern = Pattern.compile(info.mask);
            } catch (PatternSyntaxException e) {
                throw new IllegalArgumentException("mask is invalid");
            }
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId);
        LOCK.lock();
        try {
            return addObserverLocked(info, uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private ObserverInfo.Result updateObserverLocked(ObserverInfo info, @Nullable UidPackageInfo uidPackageInfo) {
        boolean installed = uidPackageInfo != null;
        if (!installed) {
            LOGGER.w("updating observer for uninstalled package: %d:%s %s -> %s", info.userId, info.packageName, info.source, info.target);
        }

        if (installed) {
            info.packageName = uidPackageInfo.getName();
        }

        if (info.enabled) {
            for (ObserverInfo oi : mObservers) {
                if (oi.userId != info.userId)
                    continue;

                if (!oi.enabled)
                    continue;

                if (oi.equals(info))
                    continue;

                final int conflict = DataVerifier.checkIfObserverInfoConflict(oi, info);
                if (conflict != DataVerifier.ObserverInfoConflict.NONE) {
                    if (UidPackageInfo.get(oi.packageName, oi.userId) != null) {
                        return new ObserverInfo.Result(conflict, oi);
                    }
                    oi.enabled = false;
                    LOGGER.i("disable uninstalled observer %d:%s %s -> %s", oi.userId, oi.packageName, oi.source, oi.target);
                }
            }
        }

        int index = mObservers.indexOf(info);
        if (index != -1) {
            final ObserverInfo oldObserverInfo = mObservers.get(index);
            info.id = oldObserverInfo.id;

            mObservers.set(index, info);

            scheduleWriteLocked();

            FileObserverHolder holder = getObserverHolderLocked(info.userId);
            holder.removeRule(oldObserverInfo);

            if (installed && info.enabled) {
                RedirectPackageInfo rpi = mPackages.get(uidPackageInfo.getName(), uidPackageInfo.getUserId());
                if (rpi != null) {
                    CoreConfig.update(rpi, uidPackageInfo, null);

                    if (!ExportFolders.isFileObserverSupported()) {
                        if (uidPackageInfo.getSharedUserId() != null) {
                            PackageHelper.killUid(uidPackageInfo.getUid());
                        } else {
                            PackageHelper.killPackage(uidPackageInfo.getPackageName(), uidPackageInfo.getUserId(), uidPackageInfo.getUid());
                        }
                    }
                }

                if (!holder.containsRule(info) && holder.addRule(info)) {
                    LOGGER.i("observer %s for package %s (%d) added", info.source, info.packageName, info.userId);
                }
            }

            return new ObserverInfo.Result(true);
        } else {
            return addObserverLocked(info, uidPackageInfo);
        }
    }

    @Override
    public ObserverInfo.Result updateObserver(ObserverInfo info, int flags) {
        enforceManagerPermission();

        if (!mUnlocked) {
            throw new IllegalStateException("Not unlocked");
        }

        Objects.requireNonNull(info);
        Objects.requireNonNull(info.source, "source can't be null");
        Objects.requireNonNull(info.target, "target can't be null");

        if (!TextUtils.isEmpty(info.mask)) {
            try {
                info.pattern = Pattern.compile(info.mask);
            } catch (PatternSyntaxException e) {
                throw new IllegalArgumentException("mask is invalid");
            }
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId);
        LOCK.lock();
        try {
            return updateObserverLocked(info, uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean removeObserverLocked(ObserverInfo info, UidPackageInfo uidPackageInfo) {
        if (uidPackageInfo != null) {
            info.packageName = uidPackageInfo.getName();
        }

        FileObserverHolder holder = getObserverHolderLocked(info.userId);
        holder.removeRule(info);

        if (!mObservers.remove(info)) {
            return false;
        }

        if (uidPackageInfo != null) {
            RedirectPackageInfo rpi = mPackages.get(uidPackageInfo.getName(), uidPackageInfo.getUserId());
            if (rpi != null && rpi.enabled) {
                CoreConfig.update(rpi, uidPackageInfo, null);

                LegacyUidFileWriter.scheduleWriteLocked(rpi, uidPackageInfo.getPackageInfo(), this, true);
                PackageHelper.killUid(uidPackageInfo.getUid());
            }
        }

        scheduleWriteLocked();
        return true;
    }

    @Override
    public boolean removeObserver(ObserverInfo info) {
        enforceManagerPermission();

        if (!mUnlocked) {
            throw new IllegalStateException("Not unlocked");
        }

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId);
        LOCK.lock();
        try {
            return removeObserverLocked(info, uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public void exit(int requestCode) {
        enforceManagerPermission();

        switch (requestCode) {
            case 0: {
                System.exit(EXIT_JAVA_SERVER_ONLY);
                break;
            }
            case 1: {
                if (ConfigProvider.getFile(false).delete())
                    LOGGER.i("configuration deleted");
                else
                    LOGGER.w("delete configuration failed");

                System.exit(EXIT_AND_CLEAR_FILES);
                break;
            }
            case 2: {
                System.exit(EXIT_ALL);
                break;
            }
            default: {
                LOGGER.w("unknown exit request %d", requestCode);
                break;
            }
        }
    }

    @Override
    public DebugInfo getDebugInfo(int flags) {
        enforceManagerPermission();

        mDebugInfo.mediaPathFromNative = NativeHelper.getMediaPath();
        return mDebugInfo;
    }

    @Override
    public String getSavedMediaPath() {
        enforceManagerPermission();

        return mConfig.mediaPath;
    }

    @Override
    public void setSavedMediaPath(String path) {
        enforceManagerPermission();

        if (setSavedMediaPathInternal(path)) {
            LOGGER.i("saved media path: " + (path == null ? "(auto)" : path));

            scheduleWriteLocked();
        }
    }

    private boolean setSavedMediaPathInternal(String path) {
        if (TextUtils.isEmpty(path)) {
            mConfig.mediaPath = null;
            return true;
        }

        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }

        if ("/storage/emulated".equals(path) || !new File(path, "0/Android").exists()) {
            throw new IllegalArgumentException("invalid path");
        }

        mConfig.mediaPath = path;

        EnvironmentCompat.setCachedMediaDirectory(new File(path));

        return true;
    }

    private List<ServerFile> fileManager_listLocked(ServerFile parent, boolean getStat, boolean asPackage) {
        final File parentFile;
        final UidPackageInfo uidPackageInfo = asPackage ? UidPackageInfo.get(parent.getPackageName(), parent.getUserId()) : null;
        if (uidPackageInfo == null) {
            asPackage = false;
        }

        // TODO Android -> use root but filter ?
        /*if (asPackage) {

        }*/

        MountDirsSet excludedDirs = null;
        if (asPackage) {
            parentFile = getFileLocked(parent, uidPackageInfo);
            excludedDirs = getMountDirsForPackageLocked(uidPackageInfo, false);
            for (SimpleMountInfo it : getSimpleMountsLocked(uidPackageInfo, parent.getUserId(), false)) {
                excludedDirs.addAll(it.paths);
            }
        } else {
            parentFile = getFileLocked(parent, null);
        }

        File[] files = FileUtils.listFilesOrEmpty(parentFile);
        ServerFileSet res = new ServerFileSet();
        for (File file : files) {
            final ServerFile f = new ServerFile(
                    parent.getRelativePath() + (!TextUtils.isEmpty(parent.getRelativePath()) ? "/" : "") + file.getName(),
                    parent.getType(),
                    parent.getUserId()
            );

            if (excludedDirs != null && excludedDirs.containsChild(f.getRelativePath())) {
                continue;
            }

            if (asPackage || ServerFile.TYPE_PACKAGE == parent.getType()) {
                f.setPackageName(parent.getPackageName());
            }

            f.setIsDirectory(file.isDirectory());

            if (getStat) {
                try {
                    StructStat stat = Os.stat(parentFile.getAbsolutePath());
                    f.setStat(new ParcelStructStat(stat));
                } catch (ErrnoException e) {
                    LOGGER.w("fileManager_list", e);
                    throw new IllegalStateException(e);
                }
            }

            res.add(f);
        }

        if (asPackage) {
            String name = uidPackageInfo.getName();
            int userId = uidPackageInfo.getUserId();
            if ("".equalsIgnoreCase(parent.getRelativePath())) {
                res.add("Android", true, name, userId);
            } else if ("Android".equalsIgnoreCase(parent.getRelativePath())) {
                res.add("Android/data", true, name, userId);
                res.add("Android/media", true, name, userId);
                res.add("Android/obb", true, name, userId);
            } else if ("Android/data".equalsIgnoreCase(parent.getRelativePath())) {
                res.add("Android/data/" + name, true, null, userId);
            } else if (uidPackageInfo.getSharedUserId() == null && "Android/media".equalsIgnoreCase(parent.getRelativePath())) {
                res.add("Android/media/" + uidPackageInfo.getName(), true, null, userId);
            } else if (uidPackageInfo.getSharedUserId() == null && "Android/obb".equalsIgnoreCase(parent.getRelativePath())) {
                res.add("Android/obb/" + uidPackageInfo.getName(), true, null, userId);
            }
        }

        return new ArrayList<>(res);
    }

    @Override
    public ParceledListSlice<ServerFile> fileManager_list(ServerFile parent, int flags) {
        enforceManagerPermission();

        final boolean getStat = (flags & SRFileManager.FLAG_GET_STAT) > 0;
        final boolean asPackage = ServerFile.TYPE_PACKAGE == parent.getType() && (flags & SRFileManager.FLAG_AS_PACKAGE) > 0;

        LOCK.lock();
        try {
            return new ParceledListSlice<>(fileManager_listLocked(parent, getStat, asPackage));
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public String fileManager_getAbsolutePath(ServerFile file) {
        enforceManagerPermission();

        if (!file.isStorageRoot()) {
            UidPackageInfo uidPackageInfo = UidPackageInfo.get(file.getPackageName(), file.getUserId());
            if (uidPackageInfo != null) {
                for (ObserverInfo observerInfo : getObserversLocked(uidPackageInfo, uidPackageInfo.getUserId(), true)) {
                    if (file.getRelativePath().startsWith(observerInfo.source)) {
                        return FileUtils.buildPathFromEmulated(
                                file.getUserId(),
                                file.getRelativePath().replace(observerInfo.source, observerInfo.target)
                        ).getAbsolutePath();
                    }
                }
            }
        }

        return getFileLocked(file).getAbsolutePath();
    }

    @Override
    public ParcelFileDescriptor fileManager_openFileDescriptor(ServerFile file, String mode) {
        enforceManagerPermission();

        /*
          Copied from ContentResolver.java
         */
        int modeBits;
        if ("r".equals(mode)) {
            modeBits = ParcelFileDescriptor.MODE_READ_ONLY;
        } else if ("w".equals(mode) || "wt".equals(mode)) {
            modeBits = ParcelFileDescriptor.MODE_WRITE_ONLY
                    | ParcelFileDescriptor.MODE_CREATE
                    | ParcelFileDescriptor.MODE_TRUNCATE;
        } else if ("wa".equals(mode)) {
            modeBits = ParcelFileDescriptor.MODE_WRITE_ONLY
                    | ParcelFileDescriptor.MODE_CREATE
                    | ParcelFileDescriptor.MODE_APPEND;
        } else if ("rw".equals(mode)) {
            modeBits = ParcelFileDescriptor.MODE_READ_WRITE
                    | ParcelFileDescriptor.MODE_CREATE;
        } else if ("rwt".equals(mode)) {
            modeBits = ParcelFileDescriptor.MODE_READ_WRITE
                    | ParcelFileDescriptor.MODE_CREATE
                    | ParcelFileDescriptor.MODE_TRUNCATE;
        } else {
            throw new IllegalArgumentException("Invalid mode: " + mode);
        }

        File javaFile = getFileLocked(file);
        try {
            return ParcelFileDescriptor.open(javaFile, modeBits);
        } catch (FileNotFoundException e) {
            LOGGER.w(javaFile + " not found.", e);
            return null;
        }
    }

    @Override
    public long fileManager_length(ServerFile file) {
        enforceManagerPermission();

        return getFileLocked(file).length();
    }

    @Override
    public boolean fileManager_delete(ServerFile file) {
        enforceManagerPermission();

        return getFileLocked(file).delete();
    }

    @Override
    public long fileManager_getLastModified(ServerFile file) {
        enforceManagerPermission();

        return getFileLocked(file).lastModified();
    }

    @Override
    public ParcelStructStat fileManager_stat(ServerFile file) {
        enforceManagerPermission();

        try {
            StructStat stat = Os.stat(getFileLocked(file).getAbsolutePath());

            return new ParcelStructStat(stat);
        } catch (ErrnoException e) {
            LOGGER.w("getFileStat", e);
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean fileManager_move(ServerFile src, ServerFile dst) {
        enforceManagerPermission();

        return getFileLocked(src).renameTo(getFileLocked(dst));
    }

    @Override
    public boolean fileManager_copy(ServerFile src, ServerFile dest) {
        enforceManagerPermission();

        final File oldFile = getFileLocked(src);
        final File newFile = getFileLocked(dest);

        try {
            try (InputStream in = new FileInputStream(oldFile)) {
                if (!newFile.createNewFile()) {
                    return false;
                }
                try (OutputStream out = new FileOutputStream(newFile)) {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    return true;
                }
            }
        } catch (IOException e) {
            LOGGER.w("fileManager_copy failed", e);
            return false;
        }
    }

    @Override
    public boolean fileManager_create(ServerFile file) {
        enforceManagerPermission();

        final File javaFile = getFileLocked(file);

        try {
            return (!javaFile.exists() || javaFile.delete()) && javaFile.createNewFile();
        } catch (IOException e) {
            LOGGER.w("createNewFile failed", e);
            return false;
        }
    }

    @Override
    public boolean fileManager_isDirectory(ServerFile file) {
        enforceManagerPermission();

        return getFileLocked(file).isDirectory();
    }

    @Override
    public boolean fileManager_exists(ServerFile file) {
        enforceManagerPermission();

        return getFileLocked(file).exists();
    }

    @NonNull
    private RedirectPackageInfoList getPackagesForMountDirsTemplateLocked(String id, int userId, boolean expandShared, boolean requirePackageInfo) {
        RedirectPackageInfoList packages = new RedirectPackageInfoList();
        MountDirsTemplate template = getMountDirsTemplateLocked(id);
        if (template == null) {
            return packages;
        }
        RedirectPackageInfoList installedPackages = getRedirectPackagesLocked(userId, expandShared, true, false, requirePackageInfo, false);
        for (RedirectPackageInfo info : installedPackages) {
            if (info.mountDirsConfig.isTemplatesAllowed()
                    && info.mountDirsConfig.templatesIds != null
                    && info.mountDirsConfig.templatesIds.contains(id)) {
                RedirectPackageInfo newInfo = new RedirectPackageInfo(info);
                newInfo.enabled = info.enabled;
                if (!requirePackageInfo) {
                    newInfo.packageInfo = null;
                }
                packages.add(newInfo);
            }
        }
        return packages;
    }

    @Override
    public ParceledListSlice<RedirectPackageInfo> getPackagesForMountDirsTemplate(String id) {
        enforceApiClientPermission();

        LOCK.lock();
        try {
            return new ParceledListSlice<>(getPackagesForMountDirsTemplateLocked(id, SRManager.USER_ALL, true, true));
        } finally {
            LOCK.unlock();
        }
    }

    private void updatePackagesForMountDirsTemplateLocked(String id, List<RedirectPackageInfo> inputNewPackages) {
        MountDirsTemplate template = getMountDirsTemplateLocked(id);
        if (template == null) {
            return;
        }

        RedirectPackageInfoList newPackages = new RedirectPackageInfoList();
        RedirectPackageInfoList oldPackages = getPackagesForMountDirsTemplateLocked(id, SRManager.USER_ALL, false, true);

        for (RedirectPackageInfo it : inputNewPackages) {
            UidPackageInfo uidPackageInfo = UidPackageInfo.get(it.packageName, it.userId);
            if (uidPackageInfo == null) {
                continue;
            }
            if (newPackages.contains(uidPackageInfo.getName(), uidPackageInfo.getUserId())) {
                continue;
            }
            it.packageName = uidPackageInfo.getName();
            newPackages.add(it);
        }

        //LOGGER.i("updatePackagesForMountDirsTemplate: template=%s, newList=%s, oldList=%s", template.id, newPackages.toString(), oldPackages.toString());

        RedirectPackageInfoList installedPackages = getRedirectPackagesLocked(SRManager.USER_ALL, false, true, false, true, false);

        for (RedirectPackageInfo it : installedPackages) {
            UidPackageInfo uidPackageInfo = UidPackageInfo.get(it.packageInfo, it.hidden);
            MountDirsConfig config = it.mountDirsConfig;
            if (config == null) {
                continue;
            }

            if (!newPackages.contains(it.packageName, it.userId)) {
                // remove
                if (config.templatesIds != null && config.templatesIds.remove(id)) {
                    LOGGER.i("updatePackagesForMountDirsTemplate: remove template %s for %d:%s", template.id, it.userId, it.packageName);

                    if (config.templatesIds.isEmpty()) {
                        config.setTemplatesAllowed(false);
                    }

                    addRedirectPackageLocked(it, uidPackageInfo, true, true, false, false, false, false);
                }
            } else if (!oldPackages.contains(it.packageName, it.userId)) {
                // add
                config.setTemplatesAllowed(true);

                if (config.templatesIds == null) {
                    config.templatesIds = new HashSet<>();
                }
                if (config.templatesIds.add(id)) {
                    LOGGER.i("updatePackagesForMountDirsTemplate: add template %s for %d:%s", template.id, it.userId, it.packageName);

                    addRedirectPackageLocked(it, uidPackageInfo, true, true, false, false, false, false);
                }
            }
        }

        scheduleWriteLocked();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void updatePackagesForMountDirsTemplate(String id, ParceledListSlice slice) {
        enforceManagerPermission();

        if (slice == null || slice.getList() == null) {
            return;
        }
        //noinspection unchecked
        List<RedirectPackageInfo> newPackages = slice.getList();

        LOCK.lock();
        try {
            updatePackagesForMountDirsTemplateLocked(id, newPackages);
        } finally {
            LOCK.unlock();
        }
    }

    private MountDirsTemplateList getMountDirsTemplatesLocked() {
        return new MountDirsTemplateList(mConfig.mountDirsTemplates);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public ParceledListSlice getMountDirsTemplates() {
        enforceApiClientPermission();

        LOCK.lock();
        try {
            return new ParceledListSlice<>(getMountDirsTemplatesLocked());
        } finally {
            LOCK.unlock();
        }
    }

    private MountDirsTemplate getMountDirsTemplateLocked(@NonNull String id) {
        return mConfig.mountDirsTemplates.get(id);
    }

    @Nullable
    @Override
    public MountDirsTemplate getMountDirsTemplate(@NonNull String id) {
        enforceApiClientPermission();

        LOCK.lock();
        try {
            return getMountDirsTemplateLocked(id);
        } finally {
            LOCK.unlock();
        }
    }

    private String addMountDirsTemplateLocked(MountDirsTemplate template) {
        if (TextUtils.isEmpty(template.id)) {
            template.id = RandomId.next();
        }

        if (mConfig.mountDirsTemplates.contains(template)) {
            LOGGER.w("addMountDirsTemplateLocked: %s exists", template.id);
            return template.id;
        }

        mConfig.mountDirsTemplates.add(template);

        scheduleWriteLocked();

        return template.id;
    }

    @Override
    public String addMountDirsTemplate(MountDirsTemplate template) {
        enforceManagerPermission();

        LOCK.lock();
        try {
            return addMountDirsTemplateLocked(template);
        } finally {
            LOCK.unlock();
        }
    }

    private void updateMountDirsTemplateLocked(MountDirsTemplate template) {
        //LOGGER.i("updateMountDirsTemplate %s", template.toString());

        MountDirsTemplate original;

        int index = mConfig.mountDirsTemplates.indexOf(template);
        if (index == -1) {
            addMountDirsTemplate(template);
            return;
        } else {
            original = mConfig.mountDirsTemplates.get(index);
            mConfig.mountDirsTemplates.set(index, template);
        }

        // Update redirect package info if template is updated
        if (original != null && !DataVerifier.isSameCollections(template.list, original.list)) {
            for (RedirectPackageInfo info : getPackagesForMountDirsTemplateLocked(template.id, SRManager.USER_ALL, false, true)) {
                if (!info.enabled) {
                    continue;
                }
                UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageInfo, info.hidden);
                if (uidPackageInfo == null) {
                    continue;
                }

                CoreConfig.update(info, uidPackageInfo, null);
                LegacyUidFileWriter.scheduleWriteLocked(info, uidPackageInfo.getPackageInfo(), this, true);
                PackageHelper.killUid(uidPackageInfo.getUid());
            }
        }

        scheduleWriteLocked();
    }

    @Override
    public void updateMountDirsTemplate(MountDirsTemplate template) {
        enforceManagerPermission();

        LOCK.lock();
        try {
            updateMountDirsTemplateLocked(template);
        } finally {
            LOCK.unlock();
        }
    }

    private void removeMountDirsTemplateLocked(String id) {
        boolean removed = false;
        for (MountDirsTemplate item : mConfig.mountDirsTemplates) {
            if (id.equals(item.id)) {
                mConfig.mountDirsTemplates.remove(item);
                removed = true;
                break;
            }
        }

        if (!removed) {
            LOGGER.w("template %s not exists", id);
            return;
        }

        // Update redirect package info if template is removed
        for (RedirectPackageInfo rpi : mPackages) {
            if (rpi.enabled
                    && rpi.mountDirsConfig != null
                    && rpi.mountDirsConfig.templatesIds != null
                    && rpi.mountDirsConfig.templatesIds.remove(id)) {
                UidPackageInfo uidPackageInfo = UidPackageInfo.get(rpi.packageName, rpi.userId);
                if (uidPackageInfo == null) {
                    continue;
                }
                CoreConfig.update(rpi, uidPackageInfo, null);
                LegacyUidFileWriter.scheduleWriteLocked(rpi, uidPackageInfo.getPackageInfo(), this, true);
                PackageHelper.killUid(uidPackageInfo.getUid());
            }
        }

        scheduleWriteLocked();
    }

    @Override
    public void removeMountDirsTemplate(String id) {
        enforceManagerPermission();

        Objects.requireNonNull(id);

        LOCK.lock();
        try {
            removeMountDirsTemplateLocked(id);
        } finally {
            LOCK.unlock();
        }
    }

    @NonNull
    public SimpleMountInfoList getSimpleMountsLocked(UidPackageInfo targetUidPackageInfo, int userId, boolean expandShared) {
        boolean all = targetUidPackageInfo == null;
        String targetName = all ? null : targetUidPackageInfo.getName();

        SimpleMountInfoList res = new SimpleMountInfoList();
        for (SimpleMountInfo smi : mSimpleMounts) {
            if (!((all || (Objects.equals(smi.targetPackage, targetName))) && (userId == SRManager.USER_ALL || userId == smi.userId))) {
                continue;
            }

            if (!all && expandShared && RedirectPackageInfo.isSharedUserId(smi.targetPackage)) {
                smi = new SimpleMountInfo(smi);
                smi.targetPackage = targetUidPackageInfo.getPackageName();
            }
            res.add(smi);
        }
        return res;
    }

    @Override
    public ParceledListSlice<SimpleMountInfo> getSimpleMounts(String targetPackageName, int flags, int userId) {
        enforceApiClientPermission();

        UidPackageInfo targetUidPackageInfo = null;
        if (targetPackageName != null) {
            targetUidPackageInfo = UidPackageInfo.get(targetPackageName, userId);
            if (targetUidPackageInfo == null) {
                LOGGER.w("can't find package %d:%s", userId, targetPackageName);
                return ParceledListSlice.emptyList();
            }
        }
        boolean expandShared = true;
        LOCK.lock();
        try {
            return new ParceledListSlice<>(getSimpleMountsLocked(targetUidPackageInfo, userId, expandShared));
        } finally {
            LOCK.unlock();
        }
    }

    private boolean addSimpleMountLocked(SimpleMountInfo smi, @Nullable UidPackageInfo sourceUidPackageInfo, @Nullable UidPackageInfo targetUidPackageInfo, boolean enableRedirect, boolean allowAdd, boolean kill) {
        boolean installed = sourceUidPackageInfo != null && targetUidPackageInfo != null;
        if (!installed) {
            LOGGER.w("adding simple mount for uninstalled package: %d:%s %s -> %s", smi.userId, smi.sourcePackage, smi.paths, smi.targetPackage);
        }

        RedirectPackageInfo targetInfo = null;
        if (smi.enabled) {
            targetInfo = getRedirectPackageInfoLocked(targetUidPackageInfo, true, true, false, false, false);

            if (targetInfo == null) {
                targetInfo = new RedirectPackageInfo(smi.targetPackage, smi.userId);
            }

            if (!targetInfo.enabled && !enableRedirect) {
                throw new IllegalArgumentException("targetPackage is not redirected");
            }
            if (!targetInfo.enabled) {
                targetInfo.enabled = true;
                addRedirectPackageLocked(targetInfo, targetUidPackageInfo, false, true, true, false, false, true);
            }
            if (getRedirectPackageInfoLocked(targetUidPackageInfo, true, true, false, false, false) == null) {
                throw new IllegalArgumentException("can't enable redirect for targetPackage");
            }
        }

        if (TextUtils.isEmpty(smi.id)) {
            smi.id = RandomId.next();
        }

        if (installed) {
            smi.sourcePackage = sourceUidPackageInfo.getName();
            smi.targetPackage = targetUidPackageInfo.getName();
        }

        if (allowAdd) {
            for (SimpleMountInfo it : mSimpleMounts) {
                if (it.equalsContent(smi)) {
                    throw new IllegalArgumentException("exists");
                }
            }
        }

        boolean updated = true;

        int index = mSimpleMounts.indexOf(smi);
        if (index != -1) {
            if (allowAdd)
                throw new IllegalArgumentException("exists");

            SimpleMountInfo current = mSimpleMounts.get(index);
            updated = !current.equalsAllFields(smi) || current.enabled != smi.enabled;

            mSimpleMounts.set(index, smi);

            LOGGER.i("simple mount %s: %d %s %s -> %s updated (%s) ", smi.id, smi.userId, smi.sourcePackage, smi.paths, smi.targetPackage, smi.enabled ? "enabled" : "disabled");
        } else {
            mSimpleMounts.add(smi);

            LOGGER.i("simple mount %s: %d %s %s -> %s added, (%s)", smi.id, smi.userId, smi.sourcePackage, smi.paths, smi.targetPackage, smi.enabled ? "enabled" : "disabled");
        }

        scheduleWriteLocked();

        if (installed && smi.enabled && targetInfo != null && targetInfo.enabled) {
            CoreConfig.update(targetInfo, targetUidPackageInfo, null);
            LegacyUidFileWriter.scheduleWriteLocked(targetInfo, targetUidPackageInfo.getPackageInfo(), this, kill);
            if (kill) {
                PackageHelper.killUid(targetUidPackageInfo.getUid());
            }
        }
        return updated;
    }

    @Override
    public boolean addSimpleMount(SimpleMountInfo info, int flags) {
        enforceManagerPermission();

        Objects.requireNonNull(info, "info can't be null");
        Objects.requireNonNull(info.sourcePackage, "sourcePackage can't be null");
        Objects.requireNonNull(info.targetPackage, "targetPackage can't be null");
        Objects.requireNonNull(info.paths, "paths can't be null");

        info.paths.remove("Android");
        info.paths.remove("Android/data");
        info.paths.remove("Android/media");
        info.paths.remove("Android/obb");

        if (info.sourcePackage.equals(info.targetPackage)) {
            throw new IllegalArgumentException("sourcePackage targetPackage can't be same");
        }

        if (!DataVerifier.isValidSimpleMountDirs(info.paths)) {
            throw new IllegalArgumentException("invalid paths (contains forbidden folders)");
        }

        boolean allowAdd = (flags & SRManager.FLAG_UPDATE_ON_EXISTS) == 0;
        boolean enableRedirect = (flags & SRManager.FLAG_FORCE_ENABLE_REDIRECT) > 0;
        boolean kill = (flags & SRManager.FLAG_KILL_PACKAGE) != 0;
        UidPackageInfo sourceInfo = UidPackageInfo.get(info.sourcePackage, info.userId, PackageManager.GET_PERMISSIONS);
        UidPackageInfo targetInfo = UidPackageInfo.get(info.targetPackage, info.userId, PackageManager.GET_PERMISSIONS);

        LOCK.lock();
        try {
            return addSimpleMountLocked(info, sourceInfo, targetInfo, enableRedirect, allowAdd, kill);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean removeSimpleMountLocked(SimpleMountInfo info, UidPackageInfo targetUidPackageInfo) {
        if (mSimpleMounts.remove(info)) {
            scheduleWriteLocked();

            if (targetUidPackageInfo != null) {
                RedirectPackageInfo rpi = mPackages.get(targetUidPackageInfo.getName(), targetUidPackageInfo.getUserId());
                if (rpi != null && rpi.enabled) {
                    CoreConfig.update(rpi, targetUidPackageInfo, null);
                    LegacyUidFileWriter.scheduleWriteLocked(rpi, targetUidPackageInfo.getPackageInfo(), this, true);
                    PackageHelper.killUid(targetUidPackageInfo.getUid());
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean removeSimpleMount(SimpleMountInfo info) {
        enforceManagerPermission();

        UidPackageInfo targetUidPackageInfo = UidPackageInfo.get(info.targetPackage, info.userId);
        LOCK.lock();
        try {
            return removeSimpleMountLocked(info, targetUidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private int getNonStandardFilesCountLocked(final UidPackageInfo uidPackageInfo) {
        if (uidPackageInfo == null) {
            return 0;
        }
        RedirectPackageInfo info = getRedirectPackageInfoLocked(uidPackageInfo, false, false, false, true, false);
        if (info == null) {
            return 0;
        }
        int userId = uidPackageInfo.getUserId();

        File file = FileUtils.buildPathFromEmulated(userId, getRedirectTargetForPackageLocked(info));
        if (!file.exists() || !file.canRead()) {
            return 0;
        }

        final Set<String> mountDirs = getMountDirsForPackageLocked(uidPackageInfo, false);
        final File[] files = file.listFiles((dir, name) -> {
            if ("Android".equalsIgnoreCase(name)) {
                return false;
            }
            for (String s : mountDirs) {
                if (s.equalsIgnoreCase(name)) {
                    return false;
                }
            }
            return true;
        });

        return files == null ? 0 : files.length;
    }

    @Override
    public int getNonStandardFilesCount(final String packageName, final int userId) {
        enforceManagerPermission();

        UidPackageInfo info = UidPackageInfo.get(packageName, userId);
        LOCK.lock();
        try {
            return getNonStandardFilesCountLocked(info);
        } finally {
            LOCK.unlock();
        }
    }

    private String getDefaultRedirectTargetLocked() {
        return mConfig.defaultTarget;
    }

    @Override
    public String getDefaultRedirectTarget() {
        enforceApiClientPermission();

        LOCK.lock();
        try {
            return getDefaultRedirectTargetLocked();
        } finally {
            LOCK.unlock();
        }
    }

    private boolean setDefaultRedirectTargetLocked(String newFmt) {
        if (Objects.equals(newFmt, mConfig.defaultTarget)) {
            return false;
        }

        if (mFileMoveRunnable != null) {
            throw new IllegalStateException("a file move action is running");
        }

        if (!DataVerifier.isValidRedirectTarget(newFmt)) {
            throw new IllegalArgumentException(newFmt + " is not valid");
        }

        String oldFmt = mConfig.defaultTarget;

        List<UidPackageInfo> packages = new ArrayList<>();
        for (RedirectPackageInfo info : mPackages) {
            // skip packages which target had been set
            if (!TextUtils.isEmpty(info.redirectTarget)) {
                continue;
            }
            // skip uninstalled
            UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId);
            if (uidPackageInfo == null) {
                continue;
            }
            packages.add(uidPackageInfo);
        }

        mFileMoveRunnable = new FileMoveRunnable(packages, oldFmt, newFmt);
        mFileMoveRunnable.prepareLocked();
        mFileMoveRunnable.execute(EXECUTOR);

        mConfig.defaultTarget = newFmt;
        scheduleWriteLocked();

        LegacyUidFileWriter.writeDefault(mConfig);

        return true;
    }

    @Override
    public boolean setDefaultRedirectTarget(String newFmt) {
        enforceManagerPermission();

        LOCK.lock();
        try {
            return setDefaultRedirectTargetLocked(newFmt);
        } finally {
            LOCK.unlock();
        }
    }

    public File getFileLocked(ServerFile serverFile) {
        return getFileLocked(serverFile, null);
    }

    public File getFileLocked(ServerFile serverFile, UidPackageInfo uidPackageInfo) {
        final String relativePath = serverFile.getRelativePath();
        final int userId = serverFile.getUserId();

        if (ServerFile.TYPE_STORAGE_ROOT == serverFile.getType()) {
            return FileUtils.buildPathFromEmulated(userId, relativePath);
        }

        final String name = serverFile.getPackageName();

        MountDirsSet mountDirs = null;
        if (uidPackageInfo != null) {
            mountDirs = getMountDirsForPackageLocked(uidPackageInfo, true);
        }

        File file;
        RedirectPackageInfo info = getRedirectPackageInfoLocked(UidPackageInfo.get(name, userId), false, false, true, false, false);
        if (mountDirs == null || !mountDirs.containsChild(serverFile.getRelativePath())) {
            if (info != null) {
                file = FileUtils.buildPathFromEmulated(userId, getRedirectTargetForPackageLocked(info));
            } else {
                file = FileUtils.buildPathFromEmulated(userId, getRedirectTargetForPackageLocked(null, name));
            }
        } else {
            file = FileUtils.buildPathFromEmulated(userId);
        }

        if (!TextUtils.isEmpty(relativePath)) {
            file = new File(file, relativePath);
        }

        return file;
    }

    @Override
    public long getRedirectedProcessCount() {
        enforceManagerPermission();

        return mRedirectProcessCount;
    }

    @Override
    public ModuleStatus getModuleStatus() {
        enforceManagerPermission();

        return EnhanceModule.status();
    }

    @Override
    public ModuleStatus getRiruStatus() {
        enforceManagerPermission();

        return Riru.status();
    }

    @Override
    public boolean isDisableExportNotificationEnabled() {
        enforceManagerPermission();

        return mConfig.moduleConfig.disableExportNotification;
    }

    private void setDisableExportNotificationEnabledLocked(boolean enabled) {
        mConfig.moduleConfig.disableExportNotification = enabled;
        scheduleWriteLocked();
    }

    @Override
    public void setDisableExportNotificationEnabled(boolean enabled) {
        enforceManagerPermission();

        DownloadManager.setIsNotificationEnabled(EnhanceModule.status().getVersionCode() <= 0 || !enabled);

        LOCK.lock();
        try {
            setDisableExportNotificationEnabledLocked(EnhanceModule.status().getVersionCode() <= 0 || enabled);
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public boolean isSdcardfsUsed() {
        enforceManagerPermission();

        return NativeHelper.isSdcardfsUsed();
    }

    private boolean isNewAppNotificationEnabledInternal() {
        return mConfig.moduleConfig.enableNewAppNotification;
    }

    @Override
    public boolean isNewAppNotificationEnabled() {
        enforceManagerPermission();

        return isNewAppNotificationEnabledInternal();
    }

    @Override
    public void setNewAppNotificationEnabled(boolean enabled) {
        enforceManagerPermission();
        mConfig.moduleConfig.enableNewAppNotification = enabled;
    }

    @Override
    public boolean isUserStorageAvailable(int userId) throws RemoteException {
        enforceManagerPermission();

        return SystemService.isUserStorageAvailable(userId);
    }

    @Deprecated
    @Override
    public boolean isModuleExists() {
        enforceManagerPermission();

        throw new UnsupportedOperationException();
    }

    private boolean isFileMonitorEnabledLocked() {
        return mConfig.moduleConfig.enableFileMonitor;
    }

    @Override
    public boolean isFileMonitorEnabled() {
        enforceManagerPermission();

        LOCK.lock();
        try {
            return isFileMonitorEnabledLocked();
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public void setFileMonitorEnabled(boolean enabled) {
        enforceManagerPermission();

        if (mConfig.moduleConfig.enableFileMonitor != enabled) {
            mConfig.moduleConfig.enableFileMonitor = enabled;
            scheduleWriteLocked();
        }
    }

    private boolean isFixRenameEnabledLocked() {
        return mConfig.moduleConfig.enableRenameFix;
    }

    @Override
    public boolean isFixRenameEnabled() {
        enforceManagerPermission();

        LOCK.lock();
        try {
            return isFixRenameEnabledLocked();
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public void setFixRenameEnabled(boolean enabled) {
        enforceManagerPermission();

        if (mConfig.moduleConfig.enableRenameFix != enabled) {
            mConfig.moduleConfig.enableRenameFix = enabled;
            scheduleWriteLocked();
        }
    }

    @Override
    public boolean isKillMediaStorageOnStartEnabled() {
        enforceManagerPermission();

        return mConfig.killMediaStorageOnStart;
    }

    @Override
    public void setKillMediaStorageOnStartEnabled(boolean enabled) {
        enforceManagerPermission();

        mConfig.killMediaStorageOnStart = enabled;
        scheduleWriteLocked();
    }

    @Override
    public boolean isNoLogDetected() {
        enforceManagerPermission();

        return mNoLogDetected;
    }

    @Override
    public boolean isFixInteractionEnabled() {
        enforceManagerPermission();

        return mConfig.moduleConfig.enableAppInteractionFix;
    }

    @Override
    public void setFixInteractionEnabled(boolean enabled) {
        enforceManagerPermission();

        if (mConfig.moduleConfig.enableAppInteractionFix != enabled) {
            mConfig.moduleConfig.enableAppInteractionFix = enabled;
            scheduleWriteLocked();
        }
    }

    @Override
    public int setPublicSdkVersion(int sdk) {
        enforceApiClientPermission();

        ModelVersion.setCallingProcessSdkVersion(sdk);
        return ModelVersion.MAX_SDK_VERSION;
    }

    @Override
    public IRemoteProvider wrapContentProvider(IBinder provider) {
        enforceManagerPermission();

        if (provider == null)
            return null;

        return new RemoteProviderHolder(ContentProviderNative.asInterface(provider), null, 0, null);
    }

    @Override
    public ParceledListSlice<FileMonitorRecord> queryFileMonitor(String selection, String[] selectionArgs, String orderBy, String limit) {
        enforceManagerPermission();

        try {
            return new ParceledListSlice<>(FileMonitor.query(selection, selectionArgs, orderBy, limit));
        } catch (Throwable e) {
            LOGGER.w("queryFileMonitor", e);
            throw new IllegalStateException(e);
        }
    }

    @Override
    public int deleteFileMonitor(String whereClause, String[] whereArgs) {
        enforceManagerPermission();

        try {
            return FileMonitor.delete(whereClause, whereArgs);
        } catch (Exception e) {
            LOGGER.w("deleteFileMonitor", e);
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean isOngoingFileMoveExists() {
        enforceManagerPermission();

        return mFileMoveRunnable != null;
    }

    @Override
    public void registerFileMoveListener(IFileMoveListener listener) {
        enforceManagerPermission();

        LOCK.lock();
        try {
            try {
                FileMoveListenerHolder holder = new FileMoveListenerHolder(listener);
                listener.asBinder().linkToDeath(holder, 0);
                mFileMoveListeners.add(holder);
            } catch (RemoteException e) {
                // already dead
            }
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public void unregisterFileMoveListener(IFileMoveListener listener) {
        enforceManagerPermission();

        LOCK.lock();
        try {
            final int N = mFileMoveListeners.size();
            for (int i = N - 1; i >= 0; i--) {
                final FileMoveListenerHolder holder = mFileMoveListeners.get(i);
                if (holder.listener.asBinder() == listener.asBinder()) {
                    mFileMoveListeners.remove(i);
                }
            }
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public int isLicenseValid() {
        enforceManagerPermission();
        return mLicenseStatus;
    }

    private void calculateFoldersSizeInRedirectTargetLocked(UidPackageInfo uidPackageInfo, IFileCalculatorListener listener) {
        ServerFile root = ServerFile.fromRedirectTarget("", uidPackageInfo.getPackageName(), uidPackageInfo.getUserId());
        root.setIsDirectory(true);

        new FileUtils.FoldersSizeCalculator(this, root, listener).calculate();
    }

    @Override
    public void calculateFoldersSizeInRedirectTarget(String packageName, int userId, IFileCalculatorListener listener) {
        enforceManagerPermission();

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);
        if (uidPackageInfo == null) {
            LOGGER.w("can't find package %d:%s", userId, packageName);
            try {
                listener.onFinished(ParceledListSlice.emptyList());
            } catch (Throwable e) {
                LOGGER.w(e, "onFinished");
            }
            return;
        }

        LOCK.lock();
        try {
            calculateFoldersSizeInRedirectTargetLocked(uidPackageInfo, listener);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean isFixInteractionEnabledForPackageLocked(String name, int userId) {
        for (Config.ModuleConfig.Package pkg : mConfig.moduleConfig.packages) {
            if (Objects.equals(name, pkg.name) && userId == pkg.userId) {
                return pkg.enableAppInteractionFix;
            }
        }
        return true;
    }

    private boolean isFixInteractionEnabledForPackageLocked(UidPackageInfo uidPackageInfo) {
        return isFixInteractionEnabledForPackageLocked(uidPackageInfo.getName(), uidPackageInfo.getUserId());
    }

    @Override
    public boolean isFixInteractionEnabledForPackage(String packageName, int userId) {
        enforceManagerPermission();

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);
        if (uidPackageInfo == null) {
            LOGGER.w("can't find uid for %d:%s", userId, packageName);
            return true;
        }
        LOCK.lock();
        try {
            return isFixInteractionEnabledForPackageLocked(uidPackageInfo);
        } finally {
            LOCK.unlock();
        }
    }

    private void setFixInteractionEnabledForPackageLocked(UidPackageInfo uidPackageInfo, boolean enabled) {
        boolean oldEnabled = isFixInteractionEnabledForPackageLocked(uidPackageInfo);
        if (oldEnabled != enabled) {
            Config.ModuleConfig.Package pkg = null;
            for (Config.ModuleConfig.Package it : mConfig.moduleConfig.packages) {
                if (Objects.equals(uidPackageInfo.getName(), it.name) && uidPackageInfo.getUserId() == it.userId) {
                    pkg = it;
                    break;
                }
            }
            if (pkg == null) {
                pkg = new Config.ModuleConfig.Package(uidPackageInfo.getName(), uidPackageInfo.getUserId());
                mConfig.moduleConfig.packages.add(pkg);
            }
            pkg.enableAppInteractionFix = enabled;

            scheduleWriteLocked();

            PackageHelper.killUid(uidPackageInfo.getUid());
        }
    }

    @Override
    public void setFixInteractionEnabledForPackage(String packageName, int userId, boolean enabled) {
        enforceManagerPermission();

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, userId);
        if (uidPackageInfo == null) {
            LOGGER.w("can't find uid for %d:%s", userId, packageName);
            return;
        }

        LOCK.lock();
        try {
            setFixInteractionEnabledForPackageLocked(uidPackageInfo, enabled);
        } finally {
            LOCK.unlock();
        }
    }

    private boolean isBlockRemountEnabledLocked() {
        return true;
    }

    @Override
    public boolean isBlockRemountEnabled() {
        enforceManagerPermission();
        LOCK.lock();
        try {
            return isBlockRemountEnabledLocked();
        } finally {
            LOCK.unlock();
        }
    }

    @Override
    public void setBlockRemountEnabled(boolean enabled) {
        enforceManagerPermission();
    }

    @NonNull
    private List<FolderSizeEntry> calculateFoldersSizeIn(@NonNull ServerFile parent) {
        List<FolderSizeEntry> res = new ArrayList<>();
        FolderSizeEntry entry = new FolderSizeEntry(parent);
        res.add(entry);
        File file = getFileLocked(parent);
        File[] childFiles = file.listFiles();
        if (childFiles != null) {
            for (File child : childFiles) {
                if (child.isFile()) {
                    entry.addSize(child.length());
                } else if (child.isDirectory()) {
                    ServerFile childFile = new ServerFile(parent, child.getName());
                    childFile.setIsDirectory(true);
                    List<FolderSizeEntry> resInChild = calculateFoldersSizeIn(childFile);
                    entry.addSize(resInChild.get(0).getSize());
                    res.addAll(resInChild);
                }
            }
        }
        return res;
    }

    public void killPackage(String packageName, int userId) {
        ApplicationInfo ai;
        try {
            ai = SystemService.getApplicationInfo(packageName, 0, userId);
        } catch (Throwable throwable) {
            LOGGER.w("getApplicationInfo %d:%s", userId, packageName);
            return;
        }
        if (ai != null) {
            PackageHelper.killPackage(packageName, userId, ai.uid);
        }
    }

    @Override
    public String[] getPackagesForUid(int uid) {
        enforceManagerPermission();

        try {
            return SystemService.getPackagesForUid(uid);
        } catch (Throwable e) {
            return null;
        }
    }

    @Override
    public int getUidForSharedUser(String sharedUserId, int userId) {
        enforceManagerPermission();

        return PackageManagerApi.getUidForSharedUser(sharedUserId, userId);
    }

    @Override
    public PackageInfo getPackageInfo(String packageName, int flags, int userId) {
        enforceManagerPermission();

        return SystemService.getPackageInfoNoThrow(packageName, flags, userId);
    }

    @Override
    public ParceledListSlice<PackageInfo> getInstalledPackages(int flags, int userId) {
        enforceManagerPermission();

        return new ParceledListSlice<>(SystemService.getInstalledPackagesNoThrow(flags, userId));
    }

    @Override
    public ApplicationInfo getApplicationInfo(String packageName, int flags, int userId) {
        enforceManagerPermission();

        return SystemService.getApplicationInfoNoThrow(packageName, flags, userId);
    }

    @Override
    public void updateLicense(int type, String value) {
        enforceManagerPermission();

        NativeHelper.updateLicense(type, value);
    }

    @Override
    public void insertFileMonitor(String callingPackage, String path, String func) {
        if (callingPackage == null || path == null || func == null) return;

        int callingUid = Binder.getCallingUid();

        long token = Binder.clearCallingIdentity();
        try {
            long res = FileMonitor.insert(callingPackage, UserHandleUtils.getUserId(callingUid), path, func);
            LOGGER.d("insertFileMonitor: path=%s, func=%s, uid=%d, res=%d", path, func, callingUid, res);
        } catch (Throwable e) {
            LOGGER.w(Log.getStackTraceString(e));
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    private List<String> requestCheckProcessImpl(int pid, int uid, @Nullable String packageName) {
        LOGGER.v("requestCheckProcess: pid=%d, uid=%d, pkg=%s", pid, uid, packageName);

        int userId = UserHandleUtils.getUserId(uid);
        List<String> res = new ArrayList<>();

        if (isFileMonitorEnabledLocked()) {
            res.add("--enable-file-monitor");
        }
        if (isFixRenameEnabledLocked()) {
            res.add("--enable-fix-rename");
        }

        if (!CoreConfig.hasUid(uid)) {
            LOGGER.d("%d %s does not require isolation", uid, packageName);
            return res;
        }

        LOGGER.v("get config for %d %s", uid, packageName);
        CoreConfig.UidConfig config = CoreConfig.get(uid);
        LOGGER.v("get config for %d %s finished", uid, packageName);

        if (config == null) {
            LOGGER.w("uid %s is isolated but no cached config", uid);
            return res;
        }

        List<String> sharedPackages = config.sharedPackages;
        Collection<Pair<String, String>> mountPairs = config.mounts;
        Collection<Pair<String, String>> observerPairs = config.observers;
        String targetPath = config.targetPath;
        String targetPackageName = config.targetName;
        boolean isSystem = config.isSystem;
        int flags = config.flags;

        if (!mUnlocked/* && (BuildUtils.isFuse() || VoldProperties.isFileBasedEncryption())*/) {
            flags |= CoreService.FLAG_ENCRYPTED;
        }

        if (UserHandleUtils.isRegularApp(uid) && !isSystem) {
            LOGGER.v("grant permission for %d %s", uid, packageName);
            for (String sharedPackageName : sharedPackages) {
                PackageInfo pi = PackageManagerApi.getPackageInfo(sharedPackageName, PackageManager.GET_PERMISSIONS, UserHandleUtils.getUserId(uid));
                if (pi != null) {
                    PermissionHelper.grantPermissions(pi, sharedPackageName, UserHandleUtils.getUserId(uid), pi.applicationInfo.uid);
                }
            }
            LOGGER.v("grant permission for %d %s finished", uid, packageName);
        } else {
            LOGGER.v("skip grant permission for %d %s", uid, packageName);
        }

        if (isFixInteractionEnabledForPackageLocked(packageName, userId)) {
            res.add("--enable-dex-inject");
        }

        res.add("--name");
        res.add(targetPackageName);

        res.add("--target");
        res.add(targetPath);

        res.add("--mounts");
        for (Pair<String, String> it : mountPairs) {
            res.add(it.getFirst());
            res.add(it.getSecond());
        }

        res.add("--observers");
        for (Pair<String, String> it : observerPairs) {
            res.add(it.getFirst());
            res.add(it.getSecond());
        }

        int reply = -1;
        try {
            LOGGER.v("connect to daemon for %d %s", uid, packageName);
            reply = CoreService.requestCheckProcess(mSocketToken, pid, uid, targetPackageName, flags, targetPath, mountPairs, observerPairs);
            LOGGER.v("connect to daemon for %d %s finished", uid, packageName);
        } catch (Throwable e) {
            LOGGER.w(Log.getStackTraceString(e));
        }

        if (reply != -1) {
            mLicenseStatus = reply;
            if (mLicenseStatus == 0) {
                mRedirectProcessCount += 1;
            }
            Log.d("License", "reply: " + mLicenseStatus);
        }
        return res;
    }

    @Override
    public String[] requestCheckProcess(int moduleVersion, String callingPackage) {
        int callingUid = Binder.getCallingUid();
        int callingPid = Binder.getCallingPid();

        LOGGER.v("requestCheckProcess: pid=%d, uid=%d, pkg=%s", callingPid, callingUid, callingPackage);

        long token = Binder.clearCallingIdentity();
        try {
            Callable<String[]> task = () -> requestCheckProcessImpl(callingPid, callingUid, callingPackage).toArray(new String[0]);
            String[] res = REQUEST_EXECUTOR.submit(task)
                    .get(5, TimeUnit.SECONDS);

            /*String[] res;
            res = requestCheckProcessLocked(callingPid, callingUid, callingPackage).toArray(new String[0]);*/

            LOGGER.v("requestCheckProcess: " + Arrays.toString(res));
            return res;
        } catch (Throwable e) {
            LOGGER.e("requestCheckProcess: " + Log.getStackTraceString(e));
            return new String[0];
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    @Override
    public boolean shouldBlockRemountForUid(int uid) {
        int callingUid = Binder.getCallingUid();
        if (callingUid != 1000 && callingUid != 0) {
            enforceManagerPermission();
        }

        if (isBlockRemountEnabledLocked()) {
            return false;
        }
        return CoreConfig.hasUid(uid);
    }

    @Override
    public void packageChanged(String action, String packageName, int uid) {
        int callingUid = Binder.getCallingUid();
        if (callingUid != 1000 && callingUid != 0) {
            return;
        }

        if (action == null || packageName == null || uid == 0) {
            return;
        }

        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, UserHandleUtils.getUserId(uid), PackageManager.GET_PERMISSIONS);
            if (uidPackageInfo != null) {
                onPackageAddedLocked(uidPackageInfo);
            }
        } else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
            onPackageRemovedLocked(packageName, uid);
        }
    }

    @Override
    public boolean isIsolationEnabledForUid(int uid) {
        int callingUid = Binder.getCallingUid();
        if (callingUid != 1000 && callingUid != 0) {
            enforceApiClientPermission();
        }

        return CoreConfig.hasUid(uid);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public ParceledListSlice getInstalledApplications(int flags, int userId) {
        enforceManagerPermission();

        return new ParceledListSlice<>(SystemService.getInstalledApplicationsNoThrow(flags, userId));
    }

    @Override
    protected void dump(@NonNull FileDescriptor fileDescriptor, @NonNull PrintWriter printWriter, String[] strings) {
        try {
            enforceManagerPermission();
        } catch (Throwable e) {
            return;
        }

        super.dump(fileDescriptor, printWriter, strings);
    }

    @Override
    public void dump(@NonNull FileDescriptor fileDescriptor, String[] strings) {
        try {
            enforceManagerPermission();
        } catch (Throwable e) {
            return;
        }

        super.dump(fileDescriptor, strings);
    }

    @Override
    public void dumpAsync(@NonNull FileDescriptor fileDescriptor, String[] strings) {
        try {
            enforceManagerPermission();
        } catch (Throwable e) {
            return;
        }

        super.dumpAsync(fileDescriptor, strings);
    }

    private class FileMoveListenerHolder implements IBinder.DeathRecipient {

        IFileMoveListener listener;

        FileMoveListenerHolder(IFileMoveListener listener) {
            this.listener = listener;
        }

        @Override
        public void binderDied() {
            unregisterFileMoveListener(listener);
        }
    }

    private class FileMoveRunnable implements Runnable {

        List<UidPackageInfo> packages;
        List<ObserverInfo> observers;
        String oldFmt;
        String newFmt;

        private FileMoveRunnable(List<UidPackageInfo> packages, String oldFmt, String newFmt) {
            this.packages = packages;
            this.oldFmt = oldFmt;
            this.newFmt = newFmt;
            this.observers = new ArrayList<>();
        }

        public void prepareLocked() {
            for (UidPackageInfo info : packages) {
                final int userId = info.getUserId();

                for (ObserverInfo oi : getObserversLocked(info, userId, false)) {
                    observers.add(new ObserverInfo(oi));
                }

                // remove all observer
                for (ObserverInfo oi : observers) {
                    try {
                        removeObserver(oi);
                    } catch (Exception e) {
                        // should never happened
                        LOGGER.e("removeObserver", e);
                    }
                }
            }
        }

        public void execute(Executor executor) {
            executor.execute(this);
        }

        private void postProgress(String name, int userId, int progress, int max) {
            HandlerKt.getMainHandler().post(() -> {
                for (FileMoveListenerHolder holder : mFileMoveListeners) {
                    try {
                        holder.listener.onProgress(name, userId, progress, max);
                    } catch (RemoteException e) {
                        // dead?
                        LOGGER.w("IFileMoveListener#onProgress", e);
                    }
                }
            });
        }

        @Override
        public void run() {
            int count = packages.size();
            int process = 0;

            for (UidPackageInfo info : packages) {
                final String name = info.getName();
                final int userId = info.getUserId();
                int index = process;

                postProgress(name, userId, index, count);

                if (info.getSharedUserId() != null) {
                    PackageHelper.killUid(info.getUid());
                } else {
                    PackageHelper.killPackage(name, userId, info.getUid());
                }

                // move files
                File oldFile = FileUtils.toMedia(FileUtils.buildPathFromEmulated(userId, oldFmt, name));
                File newFile = FileUtils.toMedia(FileUtils.buildPathFromEmulated(userId, newFmt, name));
                if (oldFile.exists()) {
                    if (!newFile.getParentFile().exists()) {
                        //noinspection ResultOfMethodCallIgnored
                        newFile.getParentFile().mkdirs();
                    }
                    if (FileUtils.rename(oldFile, newFile, true)) {
                        LOGGER.v("renamed %s to %s", oldFile.toString(), newFile.toString());
                    } else {
                        LOGGER.w("failed to rename %s to %s", oldFile.toString(), newFile.toString());
                    }
                } else {
                    LOGGER.v("%s not exists, no need to rename", oldFile.toString());
                }

                process++;

                LOGGER.v("file move progress: %d:%s (%d/%d)", userId, name, process, count);

                postProgress(null, userId, index, count);
            }

            postProgress(null, -1, count, count);

            finish();
        }

        private void finish() {
            // add all observer
            for (ObserverInfo oi : observers) {
                UidPackageInfo uidPackageInfo = UidPackageInfo.get(oi.packageName, oi.userId);
                try {
                    addObserverLocked(oi, uidPackageInfo);
                } catch (Exception e) {
                    // should never happened
                    LOGGER.e("addObserver", e);
                }
            }

            for (UidPackageInfo info : packages) {
                final String name = info.getName();
                final int userId = info.getUserId();

                // rewrite config
                for (SimpleMountInfo it : new ArrayList<>(mSimpleMounts)) {
                    if (!it.enabled || !Objects.equals(name, it.sourcePackage)) {
                        continue;
                    }
                    // TODO skip if only contains direct mount
                    RedirectPackageInfo targetInfo = mPackages.get(it.targetPackage, userId);
                    if (targetInfo != null) {
                        UidPackageInfo targetUidPackageInfo = UidPackageInfo.get(it.targetPackage, userId);
                        if (targetUidPackageInfo != null) {
                            CoreConfig.update(targetInfo, targetUidPackageInfo, null);
                            LegacyUidFileWriter.scheduleWriteLocked(targetInfo, targetUidPackageInfo.getPackageInfo(), Service.this, true);
                            PackageHelper.killUid(targetUidPackageInfo.getUid());
                        }
                    }
                }
            }
            mFileMoveRunnable = null;
        }
    }
}
