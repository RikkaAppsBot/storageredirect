package moe.shizuku.redirectstorage.server.provider;

import android.content.IContentProvider;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import java.util.Objects;

import moe.shizuku.redirectstorage.provider.IRemoteCursor;
import moe.shizuku.redirectstorage.provider.IRemoteProvider;
import moe.shizuku.redirectstorage.utils.BuildUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class RemoteProviderHolder extends IRemoteProvider.Stub implements Binder.DeathRecipient {

    private static final String QUERY_ARG_SQL_SELECTION = "android:query-arg-sql-selection";
    private static final String QUERY_ARG_SQL_SELECTION_ARGS = "android:query-arg-sql-selection-args";
    private static final String QUERY_ARG_SQL_SORT_ORDER = "android:query-arg-sql-sort-order";
    public String authority;
    public int userId;
    public String token;
    public boolean removed;
    private IContentProvider provider;

    public RemoteProviderHolder(IContentProvider provider, String authority, int userId, String token) {
        this.provider = provider;
        this.authority = authority;
        this.userId = userId;
        this.token = token;
    }

    private static Bundle createSqlQueryBundle(String selection, String[] selectionArgs, String sortOrder) {
        if (selection == null && selectionArgs == null && sortOrder == null) {
            return null;
        }

        Bundle queryArgs = new Bundle();
        if (selection != null) {
            queryArgs.putString(QUERY_ARG_SQL_SELECTION, selection);
        }
        if (selectionArgs != null) {
            queryArgs.putStringArray(QUERY_ARG_SQL_SELECTION_ARGS, selectionArgs);
        }
        if (sortOrder != null) {
            queryArgs.putString(QUERY_ARG_SQL_SORT_ORDER, sortOrder);
        }
        return queryArgs;
    }

    @Override
    public void binderDied() {

    }

    @Override
    public boolean onTransact(int code, @NonNull Parcel data, Parcel reply, int flags) throws RemoteException {
        long identity = Binder.clearCallingIdentity();
        boolean res = super.onTransact(code, data, reply, flags);
        Binder.restoreCallingIdentity(identity);
        return res;
    }

    @Override
    public IRemoteCursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws RemoteException {
        Bundle queryArgs = createSqlQueryBundle(selection, selectionArgs, sortOrder);
        final RemoteCursorHolder holder;
        if (BuildUtils.atLeast30()) {
            holder = new RemoteCursorHolder(provider.query(null, null, uri, projection, queryArgs, null));
        } else if (BuildUtils.atLeast26()) {
            holder = new RemoteCursorHolder(provider.query(null, uri, projection, queryArgs, null));
        } else {
            holder = new RemoteCursorHolder(provider.query(null, uri, projection, selection, selectionArgs, sortOrder, null));
        }
        holder.asBinder().linkToDeath(() -> {
            try {
                if (!holder.isClosed())
                    holder.close();
            } catch (Throwable tr) {
                tr.printStackTrace();
            }
        }, 0);
        return holder;
    }

    @Override
    public AssetFileDescriptor openAssetFile(Uri url, String mode) {
        try {
            if (BuildUtils.atLeast30()) {
                return provider.openAssetFile(null, null, url, mode, null);
            } else {
                return provider.openAssetFile(null, url, mode, null);
            }
        } catch (Throwable tr) {
            LOGGER.w(tr, "openAssetFile(\"%s\", \"%s\")", Objects.toString(url, "(null)"), Objects.toString(mode, "(null)"));
            throw new IllegalStateException(tr.getMessage());
        }
    }
}
