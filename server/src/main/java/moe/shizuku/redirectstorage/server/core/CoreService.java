package moe.shizuku.redirectstorage.server.core;

import android.net.LocalSocketAddress;

import androidx.annotation.NonNull;

import com.google.flatbuffers.FlatBufferBuilder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collection;

import kotlin.Pair;
import moe.shizuku.redirectstorage.server.NativeHelper;

public class CoreService {

    public static final int PERM_READ = 1;
    public static final int PERM_WRITE = 1 << 1;

    public static final int MOUNT_INSTALLER = 1 << 2;

    /**
     * For DownloadsAuthority, ExternalStorageAuthority, apps with ACCESS_MTP permission.
     */
    public static final int MOUNT_EXTERNAL_ANDROID_WRITEABLE = 1 << 3;

    /**
     * For ExternalStorageService (MediaProvider).
     */
    public static final int MOUNT_PASS_THROUGH = 1 << 4;

    public static final int FLAG_ENCRYPTED = 1 << 10;

    private static final int ACTION_REQUEST_CHECK_PROCESS = 1;

    private static FlatBufferBuilder buildHeader(String token, int action) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int tokenOffset = builder.createString(token);
        RequestHeader.startRequestHeader(builder);
        RequestHeader.addAction(builder, action);
        RequestHeader.addToken(builder, tokenOffset);
        int offset = RequestHeader.endRequestHeader(builder);
        RequestHeader.finishRequestHeaderBuffer(builder, offset);
        return builder;
    }

    private static FlatBufferBuilder buildCheckProcessRequest(
            int pid, int uid, String packageName, long flags, String target, Collection<Pair<String, String>> mounts, Collection<Pair<String, String>> observers) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int pkgPos = builder.createString(packageName);
        int targetPos = builder.createString(target);
        int[] mountPos = new int[mounts.size()];
        int[] observerPos = new int[observers.size()];
        int index;

        index = 0;
        for (Pair<String, String> it : mounts) {
            int pos1 = builder.createString(it.getFirst());
            int pos2 = builder.createString(it.getSecond());
            DirPair.startDirPair(builder);
            DirPair.addSource(builder, pos1);
            DirPair.addTarget(builder, pos2);
            mountPos[index] = DirPair.endDirPair(builder);
            index++;
        }

        index = 0;
        for (Pair<String, String> it : observers) {
            int pos1 = builder.createString(it.getFirst());
            int pos2 = builder.createString(it.getSecond());
            DirPair.startDirPair(builder);
            DirPair.addSource(builder, pos1);
            DirPair.addTarget(builder, pos2);
            observerPos[index] = DirPair.endDirPair(builder);
            index++;
        }

        int mountsPos = CheckProcessRequest.createMountsVector(builder, mountPos);
        int observersPos = CheckProcessRequest.createObserversVector(builder, observerPos);
        CheckProcessRequest.startCheckProcessRequest(builder);
        CheckProcessRequest.addPid(builder, pid);
        CheckProcessRequest.addUid(builder, uid);
        CheckProcessRequest.addPkg(builder, pkgPos);
        CheckProcessRequest.addFlags(builder, flags);
        CheckProcessRequest.addTarget(builder, targetPos);
        CheckProcessRequest.addMounts(builder, mountsPos);
        CheckProcessRequest.addObservers(builder, observersPos);
        int offset = CheckProcessRequest.endCheckProcessRequest(builder);
        CheckProcessRequest.finishCheckProcessRequestBuffer(builder, offset);
        return builder;
    }

    public static int requestCheckProcess(
            @NonNull String token, int pid, int uid, @NonNull String packageName, long mount,
            @NonNull String target, @NonNull Collection<Pair<String, String>> mounts, Collection<Pair<String, String>> observers) {

        FlatBufferBuilder header = buildHeader(token, 1);
        FlatBufferBuilder body = buildCheckProcessRequest(pid, uid, packageName, mount, target, mounts, observers);
        byte[] headerBytes = header.sizedByteArray();
        byte[] bodyBytes = body.sizedByteArray();

        ByteBuffer data = ByteBuffer.allocateDirect(8 + headerBytes.length + bodyBytes.length);

        data.order(ByteOrder.nativeOrder());
        data.putInt(headerBytes.length);
        data.put(headerBytes);
        data.putInt(bodyBytes.length);
        data.put(bodyBytes);

        ByteBuffer reply = NativeHelper.requestHandleProcess(data);
        if (reply == null) return -1;

        reply.order(ByteOrder.nativeOrder());
        CheckProcessReply checkProcessReply = CheckProcessReply.getRootAsCheckProcessReply(reply);
        int res = checkProcessReply.license();
        NativeHelper.freeDirectByteBuffer(reply);
        return res;
    }
}
