package moe.shizuku.redirectstorage.server.util;

import java.util.Collection;
import java.util.Objects;

public class ArrayUtils {

    public static boolean contains(String[] array, String s) {
        if (array == null)
            return false;

        for (String item : array) {
            if (Objects.equals(s, item)) {
                return true;
            }
        }
        return false;
    }

    public static int[] toArray(Collection<Integer> list) {
        if (list == null) {
            return null;
        }

        int[] res = new int[list.size()];
        int index = 0;
        for (int it : list) {
            res[index] = it;
            index++;
        }
        return res;
    }
}
