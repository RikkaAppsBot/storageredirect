package moe.shizuku.redirectstorage.server.util;

import android.text.TextUtils;

import java.io.File;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.server.Service;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class ObserverInfoHelper {

    /**
     * Get path for observer, such as /data/media/0/Android/data/com.example.app/cache/sdcard/pictures
     *
     * @return path for observer
     */
    public static String getSourcePath(ObserverInfo oi) {
        return FileUtils.buildPathFromMedia(oi.userId, Service.getInstance().getRedirectTargetForPackage(oi.packageName, oi.userId, false) + "/%s", oi.source).getAbsolutePath();
    }

    /**
     * Get path for user, such as /storage/emulated/0/Android/data/com.example.app/cache/sdcard/pictures
     *
     * @return path for user
     */
    public static String getSourcePathForUser(ObserverInfo oi) {
        return FileUtils.buildPathFromEmulated(oi.userId, Service.getInstance().getRedirectTargetForPackage(oi.packageName, oi.userId, false) + "/%s", oi.source).getAbsolutePath();
    }

    /**
     * Get path for user, such as /storage/emulated/0/12345
     *
     * @return path for user
     */
    public static String getSourcePathForUserFromApp(ObserverInfo oi) {
        return FileUtils.buildPathFromEmulated(oi.userId, oi.source).getAbsolutePath();
    }

    /**
     * Get path for observer, such as /data/media/0/Android/data/com.example.app
     *
     * @return path for observer
     */
    public static String getDataPathForUser(ObserverInfo oi) {
        return FileUtils.buildPathFromEmulated(oi.userId, "Android/data/%s", oi.packageName).getAbsolutePath();
    }

    /**
     * Get path for observer, such as /storage/emulated/0/Android/data/com.example.app
     *
     * @return path for observer
     */
    public static String getDataPath(ObserverInfo oi) {
        return FileUtils.buildPathFromMedia(oi.userId, "Android/data/%s", oi.packageName).getAbsolutePath();
    }

    /**
     * Get path for observer, such as /data/media/0/Pictures/ExampleApp
     *
     * @return path for observer
     */
    public static String getTargetPath(ObserverInfo oi) {
        return FileUtils.buildPathFromMedia(oi.userId, oi.target).getAbsolutePath();
    }

    /**
     * Get path for user, such as /storage/emulated/0/Pictures/ExampleApp
     *
     * @return path for user
     */
    public static String getTargetPathForUser(ObserverInfo oi) {
        return FileUtils.buildPathFromEmulated(oi.userId, oi.target).getAbsolutePath();
    }

    public static String getIgnoreReasonString(int reason) {
        switch (reason) {
            case 1:
                return "regex check";
            case 2:
                return "allow child check";
            case 3:
                return "allow tmp check";
        }
        return "unknown " + reason;
    }

    public static int shouldIgnore(ObserverInfo oi, String path, boolean sourceToTarget) {
        String filename = new File(path).getName().toLowerCase();
        path = path.toLowerCase();

        LOGGER.v("filename=%s, path=%s", filename, path);

        // check mask
        if (!TextUtils.isEmpty(oi.mask)) {
            if (oi.pattern == null) {
                try {
                    oi.pattern = Pattern.compile(oi.mask);
                } catch (PatternSyntaxException e) {
                    LOGGER.w(e, "Invalid regular expression %s", oi.mask);
                }
            }

            if (oi.pattern != null && !oi.pattern.matcher(filename).find()) {
                return 1;
            }
        }

        // check allowChild
        if (path.toLowerCase().replaceFirst(sourceToTarget ? oi.source : oi.target + "/", "").contains("/") && !oi.allowChild) {
            return 2;
        }

        // check allowTemp
        String extension = "";
        int i = filename.lastIndexOf('.');
        if (i > 0) {
            extension = filename.substring(i + 1);
        }
        boolean tmp = !TextUtils.isEmpty(extension) && (extension.endsWith("tmp") || extension.endsWith("temp"));
        if (!oi.allowTemp && tmp)
            return 3;

        return 0;
    }
}
