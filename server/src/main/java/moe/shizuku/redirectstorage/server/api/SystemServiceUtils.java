package moe.shizuku.redirectstorage.server.api;

import android.os.IBinder;
import android.os.ServiceManager;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class SystemServiceUtils {

    public static IBinder waitForSystemService(String name) {
        IBinder binder;
        do {
            binder = ServiceManager.getService(name);
            if (binder != null && binder.pingBinder()) {
                return binder;
            }

            LOGGER.i("Service " + name + " not found, wait 1s...");
            try {
                //noinspection BusyWait
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        } while (true);
    }
}
