package moe.shizuku.redirectstorage.server.ktx

import android.os.Build
import android.os.SystemProperties

val isFuseEnabled by lazy(LazyThreadSafetyMode.NONE) {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && SystemProperties.getBoolean("persist.sys.fuse", true)
}