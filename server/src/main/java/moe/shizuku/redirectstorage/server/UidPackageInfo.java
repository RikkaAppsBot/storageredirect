package moe.shizuku.redirectstorage.server;

import android.content.pm.PackageInfo;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.util.HiddenValues;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.RedirectPackageInfo.SHARED_USER_PREFIX;
import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class UidPackageInfo {

    public static UidPackageInfo get(String name, int userId) {
        return get(name, userId, 0);
    }

    public static UidPackageInfo get(String name, int userId, int pmFlags) {
        if (RedirectPackageInfo.isSharedUserId(name)) {
            String sharedUserId = RedirectPackageInfo.getRawSharedUserId(name);
            int uid = PackageManagerApi.getUidForSharedUser(sharedUserId, userId);
            if (uid == -1) {
                return null;
            }
            List<String> packages = PackageManagerApi.getPackagesForUid(uid);
            String packageName = packages.isEmpty() ? null : packages.get(0);
            PackageInfo pi = PackageManagerApi.getPackageInfo(packageName, pmFlags, userId);
            boolean hidden = false;
            if (pi == null) {
                pi = PackageManagerApi.getPackageInfo(packageName, pmFlags | HiddenValues.MATCH_UNINSTALLED_PACKAGES, userId);
                if (pi == null) {
                    // not installed in any user
                    return null;
                }
                if (!SystemService.getApplicationHiddenSettingAsUserNoThrow(pi.packageName, userId)) {
                    // installed in other users only
                    return null;
                }
                hidden = true;
            }
            return new UidPackageInfo(uid, pi, hidden);
        } else {
            PackageInfo pi = PackageManagerApi.getPackageInfo(name, pmFlags, userId);
            boolean hidden = false;
            if (pi == null) {
                pi = PackageManagerApi.getPackageInfo(name, pmFlags | HiddenValues.MATCH_UNINSTALLED_PACKAGES, userId);
                if (pi == null || pi.applicationInfo == null) {
                    // not installed in any user
                    return null;
                }
                if (!SystemService.getApplicationHiddenSettingAsUserNoThrow(pi.packageName, userId)) {
                    // installed in other users only
                    return null;
                }
                hidden = true;
            }
            return get(pi, hidden);
        }
    }

    public static UidPackageInfo get(PackageInfo pi, boolean hidden) {
        if (pi != null && pi.applicationInfo != null) {
            return new UidPackageInfo(pi.applicationInfo.uid, pi, hidden);
        } else {
            return null;
        }
    }

    public static List<UidPackageInfo> list(int userId) {
        return list(userId, 0);
    }

    public static List<UidPackageInfo> list(int userId, int pmFlags) {
        final List<PackageInfo> packages = SystemService.getInstalledPackagesNoThrow(pmFlags, userId);
        final List<PackageInfo> hiddenPackages = new ArrayList<>();
        final List<UidPackageInfo> res = new ArrayList<>();
        for (PackageInfo pi : packages) {
            if (pi.applicationInfo == null) {
                LOGGER.w("package %s does not have application", pi.packageName);
                continue;
            }
            res.add(get(pi, false));
        }
        for (PackageInfo pi : SystemService.getInstalledPackagesNoThrow(pmFlags | HiddenValues.MATCH_UNINSTALLED_PACKAGES, userId)) {
            boolean skip = false;
            for (PackageInfo pi1 : packages) {
                if (Objects.equals(pi.packageName, pi1.packageName)) {
                    skip = true;
                    break;
                }
            }
            if (skip) {
                continue;
            }

            if (pi.applicationInfo == null) {
                LOGGER.w("package %s does not have application", pi.packageName);
                continue;
            }
            if (SystemService.getApplicationHiddenSettingAsUserNoThrow(pi.packageName, userId)) {
                res.add(get(pi, true));
            }
        }

        return res;
    }

    private int uid;
    private PackageInfo packageInfo;
    private boolean hidden;

    private UidPackageInfo(int uid, PackageInfo packageInfo, boolean hidden) {
        this.uid = uid;
        this.packageInfo = packageInfo;
        this.hidden = hidden;
    }

    public String getName() {
        if (getSharedUserId() != null) {
            return SHARED_USER_PREFIX + getSharedUserId();
        } else {
            return getPackageName();
        }
    }

    public int getUserId() {
        return UserHandleUtils.getUserId(uid);
    }

    public int getUid() {
        return uid;
    }

    public String getPackageName() {
        return getPackageInfo().packageName;
    }

    public String getSharedUserId() {
        return getPackageInfo().sharedUserId;
    }

    public PackageInfo getPackageInfo() {
        return packageInfo;
    }

    public boolean isHidden() {
        return hidden;
    }

    @NonNull
    @Override
    public String toString() {
        return "UidPackageInfo{" +
                "uid=" + uid +
                ", userId=" + getUserId() +
                ", packageName=" + getPackageName() +
                ", hidden=" + isHidden() +
                ", sharedUserId=" + getSharedUserId() +
                '}';
    }
}
