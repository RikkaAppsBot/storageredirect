package moe.shizuku.redirectstorage.server.legacy;

public class FilesLegacy {

    private static final String FILE_ROOT = "/data/misc/storage_redirect";
    public static final String FILE_MONITOR_DATABASE = FILE_ROOT + "/file_monitor.db";

    private static final String FILE_CONFIG_ROOT = FILE_ROOT + "/config";
    public static final String DISABLE_FILE_MONITOR = FILE_CONFIG_ROOT + "/file_monitor/disable";
    public static final String DISABLE_FIX_RENAME = FILE_CONFIG_ROOT + "/fix_rename/disable";
    public static final String DISABLE_FIX_INTERACTION_UID = FILE_CONFIG_ROOT + "/fix_interaction/disabled";
    public static final String DISABLE_FIX_INTERACTION = FILE_CONFIG_ROOT + "/fix_interaction/disable";
    public static final String DISABLE_BLOCK_REMOUNT = FILE_CONFIG_ROOT + "/block_remount/disable";
}
