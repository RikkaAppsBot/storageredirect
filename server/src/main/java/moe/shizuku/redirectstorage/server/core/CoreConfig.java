package moe.shizuku.redirectstorage.server.core;

import android.util.ArrayMap;
import android.util.ArraySet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kotlin.Pair;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.server.NativeHelper;
import moe.shizuku.redirectstorage.server.Service;
import moe.shizuku.redirectstorage.server.UidPackageInfo;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.ktx.BuildKt;
import moe.shizuku.redirectstorage.server.util.PackageInfoUtils;
import moe.shizuku.redirectstorage.utils.BuildUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class CoreConfig {

    private CoreConfig() {
    }

    public static class UidConfig {

        public final int flags;
        public final String targetPath;
        public final String targetName;
        public final Set<Pair<String, String>> mounts;
        public final Set<Pair<String, String>> observers;
        public final List<String> sharedPackages;
        public final boolean isSystem;

        private UidConfig(
                int flags, String targetPath, String targetName, Set<Pair<String, String>> mounts, Set<Pair<String, String>> observers,
                List<String> sharedPackages, boolean isSystem) {

            this.flags = flags;
            this.targetPath = targetPath;
            this.targetName = targetName;
            this.mounts = mounts;
            this.observers = observers;
            this.sharedPackages = sharedPackages;
            this.isSystem = isSystem;
        }
    }

    private static final Set<Integer> ISOLATED_UID = Collections.synchronizedSet(new ArraySet<>());
    private static final Map<Integer, UidConfig> UID_CONFIGS = Collections.synchronizedMap(new ArrayMap<>());

    private static int getFlags(int uid, @Nullable String packageName, @NonNull List<String> packages, int storagePermission) {
        int mount = 0;
        if ((storagePermission & RedirectPackageInfo.PERMISSION_WRITE) != 0) {
            mount |= CoreService.PERM_WRITE;
        }
        if ((storagePermission & RedirectPackageInfo.PERMISSION_READ) != 0) {
            mount |= CoreService.PERM_READ;
        }

        if (!BuildKt.isFuseEnabled()) return mount;

        if (PackageManagerApi.isExternalStorageService(uid)) {
            mount |= CoreService.MOUNT_PASS_THROUGH;
        } else if (PackageManagerApi.isDownloadsAuthorityAppId(uid) || PackageManagerApi.isExternalStorageAuthorityAppId(uid)) {
            mount |= CoreService.MOUNT_EXTERNAL_ANDROID_WRITEABLE;
        } else {
            /*final boolean hasMtp = SystemService.checkPermission()
            if (mIsFuseEnabled && hasMtp) {
                ApplicationInfo ai = mIPackageManager.getApplicationInfo(packageName,
                        0, UserHandle.getUserId(uid));
                if (ai != null && ai.isSignedWithPlatformKey()) {
                    // Platform processes hosting the MTP server should be able to write in Android/
                    return Zygote.MOUNT_EXTERNAL_ANDROID_WRITABLE;
                }
            }*/
        }

        return mount;
    }

    private static UidConfig collectUidConfig(RedirectPackageInfo info, UidPackageInfo uidPackageInfo, @Nullable List<String> sharedPackages) {
        Service service = Service.getInstance();

        int uid = uidPackageInfo.getUid();
        //String packageName = uidPackageInfo.getPackageName();
        String targetName = uidPackageInfo.getName();

        if (sharedPackages == null) {
            sharedPackages = PackageManagerApi.getPackagesForUid(uid);
        }

        // Apps has no permission to access any folder in Android/data except own
        if (BuildUtils.isFuse() && info.isSharedUserId()) {
            if (!sharedPackages.isEmpty()) {
                targetName = sharedPackages.get(0);
            }
        }

        int flags = getFlags(uid, null /*packageName*/, sharedPackages, info.storagePermission);

        String target = service.getRedirectTargetForPackageLocked(info.redirectTarget, targetName);

        Set<Pair<String, String>> mountPairs = new ArraySet<>();
        Set<Pair<String, String>> observerPairs = new ArraySet<>();

        List<String> mountDirs = new ArrayList<>(service.getMountDirsForPackageLocked(uidPackageInfo, true));
        Collections.sort(mountDirs);
        for (String it : mountDirs) {
            mountPairs.add(new Pair<>(it, it));
        }

        if (!BuildUtils.isFuse() || NativeHelper.isSdcardfsUsed()) {
            List<SimpleMountInfo> simpleMounts = new ArrayList<>(service.getSimpleMountsLocked(uidPackageInfo, uidPackageInfo.getUserId(), false));

            for (SimpleMountInfo smi : simpleMounts) {
                UidPackageInfo sourceUidPackageInfo = UidPackageInfo.get(smi.sourcePackage, smi.userId);
                if (sourceUidPackageInfo == null) {
                    continue;
                }
                RedirectPackageInfo sourceInfo = service.getRedirectPackageInfoLocked(sourceUidPackageInfo, false, false, true, false, false);
                if (sourceInfo == null) {
                    continue;
                }
                Set<String> sourceSharedPackages = new HashSet<>(PackageManagerApi.getPackagesForUid(sourceUidPackageInfo.getUid()));

                for (String it : smi.getDirectPaths(sourceSharedPackages)) {
                    mountPairs.add(new Pair<>(it, it));
                }
                String sourceTargetFormat = service.getRedirectTargetForPackageLocked(sourceInfo.redirectTarget, smi.sourcePackage);
                for (String it : smi.getRedirectedPaths(sourceSharedPackages)) {
                    mountPairs.add(new Pair<>(sourceTargetFormat + File.separator + it, it));
                }
            }
        }

        List<ObserverInfo> observers = service.getObserversLocked(uidPackageInfo, uidPackageInfo.getUserId(), false);

        for (ObserverInfo oi : new ArrayList<>(observers)) {
            observerPairs.add(new Pair<>(oi.source, oi.target));
        }

        boolean isSystem = PackageInfoUtils.isSystem(uidPackageInfo.getPackageInfo());

        LOGGER.v("collected uid config for %s %s (shared packages size %d)", uid, targetName, sharedPackages.size());
        return new UidConfig(flags, target, targetName, mountPairs, observerPairs, sharedPackages, isSystem);
    }

    public static void update(@NonNull RedirectPackageInfo info, @NonNull UidPackageInfo uidPackageInfo, @Nullable List<String> packages) {
        if (info == null) {
            LOGGER.w(new Throwable(), "info is null");
            return;
        }

        if (uidPackageInfo == null) {
            LOGGER.w(new Throwable(), "uid info is null");
            return;
        }

        int uid = uidPackageInfo.getUid();

        LOGGER.v("start to collect uid config for %s %s", uid, uidPackageInfo.getName());
        UidConfig config = collectUidConfig(info, uidPackageInfo, packages);

        UID_CONFIGS.put(uid, config);
    }

    public static UidConfig get(int uid) {
        return UID_CONFIGS.get(uid);
    }

    public static boolean hasUid(int uid) {
        return ISOLATED_UID.contains(uid);
    }

    public static void addUid(int uid) {
        ISOLATED_UID.add(uid);
    }

    public static void removeUid(int uid) {
        ISOLATED_UID.remove(uid);
    }

    public static Set<Integer> getIsolatedUids() {
        return ISOLATED_UID;
    }
}
