package moe.shizuku.redirectstorage.server;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.collection.MountDirsTemplateList;
import moe.shizuku.redirectstorage.collection.ObserverInfoList;
import moe.shizuku.redirectstorage.collection.RedirectPackageInfoList;
import moe.shizuku.redirectstorage.collection.SimpleMountInfoList;

public class Config {

    public static final int LATEST_VERSION = 18;

    @SerializedName("version")
    public int version = LATEST_VERSION;

    @SerializedName(value = "media_path", alternate = "mediaPath")
    public String mediaPath;

    @SerializedName("packages")
    public RedirectPackageInfoList packages;

    @SerializedName("observers")
    public ObserverInfoList observers;

    @SerializedName("simple_mounts")
    public SimpleMountInfoList simpleMounts;

    @SerializedName("mount_dirs_templates")
    public MountDirsTemplateList mountDirsTemplates;

    @SerializedName("default_target")
    public String defaultTarget;

    @SerializedName("kill_media_storage_on_start")
    public boolean killMediaStorageOnStart;

    @SerializedName("enhanced_mode")
    public ModuleConfig moduleConfig;

    Config() {
        this(
                new RedirectPackageInfoList(),
                new ObserverInfoList(),
                new SimpleMountInfoList(),
                Constants.REDIRECT_TARGET_DATA,
                null,
                new MountDirsTemplateList(),
                true,
                new ModuleConfig());
    }

    Config(RedirectPackageInfoList packages,
           ObserverInfoList observers,
           SimpleMountInfoList simpleMounts,
           String defaultTarget,
           String mediaPath,
           MountDirsTemplateList mountDirsTemplates,
           boolean killMediaStorageOnStart,
           ModuleConfig moduleConfig) {
        this.packages = packages;
        this.observers = observers;
        this.simpleMounts = simpleMounts;
        this.defaultTarget = defaultTarget;
        this.mediaPath = mediaPath;
        this.mountDirsTemplates = mountDirsTemplates;
        this.killMediaStorageOnStart = killMediaStorageOnStart;
        this.moduleConfig = moduleConfig;
    }

    Config(Config other) {
        this.packages = other.packages != null ? new RedirectPackageInfoList(other.packages) : null;
        this.observers = other.observers != null ? new ObserverInfoList(other.observers) : null;
        this.simpleMounts = other.simpleMounts != null ? new SimpleMountInfoList(other.simpleMounts) : null;
        this.defaultTarget = other.defaultTarget;
        this.mediaPath = other.mediaPath;
        this.mountDirsTemplates = other.mountDirsTemplates != null ? new MountDirsTemplateList(other.mountDirsTemplates) : null;
        this.killMediaStorageOnStart = other.killMediaStorageOnStart;
        this.moduleConfig = other.moduleConfig;
        this.version = other.version;
    }

    @NonNull
    @Override
    public String toString() {
        return "Configuration{" +
                "version=" + version +
                ", mediaPath='" + mediaPath + '\'' +
                ", packages='" + (packages == null ? "null" : (packages.size() + " items")) + '\'' +
                ", observers='" + (observers == null ? "null" : (observers.size() + " items")) + '\'' +
                ", simpleMounts='" + (simpleMounts == null ? "null" : (simpleMounts.size() + " items")) + '\'' +
                ", mountDirsTemplates='" + (mountDirsTemplates == null ? "null" : (mountDirsTemplates.size() + " items")) + '\'' +
                ", defaultTarget='" + defaultTarget + '\'' +
                ", killMediaStorageOnStart=" + killMediaStorageOnStart +
                '}';
    }

    public static class ModuleConfig {

        @SerializedName(value = "package_override", alternate = "packages")
        public List<Package> packages;

        @SerializedName("disable_export_notification")
        public boolean disableExportNotification;

        @SerializedName("file_monitor")
        public boolean enableFileMonitor;

        @SerializedName("rename_fix")
        public boolean enableRenameFix;

        @SerializedName("app_interaction_fix")
        public boolean enableAppInteractionFix;

        @SerializedName("new_app_notification")
        public boolean enableNewAppNotification;

        public ModuleConfig() {
            packages = new ArrayList<>();
        }

        public static class Package {

            @SerializedName("name")
            public String name;

            @SerializedName("user_id")
            public int userId;

            @SerializedName(value = "app_interaction_fix", alternate = "fix_interact_enabled")
            boolean enableAppInteractionFix;

            public Package(String name, int userId) {
                this.name = name;
                this.userId = userId;
            }
        }
    }
}
