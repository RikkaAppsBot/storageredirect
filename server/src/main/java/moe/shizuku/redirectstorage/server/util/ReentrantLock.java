package moe.shizuku.redirectstorage.server.util;

import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

public class ReentrantLock extends java.util.concurrent.locks.ReentrantLock {

    public ReentrantLock() {
        super();
    }

    public ReentrantLock(boolean fair) {
        super(fair);
    }

    private void debug(String name) {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if (stack != null && stack.length > 4) {
            Logger.LOGGER.v("%s from %s", name, stack[4].toString());
        }
    }

    @Override
    public void lock() {
        //debug("lock");
        super.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        super.lockInterruptibly();
    }

    @Override
    public boolean tryLock() {
        boolean res = super.tryLock();
        //debug("tryLock (" + res + ")");
        return res;
    }

    @Override
    public boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
        boolean res = super.tryLock(timeout, unit);
        //debug("tryLock (" + res + ")");
        return res;
    }

    public boolean tryLock0() {
        boolean res = false;
        try {
            res = super.tryLock(0, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //debug("tryLock (" + res + ")");
        return res;
    }

    @Override
    public void unlock() {
        //debug("unlock");
        super.unlock();
    }

    @Override
    public Condition newCondition() {
        return super.newCondition();
    }

    @Override
    public int getHoldCount() {
        return super.getHoldCount();
    }

    @Override
    public boolean isHeldByCurrentThread() {
        return super.isHeldByCurrentThread();
    }

    @Override
    public boolean isLocked() {
        return super.isLocked();
    }

    @Override
    protected Thread getOwner() {
        return super.getOwner();
    }

    @Override
    protected Collection<Thread> getQueuedThreads() {
        return super.getQueuedThreads();
    }

    @Override
    public boolean hasWaiters(Condition condition) {
        return super.hasWaiters(condition);
    }

    @Override
    public int getWaitQueueLength(Condition condition) {
        return super.getWaitQueueLength(condition);
    }

    @Override
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        return super.getWaitingThreads(condition);
    }
}
