package moe.shizuku.redirectstorage.server.enhance

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import moe.shizuku.redirectstorage.model.FileMonitorRecord
import moe.shizuku.redirectstorage.server.ConfigProvider
import moe.shizuku.redirectstorage.server.legacy.FilesLegacy
import moe.shizuku.redirectstorage.server.util.Logger.LOGGER
import java.io.File

object FileMonitor {

    private val DATABASE_PATH = File(ConfigProvider.getConfigDir(), "/file_monitor.db").path
    private const val TABLE = "records"

    private fun createDatabase(): SQLiteDatabase? {
        val file = File(DATABASE_PATH)
        val legacyFile = File(FilesLegacy.FILE_MONITOR_DATABASE)
        val legacyExists = legacyFile.exists()
        if (!file.exists() && legacyExists) {
            try {
                legacyFile.copyTo(file, true, 8192)
                legacyFile.delete()
            } catch (e: Throwable) {
                LOGGER.e(e, "move legacy database")
            }
        }
        val db = try {
            SQLiteDatabase.openDatabase(DATABASE_PATH, null,
                    SQLiteDatabase.OPEN_READWRITE or SQLiteDatabase.CREATE_IF_NECESSARY or SQLiteDatabase.NO_LOCALIZED_COLLATORS).apply {
                execSQL("CREATE TABLE IF NOT EXISTS $TABLE(id INTEGER PRIMARY KEY AUTOINCREMENT, user INTEGER, package TEXT, path TEXT, func TEXT, timestamp BIGINT);")
            }
        } catch (e: Throwable) {
            LOGGER.e(e, "create database")
            return null
        }
        if (legacyExists) {
            db.beginTransaction()
            try {
                db.execSQL("CREATE TEMPORARY TABLE history_temp(user INTEGER, package TEXT, path TEXT, timestamp BIGINT);\n")
                db.execSQL("INSERT INTO history_temp(user, package, path, timestamp) SELECT user, package, path, timestamp FROM history;\n")
                db.execSQL("DROP TABLE history;\n")
                db.execSQL("INSERT INTO $TABLE(user, package, path, timestamp) SELECT user, package, path, timestamp FROM history_temp;\n")
                db.execSQL("DROP TABLE history_temp;")
                db.setTransactionSuccessful()
            } catch (e: Throwable) {
                LOGGER.e(e, "migrate database")
            }
            db.endTransaction()
            LOGGER.i("migrate database")
        }
        return db
    }

    private var databaseInternal: SQLiteDatabase? = null

    private val database: SQLiteDatabase?
        get() {
            if (databaseInternal != null) return databaseInternal
            databaseInternal = createDatabase()
            return databaseInternal
        }

    @JvmStatic
    fun query(selection: String?, selectionArgs: Array<String?>?, orderBy: String?, limit: String?): List<FileMonitorRecord> {
        return database?.query(TABLE, null, selection, selectionArgs, null, null, orderBy, limit)?.use { cursor ->
            val res = ArrayList<FileMonitorRecord>(cursor.count)
            val cursorIndexOfId = cursor.getColumnIndexOrThrow("id")
            val cursorIndexOfUser = cursor.getColumnIndexOrThrow("user")
            val cursorIndexOfPackage = cursor.getColumnIndexOrThrow("package")
            val cursorIndexOfPath = cursor.getColumnIndexOrThrow("path")
            val cursorIndexOfFunc = cursor.getColumnIndexOrThrow("func")
            val cursorIndexOfTimestamp = cursor.getColumnIndexOrThrow("timestamp")
            if (cursor.moveToFirst()) {
                do {
                    val entity = FileMonitorRecord()
                    entity.id = cursor.getLong(cursorIndexOfId)
                    entity.user = cursor.getInt(cursorIndexOfUser)
                    entity.packageName = cursor.getString(cursorIndexOfPackage)
                    entity.path = cursor.getString(cursorIndexOfPath)
                    entity.func = cursor.getString(cursorIndexOfFunc)
                    entity.timestamp = cursor.getLong(cursorIndexOfTimestamp)
                    res.add(entity)
                } while (cursor.moveToNext())
            }
            res
        } ?: emptyList()
    }

    @JvmStatic
    fun delete(whereClause: String?, whereArgs: Array<String>?): Int {
        return database!!.delete(TABLE, whereClause, whereArgs)
    }

    private fun deleteOutdatedNoThrow() {
        try {
            delete("timestamp < ?", arrayOf((System.currentTimeMillis() / 1000 - 60 * 60 * 24 * 7).toString()))
        } catch (e: Throwable) {
            LOGGER.w(e, "delete")
        }
    }

    @JvmStatic
    fun insert(packageName: String?, user: Int, path: String?, func: String?): Long {
        deleteOutdatedNoThrow()

        val values = ContentValues()
        values.put("user", user)
        values.put("package", packageName)
        values.put("path", path)
        values.put("func", func)
        values.put("timestamp", System.currentTimeMillis() / 1000)
        return database!!.insert(TABLE, null, values)
    }
}