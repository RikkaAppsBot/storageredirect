package moe.shizuku.redirectstorage.server.util;

import android.Manifest;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import hidden.HiddenApiBridge;

public class PackageInfoUtils {

    public static boolean containsReadStoragePermission(PackageInfo pi) {
        return containsReadStoragePermission(pi.requestedPermissions);
    }

    public static boolean containsReadStoragePermission(String[] requestedPermissions) {
        if (requestedPermissions == null)
            return false;

        for (String permission : requestedPermissions) {
            if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsWriteStoragePermission(PackageInfo pi) {
        return containsWriteStoragePermission(pi.requestedPermissions);
    }

    public static boolean containsWriteStoragePermission(String[] requestedPermissions) {
        if (requestedPermissions == null)
            return false;

        for (String permission : requestedPermissions) {
            if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isOverlay(PackageInfo pi) {
        try {
            return HiddenApiBridge.PackageInfo_overlayTarget(pi) != null;
        } catch (Throwable tr) {
            return false;
        }
    }

    public static boolean isSystem(PackageInfo pi) {
        return pi != null && pi.applicationInfo != null && (pi.applicationInfo.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0;
    }
}
