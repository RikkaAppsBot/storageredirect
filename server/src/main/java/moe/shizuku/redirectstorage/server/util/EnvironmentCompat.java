package moe.shizuku.redirectstorage.server.util;

import android.text.TextUtils;

import java.io.File;

import moe.shizuku.redirectstorage.server.NativeHelper;

public class EnvironmentCompat {

    private static File media;

    public static void setCachedMediaDirectory(File media) {
        EnvironmentCompat.media = media;
    }

    public static File getMediaDirectory() {
        if (media != null) {
            return media;
        }

        String path = NativeHelper.getMediaPath();
        if (TextUtils.isEmpty(path)) {
            return new File("/data/media");
        }

        media = new File(path);
        return media;
    }
}
