package moe.shizuku.redirectstorage.server.util;

import android.content.Intent;
import android.net.Uri;

import java.io.File;

import moe.shizuku.redirectstorage.server.Service;
import moe.shizuku.redirectstorage.server.api.SystemService;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class MediaStorage {

    private static final String MEDIA_STORAGE_PACKAGE = "com.android.providers.media";

    public static void broadcastMediaScan(File file, int userId) {
        try {
            SystemService.broadcastIntent(
                    new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                            .setPackage(MEDIA_STORAGE_PACKAGE)
                            .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES | Intent.FLAG_RECEIVER_FOREGROUND)
                            .setData(Uri.fromFile(file))
                    , userId);

            LOGGER.i("Broadcast media scan " + file.getAbsolutePath());
        } catch (Throwable tr) {
            LOGGER.e("Can't broadcast media scan.", tr);
        }
    }

    private static void kill(int userId) {
        LOGGER.i("kill media storage (%d)", userId);
        Service.getInstance().killPackage(MEDIA_STORAGE_PACKAGE, userId);
    }

    public static void killAll() {
        for (int userId : SystemService.getUserIdListNoThrow()) {
            kill(userId);
        }
    }
}
