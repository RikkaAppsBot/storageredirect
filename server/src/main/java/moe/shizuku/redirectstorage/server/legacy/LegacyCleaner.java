package moe.shizuku.redirectstorage.server.legacy;

import android.Manifest;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.system.ErrnoException;
import android.system.Os;

import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.util.ArrayUtils;
import moe.shizuku.redirectstorage.server.util.HiddenValues;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class LegacyCleaner {

    public static void main() {
        try {
            Os.setuid(1000);
        } catch (ErrnoException e) {
            LOGGER.wtf("setuid", e);
        }

        if (Os.getuid() != 1000) {
            LOGGER.wtf("can't setuid 1000, exiting...");
            System.exit(1);
            return;
        }

        LOGGER.i("starting legacy cleaner...");
        removePermissionFixForUserPackages(true, false);
        System.exit(0);
    }

    private interface PackageConsumer {
        void accept(PackageInfo pi, Integer user);
    }

    private static void foreachPackages(PackageConsumer consumer) {
        int pmFlags = PackageManager.GET_PERMISSIONS;

        for (int user : SystemService.getUserIdListNoThrow()) {
            for (PackageInfo pi : SystemService.getInstalledPackagesNoThrow(pmFlags, user)) {
                consumer.accept(pi, user);
            }
        }
    }

    public static void removePermissionFixForUserPackages(boolean removeSystemFixed, boolean removePolicyFixed) {
        if (HiddenValues.FLAG_PERMISSION_SYSTEM_FIXED == 0
                || HiddenValues.FLAG_PERMISSION_POLICY_FIXED == 0) {
            return;
        }

        int flags = (removeSystemFixed ? HiddenValues.FLAG_PERMISSION_SYSTEM_FIXED : 0) |
                (removePolicyFixed ? HiddenValues.FLAG_PERMISSION_POLICY_FIXED : 0);
        foreachPackages((pi, user) -> {
            try {
                if ((pi.applicationInfo.flags & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0) {
                    return;
                }
                int uid = pi.applicationInfo.uid;
                int appId = uid % 100000;
                boolean isRegularApp = appId >= 10000 && appId <= 19999;
                if (!isRegularApp) {
                    return;
                }

                removePermissionFix(pi, user, Manifest.permission.READ_EXTERNAL_STORAGE, flags);
                removePermissionFix(pi, user, Manifest.permission.WRITE_EXTERNAL_STORAGE, flags);
            } catch (Throwable tr) {
                LOGGER.e(tr, "removePermissionFix %d:%s", user, pi.packageName);
            }
        });
    }

    private static void removePermissionFix(PackageInfo pi, int userId, String permission, int flags) throws RemoteException {
        if (!ArrayUtils.contains(pi.requestedPermissions, permission)) {
            return;
        }

        String packageName = pi.packageName;

        int permFlags = SystemService.getPermissionFlags(permission, packageName, userId);
        if ((permFlags & flags) != 0) {
            LOGGER.v("remove fix: package=%s, user=%d, perm=%s", packageName, userId, permission);
            SystemService.updatePermissionFlags(permission, packageName, flags, 0, userId);
            LOGGER.v("remove fix: result=%s", Boolean.toString((SystemService.getPermissionFlags(permission, packageName, userId) & flags) == 0));
        }
    }
}
