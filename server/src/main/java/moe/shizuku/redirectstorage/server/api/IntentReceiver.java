package moe.shizuku.redirectstorage.server.api;

import android.content.IIntentReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import moe.shizuku.redirectstorage.Constants;

public class IntentReceiver extends IIntentReceiver.Stub {

    @Override
    public void performReceive(Intent intent, int resultCode, String data, Bundle extras,
                               boolean ordered, boolean sticky, int sendingUser) {
        String line = "Broadcast completed: result=" + resultCode + ", intent=" + intent;
        if (data != null) line = line + ", data=\"" + data + "\"";
        if (extras != null) line = line + ", extras: " + extras;
        Log.d(Constants.TAG, line);
    }
}
