package moe.shizuku.redirectstorage.server;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

import kotlin.io.FilesKt;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.collection.MountDirsTemplateList;
import moe.shizuku.redirectstorage.collection.ObserverInfoList;
import moe.shizuku.redirectstorage.collection.RedirectPackageInfoList;
import moe.shizuku.redirectstorage.collection.SimpleMountInfoList;
import moe.shizuku.redirectstorage.server.ktx.HandlerKt;
import moe.shizuku.redirectstorage.server.legacy.FilesLegacy;
import moe.shizuku.redirectstorage.server.util.FileUtils;
import moe.shizuku.redirectstorage.utils.DataVerifier;
import moe.shizuku.redirectstorage.utils.RandomId;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.server.Config.LATEST_VERSION;
import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class ConfigProvider {

    private static final String FILE_NAME = "configuration.json";

    private static final Gson GSON_IN = new GsonBuilder()
            .create();
    private static final Gson GSON_OUT = new GsonBuilder()
            .setVersion(LATEST_VERSION)
            .create();

    private static final Config DEFAULT;

    static {
        DEFAULT = new Config(
                new RedirectPackageInfoList(),
                new ObserverInfoList(),
                new SimpleMountInfoList(),
                Constants.REDIRECT_TARGET_DATA,
                null,
                new MountDirsTemplateList(),
                false,
                new Config.ModuleConfig());
        MountDirsTemplate template;
        template = MountDirsTemplate.createWithId(MountDirsTemplate.BUILT_IN_PHOTOS_ID);
        template.list.add(Environment.DIRECTORY_DCIM);
        template.list.add(Environment.DIRECTORY_PICTURES);
        DEFAULT.mountDirsTemplates.add(template);

        template = MountDirsTemplate.createWithId(MountDirsTemplate.BUILT_IN_MUSIC_ID);
        template.list.add(Environment.DIRECTORY_MUSIC);
        DEFAULT.mountDirsTemplates.add(template);

        template = MountDirsTemplate.createWithId(MountDirsTemplate.BUILT_IN_DOWNLOADS_ID);
        template.list.add(Environment.DIRECTORY_DOWNLOADS);
        template.list.add(Environment.DIRECTORY_DOCUMENTS);
        DEFAULT.mountDirsTemplates.add(template);

        DEFAULT.moduleConfig.disableExportNotification = true;
        DEFAULT.moduleConfig.enableFileMonitor = true;
        DEFAULT.moduleConfig.enableRenameFix = true;
        DEFAULT.moduleConfig.enableAppInteractionFix = true;
        DEFAULT.moduleConfig.enableNewAppNotification = true;
    }

    public static Handler getWorkerHandler() {
        return HandlerKt.getWorkerHandler();
    }

    public static File getConfigDir() {
        return new File("/data/adb/storage-isolation");
    }

    public static File getFile(boolean create) {
        File configDir = getConfigDir();
        if (!configDir.exists() && create) {
            //noinspection ResultOfMethodCallIgnored
            configDir.mkdirs();
        }
        File file = new File(configDir, FILE_NAME);
        if (!file.exists() && create) {
            // move old config file form /data/misc
            File oldData = new File("/data/misc/storage_redirect/configuration.json");
            if (oldData.exists()) {
                try {
                    FilesKt.copyTo(oldData, file, false, 8 * 1024);
                    LOGGER.i("old config file moved");
                } catch (Exception e) {
                    LOGGER.w("failed to move old config file");
                }
            }
        }
        return file;
    }

    static Config getDefault() {
        return DEFAULT;
    }

    static Config load(InputStream is) {
        Config config = GSON_IN.fromJson(new InputStreamReader(is), Config.class);
        if (TextUtils.isEmpty(config.defaultTarget)
                || !DataVerifier.isValidRedirectTarget(config.defaultTarget)) {
            config.defaultTarget = DEFAULT.defaultTarget;
        }
        if (config.version < 4 && config.packages != null) {
            for (RedirectPackageInfo info : config.packages) {
                info.enabled = true;
            }
        }
        if (config.version < 5 && config.observers != null) {
            for (ObserverInfo info : config.observers) {
                info.enabled = true;
            }
        }
        if (config.version < 6 || config.mountDirsTemplates == null) {
            config.mountDirsTemplates = DEFAULT.mountDirsTemplates;
        }
        if (config.version < 7 || config.simpleMounts == null) {
            config.simpleMounts = DEFAULT.simpleMounts;
        }
        if (config.version < 8) {
            config.killMediaStorageOnStart = DEFAULT.killMediaStorageOnStart;
        }
        if (config.version < 11 && config.packages != null) {
            boolean defaultIsEmpty = false;
            for (MountDirsTemplate template : new ArrayList<>(config.mountDirsTemplates)) {
                if (MountDirsTemplate.DEFAULT_ID.equals(template.id)) {
                    defaultIsEmpty = template.list.isEmpty();

                    // remove empty default
                    if (defaultIsEmpty) {
                        config.mountDirsTemplates.remove(template);
                    }
                    break;
                }
            }
            for (RedirectPackageInfo rpi : config.packages) {
                int type;
                if (rpi.mountDirs != null) {
                    type = rpi.mountDirs.isEmpty() ? MountDirsConfig.FLAG_NONE : MountDirsConfig.FLAG_ALLOW_CUSTOM;
                } else {
                    if ((Objects.equals(rpi.mountDirsTemplateId, MountDirsTemplate.DEFAULT_ID) && defaultIsEmpty)
                            || rpi.mountDirsTemplateId == null) {
                        type = MountDirsConfig.FLAG_NONE;
                        rpi.mountDirsTemplateId = null;
                    } else {
                        type = MountDirsConfig.FLAG_ALLOW_TEMPLATES;
                    }
                }

                rpi.mountDirsConfig = new MountDirsConfig();
                rpi.mountDirsConfig.flags = type;
                if (rpi.mountDirs != null) {
                    rpi.mountDirsConfig.customDirs = new HashSet<>(rpi.mountDirs);
                }
                if (rpi.mountDirsTemplateId != null) {
                    rpi.mountDirsConfig.templatesIds = new HashSet<>();
                    rpi.mountDirsConfig.templatesIds.add(rpi.mountDirsTemplateId);
                }

                rpi.mountDirsTemplateId = null;
                rpi.mountDirs = null;
            }
        }
        if (config.version < 12) {
            config.moduleConfig = new Config.ModuleConfig();
            File dir = new File(FilesLegacy.DISABLE_FIX_INTERACTION_UID);
            try {
                for (File file : FileUtils.listFilesOrEmpty(dir)) {
                    String name = file.getName();
                    int index = name.indexOf('.');
                    if (index <= 0) {
                        continue;
                    }
                    String userId = name.substring(0, index);
                    String packageName = name.substring(index + 1);
                    if (!TextUtils.isDigitsOnly(userId)) {
                        continue;
                    }
                    UidPackageInfo uidPackageInfo = UidPackageInfo.get(packageName, Integer.parseInt(userId));
                    if (uidPackageInfo == null) {
                        LOGGER.e("can't find uid for %s:%s", userId, packageName);
                        continue;
                    }

                    Config.ModuleConfig.Package pkg = new Config.ModuleConfig.Package(uidPackageInfo.getName(), uidPackageInfo.getUserId());
                    pkg.enableAppInteractionFix = false;
                    config.moduleConfig.packages.add(pkg);
                }
            } catch (Throwable e) {
                LOGGER.w(e, "listFilesOrEmpty %s", dir.getPath());
            }
        }
        if (config.version < 13) {
            for (RedirectPackageInfo rpi : config.packages) {
                UidPackageInfo uidPackageInfo = UidPackageInfo.get(rpi.packageName, rpi.userId);
                if (uidPackageInfo == null) {
                    continue;
                }
                if (rpi.enabled && !UserHandleUtils.isRegularApp(uidPackageInfo.getUid())) {
                    rpi.enabled = false;
                    LOGGER.w("%s (uid %d) is not regular app", rpi.packageName, uidPackageInfo.getUid());
                }
            }
        }
        if (config.version < 14) {
            if (config.moduleConfig == null) {
                config.moduleConfig = DEFAULT.moduleConfig;
            }
            config.moduleConfig.disableExportNotification = false;
        }
        if (config.version < 15) {
            for (ObserverInfo oi : config.observers) {
                if (TextUtils.isEmpty(oi.id)) {
                    oi.id = RandomId.next();
                }
                if (!TextUtils.isEmpty(oi.summary)) {
                    oi.summary = String.format("@%s", oi.summary);
                }
            }
        }
        if (config.version < 17) {
            config.moduleConfig.enableFileMonitor = !new File(FilesLegacy.DISABLE_FILE_MONITOR).exists();
            config.moduleConfig.enableRenameFix = !new File(FilesLegacy.DISABLE_FIX_RENAME).exists();
            config.moduleConfig.enableAppInteractionFix = !new File(FilesLegacy.DISABLE_FIX_INTERACTION).exists();
        }
        if (config.version < 18) {
            config.moduleConfig.enableNewAppNotification = true;
        }

        config.version = LATEST_VERSION;
        if (config.defaultTarget.startsWith("/")) {
            config.defaultTarget = config.defaultTarget.substring(1);
        }
        if (config.packages == null) {
            config.packages = new RedirectPackageInfoList();
        }
        if (config.observers == null) {
            config.observers = new ObserverInfoList();
        }
        if (config.simpleMounts == null) {
            config.simpleMounts = new SimpleMountInfoList();
        }
        if (config.moduleConfig == null) {
            config.moduleConfig = DEFAULT.moduleConfig;
        }
        if (config.moduleConfig.packages == null) {
            config.moduleConfig.packages = DEFAULT.moduleConfig.packages;
        }

        // ensure MountDirsConfig
        for (RedirectPackageInfo rpi : config.packages) {
            if (rpi.mountDirsConfig == null) {
                rpi.mountDirsConfig = new MountDirsConfig();
            }

            // remove invalid template ids
            if (rpi.mountDirsConfig.templatesIds != null) {
                for (String id : new ArrayList<>(rpi.mountDirsConfig.templatesIds)) {
                    boolean found = false;
                    for (MountDirsTemplate template : config.mountDirsTemplates) {
                        if (id.equals(template.id)) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        rpi.mountDirsConfig.templatesIds.remove(id);
                    }
                }
            }
        }

        // ensure summary
        for (ObserverInfo oi : config.observers) {
            if (TextUtils.isEmpty(oi.summary)) {
                oi.summary = "@saved_files";
            }
        }

        // remove invalid simple mount
        for (SimpleMountInfo item : new ArrayList<>(config.simpleMounts)) {
            if (item == null || !item.valid()) {
                config.simpleMounts.remove(item);
            }
        }
        for (SimpleMountInfo item : config.simpleMounts) {
            item.paths.remove("Android");
            item.paths.remove("Android/data");
            item.paths.remove("Android/media");
            item.paths.remove("Android/obb");
        }

        return config;
    }

    static void write(OutputStream os, Config config) throws IOException {
        String json = GSON_OUT.toJson(new Config(config));
        os.write(json.getBytes());
    }
}
