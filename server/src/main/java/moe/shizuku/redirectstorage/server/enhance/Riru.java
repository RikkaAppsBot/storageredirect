package moe.shizuku.redirectstorage.server.enhance;

import android.text.TextUtils;

import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.server.NativeHelper;

public class Riru {

    private static ModuleStatus instance;

    public static ModuleStatus status() {
        if (instance == null) {
            String versionName = NativeHelper.getRiruVersionName();
            String versionCodeString = NativeHelper.getRiruVersion();

            if (!TextUtils.isEmpty(versionName)
                    && !TextUtils.isEmpty(versionCodeString)
                    && TextUtils.isDigitsOnly(versionCodeString)) {
                instance = new ModuleStatus(Integer.parseInt(versionCodeString), versionName);
            } else {
                instance = new ModuleStatus(SRManager.MODULE_NOT_INSTALLED, null);
            }
        }
        return instance;
    }
}
