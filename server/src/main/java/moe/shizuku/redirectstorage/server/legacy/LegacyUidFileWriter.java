package moe.shizuku.redirectstorage.server.legacy;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Handler;
import android.system.ErrnoException;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.google.flatbuffers.FlatBufferBuilder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import kotlin.Pair;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.server.Config;
import moe.shizuku.redirectstorage.server.ConfigProvider;
import moe.shizuku.redirectstorage.server.PackageHelper;
import moe.shizuku.redirectstorage.server.Service;
import moe.shizuku.redirectstorage.server.UidPackageInfo;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.util.ArrayUtils;
import moe.shizuku.redirectstorage.server.enhance.EnhanceModule;
import moe.shizuku.redirectstorage.server.util.FileUtils;
import moe.shizuku.redirectstorage.server.util.Os;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public final class LegacyUidFileWriter {

    private static final int RDONLY = RedirectPackageInfo.PERMISSION_READ;
    private static final int RW = RedirectPackageInfo.PERMISSION_READ | RedirectPackageInfo.PERMISSION_WRITE;

    private static final File CONFIG_DIR = new File("/data/misc/storage_redirect/config");
    private static final File UID_CONFIG_DIR = new File(CONFIG_DIR, "uid");
    private static final File LEGACY_CONFIG_DIR = new File("/data/misc/storage_redirect");

    private static boolean skip() {
        return EnhanceModule.status().getVersionCode() >= EnhanceModule.V23_0;
    }

    public static void initializeDir() {
        if (skip()) return;

        if (!UID_CONFIG_DIR.exists()) {
            //noinspection ResultOfMethodCallIgnored
            UID_CONFIG_DIR.mkdirs();
        }
        Os.chmod(CONFIG_DIR.getPath(), 0710);
        Os.chown(CONFIG_DIR.getPath(), 0, 1000);
        Os.chmod(UID_CONFIG_DIR.getPath(), 0710);
        Os.chown(UID_CONFIG_DIR.getPath(), 0, 1000);
    }

    @VisibleForTesting
    public static byte[] createAppConfig(String packageName, int permission, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int pkgPos = builder.createString(packageName);
        int targetPos = builder.createString(target);

        List<Integer> mountPos = new ArrayList<>();
        if (mounts != null) {
            for (Pair<String, String> pair : mounts) {
                int pos1 = builder.createString(pair.getFirst());
                int pos2 = builder.createString(pair.getSecond());
                DirPair.startDirPair(builder);
                DirPair.addSource(builder, pos1);
                DirPair.addTarget(builder, pos2);
                mountPos.add(DirPair.endDirPair(builder));
            }
        }

        List<Integer> observerPos = new ArrayList<>();
        if (observers != null) {
            for (Pair<String, String> pair : observers) {
                int pos1 = builder.createString(pair.getFirst());
                int pos2 = builder.createString(pair.getSecond());
                DirPair.startDirPair(builder);
                DirPair.addSource(builder, pos1);
                DirPair.addTarget(builder, pos2);
                observerPos.add(DirPair.endDirPair(builder));
            }
        }

        int mountsPos = AppConfig.createMountsVector(builder, ArrayUtils.toArray(mountPos));
        int observersPos = AppConfig.createObserversVector(builder, ArrayUtils.toArray(observerPos));

        AppConfig.startAppConfig(builder);
        AppConfig.addPkg(builder, pkgPos);
        AppConfig.addPermission(builder, permission);
        AppConfig.addTarget(builder, targetPos);
        AppConfig.addMounts(builder, mountsPos);
        AppConfig.addObservers(builder, observersPos);

        int appConfigPos = AppConfig.endAppConfig(builder);
        AppConfig.finishAppConfigBuffer(builder, appConfigPos);

        ByteBuffer buffer = builder.dataBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        return bytes;
    }

    private static void writeAppConfig(String filename, String packageName, int permission, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        byte[] bytes = createAppConfig(packageName, permission, target, mounts, observers);

        File file = new File(UID_CONFIG_DIR, filename);
        try {
            //noinspection ResultOfMethodCallIgnored
            file.createNewFile();
            Os.chmod(file.getPath(), 0640);
            Os.chown(file.getPath(), 0, 1000);

            DataOutputStream writer = new DataOutputStream(new FileOutputStream(file));
            writer.write(bytes);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            LOGGER.e("failed to created file " + file + ".", e);
            return;
        }

        LOGGER.i("config %s created/updated", file.toString());
    }

    public static void writeDefault(Config config) {
        if (skip()) return;
        writeAppConfig("default", "default", RW, config.defaultTarget, null, null);
    }

    private static void writePackage(WriteItem item) {
        if (!item.info.enabled) {
            return;
        }

        writeAppConfig(Integer.toString(item.uid), item.pkg, item.permission, item.target, item.mounts, item.observers);

        for (String packageName : PackageManagerApi.getPackagesForUid(item.uid)) {
            File source = new File(UID_CONFIG_DIR, Integer.toString(item.uid));
            File target = new File(UID_CONFIG_DIR, String.format(Locale.ENGLISH, "%d.%s", UserHandleUtils.getUserId(item.uid), packageName));
            delete(target, false);
            try {
                android.system.Os.symlink(source.getPath(), target.getPath());
            } catch (ErrnoException e) {
                LOGGER.w(e, "symlink %s -> %s", source, target);
            }
        }
    }

    private static void delete(File file, boolean log) {
        if (file.delete()) {
            if (log) LOGGER.v("deleted %s", file.getPath());
        } else {
            if (log) LOGGER.w("can't delete %s", file.getPath());
        }
    }

    public static void deletePackage(String name, int user, int uid) {
        if (skip()) return;

        if (uid == -1) {
            File file = new File(UID_CONFIG_DIR, String.format(Locale.ENGLISH, "%d.%s", UserHandleUtils.getUserId(uid), name));
            if (file.exists()) {
                try {
                    String path = android.system.Os.readlink(file.getPath());
                    String uidName = new File(path).getName();
                    uid = Integer.parseInt(uidName);
                } catch (Throwable e) {
                    LOGGER.w(e, "readlink %s", file.getPath());
                }
            }
            if (uid == -1) {
                LOGGER.e("deletePackage: uid is -1");
                return;
            }
        }

        delete(new File(UID_CONFIG_DIR, Integer.toString(uid)), true);

        for (String sharedPackage : PackageManagerApi.getPackagesForUid(uid)) {
            delete(new File(UID_CONFIG_DIR, String.format(Locale.ENGLISH, "%d.%s", user, sharedPackage)), true);
        }
    }

    public static void rewriteAll(final Config config, final Service service) {
        if (skip()) return;

        List<String> files = new ArrayList<>();
        files.add("default");

        List<String> legacyFiles = new ArrayList<>();
        legacyFiles.add("configuration.json");
        legacyFiles.add("io_history.db");
        legacyFiles.add("file_monitor.db");

        for (RedirectPackageInfo info : config.packages) {
            if (!info.enabled) {
                continue;
            }

            UidPackageInfo uidPackageInfo = UidPackageInfo.get(info.packageName, info.userId);
            if (uidPackageInfo == null) {
                // not installed
                continue;
            }

            WriteItem item = collectInfo(info, uidPackageInfo, service);
            if (item == null) {
                // applicationInfo is null
                continue;
            }

            writePackage(item);

            int uid = uidPackageInfo.getUid();
            int userId = UserHandleUtils.getUserId(uid);
            List<String> sharedPackages = PackageManagerApi.getPackagesForUid(uid);

            files.add(String.valueOf(uid));
            for (String sharedPackage : sharedPackages) {
                files.add(String.format(Locale.ENGLISH, "%d.%s", userId, sharedPackage));
            }
        }

        for (File file : FileUtils.listFilesOrEmpty(UID_CONFIG_DIR)) {
            if (file.isDirectory() || files.contains(file.getName()))
                continue;

            delete(file, true);
        }

        for (File file : FileUtils.listFilesOrEmpty(LEGACY_CONFIG_DIR)) {
            String name = file.getName();
            if (file.isDirectory()
                    || name.charAt(0) == '.'
                    || legacyFiles.contains(name))
                continue;

            delete(file, true);
        }
    }

    private static WriteItem collectInfo(final RedirectPackageInfo info, final UidPackageInfo uidPackageInfo, final Service service) {
        PackageInfo pi = uidPackageInfo.getPackageInfo();
        if (pi.applicationInfo == null) {
            LOGGER.w("collectInfo: %d:%s applicationInfo is null", info.userId, info.packageName);
            return null;
        }

        String pkg = pi.sharedUserId != null ? (RedirectPackageInfo.SHARED_USER_PREFIX + pi.sharedUserId) : info.packageName;
        int uid = pi.applicationInfo.uid;
        String target = info.redirectTarget == null ? "" : info.redirectTarget;

        List<Pair<String, String>> mounts = new ArrayList<>();
        List<String> mountDirs = new ArrayList<>(service.getMountDirsForPackageLocked(uidPackageInfo, true));
        List<SimpleMountInfo> simpleMounts = new ArrayList<>(service.getSimpleMountsLocked(uidPackageInfo, uidPackageInfo.getUserId(), false));
        Collections.sort(mountDirs);
        for (String it : mountDirs) {
            mounts.add(new Pair<>(it, it));
        }
        for (SimpleMountInfo smi : simpleMounts) {
            UidPackageInfo sourceUidPackageInfo = UidPackageInfo.get(smi.sourcePackage, smi.userId);
            if (sourceUidPackageInfo == null) {
                continue;
            }
            RedirectPackageInfo sourceInfo = service.getRedirectPackageInfoLocked(sourceUidPackageInfo, false, false, true, false, false);
            if (sourceInfo == null) {
                continue;
            }
            Set<String> sharedPackages = new HashSet<>(PackageManagerApi.getPackagesForUid(sourceUidPackageInfo.getUid()));

            for (String it : smi.getDirectPaths(sharedPackages)) {
                mounts.add(new Pair<>(it, it));
            }
            String sourceTargetFormat = service.getRedirectTargetForPackageLocked(sourceInfo.redirectTarget, smi.sourcePackage);
            for (String it : smi.getRedirectedPaths(sharedPackages)) {
                mounts.add(new Pair<>(sourceTargetFormat + File.separator + it, it));
            }
        }

        List<Pair<String, String>> observers = new ArrayList<>();
        for (ObserverInfo oi : new ArrayList<>(service.getObserversLocked(uidPackageInfo, uidPackageInfo.getUserId(), false))) {
            observers.add(new Pair<>(oi.source, oi.target));
        }
        return new WriteItem(info, uid, pkg, RW, target, mounts, observers);
    }

    private static final Runner WRITE_RUNNER = new WriteRunner();

    public static void scheduleWriteLocked(@NonNull final RedirectPackageInfo info, @NonNull final PackageInfo pi, final Service service, final boolean kill) {
        if (skip()) return;
        scheduleWriteDelayedLocked(info, pi, service, kill, 0);
    }

    private static void scheduleWriteDelayedLocked(final RedirectPackageInfo info, final PackageInfo pi, final Service service, final boolean kill, long delay) {
        LOGGER.v("scheduleWrite: %d:%s", info.userId, info.packageName);

        UidPackageInfo uidPackageInfo = UidPackageInfo.get(pi, info.hidden);
        if (uidPackageInfo == null) {
            return;
        }
        WriteItem item = collectInfo(info, uidPackageInfo, service);
        if (item == null) {
            return;
        }

        synchronized (WRITE_RUNNER) {
            WRITE_RUNNER.addItem(item, kill);
        }

        Handler handler = ConfigProvider.getWorkerHandler();
        if (Build.VERSION.SDK_INT >= 29) {
            if (handler.hasCallbacks(WRITE_RUNNER)) {
                return;
            }
        } else {
            handler.removeCallbacks(WRITE_RUNNER);
        }

        if (delay == 0) {
            handler.post(WRITE_RUNNER);
        } else {
            handler.postDelayed(WRITE_RUNNER, delay);
        }
    }

    private static class WriteRunner extends Runner {

        @Override
        public synchronized void run() {
            for (WriteItem item : mItems) {
                writePackage(item);
                if (item.kill) {
                    PackageHelper.killUid(item.uid);
                }
            }
            mItems.clear();
        }
    }

    private static class WriteItem {

        private final RedirectPackageInfo info;
        private final String pkg;
        private final int uid;
        private final String target;
        private final int permission;
        private final List<Pair<String, String>> mounts;
        private final List<Pair<String, String>> observers;
        private boolean kill;

        WriteItem(RedirectPackageInfo info, int uid, String pkg, int permission, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
            this.info = info;
            this.pkg = pkg;
            this.uid = uid;
            this.target = target;
            this.permission = permission;
            this.mounts = mounts;
            this.observers = observers;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            WriteItem item = (WriteItem) o;
            return uid == item.uid;
        }
    }

    private abstract static class Runner implements Runnable {

        final List<WriteItem> mItems = new ArrayList<>();

        void addItem(WriteItem item, boolean kill) {
            int index = mItems.indexOf(item);
            if (index != -1) {
                mItems.remove(index);
            }
            item.kill |= kill;
            mItems.add(item);
        }

        void addItem(RedirectPackageInfo info, int uid, String pkg, int permission, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers, boolean kill) {
            addItem(new WriteItem(info, uid, pkg, permission, target, mounts, observers), kill);
        }
    }
}
