package moe.shizuku.redirectstorage.server.util;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;

import java.io.File;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.api.SystemService;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class DownloadManager {

    private static boolean isNotificationEnabled;

    public static void setIsNotificationEnabled(boolean isNotificationEnabled) {
        DownloadManager.isNotificationEnabled = isNotificationEnabled;
    }

    public static void notifyApp(final String packageName, final int userId, final File file) {
        if (!isNotificationEnabled) {
            return;
        }

        PackageInfo pi = PackageManagerApi.getPackageInfo(packageName, 0, userId);
        if (pi == null) {
            LOGGER.w("notifyApp: packageInfo for %d:%s is null", userId, packageName);
            return;
        }
        if (pi.applicationInfo == null) {
            LOGGER.w("notifyApp: applicationInfo for %d:%s is null", userId, packageName);
            return;
        }

        Intent intent = new Intent(Constants.APPLICATION_ID + ".action.ADD_TO_DOWNLOADS")
                .setComponent(ComponentName.createRelative(Constants.APPLICATION_ID, ".ReceiverActivity"))
                .setPackage(Constants.APPLICATION_ID)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
                .putExtra(Constants.EXTRA_PATH, file.getAbsolutePath())
                .putExtra(Constants.EXTRA_PACKAGE_INFO, pi)
                .putExtra(Constants.EXTRA_MIN_COMPATIBLE_VERSION, Constants.MIN_COMPATIBLE_VERSION);

        SystemService.startActivityNoThrow(intent, null, 0 /* notify app in main user */);
    }
}
