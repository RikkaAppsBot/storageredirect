package moe.shizuku.redirectstorage.server.observer;

import android.os.Handler;

import java.util.Collections;
import java.util.List;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.utils.BuildUtils;

public class ExportFolders {

    public static boolean isFileObserverSupported() {
        return !BuildUtils.isFuse();
    }

    public static FileObserverHolder createFileObserverHolder(int userId, Handler handler) {
        if (isFileObserverSupported()) {
            return new FileObserverHolder(userId, handler);
        } else {
            return new EmptyFileObserverHolder(userId, handler);
        }
    }

    private static class EmptyFileObserverHolder extends FileObserverHolder {

        public EmptyFileObserverHolder(int userId, Handler handler) {
            super(userId, handler);
        }

        @Override
        protected void init(int userId, Handler handler) {
        }

        @Override
        public boolean addRule(ObserverInfo oi) {
            return false;
        }

        @Override
        public List<ObserverInfo> getRules(String packageName) {
            return Collections.emptyList();
        }

        @Override
        public boolean containsRule(ObserverInfo info) {
            return false;
        }

        @Override
        public void removeRule(ObserverInfo oi) {
        }

        @Override
        public void removeRules(String packageName) {
        }

        @Override
        public void removeRules(String packageName, String source, String target) {
        }
    }
}

