package moe.shizuku.redirectstorage.server.util;

import android.system.ErrnoException;
import android.system.Os;
import android.system.StructStat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.IFileCalculatorListener;
import moe.shizuku.redirectstorage.model.FolderSizeEntry;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.server.Service;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class FileUtils {

    private static final String MEDIA_PATH = EnvironmentCompat.getMediaDirectory().getAbsolutePath();
    private static final String STORAGE_PATH = "/storage/emulated";

    private static final File[] EMPTY = new File[0];

    public static @NonNull
    File[] listFilesOrEmpty(@Nullable File dir) {
        if (dir == null) return EMPTY;
        final File[] res = dir.listFiles();
        if (res != null) {
            return res;
        } else {
            return EMPTY;
        }
    }

    /**
     * Test if a file lives under the given directory, either as a direct child
     * or a distant grandchild.
     * <p>
     * Both files <em>must</em> have been resolved using
     * {@link File#getCanonicalFile()} to avoid symlink or path traversal
     * attacks.
     */
    public static boolean contains(File[] dirs, File file) {
        for (File dir : dirs) {
            if (contains(dir, file)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test if a file lives under the given directory, either as a direct child
     * or a distant grandchild.
     * <p>
     * Both files <em>must</em> have been resolved using
     * {@link File#getCanonicalFile()} to avoid symlink or path traversal
     * attacks.
     */
    public static boolean contains(File dir, File file) {
        if (dir == null || file == null) return false;
        return contains(dir.getAbsolutePath(), file.getAbsolutePath());
    }

    public static boolean contains(String dirPath, String filePath) {
        if (dirPath.equals(filePath)) {
            return true;
        }
        if (!dirPath.endsWith("/")) {
            dirPath += "/";
        }
        return filePath.startsWith(dirPath);
    }

    public static @NonNull
    File toMedia(File file) {
        if (file.getAbsolutePath().startsWith(EnvironmentCompat.getMediaDirectory().getAbsolutePath())) {
            return file;
        }

        // /storage/emulated/0/example -> /0/example -> ["", "0", "example"]
        String[] paths = file.getAbsolutePath().replaceFirst(STORAGE_PATH, "")
                .split("/");

        File res = new File(MEDIA_PATH);
        for (int i = 1; i < paths.length; i++) {
            File child = new File(res, paths[i]);
            if (!child.exists() && res.exists() && res.isDirectory()) {
                for (File f : listFilesOrEmpty(res)) {
                    if (f.getName().equalsIgnoreCase(paths[i])) {
                        child = f;
                        break;
                    }
                }
            }
            res = child;
        }

        return res;
    }

    public static boolean rename(File src, File dst, boolean overwrite) {
        if (!src.exists()) {
            return false;
        }

        // dst not exists or empty
        if (!dst.exists() || (dst.isDirectory() && listFilesOrEmpty(dst).length == 0 && dst.delete())) {
            return src.renameTo(dst);
        } else {
            if (src.isDirectory()) {
                for (File srcChild : listFilesOrEmpty(src)) {
                    File dstChild = new File(dst, srcChild.getName());
                    rename(srcChild, dstChild, overwrite);
                }
            } else if (overwrite && dst.delete()) {
                return src.renameTo(dst);
            }
        }
        if (src.isDirectory() && listFilesOrEmpty(src).length == 0) {
            //noinspection ResultOfMethodCallIgnored
            src.delete();
        }
        return true;
    }

    public static File buildPath(File base, String... segments) {
        File cur = base;
        for (String segment : segments) {
            if (cur == null) {
                cur = new File(segment);
            } else {
                cur = new File(cur, segment);
            }
        }
        return cur;
    }

    public static File buildPathFromEmulated(int user) {
        if (user != 0 && BuildUtils.isFuse()) {
            return new File(String.format(Locale.ENGLISH, "/mnt/pass_through/%d/emulated/%d", user, user));
        } else {
            return new File(String.format(Locale.ENGLISH, Constants.STORAGE_PATH_FORMAT, user));
        }
    }

    public static File buildPathFromEmulated(int user, String child) {
        return new File(buildPathFromEmulated(user), child);
    }

    public static File buildPathFromEmulated(int user, String child, Object... args) {
        return new File(buildPathFromEmulated(user), String.format(Locale.ENGLISH, child, args));
    }

    public static File buildPathFromMedia(int user) {
        return new File(String.format(Locale.ENGLISH, EnvironmentCompat.getMediaDirectory() + "/%d", user));
    }

    public static File buildPathFromMedia(int user, String child) {
        return new File(String.format(Locale.ENGLISH, EnvironmentCompat.getMediaDirectory() + "/%d/%s", user, child));
    }

    public static File buildPathFromMedia(int user, String child, Object... args) {
        return new File(String.format(Locale.ENGLISH, EnvironmentCompat.getMediaDirectory() + "/%d/%s", user, String.format(Locale.ENGLISH, child, args)));
    }

    public static void fixCaseSensitive(File file) {
        String[] paths = file.getAbsolutePath()
                .replaceFirst(STORAGE_PATH, "")
                .replaceFirst(MEDIA_PATH, "")
                .split("/");

        File storage = new File(STORAGE_PATH);
        File media;
        List<File> temp = new ArrayList<>();
        for (int i = 1; i < paths.length - 1; i++) {
            if (!storage.exists() || storage.isFile()) {
                return;
            }

            temp.clear();

            media = toMedia(storage);
            for (File f : listFilesOrEmpty(media)) {
                if (f.getName().equalsIgnoreCase(paths[i])) {
                    temp.add(f);
                }
            }

            if (temp.size() > 1) {
                for (int j = 1; j < temp.size(); j++) {
                    rename(temp.get(j), temp.get(0), true);
                }
            }

            storage = new File(storage, paths[i]);
        }
    }

    public static void fixOwner(File file) {
        List<String> pathsToFix = new ArrayList<>();

        StructStat stat;
        int uid;
        int gid;

        do {
            try {
                stat = Os.stat(file.getAbsolutePath());
                uid = stat.st_uid;
                gid = stat.st_gid;
            } catch (ErrnoException e) {
                LOGGER.e(e, "stat %s: %s", file.getAbsolutePath(), e.getMessage());
                return;
            }

            if (uid == 0) {
                pathsToFix.add(file.getAbsolutePath());
            }

            file = file.getParentFile();
        } while (uid == 0);

        for (String path : pathsToFix) {
            try {
                Os.chown(path, uid, gid);
                LOGGER.i("chown %s %d %d", path, uid, gid);
            } catch (ErrnoException e) {
                LOGGER.e(e, "can't chown %s: %s", path, e.getMessage());
            }
        }
    }

    public static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    public static boolean isChildOf(String parent, String child) {
        if (parent.equals(child)) {
            return true;
        }
        if (!parent.endsWith("/")) {
            parent += "/";
        }
        if (!child.endsWith("/")) {
            child += "/";
        }
        return canonicalize(child).startsWith(canonicalize(parent));
    }

    public static class FoldersSizeCalculator {

        private final ForkJoinPool mForkJoinPool = new ForkJoinPool();

        private Service mService;
        private ServerFile mRoot;
        private IFileCalculatorListener mListener;

        public FoldersSizeCalculator(@NonNull Service service, @NonNull ServerFile root, @NonNull IFileCalculatorListener listener) {
            mService = service;
            mRoot = root;
            mListener = listener;
        }

        public void calculate() {
            mForkJoinPool.submit(new WalkTask(mRoot));
        }

        private class WalkTask extends RecursiveTask<List<FolderSizeEntry>> {

            private final ServerFile mParent;

            WalkTask(ServerFile parent) {
                mParent = parent;
            }

            @Override
            protected List<FolderSizeEntry> compute() {
                List<FolderSizeEntry> res = new ArrayList<>();
                FolderSizeEntry entry = new FolderSizeEntry(mParent);
                res.add(entry);
                File file = mService.getFileLocked(mParent);
                File[] childFiles = file.listFiles();
                if (childFiles != null) {
                    List<ForkJoinTask<List<FolderSizeEntry>>> tasks = new ArrayList<>();
                    for (File child : childFiles) {
                        if (child.isFile()) {
                            entry.addSize(child.length());
                        } else if (child.isDirectory()) {
                            ServerFile childFile = new ServerFile(mParent, child.getName());
                            childFile.setIsDirectory(true);
                            tasks.add(new WalkTask(childFile));
                        }
                    }
                    for (final ForkJoinTask<List<FolderSizeEntry>> task : invokeAll(tasks)) {
                        List<FolderSizeEntry> taskResult = task.join();
                        entry.addSize(taskResult.get(0).getSize());
                        res.addAll(taskResult);
                    }
                }

                if (mParent == mRoot) {
                    try {
                        mListener.onFinished(new ParceledListSlice<>(res));
                    } catch (Throwable e) {
                        LOGGER.w(e, "onFinished");
                    }
                }
                return res;
            }

        }

    }

}
