package moe.shizuku.redirectstorage.server.observer;

import android.os.FileObserver;
import android.util.ArrayMap;

import androidx.annotation.Nullable;

import java.io.File;
import java.util.Collections;
import java.util.Map;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.server.util.FileUtils;
import moe.shizuku.redirectstorage.server.util.Logger;

public class RecursiveFileObserver extends FileObserver {

    private static final Logger LOGGER = new Logger(Constants.TAG + "FO");

    private final Map<String, SingleFileObserver> mObservers;
    private final String mPath;
    private final int mMask;

    public RecursiveFileObserver(String path) {
        this(path, ALL_EVENTS);
    }

    public RecursiveFileObserver(String path, int mask) {
        super(path, mask);
        mPath = path;
        mMask = mask;

        mObservers = Collections.synchronizedMap(new ArrayMap<>());
    }

    @Override
    public void startWatching() {
        startWatching(mPath);
    }

    @Override
    public void stopWatching() {
        for (SingleFileObserver observer : mObservers.values()) {
            observer.stopWatching();
        }
        mObservers.clear();
    }

    @Override
    public void onEvent(int event, @Nullable String path) {
        int el = event & FileObserver.ALL_EVENTS;
        switch (el) {
            case FileObserver.MOVED_TO:
            case FileObserver.CREATE:
                if (new File(path).isDirectory()
                        && onCreateChildObserver(path)) {
                    startWatching(path);
                }
                break;
        }
    }

    public void startWatching(String path) {
        File file = new File(path);
        if (!file.exists() || file.isFile())
            return;

        if (FileUtils.isChildOf(getPath(), path) && !path.equals(getPath())) {
            file = new File(path);
            String parentPath;
            do {
                file = file.getParentFile();
                if (file == null)
                    break;
                parentPath = file.getPath();
                startWatching(parentPath, false);
            } while (!parentPath.equals(getPath()));
        }
        startWatching(path, true);
    }

    private void startWatching(String path, boolean includeChild) {
        SingleFileObserver sfo;
        if ((sfo = mObservers.get(path)) == null || !sfo.isRunning()) {
            sfo = new SingleFileObserver(path, mMask);
            sfo.startWatching();
            mObservers.put(path, sfo);
        }

        if (!includeChild)
            return;

        File file = new File(path);
        for (File f : FileUtils.listFilesOrEmpty(file)) {
            if (!f.isDirectory()
                    || f.getName().equals(".")
                    || f.getName().equals("..")) {
                continue;
            }

            if (onCreateChildObserver(f.getAbsolutePath())) {
                startWatching(f.getAbsolutePath(), true);
            }
        }
    }

    public void onObserverStarted(String path) {

    }

    public String getPath() {
        return mPath;
    }

    public int getMask() {
        return mMask;
    }

    public boolean onCreateChildObserver(String path) {
        return true;
    }

    private class SingleFileObserver extends FileObserver {

        private String mPath;
        private boolean mRunning;

        SingleFileObserver(String path) {
            this(path, ALL_EVENTS);
            mPath = path;
        }

        SingleFileObserver(String path, int mask) {
            super(path, mask);
            mPath = path;
        }

        public boolean isRunning() {
            return mRunning;
        }

        @Override
        public void startWatching() {
            super.startWatching();
            mRunning = true;

            onObserverStarted(mPath);
            LOGGER.i("start watching " + mPath);
        }

        @Override
        public void stopWatching() {
            super.stopWatching();
            mRunning = false;

            LOGGER.i("stop watching " + mPath);
        }

        @Override
        public void onEvent(int event, String path) {
            if (path != null) {
                String newPath = mPath + "/" + path;
                RecursiveFileObserver.this.onEvent(event, newPath);
            } else {
                if (!new File(mPath).exists()) {
                    LOGGER.i("Observer " + mPath + " stopped.");
                    super.stopWatching();
                    mRunning = false;

                    RecursiveFileObserver.this.mObservers.remove(mPath);
                }
            }
        }
    }
}
