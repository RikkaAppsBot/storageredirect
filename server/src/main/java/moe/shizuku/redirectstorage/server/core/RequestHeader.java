// automatically generated by the FlatBuffers compiler, do not modify

package moe.shizuku.redirectstorage.server.core;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class RequestHeader extends Table {
  public static void ValidateVersion() { Constants.FLATBUFFERS_1_12_0(); }
  public static RequestHeader getRootAsRequestHeader(ByteBuffer _bb) { return getRootAsRequestHeader(_bb, new RequestHeader()); }
  public static RequestHeader getRootAsRequestHeader(ByteBuffer _bb, RequestHeader obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __reset(_i, _bb); }
  public RequestHeader __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public int action() { int o = __offset(4); return o != 0 ? bb.get(o + bb_pos) & 0xFF : 0; }
  public String token() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer tokenAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public ByteBuffer tokenInByteBuffer(ByteBuffer _bb) { return __vector_in_bytebuffer(_bb, 6, 1); }

  public static int createRequestHeader(FlatBufferBuilder builder,
      int action,
      int tokenOffset) {
    builder.startTable(2);
    RequestHeader.addToken(builder, tokenOffset);
    RequestHeader.addAction(builder, action);
    return RequestHeader.endRequestHeader(builder);
  }

  public static void startRequestHeader(FlatBufferBuilder builder) { builder.startTable(2); }
  public static void addAction(FlatBufferBuilder builder, int action) { builder.addByte(0, (byte)action, (byte)0); }
  public static void addToken(FlatBufferBuilder builder, int tokenOffset) { builder.addOffset(1, tokenOffset, 0); }
  public static int endRequestHeader(FlatBufferBuilder builder) {
    int o = builder.endTable();
    builder.required(o, 6);  // token
    return o;
  }
  public static void finishRequestHeaderBuffer(FlatBufferBuilder builder, int offset) { builder.finish(offset); }
  public static void finishSizePrefixedRequestHeaderBuffer(FlatBufferBuilder builder, int offset) { builder.finishSizePrefixed(offset); }

  public static final class Vector extends BaseVector {
    public Vector __assign(int _vector, int _element_size, ByteBuffer _bb) { __reset(_vector, _element_size, _bb); return this; }

    public RequestHeader get(int j) { return get(new RequestHeader(), j); }
    public RequestHeader get(RequestHeader obj, int j) {  return obj.__assign(__indirect(__element(j), bb), bb); }
  }
}

