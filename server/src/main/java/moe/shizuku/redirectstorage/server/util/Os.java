package moe.shizuku.redirectstorage.server.util;

import android.system.ErrnoException;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

public class Os {

    public static void chmod(String path, int mode) {
        try {
            android.system.Os.chmod(path, mode);
        } catch (ErrnoException e) {
            LOGGER.e(e, "chmod %s %04o", path, mode);
        }
    }

    public static void chown(String path, int uid, int gid) {
        try {
            android.system.Os.chown(path, uid, gid);
        } catch (ErrnoException e) {
            LOGGER.e(e, "chmod %s %d %d", path, uid, gid);
        }
    }
}
