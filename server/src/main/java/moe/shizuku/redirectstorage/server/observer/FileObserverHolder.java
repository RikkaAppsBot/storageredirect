package moe.shizuku.redirectstorage.server.observer;

import android.app.ActivityManager;
import android.os.FileObserver;
import android.os.Handler;
import android.system.ErrnoException;
import android.system.Os;
import android.system.StructStat;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import hidden.HiddenApiBridge;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.server.api.PackageManagerApi;
import moe.shizuku.redirectstorage.server.api.SystemService;
import moe.shizuku.redirectstorage.server.util.DownloadManager;
import moe.shizuku.redirectstorage.server.util.FileUtils;
import moe.shizuku.redirectstorage.server.util.Logger;
import moe.shizuku.redirectstorage.server.util.MediaStorage;
import moe.shizuku.redirectstorage.server.util.ObserverInfoHelper;

import static android.os.FileObserver.CREATE;
import static android.os.FileObserver.DELETE;
import static android.os.FileObserver.MOVED_FROM;
import static android.os.FileObserver.MOVED_TO;

public class FileObserverHolder {

    private static final Logger LOGGER = new Logger(Constants.TAG + "FO");

    private static final int MEDIA_RW = 1023;

    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private static final List<String> IGNORE_DELETE_EVENT = new LinkedList<String>() {

        @Override
        public boolean add(String s) {
            if (contains(s))
                return false;

            return super.add(s);
        }

        @Override
        public boolean remove(Object o) {
            return super.remove(o);
        }
    };

    private RecursiveFileObserver mObserver;
    private List<ObserverInfo> mRules;

    @SuppressWarnings("FieldCanBeLocal")
    private int uid;
    @SuppressWarnings("FieldCanBeLocal")
    private int gid;

    public FileObserverHolder(int userId, Handler handler) {
        init(userId, handler);
    }

    protected void init(int userId, Handler handler) {
        StructStat stat = null;
        try {
            stat = Os.stat(FileUtils.buildPathFromMedia(userId).getAbsolutePath());
        } catch (ErrnoException e) {
            LOGGER.w(e, "stat %s", FileUtils.buildPathFromMedia(userId).getAbsolutePath());
        }

        if (stat != null) {
            uid = stat.st_uid;
            gid = stat.st_gid;
        } else {
            uid = MEDIA_RW;
            gid = MEDIA_RW;
        }

        LOGGER.i("uid: %d gid: %d", uid, gid);

        mRules = new CopyOnWriteArrayList<>();

        String path = String.format(Locale.ENGLISH, FileUtils.buildPathFromMedia(0).getAbsolutePath(), userId);
        mObserver = new Observer(path, /*CREATE | DELETE | MOVED_TO | MOVED_FROM*/FileObserver.ALL_EVENTS);
        mObserver.startWatching();
    }

    /**
     * @return file is created
     */
    private boolean link(File _oldFile, File _newFile, boolean deleteExisted) {
        if (!_oldFile.exists() || _oldFile.isDirectory()) {
            // return if old file not exists or is a directory
            return false;
        }

        if (!_newFile.getParentFile().exists()) {
            if (!_newFile.getParentFile().mkdirs()) {
                LOGGER.w("link: can't mkdirs %s", _newFile.getParent());
                return false;
            }
        }

        File oldFile = FileUtils.toMedia(_oldFile);
        File newFile = FileUtils.toMedia(_newFile);

        if (newFile.exists()) {
            if (deleteExisted) {
                LOGGER.i("link: existed target %s", newFile.toString());
                return false;
            }
            String path = newFile.getAbsolutePath();
            IGNORE_DELETE_EVENT.add(path);
            if (!newFile.delete()) {
                IGNORE_DELETE_EVENT.add(path);
            } else {
                LOGGER.i("link: delete existed target %s", path);
            }
        }
        try {
            Os.link(oldFile.getAbsolutePath(), newFile.getAbsolutePath());
        } catch (ErrnoException e) {
            LOGGER.w(e, "link: can't link %s -> %s", oldFile, newFile);
            return false;
        }

        if (_newFile.exists()) {
            LOGGER.i("link: %s -> %s", oldFile, newFile);
            return true;
        }

        return false;
    }

    private boolean delete(File file) {
        if (file.exists()) {
            if (file.delete()) {
                LOGGER.v("delete: deleted %s", file.getAbsolutePath());
                return true;
            } else {
                LOGGER.w("delete: can't delete %s", file.getAbsolutePath());
            }
        } else {
            LOGGER.w("delete: %s not exists", file.getAbsolutePath());
        }
        return false;
    }

    public boolean addRule(final ObserverInfo oi) {
        if (mRules.contains(oi)) {
            LOGGER.w("observer %s -> %s already exists for %d:%s", oi.source, oi.target, oi.userId, oi.packageName);
            return false;
        }

        mRules.add(oi);

        // link file from source to target
        EXECUTOR.execute(() -> {
            createLink(oi);

            // fix case sensitive
            // FileUtils.fixCaseSensitive(new File(ObserverInfoHelper.getSourcePathForUser(oi)));
            // FileUtils.fixCaseSensitive(new File(ObserverInfoHelper.getTargetPathForUser(oi)));

            // watch source / target
            mObserver.startWatching(ObserverInfoHelper.getDataPath(oi));
            mObserver.startWatching(ObserverInfoHelper.getTargetPath(oi));
        });

        LOGGER.i("create observer %s -> %s for %d:%s", oi.source, oi.target, oi.userId, oi.packageName);
        return true;
    }

    private List<ObserverInfo> getRules() {
        return mRules;
    }

    public List<ObserverInfo> getRules(String packageName) {
        List<ObserverInfo> list = new ArrayList<>();
        for (ObserverInfo observer : mRules) {
            if ((packageName == null
                    || observer.packageName.equals(packageName))) {
                list.add(observer);
            }
        }
        return list;
    }

    public boolean containsRule(ObserverInfo info) {
        return mRules.contains(info);
    }

    public void removeRule(ObserverInfo oi) {
        if (mRules.remove(oi)) {
            LOGGER.i("observer " + oi.source + " for package " + oi.packageName + " (" + oi.userId + ") removed.");
        }
    }

    public void removeRules(String packageName) {
        for (ObserverInfo oi : new ArrayList<>(mRules)) {
            if (oi.packageName.equals(packageName)) {
                mRules.remove(oi);
                LOGGER.i("observer " + oi.source + " for package " + packageName + " (" + oi.userId + ") removed.");
            }
        }
    }

    public void removeRules(String packageName, String source, String target) {
        for (ObserverInfo oi : new ArrayList<>(mRules)) {
            if (oi.packageName.equals(packageName)
                    && oi.source.equals(source)
                    && oi.target.equals(target)) {
                mRules.remove(oi);

                LOGGER.i("Observer " + source + " for package " + packageName + " (" + oi.userId + ") removed.");
            }
        }
    }

    private void createLink(ObserverInfo oi) {
        File source = new File(ObserverInfoHelper.getSourcePathForUser(oi));
        File target = new File(ObserverInfoHelper.getTargetPathForUser(oi));

        if (source.exists() && source.isDirectory()) {
            linkAll(source, target, oi, true, true);
        }

        if (target.exists() && target.isDirectory()) {
            linkAll(target, source, oi, false, false);
        }
    }

    private void linkAll(File source, File target, ObserverInfo oi, boolean notify, boolean sourceToTarget) {
        for (File from : FileUtils.listFilesOrEmpty(source)) {
            File to = new File(target, from.getName());
            if (to.exists()) {
                //delete(new File(to.getAbsolutePath().replaceFirst(Constants.MEDIA_PATH, Constants.STORAGE_PATH)));
                continue;
            }

            if (from.isDirectory()) {
                if (!oi.allowChild) {
                    continue;
                }

                if (!from.getName().startsWith(".")) {
                    linkAll(from, new File(target, from.getName()), oi, notify, sourceToTarget);
                }
                continue;
            }

            String child = from.getPath().substring(source.getPath().length() + 1);
            int reason;
            if (sourceToTarget && (reason = ObserverInfoHelper.shouldIgnore(oi, child, true)) != 0) {
                LOGGER.v("LinkAll: skip %s for %s", from, ObserverInfoHelper.getIgnoreReasonString(reason));
                continue;
            }

            if (link(from, to, false) && notify) {
                /*if (oi.showNotification) {
                    DownloadManager.notifyApp(oi.packageName, to, oi.userId, false, true);
                }*/

                if (oi.callMediaScan) {
                    MediaStorage.broadcastMediaScan(to, oi.userId);
                }
            }
        }
    }

    /**
     * @param event event
     * @param path  full path
     */
    private synchronized void handleEvent(int event, String path) {
        File file = new File(path);
        if (file.isDirectory()
                || file.getName().startsWith(".")) {
            return;
        }

        if ((event & (DELETE | MOVED_FROM)) > 0) {
            //LOGGER.v("delete pre " + IGNORE_DELETE_EVENT.toString());

            String ignore = null;
            for (String s : IGNORE_DELETE_EVENT) {
                if (s.equalsIgnoreCase(path)) {
                    ignore = s;
                    break;
                }
            }
            if (ignore != null) {
                IGNORE_DELETE_EVENT.remove(ignore);
                LOGGER.v("%s: ignore %s", (event & DELETE) > 0 ? "DELETE" : "MOVED_FROM", file);
                //LOGGER.v("delete post " + IGNORE_DELETE_EVENT.toString());
                return;
            }

            //LOGGER.v("delete post " + IGNORE_DELETE_EVENT.toString());
        }

        LOGGER.v("EVENT START: event=0x%08x, path=%s", event, path);

        for (ObserverInfo oi : mRules) {
            String _path = path.toLowerCase();
            String _source = ObserverInfoHelper.getSourcePath(oi).toLowerCase();
            String _target = ObserverInfoHelper.getTargetPath(oi).toLowerCase();

            // not it self
            if (_path.equals(_source) || _path.equals(_target)) {
                return;
            }

            if (_path.startsWith(_source)) {
                String child = path.substring(_source.length() + 1);

                int reason;
                if ((reason = ObserverInfoHelper.shouldIgnore(oi, child, true)) != 0) {
                    LOGGER.v("skip %s for %s", path, ObserverInfoHelper.getIgnoreReasonString(reason));
                    continue;
                }

                if ((event & (CREATE | MOVED_TO)) > 0) {
                    // create in source, link to target
                    LOGGER.v("%s in source: path=%s", (event & CREATE) > 0 ? "CREATE" : "MOVED_TO", path);

                    File newFile = new File(ObserverInfoHelper.getTargetPathForUser(oi), child);
                    if (link(file, newFile, true)) {
                        if (oi.showNotification) {
                            DownloadManager.notifyApp(oi.packageName, oi.userId, newFile);
                        }

                        if (oi.callMediaScan) {
                            MediaStorage.broadcastMediaScan(newFile, oi.userId);
                        }
                    }
                } else if ((event & (DELETE | MOVED_FROM)) > 0) {
                    // delete in source, delete target if app running
                    LOGGER.v("%s in source: path=%s", (event & DELETE) > 0 ? "DELETE" : "MOVED_FROM", path);

                    boolean delete = false;
                    try {
                        if (RedirectPackageInfo.isSharedUserId(oi.packageName)) {
                            int uid = PackageManagerApi.getUidForSharedUser(RedirectPackageInfo.getRawSharedUserId(oi.packageName), oi.userId);
                            if (uid != -1) {
                                delete = true;
                                for (String pkg : PackageManagerApi.getPackagesForUid(uid)) {
                                    int state = SystemService.getPackageProcessState(oi.packageName, oi.userId, null);
                                    int importance = HiddenApiBridge.ActivityManager_RunningAppProcessInfo_procStateToImportance(state);
                                    LOGGER.v("%d:%s: appState=%d, importance=%d", oi.userId, pkg, state, importance);

                                    delete &= importance <= ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
                                }
                            }
                        } else {
                            int state = SystemService.getPackageProcessState(oi.packageName, oi.userId, null);
                            int importance = HiddenApiBridge.ActivityManager_RunningAppProcessInfo_procStateToImportance(state);
                            LOGGER.v("%d:%s: appState=%d, importance=%d", oi.userId, oi.packageName, state, importance);

                            delete = importance <= ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
                        }
                    } catch (Throwable tr) {
                        LOGGER.w("getUidProcessState", tr);
                    }

                    if (delete) {
                        LOGGER.v("app is foreground, deleting target.");

                        // delete from /storage, so we don't need to handle case
                        File f = new File(ObserverInfoHelper.getTargetPathForUser(oi), child);

                        // next delete event in target is ignorable
                        String s = new File(ObserverInfoHelper.getTargetPath(oi), child).getAbsolutePath();
                        IGNORE_DELETE_EVENT.add(s);

                        //LOGGER.v("delete in source pre " + IGNORE_DELETE_EVENT.toString());

                        if (!delete(f)) {
                            IGNORE_DELETE_EVENT.remove(s);
                        }

                        //LOGGER.v("delete in source post " + IGNORE_DELETE_EVENT.toString());
                    } else {
                        LOGGER.v("app not foreground, do not delete target.");
                    }
                }
            } else if (_path.startsWith(_target)) {
                if (_path.replaceFirst(_target + "/", "").contains("/") && !oi.allowChild) {
                    continue;
                }

                String child = path.substring(_target.length());
                if ((event & (DELETE | MOVED_FROM)) > 0) {
                    // delete in target, we should also delete in source
                    if ((event & DELETE) > 0) {
                        LOGGER.v("DELETE in target: path=" + path);
                    } else {
                        LOGGER.v("MOVED_FROM in target: path=" + path);
                    }

                    // next delete event in source is ignorable
                    String s = new File(ObserverInfoHelper.getSourcePath(oi), child).getAbsolutePath();
                    IGNORE_DELETE_EVENT.add(s);

                    //LOGGER.v("delete in target pre " + IGNORE_DELETE_EVENT.toString());

                    // delete from /storage, so we don't need to handle case
                    File f = new File(ObserverInfoHelper.getSourcePathForUser(oi), child);
                    if (!delete(f)) {
                        IGNORE_DELETE_EVENT.remove(s);
                    }

                    //LOGGER.v("delete in source post " + IGNORE_DELETE_EVENT.toString());
                }
            }
        }

        LOGGER.v("EVENT END: event=0x%08x, path=%s", event, path);
    }

    private class Observer extends RecursiveFileObserver {

        Observer(String path, int mask) {
            super(path, mask);
        }

        @Override
        public boolean onCreateChildObserver(String path) {
            LOGGER.d("onCreateChildObserver: %s", path);

            if (new File(path).getName().startsWith(".")) {
                return false;
            }

            String _path = path.toLowerCase();

            for (final ObserverInfo observer : mRules) {
                String _source = ObserverInfoHelper.getSourcePath(observer).toLowerCase();
                String _target = ObserverInfoHelper.getTargetPath(observer).toLowerCase();

                if (_path.equals(_source)) {
                    EXECUTOR.execute(() -> createLink(observer));
                }

                if (_path.startsWith(_source)                                       // source (or its child)
                        || _path.equals(ObserverInfoHelper.getDataPath(observer))   // Android/data/<package>
                        || _source.startsWith(_path)                                // part of source
                        || _path.startsWith(_target)) {                             // target (or its child)
                    return true;
                }
            }

            String name = path.substring(getPath().length() + 1);
            return name.equalsIgnoreCase("Android") || name.equalsIgnoreCase("Android/data");
        }

        @Override
        public void onObserverStarted(String path) {
            for (String s : new ArrayList<>(IGNORE_DELETE_EVENT)) {
                if (FileUtils.isChildOf(path, s)) {
                    IGNORE_DELETE_EVENT.remove(s);
                    LOGGER.i("remove %s from IGNORE_DELETE_EVENT", s);
                }
            }
        }

        @Override
        public void onEvent(int event, String path) {
            super.onEvent(event, path);

            int el = event & FileObserver.ALL_EVENTS;
            if (path == null
                    || (el & (CREATE | DELETE | MOVED_TO | MOVED_FROM)) == 0) {
                return;
            }

            handleEvent(el, path);
        }
    }
}
