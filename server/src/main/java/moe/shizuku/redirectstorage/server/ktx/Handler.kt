package moe.shizuku.redirectstorage.server.ktx

import android.os.Handler
import android.os.HandlerThread
import android.os.Process

lateinit var mainHandler: Handler

private val workerThread by lazy(LazyThreadSafetyMode.NONE) {
    HandlerThread("Worker", Process.THREAD_PRIORITY_BACKGROUND).apply { start() }
}

val workerHandler by lazy {
    Handler(workerThread.looper)
}