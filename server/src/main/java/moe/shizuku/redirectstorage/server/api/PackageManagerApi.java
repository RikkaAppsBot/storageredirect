package moe.shizuku.redirectstorage.server.api;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import moe.shizuku.redirectstorage.server.util.HiddenValues;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;

import static moe.shizuku.redirectstorage.server.util.Logger.LOGGER;

/**
 * Cache to reduce pressure for system server.
 * <p>
 * MIUI has tens of uid 1000 packages, when using isolate those packages, system server will stop to respond binder calls.
 */
public class PackageManagerApi {

    private static final Map<String, Pair<PackageInfo, Integer>> PACKAGE_INFO_CACHE = Collections.synchronizedMap(new ArrayMap<>());
    private static final Map<String, Integer> NAME_TO_UID = Collections.synchronizedMap(new HashMap<>());
    private static final Map<Integer, List<String>> UID_TO_PACKAGES = Collections.synchronizedMap(new HashMap<>());

    private static final int ALL_GET_FLAGS = PackageManager.GET_PERMISSIONS
            | PackageManager.GET_ACTIVITIES
            | PackageManager.GET_CONFIGURATIONS
            | PackageManager.GET_GIDS
            | PackageManager.GET_INSTRUMENTATION
            | PackageManager.GET_INTENT_FILTERS
            | PackageManager.GET_META_DATA
            | PackageManager.GET_PERMISSIONS
            | PackageManager.GET_PROVIDERS
            | PackageManager.GET_RECEIVERS
            | PackageManager.GET_SERVICES
            | PackageManager.GET_SHARED_LIBRARY_FILES
            | PackageManager.GET_SIGNATURES
            | 0x08000000 /*PackageManager.GET_SIGNING_CERTIFICATES*/
            | PackageManager.GET_URI_PERMISSION_PATTERNS;

    private static boolean allowCache = false;

    public static void disableCache() {
        allowCache = false;

        LOGGER.d("disallow cache");
        clearCache();
    }

    public static void enableCache() {
        allowCache = true;

        int flags = PackageManager.GET_PERMISSIONS;
        for (int userId : SystemService.getUserIdListNoThrow()) {
            for (PackageInfo pi : SystemService.getInstalledPackagesNoThrow(flags | HiddenValues.MATCH_UNINSTALLED_PACKAGES, userId)) {
                String key = pi.packageName + userId;
                PACKAGE_INFO_CACHE.put(key, new Pair<>(pi, flags));
            }
        }
        LOGGER.v("added %d packages to package info cache", PACKAGE_INFO_CACHE.size());

        List<String> packages = SystemService.getPackagesForUidNoThrow(1000);
        if (!packages.isEmpty()) {
            UID_TO_PACKAGES.put(1000, packages);
        }
        LOGGER.v("added %d packages to uid cache", packages.size());
    }

    public static void clearCache() {
        LOGGER.d("clear cache");
        NAME_TO_UID.clear();
        UID_TO_PACKAGES.clear();
    }

    public static void clearCacheForPackage(String packageName, int userId) {
        String key = packageName + userId;
        PACKAGE_INFO_CACHE.remove(key);
        LOGGER.v("remove cache for %d:%s", userId, packageName);

        for (Map.Entry<Integer, List<String>> entry : new ArrayList<>(UID_TO_PACKAGES.entrySet())) {
            if (entry.getValue().contains(packageName)) {
                int uid = entry.getKey();
                LOGGER.i("SharedUserCache: clear cache for '%s', uid=%d", packageName, uid);
                UID_TO_PACKAGES.remove(uid);
                //noinspection StatementWithEmptyBody
                while (NAME_TO_UID.values().remove(uid)) ;
            }
        }
    }

    @Nullable
    public static PackageInfo getPackageInfo(@NonNull String packageName, int flags, int userId) {
        if (packageName == null) return null;

        LOGGER.d("getPackageInfo %d:%s", userId, packageName);

        PackageInfo res;
        String key = null;
        Pair<PackageInfo, Integer> cached = null;

        if (allowCache) {
            key = packageName + userId;
            cached = PACKAGE_INFO_CACHE.get(key);

            if (cached != null) {
                res = cached.first;
                int maxFlags = cached.second;
                if (((flags & ALL_GET_FLAGS) & (~maxFlags)) != 0) {
                    LOGGER.v("found cache, but request with more flags.");
                    flags |= maxFlags;
                } else if (res != null) {
                    LOGGER.d("use cache for %d:%s", userId, packageName);
                    return res;
                }
            }
        }

        try {
            res = SystemService.getPackageInfo(packageName, flags, userId);
        } catch (Throwable tr) {
            LOGGER.w(tr, "getPackageInfo failed: packageName=%s, flags=%d, user=%d", packageName, flags, userId);
            return null;
        }

        if (allowCache) {
            if (cached != null) {
                LOGGER.v("update cache for %d:%s%s", userId, packageName, res != null ? "" : " (null)");
            } else {
                LOGGER.v("add cache for %d:%s%s", userId, packageName, res != null ? "" : " (null)");
            }
            PACKAGE_INFO_CACHE.put(key, new Pair<>(res, flags));
        }

        return res;
    }

    public static int getUidForSharedUser(String sharedUserId, int userId) {
        Integer uid = NAME_TO_UID.get(sharedUserId);
        if (uid == null) {
            uid = SystemService.getUidForSharedUserNoThrow(sharedUserId);
            if (uid == -1) {
                LOGGER.w("SharedUserCache: shared user '%s' not exist", sharedUserId);
                return -1;
            }
            NAME_TO_UID.put(sharedUserId, uid);
        }

        // getUidForSharedUser returns user 0, add userId by ourselves
        int appId = UserHandleUtils.getAppId(uid);
        uid = appId + userId * UserHandleUtils.PER_USER_RANGE;
        return uid;
    }

    @NonNull
    public static List<String> getPackagesForUid(int uid, boolean forceUpdate) {
        List<String> packages = forceUpdate ? null : UID_TO_PACKAGES.get(uid);
        if (packages == null) {
            packages = SystemService.getPackagesForUidNoThrow(uid);
            UID_TO_PACKAGES.put(uid, packages);
        }
        return packages;
    }

    @NonNull
    public static List<String> getPackagesForUid(int uid) {
        return getPackagesForUid(uid, false);
    }

    @Nullable
    public static String findPackageForSharedUser(String sharedUserId, int userId) {
        int uid = getUidForSharedUser(sharedUserId, userId);
        if (uid == -1) {
            return null;
        }

        for (String packageName : getPackagesForUid(uid)) {
            if (SystemService.getApplicationInfoNoThrow(packageName, 0, userId) != null) {
                LOGGER.i("SharedUserCache: found installed package '%s' for shared user '%s'", packageName, sharedUserId);
                return packageName;
            }
        }
        LOGGER.w("SharedUserCache: can't find installed package for shared user '%s'", sharedUserId);
        return null;
    }

    private static int getAuthorityAppId(String name) {
        try {
            ProviderInfo info = SystemService.resolveContentProvider(name, 0, 0);
            if (info != null) {
                return UserHandleUtils.getAppId(info.applicationInfo.uid);
            }
        } catch (Throwable e) {
            LOGGER.e(Log.getStackTraceString(e));
        }
        return -1;
    }

    private static int mediaStoreAuthorityAppId = -1;

    public static boolean isExternalStorageService(int uid) {
        if (mediaStoreAuthorityAppId == -1) {
            mediaStoreAuthorityAppId = getAuthorityAppId("media");
        }

        return mediaStoreAuthorityAppId == UserHandleUtils.getAppId(uid);
    }

    private static int downloadsAuthorityAppId = -1;

    public static boolean isDownloadsAuthorityAppId(int uid) {
        if (downloadsAuthorityAppId == -1) {
            downloadsAuthorityAppId = getAuthorityAppId("downloads");
        }

        return downloadsAuthorityAppId == UserHandleUtils.getAppId(uid);
    }

    private static int externalStorageAuthorityAppId = -1;

    public static boolean isExternalStorageAuthorityAppId(int uid) {
        if (externalStorageAuthorityAppId == -1) {
            externalStorageAuthorityAppId = getAuthorityAppId("com.android.externalstorage.documents");
        }

        return externalStorageAuthorityAppId == UserHandleUtils.getAppId(uid);
    }
}
