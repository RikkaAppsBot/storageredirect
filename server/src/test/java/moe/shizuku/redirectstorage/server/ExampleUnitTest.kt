package moe.shizuku.redirectstorage.server

import moe.shizuku.redirectstorage.server.legacy.AppConfig
import moe.shizuku.redirectstorage.server.legacy.LegacyUidFileWriter
import org.junit.Test

import org.junit.Assert.*
import java.nio.ByteBuffer

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun flatbufferTest() {
        val bytes =  LegacyUidFileWriter.createAppConfig("1", 1, "222", listOf(Pair("2", "3")), null)
        val buffer = ByteBuffer.wrap(bytes)
        val appConfig = AppConfig.getRootAsAppConfig(buffer)
        assertEquals(appConfig.pkg(), "1")
        assertEquals(appConfig.target(), "222")
        assertEquals(appConfig.mountsLength(), 1)
        assertEquals(appConfig.mounts(0).source(), "2")
        assertEquals(appConfig.mounts(0).target(), "3")
    }
}