##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Prevent proguard from stripping interface information from TypeAdapter, TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Prevent R8 from leaving Data object members always null
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

##---------------End: proguard configuration for Gson  ----------

# Kotlin
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}

## SR rules
-obfuscationdictionary ../app/dictionary.new.txt

-repackageclasses moe.shizuku.redirectstorage.server

-keepnames class moe.shizuku.redirectstorage.RedirectPackageInfo
-keepnames class moe.shizuku.redirectstorage.ObserverInfo
-keepnames class moe.shizuku.redirectstorage.MountDirsConfig
-keepnames class moe.shizuku.redirectstorage.MountDirsTemplate
-keepnames class moe.shizuku.redirectstorage.SimpleMountInfo { *; }
-keepnames class moe.shizuku.redirectstorage.model.ModuleStatus
-keep class moe.shizuku.redirectstorage.model.FileMonitorRecord { *; }
-keep class moe.shizuku.redirectstorage.model.ParcelStructStat { *; }
-keep class moe.shizuku.redirectstorage.model.ServerFile { *; }
-keep class moe.shizuku.redirectstorage.model.DebugInfo { *; }
-keep class moe.shizuku.redirectstorage.model.FolderSizeEntry { *; }

-keep class moe.shizuku.redirectstorage.server.Starter {
    public static void main(java.lang.String[]);
}

-assumenosideeffects class android.util.Log {
    public static *** d(...);
}

-keepattributes LineNumberTable