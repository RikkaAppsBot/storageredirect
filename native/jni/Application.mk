APP_ABI := arm64-v8a armeabi-v7a x86 x86_64
APP_PLATFORM := android-23
APP_CFLAGS := -std=gnu99
APP_CPPFLAGS := -std=c++17
APP_LDFLAGS := -Wl,-exclude-libs,ALL
APP_STL := c++_static
APP_SHORT_COMMANDS := true

ifeq ($(NDK_DEBUG),1)
$(info building DEBUG version...)
APP_CFLAGS += -O0
APP_OPTIM := debug
else
$(info building RELEASE version...)
APP_LDFLAGS +=  -Wl,-exclude-libs,ALL -Wl,--gc-sections -s
APP_CFLAGS += -fvisibility=hidden -fvisibility-inlines-hidden -fdata-sections -ffunction-sections -Wl,-exclude-libs,ALL -Wl,--gc-sections
APP_CPPFLAGS += -fno-exceptions -fno-rtti
APP_OPTIM := release
APP_STRIP_MODE := --strip-unneeded --remove-section=.comment
endif