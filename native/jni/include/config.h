#pragma once

#define PROCESS_NAME                "storage_isolation_daemon"
#define PROCESS_NAME_JAVA           "storage_isolation"
#define PROCESS_NAME_JAVA_CLEANER   "storage_isolation_cleaner"
#define PROCESS_NAME_WORKER         "storage_isolation_worker"

#define SERVER_CLASS                "moe.shizuku.redirectstorage.server.Starter"

#define ROOT_PATH                   "/data/adb/storage-isolation"
#define BIN_PATH                    ROOT_PATH "/bin"