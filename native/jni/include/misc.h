#ifndef _MISC_H
#define _MISC_H

#include <sys/types.h>
#include <vector>

ssize_t fdgets(char *buf, const size_t size, int fd);
int get_proc_name(int pid, char* name, size_t size);
int mkdirs(const char *pathname, mode_t mode);
int switch_mnt_ns(int pid);
int read_mntns(const int pid, char *target, const size_t size);
int ensure_dir(const char *path, mode_t mode);
int print_mounts(const char *path);
int is_proc_dead(pid_t pid);
int is_num(const char *s);
char *trim(char * s);
int exec_command(int err, int *fd, const char *argv0, ...);
int execl_sync(const char *argv0, ...);
int execv_sync(const char *pathname, char *const *argv);
int to_int(const char *s);
int copyfileat(int src_path_fd, const char *src_path, int dst_path_fd, const char *dst_path);
int copyfile(const char *src_path, const char *dst_path);
size_t get_adoptable_storage_path(char *path, size_t size);
void gen_rand_str(char *buf, int len, bool varlen = true);
int my_strcmp(const char *s1, const char *s2);
int my_memcmp(const void *s1, const void *s2, size_t n);
int is_dir(const char *path);
intptr_t openAt(intptr_t fd, const char *path, intptr_t flag);
ssize_t read_file(const char *file, char *buf, size_t size);
ssize_t write_file(const char *file, const char *buf, size_t size, mode_t modes = 0700);

using thread_function = void *(*)(void *);
int daemon_thread(thread_function func, void *arg = nullptr, const pthread_attr_t *attr = nullptr);

using void_func = void();
void daemon_thread(void_func *func);

int read_full(int fd, void *buf, size_t count);
int write_full(int fd, const void *buf, size_t count);
int chown_recursive(const char *path, size_t offset, uid_t uid, gid_t gid);

using foreach_proc_function = bool(pid_t);
void foreach_proc(foreach_proc_function *);

#endif // _MISC_H
