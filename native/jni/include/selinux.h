#ifndef _SELINUX_SAFE_H
#define _SELINUX_SAFE_H

void dload_selinux();

extern int (*getcon)(char **con);
extern void (*freecon)(char *con);
extern int (*getfilecon)(const char *path, char **con);
extern int (*setfilecon)(const char *path, const char *con);
extern int (*lsetfilecon)(const char *path, const char *con);
extern int (*lsetfilecon)(const char *path, const char *con);
extern int (*selinux_check_access)(const char * scon, const char * tcon, const char *tclass, const char *perm, void *auditdata);

#endif // _SELINUX_SAFE_H