#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>
#include <sys/un.h>

#define SOCKET_SERVER_PROCESS_ADDRESS "sr-process-handler"
#define SOCKET_REQUEST_HANDLER_ADDRESS "sr-request-handler"

socklen_t setup_sockaddr(struct sockaddr_un *sun, const char *name);
int get_client_cred(int fd, struct ucred *cred);
int set_socket_timeout(int fd, long sec);

#endif // SOCKET_H
