#include <cstdio>
#include <cstring>
#include <cerrno>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/wait.h>
#include <thread>
#include <dlfcn.h>
#include <sys/system_properties.h>
#include <selinux.h>
#include <version.h>
#include <misc.h>
#include <wrap.h>

#define TARGET_PATH "/data/adb/storage-isolation"
#define TARGET_BIN_PATH TARGET_PATH "/bin"
#define TARGET_DAEMON_PATH TARGET_BIN_PATH "/daemon"

#define EXIT_FATAL_EXEC_DENIED 10

int main(int argc, char **argv) {
    dload_selinux();

    printf("info: switching mount namespace to init...\n");
    fflush(stdout);

    switch_mnt_ns(1);

    // ensure dirs
    if (_mkdirs(TARGET_BIN_PATH, 0700) != 0) {
        printf("fatal: unable to create %s (%s)\n", TARGET_PATH, strerror(errno));
        printf("info: this may because your root is constrained, consider switch to Magisk\n");
        fflush(stdout);
        return 1;
    }

    _chmod(TARGET_PATH, 0700);
    _chmod(TARGET_BIN_PATH, 0700);

    // read args
    char source_path[PATH_MAX], target_path[PATH_MAX];
    const char *source_bin_path = nullptr;
    bool from_ui = false;
    for (int i = 0; i < argc; ++i) {
        if (strncmp(argv[i], "--bin-path=", 11) == 0) {
            source_bin_path = strdup(argv[i] + 11);
        } else if (strncmp(argv[i], "--from-ui", 9) == 0) {
            from_ui = true;
        }
    }

    // copy daemon
    sprintf(source_path, "%s/%s", source_bin_path, "daemon");
    sprintf(target_path, "%s/%s", TARGET_BIN_PATH, "daemon-v" VERSION_NAME);
    if (access(source_path, R_OK) != 0) {
        printf("info: %s not exist, please open Storage Isolation app and try again.\n", source_path);
        fflush(stdout);
        return 1;
    }
    unlink(target_path);
    _copyfile(source_path, target_path);
    _chmod(target_path, 0700);

    sprintf(source_path, "%s/%s", TARGET_BIN_PATH, "daemon");
    _remove(source_path);
    symlink(target_path, source_path);

    // copy daemon to old location
    if (access("/data/misc/storage_redirect/bin/daemon", F_OK) == 0) {
        printf("info: old files exists...\n");
        fflush(stdout);

        sprintf(source_path, "%s/%s", source_bin_path, "daemon");
        sprintf(target_path, "%s/%s", "/data/misc/storage_redirect/bin", "daemon-v" VERSION_NAME);
        unlink(target_path);
        _copyfile(source_path, target_path);
        _chmod(target_path, 0700);

        symlink(target_path, "/data/misc/storage_redirect/bin/daemon");
    }

    // copy libhelper.so
    sprintf(source_path, "%s/%s", source_bin_path, "libhelper.so");
    sprintf(target_path, "%s/%s", TARGET_BIN_PATH, "libhelper.so");
    _remove(target_path);
    _copyfile(source_path, target_path);
    _chmod(target_path, 0700);

    // copy main.dex
    sprintf(source_path, "%s/%s", source_bin_path, "main.dex");
    sprintf(target_path, "%s/%s", TARGET_BIN_PATH, "main.dex");
    _remove(target_path);
    _copyfile(source_path, target_path);

    int status;
    pid_t pid = fork();
    if (pid == 0) {
        printf("info: execl %s\n", TARGET_DAEMON_PATH);
        fflush(stdout);
        execl(TARGET_DAEMON_PATH, TARGET_DAEMON_PATH,
              "--library-path=" TARGET_BIN_PATH "/libhelper.so", from_ui ? "--from-user" : "--from-boot",
              nullptr);

        errno = EACCES;
        printf("fatal: execl %s failed with %d: %s\n", TARGET_DAEMON_PATH, errno, strerror(errno));

        int errno2 = errno;
        errno = 0;

        int res2 = access(TARGET_DAEMON_PATH, R_OK | X_OK);
        printf("info: access(%s, R_OK|X_OK): %d (%d: %s)\n",
               TARGET_DAEMON_PATH, res2, errno, strerror(errno));
        fflush(stdout);

        if ((errno2 == EACCES || errno2 == EPERM) && res2 == 0) {
            printf("info: exec get permission denied when file is accessible\n");
        }

        printf("info: Samsung kernel problem? please try arm version...\n");
        fflush(stdout);

        return EXIT_FATAL_EXEC_DENIED;
    } else if (pid == -1) {
        printf("fatal: can't fork\n");
        fflush(stdout);
        return 1;
    } else {
        wait(&status);

        if (WIFEXITED(status)) {
            printf("info: exit code %d\n", WEXITSTATUS(status));
            fflush(stdout);
            return WEXITSTATUS(status);
        } else {
            printf("info: not terminated with exit\n");
            fflush(stdout);
            return 1;
        }
    }
}