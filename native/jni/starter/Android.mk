LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE     := starter
LOCAL_STATIC_LIBRARIES := selinux utils
LOCAL_LDLIBS += -ldl -llog -lz
LOCAL_LDFLAGS := -Wl,--hash-style=both

LOCAL_C_INCLUDES := jni/include

LOCAL_SRC_FILES:= main.cpp

include $(BUILD_EXECUTABLE)

#$(NDK_APP_LIBS_OUT)/%/libstarter.so: $(NDK_APP_LIBS_OUT)/%/starter
#		$(call host-mv,$<,$@)
#
#all: $(foreach _abi,$(APP_ABI),$(NDK_APP_LIBS_OUT)/$(_abi)/libstarter.so)
