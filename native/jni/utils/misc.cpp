#include <cstdio>
#include <cstring>
#include <random>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <cerrno>
#include <sys/stat.h>
#include <cstdlib>
#include <string>
#include <mntent.h>
#include <vector>
#include <wait.h>
#include <dlfcn.h>
#include <sys/sendfile.h>
#include <sys/sysmacros.h>
#include <syscall.h>

#include "logging.h"
#include "misc.h"
#include "wrap.h"

ssize_t fdgets(char *buf, const size_t size, int fd) {
    ssize_t len = 0;
    buf[0] = '\0';
    while (len < size - 1) {
        ssize_t ret = read(fd, buf + len, 1);
        if (ret < 0)
            return -1;
        if (ret == 0)
            break;
        if (buf[len] == '\0' || buf[len++] == '\n') {
            buf[len] = '\0';
            break;
        }
    }
    buf[len] = '\0';
    buf[size - 1] = '\0';
    return len;
}

int get_proc_name(int pid, char *name, size_t _size) {
    int fd;
    ssize_t __size;

    char buf[1024];
    snprintf(buf, sizeof(buf), "/proc/%d/cmdline", pid);
    if (access(buf, R_OK) == -1 || (fd = open(buf, O_RDONLY)) == -1)
        return 1;
    if ((__size = fdgets(buf, sizeof(buf), fd)) == 0) {
        snprintf(buf, sizeof(buf), "/proc/%d/comm", pid);
        close(fd);
        if (access(buf, R_OK) == -1 || (fd = open(buf, O_RDONLY)) == -1)
            return 1;
        __size = fdgets(buf, sizeof(buf), fd);
    }
    close(fd);

    if (__size < _size) {
        strncpy(name, buf, static_cast<size_t>(__size));
        name[__size] = '\0';
    } else {
        strncpy(name, buf, _size);
        name[_size] = '\0';
    }

    return 0;
}

int is_num(const char *s) {
    size_t len = strlen(s);
    for (size_t i = 0; i < len; ++i)
        if (s[i] < '0' || s[i] > '9')
            return 0;
    return 1;
}

void foreach_proc(foreach_proc_function *func) {
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir("/proc")))
        return;

    while ((entry = readdir(dir))) {
        if (entry->d_type != DT_DIR) continue;
        if (!is_num(entry->d_name)) continue;
        pid_t pid = atoi(entry->d_name);
        if (func(pid)) break;
    }

    closedir(dir);
}

int mkdirs(const char *pathname, mode_t mode) {
    char *path = strdup(pathname), *p;
    errno = 0;
    for (p = path + 1; *p; ++p) {
        if (*p == '/') {
            *p = '\0';
            if (mkdir(path, mode) == -1) {
                if (errno != EEXIST)
                    return -1;
            }
            *p = '/';
        }
    }
    if (mkdir(path, mode) == -1) {
        if (errno != EEXIST)
            return -1;
    }
    free(path);
    return 0;
}

int switch_mnt_ns(int pid) {
    char mnt[32];
    snprintf(mnt, sizeof(mnt), "/proc/%d/ns/mnt", pid);
    if (access(mnt, R_OK) == -1) return -1; // Maybe process died..

    int fd, res;
    fd = _open(mnt, O_RDONLY);
    if (fd < 0) return -1;
    // Switch to its namespace
    res = _setns(fd, 0);
    close(fd);
    return res;
}

int read_mntns(const int pid, char *target, const size_t size) {
    char path[32];
    snprintf(path, sizeof(path), "/proc/%d/ns/mnt", pid);
    if (access(path, R_OK) == -1)
        return 1;
    _readlink(path, target, size);
    return 0;
}

int ensure_dir(const char *path, mode_t mode) {
    if (access(path, R_OK) == -1)
        return _mkdirs(path, mode);

    return 0;
}

int print_mounts(const char *path) {
    size_t path_len = strlen(path);

    FILE *fp = setmntent("/proc/mounts", "r");
    if (fp == nullptr) {
        LOGE("error opening /proc/mounts: %s", strerror(errno));
        return -1;
    }

    struct mntent *mentry;
    while ((mentry = getmntent(fp)) != nullptr) {
        if (strncmp(mentry->mnt_dir, path, path_len) == 0) {
            LOGV("%s %s", mentry->mnt_dir, mentry->mnt_opts);
        }
    }
    endmntent(fp);
    return 0;
}

int is_proc_dead(pid_t pid) {
    return getpgid(pid) == -1;
}

char *trim(char *str) {
    size_t len = 0;
    char *frontp = str;
    char *endp = nullptr;

    if (str == nullptr) { return nullptr; }
    if (str[0] == '\0') { return str; }

    len = strlen(str);
    endp = str + len;

    /* Move the front and back pointers to address the first non-whitespace
     * characters from each end.
     */
    while (isspace((unsigned char) *frontp)) { ++frontp; }
    if (endp != frontp) {
        while (isspace((unsigned char) *(--endp)) && endp != frontp) {}
    }

    if (str + len - 1 != endp)
        *(endp + 1) = '\0';
    else if (frontp != str && endp == frontp)
        *str = '\0';

    /* Shift the string so that it starts at str so that if it's dynamically
     * allocated, we can still free it on the returned pointer.  Note the reuse
     * of endp to mean the front of the string buffer now.
     */
    endp = str;
    if (frontp != str) {
        while (*frontp) { *endp++ = *frontp++; }
        *endp = '\0';
    }

    return str;
}

/*
   fd == NULL -> Ignore output
  *fd < 0     -> Open pipe and set *fd to the read end
  *fd >= 0    -> STDOUT (or STDERR) will be redirected to *fd
  *cb         -> A callback function which runs after fork
*/
static int exec_command_v(int err, int *fd, const char *pathname, char *const *argv) {
    int pipefd[2], writeEnd = -1;

    if (fd) {
        if (*fd < 0) {
            if (_pipe2(pipefd, O_CLOEXEC) == -1)
                return -1;
            writeEnd = pipefd[1];
        } else {
            writeEnd = *fd;
        }
    }

    // Setup environment
    char *const *envp;
    extern char **environ;
    envp = environ;

    int pid = _fork();
    if (pid == -1) {
        return -1;
    }

    if (pid != 0) {
        if (fd && *fd < 0) {
            // Give the read end and close write end
            *fd = pipefd[0];
            close(pipefd[1]);
        }
        return pid;
    }

    if (fd) {
        _dup2(writeEnd, STDOUT_FILENO);
        if (err) _dup2(writeEnd, STDERR_FILENO);
    }

    execvpe(pathname, argv, envp);
    PLOGE("execvpe %s", pathname);
    exit(1);
}

int execv_sync(const char *pathname, char *const *argv) {
    int pid, status;
    pid = exec_command_v(0, nullptr, pathname, argv);
    if (pid < 0)
        return pid;
    waitpid(pid, &status, 0);

    if (WIFEXITED(status)) {
        int returned = WEXITSTATUS(status);
        LOGD("%s exited normally with status %d", pathname, returned);
        return returned;
    } else if (WIFSIGNALED(status)) {
        int signum = WTERMSIG(status);
        LOGD("%s exited due to receiving signal %d", pathname, signum);
        return -1;
    } else if (WIFSTOPPED(status)) {
        int signum = WSTOPSIG(status);
        LOGD("%s stopped due to receiving signal %d", pathname, signum);
        return -1;
    } else {
        LOGD("%s something strange just happened", pathname);
        return -1;
    }
}

/*
   fd == NULL -> Ignore output
  *fd < 0     -> Open pipe and set *fd to the read end
  *fd >= 0    -> STDOUT (or STDERR) will be redirected to *fd
  *cb         -> A callback function which runs after fork
*/
static int exec_command_l(int err, int *fd, const char *argv0, va_list argv) {
    // Collect va_list into vector
    std::vector<char *> args;
    args.push_back(strdup(argv0));
    for (char *arg = va_arg(argv, char*); arg; arg = va_arg(argv, char*))
        args.push_back(strdup(arg));
    args.push_back(nullptr);
    return exec_command_v(err, fd, argv0, args.data());
}

int execl_sync(const char *argv0, ...) {
    va_list argv;
    va_start(argv, argv0);
    int pid, status;
    pid = exec_command_l(0, nullptr, argv0, argv);
    va_end(argv);
    if (pid < 0)
        return pid;
    waitpid(pid, &status, 0);

    if (WIFEXITED(status)) {
        int returned = WEXITSTATUS(status);
        LOGD("%s exited normally with status %d", argv0, returned);
        return returned;
    } else if (WIFSIGNALED(status)) {
        int signum = WTERMSIG(status);
        LOGD("%s exited due to receiving signal %d", argv0, signum);
        return -1;
    } else if (WIFSTOPPED(status)) {
        int signum = WSTOPSIG(status);
        LOGD("%s stopped due to receiving signal %d", argv0, signum);
        return -1;
    } else {
        LOGD("%s something strange just happened", argv0);
        return -1;
    }
}

int exec_command(int err, int *fd, const char *argv0, ...) {
    va_list argv;
    va_start(argv, argv0);
    int pid = exec_command_l(err, fd, argv0, argv);
    va_end(argv);
    return pid;
}

int to_int(const char *s) {
    if (!is_num(s))
        return -1;

    return atoi(s);
}

int copyfileat(int src_path_fd, const char *src_path, int dst_path_fd, const char *dst_path) {
    int src_fd;
    int dst_fd;
    struct stat stat_buf{};
    int64_t size_remaining;
    size_t count;
    ssize_t result;

    if ((src_fd = openat(src_path_fd, src_path, O_RDONLY)) == -1)
        return -1;

    if (fstat(src_fd, &stat_buf) == -1)
        return -1;

    dst_fd = openat(dst_path_fd, dst_path, O_WRONLY | O_CREAT | O_TRUNC, stat_buf.st_mode);
    if (dst_fd == -1) {
        close(src_fd);
        return -1;
    }

    size_remaining = stat_buf.st_size;
    for (;;) {
        if (size_remaining > 0x7ffff000)
            count = 0x7ffff000;
        else
            count = static_cast<size_t>(size_remaining);

        result = sendfile(dst_fd, src_fd, nullptr, count);
        if (result == -1) {
            close(src_fd);
            close(dst_fd);
            unlink(dst_path);
            return -1;
        }

        size_remaining -= result;
        if (size_remaining == 0) {
            close(src_fd);
            close(dst_fd);
            return 0;
        }
    }
}

int copyfile(const char *src_path, const char *dst_path) {
    return copyfileat(0, src_path, 0, dst_path);
}

size_t get_adoptable_storage_path(char *path, size_t size) {
    FILE *fp = setmntent("/proc/mounts", "r");
    if (fp == NULL) {
        LOGE("error opening /proc/mounts: %s", strerror(errno));
        return 0;
    }
    size_t len = 0;
    struct mntent *mentry;
    while ((mentry = getmntent(fp)) != NULL) {
        if (strcmp(mentry->mnt_dir, "/storage/emulated") == 0) {
            // mnt_fsname should be /mnt/expand/<id>/media
            char tmp[PATH_MAX];
            if (sscanf(mentry->mnt_fsname, "/mnt/expand/%[^/]/media", tmp) == 1) {
                len = strlen(mentry->mnt_fsname) - 6;
                if (len >= size) {
                    len = size - 1;
                }
                strncpy(path, mentry->mnt_fsname, len);
            }
        }
    }
    endmntent(fp);
    return len;
}

constexpr char ALPHANUM[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
static bool seeded = false;
static std::mt19937 gen;
static std::uniform_int_distribution<int> dist(0, sizeof(ALPHANUM) - 2);

void gen_rand_str(char *buf, int len, bool varlen) {
    if (!seeded) {
        if (access("/dev/urandom", F_OK) != 0)
            mknod("/dev/urandom", 0600 | S_IFCHR, makedev(1, 9));
        int fd = _open("/dev/urandom", O_RDONLY | O_CLOEXEC);
        unsigned seed;
        read(fd, &seed, sizeof(seed));
        gen.seed(seed);
        close(fd);
        seeded = true;
    }
    if (varlen) {
        std::uniform_int_distribution<int> len_dist(len / 2, len);
        len = len_dist(gen);
    }
    for (int i = 0; i < len - 1; ++i)
        buf[i] = ALPHANUM[dist(gen)];
    buf[len - 1] = '\0';
}

int my_strcmp(const char *s1, const char *s2) {
    while (*s1 == *s2++)
        if (*s1++ == 0)
            return (0);
    return (*(unsigned char *) s1 - *(unsigned char *) --s2);
}

int my_memcmp(const void *s1, const void *s2, size_t n) {
    const auto *p1 = static_cast<const unsigned char *>(s1);
    const auto *end1 = p1 + n;
    const auto *p2 = static_cast<const unsigned char *>(s2);
    int d = 0;
    for (;;) {
        if (d || p1 >= end1) break;
        d = (int) *p1++ - (int) *p2++;
        if (d || p1 >= end1) break;
        d = (int) *p1++ - (int) *p2++;
        if (d || p1 >= end1) break;
        d = (int) *p1++ - (int) *p2++;
        if (d || p1 >= end1) break;
        d = (int) *p1++ - (int) *p2++;
    }
    return d;
}

static ssize_t prop_fdgets(char *buf, const size_t size, int fd) {
    ssize_t len = 0;
    buf[0] = '\0';
    while (len < size - 1) {
        ssize_t ret = read(fd, buf + len, 1);
        if (ret < 0)
            return -1;
        if (ret == 0)
            break;
        if (buf[len] == '\0' || buf[len++] == '\n' || buf[len++] == '\r') {
            buf[len] = '\0';
            break;
        }
    }
    buf[len] = '\0';
    buf[size - 1] = '\0';
    return len;
}

int is_dir(const char *path) {
    struct stat path_stat{};
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

intptr_t openAt(intptr_t fd, const char *path, intptr_t flag) {
#if defined(__arm__)
    intptr_t r;
    asm volatile(
#ifndef OPTIMIZE_ASM
    "mov r0, %1\n\t"
    "mov r1, %2\n\t"
    "mov r2, %3\n\t"
#endif

    "mov ip, r7\n\t"
    ".cfi_register r7, ip\n\t"
    "ldr r7, =" STR(__NR_openat) "\n\t"
    "svc #0\n\t"
    "mov r7, ip\n\t"
    ".cfi_restore r7\n\t"

#ifndef OPTIMIZE_ASM
    "mov %0, r0\n\t"
#endif
    : "=r" (r)
    : "r" (fd), "r" (path), "r" (flag));
    return r;
#elif defined(__aarch64__)
    intptr_t r;
    asm volatile(
#ifndef OPTIMIZE_ASM
    "mov x0, %1\n\t"
    "mov x1, %2\n\t"
    "mov x2, %3\n\t"
#endif

    "mov x8, " STR(__NR_openat) "\n\t"
    "svc #0\n\t"

#ifndef OPTIMIZE_ASM
    "mov %0, x0\n\t"
#endif
    : "=r" (r)
    : "r" (fd), "r" (path), "r" (flag));
    return r;
#else
    return (intptr_t) syscall(__NR_openat, fd, path, flag);
#endif
}

ssize_t read_file(const char *file, char *buf, size_t size) {
    int fd = open(file, O_RDONLY);
    if (fd != -1) {
        auto res = fdgets(buf, size, fd);
        close(fd);
        return res;
    }
    return -1;
}

ssize_t write_file(const char *file, const char *buf, size_t size, mode_t modes) {
    int fd = open(file, O_WRONLY | O_CREAT | O_TRUNC, modes);
    if (fd != -1) {
        auto res = write(fd, buf, size);
        close(fd);
        return res;
    }
    return -1;
}

int daemon_thread(thread_function func, void *arg, const pthread_attr_t *attr) {
    pthread_t thread;
    int res = pthread_create(&thread, attr, func, arg);
    if (res == 0)pthread_detach(thread);
    return res;
}

struct thread_function_wrapper {
    void_func *func;
};

static void *thread_wrapper(void *arg) {
    ((thread_function_wrapper *) arg)->func();
    free(arg);
    return nullptr;
}

void daemon_thread(void_func *func) {
    auto wrapper = (thread_function_wrapper *) malloc(sizeof(thread_function_wrapper));
    wrapper->func = func;
    daemon_thread(thread_wrapper, wrapper);
}

static ssize_t read_eintr(int fd, void *out, size_t len) {
    ssize_t ret;
    do {
        ret = read(fd, out, len);
    } while (ret < 0 && errno == EINTR);
    return ret;
}

int read_full(int fd, void *out, size_t len) {
    while (len > 0) {
        ssize_t ret = read_eintr(fd, out, len);
        if (ret <= 0) {
            return -1;
        }
        out = (void *) ((uintptr_t) out + ret);
        len -= ret;
    }
    return 0;
}

int write_full(int fd, const void *buf, size_t count) {
    while (count > 0) {
        ssize_t size = _write(fd, buf, count < SSIZE_MAX ? count : SSIZE_MAX);
        if (size <= 0) {
            if (errno == EINTR)
                continue;
            else
                return -1;
        }
        buf = (const void *) ((uintptr_t) buf + size);
        count -= size;
    }
    return 0;
}

int chown_recursive(const char *_path, size_t offset, uid_t uid, gid_t gid) {
    auto path = strdup(_path);
    for (auto *p = path + offset;; ++p) {
        if (*p != '/' && *p != '\0') continue;
        auto end = (*p == '\0');
        *p = '\0';

        LOGD("chown %s %d %d", path, uid, gid);
        _chown(path, uid, gid);

        if (end) break;
        *p = '/';
    }
    free(path);
    return errno == 0 ? 0 : -1;
}