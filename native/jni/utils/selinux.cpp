#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <syscall.h>
#include <malloc.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dlfcn.h>

static int __setcon(const char *ctx) {
    int fd = open("/proc/self/attr/current", O_WRONLY | O_CLOEXEC);
    if (fd < 0)
        return fd;
    size_t len = strlen(ctx) + 1;
    int rc = write(fd, ctx, len);
    close(fd);
    return rc != len;
}

static int __getcon(char **context) {
    int fd = open("/proc/self/attr/current", O_RDONLY | O_CLOEXEC);
    if (fd < 0)
        return fd;

    char *buf;
    size_t size;
    int errno_hold;
    ssize_t ret;

    size = sysconf(_SC_PAGE_SIZE);
    buf = (char *) malloc(size);
    if (!buf) {
        ret = -1;
        goto out;
    }
    memset(buf, 0, size);

    do {
        ret = read(fd, buf, size - 1);
    } while (ret < 0 && errno == EINTR);
    if (ret < 0)
        goto out2;

    if (ret == 0) {
        *context = nullptr;
        goto out2;
    }

    *context = strdup(buf);
    if (!(*context)) {
        ret = -1;
        goto out2;
    }
    ret = 0;
    out2:
    free(buf);
    out:
    errno_hold = errno;
    close(fd);
    errno = errno_hold;
    return 0;
}

static int __getfilecon(const char *path, char **con) {
    char buf[1024];
    int rc = syscall(__NR_getxattr, path, "security.selinux", buf, sizeof(buf) - 1);
    if (rc < 0) {
        errno = -rc;
        return -1;
    }
    *con = strdup(buf);
    return rc;
}

int __setfilecon(const char *path, const char *con) {
    int rc = syscall(__NR_setxattr, path, "security.selinux", con, strlen(con) + 1, 0);
    if (rc) {
        errno = -rc;
        return -1;
    }
    return 0;
}

static int __lsetfilecon(const char *path, const char *con) {
    int rc = syscall(__NR_lsetxattr, path, "security.selinux", con, strlen(con) + 1, 0);
    if (rc) {
        errno = -rc;
        return -1;
    }
    return 0;
}

static void __freecon(char *con) {
    free(con);
}

static int __setsockcreatecon(const char *ctx) {
    int fd = open("/proc/thread-self/attr/sockcreate", O_RDWR | O_CLOEXEC);
    if (fd < 0)
        return fd;
    ssize_t rc;
    if (ctx) {
        do {
            rc = write(fd, ctx, strlen(ctx) + 1);
        } while (rc < 0 && errno == EINTR);
    } else {
        do {
            rc = write(fd, nullptr, 0);    /* clear */
        } while (rc < 0 && errno == EINTR);
    }
    close(fd);
    return rc == -1 ? -1 : 0;
}

static int stub(const char * scon, const char * tcon, const char *tclass, const char *perm, void *auditdata) {
    return 0;
}

int (*getcon)(char **con) = __getcon;
void (*freecon)(char *) = __freecon;
int (*getfilecon)(const char *, char **) = __getfilecon;
int (*setfilecon)(const char *, const char *) = __setfilecon;
int (*lsetfilecon)(const char *, const char *) = __lsetfilecon;
int (*selinux_check_access)(const char *, const char *, const char *, const char *, void *) = stub;

static void selinux_builtin_impl() {
}

void dload_selinux() {
#ifdef __LP64__
    if (access("/system/lib64/libselinux.so", F_OK) != 0) {
#else
    if (access("/system/lib/libselinux.so", F_OK) != 0) {
#endif
    return;
    }

    selinux_builtin_impl();

#ifdef __LP64__
    void *handle = dlopen("/system/lib64/libselinux.so", RTLD_LAZY | RTLD_LOCAL);
#else
    void *handle = dlopen("/system/lib/libselinux.so", RTLD_LAZY | RTLD_LOCAL);
#endif
    selinux_check_access = (int (*)(const char *, const char *, const char *, const char *, void *)) (dlsym(handle, "selinux_check_access"));
}