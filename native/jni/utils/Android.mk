LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := utils
LOCAL_SRC_FILES := misc.cpp \
    wrap.cpp \
    socket.cpp \
    tinynew.cpp
LOCAL_C_INCLUDES := jni/include

ifeq ($(NDK_DEBUG),1)
LOCAL_CFLAGS += -DDEBUG -DNO_LICENSE_CHECK
else
LOCAL_CFLAGS += -DNO_DEBUG_LOG
endif

ifeq ($(TOOLCHAIN_LLVM_HIKARI),1)
LOCAL_CFLAGS += -mllvm -enable-bcfobf -mllvm -enable-cffobf -mllvm -enable-strcry
endif

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := selinux
LOCAL_SRC_FILES := selinux.cpp
LOCAL_C_INCLUDES := jni/include
include $(BUILD_STATIC_LIBRARY)