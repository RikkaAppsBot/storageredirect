#include <sys/system_properties.h>
#include <cstring>
#include <climits>
#include <fcntl.h>
#include <misc.h>
#include <unistd.h>

namespace android {

    int GetApiLevel() {
        static int apiLevel = 0;
        if (apiLevel > 0) return apiLevel;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("ro.build.version.sdk", buf) > 0)
            apiLevel = atoi(buf);

        return apiLevel;
    }

    int GetPreviewApiLevel() {
        static int previewApiLevel = 0;
        if (previewApiLevel > 0) return previewApiLevel;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("ro.build.version.preview_sdk", buf) > 0)
            previewApiLevel = atoi(buf);

        return previewApiLevel;
    }

    bool IsFuse() {
        if (GetApiLevel() < 30) return false;

        static bool res = false;
        static bool init = false;
        if (init) return res;
        init = true;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("persist.sys.fuse", buf) > 0)
            res = strcmp("true", buf) == 0;

        return res;
    }

    bool IsFilesystemSupported(const char *name) {
        int fd = open("/proc/filesystems", O_RDONLY);
        if (fd == -1) return false;

        bool res = false;
        char buf[PATH_MAX];
        while (fdgets(buf, PATH_MAX, fd) > 0) {
            if (strstr(buf, name)) {
                res = true;
                break;
            }
        }
        close(fd);
        return res;
    }

    bool IsSdcardfsUsed() {
        if (GetApiLevel() < 30) return true;

        static bool res = false;
        static bool init = false;
        if (init) return res;
        init = true;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("external_storage.sdcardfs.enabled", buf) <= 0)
            strcpy(buf, "true");

        res = IsFilesystemSupported("sdcardfs") && strcmp("true", buf) == 0;
        return res;
    }
}