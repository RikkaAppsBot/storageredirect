#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <misc.h>
#include <wrap.h>
#include <logging.h>
#include <private/ScopedFd.h>

namespace cgroup {
    int get_cgroup(int pid, int *cuid, int *cpid) {
        char buf[PATH_MAX];
        snprintf(buf, PATH_MAX, "/proc/%d/cgroup", pid);

        ScopedFd fd(_open(buf, O_RDONLY));
        if (fd.get() == -1)
            return -1;

        while (fdgets(buf, PATH_MAX, fd.get()) > 0) {
            if (sscanf(buf, "%*d:cpuacct:/uid_%d/pid_%d", cuid, cpid) == 2
                || sscanf(buf, "%*d::/uid_%d/pid_%d", cuid, cpid) == 2) {
                return 0;
            }
        }
        return -1;
    }

    static int switch_cgroup(int pid, int cuid, int cpid, const char *name) {
        char buf[PATH_MAX];
        if (cuid != -1 && cpid != -1) {
            snprintf(buf, PATH_MAX, "/acct/uid_%d/pid_%d/%s", cuid, cpid, name);
        } else {
            snprintf(buf, PATH_MAX, "/acct/%s", name);
        }

        if (access(buf, F_OK) != 0) {
            if (cuid != -1 && cpid != -1) {
                snprintf(buf, PATH_MAX, "/sys/fs/cgroup/uid_%d/pid_%d/%s", cuid, cpid, name);
            } else {
                snprintf(buf, PATH_MAX, "/sys/fs/cgroup/%s", name);
            }
        }

        ScopedFd fd( _open(buf, O_WRONLY | O_APPEND));
        if (fd.get() == -1)
            return -1;

        snprintf(buf, PATH_MAX, "%d\n", pid);
        if (_write(fd.get(), buf, strlen(buf)) == -1) {
            return -1;
        }

        return 0;
    }

    int switch_cgroup(int pid, int cuid, int cpid) {
        int res = 0;
        res += switch_cgroup(pid, cuid, cpid, "cgroup.procs");
        res += switch_cgroup(pid, cuid, cpid, "tasks");
        return res;
    }

}