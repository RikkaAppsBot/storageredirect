#include <cstdlib>
#include <unistd.h>
#include <sys/system_properties.h>
#include <wrap.h>
#include <fcntl.h>
#include <fstream>
#include <logging.h>
#include <version.h>

#include "dex/server_dex.h"
#include "dex_writer.h"
#include "misc.h"

namespace dex_writer {

    static int write_to_file(const char *path, uint8_t *array, size_t size) {
        if (access(path, F_OK) == 0)
            unlink(path);

        int fd = open(path, O_WRONLY | O_CREAT, 0700);
        if (fd == -1)
            return 1;

        uint32_t n = 0;
        for (size_t i = 0; i < size; i++) {
            array[i] ^= ((uint8_t) ((n % 33) + n));
            ++n;
        }

        int res = write_full(fd, array, size);
        close(fd);
        _chmod(path, 0700);
        return res;
    }

    int write_dex() __attribute((__annotate__(("nostrenc"))));

    int write_dex() {
        return write_to_file("/data/adb/storage-isolation/bin/server-v" VERSION_NAME ".dex", server_dex_array, server_dex_length);
    }
}