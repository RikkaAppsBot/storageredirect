#include <ctime>
#include <cstdlib>
#include <misc.h>
#include <logging.h>
#include <sys/socket.h>
#include <zconf.h>
#include <linux/un.h>
#include <flatbuffers/flatbuffers.h>
#include <socket.h>
#include <config.h>
#include <wait.h>
#include "misc.h"
#include "flatbuffers/request_header_generated.h"
#include "flatbuffers/request_check_process_generated.h"
#include "flatbuffers/reply_check_process_generated.h"
#include "isolation.h"
#include "license.h"
#include "request_handler.h"
#include "setproctitle.h"

namespace RequestHandler {

    static char socket_addr[17] = {0};
    static char token[32];
    static std::vector<pid_t> child_pids;

#define ACTION_REQUEST_CHECK_PROCESS 1

    static void find_unused_address() {
        char buf[PATH_MAX];
        int fd;
        struct sockaddr_un addr{};

        while (true) {
            gen_rand_str(socket_addr, sizeof(socket_addr) - 1);

            // /dev/<socket_addr> is used to check invalid license
            snprintf(buf, PATH_MAX, "/dev/%s", socket_addr);
            if (access(socket_addr, F_OK) == 0) continue;

            LOGI("handler socket name is %s", socket_addr);

            if ((fd = socket(PF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0)) < 0) {
                PLOGE("socket");
                sleep(1);
                continue;
            }

            socklen_t socklen = setup_sockaddr(&addr, socket_addr);
            if (bind(fd, (sockaddr *) (&addr), socklen) == 0)
                break;

            PLOGE("bind %s", socket_addr);
            close(fd);

            sleep(1);
        }

        close(fd);
        LOGI("find socket is %s", socket_addr);
    }

    static void handle_socket(int fd) {
        struct ucred cred{};
        int32_t size;
        uint8_t *buf;
        uint8_t action;

        if (get_client_cred(fd, &cred) == 0) {
            if (cred.uid != 0) {
                LOGE("not from uid 0");
                return;
            }
        }

        if (read_full(fd, &size, sizeof(size)) == -1) {
            PLOGE("read");
            return;
        }

        LOGD("size %d", size);
        if (size <= 0) return;

        buf = static_cast<uint8_t *>(malloc(size));
        if (read_full(fd, buf, size) == -1) {
            PLOGE("read");
            return;
        }

        auto headerVerifier = flatbuffers::Verifier(buf, (size_t) size);
        if (!VerifyRequestHeaderBuffer(headerVerifier)) {
            LOGD("invalid header");
            return;
        }

        auto header = GetRequestHeader(buf);
        action = header->action();
        LOGD("header: %d %s", header->action(), header->token()->c_str());

        if (strncmp(token, header->token()->c_str(), 32) != 0) {
            LOGD("invalid token");
            return;
        }

        switch (action) {
            case ACTION_REQUEST_CHECK_PROCESS: {
                if (read_full(fd, &size, sizeof(size)) == -1) {
                    PLOGE("read");
                    return;
                }
                LOGD("size %d", size);
                if (size <= 0) return;

                // read body
                buf = static_cast<uint8_t *>(realloc(buf, size));
                if (read_full(fd, buf, size) == -1) {
                    PLOGE("read");
                    return;
                }

                auto verifier = flatbuffers::Verifier(buf, (size_t) size);
                if (!VerifyCheckProcessRequestBuffer(verifier)) {
                    LOGD("invalid body");
                    return;
                }

                auto body = GetCheckProcessRequest(buf);
                LOGD("body: %d %s -> %s", body->pid(), body->pkg()->c_str(), body->target()->c_str());

                // do isolation
                Isolation::handleProcess(body);

                // write reply
                flatbuffers::FlatBufferBuilder builder(8);
                auto l = CreateCheckProcessReply(builder, License::code());
                FinishCheckProcessReplyBuffer(builder, l);

                size = builder.GetSize();

                LOGD("size: %d", size);

                write_full(fd, &size, sizeof(size));
                write_full(fd, builder.GetBufferPointer(), size);
                break;
            }
        }

        if (buf) free(buf);

        LOGD("handle socket %d finished", fd);
    }

    [[noreturn]] static void handler_thread() {
        // Ignore SIGPIPE
        struct sigaction act{};
        memset(&act, 0, sizeof(act));
        act.sa_handler = SIG_IGN;
        sigaction(SIGPIPE, &act, nullptr);

        int fd, clifd;
        struct sockaddr_un addr{};

        while (true) {
            if ((fd = socket(PF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0)) == -1) {
                PLOGE("socket");
                sleep(1);
                continue;
            }

            socklen_t socklen = setup_sockaddr(&addr, socket_addr);
            if (bind(fd, (sockaddr *) (&addr), socklen) == -1) {
                PLOGE("bind %s", SOCKET_SERVER_PROCESS_ADDRESS);
                sleep(1);
                continue;
            }
            LOGI("socket %s created", socket_addr);

            if (listen(fd, 10) == -1) {
                PLOGE("listen");
                sleep(1);
                continue;
            }

            while (true) {
                clifd = accept4(fd, nullptr, nullptr, SOCK_CLOEXEC);
                if (clifd == -1) {
                    PLOGE("accept");
                    break;
                }

                char buf[PATH_MAX];
                snprintf(buf, PATH_MAX, "/dev/%s", RequestHandler::getSocketAddress());
                if (access(buf, F_OK) == 0) {
                    unlink(buf);
                    License::SetInvalidLicense(License::INVALID_LICENSE);
                }

                auto pid = fork();
                if (pid == 0) {
                    setproctitle(PROCESS_NAME_WORKER);
                    handle_socket(clifd);
                    close(clifd);
                    exit(0);
                } else {
                    if (pid != -1) {
                        child_pids.emplace_back(pid);
                    }
                    close(clifd);
                }
            }

            LOGI("close");
            close(fd);
        }
    }

    const char *getSocketToken() {
        return token;
    }

    const char *getSocketAddress() {
        return socket_addr;
    }

    static void sigchild_handler(int) {
        int status;
        pid_t pid;
        auto it = child_pids.begin();
        while (it != child_pids.end()) {
            pid = *it;
            if (pid == waitpid(pid, &status, WNOHANG)) {
                if (WIFEXITED(status)) {
                    int returned = WEXITSTATUS(status);
                    LOGD("%d exited normally with status %d", pid, returned);
                } else if (WIFSIGNALED(status)) {
                    int signum = WTERMSIG(status);
                    LOGD("%d exited due to receiving signal %d", pid, signum);
                } else if (WIFSTOPPED(status)) {
                    int signum = WSTOPSIG(status);
                    LOGD("%d stopped due to receiving signal %d", pid, signum);
                } else {
                    LOGD("%d something strange just happened", pid);
                }

                it = child_pids.erase(it);
            } else ++it;
        }
    }

    void start() {
        signal(SIGCHLD, sigchild_handler);

        gen_rand_str(token, 32);

        find_unused_address();
        daemon_thread(handler_thread);
    }
}