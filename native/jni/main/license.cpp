#include <string>
#include <regex>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/system_properties.h>
#include <sys/inotify.h>
#include <sys/wait.h>
#include <sstream>

#ifndef DEBUG
#define NO_LOG
#endif

#include <config.h>
#include <logging.h>
#include <misc.h>
#include <wrap.h>
#include <list>
#include <signing/signing_block.h>

#include "isolation.h"
#include "license.h"
#include "misc.h"
#include "request_handler.h"

static int license_status = 10;
constexpr int divisor = 10000;

namespace License {

    void SetInvalidLicense(int reason) {
        LOGD("invalid license %d", reason);
        license_status = reason * divisor;
    }

    bool valid() {
        // valid value is 10, 10 % 10000 = 10
        return license_status % divisor >= 8;
    }

    int code() {
        return license_status / divisor;
    }

    static inline ssize_t get_apk_path(char *buf, size_t size) {
        ssize_t res;
        int fd = -1, status;
        int pid = exec_command(0, &fd, "pm", "path", "moe.shizuku.redirectstorage", NULL);

        res = fdgets(buf, size, fd);

        waitpid(pid, &status, 0);
        close(fd);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
            return res;

        return -1;
    }

    static inline int check_signature() {
        LOGI("check signature");

        char path[1024] = {0};
        while (path[0] == '\0') {
            get_apk_path(path, 1024);
            sleep(10);
        }
        char *_path = trim(path + 8);
        LOGD("path=%s", _path);
        if (parse_signing_block(_path) == 1) {
            SetInvalidLicense(License::INVALID_BAD_APK);
            return 1;
        }
        return 0;
    }

    static void start_check() {
        check_signature();
    }

    void daemon() {
        static int started = 0;
        if (started) {
            return;
        }
        started = 1;

        daemon_thread(start_check);
    }
}

