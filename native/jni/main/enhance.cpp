#include <cstring>
#include <sys/system_properties.h>
#include <logging.h>
#include <misc.h>
#include "enhance.h"
#include "misc.h"

extern "C" {
#include "pmparser.h"
}

static int read_versions_from_files(const char *version_file, int &version_code, const char *version_name_file, char *version_name) {
    LOGD("%s", version_file);
    LOGD("%s", version_name_file);

    version_code = Enhance::MODULE_VERSION_NOT_FOUND;

    char buf[4096];

    if (read_file(version_file, buf, 4096) > 0) {
        if (is_num(buf)) {
            version_code = atoi(buf);
            LOGI("read version code '%d' from %s", version_code, version_file);
        }
    }
    if (read_file(version_name_file, buf, 4096) > 0) {
        strncpy(version_name, buf, 64);
        LOGI("read version name '%s' from %s", version_name, version_name_file);
    }
    return 0;
}

static bool check_riru_enhance_module_impl(
        int has32bit, int has64bit, int &riru_version, char *riru_version_name, int &module_version, char *module_version_name) {

    bool res;

    static const char* zygote_name;
    static pid_t zygote_pid = -1;
    auto check_zygote_func = [](pid_t pid) -> bool {
        if (pid == getpid()) return false;

        char buf[64];
        snprintf(buf, 64, "/proc/%d/cmdline", pid);

        int fd = open(buf, O_RDONLY);
        if (fd > 0) {
            memset(buf, 0, 64);
            if (read(fd, buf, 64) > 0 && strcmp(zygote_name, buf) == 0) {
                zygote_pid = pid;
            }
            close(fd);
        }
        return zygote_pid != -1;
    };

    if (has32bit) {
        LOGD("waiting for zygote");
        zygote_name = "zygote";
        while (true) {
            foreach_proc(check_zygote_func);
            if (zygote_pid != -1) break;
            sleep(1);
        }
        LOGD("found zygote");
    }

    if (has64bit) {
        LOGD("waiting for zygote64");
        zygote_name = "zygote64";
        while (true) {
            foreach_proc(check_zygote_func);
            if (zygote_pid != -1) break;
            sleep(1);
        }
        LOGD("found zygote64");
    }

    size_t size;
    char dev_path[PATH_MAX];
    strcpy(dev_path, "/dev");
#ifdef __LP64
    strcat(dev_path, "/riru64_");
#else
    strcat(dev_path, "/riru_");
#endif

    char *tmp = dev_path + strlen(dev_path);

    int fd = open("/data/adb/riru/dev_random", O_RDONLY);
    if (fd == -1) {
        LOGD("/data/adb/riru/dev_random not exists");
        return false;
    }
    if ((size = read(fd, tmp, 4096)) > 0) {
        tmp[size] = '\0';
    }
    close(fd);
    LOGD("path is %s", dev_path);

    res = access(dev_path, F_OK) == 0;
    if (res) {
        LOGI("found Riru v22+");

        char version_file[PATH_MAX], version_name_file[PATH_MAX];
        strcpy(version_file, dev_path);
        strcat(version_file, "/version");
        strcpy(version_name_file, dev_path);
        strcat(version_name_file, "/version_name");

        read_versions_from_files(version_file, riru_version, version_name_file, riru_version_name);
    }

    auto end = dev_path + strlen(dev_path);

    strcat(dev_path, "/modules/riru_storage_redirect@storage_redirect");
    if (access(dev_path, F_OK) != 0) {
        *end = 0;
        strcat(dev_path, "/modules/storage_redirect");
    }

    res &= access(dev_path, F_OK) == 0;
    if (res) {
        LOGI("found Riru v22+");

        char version_file[PATH_MAX], version_name_file[PATH_MAX];
        strcpy(version_file, dev_path);
        strcat(version_file, "/version");
        strcpy(version_name_file, dev_path);
        strcat(version_name_file, "/version_name");

        read_versions_from_files(version_file, module_version, version_name_file, module_version_name);
    }

    return res;
}


namespace Enhance {

    static char module_version_name[64] = {0}, riru_version_name[64] = {0};
    static int module_version = MODULE_NOT_INSTALLED, riru_version = MODULE_NOT_INSTALLED;

    void waitForZygote(bool forever) {
        char abilist64[PROP_VALUE_MAX + 1];
        int has32bit = __system_property_get("ro.product.cpu.abilist32", abilist64) > 0;
        int has64bit = __system_property_get("ro.product.cpu.abilist64", abilist64) > 0;

        while (true) {
            if (check_riru_enhance_module_impl(has32bit, has64bit, riru_version, riru_version_name, module_version, module_version_name)) {
                LOGI("Riru v22+");
            }

            if (!forever || module_version > 0) break;

            sleep(1);
        }

        LOGI("Riru version is %d", riru_version);
        LOGI("module version is %d", module_version);
    }

    int getRiruVersion() {
        return riru_version;
    }

    const char *getRiruVersionName() {
        return riru_version_name;
    }

    int getModuleVersion() {
        return module_version;
    }

    const char *getModuleVersionName() {
        return module_version_name;
    }
}