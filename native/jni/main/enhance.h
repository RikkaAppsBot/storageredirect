#pragma once

namespace Enhance {

    const int MODULE_NOT_INSTALLED = -1;
    const int MODULE_NO_64BIT_LIB = -2;
    const int MODULE_NOT_IN_MEMORY = -3;
    const int MODULE_NOT_IN_64_MEMORY = -4;
    const int MODULE_VERSION_NOT_FOUND = -5;

    const int V23_0 = 47;

    void waitForZygote(bool forever);

    int getRiruVersion();

    const char *getRiruVersionName();

    int getModuleVersion();

    const char *getModuleVersionName();
}