#undef LOG_TAG
#define LOG_TAG "LegacyProcessMonitor"

#include <string>
#include <sstream>
#include <unistd.h>
#include <wait.h>
#include <chrono>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <fcntl.h>

#include <config.h>
#include <logging.h>
#include <misc.h>
#include <wrap.h>
#include <socket.h>
#include "moe_shizuku_storageredirect_server_NativeHelper.h"

static int handle_log(char *log) {
    int user, pid, uid;
    char *type, *package;

    char *start = strchr(log, '[');
    char *end = strchr(log, ']');
    if (!start || !end || end < start)
        return 1;

    size_t size = end - start - 1;
    char ss[512];
    strncpy(ss, start + 1, size);
    ss[size] = '\0';

    // am_proc_start (User|1|5),(PID|1|5),(UID|1|5),(Process Name|3),(Type|3),(Component|3)

    std::vector<std::string> tokens;

    char *s = ss;
    while (auto next = strchr(s, ',')) {
        tokens.emplace_back(s, next);
        s = next + 1;
    }
    tokens.emplace_back(s);

    if (tokens.size() != 6)
        return 1;

    user = to_int(tokens[0].c_str());
    pid = to_int(tokens[1].c_str());
    uid = to_int(tokens[2].c_str());
    char *_type = strdup(tokens[4].c_str());
    char *_package = strdup(tokens[5].c_str());

    type = strdup(trim(_type));
    package = strdup(trim(_package));

    free(_type);
    free(_package);

    if (package[0] == '{' && package[strlen(package) - 1] == '}') {
        _package = strndup(package + 1, strlen(package) - 2);
        package = strdup(_package);
        free(_package);
    }

    int res;
    while (true) {
        if (user == -1 || pid == -1 || uid == -1) {
            res = 1;
            break;
        }

        if (strlen(package) == 0) {
            // read uid link for normal app only
            int appId = uid % 100000;
            if (appId < 10000 || appId > 19999) {
                LOGI("no package name from log and appId %d is not regular, skip", appId);
                res = 0;
                break;
            }
        } else {
            char *pos = strchr(package, '/');
            if (pos)
                *pos = '\0';
        }

        if (!package || strlen(package) == 0) {
            res = 1;
            LOGW("can't find package name");
            break;
        }

        LOGI("process started (logcat): pid=%d, uid=%d, user=%d, package=%s, type=%s",
             pid, uid, user, package, type ? type : "(null)");

        // skip webview process
        if (type && strcmp("webview_service", type) == 0) {
            LOGV("skip webview process");
            res = 0;
            break;
        }

        // skip non-regular
        int appId = uid % 100000;
        if (appId < 10000 || appId > 19999) {
            LOGV("non regular app is not supported on logcat mode");
            res = 0;
            break;
        }

        JNI::NativeHelper::onProcessStarted(pid, uid, package);

        res = 0;
        break;
    }

    free(type);
    free(package);
    return res;
}

static int log_parsed = 0;
static int logger_from_app = 0;

[[noreturn]] static void logger_thread() {
    int from_app = logger_from_app;

    LOGI("starting logcat...");

    ssize_t size;
    int log_fd = -1, log_pid;
    char buf[1024];

    while (true) {
        if (from_app)
            execl_sync("logcat", "-b", nullptr);

        // Start logcat
        log_pid = exec_command(0, &log_fd, "logcat", "-b", "events", "-v", "threadtime", "-s", "am_proc_start", nullptr);
        while ((size = fdgets(buf, sizeof(buf), log_fd))) {
            if (size > 0) {
                if (!log_parsed) {
                    JNI::NativeHelper::onLogcatStatusChangedMethod(true);
                    LOGI("am_proc_start log detected");
                }

                log_parsed = 1;
            }

            if (handle_log(buf) != 0)
                LOGW("failed to parse log: %s", buf);
            else
                LOGV("log: %s", buf);

            if (kill(log_pid, 0))
                break;
        }

        // Cleanup
        close(log_fd);
        log_fd = -1;
        kill(log_pid, SIGTERM);
        waitpid(log_pid, nullptr, 0);

        if (!from_app)
            execl_sync("logcat", "-b", nullptr);
    }
}

[[noreturn]] static void no_log_thread() {
    int next_wait = 60 * 60;
    while (true) {
        log_parsed = 0;

        sleep(next_wait);

        if (!log_parsed) {
            JNI::NativeHelper::onLogcatStatusChangedMethod(false);
        } else {
            LOGI("check log in %ds", next_wait);
        }
    }
}

static void start_logcat(int from_app) {
    logger_from_app = from_app;
    daemon_thread(logger_thread);
    daemon_thread(no_log_thread);
}

namespace ProcessMonitor {

    void start(bool has_module, bool from_app) {
        LOGI("use legacy socket for process monitor");

        if (!has_module) {
            LOGI("use legacy logcat");
            start_logcat(from_app);
        }
    }
}

