#pragma once

#include <stdint.h>
#include <config.h>

#define LICENSE_TYPE_GOOGLE 0
#define LICENSE_TYPE_ALIPAY 1
#define LICENSE_TYPE_REDEEM 2
#define LICENSE_TYPE_INSTANCE_ID 3
#define LICENSE_MAX_TYPES 3

#define STATUS_UNKNOWN 0
#define STATUS_INVALID 1
#define STATUS_VERIFIED_ONCE 2
#define STATUS_VERIFIED 3

#define INVALID_VALUE "-1"

#define LICENSE_FILE BIN_PATH "/local.license"
#define LICENSE_FILE_VERSION 1

namespace License {

    struct Status {

        char *instance_id = nullptr;
        char *tokens[LICENSE_MAX_TYPES] = {};
        uint8_t status[LICENSE_MAX_TYPES] = {STATUS_UNKNOWN};
    };

    bool VerifyLocal(int type, const char *value);

    void Init(const char *invalid_file);

    bool IsNoValidLicense();

    void Update(int type, const char *value);
}