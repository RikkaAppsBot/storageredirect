#pragma once

#include <jni.h>
#include <sys/types.h>

namespace JNI::NativeHelper {

    jint registerNative(JNIEnv *env);

    void onProcessStarted(pid_t pid, uid_t uid, const char *packageName);

    void onLogcatStatusChangedMethod(bool hasLog);
}
