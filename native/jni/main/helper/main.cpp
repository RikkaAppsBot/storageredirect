#include <jni.h>
#include "moe_shizuku_storageredirect_server_NativeHelper.h"
#include "license.h"

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env = nullptr;

    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) return JNI_ERR;
    if (JNI::NativeHelper::registerNative(env) != JNI_OK) return JNI_ERR;
    return JNI_VERSION_1_6;
}