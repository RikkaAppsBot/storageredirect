#pragma once

#include <stdint.h>
#include <config.h>
#include "license.h"

namespace License {

    bool ReadFromFile(Status &output);

    void WriteToFile(const Status &input);
}