#undef LOG_TAG
#define LOG_TAG "License"

#include <config.h>
#include <obfuscate.h>
#include <logging.h>
#include <list>
#include <sys/file.h>
#include <sys/system_properties.h>
#include <curl/curl.h>
#include <openssl/nid.h>
#include <openssl/ssl.h>
#include <nlohmann/json.hpp>
#include <misc.h>
#include <unistd.h>

#include "license.h"
#include "license_file.h"

static const char *invalidFile;
static License::Status memoryStatus;

bool License::VerifyLocal(int type, const char *value) {
    if (!value) return false;

    auto len = strlen(value);
    if (len == 0
        || strncmp("Cracked", value, 7) == 0
        || strcmp(INVALID_VALUE, value) == 0) {
        return false;
    }

    if (type == LICENSE_TYPE_GOOGLE) return len > 24;
    return len > 0;
}

static inline void WriteInvalidFile() {
    // /dev/<socket_addr> is used to check invalid license
    char buf[PATH_MAX];
    snprintf(buf, PATH_MAX, "/dev/%s", invalidFile);
    creat(buf, 0700);

    LOGD("write invalid file %s", buf);
}

namespace License::Network {

    static auto url_google = OBF("https://api-secure.rikka.app/v1/purchases/moe.shizuku.redirectstorage/unlock/google/");
    static auto url_alipay = OBF("https://api-secure.rikka.app/v1/alipay/moe.shizuku.redirectstorage/unlock/");
    static auto url_redeem = OBF("https://api-secure.rikka.app/v1/purchases/moe.shizuku.redirectstorage/unlock/redeem/");

    using json = nlohmann::json;

    const char *mypem[] = {
/*#include "cert/cacert_ISRG_Root_X1"

            ,

#include "cert/cacert_DST_Root_CA_X3"*/
#include "cert/cacert_Rikka"
    };

    static CURLcode SSLCtxFunction(CURL *curl, void *sslctx, void *parm) {
        CURLcode rv = CURLE_ABORTED_BY_CALLBACK;
        X509_STORE *store;
        BIO *bio;

        /* get a pointer to the X509 certificate store (which may be empty!) */
        store = SSL_CTX_get_cert_store((SSL_CTX *) sslctx);
        if (!store)
            return rv;

        for (auto &pem : mypem) {
            X509 *cert = nullptr;
            ERR_clear_error();
            bio = BIO_new_mem_buf(pem, -1);
            if (!bio)
                continue;

            if (!PEM_read_bio_X509(bio, &cert, nullptr, nullptr))
                continue;

            if (!X509_STORE_add_cert(store, cert)) {
                unsigned long error = ERR_peek_last_error();
                if (ERR_GET_LIB(error) != ERR_LIB_X509 ||
                    ERR_GET_REASON(error) != X509_R_CERT_ALREADY_IN_HASH_TABLE)
                    continue;

                ERR_clear_error();
            }

            rv = CURLE_OK;

            X509_free(cert);
            BIO_free(bio);
            ERR_clear_error();
        }

        return rv;
    }

    struct MemoryStruct {
        char *memory;
        size_t size;
    };

    static size_t WriteFunction(void *contents, size_t size, size_t nmemb, void *userp) {
        size_t realsize = size * nmemb;
        auto *mem = (struct MemoryStruct *) userp;

        char *ptr = (char *) realloc(mem->memory, mem->size + realsize + 1);
        if (ptr == nullptr) {
            printf("not enough memory (realloc returned NULL)\n");
            return 0;
        }

        mem->memory = ptr;
        memcpy(&(mem->memory[mem->size]), contents, realsize);
        mem->size += realsize;
        mem->memory[mem->size] = 0;

        return realsize;
    }

    static CURLcode CurlGlobalInit() {
        static auto res = curl_global_init(CURL_GLOBAL_SSL);
        return res;
    }

    static CURLcode DoRequest(long *resp_code, std::string &url, std::list<std::string> &headers,
                              MemoryStruct *writedata) {
        CURLcode res;
        CurlGlobalInit();
        auto curl = curl_easy_init();
        if (curl == nullptr) {
            return CURLE_FAILED_INIT;
        }
        curl_easy_setopt(curl, CURLOPT_VERBOSE, false);
        curl_easy_setopt(curl, CURLOPT_HEADER, false);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, true);
        curl_easy_setopt(curl, CURLOPT_NOSIGNAL, true);
        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10L);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
        curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, SSLCtxFunction);
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        struct curl_slist *header_list = nullptr;
        header_list = curl_slist_append(header_list, "Accept: */*");
        header_list = curl_slist_append(header_list, "Connection: close");
        for (const auto &header : headers) {
            header_list = curl_slist_append(header_list, header.c_str());
        }
        curl_easy_setopt(curl, CURLOPT_HTTPGET, true);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header_list);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteFunction);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, writedata);
        res = curl_easy_perform(curl);
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, resp_code);
        curl_easy_cleanup(curl);
        return res;
    }

    static void Check(int type) {
        // url
        std::string url;
        if (type == LICENSE_TYPE_GOOGLE) {
            url = std::string(url_google.get()) + memoryStatus.tokens[type];
        } else if (type == LICENSE_TYPE_ALIPAY) {
            if (!memoryStatus.instance_id) return;

            url = std::string(url_alipay.get()) + memoryStatus.tokens[type];
        } else if (type == LICENSE_TYPE_REDEEM) {
            url = std::string(url_redeem.get()) + memoryStatus.tokens[type];
        } else {
            return;
        }
        LOGD("Url: %s", url.c_str());

        // header
        std::list<std::string> headers;

        if (type == LICENSE_TYPE_GOOGLE || type == LICENSE_TYPE_REDEEM) {
            std::string serialno = "SerialNo:";

            char buf[PROP_VALUE_MAX];
            if (__system_property_get("ro.serialno", buf) > 0) {
                headers.emplace_back(serialno + buf);
            }

            if (__system_property_get("ro.product.device", buf) > 0) {
                if (type == LICENSE_TYPE_GOOGLE) {
                    std::string device = "Device:";
                    headers.emplace_back(device + buf);
                } else {
                    std::string deviceId = "DeviceId:";
                    headers.emplace_back(deviceId + buf);
                }
            }
        } else if (type == LICENSE_TYPE_ALIPAY) {
            std::string device_instance_id = "DeviceInstanceId:";
            headers.emplace_back(device_instance_id + memoryStatus.instance_id);

            std::string device_model = "DeviceModel:";
            headers.emplace_back(device_model + "TODO");
        }

        long responseCode = 0;
        MemoryStruct readBuffer{};
        readBuffer.memory = static_cast<char *>(malloc(1));
        readBuffer.size = 0;

        auto res = DoRequest(&responseCode, url, headers, &readBuffer);
        LOGD("Request: %d", res);

        if (res != 0) {
            return;
        }

        LOGD("Response code: %ld", responseCode);

        if (responseCode == 200) {
            std::string body = readBuffer.memory;
            LOGD("Response body: %s", body.c_str());

            json b = json::parse(body, nullptr, false);

            if (!b.is_discarded()) {
                if (type == LICENSE_TYPE_GOOGLE || type == LICENSE_TYPE_REDEEM) {
                    auto code = b["code"].get<int>();
                    LOGD("Body code: %d", code);

                    if (code == 1)
                        memoryStatus.status[type] = STATUS_INVALID;
                    else if (code == 0)
                        memoryStatus.status[type] = STATUS_VERIFIED;
                } else if (type == LICENSE_TYPE_ALIPAY) {
                    auto instanceId = b["deviceInstanceId"];
                    if (instanceId.is_null()) {
                        LOGD("Body deviceInstanceId: null");
                        memoryStatus.status[type] = STATUS_INVALID;
                    } else {
                        auto value = instanceId.get<std::string>();
                        LOGD("Body deviceInstanceId: %s", value.c_str());

                        if (strcmp(value.c_str(), memoryStatus.instance_id) == 0) {
                            memoryStatus.status[type] = STATUS_VERIFIED;
                        } else {
                            memoryStatus.status[type] = STATUS_UNKNOWN;
                        }
                    }
                }
            }
        }

        LOGD("license status for %d is %d", type, memoryStatus.status[type]);

        if (memoryStatus.status[type] == STATUS_INVALID) {
            WriteInvalidFile();
        }
    }
}

namespace License {

    pthread_mutex_t mutex;

    static void FirstVerify() {
        if (pthread_mutex_lock(&mutex) != 0) {
            LOGD("lock error");
        }
        // wait for decryption (network may be connected)
        do {
            if (access("/storage/emulated/0/Android", F_OK) == 0) {
                break;
            }
            sleep(10);
        } while (true);

        for (int type = 0; type < LICENSE_MAX_TYPES; ++type) {
            if (memoryStatus.status[type] != STATUS_VERIFIED_ONCE) continue;

            Network::Check(type);
        }

        pthread_mutex_unlock(&mutex);
    }

    static void ReadAndPostVerify() {
        if (pthread_mutex_lock(&mutex) != 0) {
            LOGD("lock error");
        }

        LOGD("read license file");

        if (!ReadFromFile(memoryStatus)) {
            LOGD("failed to read license file");
            goto unlock;
        }

        unlock:
        pthread_mutex_unlock(&mutex);

        daemon_thread(FirstVerify);
    }

    void Init(const char *invalid_file) {
        invalidFile = invalid_file;

        pthread_mutex_init(&mutex, nullptr);

        ReadAndPostVerify();
    }

    bool IsNoValidLicense() {
        for (unsigned char s : memoryStatus.status) {
            if (s == STATUS_INVALID)
                return true;
        }

        for (unsigned char s : memoryStatus.status) {
            if (s == STATUS_VERIFIED || s == STATUS_VERIFIED_ONCE)
                return false;
        }

        return true;
    }

    void Update(int type, const char *value) {
        if (type == LICENSE_TYPE_INSTANCE_ID) {
            LOGD("instance id: %s", value);
            if (memoryStatus.instance_id == nullptr ||
                strcmp(memoryStatus.instance_id, value) != 0) {
                if (memoryStatus.instance_id) {
                    free(memoryStatus.instance_id);
                }
                LOGD("instance id changed, remove saved file");
                unlink(LICENSE_FILE);

                memoryStatus.instance_id = strdup(value);
            }
            return;
        }

        if (!VerifyLocal(type, value)) return;

        if (pthread_mutex_lock(&mutex) != 0) {
            LOGD("lock error");
        }

        if (memoryStatus.tokens[type] && strcmp(memoryStatus.tokens[type], value) == 0) {
            // should be checked before
            goto unlock;
        }

        LOGD("check type %d %s", type, value);

        if (memoryStatus.tokens[type]) free(memoryStatus.tokens[type]);

        memoryStatus.tokens[type] = strdup(value);
        memoryStatus.status[type] = STATUS_UNKNOWN;

        Network::Check(type);

        if (memoryStatus.status[type] != STATUS_UNKNOWN) {
            LOGD("write license file");
            WriteToFile(memoryStatus);
        }

        unlock:
        pthread_mutex_unlock(&mutex);
    }
}