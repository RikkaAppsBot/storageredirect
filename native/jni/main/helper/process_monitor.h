#pragma once

#include <jni.h>

namespace ProcessMonitor {
    void start(bool has_module, bool from_app);
}