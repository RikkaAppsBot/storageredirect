#undef LOG_TAG
#define LOG_TAG "License"

#include <fcntl.h>
#include <misc.h>
#include <logging.h>
#include <unistd.h>
#include <cinttypes>
#include <crypto/aes.h>
#include <sys/stat.h>
#include <crypto/digests.h>
#include <obfuscate.h>
#include "license_file.h"
#include "flatbuffers/flatbuffers.h"
#include "helper/license_file_header_generated.h"
#include "helper/license_file_generated.h"

static auto aad_source_file = "/data/adb/storage-isolation";

static void GenerateAAD(unsigned char *out, size_t *out_len) {
    struct stat st{};
    stat(aad_source_file, &st);

    unsigned int hash_len;
    sha256((unsigned char *) &st.st_ino, sizeof(st.st_ino), out, &hash_len);
    *out_len = hash_len;
}

static void GenerateKey(unsigned char *out) {
    static auto key = "cSe8TxQFag7y62dxUWyNVUsaDwT1Ewzz";

    struct stat st{};
    stat(aad_source_file, &st);

    auto size = sizeof(st.st_ino);

    memcpy(out + size, key, 32 - size);
    memcpy(out, &st.st_ino, size);
}

static inline void GenerateIV(unsigned char *iv, const unsigned char *plaintext, size_t plaintext_len) {
    int i = 0;
    while (i < 12) {
        iv[i] = plaintext[i % plaintext_len] ^ (unsigned char) (i % 12 + 1);
        i++;
    }
}

bool License::ReadFromFile(License::Status &output) {
    unsigned char *ciphertext = nullptr, *plaintext = nullptr, *buf;
    size_t ciphertext_len;
    uint32_t size;
    bool res = true;

    int fd = openAt(AT_FDCWD, LICENSE_FILE, O_RDONLY);
    if (fd < 0) {
        LOGD("no license file");
        goto failed;
    }

    ciphertext_len = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);
    ciphertext = (uint8_t *) malloc(ciphertext_len);

    if (read_full(fd, ciphertext, ciphertext_len) == -1) goto failed;

    unsigned char aad[32];
    size_t aad_len;
    GenerateAAD(aad, &aad_len);

    unsigned char key[32];
    GenerateKey(key);

    size_t plaintext_len;
    plaintext = aes_256_gcm_decrypt(ciphertext, ciphertext_len, aad, aad_len, key, &plaintext_len);
    if (!plaintext) goto failed;

    // --------------- header ---------------

    buf = plaintext;
    memcpy(&size, buf, sizeof(size));
    buf += sizeof(size);

    {
        auto verifier = flatbuffers::Verifier(buf, size);
        if (!License::VerifyLicenseFileHeaderBuffer(verifier)) {
            LOGD("invalid license header");
            goto failed;
        }
        auto header = License::GetLicenseFileHeader(buf);
        if (header->version() > LICENSE_FILE_VERSION) {
            LOGD("license file version: %d, max=%d", header->version(), LICENSE_FILE_VERSION);
            goto failed;
        }
    }

    buf += size;

    // --------------- body ---------------

    memcpy(&size, buf, sizeof(size));
    buf += sizeof(size);

    {
        auto verifier = flatbuffers::Verifier(buf, size);
        if (!License::VerifyLicenseFileBuffer(verifier)) {
            LOGD("invalid license body");
            goto failed;
        }
        auto body = License::GetLicenseFile(buf);

        output.instance_id = strdup(body->instance_id()->c_str());

        const flatbuffers::String *tokens[]{body->google(), body->alipay(), body->redeem()};

        for (int type = 0; type < LICENSE_MAX_TYPES; ++type) {
            if (!tokens[type] || strlen(tokens[type]->c_str()) == 0) {
                output.status[type] = STATUS_UNKNOWN;
                continue;
            }

            auto str = tokens[type]->c_str();
            if (!License::VerifyLocal(type, str)) {
                output.status[type] = STATUS_INVALID;
            } else {
                output.tokens[type] = strdup(str);
                output.status[type] = STATUS_VERIFIED_ONCE;
            }

            LOGD("license from file: type=%d, value=%s, status=%d", type, output.tokens[type], output.status[type]);
        }
    }

    goto cleanup;

    // ------------------------------

    failed:
    res = false;
    unlink(LICENSE_FILE);

    cleanup:
    if (ciphertext) free(ciphertext);
    if (plaintext) free(plaintext);
    if (fd != -1) {
        close(fd);
    }
    return res;
}

/**
 * verified -> write
 * invalid -> STATUS_INVALID_VALUE
 * others -> null
 */
static const char *GetValueForFile(const License::Status &input, int type) {
    if (!License::VerifyLocal(type, input.tokens[type])) return nullptr;

    if (input.status[type] == STATUS_VERIFIED) return input.tokens[type];
    if (input.status[type] == STATUS_INVALID) return INVALID_VALUE;
    return nullptr;
}

void License::WriteToFile(const License::Status &input) {
    if (!input.instance_id) return;

    int fd = openat(AT_FDCWD, LICENSE_FILE, O_WRONLY | O_CREAT | O_TRUNC, 0600);
    if (fd < 0) return;

    flatbuffers::FlatBufferBuilder builder;
    char buffer[4096] = {0};
    size_t size = 0;

    // --------------- header ---------------

    {
        auto l = License::CreateLicenseFileHeader(builder, LICENSE_FILE_VERSION);
        License::FinishLicenseFileHeaderBuffer(builder, l);
        uint32_t s = builder.GetSize();

        memcpy(buffer + size, &s, sizeof(s));
        size += sizeof(s);

        memcpy(buffer + size, builder.GetBufferPointer(), s);
        size += s;
    }

    builder.Clear();

    // --------------- body ---------------

    {
        auto l = License::CreateLicenseFileDirect(
                builder,
                input.instance_id,
                GetValueForFile(input, LICENSE_TYPE_GOOGLE),
                GetValueForFile(input, LICENSE_TYPE_ALIPAY),
                GetValueForFile(input, LICENSE_TYPE_REDEEM));
        License::FinishLicenseFileBuffer(builder, l);
        uint32_t s = builder.GetSize();

        memcpy(buffer + size, &s, sizeof(s));
        size += sizeof(s);

        memcpy(buffer + size, builder.GetBufferPointer(), s);
        size += s;
    }


    auto *plaintext = (unsigned char *) buffer;
    auto plaintext_len = 4096;

    unsigned char iv[12];
    GenerateIV(iv, plaintext, plaintext_len);

    unsigned char aad[32];
    size_t aad_len;
    GenerateAAD(aad, &aad_len);

    unsigned char key[32];
    GenerateKey(key);

    size_t ciphertext_len;
    auto ciphertext = aes_256_gcm_encrypt(plaintext, plaintext_len, aad, aad_len, key, iv, &ciphertext_len);

    if (ciphertext) {
        write_full(fd, ciphertext, ciphertext_len);
        free(ciphertext);
    }
    close(fd);
}