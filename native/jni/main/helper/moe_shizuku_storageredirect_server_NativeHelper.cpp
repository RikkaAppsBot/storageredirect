#undef LOG_TAG
#define LOG_TAG "StorageRedirectHelper"

#include <jni.h>
#include <dirent.h>
#include <unistd.h>
#include <mntent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wrap.h>
#include <misc.h>
#include <logging.h>
#include <config.h>
#include <nativehelper/scoped_utf_chars.h>
#include <android.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <socket.h>
#include <endian.h>
#include <fcntl.h>
#include "process_monitor.h"
#include "license.h"

static JavaVM *javaVm = nullptr;
static jobject globalListener = nullptr;
static jmethodID onProcessStartedMethodId = nullptr, onLogcatStatusChangedMethodId = nullptr;
static const char *socketToken = nullptr, *socketAddress = nullptr;
static pid_t daemonPid;
static const char *riruVersion = nullptr, *riruVersionName;
static const char *moduleVersion = nullptr, *moduleVersionName;

static jint killUid(JNIEnv *env, jclass cls, jint uid) {
    DIR *dir;
    struct dirent *entry;
    struct stat st{};
    char path[32];

    if (!(dir = _opendir("/proc")))
        return 1;

    while ((entry = _readdir(dir))) {
        if (entry->d_type == DT_DIR) {
            if (is_num(entry->d_name)) {
                pid_t pid = atoi(entry->d_name);
                sprintf(path, "/proc/%s", entry->d_name);
                stat(path, &st);
                if (uid == st.st_uid) {
                    kill(pid, SIGKILL);

                    LOGI("kill %d (uid %d)", pid, uid);
                }
            }
        }
    }

    closedir(dir);
    return 0;
}

static jstring getMediaPath(JNIEnv *env) {
    struct mntent *ent;
    FILE *fp;
    char *media_path = nullptr;

    // parse mounts first
    fp = setmntent("/proc/mounts", "r");
    if (fp == nullptr) {
        PLOGE("setmntent");
        return nullptr;
    }
    while (nullptr != (ent = getmntent(fp))) {
        if (strcmp(ent->mnt_dir, "/mnt/runtime/write/emulated") == 0) {
            if (ent->mnt_fsname) {
                media_path = strdup(ent->mnt_fsname);
            }
            break;
        }
    }
    endmntent(fp);

    // if empty or not start with /data, /mnt/expand
    if (!media_path ||
        (strncmp(media_path, "/data", strlen("/data")) != 0 &&
         strncmp(media_path, "/mnt/expand", strlen("/mnt/expand")) != 0)) {

        if (access("/data/share/0/Android", F_OK) == 0) { // only some special device
            media_path = strdup("/data/share");
        } else if (access("/data/media/0/Android", F_OK) == 0) {
            media_path = strdup("/data/media");
        }
    }

    jstring res = env->NewStringUTF(media_path);
    if (media_path) free(media_path);
    return res;
}

static jstring getString(JNIEnv *env, jclass cls, jint type) {
    if (type == 0) {
        return getMediaPath(env);
    } else if (type == 1) {
        return socketToken ? env->NewStringUTF(socketToken) : nullptr;
    } else if (type == 2) {
        return socketAddress ? env->NewStringUTF(socketAddress) : nullptr;
    } else if (type == 3) {
        return riruVersion ? env->NewStringUTF(riruVersion) : nullptr;
    } else if (type == 4) {
        return riruVersionName ? env->NewStringUTF(riruVersionName) : nullptr;
    } else if (type == 5) {
        return moduleVersion ? env->NewStringUTF(moduleVersion) : nullptr;
    } else if (type == 6) {
        return moduleVersionName ? env->NewStringUTF(moduleVersionName) : nullptr;
    }
    return nullptr;
}

static jboolean isNativeProcessExists() {
    static pid_t native_pid;
    native_pid = -1;

    foreach_proc([](pid_t pid) {
        if (pid == getpid()) return false;

        char buf[64];
        snprintf(buf, 64, "/proc/%d/cmdline", pid);

        int fd = open(buf, O_RDONLY);
        if (fd > 0) {
            memset(buf, 0, 64);
            if (read(fd, buf, 64) > 0 && strcmp(PROCESS_NAME, buf) == 0) {
                native_pid = pid;
            }
            close(fd);
        }

        return native_pid != -1;
    });

    return native_pid != -1;
}

static jboolean getBoolean(JNIEnv *env, jclass cls, jint type) {
    if (type == 0) {
        return isNativeProcessExists();
    } else if (type == -1) {
        auto isNoValidLicense = License::IsNoValidLicense();
        LOGD("isNoValidLicense=%s", isNoValidLicense ? "true" : "false");
        return isNoValidLicense;
    } else if (type == 1) {
        return android::IsSdcardfsUsed();
    } else {
        return false;
    }
}

static void startMonitors(JNIEnv *env, jclass cls, jobject listener, jboolean hasModule, jboolean fromApp) {
    if (!globalListener) {
        ProcessMonitor::start(hasModule, fromApp);
    }
    globalListener = env->NewGlobalRef(listener);
}

static void updateLicense(JNIEnv *env, jclass cls, jint type, jobject jValue) {
    if (jValue) {
        ScopedUtfChars value(env, (jstring) jValue);
        LOGD("updateLicense %d %s", type, value.c_str());
        License::Update(type, value.c_str());
    } else {
        LOGD("updateLicense %d (null)", type);
    }
}

static void parseArgs(JNIEnv *env, jclass cls, jobjectArray jArgs) {
    if (!jArgs) return;

    auto length = env->GetArrayLength(jArgs);
    for (int i = 0; i < length; i++) {
        auto jArg = (jstring) (env->GetObjectArrayElement(jArgs, i));
        if (!jArg) continue;

        ScopedUtfChars s_arg(env, jArg);
        auto arg = s_arg.c_str();

        if (strncmp(arg, "--token=", 8) == 0) {
            socketToken = strdup(arg + 8);
            LOGD("socket token: %s", socketToken);

        } else if (strncmp(arg, "--socket-address=", 17) == 0) {
            socketAddress = strdup(arg + 17);
            LOGD("socket address: %s", socketAddress);

        } else if (strncmp(arg, "--parent-pid=", 13) == 0) {
            daemonPid = atoi(arg + 13);
            LOGD("ppid: %d", daemonPid);

        } else if (strncmp(arg, "--riru-version=", 15) == 0) {
            riruVersion = strdup(arg + 15);
            LOGD("riru version: %s", riruVersion);

        } else if (strncmp(arg, "--riru-version-name=", 20) == 0) {
            riruVersionName = strdup(arg + 20);
            LOGD("riru version name: %s", riruVersionName);

        } else if (strncmp(arg, "--module-version=", 17) == 0) {
            moduleVersion = strdup(arg + 17);
            LOGD("module version: %s", moduleVersion);

        } else if (strncmp(arg, "--module-version-name=", 22) == 0) {
            moduleVersionName = strdup(arg + 22);
            LOGD("module version name: %s", moduleVersionName);
        }
    }

    License::Init(socketAddress);
}

static jobject connectToDaemon(JNIEnv *env, jclass cls, jobject data) {
    if (data == nullptr) return nullptr;

    auto buffer = env->GetDirectBufferAddress(data);
    auto buffer_size = env->GetDirectBufferCapacity(data);

    struct sockaddr_un addr{};
    int fd;
    socklen_t socklen;
    int32_t size;
    char *reply_buffer;
    jobject reply = nullptr;

    if ((fd = socket(PF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0)) < 0) {
        PLOGE("socket");
        return nullptr;
    }

    socklen = setup_sockaddr(&addr, socketAddress);

    if (connect(fd, (struct sockaddr *) &addr, socklen) == -1) {
        PLOGE("connect %s", socketAddress);
        goto clean;
    }

    LOGD("write %llu", buffer_size);
    if (write_full(fd, buffer, buffer_size) != 0) {
        PLOGE("write %s", socketAddress);
        goto clean;
    }

    if (read_full(fd, &size, sizeof(size)) != 0) {
        goto clean;
        PLOGE("read %s", socketAddress);
    }

    if (size < 0) goto clean;

    reply_buffer = (char *) malloc(size);

    if (read_full(fd, reply_buffer, size) != 0) {
        goto clean;
        PLOGE("read %s", socketAddress);
    }

    reply = env->NewDirectByteBuffer(reply_buffer, size);

    clean:
    close(fd);
    return reply;
}

static void freeDirectByteBuffer(JNIEnv *env, jclass cls, jobject data) {
    if (data == nullptr) return;
    auto buffer = env->GetDirectBufferAddress(data);
    if (buffer) free(buffer);
}

namespace JNI::NativeHelper {

    jint registerNative(JNIEnv *env) {
        env->GetJavaVM(&javaVm);

        static JNINativeMethod methods[] = {
                {"a",  "(I)I",                                   (void *) killUid},
                {"a0", "(I)Ljava/lang/String;",                  (void *) getString},
                {"a1", "(I)Z",                                   (void *) getBoolean},
                {"a2", "(Ljava/lang/Object;ZZ)V",                (void *) startMonitors},
                {"a3", "(ILjava/lang/Object;)V",                 (void *) updateLicense},
                {"b",  "([Ljava/lang/Object;)V",                 (void *) parseArgs},
                {"c",  "(Ljava/lang/Object;)Ljava/lang/Object;", (void *) connectToDaemon},
                {"d",  "(Ljava/lang/Object;)V",                  (void *) freeDirectByteBuffer},
        };

        auto mainClass = env->FindClass("moe/shizuku/redirectstorage/server/c");
        auto listenerClass = env->FindClass("moe/shizuku/redirectstorage/server/c1");
        onProcessStartedMethodId = env->GetMethodID(listenerClass, "a", "(IILjava/lang/String;)V");
        onLogcatStatusChangedMethodId = env->GetMethodID(listenerClass, "a0", "(Z)V");

        return env->RegisterNatives(mainClass, methods, sizeof(methods) / sizeof(methods[0]));
    }

    void onProcessStarted(pid_t pid, uid_t uid, const char *packageName) {
        if (!globalListener) return;

        JNIEnv *env;
        auto res = javaVm->GetEnv((void **) &env, JNI_VERSION_1_6);
        if (res == JNI_EDETACHED) {
            if (javaVm->AttachCurrentThread(&env, nullptr) != JNI_OK) {
                LOGD("unable to attach JVM");
                return;
            }
        } else if (res == JNI_OK) {
        } else if (res == JNI_EVERSION) {
            return;
        }

        LOGD("onProcessStarted %d %d %s", pid, uid, packageName);

        jstring jPackageName = packageName ? env->NewStringUTF(packageName) : nullptr;
        env->CallVoidMethod(globalListener, onProcessStartedMethodId, pid, uid, jPackageName);

        if (env->ExceptionCheck()) {
#ifdef DEBUG
            env->ExceptionDescribe();
#endif
            env->ExceptionClear();
        }
    }

    void onLogcatStatusChangedMethod(bool hasLog) {
        if (!globalListener) return;

        JNIEnv *env;
        auto res = javaVm->GetEnv((void **) &env, JNI_VERSION_1_6);
        if (res == JNI_EDETACHED) {
            if (javaVm->AttachCurrentThread(&env, nullptr) != JNI_OK) {
                LOGD("unable to attach JVM");
                return;
            }
        } else if (res == JNI_OK) {
        } else if (res == JNI_EVERSION) {
            return;
        }

        LOGD("onLogcatStatusChangedMethod %s", hasLog ? "true" : "false");

        env->CallVoidMethod(globalListener, onLogcatStatusChangedMethodId, hasLog ? JNI_TRUE : JNI_FALSE);

        if (env->ExceptionCheck()) {
#ifdef DEBUG
            env->ExceptionDescribe();
#endif
            env->ExceptionClear();
        }
    }
}