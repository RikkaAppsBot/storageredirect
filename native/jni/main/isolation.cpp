#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <sys/mount.h>
#include <mntent.h>
#include <dlfcn.h>
#include <sys/system_properties.h>
#include <fcntl.h>

#include <logging.h>
#include <misc.h>
#include <wrap.h>
#include <sys/stat.h>
#include <wait.h>
#include <selinux.h>
#include <sys/un.h>
#include <endian.h>
#include <flatbuffers/request_check_process_generated.h>
#include <config.h>
#include "misc.h"
#include "setproctitle.h"
#include "enhance.h"
#include "android.h"
#include "license.h"

#define PERM_READ                           1u
#define PERM_WRITE                          1u << 1u
#define MOUNT_INSTALLER                     1u << 2u
#define MOUNT_EXTERNAL_ANDROID_WRITEABLE    1u << 3u
#define MOUNT_PASS_THROUGH                  1u << 4u
#define FLAG_ENCRYPTED                      1u << 10u

static int waitpath(const int pid, const char *path, useconds_t useconds, unsigned int timeout) {
    int i = 0;
    while (true) {
        if (access(path, F_OK) == 0) {
            if (i) {
                LOGI("wait path %dus works", i * useconds);
            }

            return 0;
        }

        if (is_proc_dead(pid)) {
            LOGW("%d is dead", pid);
            return 2;
        }

        if (i == 0)
            usleep(useconds);

        ++i;

        if (i * useconds >= timeout)
            break;
    }

    LOGW("wait path failed after %dus", i * useconds);

    return 1;
}

static int read_ns(const int pid, struct stat *st) {
    char path[32];
    sprintf(path, "/proc/%d/ns/mnt", pid);
    return stat(path, st);
}

static int parse_ppid(int pid) {
    char stat[512], path[32];
    int fd, ppid = -1;
    sprintf(path, "/proc/%d/stat", pid);
    fd = _open(path, O_RDONLY);
    if (fd != -1) {
        read(fd, stat, sizeof(stat));
        close(fd);
        /* PID COMM STATE PPID ..... */
        sscanf(stat, "%*d %*s %*c %d", &ppid);
    }
    return ppid;
}

static int waitpidns(int pid, int ppid, useconds_t useconds, unsigned int timeout) {
    struct stat ns{}, pns{};
    int ok;
    int trial = 0;
    read_ns(ppid, &pns);
    while (true) {
        if (is_proc_dead(pid)) {
            LOGW("%d is dead", pid);
            return 2;
        }

        read_ns(pid, &ns);
        ok = (ns.st_dev != pns.st_dev || ns.st_ino != pns.st_ino);

        if (!ok) {
            usleep(useconds);

            trial++;

            if (trial * useconds >= timeout) {
                LOGW("wait ns failed after %dus", trial * useconds);
                return 1;
            }
        } else {
            if (trial != 0) {
                LOGV("wait ns works after %dus", trial * useconds);
            }
            break;
        }
    }

    return 0;
}

static int is_isolated(const char *package, int user) {
    int res = 0;

    FILE *fp = setmntent("/proc/mounts", "r");
    if (fp == nullptr) {
        LOGE("Error opening /proc/mounts: %s", strerror(errno));
    }

    char check[64];
    snprintf(check, sizeof(check), "/storage/emulated/%d/Android/data/%s", user, package);

    struct mntent *mentry;
    while ((mentry = getmntent(fp)) != nullptr) {
        if (strncmp(mentry->mnt_dir, check, strlen(check)) == 0) {
            res = 1;
            break;
        }
    }
    endmntent(fp);

    return res;
}

static int failed(const int pid, const char *reason) {
    kill(pid, SIGKILL);

    LOGE("%s, killing %d...", reason, pid);
    return 1;
}

static int lazy_mount(const char *source, const char *target, unsigned long flags) {
    if (ensure_dir(source, 0775) && !is_dir(source))
        return -1;

    if (ensure_dir(target, 0775) && !is_dir(target))
        return -1;

    //LOGD("mount: %s->%s", source, target);

    if (!License::valid()) return 0;
    return _mount(source, target, flags);
}

static int touch(const char *path, mode_t mode) {
    if (access(path, F_OK) == 0) {
        _chmod(path, mode);
        return 0;
    }

    if (creat(path, 0755) == 0) {
        _chmod(path, mode);
        return 0;
    }
    return 1;
}

static int ensure_dir(const char *path) {
    if (access(path, F_OK) != 0 && ensure_dir(path, 0755) != 0)
        return 1;


    return 0;
}

static int handle_process(const RequestHandler::CheckProcessRequest *request) {
    struct stat st{};

    pid_t pid = request->pid();
    uid_t uid = request->uid();
    auto package = request->pkg()->c_str();
    auto flags = request->flags();

    uint32_t user = uid / 100000;
    auto decrypted = (flags & FLAG_ENCRYPTED) == 0;
    auto logcatMode = Enhance::getModuleVersion() < 0;
    auto isFuse = android::IsFuse();
    auto isSdcardfsUsed = android::IsSdcardfsUsed();

    LOGV("isolation for uid=%d, pid=%d, package=%s, decrypted=%s, fuse=%s, sdcardfs=%s",
         uid, pid, package, decrypted ? "true" : "false", isFuse ? "true" : "false", isSdcardfsUsed ? "true" : "false");

    LOGD("isolation for uid=%d, pid=%d, flags=%d", uid, pid, flags);

    // Step 1
    char buf[PATH_MAX];

    // Step 2: prepare
    // wait for process call unshare when not using module
    if (logcatMode) {
        int ppid = parse_ppid(pid);
        switch (waitpidns(pid, ppid, 50, 1000 * 1000)) {
            case 2:
                return 2;
            case 1:
                // timeout
                return 0;
            default:
                break;
        }
    }

    // attach mount namespace
    if (switch_mnt_ns(pid) != 0)
        return failed(pid, "switch_mnt_ns failed");

    // check if isolation have run when not using module
    if (logcatMode && is_isolated(package, user)) {
        LOGV("isolation have already run for pid %d.", pid);
        return 0;
    }

    if (logcatMode) {
        // wait for mount /storage
        snprintf(buf, sizeof(buf), "/storage/emulated/%d", user);
        switch (waitpath(pid, buf, 50, 1000 * 1000)) {
            case 2:
                // killed
                return 2;
            case 1:
                // timeout
                return 0;
            default:
                break;
        }
    }

    // Step 3: mount sdcard
    char real[PATH_MAX];
    if (isFuse && !isSdcardfsUsed) {
        if (flags & MOUNT_PASS_THROUGH) {
            snprintf(real, sizeof(real), "/mnt/pass_through/%d/emulated/%d", user, user);
        } else {
            snprintf(real, sizeof(real), "/mnt/user/%d/emulated/%d", user, user);
        }
    } else {
        snprintf(real, sizeof(real), "/mnt/runtime/write/emulated/%d", user);
        /*if (flags & PERM_WRITE) {
            snprintf(real, sizeof(real), "/mnt/runtime/write/emulated/%d", user);
        } else {
            snprintf(real, sizeof(real), "/mnt/runtime/read/emulated/%d", user);
        }*/
    }
    LOGD("real: %s", real);

    char storage[PATH_MAX];
    snprintf(storage, sizeof(storage), "/storage/emulated/%d", user);

    if (!decrypted) {
        LOGW("not decrypted?");

        if (android::GetApiLevel() < 30) {
            char default_external[PATH_MAX];
            snprintf(default_external, sizeof(default_external), "/mnt/runtime/default/emulated/%d", user);
            if (0 != _mount(default_external, storage, MS_BIND)) {
                LOGW("failed to mount /mnt/runtime/default/emulated/%d", user);
            }
        }

        switch (_fork()) {
            case 0:
                break;
            default:
#ifdef DEBUG
                print_mounts("/mnt");
                print_mounts("/storage");
#endif

                LOGW("continue redirected process");
                return 0;
        }
    }

    if (!decrypted) {
        // wait for decryption
        while (true) {
            if (is_proc_dead(pid)) {
                LOGW("%d:%s:%d: process dead", user, package, pid);
                exit(0);
            }

            snprintf(buf, sizeof(buf), "/storage/emulated/%d/Android", user);
            if (access(buf, F_OK) == 0) {
                LOGI("%d:%s:%d: decrypted", user, package, pid);
                break;
            }

            LOGI("%d:%s:%d: not decrypted, wait 1s", user, package, pid);
            sleep(1);
        }

        _umount2(storage, MNT_DETACH);
    }

    // make unshared
    if (isFuse) {
        /*if (mount(nullptr, "/mnt/user", nullptr, MS_SLAVE | MS_REC, nullptr))
            PLOGE("mount MS_SLAVE /mnt/user");*/
    } else {
        if (mount(nullptr, "/mnt/runtime/write/emulated", nullptr, MS_SLAVE | MS_REC, nullptr))
            PLOGE("mount MS_SLAVE %s", "/mnt/runtime/write/emulated");
    }

    if (mount(nullptr, "/storage/emulated", nullptr, MS_SLAVE | MS_REC, nullptr)
        || mount(nullptr, "/storage", nullptr, MS_SLAVE | MS_REC, nullptr)) {
        PLOGE("mount MS_SLAVE /storage");
    }

    char new_sdcard[PATH_MAX];
    if (isFuse) {
        sprintf(new_sdcard, "%s/%s", real, request->target()->c_str());
    } else {
        sprintf(new_sdcard, "/storage/emulated/%d/%s", user, request->target()->c_str());
    }
    ensure_dir(new_sdcard, 0755);

    LOGD("new_sdcard %s", new_sdcard);

    if (isFuse && !isSdcardfsUsed) {
        snprintf(buf, sizeof(buf), "/data/media/%d/Android/data/%s", user, package);
        auto offset = strlen(buf);
        snprintf(buf, sizeof(buf), "/data/media/%d/%s", user, request->target()->c_str());

        if (stat(buf, &st) == 0) {
            LOGD("chown_recursive %s %d %d", buf, uid, st.st_gid);
            chown_recursive(buf, offset, uid, st.st_gid);
        } else {
            LOGD("stat %s failed with %d: %s", buf, errno, strerror(errno));
        }
    }

    LOGD("%s -> %s", new_sdcard, storage);

    if (lazy_mount(new_sdcard, storage, MS_BIND) != 0) {
        LOGW("%d:%s:%d: can't remount %s", user, package, pid, new_sdcard);
        if (!decrypted) {
            exit(0);
        } else {
            return 0;
        }
    }

    // Step 4: mount folders
    char source[PATH_MAX];
    char target[PATH_MAX];

    if (request->mounts()) {
        LOGD("mounts count: %d", request->mounts()->size());
        for (uint32_t i = 0; i < request->mounts()->size(); ++i) {
            auto mount = request->mounts()->Get(i);

            snprintf(source, sizeof(source), "%s/%s", real, mount->source()->c_str());
            snprintf(target, sizeof(target), "%s/%s", storage, mount->target()->c_str());

            if (access(source, F_OK) != 0) {
                LOGW("can't access %s (%d: %s), creating", source, errno, strerror(errno));
                if (ensure_dir(source, 0755) != 0) {
                    LOGW("can't create %s (%d: %s)", source, errno, strerror(errno));
                    continue;
                }
            }

            LOGD("%s -> %s", source, target);
            lazy_mount(source, target, MS_BIND);

            if (isFuse) {
                snprintf(buf, sizeof(buf), "/data/media/%d/%s/", user, request->target()->c_str());
                auto offset = strlen(buf);
                strcat(buf, mount->target()->c_str());

                if (stat(buf, &st) == 0) {
                    chown_recursive(buf, offset, isSdcardfsUsed ? 1023 : uid, st.st_gid);
                } else {
                    LOGD("stat %s failed with %d: %s", buf, errno, strerror(errno));
                }
            }
        }
    } else {
        LOGD("mounts is null");
    }

    if (isFuse) {
        if (request->observers()) {
            LOGD("observers count: %d", request->observers()->size());
            for (uint32_t i = 0; i < request->observers()->size(); ++i) {
                auto mount = request->observers()->Get(i);

                snprintf(source, sizeof(source), "%s/%s", real, mount->target()->c_str());
                snprintf(target, sizeof(target), "%s/%s", storage, mount->source()->c_str());

                if (access(source, F_OK) != 0) {
                    LOGW("can't access %s (%d: %s), creating", source, errno, strerror(errno));
                    if (ensure_dir(source, 0755) != 0) {
                        LOGW("can't create %s (%d: %s)", source, errno, strerror(errno));
                        continue;
                    }
                }

                LOGD("%s -> %s", source, target);
                lazy_mount(source, target, MS_BIND);

                snprintf(buf, sizeof(buf), "/data/media/%d/%s/", user, request->target()->c_str());
                auto offset = strlen(buf);
                strcat(buf, mount->target()->c_str());

                if (stat(buf, &st) == 0) {
                    chown_recursive(buf, offset, isSdcardfsUsed ? 1023 : uid, st.st_gid);
                } else {
                    LOGD("stat %s failed with %d: %s", buf, errno, strerror(errno));
                }
            }
        } else {
            LOGD("observers is null");
        }
    }

    // Step 5: create .nomedia
    snprintf(target, sizeof(target), "%s/Android/data/%s/.nomedia", storage, package);
    touch(target, 0644);

    if (isFuse) {
        if (stat(target, &st) == 0) {
            _chown(target, uid, st.st_gid);
        }
    }

#ifdef DEBUG
    print_mounts("/mnt");
    print_mounts("/storage");
#endif

    if (!decrypted) {
        LOGV("isolation finished");
        exit(0);
    }

    LOGV("isolation finished");
    return 0;
}

namespace Isolation {

    int handleProcess(const RequestHandler::CheckProcessRequest *request) {
        pid_t pid = request->pid();
        if (pid <= 0) return 0;

        if (is_proc_dead(pid)) {
            LOGW("%d is dead", pid);
            return 0;
        }

        /*pid_t child;
        int status;

        switch (child = _fork()) {
            case 0:
                setproctitle(PROCESS_NAME_WORKER);
                handle_process(request);
                exit(0);
            case -1:
                return 1;
            default:
                waitpid(child, &status, 0);
                int code = WEXITSTATUS(status);
                LOGD("res=%d", code);
                return code;
        }*/
        return handle_process(request);
    }
}

