#ifndef DIGESTS_H
#define DIGESTS_H

void sha256(const unsigned char *message, size_t message_len, unsigned char *digest, unsigned int *digest_len);

#endif // DIGESTS_H
