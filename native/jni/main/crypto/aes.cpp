#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <cstring>

static int handleErrors() {
    return -1;
}

static const size_t IV_SIZE_IN_BYTES = 12;
static const size_t TAG_SIZE_IN_BYTES = 16;

static int aes_256_gcm_encrypt(unsigned char *plaintext, int plaintext_len,
                               unsigned char *aad, int aad_len,
                               unsigned char *key,
                               unsigned char *iv, int iv_len,
                               unsigned char *ciphertext,
                               unsigned char *tag) {
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;


    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new()))
        return handleErrors();

    /* Initialise the encryption operation. */
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), nullptr, nullptr, nullptr))
        return handleErrors();

    /*
     * Set IV length if default 12 bytes (96 bits) is not appropriate
     */
    if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, nullptr))
        return handleErrors();

    /* Initialise key and IV */
    if (1 != EVP_EncryptInit_ex(ctx, nullptr, nullptr, key, iv))
        return handleErrors();

    /*
     * Provide any AAD data. This can be called zero or more times as
     * required
     */
    if (1 != EVP_EncryptUpdate(ctx, nullptr, &len, aad, aad_len))
        return handleErrors();

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        return handleErrors();
    ciphertext_len = len;

    /*
     * Finalise the encryption. Normally ciphertext bytes may be written at
     * this stage, but this does not occur in GCM mode
     */
    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        return handleErrors();
    ciphertext_len += len;

    /* Get the tag */
    if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
        return handleErrors();

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

static int aes_256_gcm_decrypt(unsigned char *ciphertext, int ciphertext_len,
                               unsigned char *aad, int aad_len,
                               unsigned char *tag,
                               unsigned char *key,
                               unsigned char *iv, int iv_len,
                               unsigned char *plaintext) {
    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len;
    int ret;

    /* Create and initialise the context */
    if (!(ctx = EVP_CIPHER_CTX_new()))
        return handleErrors();

    /* Initialise the decryption operation. */
    if (!EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), nullptr, nullptr, nullptr))
        return handleErrors();

    /* Set IV length. Not necessary if this is 12 bytes (96 bits) */
    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, nullptr))
        return handleErrors();

    /* Initialise key and IV */
    if (!EVP_DecryptInit_ex(ctx, nullptr, nullptr, key, iv))
        return handleErrors();

    /*
     * Provide any AAD data. This can be called zero or more times as
     * required
     */
    if (!EVP_DecryptUpdate(ctx, nullptr, &len, aad, aad_len))
        return handleErrors();

    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary
     */
    if (!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        return handleErrors();
    plaintext_len = len;

    /* Set expected tag value. Works in OpenSSL 1.0.1d and later */
    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag))
        return handleErrors();
    /*
     * Finalise the decryption. A positive return value indicates success,
     * anything else is a failure - the plaintext is not trustworthy.
     */
    ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    if (ret > 0) {
        /* Success */
        plaintext_len += len;
        return plaintext_len;
    } else {
        /* Verify failed */
        return -1;
    }
}

unsigned char *aes_256_gcm_encrypt(unsigned char *plaintext, size_t plaintext_len,
                                   unsigned char *aad, size_t aad_len,
                                   unsigned char *key,
                                   unsigned char *iv,
                                   size_t *ciphertext_len) {
    *ciphertext_len = IV_SIZE_IN_BYTES + plaintext_len + TAG_SIZE_IN_BYTES;

    auto *ciphertext = (unsigned char *) malloc(*ciphertext_len);
    if (!iv) {
        RAND_bytes(ciphertext, IV_SIZE_IN_BYTES);
    } else {
        memcpy(ciphertext, iv, IV_SIZE_IN_BYTES);
    }
    iv = ciphertext;

    if (aes_256_gcm_encrypt(plaintext, (int) plaintext_len,
                            aad, (int) aad_len,
                            key,
                            iv, IV_SIZE_IN_BYTES,
                            ciphertext + IV_SIZE_IN_BYTES,
                            ciphertext + IV_SIZE_IN_BYTES + plaintext_len) >= 0) {
        return ciphertext;
    } else {
        *ciphertext_len = 0;
        return nullptr;
    }
}

unsigned char *aes_256_gcm_encrypt(unsigned char *plaintext, size_t plaintext_len,
                                   unsigned char *aad, size_t aad_len,
                                   unsigned char *key,
                                   size_t *ciphertext_len) {
    return aes_256_gcm_encrypt(plaintext, (int) plaintext_len, aad, (int) aad_len, key, nullptr, ciphertext_len);
}

unsigned char *aes_256_gcm_decrypt(unsigned char *ciphertext, size_t ciphertext_len,
                                   unsigned char *aad, size_t aad_len,
                                   unsigned char *key,
                                   size_t *plaintext_len) {
    if (ciphertext_len < IV_SIZE_IN_BYTES + TAG_SIZE_IN_BYTES) {
        return nullptr;
    }
    *plaintext_len = ciphertext_len - IV_SIZE_IN_BYTES - TAG_SIZE_IN_BYTES;

    auto *plaintext = (unsigned char *) malloc(*plaintext_len);
    unsigned char *iv = ciphertext;

    if (aes_256_gcm_decrypt(ciphertext + IV_SIZE_IN_BYTES, (int) *plaintext_len,
                            aad, (int) aad_len,
                            ciphertext + ciphertext_len - TAG_SIZE_IN_BYTES,
                            key,
                            iv, IV_SIZE_IN_BYTES,
                            plaintext) >= 0) {
        return plaintext;
    } else {
        *plaintext_len = 0;
        return nullptr;
    }
}
