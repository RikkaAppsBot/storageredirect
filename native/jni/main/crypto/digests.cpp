#include <cstdio>
#include <openssl/ossl_typ.h>
#include <openssl/evp.h>
#include <openssl/sha.h>

void sha256(const unsigned char *message, size_t message_len, unsigned char *digest, unsigned int *digest_len) {
    EVP_MD_CTX *mdctx;

    if ((mdctx = EVP_MD_CTX_create()) == nullptr)
        return;

    if (1 != EVP_DigestInit_ex(mdctx, EVP_sha256(), nullptr))
        return;

    if (1 != EVP_DigestUpdate(mdctx, message, message_len))
        return;

    if (1 != EVP_DigestFinal_ex(mdctx, digest, digest_len))
        return;

    EVP_MD_CTX_destroy(mdctx);
}