// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_REQUESTHEADER_REQUESTHANDLER_H_
#define FLATBUFFERS_GENERATED_REQUESTHEADER_REQUESTHANDLER_H_

#include "flatbuffers/flatbuffers.h"

namespace RequestHandler {

struct RequestHeader;
struct RequestHeaderBuilder;

struct RequestHeader FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef RequestHeaderBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_ACTION = 4,
    VT_TOKEN = 6
  };
  uint8_t action() const {
    return GetField<uint8_t>(VT_ACTION, 0);
  }
  const flatbuffers::String *token() const {
    return GetPointer<const flatbuffers::String *>(VT_TOKEN);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyField<uint8_t>(verifier, VT_ACTION) &&
           VerifyOffsetRequired(verifier, VT_TOKEN) &&
           verifier.VerifyString(token()) &&
           verifier.EndTable();
  }
};

struct RequestHeaderBuilder {
  typedef RequestHeader Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_action(uint8_t action) {
    fbb_.AddElement<uint8_t>(RequestHeader::VT_ACTION, action, 0);
  }
  void add_token(flatbuffers::Offset<flatbuffers::String> token) {
    fbb_.AddOffset(RequestHeader::VT_TOKEN, token);
  }
  explicit RequestHeaderBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  RequestHeaderBuilder &operator=(const RequestHeaderBuilder &);
  flatbuffers::Offset<RequestHeader> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<RequestHeader>(end);
    fbb_.Required(o, RequestHeader::VT_TOKEN);
    return o;
  }
};

inline flatbuffers::Offset<RequestHeader> CreateRequestHeader(
    flatbuffers::FlatBufferBuilder &_fbb,
    uint8_t action = 0,
    flatbuffers::Offset<flatbuffers::String> token = 0) {
  RequestHeaderBuilder builder_(_fbb);
  builder_.add_token(token);
  builder_.add_action(action);
  return builder_.Finish();
}

inline flatbuffers::Offset<RequestHeader> CreateRequestHeaderDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    uint8_t action = 0,
    const char *token = nullptr) {
  auto token__ = token ? _fbb.CreateString(token) : 0;
  return RequestHandler::CreateRequestHeader(
      _fbb,
      action,
      token__);
}

inline const RequestHandler::RequestHeader *GetRequestHeader(const void *buf) {
  return flatbuffers::GetRoot<RequestHandler::RequestHeader>(buf);
}

inline const RequestHandler::RequestHeader *GetSizePrefixedRequestHeader(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<RequestHandler::RequestHeader>(buf);
}

inline bool VerifyRequestHeaderBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<RequestHandler::RequestHeader>(nullptr);
}

inline bool VerifySizePrefixedRequestHeaderBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<RequestHandler::RequestHeader>(nullptr);
}

inline void FinishRequestHeaderBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<RequestHandler::RequestHeader> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedRequestHeaderBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<RequestHandler::RequestHeader> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace RequestHandler

#endif  // FLATBUFFERS_GENERATED_REQUESTHEADER_REQUESTHANDLER_H_
