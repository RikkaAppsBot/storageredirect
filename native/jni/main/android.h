#pragma once

namespace android {

    int GetApiLevel();

    int GetPreviewApiLevel();

    bool IsFuse();

    bool IsSdcardfsUsed();
}