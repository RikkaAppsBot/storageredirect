#include <cstring>
#include <cstdlib>
#include <csignal>
#include <linux/sched.h>
#include <unistd.h>
#include <wait.h>
#include <ctime>
#include <string>
#include <sys/stat.h>
#include <vector>
#include <fstream>
#include <fcntl.h>

#include <config.h>
#include <logging.h>
#include <misc.h>
#include <wrap.h>
#include <sys/system_properties.h>
#include <version.h>
#include <mntent.h>

#include "setproctitle.h"
#include "isolation.h"
#include "selinux.h"
#include "license.h"
#include "exitcode.h"
#include "dex_writer.h"
#include "misc.h"
#include "cgroup.h"
#include "enhance.h"
#include "android.h"
#include "request_handler.h"

#ifdef DEBUG
#define JAVA_DEBUGGABLE
#endif

#define perrorf(...) fprintf(stderr, __VA_ARGS__)

struct timespec ts;

static void logcat(time_t now) {
    char command[BUFSIZ];
    char time[BUFSIZ];
    struct tm *tm = localtime(&now);
    strftime(time, sizeof(time), "%m-%d %H:%M:%S.000", tm);
    printf("------\n");
    sprintf(command, "logcat -b crash -t '%s' -d", time);
    printf("[command] %s\n", command);
    fflush(stdout);
    system(command);
    fflush(stdout);
    printf("------\n");
    sprintf(command,
            "logcat -b main -t '%s' -d -s app_process StorageRedirect StorageRedirectNative StorageRedirectInject",
            time);
    printf("[command] %s\n", command);
    fflush(stdout);
    system(command);
    fflush(stdout);
    printf("------\n");
    fflush(stdout);
}

static const char *get_app_process() {
#ifdef __LP64__
    if (access("/system/bin/app_process64_original", R_OK) == 0) {
        return "/system/bin/app_process64_original";
    } else if (access("/system/bin/app_process64_init", R_OK) == 0) {
        return "/system/bin/app_process64_init";
    } else {
        return "/system/bin/app_process64";
    }
#else
    if (access("/system/bin/app_process32_original", R_OK) == 0) {
        return "/system/bin/app_process32_original";
    } else if (access("/system/bin/app_process32_init", R_OK) == 0) {
        return "/system/bin/app_process32_init";
    } else {
        return "/system/bin/app_process32";
    }
#endif
}

static int should_relocate_app_process() {
    return android::GetApiLevel() < 29;
}

static int java_server(const char *dex_path, bool &run_cleaner) {
    if (access(dex_path, R_OK) != 0) {
        perrorf("fatal: can't access %s.\n", dex_path);
        fflush(stderr);
        exit(EXIT_FATAL_CANNOT_ACCESS_DEX);
    }

    auto app_process = get_app_process();

    if (access(app_process, F_OK) != 0) {
        LOGE("can't find %s", app_process);

        logcat(ts.tv_sec);
        exit(EXIT_FATAL_APP_PROCESS);
    }

    printf("info: app_process is %s\n", app_process);
    fflush(stdout);

    auto file = BIN_PATH "/app_process";

    _remove(file);
    if (should_relocate_app_process()) {
        printf("info: copy app_process\n");
        fflush(stdout);

        if (copyfile(app_process, file) != 0) {
            PLOGE("copyfile %s %s", app_process, file);
        }
        _chmod(file, 0700);
        //setfilecon(file, "u:object_r:system_file:s0");
    } else {
        _symlink(app_process, file);
        //lsetfilecon(file, "u:object_r:system_file:s0");
    }

    _chmod(dex_path, 0600);
    //setfilecon(dex_path, "u:object_r:system_file:s0");

    if (setenv("CLASSPATH", dex_path, 1)) {
        LOGE("can't set CLASSPATH\n");
        exit(EXIT_FATAL_SET_CLASSPATH);
    }

    printf("info: starting server...\n");
    fflush(stdout);

    // do license check
    License::daemon();

    foreach_proc([](pid_t pid) {
        if (pid == getpid()) return false;

        char name[1024];
        if (get_proc_name(pid, name, 1024) != 0) return false;

        if (strcmp(PROCESS_NAME_JAVA, name) != 0
            && strcmp(PROCESS_NAME_JAVA_CLEANER, name) != 0
            && strcmp("storage_redirect_server", name) != 0
            && strcmp("storage_redirect_helper_server", name) != 0)
            return false;

        if (kill(pid, SIGKILL) == 0)
            LOGI("killed %d (%s)\n", pid, name);
        else
            LOGW("failed to kill %d (%s)\n", pid, name);

        return false;
    });

#define ARG(v) std::vector<char *> v; \
    char buf_##v[PATH_MAX];
#define ARG_PUSH(v, arg) v.push_back(strdup(arg));
#define ARG_END(v) v.push_back(nullptr);

#define ARG_PUSH_FMT(v, fmt, ...) snprintf(buf_##v, PATH_MAX, fmt, __VA_ARGS__); \
    ARG_PUSH(v, buf_##v)

#ifdef JAVA_DEBUGGABLE
#define ARG_PUSH_DEBUG_VM_PARAMS(v) \
    if (android::GetApiLevel() >= 30) { \
        ARG_PUSH(v, "-Xcompiler-option"); \
        ARG_PUSH(v, "--debuggable"); \
        ARG_PUSH(v, "-XjdwpProvider:adbconnection"); \
        ARG_PUSH(v, "-XjdwpOptions:suspend=n,server=y"); \
    } else if (android::GetApiLevel() >= 28) { \
        ARG_PUSH(v, "-Xcompiler-option"); \
        ARG_PUSH(v, "--debuggable"); \
        ARG_PUSH(v, "-XjdwpProvider:internal"); \
        ARG_PUSH(v, "-XjdwpOptions:transport=dt_android_adb,suspend=n,server=y"); \
    } else { \
        ARG_PUSH(v, "-Xcompiler-option"); \
        ARG_PUSH(v, "--debuggable"); \
        ARG_PUSH(v, "-agentlib:jdwp=transport=dt_android_adb,suspend=n,server=y"); \
    }
#define ARG_PUSH_DEBUG_PARAMS(v) ARG_PUSH(v, "--debug")
#else
#define ARG_PUSH_DEBUG_VM_PARAMS(v)
#define ARG_PUSH_DEBUG_PARAMS(v)
#endif
    // -------------- legacy cleaner --------------

    /*if (run_cleaner) {
        run_cleaner = false;

        ARG(argv1)
        ARG_PUSH(argv1, file)
        ARG_PUSH_FMT(argv1, "-Djava.class.path=%s", dex_path)
        ARG_PUSH_DEBUG_VM_PARAMS(argv1)
        ARG_PUSH(argv1, "/system/bin")
        ARG_PUSH(argv1, "--nice-name=" PROCESS_NAME_JAVA_CLEANER)
        ARG_PUSH(argv1, SERVER_CLASS)
        ARG_PUSH(argv1, "--run-as-legacy-cleaner")
        ARG_PUSH_DEBUG_PARAMS(argv1)
        ARG_END(argv1);

        execv_sync(file, argv1.data());
    }*/

    // -------------- server --------------

    ARG(argv2);
    ARG_PUSH(argv2, file)
    ARG_PUSH_FMT(argv2, "-Djava.class.path=%s", dex_path)
    ARG_PUSH_DEBUG_VM_PARAMS(argv2)
    ARG_PUSH(argv2, "/system/bin")
    ARG_PUSH(argv2, "--nice-name=" PROCESS_NAME_JAVA)
    ARG_PUSH(argv2, SERVER_CLASS)
    ARG_PUSH_FMT(argv2, "--parent-pid=%d", getpid())
    ARG_PUSH_FMT(argv2, "--token=%s", RequestHandler::getSocketToken())
    ARG_PUSH_FMT(argv2, "--socket-address=%s", RequestHandler::getSocketAddress())
    ARG_PUSH_FMT(argv2, "--riru-version=%d", Enhance::getRiruVersion())
    ARG_PUSH_FMT(argv2, "--riru-version-name=%s", Enhance::getRiruVersionName())
    ARG_PUSH_FMT(argv2, "--module-version=%d", Enhance::getModuleVersion())
    ARG_PUSH_FMT(argv2, "--module-version-name=%s", Enhance::getModuleVersionName())
    ARG_PUSH_DEBUG_PARAMS(argv2)
    ARG_END(argv2);

    return execv_sync(file, argv2.data());
}

[[noreturn]] static void daemon_java_server(const char *dex_path) {
    int exit_code;
    bool run_cleaner = true;
    while (true) {
        exit_code = java_server(dex_path, run_cleaner);
        if (exit_code == 55 || exit_code == 56) {
            if (exit_code == 56) {
                if (remove(ROOT_PATH) == 0)
                    LOGI("server files deleted");
                else
                    PLOGE("remove %s", ROOT_PATH);
            }

            LOGI("server received exit");
            exit(0);
        }
        LOGI("server exited with code %d, restarting...", exit_code);

#ifdef DEBUG
        if (exit_code != 52) {
            sleep(10);
            LOGD("wait 10s");
        }
#endif
    }
}

static void ensure_paths() {
    _chmod(BIN_PATH, 0700);
}

static void clear_old_files(const char *dex_path, const char *daemon_path) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = _opendir(BIN_PATH))) {
        std::vector<char *> all_dex_paths;
        std::vector<char *> all_daemon_paths;
        while ((entry = _readdir(dir))) {
            if (entry->d_type == DT_REG) {
                int v;
                if (sscanf(entry->d_name, "server-v%d.dex", &v) == 1) {
                    all_dex_paths.push_back(strdup(
                            (std::string(BIN_PATH "/") + entry->d_name).c_str()));
                } else if (sscanf(entry->d_name, "daemon-v%d", &v) == 1) {
                    all_daemon_paths.push_back(strdup(
                            (std::string(BIN_PATH "/") + entry->d_name).c_str()));
                }
            }
        }
        for (auto path : all_dex_paths) {
            if (strcmp(path, dex_path) != 0) {
                if (_remove(path) == 1) {
                    printf("info: removed old dex successfully %s", path);
                    fflush(stdout);
                }
            }
            free(path);
        }
        for (auto path : all_daemon_paths) {
            if (strcmp(path, daemon_path) != 0) {
                if (_remove(path) == 1) {
                    printf("info: removed old daemon successfully %s", path);
                    fflush(stdout);
                }
            }
            free(path);
        }
        closedir(dir);
    }
}

static int switch_cgroup() {
    int s_cuid, s_cpid;
    int spid = getpid();

    if (cgroup::get_cgroup(spid, &s_cuid, &s_cpid) != 0) {
        printf("warn: can't read cgroup, maybe it has been switched before?\n");
        fflush(stdout);
        return -1;
    }

    printf("info: cgroup is /uid_%d/pid_%d\n", s_cuid, s_cpid);
    fflush(stdout);

    if (cgroup::switch_cgroup(spid, -1, -1) != 0) {
        printf("warn: can't switch cgroup\n");
        fflush(stdout);
        return -1;
    }

    if (cgroup::get_cgroup(spid, &s_cuid, &s_cpid) != 0) {
        printf("info: switch cgroup succeeded\n");
        fflush(stdout);
        return 0;
    }

    printf("warn: can't switch self, current cgroup is /uid_%d/pid_%d\n", s_cuid, s_cpid);
    fflush(stdout);
    return -1;
}

static bool has_daemon_process() {
    static pid_t daemon_pid;
    daemon_pid = -1;

    foreach_proc([](pid_t pid) {
        if (pid == getpid()) return false;

        char buf[64];
        snprintf(buf, 64, "/proc/%d/cmdline", pid);

        int fd = open(buf, O_RDONLY);
        if (fd > 0) {
            memset(buf, 0, 64);
            if (read(fd, buf, 64) > 0 && strcmp(PROCESS_NAME, buf) == 0) {
                daemon_pid = pid;
            }
            close(fd);
        }

        return daemon_pid != -1;
    });

    return daemon_pid != -1;
}

int main(int argc, char **argv) {
    // check dex path
    int i;
    int from_module = 0;
    int from_user = 0;
    int from_boot = 0;
    for (i = 0; i < argc; ++i) {
        if (strncmp(argv[i], "--from-user", 11) == 0) {
            from_user = 1;
        } else if (strncmp(argv[i], "--from-boot", 11) == 0) {
            from_boot = 1;
        } else if (strncmp(argv[i], "--from-module", 13) == 0) {
            from_module = 1;
        }
    }

    if (from_boot && has_daemon_process()) {
        LOGW("start from boot receiver, but already running");
        exit(0);
    }

    switch_cgroup();

    // set proc name
    spt_init(argc, argv);
    setproctitle(PROCESS_NAME);

    // read enhance module versions
    Enhance::waitForZygote(from_module);

    // check mount namespace support
    if (access("/proc/1/ns/mnt", F_OK) != 0) {
        perrorf("fatal: no mount namespace isolation support.\n");
        fflush(stderr);
        exit(EXIT_FATAL_NO_MNT_NS);
    }

    printf("info: api %d (preview %d)\n", android::GetApiLevel(), android::GetPreviewApiLevel());
    fflush(stdout);

    // kill old processes
    printf("info: killing running processes...\n");
    fflush(stdout);

    foreach_proc([](pid_t pid) {
        if (pid == getpid()) return false;

        char name[1024];
        if (get_proc_name(pid, name, 1024) != 0) return false;

        if (strcmp(PROCESS_NAME, name) != 0
            && strcmp(PROCESS_NAME_WORKER, name) != 0
            && strcmp("storage_redirect", name) != 0
            && strcmp("storage_redirect_worker", name) != 0)
            return false;

        if (kill(pid, SIGKILL) == 0)
            LOGI("killed %d (%s)\n", pid, name);
        else
            LOGW("failed to kill %d (%s)\n", pid, name);

        return false;
    });

    // write dex
    if (dex_writer::write_dex() == -1) {
        perrorf("fatal: can't write dex.\n");
        fflush(stderr);
        exit(EXIT_FATAL_WRITE_DEX);
    }

    auto daemon_path = BIN_PATH "/daemon-v" VERSION_NAME;
    auto dex_path = "/data/adb/storage-isolation/bin/server-v" VERSION_NAME ".dex";

    if (access(dex_path, R_OK) != 0) {
        perrorf("fatal: can't access %s.\n", dex_path);
        fflush(stderr);
        return EXIT_FATAL_CANNOT_ACCESS_DEX;
    }

    // write server version (for module installer only)
    // TODO move to /dev
    write_file("/data/adb/storage-isolation/.server_version", VERSION_NAME, strlen(VERSION_NAME));

    printf("info: daemon path is %s\n", daemon_path);
    printf("info: dex path is %s\n", dex_path);
    fflush(stdout);

    printf("info: cleaning old files\n");
    fflush(stdout);

    ensure_paths();
    clear_old_files(dex_path, daemon_path);
    fflush(stdout);

    clock_gettime(CLOCK_REALTIME, &ts);

    // init
    pid_t pid = fork();
    if (pid == 0) {
        daemon(0, 0);

        // start request handler
        RequestHandler::start();

        // start child
        daemon_java_server(dex_path);
    } else if (pid != -1) {
        if (from_module)
            return EXIT_SUCCESS;

        signal(SIGCHLD, SIG_IGN);
        signal(SIGHUP, SIG_IGN);
        printf("info: process forked, pid=%d\n", pid);
        fflush(stdout);
        printf("info: checking " PROCESS_NAME " start...\n");
        fflush(stdout);
        int count = 0;
        while (!has_daemon_process()) {
            fflush(stdout);
            usleep(200 * 1000);
            count++;
            if (count >= 50) {
                perrorf("warn: timeout but can't get pid of " PROCESS_NAME ".\n");
                logcat(ts.tv_sec);
                return EXIT_WARN_START_TIMEOUT;
            }
        }
        count = 0;
        while (has_daemon_process()) {
            printf("info: checking " PROCESS_NAME " stability...\n");
            fflush(stdout);
            sleep(1);
            count++;
            if (count >= 3) {
                printf("info: " PROCESS_NAME " started.\n");
                fflush(stdout);
                logcat(ts.tv_sec);
                return EXIT_SUCCESS;
            }
        }
        perrorf("warn: "
                        PROCESS_NAME
                        " stopped after started.\n");
        logcat(ts.tv_sec);
        return EXIT_WARN_SERVER_STOP;
    } else {
        return EXIT_FATAL_FORK;
    }
}