#ifndef CORE_H
#define CORE_H

#include <sys/types.h>
#include <flatbuffers/request_check_process_generated.h>

namespace Isolation {

    int handleProcess(const RequestHandler::CheckProcessRequest *request);
}

#endif