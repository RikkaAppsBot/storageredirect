#pragma once

namespace RequestHandler {

    const char *getSocketToken();

    const char *getSocketAddress();

    void start();
}