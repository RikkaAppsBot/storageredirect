LOCAL_PATH := $(call my-dir)

########################
# daemon
########################

include $(CLEAR_VARS)

LOCAL_MODULE     := daemon
LOCAL_STATIC_LIBRARIES := utils flatbuffers nativehelper_header_only
LOCAL_C_INCLUDES := \
	jni/include \
	$(EXT_PATH)/include
LOCAL_CLAGS += -Werror=format
LOCAL_LDLIBS += -ldl -llog
LOCAL_LDFLAGS := -Wl,--hash-style=both

LOCAL_SRC_FILES:= main.cpp \
    android.cpp \
	isolation.cpp \
	setproctitle.c \
	license.cpp \
	dex_writer.cpp \
	pmparser.c \
	cgroup.cpp \
	enhance.cpp \
	request_handler.cpp \
	signing/signing_block.cpp

ifeq ($(NDK_DEBUG),1)
LOCAL_CFLAGS += -DDEBUG -DNO_LICENSE_CHECK
else
LOCAL_CFLAGS += -DNO_DEBUG_LOG
endif

ifeq ($(TOOLCHAIN_LLVM_HIKARI),1)
LOCAL_CFLAGS += -mllvm -enable-bcfobf -mllvm -enable-cffobf -mllvm -enable-strcry
endif

ifeq ($(IDE),1)
LOCAL_C_INCLUDES += jni/include_dummy
else
LOCAL_C_INCLUDES += jni/include_dex
endif

include $(BUILD_EXECUTABLE)

########################
# libhelper
########################

include $(CLEAR_VARS)

LOCAL_MODULE := helper
LOCAL_STATIC_LIBRARIES := curl crypto ssl flatbuffers nativehelper_header_only utils
LOCAL_C_INCLUDES := \
    jni/include \
    $(EXT_PATH)/include
LOCAL_LDLIBS += -ldl -llog -lz
LOCAL_LDFLAGS := -Wl,--hash-style=both

LOCAL_SRC_FILES:= helper/main.cpp \
	helper/process_monitor.cpp \
	helper/license.cpp \
	helper/license_file.cpp \
	helper/moe_shizuku_storageredirect_server_NativeHelper.cpp \
	android.cpp \
    crypto/aes.cpp \
    crypto/base64.c \
    crypto/digests.cpp

ifeq ($(NDK_DEBUG),1)
LOCAL_CFLAGS += -DDEBUG -DNO_LICENSE_CHECK
else
LOCAL_CFLAGS += -DNO_DEBUG_LOG
endif

ifeq ($(TOOLCHAIN_LLVM_HIKARI),1)
LOCAL_CFLAGS += -mllvm -enable-bcfobf -mllvm -enable-cffobf -mllvm -enable-strcry
endif

include $(BUILD_SHARED_LIBRARY)

########################
# prefab
########################

ifeq ($(CMD_BUILD),1)
$(call import-add-path, .cxx/ndkBuild/release/prefab/$(TARGET_ARCH_ABI))
endif

$(call import-module, prefab/curl)
$(call import-module, prefab/nativehelper)
$(call import-module, prefab/flatbuffers)