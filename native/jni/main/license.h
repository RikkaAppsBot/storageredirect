#ifndef LICENSE_CHECK_H
#define LICENSE_CHECK_H

namespace License {

    constexpr int INVALID_BAD_APK = 11;
    constexpr int INVALID_LICENSE = 21;

    void SetInvalidLicense(int reason);

    int code();

    bool valid();

    void daemon();
}
#endif //LICENSE_CHECK_H
