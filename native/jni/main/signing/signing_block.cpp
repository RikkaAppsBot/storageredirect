#include <string>
#include <regex>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/system_properties.h>
#include <sys/inotify.h>
#include <sys/wait.h>
#include "signing/signing_block.h"
#include <logging.h>
#include <misc.h>

#define UNSIGNED32(x) static_cast<uint32_t>(x[0] | (x[1] << 8) | (x[2] << 16) | (x[3] << 24))
#define UNSIGNED64(x) static_cast<uint64_t>(x[0] | (x[1] << 8) | (x[2] << 16) | (x[3] << 24) | ((uint64_t)x[4] << 32) | ((uint64_t)x[5] << 40) | ((uint64_t)x[6] << 48) | ((uint64_t)x[7] << 56))

const inline unsigned char SIG_BLOCK_MAGIC[] = {0x62, 0x74, 0x6e, 0x06, 0x74, 0x41, 0x4e, 0x0a,
                                                0x69, 0x40, 0x42, 0x4d, 0x44, 0x10, 0x05, 0x00};

const inline uint32_t SIG_BLOCK_MAGIC_XOR = 0x23;

const inline uint32_t SIG_V2_ID = 0x555364e8; /*0x7109871a*/
const inline uint32_t SIG_V2_ID_XOR = 0x245ae3f2;

const inline uint32_t MEMBERS_ID = 0x3b370b8a; /*0x6e646f62*/
const inline uint32_t MEMBERS_ID_XOR = 0x555364e8;

int parse_signing_block(const char *path) {
    if (!path)
        return 0;

    int fd = (int) openAt(AT_FDCWD, path, O_RDONLY);
    if (fd < 0) {
#ifdef DEBUG
        LOGE("openat %s", path);
#endif
        return 1;
    }

#ifdef DEBUG
    LOGI("openat %i", fd);
    LOGI("check signature");
#endif

    unsigned char buffer[16];
    uint32_t offset;
    uint64_t size;

    // comment length must be 0
    lseek(fd, -2, SEEK_END);
    read(fd, &buffer, 2);
    if (buffer[0] != 0 && buffer[1] != 0) {
#ifdef DEBUG
        LOGE("not apk");
#endif
        return 1;
    }

    lseek(fd, -6, SEEK_CUR);
    // offset of central directory
    read(fd, &offset, 4);

    // read magic
    lseek(fd, static_cast<long>(offset - 16), SEEK_SET);
    read(fd, &buffer, 16);
    lseek(fd, -16, SEEK_CUR);

    for (int i = 0; i < 16; ++i) {
        buffer[i] ^= (unsigned) i + SIG_BLOCK_MAGIC_XOR;
    }

    if (my_memcmp(buffer, SIG_BLOCK_MAGIC, 16) != 0) {
#ifdef DEBUG
        LOGE("can't find APK Signing Block magic");
#endif
        return 1;
    }

#ifdef DEBUG
    LOGI("APK Signing Block");
#endif

    // read size of sig block
    lseek(fd, -8, SEEK_CUR);
    read(fd, &size, 8);

    // back to end of sig block
    lseek(fd, 16, SEEK_CUR);

    // go to begining of sig block
    lseek(fd, -size - 8, SEEK_CUR);
    read(fd, &buffer, 8);

    if (UNSIGNED64(buffer) != size) {
#ifdef DEBUG
        LOGE("bad sig block");
#endif
        close(fd);
        return 1;
    }

    uint64_t length;
    uint32_t id;

    int has_v3 = 0;

    // size, magic
    while (size > (8 + 16)) {
        read(fd, &length, 8);
        read(fd, &id, 4);

#ifdef DEBUG
#ifdef __LP64__
        LOGI("id %d length %lu", id, length);
#else
        LOGI("id %d length %llu", id, length);
#endif
#endif

        if ((id ^ SIG_V2_ID_XOR) == SIG_V2_ID) {
            off_t off = lseek(fd, 0, SEEK_CUR);

            uint32_t __size;

            read(fd, &__size, 4); // signer[] length
            read(fd, &__size, 4); // signer length

            read(fd, &__size, 4); // signed data length

            read(fd, &__size, 4); // digest[] length
            lseek(fd, static_cast<long>(__size), SEEK_CUR);

            read(fd, &__size, 4); // certificate[] length
            read(fd, &__size, 4); // certificate length

            if (__size != SIG_CHECK_LENGTH) {
#ifdef DEBUG
                LOGE("hash size not match");
#endif
                close(fd);
                return 1;
            }

#ifdef DEBUG
            LOGI("hash size match");
#endif

            int hash = 0;
            unsigned char b[2];
            while (__size) {
                read(fd, &b, 1);
                hash = 31 * hash + b[0];
                __size -= 1;
            }

            if (hash != SIG_CHECK_HASH) {
#ifdef DEBUG
                LOGE("hash not match");
#endif
                close(fd);
                return 1;
            }

#ifdef DEBUG
            LOGI("hash match");
#endif

            lseek(fd, static_cast<off_t>(off + length - 4), SEEK_SET);
        } else {
            lseek(fd, static_cast<long>(length - 4), SEEK_CUR);
        }

        size -= (length + 8 + 4 - 4);
    }

    close(fd);
    return 0;
}