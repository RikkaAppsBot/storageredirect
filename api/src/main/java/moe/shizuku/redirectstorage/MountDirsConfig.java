package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class MountDirsConfig implements Parcelable {

    public static final int FLAG_ALLOW_TEMPLATES = 1;
    public static final int FLAG_ALLOW_CUSTOM = 1 << 1;

    public int flags;
    public Set<String> templatesIds;
    public Set<String> customDirs;

    public MountDirsConfig(int flags, Set<String> templatesIds, Set<String> customDirs) {
        this.flags = flags;
        this.templatesIds = templatesIds;
        this.customDirs = customDirs;
    }

    public boolean isTemplatesAllowed() {
        return (flags & FLAG_ALLOW_TEMPLATES) > 0;
    }

    public boolean isCustomAllowed() {
        return (flags & FLAG_ALLOW_CUSTOM) > 0;
    }

    public boolean isNone() {
        return (flags & (FLAG_ALLOW_TEMPLATES | FLAG_ALLOW_CUSTOM)) == 0;
    }

    private String getTypeCodeName() {
        if (isNone()) {
            return "NONE";
        } else if (isTemplatesAllowed() && isCustomAllowed()) {
            return "TEMPLATES | CUSTOM";
        } else if (isTemplatesAllowed()) {
            return "TEMPLATES";
        } else if (isCustomAllowed()) {
            return "CUSTOM";
        } else {
            return "UNKNOWN";
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "MountDirsConfig{" +
                "type=" + getTypeCodeName() +
                ", templatesIds=" + templatesIds +
                ", customDirs=" + customDirs +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MountDirsConfig that = (MountDirsConfig) o;
        return flags == that.flags &&
                Objects.equals(templatesIds, that.templatesIds) &&
                Objects.equals(customDirs, that.customDirs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flags, templatesIds, customDirs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.flags);
        dest.writeStringList(this.templatesIds == null ? null : new ArrayList<>(this.templatesIds));
        dest.writeStringList(this.customDirs == null ? null : new ArrayList<>(this.customDirs));
    }

    protected MountDirsConfig(Parcel in) {
        this.flags = in.readInt();
        this.templatesIds = createStringSet(in);
        this.customDirs = createStringSet(in);
    }

    private static Set<String> createStringSet(Parcel in) {
        List<String> list = in.createStringArrayList();
        if (list != null) {
            return new HashSet<>(list);
        } else {
            return null;
        }
    }

    public static final Creator<MountDirsConfig> CREATOR = new Creator<MountDirsConfig>() {
        @Override
        public MountDirsConfig createFromParcel(Parcel source) {
            return new MountDirsConfig(source);
        }

        @Override
        public MountDirsConfig[] newArray(int size) {
            return new MountDirsConfig[size];
        }
    };
}
