package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;

public class MountDirsTemplate implements Parcelable {

    public String id;
    public String title;
    public List<String> list;
    private transient List<RedirectPackageInfo> packages;

    private MountDirsTemplate(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.list = in.createStringArrayList();
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof MountDirsTemplate)) {
            return false;
        }
        final MountDirsTemplate other = (MountDirsTemplate) o;
        return Objects.equals(this.id, other.id);
    }

    @Override
    @NonNull
    public String toString() {
        return "MountDirsTemplate[" +
                "id=" + id + ", " +
                "title=" + title + ", " +
                "list=" + list +
                "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeStringList(list);
    }

    public static final Creator<MountDirsTemplate> CREATOR = new Creator<MountDirsTemplate>() {
        @Override
        public MountDirsTemplate createFromParcel(Parcel in) {
            return new MountDirsTemplate(in);
        }

        @Override
        public MountDirsTemplate[] newArray(int size) {
            return new MountDirsTemplate[size];
        }
    };

}
