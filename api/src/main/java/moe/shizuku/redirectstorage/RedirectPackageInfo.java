package moe.shizuku.redirectstorage;

import android.content.pm.PackageInfo;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rikka.storageisolation.StorageIsolationManager;

@SuppressWarnings("WeakerAccess")
public class RedirectPackageInfo implements Parcelable {

    public static final int PERMISSION_READ = 1;
    public static final int PERMISSION_WRITE = 1 << 1;

    public final String packageName;
    public int userId;
    public boolean enabled;
    public transient PackageInfo packageInfo;
    public transient boolean systemApp;
    public transient byte storagePermission;
    public transient boolean storagePermissionGranted;
    public String redirectTarget;
    public MountDirsConfig mountDirsConfig;

    @Deprecated
    protected String mountDirsTemplateId;

    public Set<String> mountDirs;

    public boolean readPermissionOnly() {
        return this.storagePermission == PERMISSION_READ;
    }

    public boolean readPermission() {
        return (this.storagePermission & PERMISSION_READ) > 0;
    }

    public boolean writePermission() {
        return (this.storagePermission & PERMISSION_WRITE) > 0;
    }

    @Override
    @NonNull
    public String toString() {
        return "RedirectPackageInfo{" +
                "enabled='" + enabled + '\'' +
                ", packageName='" + packageName + '\'' +
                ", userId=" + userId +
                ", redirectTarget='" + redirectTarget + '\'' +
                ", mountDirs=" + mountDirs +
                ", mountDirsTemplateId='" + mountDirsTemplateId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RedirectPackageInfo that = (RedirectPackageInfo) o;

        return userId == that.userId
                && packageName.equals(that.packageName);
    }

    @Override
    public int hashCode() {
        int result = packageName.hashCode();
        result = 31 * result + userId;
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageName);
        dest.writeInt(this.userId);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
        dest.writeString(this.redirectTarget);
        dest.writeParcelable(this.packageInfo, flags);
        dest.writeByte(this.systemApp ? (byte) 1 : (byte) 0);
        dest.writeByte(this.storagePermission);
        dest.writeByte(this.storagePermissionGranted ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mountDirsConfig, flags);
    }

    protected RedirectPackageInfo(Parcel in) {
        switch (StorageIsolationManager.getServerSdkVersion()) {
            case 1: {
                this.packageName = in.readString();
                this.userId = in.readInt();
                this.enabled = in.readByte() != 0;
                this.redirectTarget = in.readString();
                List<String> mountDirs = in.createStringArrayList();
                this.mountDirs = mountDirs == null ? null : new HashSet<>(mountDirs);
                this.packageInfo = in.readParcelable(PackageInfo.class.getClassLoader());
                this.systemApp = in.readByte() != 0;
                this.storagePermission = in.readByte();
                this.storagePermissionGranted = in.readByte() != 0;
                this.mountDirsTemplateId = in.readString();

                Set<String> templates, custom;
                int type;
                if (this.mountDirs != null) {
                    type = MountDirsConfig.FLAG_ALLOW_CUSTOM;
                    templates = null;
                    custom = new HashSet<>(this.mountDirs);
                } else if (this.mountDirsTemplateId != null) {
                    type = MountDirsConfig.FLAG_ALLOW_TEMPLATES;
                    templates = new HashSet<>();
                    templates.add(this.mountDirsTemplateId);
                    custom = null;
                } else {
                    type = 0;
                    templates = null;
                    custom = null;
                }
                this.mountDirsConfig = new MountDirsConfig(type, templates, custom);
                this.mountDirsTemplateId = null;
                break;
            }
            default:
            case 2: {
                this.packageName = in.readString();
                this.userId = in.readInt();
                this.enabled = in.readByte() != 0;
                this.redirectTarget = in.readString();
                this.packageInfo = in.readParcelable(PackageInfo.class.getClassLoader());
                this.systemApp = in.readByte() != 0;
                this.storagePermission = in.readByte();
                this.storagePermissionGranted = in.readByte() != 0;
                this.mountDirsConfig = in.readParcelable(MountDirsConfig.class.getClassLoader());
                break;
            }
        }
    }

    public static final Creator<RedirectPackageInfo> CREATOR = new Creator<RedirectPackageInfo>() {
        @Override
        public RedirectPackageInfo createFromParcel(Parcel source) {
            return new RedirectPackageInfo(source);
        }

        @Override
        public RedirectPackageInfo[] newArray(int size) {
            return new RedirectPackageInfo[size];
        }
    };
}
