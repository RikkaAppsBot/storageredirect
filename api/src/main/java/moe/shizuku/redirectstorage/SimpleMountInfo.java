package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.List;
import java.util.Objects;

public class SimpleMountInfo implements Parcelable {

    public String id;
    public int userId;
    public final String sourcePackage;
    public final String targetPackage;
    public final List<String> paths;
    public boolean enabled;

    public boolean valid() {
        return !TextUtils.isEmpty(sourcePackage)
                && !TextUtils.isEmpty(targetPackage)
                && paths != null
                && !TextUtils.isEmpty(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleMountInfo that = (SimpleMountInfo) o;
        return userId == that.userId &&
                Objects.equals(id, that.id);
    }

    public boolean equalsAllFields(Object o) {
        if (!equals(o)) return false;
        final SimpleMountInfo that = (SimpleMountInfo) o;
        return Objects.equals(this.sourcePackage, that.sourcePackage) &&
                Objects.equals(this.targetPackage, that.targetPackage) &&
                Objects.equals(this.paths, that.paths);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, sourcePackage, targetPackage, paths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.userId);
        dest.writeString(this.sourcePackage);
        dest.writeString(this.targetPackage);
        dest.writeStringList(this.paths);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
    }

    protected SimpleMountInfo(Parcel in) {
        this.id = in.readString();
        this.userId = in.readInt();
        this.sourcePackage = in.readString();
        this.targetPackage = in.readString();
        this.paths = in.createStringArrayList();
        this.enabled = in.readByte() == 1;
    }

    public static final Creator<SimpleMountInfo> CREATOR = new Creator<SimpleMountInfo>() {
        @Override
        public SimpleMountInfo createFromParcel(Parcel source) {
            return new SimpleMountInfo(source);
        }

        @Override
        public SimpleMountInfo[] newArray(int size) {
            return new SimpleMountInfo[size];
        }
    };
}
