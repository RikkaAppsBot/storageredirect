package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

import androidx.annotation.NonNull;

public class ObserverInfo implements Parcelable {

    /**
     * file need match this
     **/
    public final String mask;

    public final boolean callMediaScan;
    public final boolean showNotification;

    public String packageName;
    public int userId = -1;

    /**
     * source path, from /sdcard/Android/data/[package]/sdcard
     */
    public final String source;

    /**
     * target path, from /sdcard
     */
    public final String target;

    public final String description;

    /**
     * allow folders in source path
     */
    public final boolean allowChild;

    /**
     * allow file end with .tmp or .temp
     */
    public final boolean allowTemp;

    public boolean enabled;

    @Override
    @NonNull
    public String toString() {
        return "ObserverInfo{" +
                "enabled='" + enabled + '\'' +
                ", mask='" + mask + '\'' +
                ", allowChild=" + allowChild +
                ", allowTemp=" + allowTemp +
                ", callMediaScan=" + callMediaScan +
                ", showNotification=" + showNotification +
                ", packageName='" + packageName + '\'' +
                ", userId=" + userId +
                ", source='" + source + '\'' +
                ", target='" + target + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObserverInfo that = (ObserverInfo) o;
        boolean result = Objects.equals(packageName, that.packageName);
        // Source as id
        result &= Objects.equals(source, that.source);
        // Check if same user
        if (that.userId != -1 && userId != -1) {
            result &= that.userId == userId;
        }
        return result;
    }

    public boolean equalsAllFields(Object o) {
        if (!equals(o)) return false;
        final ObserverInfo that = (ObserverInfo) o;
        return Objects.equals(this.target, that.target) &&
                Objects.equals(this.mask, that.mask) &&
                this.allowChild == that.allowChild &&
                this.allowTemp == that.allowTemp &&
                this.showNotification == that.showNotification &&
                this.callMediaScan == that.callMediaScan &&
                Objects.equals(this.description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mask, callMediaScan, showNotification, packageName, source, target, allowChild, allowTemp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mask);
        dest.writeByte(this.callMediaScan ? (byte) 1 : (byte) 0);
        dest.writeByte(this.showNotification ? (byte) 1 : (byte) 0);
        dest.writeString(this.packageName);
        dest.writeInt(this.userId);
        dest.writeString(this.source);
        dest.writeString(this.target);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
        dest.writeString(this.description);
        dest.writeByte(this.allowChild ? (byte) 1 : (byte) 0);
        dest.writeByte(this.allowTemp ? (byte) 1 : (byte) 0);
    }

    protected ObserverInfo(Parcel in) {
        this.mask = in.readString();
        this.callMediaScan = in.readByte() != 0;
        this.showNotification = in.readByte() != 0;
        this.packageName = in.readString();
        this.userId = in.readInt();
        this.source = in.readString();
        this.target = in.readString();
        this.enabled = in.readByte() != 0;
        this.description = in.readString();
        this.allowChild = in.readByte() != 0;
        this.allowTemp = in.readByte() != 0;
    }

    public static final Creator<ObserverInfo> CREATOR = new Creator<ObserverInfo>() {
        @Override
        public ObserverInfo createFromParcel(Parcel source) {
            return new ObserverInfo(source);
        }

        @Override
        public ObserverInfo[] newArray(int size) {
            return new ObserverInfo[size];
        }
    };
}
