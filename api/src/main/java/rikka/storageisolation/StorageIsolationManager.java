package rikka.storageisolation;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import moe.shizuku.redirectstorage.IService;
import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

@SuppressWarnings({"WeakerAccess"})
public class StorageIsolationManager {

    public static final int SDK_CLIENT_VERSION = 2;
    public static final String PACKAGE_NAME = "moe.shizuku.redirectstorage";
    public static final String PERMISSION = "moe.shizuku.redirectstorage.permission.GET_CONFIGURATION";

    /**
     * Match any user.
     */
    public static final int USER_ALL = -1;

    /**
     * Request {@link android.content.pm.PackageInfo}.
     */
    public final static int GET_PACKAGE_INFO = 1 << 3;


    public final static int GET_INCLUDE_SOURCE = 1 << 4;

    private static final int BRIDGE_TRANSACTION_CODE = ('_' << 24) | ('_' << 16) | ('S' << 8) | 'R';
    private static final String BRIDGE_SERVICE_DESCRIPTOR = "android.app.IActivityManager";
    private static final String BRIDGE_SERVICE_NAME = "activity";
    private static final int BRIDGE_ACTION_GET_BINDER = 2;

    private static IService storageIsolationService;
    private static IBinder storageIsolationBinder;

    private static final Handler MAIN_HANDLER = new Handler(Looper.getMainLooper());

    private static final CopyOnWriteArraySet<ReceivedListener> RECEIVED_LISTENERS = new CopyOnWriteArraySet<>();
    private static final CopyOnWriteArraySet<DeathListener> DEATH_LISTENERS = new CopyOnWriteArraySet<>();

    public interface ReceivedListener {
        void onServiceBinderRecevied();
    }

    public interface DeathListener {
        void onServiceDead();
    }

    private static IBinder requestBinderFromBridge() {
        IBinder binder = ServiceManager.getService(BRIDGE_SERVICE_NAME);
        if (binder == null) return null;

        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        try {
            data.writeInterfaceToken(BRIDGE_SERVICE_DESCRIPTOR);
            data.writeInt(BRIDGE_ACTION_GET_BINDER);
            binder.transact(BRIDGE_TRANSACTION_CODE, data, reply, 0);
            reply.readException();
            IBinder received = reply.readStrongBinder();
            if (received != null) {
                return received;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            data.recycle();
            reply.recycle();
        }
        return null;
    }

    private static IBinder requestBinderFromServiceManager() {
        try {
            return ServiceManager.getService("storage_redirect");
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public static IBinder requestBinder() {
        if (storageIsolationBinder != null) {
            return storageIsolationBinder;
        }

        IBinder binder = requestBinderFromBridge();
        if (binder == null) {
            binder = requestBinderFromServiceManager();
        }

        if (binder == null || !binder.pingBinder()) {
            return null;
        }

        try {
            binder.linkToDeath(() -> {
                storageIsolationService = null;
                storageIsolationBinder = null;

                MAIN_HANDLER.post(() -> {
                    for (DeathListener listener : DEATH_LISTENERS) {
                        listener.onServiceDead();
                    }
                });
            }, 0);
        } catch (RemoteException ignored) {
            return null;
        }

        storageIsolationBinder = binder;

        MAIN_HANDLER.post(() -> {
            for (ReceivedListener listener : RECEIVED_LISTENERS) {
                listener.onServiceBinderRecevied();
            }
        });

        return binder;
    }

    public static void addDeathListener(@NonNull DeathListener listener) {
        if (listener == null) return;
        DEATH_LISTENERS.add(listener);
    }

    public static void removeDeathListener(@NonNull DeathListener listener) {
        if (listener == null) return;
        DEATH_LISTENERS.remove(listener);
    }

    public static void addReceivedListener(@NonNull ReceivedListener listener) {
        if (listener == null) return;
        RECEIVED_LISTENERS.add(listener);
    }

    public static void removeReceivedListener(@NonNull ReceivedListener listener) {
        if (listener == null) return;
        RECEIVED_LISTENERS.remove(listener);
    }

    private static int serverSdkVersion;

    public static int getServerSdkVersion() {
        return serverSdkVersion;
    }

    @Nullable
    private static IService requestService() {
        if (storageIsolationService != null) {
            return storageIsolationService;
        }

        IBinder binder = requestBinder();
        if (binder == null) return null;

        IService service = IService.Stub.asInterface(binder);
        try {
            if ((serverSdkVersion = service.setPublicSdkVersion(SDK_CLIENT_VERSION)) == 0) {
                throw new IllegalStateException("remote service dose not support sdk");
            }
        } catch (Throwable tr) {
            tr.printStackTrace();
            return null;
        }
        storageIsolationService = service;

        return storageIsolationService;
    }

    @Nullable
    public static StorageIsolationManager create() {
        IService remote;
        try {
            remote = requestService();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }

        if (remote == null) {
            return null;
        }
        return new StorageIsolationManager(remote);
    }

    @NonNull
    public static StorageIsolationManager createThrow() throws Exception {
        IService service = requestService();
        if (service == null) {
            throw new Exception("service not running");
        }
        return new StorageIsolationManager(service);
    }

    private static RuntimeException rethrowFromSystemServer(RemoteException e) throws RuntimeException {
        if (e instanceof DeadObjectException) {
            throw new RuntimeException("system dead?");
        } else {
            throw new RuntimeException(e);
        }
    }

    public static boolean isSupported(@NonNull PackageManager pm) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }
        try {
            return pm.getPermissionInfo(StorageIsolationManager.PERMISSION, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private final IService mService;

    private StorageIsolationManager(IService service) {
        mService = service;
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<RedirectPackageInfo> getRedirectPackages(int flags, int userId) {
        String defaultRedirectTarget;
        List<RedirectPackageInfo> list;

        try {
            defaultRedirectTarget = mService.getDefaultRedirectTarget();

            //noinspection unchecked
            ParceledListSlice<RedirectPackageInfo> parceledListSlice = mService.getRedirectPackages(flags, userId);
            if (parceledListSlice == null)
                return new ArrayList<>();

            list = parceledListSlice.getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }

        for (RedirectPackageInfo rpi : list) {
            if (rpi.redirectTarget == null)
                rpi.redirectTarget = defaultRedirectTarget;

            if (rpi.redirectTarget != null)
                rpi.redirectTarget = String.format(rpi.redirectTarget, rpi.packageName);

            if (rpi.mountDirs == null) {
                rpi.mountDirs = getMountDirsForConfig(rpi.mountDirsConfig);
            }
        }

        return list;
    }

    @RequiresPermission(PERMISSION)
    @Nullable
    public RedirectPackageInfo getRedirectPackageInfo(@NonNull String packageName, int flags, int userId) {
        try {
            RedirectPackageInfo rpi = mService.getRedirectPackageInfo(packageName, flags, userId);
            if (rpi == null)
                return null;

            if (rpi.redirectTarget == null)
                rpi.redirectTarget = mService.getDefaultRedirectTarget();

            if (rpi.redirectTarget != null)
                rpi.redirectTarget = String.format(rpi.redirectTarget, packageName);

            if (rpi.mountDirs == null) {
                rpi.mountDirs = getMountDirsForConfig(rpi.mountDirsConfig);
            }
            return rpi;
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<ObserverInfo> getObservers(int userId) {
        try {
            //noinspection unchecked
            return mService.getObservers(null, userId).getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<ObserverInfo> getObservers(@NonNull String packageName, int userId) {
        Objects.requireNonNull(packageName, "Package name cannot be null.");
        try {
            //noinspection unchecked
            return mService.getObservers(packageName, userId).getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public String getDefaultRedirectTarget() {
        try {
            return mService.getDefaultRedirectTarget();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<MountDirsTemplate> getMountDirsTemplates() {
        try {
            //noinspection unchecked
            return mService.getMountDirsTemplates().getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @Nullable
    public MountDirsTemplate getMountDirsTemplate(@NonNull String id) {
        Objects.requireNonNull(id, "Id cannot be null.");
        try {
            return mService.getMountDirsTemplate(id);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<SimpleMountInfo> getSimpleMounts(int userId) {
        try {
            //noinspection unchecked
            return mService.getSimpleMounts(null, 0, userId).getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @NonNull
    public List<SimpleMountInfo> getSimpleMounts(@NonNull String packageName, int userId, int flags) {
        Objects.requireNonNull(packageName, "Package name cannot be null.");
        try {
            //noinspection unchecked
            return mService.getSimpleMounts(packageName, flags, userId).getList();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    @Nullable
    public Set<String> getMountDirsForConfig(@NonNull MountDirsConfig config) {
        Objects.requireNonNull(config, "Config cannot be null.");
        try {
            if (serverSdkVersion == 2) {
                List<String> list = mService.getMountDirsForConfig(config);
                return list == null ? null : new HashSet<>(list);
            } else if (serverSdkVersion == 1) {
                if (config.isCustomAllowed()) {
                    return config.customDirs;
                } else if (config.isTemplatesAllowed() && config.templatesIds != null) {
                    Set<String> set = new HashSet<>();
                    for (String id : config.templatesIds) {
                        MountDirsTemplate template = getMountDirsTemplate(id);
                        if (template != null && template.list != null) {
                            set.addAll(template.list);
                        }
                    }
                    return set;
                }
                return new HashSet<>();
            }
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
        return null;
    }

    @RequiresPermission(PERMISSION)
    @Nullable
    public Set<String> getMountDirsForPackage(@NonNull String packageName, int userId) {
        Objects.requireNonNull(packageName, "Package name cannot be null.");
        try {
            if (serverSdkVersion == 2) {
                List<String> list = mService.getMountDirsForPackage(packageName, userId, true);
                return list == null ? null : new HashSet<>(list);
            } else if (serverSdkVersion == 1) {
                RedirectPackageInfo info = getRedirectPackageInfo(packageName, 0, userId);
                if (info != null) {
                    return info.mountDirs;
                }
            }
            return null;
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @RequiresPermission(PERMISSION)
    public boolean isIsolationEnabledForUid(int uid) {
        try {
            return mService.isIsolationEnabledForUid(uid);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }
}
