package moe.shizuku.redirectstorage;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

interface IService {

    ParceledListSlice getRedirectPackages(int flags, int userId) = 1;

    RedirectPackageInfo getRedirectPackageInfo(String packageName, int flags, int userId) = 2;

    ParceledListSlice getObservers(String packageName, int userId) = 6;

    String getDefaultRedirectTarget() = 10;

    ParceledListSlice getMountDirsTemplates() = 37;

    MountDirsTemplate getMountDirsTemplate(String id) = 41;

    ParceledListSlice getSimpleMounts(String packageName, int flags, int userId) = 42;

    int setPublicSdkVersion(int sdk) = 60;

    List<String> getMountDirsForPackage(String packageName, int userId, boolean includeDefault) = 65;

    List<String> getMountDirsForConfig(in MountDirsConfig config) = 68;

    boolean isIsolationEnabledForUid(int uid) = 20000;
}
