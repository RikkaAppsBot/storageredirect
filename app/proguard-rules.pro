##---------------Begin: proguard-android-optimize.txt  ----------
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify

# The remainder of this file is identical to the non-optimized version
# of the Proguard configuration file (except that the other file has
# flags to turn off optimization).

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
#-keepclasseswithmembernames class * {
#    native <methods>;
#}
-keepclasseswithmembers,allowobfuscation class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

##---------------End: proguard-android-optimize.txt  ----------

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Prevent proguard from stripping interface information from TypeAdapter, TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Prevent R8 from leaving Data object members always null
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

##---------------End: proguard configuration for Gson  ----------

# Egit github core

-dontwarn org.eclipse.egit.github.**
-keep class org.eclipse.egit.github.** { *; }
-keep interface org.eclipse.egit.github.** { *; }

# Kotlin

-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
	public static void checkExpressionValueIsNotNull(...);
	public static void checkNotNullExpressionValue(...);
	public static void checkReturnedValueIsNotNull(...);
	public static void checkFieldIsNotNull(...);
	public static void checkParameterIsNotNull(...);
}

# Storage Isolation

-obfuscationdictionary dictionary.new.txt
-repackageclasses 'moe.shizuku.redirectstorage'

-keepnames class moe.shizuku.redirectstorage.RedirectPackageInfo
-keepnames class moe.shizuku.redirectstorage.ObserverInfo
-keepnames class moe.shizuku.redirectstorage.MountDirsConfig
-keepnames class moe.shizuku.redirectstorage.MountDirsTemplate
-keepnames class moe.shizuku.redirectstorage.SimpleMountInfo
-keepnames class moe.shizuku.redirectstorage.model.ModuleStatus
-keep class moe.shizuku.redirectstorage.model.FileMonitorRecord { *; }
-keep class moe.shizuku.redirectstorage.model.ParcelStructStat { *; }
-keep class moe.shizuku.redirectstorage.model.ServerFile { *; }
-keep class moe.shizuku.redirectstorage.model.DebugInfo { *; }
-keep class moe.shizuku.redirectstorage.model.FolderSizeEntry { *; }
-keep class moe.shizuku.redirectstorage.utils.SizeTagHandler { *; }

-dontwarn moe.shizuku.redirectstorage.**

-assumenosideeffects class android.util.Log {
    public static *** d(...);
}

-keepattributes SourceFile,LineNumberTable
-renamesourcefileattribute SourceFile
