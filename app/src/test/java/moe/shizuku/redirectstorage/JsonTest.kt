package moe.shizuku.redirectstorage

import moe.shizuku.redirectstorage.model.AppConfiguration
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class JsonTest {

    companion object {
        private val GSON = SRApplication.GSON
    }

    @Test
    @Throws(Exception::class)
    fun appConfig_Recommendation_Preset() {
        val json = """
        {
            "recommendation": "recommended"
        }
        """

        val obj = GSON.fromJson(json, AppConfiguration::class.java)
        assertNotNull("recommendation is null", obj.recommendation)
        assertNotNull("recommendation.preset is null", obj.recommendation!!.presetKey)
        assertEquals("recommended", obj.recommendation!!.presetKey)
    }

    @Test
    @Throws(Exception::class)
    fun appConfig_Recommendation_Custom() {
        val json = """
            {
                "recommendation": {
                    "icon": "check",
                    "color": "green",
                    "zh_CN": "1",
                    "zh": "2",
                    "en": "3"
                }
            }
        """

        val obj = GSON.fromJson(json, AppConfiguration::class.java)
        assertNotNull("recommendation is null", obj.recommendation)
        assertNotNull("recommendation.custom is null", obj.recommendation!!.custom)
        Locale.setDefault(Locale.SIMPLIFIED_CHINESE)
        assertEquals("1", obj.recommendation!!.custom!!.getText())
    }
}