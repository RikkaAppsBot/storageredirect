package moe.shizuku.redirectstorage;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import moe.shizuku.redirectstorage.utils.DataVerifier;

import static org.junit.Assert.*;

public class DataVerifierTest {

    private static final List<String> STANDARD_FOLDERS = Arrays.asList(
            "Android/data",
            "Android/media",
            "Android/obb",
            "Alarms",
            "DCIM",
            "Documents",
            "Download",
            "Movies",
            "Music",
            "Notifications",
            "Pictures",
            "Podcasts",
            "Ringtones"
    );

    @Test
    public void test_isPathInStandardFolders() {
        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage"
        ));

        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated"
        ));

        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated/0"
        ));

        assertFalse(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated/0/.bky"
        ));

        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated/0/Download/1.txt"
        ));
        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated/1/Download/1.txt"
        ));
        assertFalse(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/storage/emulated/0",
                "/storage/emulated/0/test/test"
        ));

        assertTrue(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/test/0",
                "/test/0/Download/1.txt"
        ));
        assertFalse(DataVerifier.isPathInStandardFolders(
                STANDARD_FOLDERS,
                "/test/0",
                "/test/0/.bky"
        ));
    }

}
