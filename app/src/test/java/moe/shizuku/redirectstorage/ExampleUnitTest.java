package moe.shizuku.redirectstorage;

import android.util.Base64;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import moe.shizuku.redirectstorage.model.AppConfiguration;
import rikka.internal.model.MultiLocaleEntity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        //assertEquals(4, 2 + 2);
        List<String> res = new ArrayList<>();

        res.add("echo \"Disable SELinux...\"");
        res.add("setenforce 0");

        res.add("echo \"Patching SELinux policy...\"");
        res.add("supolicy --live \"allow untrusted_app default_android_service service_manager find\"");
        res.add("supolicy --live \"allow untrusted_app su binder transfer\"");
        res.add("supolicy --live \"allow untrusted_app su binder call\"");

        res.add("supolicy --live \"allow untrusted_app_25 default_android_service service_manager find\"");
        res.add("supolicy --live \"allow untrusted_app_25 su binder transfer\"");
        res.add("supolicy --live \"allow untrusted_app_25 su binder call\"");

        res.add("supolicy --live \"allow * su binder *\"");
        res.add("supolicy --live \"allow * default_android_service service_manager find\"");

        res.add("com.eg.android.AlipayGphone");
        res.add("alipayqr://platformapi/startapp?saId=10000007&qrcode=HTTPS://QR.ALIPAY.COM/FKX01079WRO0JVHFKXIJ5B");
        res.add("rikka.pay@shizuku.moe");

        for (String str : res) {
            System.out.println(str);
            System.out.println(Base64.encodeToString(str.getBytes(), Base64.DEFAULT));
        }
    }

    @Test
    public void regex() throws Exception {
        Pattern pattern = Pattern.compile(".*(?<!\\.tmp)$");

        assertFalse("1.tmp", pattern.matcher("1.tmp").matches());
        assertTrue("1.jpg", pattern.matcher("1.jpg").matches());
    }

    @Test
    public void resolveReason() throws Exception {
        MultiLocaleEntity reason = new MultiLocaleEntity();
        reason.put("zh", "原因");
        reason.put("zh_TW", "1234");
        reason.put("en", "reason");

        assertEquals(AppConfiguration.resolveReason(reason, Locale.SIMPLIFIED_CHINESE, null), "原因");
        assertEquals(AppConfiguration.resolveReason(reason, Locale.TRADITIONAL_CHINESE, null), "1234");
        assertEquals(AppConfiguration.resolveReason(reason, new Locale("zh", "HK"), null), "原因");
        assertEquals(AppConfiguration.resolveReason(reason, new Locale("zh", "CN", "Hans"), null), "原因");
        assertEquals(AppConfiguration.resolveReason(reason, Locale.JAPANESE, null), "reason");

        reason.clear();
        reason.put("zh_CN", "原因");
        reason.put("zh", "原因1234");
        reason.put("en", "reason");
        assertEquals(AppConfiguration.resolveReason(reason, new Locale("zh", "HK"), null), "原因1234");
        assertEquals(AppConfiguration.resolveReason(reason, new Locale("zh", "CN"), null), "原因");

        reason.clear();
        reason.put("ja", "123");
        assertEquals(AppConfiguration.resolveReason(reason, new Locale("zh", "HK"), null), "123");
    }
}