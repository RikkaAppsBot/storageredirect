package moe.shizuku.redirectstorage;

import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.runner.AndroidJUnit4;

/**
 * Created by Fung Gwo on 2018/2/22.
 */

@RunWith(AndroidJUnit4.class)
public class PrintTestLogTest {

    private static final String TAG = "StorageRedirect";

    @Test
    public void printTestLogs() {
        Log.v(TAG, "Test Logs");
        Log.d(TAG, "Test Logs");
        Log.i(TAG, "Test Logs");
        Log.w(TAG, "Test Logs");
        Log.e(TAG, "Test Logs");
    }
}
