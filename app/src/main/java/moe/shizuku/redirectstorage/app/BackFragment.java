package moe.shizuku.redirectstorage.app;

public interface BackFragment {

    /**
     * @return should back
     */
    default boolean onBackPressed() {
        return true;
    }
}
