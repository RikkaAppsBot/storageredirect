package moe.shizuku.redirectstorage.app;

import android.app.IntentService;
import android.content.Context;
import android.content.res.Configuration;

public abstract class BaseIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BaseIntentService(String name) {
        super(name);
    }

    @Override
    protected void attachBaseContext(Context base) {
        Configuration configuration = base.getResources().getConfiguration();
        configuration.setLocale(Settings.getLocale());
        super.attachBaseContext(base.createConfigurationContext(configuration));
    }
}
