package moe.shizuku.redirectstorage.app

import android.app.Dialog
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import androidx.annotation.CallSuper
import androidx.fragment.app.FragmentManager
import moe.shizuku.redirectstorage.utils.ThemeHelper
import rikka.core.res.resolveColor
import rikka.material.app.MaterialDialog
import rikka.material.app.MaterialDialogFragment

abstract class BaseDialogFragment : MaterialDialogFragment() {

    fun show(fragmentManager: FragmentManager?) {
        if (fragmentManager == null || fragmentManager.isStateSaved)
            return

        show(fragmentManager, javaClass.simpleName)
    }

    @CallSuper
    override fun onDialogShow(dialog: Dialog) {
        if (dialog !is MaterialDialog) {
            if (shouldApplyTranslucentSystemBars()) {
                onApplyTranslucentSystemBars()
            }
        }
    }

    override fun onApplyUserThemeResource(theme: Resources.Theme) {
        theme.applyStyle(ThemeHelper.getThemeStyleRes(requireContext()), true)
    }

    override fun shouldApplyTranslucentSystemBars(): Boolean {
        return true
    }

    override fun onApplyTranslucentSystemBars() {
        super.onApplyTranslucentSystemBars()

        val window = dialog!!.window
        val theme = dialog!!.context.theme

        window?.statusBarColor = Color.TRANSPARENT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window?.decorView?.post {
                if (window.decorView.rootWindowInsets?.systemWindowInsetBottom ?: 0 >= Resources.getSystem().displayMetrics.density * 40) {
                    window.navigationBarColor = theme.resolveColor(android.R.attr.navigationBarColor) and 0x00ffffff or -0x20000000
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        window.isNavigationBarContrastEnforced = false
                    }
                } else {
                    window.navigationBarColor = Color.TRANSPARENT
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        window.isNavigationBarContrastEnforced = true
                    }
                }
            }
        }
    }
}
