package moe.shizuku.redirectstorage.app

import android.os.Bundle
import rikka.material.app.AppBar
import rikka.widget.borderview.BorderView

open class AppBarFragment : AppFragment() {

    override fun getBaseActivity(): AppBarFragmentActivity? {
        return super.getBaseActivity() as AppBarFragmentActivity?
    }

    override fun requireBaseActivity(): AppBarFragmentActivity {
        return super.requireBaseActivity() as AppBarFragmentActivity
    }

    private fun getAppBar(): AppBar? {
        return baseActivity?.appBar
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val appBar = getAppBar()
        appBar?.let {
            onAppBarCreated(appBar, savedInstanceState)
            view.let {
                if (view is BorderView) {
                    (view as BorderView).borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
                        appBar.setRaised(!top)
                    }
                }
            }
        }
    }

    open fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {

    }
}