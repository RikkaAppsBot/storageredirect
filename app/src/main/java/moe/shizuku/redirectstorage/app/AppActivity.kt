package moe.shizuku.redirectstorage.app

import android.app.Activity
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Resources
import android.content.res.Resources.Theme
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import moe.shizuku.fontprovider.FontProviderClient
import moe.shizuku.fontprovider.compat.TypefaceCompat
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.app.Settings.preferences
import moe.shizuku.redirectstorage.component.filebrowser.AppFileBrowserActivity
import moe.shizuku.redirectstorage.component.logcat.LogcatStatusActivity
import moe.shizuku.redirectstorage.component.payment.MyPurchaseFragment
import moe.shizuku.redirectstorage.component.payment.PurchaseFragment
import moe.shizuku.redirectstorage.component.settings.SettingsActivity
import moe.shizuku.redirectstorage.component.starter.StartResultDialogFragment
import moe.shizuku.redirectstorage.component.starter.StarterActivity
import moe.shizuku.redirectstorage.utils.BuildUtils
import moe.shizuku.redirectstorage.utils.Logger.LOGGER
import moe.shizuku.redirectstorage.utils.ThemeHelper
import rikka.core.res.resolveColor
import rikka.core.util.ResourceUtils
import rikka.html.text.HtmlCompat
import rikka.internal.help.HelpProvider
import rikka.internal.model.MultiLocaleEntity
import rikka.internal.model.MultiLocaleEntity.LocaleProvider
import rikka.material.app.LocaleDelegate
import rikka.material.app.MaterialActivity
import java.io.File
import java.util.*
import kotlin.coroutines.CoroutineContext

abstract class AppActivity : MaterialActivity(), CoroutineScope {

    companion object {

        private var fontInitialized = false

        private fun initializeFont(context: Context) {
            if (fontInitialized || Build.VERSION.SDK_INT >= 28) {
                return
            }
            fontInitialized = true
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1) {
                    TypefaceCompat.getSystemFontMap()!!["sans-serif-condensed-medium"] = Typeface.create("sans-serif-condensed", Typeface.BOLD)
                }
                if (!File("/system/fonts/NotoSansCJK-Medium.ttc").exists()
                        && !File("/system/fonts/NotoSansCJKsc-Medium.ttf").exists()
                        && !File("/system/fonts/NotoSansCJKsc-Medium.otf").exists()) {
                    val client = FontProviderClient.create(context)
                    client?.replace("Noto Sans CJK", "sans-serif", "sans-serif-medium")
                }
            } catch (tr: Throwable) {
                tr.printStackTrace()
            }
        }

        private const val DNS_BLOCKED_TITLE = "Network requests are blocked"
        private const val DNS_BLOCKED_MESSAGE = "The domain used for online contents is resolved to an invalid location.<p>If you are using some AdBlocker apps, please add <b><font face=\"monospace\">*.rikka.app</font></b> to the whitelist."

        init {
            MultiLocaleEntity.setLocaleProvider(object : LocaleProvider() {
                override fun get(): Locale {
                    return LocaleDelegate.defaultLocale
                }
            })
        }
    }

    private var mJob: Job? = null
    override val coroutineContext: CoroutineContext
        get() = Main.plus(mJob!!)

    val localBroadcastManager: LocalBroadcastManager
        get() = LocalBroadcastManager.getInstance(this)

    private val startExitReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(applicationContext: Context, intent: Intent) {
            LOGGER.i(this@AppActivity.toString())
            if (isFinishing) return

            val exitCode = intent.getIntExtra(AppConstants.EXTRA_EXIT_CODE, Int.MIN_VALUE)

            StartResultDialogFragment.newInstance(exitCode).show(supportFragmentManager)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        mJob = Job(null)
        initializeFont(this)
        HelpProvider.load(this)
        //AppCategory.load(this);

        // Disable ndef push by default
        /*if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            final NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            if (nfcAdapter != null) {
                nfcAdapter.setNdefPushMessageCallback(null, this);
            }
        }*/
        super.onCreate(savedInstanceState)
    }

    override fun onApplyUserThemeResource(theme: Theme, isDecorView: Boolean) {
        if (javaClass == AppFileBrowserActivity::class.java && !ResourceUtils.isNightMode(resources.configuration)) {
            return
        }
        theme.applyStyle(ThemeHelper.getThemeStyleRes(this), true)
    }

    override fun onDestroy() {
        super.onDestroy()
        mJob!!.cancel(null)
    }

    override fun onBackPressed() {
        val fragments = supportFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment == null || !fragment.isAdded || fragment.isHidden) {
                continue
            }
            if (fragment is BackFragment) {
                val baseFragment = fragment as BackFragment
                if (!baseFragment.onBackPressed()) {
                    return
                }
            }
        }
        super.onBackPressed()
    }

    override fun computeUserThemeKey(): String {
        return ThemeHelper.getTheme(this) + ThemeHelper.getAccentColor()
    }

    fun showNetworkBlockedDialog(): Boolean {
        if (preferences.getBoolean("db", false)) {
            try {
                AlertDialog.Builder(this@AppActivity)
                        .setTitle(DNS_BLOCKED_TITLE)
                        .setMessage(HtmlCompat.fromHtml(DNS_BLOCKED_MESSAGE, HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                        .setPositiveButton(android.R.string.ok, null)
                        .setCancelable(false)
                        .show()
                preferences.edit().putBoolean("db", false).apply()
            } catch (ignored: Throwable) {
            }
            return true
        }
        return false
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        localBroadcastManager.registerReceiver(startExitReceiver, IntentFilter(AppConstants.ACTION_STARTER_EXIT))
        showNetworkBlockedDialog()
    }

    override fun onPause() {
        super.onPause()
        localBroadcastManager.unregisterReceiver(startExitReceiver)
    }

    override fun recreate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            window.setWindowAnimations(R.style.Animation_WindowEnterExitFade)
        }
        super.recreate()
    }

    override fun shouldUpRecreateTask(targetIntent: Intent?): Boolean {
        return if (BuildUtils.atLeast30()) false else super.shouldUpRecreateTask(targetIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (item.itemId == android.R.id.home && ((supportActionBar?.displayOptions ?: 0) and ActionBar.DISPLAY_HOME_AS_UP) != 0) {
                    return onNavigateUp()
                }
                false
            }
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            R.id.action_about -> {
                val versionName: String
                try {
                    versionName = packageManager.getPackageInfo(BuildConfig.APPLICATION_ID, 0).versionName
                    val dialog: Dialog = AlertDialog.Builder(this)
                            .setView(R.layout.dialog_about)
                            .show()
                    (dialog.findViewById<View>(R.id.design_about_icon) as ImageView).setImageDrawable(getDrawable(R.drawable.ic_launcher))
                    (dialog.findViewById<View>(R.id.design_about_title) as TextView).text = getString(R.string.app_name)
                    (dialog.findViewById<View>(R.id.design_about_version) as TextView).text = versionName
                    (dialog.findViewById<View>(R.id.design_about_info) as TextView).isVisible = false
                    dialog.findViewById<View>(R.id.design_about_icon).setOnLongClickListener { v: View? ->
                        preferences.edit().putBoolean("show_unlock", true).apply()
                        if (javaClass == MainActivity::class.java) {
                            invalidateOptionsMenu()
                        }
                        true
                    }
                } catch (ignored: PackageManager.NameNotFoundException) {
                }
                true
            }
            R.id.action_unlock -> {
                PurchaseFragment.startActivity(this)
                true
            }
            R.id.action_support -> {
                startActivity(Intent(this, HelpActivity::class.java))
                true
            }
            R.id.action_changelog -> {
                HelpProvider.get(this, "changelog")?.startActivity(this)
                true
            }
            R.id.action_logcat -> {
                startActivity(Intent(this, LogcatStatusActivity::class.java))
                true
            }
            R.id.action_my_purchase -> {
                MyPurchaseFragment.startActivity(this)
                true
            }
            R.id.action_report -> {
                val entity = HelpProvider.get(this, "how_to_report_problems")
                if (entity != null) {
                    entity.startActivity(this)
                } else {
                    Toast.makeText(this, R.string.toast_cannot_load_help, Toast.LENGTH_LONG).show()
                }
                return true
            }
            R.id.action_reset_tips -> {
                preferences.edit()
                        .remove("read_help_tip")
                        .remove("enhance_module_tip")
                        .remove("file_monitor_tip_v2")
                        .remove("first_redirect_tip")
                        .remove("delay_start_tip")
                        .remove(Settings.IGNORE_UNSUPPORTED_SU)
                        .remove(Settings.IGNORE_UNSUPPORTED_STORAGE)
                        .remove(Settings.IGNORE_NO_LOG)
                        .remove("ignore_samsung_arm64")
                        .remove("ignore_meizu")
                        .remove("ignore_miui")
                        .apply()
                false
            }
            R.id.action_exit -> {
                val sm = SRManager.create()
                if (sm == null) {
                    Toast.makeText(this, R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show()
                    return true
                }
                AlertDialog.Builder(this)
                        .setMessage(R.string.dialog_exit_title)
                        .setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int ->
                            try {
                                sm.exit(0)
                            } catch (ignored: Throwable) {
                            }
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    val actualReferrer: String?
        get() {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
                try {
                    val referrerField = Activity::class.java.getDeclaredField("mReferrer")
                    referrerField.isAccessible = true
                    return referrerField[this] as String
                } catch (ignored: Exception) {
                }
            }
            return referrer?.authority ?: callingPackage
        }

    open var actionBarTitle: CharSequence?
        get() = if (supportActionBar != null) supportActionBar!!.title else null
        set(title) {
            if (supportActionBar != null) supportActionBar!!.title = title
        }

    open var actionBarSubtitle: CharSequence?
        get() = if (supportActionBar != null) supportActionBar!!.subtitle else null
        set(title) {
            if (supportActionBar != null) supportActionBar!!.subtitle = title
        }

    open fun setActionBarTitle(@StringRes resId: Int) {
        actionBarTitle = getString(resId)
    }

    open fun setActionBarSubtitle(@StringRes resId: Int) {
        actionBarSubtitle = getString(resId)
    }

    fun startStarter() {
        startActivity(Intent(this, StarterActivity::class.java))
    }

    fun showRestartDialog() {
        AlertDialog.Builder(this)
                .setMessage(R.string.dialog_restart_message)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int -> startStarter() }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
    }

    override fun shouldApplyTranslucentSystemBars(): Boolean {
        return true
    }

    override fun onApplyTranslucentSystemBars() {
        super.onApplyTranslucentSystemBars()

        val window = window
        val theme = theme

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window?.decorView?.post {
                if (window.decorView.rootWindowInsets?.systemWindowInsetBottom ?: 0 >= Resources.getSystem().displayMetrics.density * 40) {
                    window.navigationBarColor = theme.resolveColor(android.R.attr.navigationBarColor) and 0x00ffffff or -0x20000000
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        window.isNavigationBarContrastEnforced = false
                    }
                } else {
                    window.navigationBarColor = Color.TRANSPARENT
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        window.isNavigationBarContrastEnforced = true
                    }
                }
            }
        }
    }
}