package moe.shizuku.redirectstorage.app;

import android.content.Context;
import android.content.res.Configuration;

import rikka.core.app.ForegroundIntentService;

public abstract class BaseForegroundIntentService extends ForegroundIntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BaseForegroundIntentService(String name) {
        super(name);
    }

    @Override
    protected void attachBaseContext(Context base) {
        Configuration configuration = base.getResources().getConfiguration();
        configuration.setLocale(Settings.getLocale());
        super.attachBaseContext(base.createConfigurationContext(configuration));
    }
}
