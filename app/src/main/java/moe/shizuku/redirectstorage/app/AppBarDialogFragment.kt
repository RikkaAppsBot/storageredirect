package moe.shizuku.redirectstorage.app

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.CallSuper
import androidx.appcompat.widget.Toolbar
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.ComponentActivity
import rikka.material.app.AppBar
import rikka.material.widget.AppBarLayout
import rikka.widget.borderview.BorderView

open class AppBarDialogFragment : BaseDialogFragment() {

    @CallSuper
    override fun onDialogOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            dialog?.onBackPressed()
            return true
        }
        return super.onDialogOptionsItemSelected(item)
    }

    fun requestPrepareDialogOptionsMenu() {
        if (!hasDialogOptionsMenu() || appBar == null)
            return

        dialog?.invalidateOptionsMenu()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is ComponentActivity) {
            setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog_FullScreen_NoAnimation)
        } else {
            setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog_FullScreen)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (activity is ComponentActivity) {
            activity?.finish()
        }
    }

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.appbar_dialog, container, false)
        val view = onCreateContainerView(/*layoutInflater*/LayoutInflater.from(activity/*contentView.context*/), root as ViewGroup, savedInstanceState)
        view.id = android.R.id.content
        root.addView(view, 0, FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        return root
    }

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val appBarLayout = view.findViewById<AppBarLayout>(R.id.toolbar_container)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        val container = view.findViewById<View>(android.R.id.content)
        if (container is BorderView) {
            (container as BorderView).borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
                appBarLayout.isRaised = !top
            }
        }
        setAppBar(appBarLayout, toolbar)
        onAppBarCreated(appBar!!, savedInstanceState)

        onContainerViewCreated(container, savedInstanceState)

        if (container is ViewGroup) {
            val content = onCreateContentView(LayoutInflater.from(activity/*contentView.context*/), container, savedInstanceState)
            content?.let {
                onContentViewCreated(it, savedInstanceState)
                container.addView(it)
            }
        }
    }

    open fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {

    }

    open fun onCreateContainerView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.appbar_container_scroll, container, false)
    }

    open fun onContainerViewCreated(view: View, savedInstanceState: Bundle?) {

    }

    open fun onCreateContentView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View? {
        return null
    }

    open fun onContentViewCreated(view: View, savedInstanceState: Bundle?) {

    }
}