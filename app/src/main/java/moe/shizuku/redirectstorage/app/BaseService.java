package moe.shizuku.redirectstorage.app;

import android.app.Service;
import android.content.Context;
import android.content.res.Configuration;

public abstract class BaseService extends Service {

    @Override
    protected void attachBaseContext(Context base) {
        Configuration configuration = base.getResources().getConfiguration();
        configuration.setLocale(Settings.getLocale());
        super.attachBaseContext(base.createConfigurationContext(configuration));
    }


}
