package moe.shizuku.redirectstorage.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiConsumer;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver;

public abstract class AppFragment extends Fragment implements CoroutineScope, BackFragment {

    protected CompositeDisposable mCompositeDisposable
            = new CompositeDisposable();

    private final List<BroadcastReceiver> mLocalBroadcastReceivers = new ArrayList<>();

    private Job mJob;

    @NotNull
    @Override
    public CoroutineContext getCoroutineContext() {
        return Dispatchers.getMain().plus(mJob);
    }

    public void cancelJobs() {
        mJob.cancel(null);
    }

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mJob = SupervisorKt.SupervisorJob(null);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterAllLocalBroadcastReceivers();
        mJob.cancel(null);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mCompositeDisposable.clear();
    }

    public LocalBroadcastManager getLocalBroadcastManager() {
        return LocalBroadcastManager.getInstance(requireContext());
    }

    public void registerLocalBroadcastReceiver(@NonNull IntentFilter intentFilter, @NonNull BroadcastReceiver receiver) {
        getLocalBroadcastManager().registerReceiver(receiver, intentFilter);
        mLocalBroadcastReceivers.add(receiver);
    }

    public void registerLocalBroadcastReceiver(@NonNull String action, @NonNull BroadcastReceiver receiver) {
        getLocalBroadcastManager().registerReceiver(receiver, new IntentFilter(action));
        mLocalBroadcastReceivers.add(receiver);
    }

    public void registerLocalBroadcastReceiver(@NonNull IntentFilter intentFilter, @NonNull BiConsumer<Context, Intent> broadcastConsumer) {
        registerLocalBroadcastReceiver(intentFilter, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    broadcastConsumer.accept(context, intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    //throw new RuntimeException(e);
                }
            }
        });
    }

    public void registerLocalBroadcastReceiver(@NonNull String action, @NonNull BiConsumer<Context, Intent> broadcastConsumer) {
        registerLocalBroadcastReceiver(new IntentFilter(action), broadcastConsumer);
    }

    public void registerLocalBroadcastReceiver(@NonNull ActionBroadcastReceiver broadcastReceiver) {
        registerLocalBroadcastReceiver(broadcastReceiver.getAction(), broadcastReceiver);
    }

    public void unregisterAllLocalBroadcastReceivers() {
        final LocalBroadcastManager lbm = getLocalBroadcastManager();
        for (BroadcastReceiver registered : mLocalBroadcastReceivers) {
            lbm.unregisterReceiver(registered);
        }
        mLocalBroadcastReceivers.clear();
    }

    @Nullable
    public AppActivity getBaseActivity() {
        return (AppActivity) getActivity();
    }

    public AppActivity requireBaseActivity() {
        return (AppActivity) requireActivity();
    }

}
