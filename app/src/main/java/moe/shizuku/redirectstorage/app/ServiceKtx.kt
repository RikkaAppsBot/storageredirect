package moe.shizuku.redirectstorage.app

import android.app.Notification
import android.app.Service
import android.widget.Toast
import moe.shizuku.redirectstorage.R

fun Service.startForegroundOrNotify(id: Int, notification: Notification) {
    try {
        startForeground(id, notification)
    } catch (e: Exception) {
        e.printStackTrace()
        if (e.message == "foreground not allowed as per app op") {
            Toast.makeText(this, getString(R.string.toast_start_foreground_denied), Toast.LENGTH_LONG).show()
        }
    }
}