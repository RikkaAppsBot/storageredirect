package moe.shizuku.redirectstorage.app

import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import moe.shizuku.redirectstorage.model.AppInfo

object SharedUserLabelCache {

    private val cache = HashMap<String, CharSequence>()

    @JvmStatic
    fun get(sharedUserId: String?): CharSequence? {
        if (sharedUserId == null) {
            return null
        }
        return cache[sharedUserId] ?: sharedUserId
    }

    @JvmStatic
    fun load(pm: PackageManager, sharedUserLabel: Int, sharedUserId: String, packageName: String, applicationInfo: ApplicationInfo): CharSequence {
        var value = cache[sharedUserId]
        if (value != null) {
            return value
        }
        if (sharedUserLabel == 0) {
            return sharedUserId
        }
        try {
            value = pm.getText(packageName, sharedUserLabel, applicationInfo) ?: packageName
            if (value.isNotBlank()) {
                cache[sharedUserId] = value
            }
            return value
        } catch (e: Exception) {
        }
        return sharedUserId
    }

    @JvmStatic
    fun load(pm: PackageManager, appInfo: AppInfo) {
        if (appInfo.sharedUserId == null || appInfo.applicationInfo == null) {
            return
        }
        load(pm, appInfo.packageInfo.sharedUserLabel, appInfo.sharedUserId!!, appInfo.packageName, appInfo.applicationInfo!!)
    }
}