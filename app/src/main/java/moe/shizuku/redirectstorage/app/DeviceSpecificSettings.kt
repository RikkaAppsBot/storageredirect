package moe.shizuku.redirectstorage.app

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import moe.shizuku.redirectstorage.ktx.createPreferenceContext
import moe.shizuku.redirectstorage.utils.EncryptedSharedPreferences
import moe.shizuku.redirectstorage.utils.EncryptedSharedPreferencesLegacy
import rikka.core.content.putAll
import java.io.File
import java.util.*

object DeviceSpecificSettings {

    private const val XML_NAME = "local_settings"
    private const val XML_NAME_LEGACY = "local_state"

    private const val INSTANCE_ID = "XXRpKf5RijI3x258"
    private const val FIRST_INSTALL_TIME = "first_install_time"

    @JvmStatic
    lateinit var preferences: SharedPreferences
        private set

    private fun migrate(context: Context): Map<String?, *>? {
        val oldFile = File(context.filesDir.parent!! + File.separator + "shared_prefs" + File.separator + XML_NAME_LEGACY + ".xml")
        val newFile = File(context.filesDir.parent!! + File.separator + "shared_prefs" + File.separator + XML_NAME + ".xml")
        if (newFile.exists()) {
            return null
        }

        var oldMap: Map<String?, *>? = null
        if (oldFile.exists()) {
            val preferences = context.getSharedPreferences(XML_NAME_LEGACY, Context.MODE_PRIVATE)
            val encrypted = EncryptedSharedPreferencesLegacy.createDeviceSpecific(preferences)
            oldMap = encrypted.all.toMutableMap()
            oldFile.delete()
        }
        return oldMap
    }

    @JvmStatic
    fun initialize(context: Context) {
        val preferenceContext = context.createPreferenceContext()
        val oldMap = migrate(preferenceContext)
        val original = preferenceContext.getSharedPreferences(XML_NAME, Context.MODE_PRIVATE)
        this.preferences = EncryptedSharedPreferences.createDeviceSpecific(original)
        if (oldMap != null) {
            this.preferences.edit { putAll(oldMap) }
        }
    }

    @JvmStatic
    fun getInstanceId(): String {
        var id = preferences.getString(INSTANCE_ID, null)
        if (id.isNullOrBlank()) {
            id = UUID.randomUUID().toString()
            preferences.edit { putString(INSTANCE_ID, id) }
        }
        return id
    }

    fun getInstanceIdOrNull(): String? {
        return preferences.getString(INSTANCE_ID, null)
    }

    fun setInstanceId(id: String) {
        preferences.edit { putString(INSTANCE_ID, id) }
    }

    @JvmStatic
    var firstInstallTime: Long
        get() = Settings.preferences.getLong(FIRST_INSTALL_TIME, 0)
        set(time) {
            Settings.preferences.edit().putLong(FIRST_INSTALL_TIME, time).apply()
        }

    val installTime: Long
        get() {
            val time = firstInstallTime
            return if (time > 0) {
                System.currentTimeMillis() - time
            } else 0
        }
}