package moe.shizuku.redirectstorage.app

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.text.TextUtils
import android.util.Log
import androidx.annotation.IntDef
import androidx.core.content.edit
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.component.gallery.ViewGalleryActivity
import moe.shizuku.redirectstorage.component.gallery.ViewGalleryActivity.SortMode
import moe.shizuku.redirectstorage.ktx.createPreferenceContext
import moe.shizuku.redirectstorage.utils.EncryptedSharedPreferences
import rikka.core.content.putAll
import rikka.material.app.DayNightDelegate
import java.util.*

object Settings {

    private const val PREFERENCE_NAME = "settings"

    const val LANGUAGE = "language"
    const val NIGHT_MODE = "night_mode"
    private const val DELAYED_BOOT = "delayed_boot"
    private const val GITHUB_TOKEN = "github_token"
    private const val LIST_SORT = "list_sort"
    private const val LIST_SORT_ENABLED_FIRST = "list_sort_enabled_first"
    const val LIST_FILTER_SHOW_SYSTEM = "list_filter_show_system"
    const val LIST_FILTER_SHOW_DISABLED = "list_filter_show_disabled"
    private const val ONLINE_RULES_ENABLED = "online_rules_enabled"
    private const val CUSTOM_REPO_OWNER = "custom_repo_owner"
    private const val CUSTOM_REPO_NAME = "custom_repo_name"
    const val CUSTOM_REPO_BRANCH = "custom_repo_branch"
    private const val CUSTOM_REPO_ENABLED = "custom_repo_enabled"
    const val LIST_FILTER_HIDE_VERIFIED = "list_filter_hide_verified"
    private const val LAST_INSTALLED_VERSION = "last_installed_version"
    private const val NEW_APP_NOTIFICATION = "new_app_notification"
    private const val SHOW_MULTI_USER = "show_multi_user"
    const val IGNORE_UNSUPPORTED_SU = "ignore_unsupported_su"
    const val IGNORE_UNSUPPORTED_STORAGE = "ignore_unsupported_storage"
    private const val FILE_MONITOR_PATH_FILTER_KEYWORD = "file_monitor_path_filter_keyword"
    private const val FILE_MONITOR_PACKAGE_FILTER_MODE = "file_monitor_package_filter_mode"
    private const val FILE_MONITOR_PACKAGE_FILTER_LIST = "file_monitor_package_filter_list"
    const val IGNORE_NO_LOG = "ignore_no_log"
    private const val VIEW_GALLERY_DEFAULT_MODE = "view_gallery_default_mode"
    private const val REDUCE_HELP = "reduce_help"

    private const val DEFAULT_REPO_OWNER = BuildConfig.RULE_REPO_USER
    private const val DEFAULT_REPO_NAME = BuildConfig.RULE_REPO_NAME
    const val DEFAULT_REPO_BRANCH = "v2"

    @JvmStatic
    lateinit var preferences: SharedPreferences
        private set

    private fun decrypt(original: SharedPreferences) {
        val encrypted = EncryptedSharedPreferences.create(original, PREFERENCE_NAME)

        val all = encrypted.all.toMutableMap()
        all.remove("aGUdW9JZeAahxWZF")
        all.remove("first_install_time")

        original.edit {
            clear()
            putAll(all)
        }
    }

    @JvmStatic
    fun initialize(context: Context) {
        preferences = context.createPreferenceContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        if (preferences.contains("BnCeBS9DUCyFx5ldL3IbyfYDAi4urLnrdR1ESESNso98TVXwnqKjBR9uQuY=")) {
            decrypt(preferences)
        }
        Log.d(AppConstants.TAG, "SRSettings initialized.")
    }

    @JvmStatic
    val locale: Locale
        get() {
            val tag = preferences.getString(LANGUAGE, null)
            return if (TextUtils.isEmpty(tag) || "SYSTEM" == tag) {
                Locale.getDefault()
            } else Locale.forLanguageTag(tag!!)
        }

    fun createLocalizedContext(context: Context): Context {
        val config = Configuration()
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }

    @JvmStatic
    @get:DayNightDelegate.NightMode
    var nightMode: Int
        get() = preferences.getInt(NIGHT_MODE, DayNightDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        set(mode) {
            DayNightDelegate.setDefaultNightMode(mode)
        }

    @JvmStatic
    val isDelayedBoot: Boolean
        get() = preferences.getBoolean(DELAYED_BOOT, true)

    @JvmStatic
    var githubToken: String?
        get() = preferences.getString(GITHUB_TOKEN, null)
        set(token) {
            preferences.edit().putString(GITHUB_TOKEN, token).apply()
        }

    @JvmStatic
    @get:SortAccording
    var sortAccording: Int
        get() = preferences.getInt(LIST_SORT, SortAccording.ACCORDING_NAME)
        set(sortAccording) {
            preferences.edit().putInt(LIST_SORT, sortAccording).apply()
        }

    @JvmStatic
    var isSortEnabledFirst: Boolean
        get() = preferences.getBoolean(LIST_SORT_ENABLED_FIRST, true)
        set(enabledFirst) {
            preferences.edit().putBoolean(LIST_SORT_ENABLED_FIRST, enabledFirst).apply()
        }

    @JvmStatic
    var isFilterShowSystem: Boolean
        get() = preferences.getBoolean(LIST_FILTER_SHOW_SYSTEM, true)
        set(showSystem) {
            preferences.edit().putBoolean(LIST_FILTER_SHOW_SYSTEM, showSystem).apply()
        }

    @JvmStatic
    var isFilterHideVerified: Boolean
        get() = preferences.getBoolean(LIST_FILTER_HIDE_VERIFIED, false)
        set(hideVerified) {
            preferences.edit().putBoolean(LIST_FILTER_HIDE_VERIFIED, hideVerified).apply()
        }

    @JvmStatic
    var isFilterShowDisabled: Boolean
        get() = preferences.getBoolean(LIST_FILTER_SHOW_DISABLED, true)
        set(showDisabled) {
            preferences.edit().putBoolean(LIST_FILTER_SHOW_DISABLED, showDisabled).apply()
        }

    val isOnlineRulesEnabled: Boolean
        get() = preferences.getBoolean(ONLINE_RULES_ENABLED, true)

    @JvmStatic
    val isCustomRepoEnabled: Boolean
        get() = preferences.getBoolean(CUSTOM_REPO_ENABLED, false)

    @JvmStatic
    val customRulesRepoOwner: String
        get() = preferences.getString(CUSTOM_REPO_OWNER, null) ?: DEFAULT_REPO_OWNER

    @JvmStatic
    val customRulesRepoName: String
        get() = preferences.getString(CUSTOM_REPO_NAME, null) ?: DEFAULT_REPO_NAME

    val currentRulesRepoOwner: String
        get() = if (isCustomRepoEnabled) customRulesRepoOwner else DEFAULT_REPO_OWNER

    val currentRulesRepoName: String
        get() = if (isCustomRepoEnabled) customRulesRepoName else DEFAULT_REPO_NAME

    @JvmStatic
    fun setCustomRepoOwner(repoUser: String?) {
        preferences.edit().putString(CUSTOM_REPO_OWNER, if (repoUser.isNullOrBlank()) null else repoUser).apply()
    }

    @JvmStatic
    fun setCustomRepoName(repoName: String?) {
        preferences.edit().putString(CUSTOM_REPO_NAME, if (repoName.isNullOrBlank()) null else repoName).apply()
    }

    val currentRepoBranch: String
        get() = if (isCustomRepoEnabled) preferences.getString(CUSTOM_REPO_BRANCH, null) ?: DEFAULT_REPO_BRANCH else DEFAULT_REPO_BRANCH

    var lastInstalledVersion: Int
        get() = preferences.getInt(LAST_INSTALLED_VERSION, 0)
        set(newValue) {
            preferences.edit().putInt(LAST_INSTALLED_VERSION, newValue).apply()
        }

    @JvmStatic
    val isNewAppNotificationEnabled: Boolean
        get() = preferences.getBoolean(NEW_APP_NOTIFICATION, true)

    @JvmStatic
    val isShowMultiUser: Boolean
        get() = preferences.getBoolean(SHOW_MULTI_USER, true)

    private var sIgnoreUnsupportedStorage = false

    @JvmStatic
    val isIgnoreUnsupportedStorage: Boolean
        get() = sIgnoreUnsupportedStorage || preferences.getBoolean(IGNORE_UNSUPPORTED_STORAGE, false)

    fun setIgnoreUnsupportedStorage(newValue: Boolean, forever: Boolean) {
        sIgnoreUnsupportedStorage = newValue
        if (forever) preferences.edit().putBoolean(IGNORE_UNSUPPORTED_STORAGE, newValue).apply()
    }

    @JvmStatic
    var fileMonitorPathFilterKeyword: String?
        get() = preferences.getString(FILE_MONITOR_PATH_FILTER_KEYWORD, null)
        set(keyword) {
            preferences.edit().putString(FILE_MONITOR_PATH_FILTER_KEYWORD, keyword).apply()
        }

    @JvmStatic
    @get:PackageFilterMode
    var fileMonitorPackageFilterMode: Int
        get() = preferences.getInt(FILE_MONITOR_PACKAGE_FILTER_MODE, PackageFilterMode.NONE)
        set(filterMode) {
            preferences.edit().putInt(FILE_MONITOR_PACKAGE_FILTER_MODE, filterMode).apply()
        }

    @JvmStatic
    var fileMonitorPackageFilterList: List<String>
        get() = ArrayList(preferences.getStringSet(
                FILE_MONITOR_PACKAGE_FILTER_LIST, emptySet())!!)
        set(list) {
            preferences.edit()
                    .putStringSet(FILE_MONITOR_PACKAGE_FILTER_LIST, HashSet(list))
                    .apply()
        }

    private var sIgnoreIgnoreNoLog = false
    fun setIgnoreIgnoreNoLog(newValue: Boolean, forever: Boolean) {
        sIgnoreIgnoreNoLog = newValue
        if (forever) preferences.edit().putBoolean(IGNORE_NO_LOG, newValue).apply()
    }

    @JvmStatic
    val isIgnoreNoLog: Boolean
        get() = sIgnoreIgnoreNoLog || preferences.getBoolean(IGNORE_NO_LOG, false)

    @JvmStatic
    var isIgnoreModuleIssue = false

    @get:SortMode
    var viewGalleryDefaultMode: Int
        get() = preferences.getInt(VIEW_GALLERY_DEFAULT_MODE, ViewGalleryActivity.SORT_MODE_FOLDERS)
        set(mode) {
            preferences.edit().putInt(VIEW_GALLERY_DEFAULT_MODE, mode).apply()
        }

    val reduceHelp: Boolean
        get() = preferences.getBoolean(REDUCE_HELP, false)

    @IntDef(SortAccording.ACCORDING_NAME, SortAccording.ACCORDING_INSTALL_TIME, SortAccording.ACCORDING_UPDATE_TIME)
    @Retention(AnnotationRetention.SOURCE)
    annotation class SortAccording {
        companion object {
            const val ACCORDING_NAME = 0
            const val ACCORDING_INSTALL_TIME = 1
            const val ACCORDING_UPDATE_TIME = 2
        }
    }

    @IntDef(PackageFilterMode.NONE, PackageFilterMode.WHITELIST, PackageFilterMode.BLACKLIST)
    @Retention(AnnotationRetention.SOURCE)
    annotation class PackageFilterMode {
        companion object {
            const val NONE = 0
            const val WHITELIST = 1
            const val BLACKLIST = 2
        }
    }
}