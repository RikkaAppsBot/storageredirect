package moe.shizuku.redirectstorage.content;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.UserHelper;

@SuppressWarnings("JavaReflectionMemberAccess")
@SuppressLint({"PrivateApi", "BlockedPrivateApi"})
public class ContentProviders {

    private static Field field_ContentProviderHolder_provider;
    private static Method method_ActivityManagerNative_getDefault;
    private static Method method_IActivityManager_getContentProvider;
    private static Method method_ActivityThread_currentActivityThread;
    private static Method method_ActivityThread_getApplicationThread;
    private static Method method_ContentProviderProxy_asBinder;

    private static Object am;

    static {
        try {
            Class<?> class_ContentProviderHolder;

            if (Build.VERSION.SDK_INT >= 26)
                class_ContentProviderHolder = Class.forName("android.app.ContentProviderHolder");
            else
                class_ContentProviderHolder = Class.forName("android.app.IActivityManager$ContentProviderHolder");

            field_ContentProviderHolder_provider = class_ContentProviderHolder.getDeclaredField("provider");
            field_ContentProviderHolder_provider.setAccessible(true);

            method_ActivityManagerNative_getDefault = Class.forName("android.app.ActivityManagerNative").getDeclaredMethod("getDefault");
            method_ActivityManagerNative_getDefault.setAccessible(true);

            if (BuildUtils.atLeast29())
                method_IActivityManager_getContentProvider = Class.forName("android.app.IActivityManager").getDeclaredMethod("getContentProvider",
                        Class.forName("android.app.IApplicationThread"), String.class, String.class, int.class, boolean.class);
            else
                method_IActivityManager_getContentProvider = Class.forName("android.app.IActivityManager").getDeclaredMethod("getContentProvider",
                        Class.forName("android.app.IApplicationThread"), String.class, int.class, boolean.class);

            method_IActivityManager_getContentProvider.setAccessible(true);

            Class<?> class_ActivityThread = Class.forName("android.app.ActivityThread");
            method_ActivityThread_currentActivityThread = class_ActivityThread.getDeclaredMethod("currentActivityThread");
            method_ActivityThread_currentActivityThread.setAccessible(true);

            method_ActivityThread_getApplicationThread = class_ActivityThread.getDeclaredMethod("getApplicationThread");
            method_ActivityThread_currentActivityThread.setAccessible(true);
        } catch (Throwable tr) {
            Log.e(AppConstants.TAG, "reflection error", tr);
        }
    }

    public static IBinder getProviderBinder(String callingPackage, String name, boolean stable) {
        try {
            if (am == null)
                am = method_ActivityManagerNative_getDefault.invoke(null);

            Object activityThread = method_ActivityThread_currentActivityThread.invoke(null);
            Object applicationThread = method_ActivityThread_getApplicationThread.invoke(activityThread);

            Object cph;
            if (BuildUtils.atLeast29())
                cph = method_IActivityManager_getContentProvider.invoke(am, applicationThread, callingPackage, name, UserHelper.myUserId(), stable);
            else
                cph = method_IActivityManager_getContentProvider.invoke(am, applicationThread, name, UserHelper.myUserId(), stable);

            Object provider = field_ContentProviderHolder_provider.get(cph);

            if (provider != null) {
                if (method_ContentProviderProxy_asBinder == null) {
                    method_ContentProviderProxy_asBinder = provider.getClass().getDeclaredMethod("asBinder");
                }
                return (IBinder) method_ContentProviderProxy_asBinder.invoke(provider);
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(AppConstants.TAG, "reflection error", e);
        }
        return null;
    }
}
