package moe.shizuku.redirectstorage

import android.app.Activity
import android.os.Bundle
import moe.shizuku.redirectstorage.service.WorkService
import moe.shizuku.redirectstorage.utils.Logger.LOGGER

class ReceiverActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        LOGGER.i("ReceiverActivity: ${intent?.toString() ?: "intent is null"}")
        intent?.let {
            WorkService.start(this, intent)
        }

        intent = null
        finish()
    }

}
