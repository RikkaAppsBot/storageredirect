package moe.shizuku.redirectstorage.dao;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import moe.shizuku.redirectstorage.model.VerifiedApp;

@Dao
public abstract class VerifiedAppsDao {

    @Query("SELECT * FROM verified_apps")
    public abstract List<VerifiedApp> getAll();

    @Query("SELECT * FROM verified_apps WHERE package_name LIKE :packageName LIMIT 1")
    public abstract VerifiedApp find(String packageName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void put(VerifiedApp... items);

    @Query("DELETE FROM verified_apps")
    public abstract void clear();

    public void update(@NonNull Context context, @NonNull List<VerifiedApp> items) {
        clear();
        put(items.toArray(new VerifiedApp[0]));
        setLastUpdateTime(context, System.currentTimeMillis());
    }

    public static long getLastUpdateTime(@NonNull Context context) {
        return DaoHelper.getLastUpdateTime(context, "verified_apps");
    }

    public static void setLastUpdateTime(@NonNull Context context, long lastUpdateTime) {
        DaoHelper.setLastUpdateTime(context, "verified_apps", lastUpdateTime);
    }

}
