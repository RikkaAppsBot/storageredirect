package moe.shizuku.redirectstorage.dao;

import androidx.room.TypeConverter;

import com.google.gson.reflect.TypeToken;

import org.eclipse.egit.github.core.client.GsonUtils;

import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.model.LatestVersionInfo;

public class DaoConverters {

    public static class Download {

        @TypeConverter
        public static String toString(LatestVersionInfo.Download entity) {
            try {
                return GsonUtils.toJson(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @TypeConverter
        public static LatestVersionInfo.Download toDownload(String json) {
            try {
                return GsonUtils.fromJson(json, LatestVersionInfo.Download.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class Module {

        @TypeConverter
        public static String toString(List<LatestVersionInfo.Module> entity) {
            try {
                return GsonUtils.toJson(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @TypeConverter
        public static List<LatestVersionInfo.Module> toModules(String json) {
            try {
                return GsonUtils.fromJson(json, new TypeToken<ArrayList<LatestVersionInfo.Module>>() {
                }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class Dialog {

        @TypeConverter
        public static String toString(List<LatestVersionInfo.DialogInfo> entity) {
            try {
                return GsonUtils.toJson(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @TypeConverter
        public static List<LatestVersionInfo.DialogInfo> toDialogs(String json) {
            try {
                return GsonUtils.fromJson(json, new TypeToken<ArrayList<LatestVersionInfo.DialogInfo>>() {
                }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        public static class Button {

            @TypeConverter
            public static String toString(LatestVersionInfo.DialogInfo.ButtonInfo entity) {
                try {
                    return GsonUtils.toJson(entity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @TypeConverter
            public static LatestVersionInfo.DialogInfo.ButtonInfo toButton(String json) {
                try {
                    return GsonUtils.fromJson(json, LatestVersionInfo.DialogInfo.ButtonInfo.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }
    }
}
