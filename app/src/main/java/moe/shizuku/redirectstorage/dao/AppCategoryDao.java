package moe.shizuku.redirectstorage.dao;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import moe.shizuku.redirectstorage.model.AppCategory;

@Dao
public abstract class AppCategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void putBaseItem(AppCategory.Item... items);

    @Query("SELECT * FROM app_category_base_item")
    public abstract List<AppCategory.Item> getBaseItems();

    @Query("DELETE FROM app_category_base_item")
    public abstract void clearBaseItems();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void putDescriptionItem(AppCategory.DescriptionItem... items);

    @Query("SELECT * FROM app_category_description_item")
    public abstract List<AppCategory.DescriptionItem> getDescriptionItems();

    @Query("DELETE FROM app_category_description_item")
    public abstract void clearDescriptionItems();

    public void update(@NonNull Context context,
                       @NonNull List<AppCategory.Item> items,
                       @NonNull List<AppCategory.DescriptionItem> descriptions) {
        clearBaseItems();
        clearDescriptionItems();
        putBaseItem(items.toArray(new AppCategory.Item[0]));
        putDescriptionItem(descriptions.toArray(new AppCategory.DescriptionItem[0]));
        setLastUpdateTime(context, System.currentTimeMillis());
    }

    public static long getLastUpdateTime(@NonNull Context context) {
        return DaoHelper.getLastUpdateTime(context, "app_category_item");
    }

    public static void setLastUpdateTime(@NonNull Context context, long lastUpdateTime) {
        DaoHelper.setLastUpdateTime(context, "app_category_item", lastUpdateTime);
    }

}
