package moe.shizuku.redirectstorage.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import moe.shizuku.redirectstorage.model.AppCategory;
import moe.shizuku.redirectstorage.model.LatestVersionInfo;
import moe.shizuku.redirectstorage.model.VerifiedApp;
import rikka.internal.dao.DaoMultiLocaleEntitConverter;
import rikka.internal.dao.HelpEntitiesDao;
import rikka.internal.help.HelpEntity;

@Database(
        entities = {
                VerifiedApp.class,
                HelpEntity.class,
                LatestVersionInfo.class,
                AppCategory.Item.class,
                AppCategory.DescriptionItem.class
        },
        version = 12,
        exportSchema = false
)
@TypeConverters(
        {
                DaoConverters.Dialog.class,
                DaoConverters.Dialog.Button.class,
                DaoConverters.Module.class,
                DaoConverters.Download.class,
                DaoMultiLocaleEntitConverter.class
        }
)
public abstract class SRDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "sr.db";

    public abstract VerifiedAppsDao getVerifiedAppsDao();

    public abstract HelpEntitiesDao getHelpEntitiesDao();

    public abstract LatestVersionInfoDao getLatestVersionInfoDao();

    public abstract AppCategoryDao getAppCategoryDao();

    public static RoomDatabase.Builder<SRDatabase> newBuilder(Context appContext) {
        return Room.databaseBuilder(appContext, SRDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration();
    }

}
