package moe.shizuku.redirectstorage.dao;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import moe.shizuku.redirectstorage.utils.FileBrowserUtils;

final class DaoHelper {

    private static final Map<String, Long> sDaoLastUpdateTime = new HashMap<>();

    static long getLastUpdateTime(@NonNull Context context, @NonNull String daoName) {
        if (sDaoLastUpdateTime.containsKey(daoName)) {
            return sDaoLastUpdateTime.get(daoName);
        }
        File file = new File(context.getCacheDir() + File.separator + "database", "." + daoName + "_update_time");
        if (!file.exists()) {
            return 0;
        }
        if (!file.isFile()) {
            file.delete();
            return 0;
        }
        try {
            String content = FileBrowserUtils.readFileToString(file);
            long result = Long.parseLong(content);
            sDaoLastUpdateTime.put(daoName, result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    static void setLastUpdateTime(@NonNull Context context, @NonNull String daoName, long lastUpdateTime) {
        sDaoLastUpdateTime.put(daoName, lastUpdateTime);
        File file = new File(context.getCacheDir(), "." + daoName + "_update_time");
        try {
            if (file.isDirectory()) {
                file.delete();
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            FileBrowserUtils.writeStringToFile(file, String.valueOf(lastUpdateTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
