package moe.shizuku.redirectstorage.dao;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import moe.shizuku.redirectstorage.model.LatestVersionInfo;

@Dao
public abstract class LatestVersionInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void put(LatestVersionInfo... info);

    @Query("SELECT * FROM latest_version_info LIMIT 1")
    public abstract LatestVersionInfo get();

    @Query("DELETE FROM latest_version_info")
    public abstract void clear();

    public void update(@NonNull Context context, @NonNull LatestVersionInfo info) {
        clear();
        put(info);
        setLastUpdateTime(context, System.currentTimeMillis());
    }

    public static long getLastUpdateTime(@NonNull Context context) {
        return DaoHelper.getLastUpdateTime(context, "latest_version_info");
    }

    public static void setLastUpdateTime(@NonNull Context context, long lastUpdateTime) {
        DaoHelper.setLastUpdateTime(context, "latest_version_info", lastUpdateTime);
    }
}
