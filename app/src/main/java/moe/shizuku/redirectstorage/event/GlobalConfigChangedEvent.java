package moe.shizuku.redirectstorage.event;

import androidx.annotation.NonNull;

import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;

public interface GlobalConfigChangedEvent extends Event {

    default void onMountDirsTemplateAdded(@NonNull MountDirsTemplate template) {}

    default void onMountDirsTemplateChanged(@NonNull MountDirsTemplate newTemplate, @NonNull MountDirsTemplate oldTemplate) {}

    default void onMountDirsTemplateRemoved(@NonNull MountDirsTemplate template) {}

    default void onDefaultRedirectTargetChanged(@NonNull String redirectTarget) {}

    default void onSimpleMountAdded(@NonNull SimpleMountInfo simpleMountInfo) {}

    default void onSimpleMountChanged(@NonNull SimpleMountInfo newSimpleMountInfo, @NonNull SimpleMountInfo oldSimpleMountInfo) {}

    default void onSimpleMountRemoved(@NonNull SimpleMountInfo simpleMountInfo) {}

    default void onObserverAdded(@NonNull ObserverInfo observerInfo) {}

    default void onObserverChanged(@NonNull ObserverInfo newObserver, @NonNull ObserverInfo oldObserver) {}

    default void onObserverRemoved(@NonNull ObserverInfo observerInfo) {}

}
