package moe.shizuku.redirectstorage.event;

import android.content.Intent;
import android.os.Parcelable;

import rikka.recyclerview.BaseViewHolder;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public final class EventIntents {

    private EventIntents() {
    }

    public static final class CommonList {

        private CommonList() {
        }

        public static final String ACTION_ITEM_CLICK_FORMAT = "%s.action.ON_CLICK";

        public static Intent notifyItemClick(Class<?> viewHolderClass) {
            return new Intent(String.format(ACTION_ITEM_CLICK_FORMAT, viewHolderClass.getName()));
        }

        public static <T extends Parcelable> Intent notifyItemClick(
                Class<?> viewHolderClass, T data
        ) {
            return notifyItemClick(viewHolderClass).putExtra(EXTRA_DATA, data);
        }

        public static Intent notifyItemClick(Class<?> viewHolderClass, String data) {
            return notifyItemClick(viewHolderClass).putExtra(EXTRA_DATA, data);
        }

        public static Intent notifyItemClick(Class<?> viewHolderClass, int data) {
            return notifyItemClick(viewHolderClass).putExtra(EXTRA_DATA, data);
        }

        public static Intent notifyItemClick(Class<?> viewHolderClass, boolean data) {
            return notifyItemClick(viewHolderClass).putExtra(EXTRA_DATA, data);
        }

        public static <T extends Parcelable, VH extends BaseViewHolder<T>>
        Intent notifyCurrentItemClick(VH viewHolder) {
            return notifyItemClick(viewHolder.getClass(), viewHolder.getData());
        }

        public static <VH extends BaseViewHolder<String>>
        Intent notifyCurrentStringItemClick(VH viewHolder) {
            return notifyItemClick(viewHolder.getClass(), viewHolder.getData());
        }

    }

}
