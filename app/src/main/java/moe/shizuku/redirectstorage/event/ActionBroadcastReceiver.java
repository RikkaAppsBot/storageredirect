package moe.shizuku.redirectstorage.event;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import java.util.Objects;

import io.reactivex.functions.BiConsumer;

public abstract class ActionBroadcastReceiver extends BroadcastReceiver {

    public static ActionBroadcastReceiver createReceiver(
            @NonNull String action, @NonNull BiConsumer<Context, Intent> biConsumer) {
        return new ActionBroadcastReceiver(action) {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    biConsumer.accept(context, intent);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    private final String mAction;

    public ActionBroadcastReceiver(@NonNull String action) {
        mAction = Objects.requireNonNull(action);
    }

    @NonNull
    public String getAction() {
        return mAction;
    }

}
