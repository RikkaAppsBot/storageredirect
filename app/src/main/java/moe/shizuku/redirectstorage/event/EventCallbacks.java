package moe.shizuku.redirectstorage.event;

import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

import moe.shizuku.redirectstorage.ObserverInfo;

import static moe.shizuku.redirectstorage.AppConstants.ACTION_REMOVE_OBSERVER;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public final class EventCallbacks {

    private EventCallbacks() {
    }

    public static final class AppRedirectConfig {

        private AppRedirectConfig() {
        }

        public static ActionBroadcastReceiver whenRequestRemoveObserver(
                @NonNull Consumer<ObserverInfo> consumer
        ) {
            return ActionBroadcastReceiver.createReceiver(ACTION_REMOVE_OBSERVER,
                    (context, intent) -> consumer.accept(intent.getParcelableExtra(EXTRA_DATA)));
        }

    }

    public static final class CommonList {

        private CommonList() {
        }

        public static ActionBroadcastReceiver whenItemClick(
                Class<?> viewHolderClass, Runnable runnable
        ) {
            return ActionBroadcastReceiver.createReceiver(
                    String.format(EventIntents.CommonList.ACTION_ITEM_CLICK_FORMAT,
                            viewHolderClass.getName()),
                    (context, intent) -> runnable.run()
            );
        }

        public static <T extends Parcelable> ActionBroadcastReceiver whenItemClickWithParcelable(
                Class<?> viewHolderClass, Consumer<T> consumer
        ) {
            return ActionBroadcastReceiver.createReceiver(
                    String.format(EventIntents.CommonList.ACTION_ITEM_CLICK_FORMAT,
                            viewHolderClass.getName()),
                    (context, intent) -> consumer.accept(intent.getParcelableExtra(EXTRA_DATA))
            );
        }

        public static ActionBroadcastReceiver whenItemClickWithString(
                Class<?> viewHolderClass, Consumer<String> consumer
        ) {
            return ActionBroadcastReceiver.createReceiver(
                    String.format(EventIntents.CommonList.ACTION_ITEM_CLICK_FORMAT,
                            viewHolderClass.getName()),
                    (context, intent) -> consumer.accept(intent.getStringExtra(EXTRA_DATA))
            );
        }

        public static ActionBroadcastReceiver whenItemClickWithInteger(
                Class<?> viewHolderClass, Consumer<Integer> consumer
        ) {
            return ActionBroadcastReceiver.createReceiver(
                    String.format(EventIntents.CommonList.ACTION_ITEM_CLICK_FORMAT,
                            viewHolderClass.getName()),
                    (context, intent) -> consumer.accept(intent.getIntExtra(EXTRA_DATA, 0))
            );
        }

        public static ActionBroadcastReceiver whenItemClickWithBoolean(
                Class<?> viewHolderClass, Consumer<Boolean> consumer
        ) {
            return ActionBroadcastReceiver.createReceiver(
                    String.format(EventIntents.CommonList.ACTION_ITEM_CLICK_FORMAT,
                            viewHolderClass.getName()),
                    (context, intent) -> consumer.accept(intent.getBooleanExtra(EXTRA_DATA, false))
            );
        }

    }

}
