package moe.shizuku.redirectstorage.event

import android.content.Context
import android.widget.Toast
import moe.shizuku.redirectstorage.R

typealias ErrorHandler = (Throwable) -> Unit

open class ToastErrorHandler(val context: Context) : ErrorHandler {

    override fun invoke(throwable: Throwable) {
        throwable.printStackTrace()
        Toast.makeText(context,
                context.getString(R.string.toast_failed, throwable.toString()), Toast.LENGTH_SHORT).show()
    }
}