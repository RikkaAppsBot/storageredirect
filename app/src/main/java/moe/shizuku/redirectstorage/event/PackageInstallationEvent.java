package moe.shizuku.redirectstorage.event;

import moe.shizuku.redirectstorage.RedirectPackageInfo;

public interface PackageInstallationEvent {

    default void onPackageAdded(RedirectPackageInfo packageInfo) {}

    default void onPackageRemoved(String packageName, int userId) {}
}
