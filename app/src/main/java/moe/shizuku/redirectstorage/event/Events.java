package moe.shizuku.redirectstorage.event;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;

import kotlin.jvm.functions.Function1;

public class Events {

    private static class Receiver<T> {

        public T event;
        public Object[] tags;

        private Receiver(T event, Object[] tags) {
            this.event = event;
            this.tags = tags;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Receiver<?> receiver1 = (Receiver<?>) o;
            return event.equals(receiver1.event) &&
                    Arrays.equals(tags, receiver1.tags);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(event);
            result = 31 * result + Arrays.hashCode(tags);
            return result;
        }
    }

    private static class ReceiverSet<EV> extends CopyOnWriteArraySet<Receiver<EV>> {

        public void removeAll(Receiver<EV> receiver) {
            for (Receiver<EV> r : new ArrayList<>(this)) {
                if (r.event == receiver) {
                    remove(r);
                }
            }
        }
    }

    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    private static <T> void post(Function1<T, ?> consumer, Collection<Receiver<T>> receivers, Object... tags) {
        for (Receiver<T> receiver : receivers) {
            if (receiver.tags.length != 0 && !Arrays.equals(tags, receiver.tags)) {
                continue;
            }

            if (receiver.event instanceof LifecycleOwner) {
                LifecycleOwner lifecycleOwner = (LifecycleOwner) receiver.event;
                if (!lifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
                    continue;
                }
            }

            if (Looper.myLooper() != Looper.getMainLooper()) {
                mainThreadHandler.post(() -> consumer.invoke(receiver.event));
            } else {
                consumer.invoke(receiver.event);
            }
        }
    }

    private static final ReceiverSet<GlobalConfigChangedEvent> globalConfigChangedEventReceivers = new ReceiverSet<>();

    private static final ReceiverSet<AppConfigChangedEvent> appConfigChangedEventReceivers = new ReceiverSet<>();

    private static final ReceiverSet<PackageInstallationEvent> packageInstallationEventReceivers = new ReceiverSet<>();

    public static void registerGlobalConfigChangedEvent(@NonNull GlobalConfigChangedEvent receiver, Object... tags) {
        globalConfigChangedEventReceivers.add(new Receiver<>(receiver, tags));
    }

    public static void registerAppConfigChangedEvent(@NonNull AppConfigChangedEvent receiver, Object... tags) {
        appConfigChangedEventReceivers.add(new Receiver<>(receiver, tags));
    }

    public static void registerPackageInstallationEvent(@NonNull PackageInstallationEvent receiver, Object... tags) {
        packageInstallationEventReceivers.add(new Receiver<>(receiver, tags));
    }

    public static void unregisterGlobalConfigChangedEvent(@NonNull GlobalConfigChangedEvent receiver, Object... tags) {
        globalConfigChangedEventReceivers.remove(new Receiver<>(receiver, tags));
    }

    public static void unregisterAppConfigChangedEvent(@NonNull AppConfigChangedEvent receiver, Object... tags) {
        appConfigChangedEventReceivers.remove(new Receiver<>(receiver, tags));
    }

    public static void unregisterPackageInstallationEvent(@NonNull PackageInstallationEvent receiver, Object... tags) {
        packageInstallationEventReceivers.remove(new Receiver<>(receiver, tags));
    }

    public static void postGlobalConfigChangedEvent(Function1<GlobalConfigChangedEvent, ?> consumer) {
        post(consumer, globalConfigChangedEventReceivers);
    }

    public static void postAppConfigChangedEvent(String packageName, int userId, Function1<AppConfigChangedEvent, ?> consumer) {
        post(consumer, appConfigChangedEventReceivers, packageName, userId);
    }

    public static void postPackageInstallationEvent(Function1<PackageInstallationEvent, ?> consumer) {
        post(consumer, packageInstallationEventReceivers);
    }
}
