package moe.shizuku.redirectstorage.event

import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.utils.DataVerifier

object RemoteRequestHandler {

    fun setEnabled(packageName: String, sharedUserId: String?, userId: Int, enabled: Boolean, errorHandler: ErrorHandler? = null): Boolean {
        try {
            if (enabled) {
                val info = RedirectPackageInfo(packageName, userId).apply { this.enabled = true }
                SRManager.createThrow().addRedirectPackage(info, SRManager.FLAG_KILL_PACKAGE or SRManager.FLAG_NO_PERMISSION_CHECK or SRManager.FLAG_TOGGLE_ENABLED_ONLY)
            } else {
                SRManager.createThrow().removeRedirectPackage(packageName, userId)
            }
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        Events.postAppConfigChangedEvent(packageName, userId) {
            it.onEnabledChanged(packageName, sharedUserId, userId, enabled)
        }
        return true
    }

    @JvmStatic
    fun addMountDirsTemplate(template: MountDirsTemplate, errorHandler: ErrorHandler? = null) {
        val id = try {
            SRManager.createThrow().addMountDirsTemplate(template)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return
        }

        template.id = id

        Events.postGlobalConfigChangedEvent { it.onMountDirsTemplateAdded(template) }
    }

    @JvmStatic
    fun updateMountDirsTemplate(newTemplate: MountDirsTemplate, oldTemplate: MountDirsTemplate, errorHandler: ErrorHandler? = null) {
        if (oldTemplate.contentEquals(newTemplate) && oldTemplate.title == newTemplate.title) {
            // Nothing changed
            return
        }

        try {
            SRManager.createThrow().updateMountDirsTemplate(newTemplate)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return
        }

        Events.postGlobalConfigChangedEvent { it.onMountDirsTemplateChanged(newTemplate, oldTemplate) }
    }

    @JvmStatic
    fun removeMountDirsTemplate(template: MountDirsTemplate, errorHandler: ErrorHandler? = null) {
        try {
            SRManager.createThrow().removeMountDirsTemplate(template.id)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return
        }

        Events.postGlobalConfigChangedEvent { it.onMountDirsTemplateRemoved(template) }
        return
    }

    @JvmStatic
    fun setDefaultRedirectTarget(path: String, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().setDefaultRedirectTarget(path)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postGlobalConfigChangedEvent { it.onDefaultRedirectTargetChanged(path) }
        }
        return res
    }

    @JvmStatic
    fun setMountDirsForPackage(config: MountDirsConfig, packageName: String, sharedUserId: String?, userId: Int, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().setMountDirsForPackage(config, packageName, userId)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }
        if (res) {
            Events.postAppConfigChangedEvent(packageName, userId) { it.onMountDirsConfigChanged(packageName, sharedUserId, userId, config) }
        }
        return false
    }

    @JvmStatic
    fun setRedirectTargetForPackage(path: String?, packageName: String, sharedUserId: String?, userId: Int, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().setRedirectTargetForPackage(path, packageName, userId)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postAppConfigChangedEvent(packageName, userId) { it.onRedirectTargetChanged(packageName, sharedUserId, userId, path) }
        }
        return false
    }

    @JvmStatic
    fun addSimpleMount(smi: SimpleMountInfo, flags: Int = SRManager.FLAG_KILL_PACKAGE or SRManager.FLAG_FORCE_ENABLE_REDIRECT, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().addSimpleMount(smi, flags)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postGlobalConfigChangedEvent { it.onSimpleMountAdded(smi) }
        }
        return false
    }

    @JvmStatic
    fun updateSimpleMount(newSimpleMountInfo: SimpleMountInfo, oldSimpleMountInfo: SimpleMountInfo, flags: Int = SRManager.FLAG_KILL_PACKAGE or SRManager.FLAG_FORCE_ENABLE_REDIRECT, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().updateSimpleMount(newSimpleMountInfo, flags)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postGlobalConfigChangedEvent { it.onSimpleMountChanged(newSimpleMountInfo, oldSimpleMountInfo) }
        }
        return false
    }

    @JvmStatic
    fun removeSimpleMount(smi: SimpleMountInfo, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().removeSimpleMount(smi)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postGlobalConfigChangedEvent { it.onSimpleMountRemoved(smi) }
        }
        return false
    }

    @JvmStatic
    fun addObserver(observerInfo: ObserverInfo, errorHandler: ErrorHandler? = null): ObserverInfo.Result? {
        val res: ObserverInfo.Result
        try {
            res = SRManager.createThrow().addObserver(observerInfo)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return null
        }

        if (res.conflict == DataVerifier.ObserverInfoConflict.NONE) {
            Events.postGlobalConfigChangedEvent { it.onObserverAdded(observerInfo) }
        }
        return res
    }

    @JvmStatic
    fun updateObserver(newObserverInfo: ObserverInfo, oldObserverInfo: ObserverInfo, errorHandler: ErrorHandler? = null): ObserverInfo.Result? {
        val res: ObserverInfo.Result
        try {
            res = SRManager.createThrow().updateObserver(newObserverInfo)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return null
        }

        if (res.conflict == DataVerifier.ObserverInfoConflict.NONE) {
            Events.postGlobalConfigChangedEvent { it.onObserverChanged(newObserverInfo, oldObserverInfo) }
        }
        return res
    }

    @JvmStatic
    fun removeObserver(oi: ObserverInfo, errorHandler: ErrorHandler? = null): Boolean {
        val res: Boolean
        try {
            res = SRManager.createThrow().removeObserver(oi)
        } catch (e: Throwable) {
            errorHandler?.invoke(e)
            return false
        }

        if (res) {
            Events.postGlobalConfigChangedEvent { it.onObserverRemoved(oi) }
        }
        return false
    }
}

