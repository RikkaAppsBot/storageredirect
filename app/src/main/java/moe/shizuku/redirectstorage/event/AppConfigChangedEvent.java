package moe.shizuku.redirectstorage.event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import moe.shizuku.redirectstorage.MountDirsConfig;

public interface AppConfigChangedEvent extends Event {

    default void onEnabledChanged(@NonNull String packageName, @Nullable String sharedUserId, int userId, boolean enabled) {
    }

    default void onMountDirsConfigChanged(@NonNull String packageName, @Nullable String sharedUserId, int userId, @NonNull MountDirsConfig config) {
    }

    default void onRedirectTargetChanged(@NonNull String packageName, @Nullable String sharedUserId, int userId, @Nullable String path) {
    }
}
