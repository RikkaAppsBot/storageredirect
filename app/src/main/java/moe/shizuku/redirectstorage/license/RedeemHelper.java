package moe.shizuku.redirectstorage.license;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rikka.internal.payment.model.TokenResult;

import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.RESULT_INVALID;

public class RedeemHelper {

    private final static String KEY_REDEEM_NEXT_CHECK = "lXsUgcH3UYdfUtkg";

    public final static String KEY_REDEEM_CODE = "33tpAXJTbMGV8P0C";

    public static String getRedeemCode() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_REDEEM_CODE, null);
    }

    public static void setRedeemCode(String code) {
        if (!DeviceSpecificSettings.getPreferences().edit().putString(KEY_REDEEM_CODE, code).commit()) {
            throw new RuntimeException();
        }
    }

    public static boolean checkLocal() {
        return !TextUtils.isEmpty(getRedeemCode());
    }

    public static void check(final Context context) {
        if (!checkLocal()) {
            return;
        }

        if (System.currentTimeMillis() < DeviceSpecificSettings.getPreferences().getLong(KEY_REDEEM_NEXT_CHECK, 0)
                && !BuildConfig.DEBUG) {
            return;
        }

        ContextKt.getApplication(context).getPaymentService().verifyRedeemCode(License.PACKAGE_NAME, License.PRODUCT_ID, getRedeemCode())
                .enqueue(new Callback<TokenResult>() {
                    @Override
                    public void onResponse(@NonNull Call<TokenResult> call, @NonNull Response<TokenResult> response) {
                        if (response.code() != 200 || response.body() == null) {
                            onFailure(call, new RuntimeException("bad response"));
                            return;
                        }

                        SharedPreferences.Editor editor = DeviceSpecificSettings.getPreferences().edit();
                        if (response.body().code == RESULT_INVALID) {
                            /*LocalBroadcastManager.getInstance(context)
                                        .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));*/

                            editor.remove(KEY_REDEEM_CODE);
                        }
                        editor.putLong("rdn", System.currentTimeMillis() + 24 * 60 * 60 * 1000)
                                .apply();
                    }

                    @Override
                    public void onFailure(@NonNull Call<TokenResult> call, @NonNull Throwable t) {
                        if (BuildConfig.DEBUG) {
                            t.printStackTrace();
                        }
                    }
                });
    }

}
