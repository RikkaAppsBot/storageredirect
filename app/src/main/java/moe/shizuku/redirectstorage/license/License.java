package moe.shizuku.redirectstorage.license;

import moe.shizuku.redirectstorage.SRManager;

public class License {

    public static final String PACKAGE_NAME = "moe.shizuku.redirectstorage";
    public static final String PRODUCT_ID = "unlock";

    public static boolean checkLocal() {
        return GoogleBillingHelper.checkLocal()
                || AlipayHelper.checkLocal()
                || RedeemHelper.checkLocal();
    }

    public static void resetNativeLicense() {
        native_doAction(0, SRManager.requestBinder());
    }

    @SuppressWarnings("JavaJniMissingFunction")
    private static native void native_doAction(int action, Object arg);
}
