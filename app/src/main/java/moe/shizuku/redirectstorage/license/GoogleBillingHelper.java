package moe.shizuku.redirectstorage.license;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponseCode;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import moe.shizuku.redirectstorage.utils.CrashReportHelper;
import moe.shizuku.redirectstorage.utils.PackageManagerUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rikka.html.text.HtmlCompat;
import rikka.internal.help.HelpEntity;
import rikka.internal.help.HelpProvider;
import rikka.internal.payment.model.TokenResult;

import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.RESULT_INVALID;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.RESULT_OK;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.RESULT_SERVER_ERROR;

public class GoogleBillingHelper {

    private static final String GOOGLE_PLAY_PACKAGE = "com.android.vending";
    private static final String SKU = "unlock";

    private static final String KEY_ORDER_ID = "N6PkdEVUxGOPI6ln";
    private static final String KEY_PURCHASE_TIME = "DvA49SjnAg8JcIYD";

    private static final String KEY_PRICE = "lpqJ2bdPaxSFQgUy";
    private static final String KEY_SKU_TITLE = "XFZ8JMM3BZZGFj44";
    private static final String KEY_SKU_DESC = "qdeENl84sWtHJmrC";

    public static final String KEY_TOKEN = "2VgAwGthzMqFFpHs";

    private static SkuDetails details;
    private static BillingClient client;

    public static boolean isGooglePlayAvailable(Context context) {
        return PackageManagerUtils.isPackageEnabled(context, GOOGLE_PLAY_PACKAGE);
    }

    public static boolean isInstallerGooglePlay(Context context) {
        String packageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
        return packageName != null && packageName.trim().replace("\"", "").equals(GOOGLE_PLAY_PACKAGE);
    }

    public static boolean checkLocal() {
        String token = getToken();
        return !TextUtils.isEmpty(token) && token.length() > 24;
    }

    public static String getPrice() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_PRICE, "");
    }

    public static void setPrice(String value) {
        DeviceSpecificSettings.getPreferences().edit().putString(KEY_PRICE, value).apply();
    }

    public static String getToken() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_TOKEN, "");
    }

    public static String getOrderId() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_ORDER_ID, "");
    }

    public static String getItemTitle() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_SKU_TITLE, "");
    }

    public static String getItemDescription() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_SKU_DESC, "");
    }

    public static void setItemTitle(String title) {
        DeviceSpecificSettings.getPreferences().edit().putString(KEY_SKU_TITLE, title).apply();
    }

    private static void setItemDescription(String description) {
        DeviceSpecificSettings.getPreferences().edit().putString(KEY_SKU_DESC, description).apply();
    }

    public static long getPurchaseTime() {
        return DeviceSpecificSettings.getPreferences().getLong(KEY_PURCHASE_TIME, 0);
    }

    public static void checkToken(Context context) {
        Callback<TokenResult> callback = new Callback<TokenResult>() {
            @Override
            public void onResponse(@NonNull Call<TokenResult> call, @NonNull Response<TokenResult> response) {
                if (response.code() != 200 || response.body() == null) {
                    onFailure(call, new RuntimeException());
                    return;
                }

                if (response.body().code == RESULT_INVALID) {
                    DeviceSpecificSettings.getPreferences().edit().remove(KEY_TOKEN).apply();

                    LocalBroadcastManager.getInstance(context)
                            .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResult> call, @NonNull Throwable t) {
                if (BuildConfig.DEBUG) {
                    t.printStackTrace();
                }
            }
        };

        ContextKt.getApplication(context).getPaymentService().verifyGoogleToken(License.PACKAGE_NAME, License.PRODUCT_ID, getToken())
                .enqueue(callback);
    }

    private static void onPurchaseFound(Context context, Purchase purchase) {
        if (TextUtils.isEmpty(purchase.getPurchaseToken())) {
            return;
        }

        if (!purchase.isAcknowledged()) {
            AcknowledgePurchaseParams params = AcknowledgePurchaseParams.newBuilder()
                    .setPurchaseToken(purchase.getPurchaseToken())
                    .build();

            client.acknowledgePurchase(params, billingResult1 -> {
            });
        }

        boolean purchased = checkLocal();
        if (!purchased) {
            // for new purchase, force network check
            Callback<TokenResult> callback = new Callback<TokenResult>() {
                @Override
                public void onResponse(@NonNull Call<TokenResult> call, @NonNull Response<TokenResult> response) {
                    if (response.code() != 200 || response.body() == null) {
                        onFailure(call, new RuntimeException());
                        return;
                    }

                    switch (response.body().code) {
                        case RESULT_OK: {
                            onPurchaseSuccess(context, purchase, true);
                            break;
                        }
                        case RESULT_INVALID: {
                            showErrorMessage(context, context.getString(R.string.dialog_invalid_purchase_message));
                            break;
                        }
                        case RESULT_SERVER_ERROR: {
                            showErrorMessage(context, context.getString(R.string.dialog_network_not_available_message));
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TokenResult> call, @NonNull Throwable t) {
                    if (BuildConfig.DEBUG) {
                        t.printStackTrace();
                    }

                    showErrorMessage(context, context.getString(R.string.dialog_network_not_available_message));
                }
            };

            ContextKt.getApplication(context).getPaymentService()
                    .verifyGoogleToken(License.PACKAGE_NAME, License.PRODUCT_ID, purchase.getPurchaseToken())
                    .enqueue(callback);
        } else {
            onPurchaseSuccess(context, purchase, false);
        }
    }

    private static void onPurchaseSuccess(Context context, Purchase purchase, boolean broadcast) {
        SharedPreferences.Editor editor = DeviceSpecificSettings.getPreferences().edit();
        if (!TextUtils.isEmpty(purchase.getOrderId()))
            editor.putString(KEY_ORDER_ID, purchase.getOrderId());
        if (purchase.getPurchaseTime() > 0)
            editor.putLong(KEY_PURCHASE_TIME, purchase.getPurchaseTime());
        if (!TextUtils.isEmpty(purchase.getPurchaseToken()))
            editor.putString(KEY_TOKEN, purchase.getPurchaseToken());
        if (!editor.commit()) {
            throw new RuntimeException();
        }

        if (broadcast) {
            LocalBroadcastManager.getInstance(context)
                    .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));
        }
    }

    private static boolean checkPurchases(Context context, BillingClient client) {
        Purchase.PurchasesResult purchasesResult = client.queryPurchases(SkuType.INAPP);
        if (purchasesResult.getResponseCode() != BillingResponseCode.OK || purchasesResult.getPurchasesList() == null) {
            return false;
        }

        boolean purchased = checkLocal();
        boolean found = false;

        for (Purchase purchase : purchasesResult.getPurchasesList()) {
            if (SKU.equals(purchase.getSku())) {
                onPurchaseFound(context, purchase);

                found = true;
            }
        }

        if (!found) {
            if (!DeviceSpecificSettings.getPreferences().edit()
                    .remove(KEY_ORDER_ID)
                    .remove(KEY_PURCHASE_TIME)
                    .remove(KEY_TOKEN).commit()) {
                throw new RuntimeException();
            }

            if (purchased) {
                LocalBroadcastManager.getInstance(context)
                        .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));
            }
        }

        return found;
    }

    private static void consume(Context context, BillingClient client) {
        client.consumeAsync(ConsumeParams.newBuilder().setPurchaseToken(getToken()).build(), (billingResult, purchaseToken) -> {
            if (billingResult.getResponseCode() != BillingResponseCode.OK) {
                return;
            }

            if (!DeviceSpecificSettings.getPreferences().edit()
                    .remove(KEY_ORDER_ID)
                    .remove(KEY_PURCHASE_TIME)
                    .remove(KEY_TOKEN).commit()) {
                throw new RuntimeException();
            }

            LocalBroadcastManager.getInstance(context)
                    .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));
        });
    }

    private static void onBillingSetupFinished(Context context, BillingClient client, BillingResult result) {
        if (result.getResponseCode() != BillingResponseCode.OK) {
            return;
        }

        checkPurchases(context, client);

        List<String> sku = new ArrayList<>();
        sku.add(SKU);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(sku).setType(SkuType.INAPP);
        client.querySkuDetailsAsync(params.build(), (res, skuDetailsList) -> {
            if (res.getResponseCode() == BillingResponseCode.OK
                    && skuDetailsList != null
                    && !skuDetailsList.isEmpty()) {
                details = skuDetailsList.get(0);
                String price;
                if (details.getPriceAmountMicros() % 1000000 == 0) {
                    price = String.format(Locale.getDefault(), "%d %s", details.getPriceAmountMicros() / 1000000, details.getPriceCurrencyCode());
                } else {
                    price = String.format(Locale.getDefault(), "%.2f %s", details.getPriceAmountMicros() / 1000000.0, details.getPriceCurrencyCode());
                }

                setPrice(price);
                setItemDescription(details.getDescription());
                setItemTitle(details.getTitle());
            }
        });
    }

    public static void check(final Context context, boolean checkToken) {
        if (checkToken && GoogleBillingHelper.checkLocal()) {
            GoogleBillingHelper.checkToken(context);
        }

        final BillingClient client = BillingClient
                .newBuilder(context)
                .enablePendingPurchases()
                .setListener((responseCode, purchases) -> {

                })
                .build();

        try {
            client.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@NotNull BillingResult result) {
                    try {
                        GoogleBillingHelper.onBillingSetupFinished(context, client, result);
                        client.endConnection();
                    } catch (Throwable tr) {
                        tr.printStackTrace();

                        CrashReportHelper.logException(tr);
                    }
                }

                @Override
                public void onBillingServiceDisconnected() {

                }
            });
        } catch (Throwable tr) {
            tr.printStackTrace();
            CrashReportHelper.logException(tr);
        }

        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (GoogleBillingHelper.isGooglePlayAvailable(context)) {
                    GoogleBillingHelper.check(context.getApplicationContext(), true);
                }

                context.getApplicationContext().unregisterReceiver(this);
            }
        }, new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED"));
    }

    public static void startPurchase(final Activity activity) {
        client = BillingClient.newBuilder(activity.getApplicationContext()).enablePendingPurchases().setListener((result, purchases) -> {
            if (result.getResponseCode() == BillingResponseCode.OK
                    && purchases != null) {
                for (Purchase purchase : purchases) {
                    if (SKU.equals(purchase.getSku())) {
                        onPurchaseFound(activity, purchase);
                    }
                }
            } else if (result.getResponseCode() != BillingResponseCode.USER_CANCELED) {
                showErrorMessage(activity, result);
            }

            client.endConnection();
        }).build();

        client.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NotNull BillingResult result) {
                if (result.getResponseCode() != BillingResponseCode.OK) {
                    showErrorMessage(activity, result);
                    client.endConnection();
                    return;
                }

                try {
                    GoogleBillingHelper.onBillingSetupFinished(activity, client, result);
                } catch (Throwable tr) {
                    tr.printStackTrace();
                    CrashReportHelper.logException(tr);
                }

                if (checkPurchases(activity, client)) {
                    client.endConnection();
                    return;
                }

                List<String> skus = new ArrayList<>();
                skus.add(SKU);
                client.querySkuDetailsAsync(SkuDetailsParams.newBuilder().setType(SkuType.INAPP).setSkusList(skus).build(), (billingResult, skuDetailsList) -> {
                    if (billingResult.getResponseCode() != BillingResponseCode.OK) {
                        showErrorMessage(activity, billingResult);
                        client.endConnection();
                        return;
                    }

                    if (skuDetailsList != null && !skuDetailsList.isEmpty()) {
                        for (SkuDetails details : skuDetailsList) {
                            if (details.getSku().equals(SKU)) {
                                BillingFlowParams.Builder builder = BillingFlowParams.newBuilder()
                                        .setSkuDetails(details);

                                BillingResult res = client.launchBillingFlow(activity, builder.build());
                                showErrorMessage(activity, res);
                            }
                        }
                    }
                });
            }

            @Override
            public void onBillingServiceDisconnected() {

            }
        });
    }

    private static void showErrorMessage(Activity activity, BillingResult result) {
        if (activity.isFinishing() || result.getResponseCode() == BillingResponseCode.OK || result.getResponseCode() == BillingResponseCode.USER_CANCELED) {
            return;
        }

        HelpEntity entity = HelpProvider.get(activity, "payment");
        AlertDialog dialog;
        int responseCode = result.getResponseCode();
        String message = activity.getString(R.string.dialog_billing_error_message, entity.content.get())
                + "<br><br>"
                + String.format(Locale.ENGLISH, "Response code: %d", responseCode);

        if (BuildConfig.DEBUG) {
            message += "<br><br>"
                    + String.format(Locale.ENGLISH, "Debug message: %s", result.getDebugMessage());
        }

        dialog = new AlertDialog.Builder(activity)
                .setMessage(HtmlCompat.fromHtml(message))
                .setPositiveButton(android.R.string.ok, null)
                .show();

        ((TextView) dialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    private static void showErrorMessage(Context context, CharSequence message) {
        if (context instanceof Activity && !((Activity) context).isFinishing()) {
            try {
                AlertDialog dialog;
                dialog = new AlertDialog.Builder(context)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();

                if (dialog != null) {
                    ((TextView) dialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
                }
                return;
            } catch (Throwable ignored) {
            }
        }

        Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
