package moe.shizuku.redirectstorage.license;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Objects;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import moe.shizuku.redirectstorage.utils.UserHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rikka.internal.payment.model.AlipayOrderInfo;

import static moe.shizuku.redirectstorage.AppConstants.ACTION_PURCHASED_CHANGED;

public class AlipayHelper {

    private static final String KEY_ALIPAY_PRICE = "hIU8JD8IcmZF1qty";
    private final static String KEY_ALIPAY_CREATE_TIME = "hnpc70hTpRvBUpNb";
    private final static String KEY_ALIPAY_LAST_ORDER_ID = "wBXani7FN9IBuZio";
    private final static String KEY_ALIPAY_NEXT_CHECK = "KAYCM7iK3DLVe8Nl";

    public final static String KEY_ALIPAY_ORDER_ID = "T8yQS4LatKY0DlGe";

    private static final String PACKAGE_NAME = "com.eg.android.AlipayGphone";

    public static boolean isAlipayInstalled(Context context) {
        try {
            SRManager srm = SRManager.createThrow();
            ApplicationInfo ai = srm.getApplicationInfo(PACKAGE_NAME, 0, UserHelper.myUserId());
            return ai != null && ai.enabled;
        } catch (Throwable ignored) {
        }

        try {
            SRManager srm = SRManager.create();
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(PACKAGE_NAME, 0);
            return true/*ai.enabled*/;
        } catch (PackageManager.NameNotFoundException ignored) {
        }

        return false;
    }

    public static boolean startAlipay(Context context) {
        try {
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(PACKAGE_NAME));
            return true;
        } catch (Throwable tr) {
            return false;
        }
    }

    public static String getPrice() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_ALIPAY_PRICE, null);
    }

    public static void setPrice(String value) {
        DeviceSpecificSettings.getPreferences().edit().putString(KEY_ALIPAY_PRICE, value).apply();
    }

    public static String getOrderId() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_ALIPAY_ORDER_ID, null);
    }

    public static void setOrderId(String orderId) {
        if (!DeviceSpecificSettings.getPreferences().edit().putString(KEY_ALIPAY_ORDER_ID, orderId).commit()) {
            throw new RuntimeException();
        }
    }

    public static long getCreateTime() {
        return DeviceSpecificSettings.getPreferences().getLong(KEY_ALIPAY_CREATE_TIME, 0);
    }

    public static void setCreateTime(long createTime) {
        DeviceSpecificSettings.getPreferences().edit().putLong(KEY_ALIPAY_CREATE_TIME, createTime).apply();
    }

    public static String getLastOrderId() {
        return DeviceSpecificSettings.getPreferences().getString(KEY_ALIPAY_LAST_ORDER_ID, null);
    }

    public static void setLastOrderId(String orderId) {
        DeviceSpecificSettings.getPreferences().edit().putString(KEY_ALIPAY_LAST_ORDER_ID, orderId).apply();
    }

    public static boolean checkLocal() {
        return !TextUtils.isEmpty(getOrderId());
    }

    public static void check(final Context context) {
        if (!checkLocal()) {
            return;
        }

        if (System.currentTimeMillis() < DeviceSpecificSettings.getPreferences().getLong(KEY_ALIPAY_NEXT_CHECK, 0)
                && !BuildConfig.DEBUG) {
            return;
        }

        ContextKt.getApplication(context).getPaymentService().getAlipayOrderInfo(License.PACKAGE_NAME, License.PRODUCT_ID, getOrderId())
                .enqueue(new Callback<AlipayOrderInfo>() {
                    @Override
                    public void onResponse(@NonNull Call<AlipayOrderInfo> call, @NonNull Response<AlipayOrderInfo> response) {
                        if (response.code() != 200 || response.body() == null) {
                            onFailure(call, new RuntimeException());
                            return;
                        }

                        SharedPreferences.Editor editor = DeviceSpecificSettings.getPreferences().edit();
                        AlipayOrderInfo info = response.body();
                        if (!info.valid()
                                || !info.succeed()
                                || !Objects.equals(DeviceSpecificSettings.getInstanceId(), info.deviceInstanceId)) {
                            LocalBroadcastManager.getInstance(context)
                                    .sendBroadcast(new Intent(ACTION_PURCHASED_CHANGED));

                            editor.remove(KEY_ALIPAY_ORDER_ID);
                            editor.remove(KEY_ALIPAY_LAST_ORDER_ID);
                        }
                        editor.putLong(KEY_ALIPAY_NEXT_CHECK, System.currentTimeMillis() + 24 * 60 * 60 * 1000)
                                .apply();
                    }

                    @Override
                    public void onFailure(@NonNull Call<AlipayOrderInfo> call, @NonNull Throwable t) {
                    }
                });
    }
}
