package moe.shizuku.redirectstorage;

/**
 * Created by rikka on 2017/11/12.
 */

public class AppConstants {

    public static final String TAG = "StorageRedirectClient";

    public static final String NOTIFICATION_CHANNEL_STATUS = "starter";
    public static final String NOTIFICATION_CHANNEL_LOGCAT = "logcat";
    public static final String NOTIFICATION_CHANNEL_DOWNLOAD = "download";
    public static final String NOTIFICATION_CHANNEL_PACKAGE_ADDED = "package_added";
    public static final String NOTIFICATION_CHANNEL_WORK_SERVICE = "work_service";
    public static final String NOTIFICATION_CHANNEL_BACKUP_RESTORE = "backup_and_restore";
    public static final String NOTIFICATION_CHANNEL_RECEIVED_FROM_OTHER_DEVICES = "received_from_other_devices";
    public static final int NOTIFICATION_ID_STATUS = 1;
    public static final int NOTIFICATION_ID_LOGCAT = 2;
    public static final int NOTIFICATION_ID_LOGCAT_DELETE_HISTORY = 3;
    public static final int NOTIFICATION_ID_BACKUP = 4;
    public static final int NOTIFICATION_ID_RECEIVED = 5;
    public static final int NOTIFICATION_ID_WORKING = 6;

    public static final String ACTION_PREFIX = BuildConfig.APPLICATION_ID + ".action";
    public static final String ACTION_PURCHASED_CHANGED = ACTION_PREFIX + ".PURCHASED_CHANGED";
    public static final String ACTION_CANCEL = ACTION_PREFIX + ".CANCEL";
    public static final String ACTION_STARTER_LINE = ACTION_PREFIX + ".STARTER_LINE";
    public static final String ACTION_STARTER_EXIT = ACTION_PREFIX + ".STARTER_EXIT";
    public static final String ACTION_SERVER_STARTED = ACTION_PREFIX + ".SERVER_STARTED";
    public static final String ACTION_ADD_TO_DOWNLOADS = ACTION_PREFIX + ".ADD_TO_DOWNLOADS";
    public static final String ACTION_LOAD_MORE = ACTION_PREFIX + ".LOAD_MORE";
    public static final String ACTION_FILE_MONITOR_MODE_CHANGED = ACTION_PREFIX + ".FILE_MONITOR_MODE_CHANGED";
    public static final String ACTION_FILE_MONITOR_SEARCH_PACKAGE_CHANGED = ACTION_PREFIX + ".FILE_MONITOR_SEARCH_PACKAGE_CHANGED";
    public static final String ACTION_APP_REDIRECT_CONFIGURATION_CHANGED = ACTION_PREFIX + ".APP_REDIRECT_CONFIGURATION_CHANGED";
    public static final String ACTION_BACKUP = ACTION_PREFIX + ".BACKUP";
    public static final String ACTION_RESTORE = ACTION_PREFIX + ".RESTORE";
    public static final String ACTION_BACKUP_FINISHED = ACTION_PREFIX + ".BACKUP_FINISHED";
    public static final String ACTION_GLOBAL_MOUNT_DIRS_CHANGED = ACTION_PREFIX + ".GLOBAL_MOUNT_DIRS_CHANGED";
    public static final String ACTION_REMOVE_OBSERVER = ACTION_PREFIX + ".REMOVE_OBSERVER";
    public static final String ACTION_REMOVE_SIMPLE_MOUNT = ACTION_PREFIX + ".REMOVE_SIMPLE_MOUNT";
    public static final String ACTION_REQUEST_REFRESH_LIST = ACTION_PREFIX + ".REQUEST_REFRESH_LIST";
    public static final String ACTION_GITHUB_AUTHORIZED = ACTION_PREFIX + ".GITHUB_AUTHORIZED";
    public static final String ACTION_GITHUB_AUTHORIZE_FAILED = ACTION_PREFIX + ".GITHUB_AUTHORIZE_FAILED";
    public static final String ACTION_REQUEST_REFRESH_HOME = ACTION_PREFIX + ".REQUEST_REFRESH_HOME";
    public static final String ACTION_REQUEST_CLEAR_APP_LIST = ACTION_PREFIX + ".REQUEST_CLEAR_APP_LIST";

    public static final String EXTRA_PREFIX = BuildConfig.APPLICATION_ID + ".extra";
    public static final String EXTRA_KEY = EXTRA_PREFIX + ".KEY";
    public static final String EXTRA_DATA = EXTRA_PREFIX + ".DATA";
    public static final String EXTRA_PACKAGE_NAME = EXTRA_PREFIX + ".PACKAGE_NAME";
    public static final String EXTRA_SHARED_USER_ID = EXTRA_PREFIX + ".SHARED_USER_ID";
    public static final String EXTRA_EXIT_CODE = EXTRA_PREFIX + ".CODE";
    public static final String EXTRA_FROM_UI = EXTRA_PREFIX + ".FROM_UI";
    public static final String EXTRA_SOURCE_ACTION = EXTRA_PREFIX + ".SOURCE_ACTION";
    public static final String EXTRA_USER_ID = Constants.APPLICATION_ID + ".extra.USER_ID";
    public static final String EXTRA_PATH = Constants.APPLICATION_ID + ".extra.PATH";
    public static final String EXTRA_MIME_TYPE = Constants.APPLICATION_ID + ".extra.MIME_TYPE";
    public static final String EXTRA_TITLE = Constants.APPLICATION_ID + ".extra.TITLE";
    public static final String EXTRA_SUBTITLE = EXTRA_PREFIX + ".SUBTITLE";
    public static final String EXTRA_CONFIG_CHANGED_TYPE = Constants.APPLICATION_ID + ".extra.CONFIG_CHANGED_TYPE";
    public static final String EXTRA_OLD_DATA = EXTRA_PREFIX + ".OLD_DATA";
    public static final String EXTRA_MOUNT_DIRS_TEMPLATE_ID = EXTRA_PREFIX + ".MOUNT_DIRS_TEMPLATE_ID";
    public static final String EXTRA_PACKAGE_FILTER_LIST = EXTRA_PREFIX + ".PACKAGE_FILTER_LIST";
    public static final String EXTRA_PACKAGE_FILTER_MODE = EXTRA_PREFIX + ".PACKAGE_FILTER_MODE";
    public static final String EXTRA_FILTER_KEYWORD = EXTRA_PREFIX + ".FILTER_KEYWORD";
    public static final String EXTRA_APP_INFO = EXTRA_PREFIX + ".APP_INFO";
    public static final String EXTRA_LABEL = Constants.APPLICATION_ID + ".extra.LABEL";

    public static final String STATE_PREFIX = BuildConfig.APPLICATION_ID + ".state";

    public static final long VERIFIED_APPS_CACHE_MAX_ALIVE_TIME = 10 * 60 * 1000;

    public static final String SUPPORT_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".SupportFileProvider";

    public static final Object PAYLOAD = new Object();

    public static final byte[] BACKUP_MAGIC = new byte[]{0x53, 0x52, 0x42, 0x41, 0x43, 0x4b, 0x55, 0x50}; // SRBACKUP
    public static final byte BACKUP_XOR = 1;

}
