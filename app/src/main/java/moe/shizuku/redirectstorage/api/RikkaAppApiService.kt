package moe.shizuku.redirectstorage.api

import moe.shizuku.redirectstorage.model.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import rikka.internal.help.HelpEntity

interface RikkaAppApiService {

    @GET("storage_redirect/help.json")
    suspend fun getHelpDocumentsAsync(): Response<List<HelpEntity>>

    @GET("storage_redirect/update.json")
    suspend fun getUpdateVersionInfoAsync(): Response<LatestVersionInfo>

    @GET("storage_redirect/app_category/app_categories_data.json")
    suspend fun getAppCategoryBaseDataAsync(): Response<List<AppCategory.Item>>

    @GET("storage_redirect/app_category/apps/{package_name}.json")
    suspend fun getAppCategorySuggestionAsync(
            @Path("package_name") packageName: String
    ): Response<AppCategory.AppSuggestion>

    @GET("storage_redirect/app_category/app_categories_description.json")
    suspend fun getAppCategoriesDescriptionSync(): Response<List<AppCategory.DescriptionItem>>

    @GET("storage_redirect/app_rule/app_recommendation_data.json")
    suspend fun getAppRecommendationDataAsync(): Response<AppRecommendationTexts>

    @GET("storage_redirect/app_rule/apps/{packageName}.json")
    suspend fun getConfigurationAsync(
            @Path("packageName") packageName: String
    ): Response<AppConfiguration>

    @GET("storage_redirect/app_rule/verified_apps.json")
    suspend fun getVerifiedAppsSync(): Response<List<VerifiedApp>>

}
