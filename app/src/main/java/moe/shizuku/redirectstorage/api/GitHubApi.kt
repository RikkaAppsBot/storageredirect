package moe.shizuku.redirectstorage.api

import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.webview.WebViewActivity
import moe.shizuku.redirectstorage.ktx.fromJson
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.CustomTabsHelper
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.eclipse.egit.github.core.*
import org.eclipse.egit.github.core.client.GitHubClient
import org.eclipse.egit.github.core.service.GitHubService
import org.eclipse.egit.github.core.service.IssueService
import org.eclipse.egit.github.core.service.RepositoryService
import org.eclipse.egit.github.core.service.UserService
import java.util.*

object GitHubApi {

    private val client = GitHubClient()

    @JvmStatic
    var isLoginSuccess = false
        private set

    private const val API_ID = BuildConfig.GITHUB_API_ID
    private const val API_SECRET = BuildConfig.GITHUB_API_SECRET

    private const val AUTHORIZE_URL = "https://github.com/login/oauth/authorize?client_id=$API_ID&scope=public_repo"
    private const val ACCESS_TOKEN_API_URL = "https://github.com/login/oauth/access_token"
    private const val ACCESS_TOKEN_API_POST_FORM = "client_id=$API_ID&client_secret=$API_SECRET&code="

    private const val API_STATUS_URL = "https://raw.githubusercontent.com/RikkaApps/StorageRedirect-assets/master/.github.api.status"

    private val PRETTY_GSON = GsonBuilder().setPrettyPrinting().create()

    private fun <T : GitHubService> getService(clazz: Class<T>): T {
        return clazz.getConstructor(GitHubClient::class.java).newInstance(client)
    }

    private inline fun <reified T : GitHubService> getService(): T {
        return getService(T::class.java)
    }

    suspend fun checkLogin() = withContext(IO) {
        try {
            return@withContext getService<UserService>().user != null
        } catch (e: Exception) {
            return@withContext false
        }
    }

    suspend fun getUser(): User? = withContext(IO) {
        return@withContext getService<UserService>().user
    }

    suspend fun login(user: String, password: String) = withContext(IO) {
        client.setCredentials(user, password)
        return@withContext checkLogin()
    }

    suspend fun login(oauth2Token: String) = withContext(IO) {
        client.setOAuth2Token(oauth2Token)
        return@withContext checkLogin()
    }

    @JvmStatic
    fun loginWithoutVerifying(user: String, password: String) {
        client.setCredentials(user, password)
        isLoginSuccess = true
    }

    @JvmStatic
    fun loginWithoutVerifying(oauth2Token: String) {
        client.setOAuth2Token(oauth2Token)
        isLoginSuccess = true
    }

    @JvmStatic
    fun requestAuthorize(context: Context) {
        val url = Uri.parse(AUTHORIZE_URL)
        if (!CustomTabsHelper.launchUrl(context, url)) {
            WebViewActivity.start(context, url, "")
        }
    }

    suspend fun requestAccessToken(code: String) = withContext(IO) {
        val httpClient = OkHttpClient()
        val request = Request.Builder()
                .url(ACCESS_TOKEN_API_URL)
                .method("POST", RequestBody.create(
                        "application/x-www-form-urlencoded".toMediaTypeOrNull(),
                        ACCESS_TOKEN_API_POST_FORM + code
                ))
                .addHeader("Accept", "application/json")
                .build()
        val response = httpClient.newCall(request).execute()
        if (response.code == 200) {
            val body = response.body?.string()
            val result = PRETTY_GSON.fromJson<AccessTokenResult>(body!!)
            return@withContext result.accessToken
        }
        return@withContext null
    }

    suspend fun getRepository(user: String, repoName: String) = withContext(IO) {
        return@withContext getService<RepositoryService>()
                .getRepository(RepositoryId(user, repoName))
    }

    suspend fun getSRRulesRepository() = withContext(IO) {
        return@withContext getRepository(
                Settings.currentRulesRepoOwner,
                Settings.currentRulesRepoName
        )
    }

    suspend fun getIssues(repo: IRepositoryIdProvider) = withContext(IO) {
        return@withContext getService<IssueService>()
                .getIssues(repo, mapOf("filter" to "all")) ?: emptyList<Issue>()
    }

    suspend fun createIssue(repo: IRepositoryIdProvider, issue: Issue): Issue? = withContext(IO) {
        return@withContext getService<IssueService>()
                .createIssue(repo, issue)
    }

    suspend fun createIssueComment(
            repo: IRepositoryIdProvider,
            issueId: Int,
            comment: String
    ): Comment? = withContext(IO) {
        return@withContext getService<IssueService>()
                .createComment(repo, issueId, comment)
    }

    suspend fun buildRulesRequestIssue(
            context: Context,
            appInfo: AppInfo,
            appConfig: AppConfigurationBuilder
    ): Issue = withContext(Dispatchers.Default) {
        val packageName = appInfo.packageName
        val appTitle = appInfo.label

        val issue = Issue()
        issue.title = context.getString(R.string.app_rules_request_github_issue_title_format, packageName)

        var template = context.getString(R.string.app_rules_request_github_issue_body_format)
        fun applyTemp(key: String, value: String) {
            template = template.replace(key, value)
        }

        val jsonObject = JsonObject()

        // Application information
        applyTemp("{packageName}", packageName)
        jsonObject.addProperty("package", packageName)
        applyTemp("{displayName}", appTitle.toString())
        val (versionName, versionCode) = try {
            val packInfo = context.packageManager.getPackageInfo(packageName,
                    PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)
            packInfo.versionName to packInfo.versionCode.toString()
        } catch (e: Exception) {
            e.printStackTrace()
            "Unknown" to "Unknown"
        }
        applyTemp("{versionName}", versionName)
        applyTemp("{versionCode}", versionCode)

        // Rules
        applyTemp("{recommend}",
                if (appConfig.isRecommended == true) "yes" else "no")
        applyTemp("{verified}",
                if (appConfig.isVerified == true) "yes" else "no")
        jsonObject.addProperty("recommend", appConfig.isRecommended == true)
        jsonObject.addProperty("verified", appConfig.isVerified)
        applyTemp("{default_reason}", appConfig.defaultReason ?: "")

        if (!appConfig.defaultReason.isNullOrEmpty()) {
            jsonObject.add("description", JsonObject().apply {
                addProperty(Locale.getDefault().toString(), appConfig.defaultReason)
            })
        }

        // Author name
        jsonObject.add("authors", JsonArray().apply {
            add(getUser()?.login!!)
        })

        // Observer Info
        if (appConfig.observers.isNotEmpty()) {
            val observersString = StringBuilder()
            val observerTemplate = context.getString(
                    R.string.app_rules_request_github_issue_observer_format)
            val observersArray = JsonArray()
            for (observerInfo in appConfig.observers) {
                observersString.append(
                        observerTemplate
                                .replace("{source_path}", observerInfo.source)
                                .replace("{description}", observerInfo.summary)
                )
                val observerObj = JsonObject()
                with(observerObj) {
                    addProperty("source", observerInfo.source)
                    addProperty("target", observerInfo.target)
                    addProperty("description", observerInfo.summary)
                    addProperty("call_media_scan", observerInfo.callMediaScan)
                    addProperty("add_to_downloads", observerInfo.showNotification)
                    addProperty("allow_child", observerInfo.allowChild)
                    addProperty("allow_temp", observerInfo.allowTemp)
                }
                observersArray.add(observerObj)
            }
            applyTemp("{observers}", observersString.toString())
            jsonObject.add("observers", observersArray)
        } else {
            applyTemp("{observers}", context.getString(
                    R.string.app_rules_request_github_issue_observer_empty))
        }

        // More information
        applyTemp("{androidVersion}", Build.VERSION.RELEASE)
        applyTemp("{sdkVersion}", Build.VERSION.SDK_INT.toString())
        applyTemp("{locale}", Locale.getDefault().toString())

        // Get Storage Isolation version
        val (srVersionName, srVersionCode) = try {
            val packInfo = context.packageManager.getPackageInfo(
                    context.packageName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT
            )
            packInfo.versionName to packInfo.versionCode.toString()
        } catch (e: Exception) {
            e.printStackTrace()
            "Unknown" to "Unknown"
        }
        applyTemp("{rsAppVersionName}", srVersionName)
        applyTemp("{rsAppVersionCode}", srVersionCode)

        // JSON Output
        applyTemp("{jsonOutput}", PRETTY_GSON.toJson(jsonObject))

        issue.body = template

        return@withContext issue
    }

    suspend fun buildInvalidVerifiedAppIssue(
            context: Context,
            packageName: String,
            path: String?
    ) = withContext(Dispatchers.Default) {
        val issue = Issue()
        issue.title = context.getString(R.string.invalid_verified_app_issue_title_format, packageName)

        var template = context.getString(R.string.invalid_verified_app_issue_body_format)
        fun applyTemp(key: String, value: String) {
            template = template.replace(key, value)
        }

        applyTemp("{packageName}", packageName)
        val (versionName, versionCode) = try {
            val packInfo = context.packageManager
                    .getPackageInfo(packageName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)
            packInfo.versionName to packInfo.versionCode.toString()
        } catch (e: Exception) {
            e.printStackTrace()
            "Unknown" to "Unknown"
        }
        applyTemp("{versionName}", versionName)
        applyTemp("{versionCode}", versionCode)

        applyTemp("{path}", path ?: "null")

        issue.body = template

        return@withContext issue
    }

    suspend fun getGitHubApiStatus(): Boolean = withContext(IO) {
        val client = OkHttpClient()
        try {
            val response = Request.Builder()
                    .url(API_STATUS_URL)
                    .build()
                    .let(client::newCall)
                    .execute()
            if (response.isSuccessful && response.body != null) {
                val obj = PRETTY_GSON.fromJson<JsonObject>(response.body!!.string())
                return@withContext "ok".equals(obj.get("status").asString, ignoreCase = true)
            }
        } catch (ignored: Exception) {

        }
        return@withContext false
    }

    @JvmStatic
    fun logout() {
        client.setOAuth2Token(null)
        isLoginSuccess = false
    }

    private class AccessTokenResult(
            @SerializedName("access_token")
            var accessToken: String? = null
    )

}