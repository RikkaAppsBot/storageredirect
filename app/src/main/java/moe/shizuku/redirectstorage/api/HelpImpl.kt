package moe.shizuku.redirectstorage.api

import android.content.Context
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.ktx.DispatchersNoThrow
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import org.eclipse.egit.github.core.client.GsonUtils
import rikka.internal.dao.HelpEntitiesDao
import rikka.internal.help.HelpEntity
import rikka.internal.help.HelpProvider.Impl
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader

object HelpImpl : Impl {

    override fun getDao(context: Context): HelpEntitiesDao {
        return context.application.database.helpEntitiesDao
    }

    override fun loadAllAsync(context: Context) {
        GlobalScope.launch(DispatchersNoThrow.IO) {
            try {
                val response = context.application.rikkaAppApiService
                        .getHelpDocumentsAsync();
                if (response.isSuccessful && response.code() == 200) {
                    val result = response.body()
                    if (result != null) {
                        try {
                            getDao(context).update(context, result)
                        } catch (tr: Throwable) {
                        }
                    }
                }
            } catch (e: Exception) {
                CrashReportHelper.logException(e)
            }
        }
    }

    override fun loadLocal(context: Context): List<HelpEntity>? {
        try {
            val reader: Reader = BufferedReader(InputStreamReader(context.assets.open("help.json")))
            val builder = StringBuilder()
            val buffer = CharArray(8192)
            var read: Int
            while (reader.read(buffer, 0, buffer.size).also { read = it } > 0) {
                builder.append(buffer, 0, read)
            }
            return listOf(*GsonUtils.fromJson(builder.toString(), Array<HelpEntity>::class.java))
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
        }
        return null
    }
}