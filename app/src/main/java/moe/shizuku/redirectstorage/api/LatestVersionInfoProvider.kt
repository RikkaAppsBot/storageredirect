package moe.shizuku.redirectstorage.api

import android.content.Context
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.dao.LatestVersionInfoDao
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.model.LatestVersionInfo
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import org.eclipse.egit.github.core.client.GsonUtils
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader

object LatestVersionInfoProvider {

    private const val CACHE_MAX_ALIVE_TIME = 10 * 60 * 1000L

    private var local: LatestVersionInfo? = null

    private fun getDatabase(context: Context): LatestVersionInfoDao {
        return context.application.database.latestVersionInfoDao
    }

    suspend fun load(context: Context) {
        val time = System.currentTimeMillis() - LatestVersionInfoDao.getLastUpdateTime(context)
        if (time <= CACHE_MAX_ALIVE_TIME && !BuildConfig.DEBUG) {
            return
        }

        try {
            val response = context.application.rikkaAppApiService
                    .getUpdateVersionInfoAsync()
            if (response.isSuccessful && response.code() == 200) {
                val result = response.body()
                if (result != null) {
                    try {
                        getDatabase(context).update(context, result)
                    } catch (tr: Throwable) {
                    }
                }
            }
        } catch (e: Exception) {
            CrashReportHelper.logException(e)
        }
    }

    @JvmStatic
    fun get(context: Context): LatestVersionInfo {
        val res = try {
            getDatabase(context).get()
        } catch (tr: Throwable) {
            null
        }
        if (res != null) {
            return res
        }

        if (local == null) {
            local = loadLocal(context)
        }
        return local ?: LatestVersionInfo()
    }

    private fun loadLocal(context: Context): LatestVersionInfo? {
        try {
            val reader: Reader = BufferedReader(InputStreamReader(context.assets.open("update.json")))
            val builder = StringBuilder()
            val buffer = CharArray(8192)
            var read: Int
            while (reader.read(buffer, 0, buffer.size).also { read = it } > 0) {
                builder.append(buffer, 0, read)
            }
            return GsonUtils.fromJson(builder.toString(), LatestVersionInfo::class.java)
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
        }
        return null
    }
}
