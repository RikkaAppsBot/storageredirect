package moe.shizuku.redirectstorage.api

import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.model.AppConfiguration
import moe.shizuku.redirectstorage.model.VerifiedApp
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubRulesApiService {

    /*@GET("RikkaApps/StorageRedirect-assets/master/app_category/app_categories_description.json")
    fun getAppCategoriesDescription(): Single<Response<List<AppCategory.DescriptionItem>>>

    @GET("{repoOwner}/{repoName}/{repoBranch}/rules/apps/{packageName}.json")
    fun getConfiguration(
            @Path("repoOwner") repoOwner: String = SRSettings.getCurrentRulesRepoOwner(),
            @Path("repoName") repoName: String = SRSettings.getCurrentRulesRepoName(),
            @Path("repoBranch") repoBranch: String = SRSettings.getCurrentRepoBranch(),
            @Path("packageName") packageName: String
    ): Single<Response<AppConfiguration>>

    @GET("{repoOwner}/{repoName}/{repoBranch}/rules/verified_apps.json")
    fun getVerifiedApps(
            @Path("repoOwner") repoOwner: String,
            @Path("repoName") repoName: String,
            @Path("repoBranch") repoBranch: String
    ): Single<Response<List<VerifiedApp>>>*/

    // Kotlin Coroutines Style

    @GET("{repoOwner}/{repoName}/{repoBranch}/app_rule/apps/{packageName}.json")
    suspend fun getConfigurationAsync(
            @Path("repoOwner") repoOwner: String = Settings.currentRulesRepoOwner,
            @Path("repoName") repoName: String = Settings.currentRulesRepoName,
            @Path("repoBranch") repoBranch: String = Settings.currentRepoBranch,
            @Path("packageName") packageName: String
    ): Response<AppConfiguration>

    @GET("{repoOwner}/{repoName}/{repoBranch}/app_rule/verified_apps.json")
    suspend fun getVerifiedAppsSync(
            @Path("repoOwner") repoOwner: String = Settings.currentRulesRepoOwner,
            @Path("repoName") repoName: String = Settings.currentRulesRepoName,
            @Path("repoBranch") repoBranch: String = Settings.currentRepoBranch
    ): Response<List<VerifiedApp>>

}
