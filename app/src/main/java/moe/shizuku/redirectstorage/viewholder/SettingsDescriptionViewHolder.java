package moe.shizuku.redirectstorage.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;

public class SettingsDescriptionViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) ->
            new SettingsDescriptionViewHolder(
                    inflater.inflate(R.layout.settings_description_view, parent, false)
            );

    public static final Creator<Object> OUTLINE_STYLE_CREATOR = (inflater, parent) ->
            new SettingsDescriptionViewHolder(
                    inflater.inflate(R.layout.settings_description_view_outline, parent, false)
            );

    public static final Creator<Object> NO_ICON_STYLE_CREATOR = (inflater, parent) ->
            new SettingsDescriptionViewHolder(
                    inflater.inflate(R.layout.settings_description_view_no_icon, parent, false)
            );

    public static final Creator<Object> DIALOG_STYLE_CREATOR = (inflater, parent) -> {
        final View rootView = inflater.inflate(R.layout.settings_description_view, parent, false);
        ((ViewGroup.MarginLayoutParams) rootView.findViewById(android.R.id.icon).getLayoutParams())
                .setMarginStart(parent.getResources().getDimensionPixelSize(R.dimen.padding_8dp));
        ((ViewGroup.MarginLayoutParams) rootView.findViewById(android.R.id.text1).getLayoutParams())
                .setMarginStart(parent.getResources().getDimensionPixelSize(R.dimen.padding_8dp) / 2 * 3);
        ((ViewGroup.MarginLayoutParams) rootView.findViewById(android.R.id.text1).getLayoutParams())
                .setMarginEnd(parent.getResources().getDimensionPixelSize(R.dimen.padding_8dp));
        return new SettingsDescriptionViewHolder(rootView);
    };

    private final TextView mTextView;

    public SettingsDescriptionViewHolder(View itemView) {
        super(itemView);

        mTextView = itemView.findViewById(android.R.id.text1);
    }

    @Override
    public void onBind() {
        if (getData() instanceof CharSequence) {
            mTextView.setText((CharSequence) getData());
        } else if (getData() instanceof Integer) {
            mTextView.setText((int) getData());
        }
    }

}
