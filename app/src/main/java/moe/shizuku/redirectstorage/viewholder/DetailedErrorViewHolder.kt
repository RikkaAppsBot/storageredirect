package moe.shizuku.redirectstorage.viewholder

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import moe.shizuku.redirectstorage.R
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class DetailedErrorViewHolder(itemView: View) : BaseViewHolder<DetailedErrorViewHolder.Data>(itemView) {

    companion object {
        val LIST_ITEM_STYLE_CREATOR = Creator<Data> { inflater, parent -> DetailedErrorViewHolder(inflater.inflate(R.layout.item_error_detailed, parent, false)) }
    }

    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val message: TextView = itemView.findViewById(android.R.id.message)

    override fun onBind() {
        icon.setImageDrawable(data.icon)

        if (data.title != null) {
            title.text = data.title
            title.isVisible = true
        } else {
            title.isVisible = false
        }

        val sb = StringBuilder()
        if (data.message != null) {
            sb.append(data.message)
        }
        if (data.throwable?.message != null) {
            sb.append("<p>").append(data.throwable!!.message)
        }
        val messageText = sb.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)

        if (messageText.isNotBlank()) {
            message.text = messageText
            message.isVisible = true
        } else {
            message.isVisible = false
        }
    }

    data class Data(val throwable: Throwable?, val icon: Drawable?, val title: CharSequence?, val message: CharSequence?)
}
