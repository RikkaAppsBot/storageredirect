package moe.shizuku.redirectstorage.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;


public class ErrorViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> LIST_ITEM_STYLE_CREATOR = (inflater, parent) -> new ErrorViewHolder(inflater.inflate(R.layout.item_error, parent, false));

    public ErrorViewHolder(View itemView) {
        super(itemView);
    }
}