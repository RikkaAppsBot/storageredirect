package moe.shizuku.redirectstorage.viewholder;

import android.view.View;

public abstract class SimpleItemViewHolder<T> extends SimpleItemListenerViewHolder<T, Object> {

    public SimpleItemViewHolder(View itemView) {
        super(itemView);
    }
}