package moe.shizuku.redirectstorage.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import rikka.recyclerview.BaseListenerViewHolder;

public abstract class SimpleItemListenerViewHolder<T, L> extends BaseListenerViewHolder<T, L> implements View.OnClickListener {

    public ImageView icon;
    public TextView title;
    public TextView summary;

    public SimpleItemListenerViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);

        itemView.setOnClickListener(this);
    }

    @Override
    public abstract void onClick(final View view);
}
