package moe.shizuku.redirectstorage.viewholder;

import android.view.View;
import android.widget.Checkable;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import java.util.List;

public abstract class CheckableViewHolder<T> extends SimpleItemViewHolder<T> {

    protected Checkable checkable;

    public CheckableViewHolder(View itemView) {
        super(itemView);

        checkable = itemView.findViewById(android.R.id.button1);
    }

    @Override
    public abstract void onClick(final View view);

    public abstract boolean isChecked();

    @CallSuper
    @Override
    public void onBind() {
        syncEnabledState();
    }

    @CallSuper
    @Override
    public void onBind(@NonNull List<Object> payloads) {
        syncEnabledState();
    }

    private void syncEnabledState() {
        checkable.setChecked(isChecked());
    }
}
