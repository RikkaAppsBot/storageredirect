package moe.shizuku.redirectstorage.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;

public abstract class SimpleTwoButtonViewHolder<T> extends BaseViewHolder<T> {

    public ImageView icon;
    public TextView title;
    public TextView summary;
    public View button1;
    public View button2;
    public View divider;

    public SimpleTwoButtonViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);
        button1 = itemView.findViewById(android.R.id.button1);
        button2 = itemView.findViewById(android.R.id.button2);
        divider = itemView.findViewById(R.id.divider);

        button1.setOnClickListener(this::onPrimaryButtonClick);
        button2.setOnClickListener(this::onSecondaryButtonClick);
    }

    public abstract void onPrimaryButtonClick(final View view);

    public abstract void onSecondaryButtonClick(final View view);
}
