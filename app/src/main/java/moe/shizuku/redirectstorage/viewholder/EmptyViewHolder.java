package moe.shizuku.redirectstorage.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;


public class EmptyViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) -> new EmptyViewHolder(inflater.inflate(R.layout.item_empty, parent, false));

    public EmptyViewHolder(View itemView) {
        super(itemView);
    }
}
