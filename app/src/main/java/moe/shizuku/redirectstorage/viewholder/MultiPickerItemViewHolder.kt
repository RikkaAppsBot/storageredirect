package moe.shizuku.redirectstorage.viewholder

import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.model.SelectableServerFile
import moe.shizuku.redirectstorage.utils.AppIconCache
import moe.shizuku.redirectstorage.utils.FileBrowserUtils
import moe.shizuku.redirectstorage.utils.ViewUtils
import moe.shizuku.redirectstorage.widget.IndeterminateCheckBox
import rikka.core.res.resolveColor
import rikka.recyclerview.BaseViewHolder.Creator
import java.io.FileInputStream
import kotlin.math.max

class MultiPickerItemViewHolder(itemView: View) : BaseCoroutinesViewHolder<SelectableServerFile, MultiPickerItemViewHolder.Listener>(itemView) {

    companion object {

        @JvmField
        val CREATOR = Creator<SelectableServerFile> { inflater, parent ->
            MultiPickerItemViewHolder(
                    inflater.inflate(R.layout.multi_picker_file_item, parent, false)
            )
        }

    }

    interface Listener {

        fun onItemCheckBoxClick(item: SelectableServerFile)

        fun onItemClick(item: SelectableServerFile)
    }

    private val widgetFrame: View = itemView.findViewById(android.R.id.widget_frame)
    private val checkBox: IndeterminateCheckBox = itemView.findViewById(android.R.id.button1)
    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val imagePreview: ImageView = itemView.findViewById(R.id.image_preview)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val info: TextView = itemView.findViewById(android.R.id.text1)

    private val widgetFrameSpace: View = itemView.findViewById(R.id.widget_frame_space)
    private val contentView: View = itemView.findViewById(android.R.id.content)
    private val rightArrow: View = itemView.findViewById(R.id.right_arrow_view)
    private val divider: View = itemView.findViewById(R.id.two_target_divider)

    private val previewSize = itemView.resources.getDimensionPixelSize(R.dimen.file_manager_preview_size)

    init {
        itemView.findViewById<View>(android.R.id.widget_frame).setOnClickListener {
            if (data?.selectable == false || data?.isBackItem == true) {
                return@setOnClickListener
            }
            listener.onItemCheckBoxClick(data)
        }
        contentView.setOnClickListener {
            listener.onItemClick(data)
        }
    }

    override fun onBind() {
        if (data.isBackItem) {
            icon.setImageResource(R.drawable.ic_folder_open_24dp)
        } else {
            val iconRes = FileBrowserUtils.getFileIconResources(data.serverFile)
            icon.setImageResource(iconRes)
            if (iconRes == R.drawable.ic_photo_24dp) {
                launch {
                    try {
                        val res = withContext(AppIconCache.dispatcher()) {
                            val fm = SRManager.createThrow().fileManager
                            var fileDescriptor = fm.openFileDescriptor(data.serverFile, "r")
                            lateinit var inOpt: BitmapFactory.Options
                            fileDescriptor.use {
                                val input = FileInputStream(fileDescriptor.fileDescriptor)
                                val outOpt = BitmapFactory.Options().also {
                                    it.inJustDecodeBounds = true
                                }
                                BitmapFactory.decodeStream(input, null, outOpt)
                                val sampleSize = max(
                                        1,
                                        max(outOpt.outHeight, outOpt.outWidth) / previewSize
                                )
                                inOpt = BitmapFactory.Options().also {
                                    it.inSampleSize = sampleSize
                                }
                            }

                            fileDescriptor = fm.openFileDescriptor(data.serverFile, "r")
                            fileDescriptor.use {
                                return@withContext BitmapFactory.decodeStream(
                                        FileInputStream(fileDescriptor.fileDescriptor),
                                        null, inOpt
                                )
                            }
                        }

                        imagePreview.setImageBitmap(res)
                    } catch (e: CancellationException) {
                        // do nothing if canceled
                        return@launch
                    } catch (e: Exception) {
                        e.printStackTrace()
                        return@launch
                    }
                }
            }
        }

        title.text = if (data.isBackItem) "…" else data.name

        widgetFrame.visibility = if (data.isBackItem) View.GONE else View.VISIBLE
        widgetFrameSpace.visibility = if (data.isBackItem) View.VISIBLE else View.GONE

        updateCheckedState()
    }

    override fun onBind(payloads: MutableList<Any>) {
        updateCheckedState()
    }

    private fun updateCheckedState() {
        if (!data.neverSelectable) {
            checkBox.state = if (data.selectedPartially) null else data.selected
            checkBox.isEnabled = data.selectable
            checkBox.visibility = View.VISIBLE

            contentView.isEnabled = data.allowEnter
            rightArrow.visibility =
                    if (data.isDirectory && data.allowEnter && !data.isBackItem)
                        View.VISIBLE else View.GONE

            divider.visibility = View.VISIBLE

            icon.isEnabled = checkBox.isEnabled
            title.isEnabled = checkBox.isEnabled
            info.isEnabled = checkBox.isEnabled
        } else {
            checkBox.visibility = View.GONE

            contentView.isEnabled = false
            rightArrow.visibility = View.GONE
            divider.visibility = View.GONE

            icon.isEnabled = false
            title.isEnabled = false
            info.isEnabled = false
        }

        if (data.errorReason != null) {
            val colorWarning = context.theme.resolveColor(R.attr.colorWarning)
            ViewUtils.setCompoundDrawableStartWithIntrinsicBounds(info,
                    context.getDrawable(R.drawable.ic_warning_16dp)!!.mutate().apply {
                        setTint(colorWarning)
                    })
            info.setTextColor(colorWarning)

            info.text = data.errorReason
        } else {
            ViewUtils.setCompoundDrawableStartWithIntrinsicBounds(info, null)
            info.setTextColor(context.theme.resolveColor(android.R.attr.textColorSecondary))

            if (data.normalReason != null) {
                info.text = data.normalReason
            } else {
                if (data.selected) {
                    info.setText(R.string.multi_picker_item_description_selected_all)
                } else {
                    info.text = when {
                        data.selectedSubfiles != 0 && data.selectedSubfolders != 0 ->
                            context.getString(R.string.multi_picker_item_description_selected_subfolder_and_subfiles,
                                    data.selectedSubfolders, data.selectedSubfiles)
                        data.selectedSubfolders != 0 ->
                            context.getString(R.string.multi_picker_item_description_selected_subfolder,
                                    data.selectedSubfolders)
                        data.selectedSubfiles != 0 ->
                            context.getString(R.string.multi_picker_item_description_selected_subfiles,
                                    data.selectedSubfiles)
                        else -> ""
                    }
                }
            }
        }

        info.visibility = if (info.text.isEmpty()) View.GONE else View.VISIBLE
    }

    override fun onRecycle() {
        super.onRecycle()
        imagePreview.setImageBitmap(null)
    }
}