package moe.shizuku.redirectstorage.viewholder;

import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import java.util.List;

import rikka.recyclerview.BaseViewHolder;

public abstract class MasterCheckableViewHolder<T> extends BaseViewHolder<T> implements View.OnClickListener {

    private Checkable checkable;
    protected ImageView icon;
    protected TextView title;
    protected TextView summary;

    public MasterCheckableViewHolder(View itemView) {
        super(itemView);

        checkable = itemView.findViewById(android.R.id.button1);
        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);

        itemView.findViewById(android.R.id.widget_frame).setOnClickListener(this::onSwitchClick);
        itemView.findViewById(android.R.id.content).setOnClickListener(this);
    }

    @Override
    public abstract void onClick(final View view);

    public abstract void onSwitchClick(final View view);

    public abstract boolean isChecked();

    @CallSuper
    @Override
    public void onBind() {
        syncEnabledState();
    }

    @CallSuper
    @Override
    public void onBind(@NonNull List<Object> payloads) {
        syncEnabledState();
    }

    private void syncEnabledState() {
        checkable.setChecked(isChecked());
    }
}
