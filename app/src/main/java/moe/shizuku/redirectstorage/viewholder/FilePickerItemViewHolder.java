package moe.shizuku.redirectstorage.viewholder;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;
import rikka.core.util.ResourceUtils;
import rikka.recyclerview.BaseViewHolder;

/**
 * Created by fytho on 2017/12/12.
 */

public class FilePickerItemViewHolder extends BaseViewHolder<ServerFile> {

    public static ItemCreator newCreator(OnItemClickListener listener) {
        return new ItemCreator(false, listener);
    }

    public static ItemCreator newCompactCreator(OnItemClickListener listener) {
        return new ItemCreator(true, listener);
    }

    private ImageView icon;
    private TextView title;

    public FilePickerItemViewHolder(View itemView, final OnItemClickListener listener) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);

        itemView.setOnClickListener(view -> listener.onItemClick(getName(), isFile()));
    }

    @Override
    public void onBind() {
        super.onBind();

        if (getName() != null) {
            title.setText(getName());
        } else {
            title.setText("...");
        }
        Resources.Theme theme = icon.getContext().getTheme();
        itemView.setEnabled(!isFile());
        title.setTextColor(ResourceUtils.resolveColor(theme, isFile() ? android.R.attr.textColorSecondaryNoDisable : android.R.attr.textColorPrimary));
        icon.setImageTintList(ColorStateList.valueOf(ResourceUtils.resolveColor(theme, isFile() ? android.R.attr.textColorSecondaryNoDisable : android.R.attr.colorAccent)));
        icon.setImageResource(FileBrowserUtils.getFileIconResources(getData()));
    }

    @Nullable
    public String getName() {
        return getData().getName();
    }

    public boolean isFile() {
        return getName() != null && !getData().isDirectory();
    }

    public interface OnItemClickListener {

        void onItemClick(@Nullable String name, boolean isFile);

    }

    static class ItemCreator implements Creator<ServerFile> {

        private final boolean isCompactLayout;
        private final OnItemClickListener listener;

        ItemCreator(boolean isCompactLayout, OnItemClickListener listener) {
            this.isCompactLayout = isCompactLayout;
            this.listener = listener;
        }

        private int getLayoutResource() {
            return isCompactLayout ? R.layout.file_picker_item_compact : R.layout.file_picker_item;
        }

        @Override
        public BaseViewHolder<ServerFile> createViewHolder(LayoutInflater inflater, ViewGroup parent) {
            final View rootView = inflater.inflate(getLayoutResource(), parent, false);
            return new FilePickerItemViewHolder(rootView, listener);
        }
    }

}
