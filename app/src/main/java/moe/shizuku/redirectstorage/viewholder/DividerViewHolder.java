package moe.shizuku.redirectstorage.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;


public class DividerViewHolder extends BaseViewHolder<Object> {

    static class ItemCreator implements Creator<Object> {
        int resId;

        ItemCreator(@LayoutRes int resId) {
            this.resId = resId;
        }

        @Override
        public BaseViewHolder<Object> createViewHolder(LayoutInflater inflater, ViewGroup parent) {
            return new DividerViewHolder(inflater.inflate(resId, parent, false));
        }
    }

    public static Creator<Object> newCreator(@LayoutRes int resId) {
        return new ItemCreator(resId);
    }

    public static final Creator<Object> CREATOR = newCreator(R.layout.item_divider);

    public static final Creator<Object> CREATOR2 = newCreator(R.layout.item_divider2);

    public DividerViewHolder(View itemView) {
        super(itemView);
    }
}
