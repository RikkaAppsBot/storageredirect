package moe.shizuku.redirectstorage.viewholder

import android.view.View
import androidx.annotation.CallSuper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import rikka.recyclerview.BaseListenerViewHolder
import kotlin.coroutines.CoroutineContext

open class BaseCoroutinesViewHolder<T, L>(itemView: View): BaseListenerViewHolder<T, L>(itemView), CoroutineScope {

    override val coroutineContext: CoroutineContext get() = SupervisorJob() + Dispatchers.Main

    @CallSuper
    override fun onRecycle() {
        super.onRecycle()
        coroutineContext.cancelChildren()
    }
}