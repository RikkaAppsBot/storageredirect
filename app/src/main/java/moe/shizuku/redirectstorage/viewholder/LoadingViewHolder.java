package moe.shizuku.redirectstorage.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;


public class LoadingViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> FULLSCREEN_STYLE_CREATOR = (inflater, parent) -> new LoadingViewHolder(inflater.inflate(R.layout.item_loading_fullscreen, parent, false));

    public static final Creator<Object> LIST_ITEM_STYLE_CREATOR = (inflater, parent) -> new LoadingViewHolder(inflater.inflate(R.layout.item_loading, parent, false));

    public LoadingViewHolder(View itemView) {
        super(itemView);
    }
}