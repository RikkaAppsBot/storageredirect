package moe.shizuku.redirectstorage.model

interface IPackageDescriptor {

    val packageName: String

    val userId: Int

    val sharedUserId: String?
}