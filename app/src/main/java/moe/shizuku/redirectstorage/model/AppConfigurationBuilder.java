package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Locale;

import moe.shizuku.redirectstorage.ObserverInfo;

import static java.util.Objects.requireNonNull;
import static moe.shizuku.redirectstorage.utils.ParcelUtils.booleanFromInt;
import static moe.shizuku.redirectstorage.utils.ParcelUtils.booleanToInt;

public class AppConfigurationBuilder implements Parcelable {

    public static final Creator<AppConfigurationBuilder> CREATOR = new Creator<AppConfigurationBuilder>() {

        @Override
        public AppConfigurationBuilder createFromParcel(Parcel in) {
            return new AppConfigurationBuilder(in);
        }

        @Override
        public AppConfigurationBuilder[] newArray(int size) {
            return new AppConfigurationBuilder[size];
        }

    };
    @NonNull
    private AppConfiguration configuration;
    @Nullable
    private Boolean recommended, verified;

    public AppConfigurationBuilder(@NonNull AppInfo appInfo) {
        this(appInfo.getPackageName());
    }

    public AppConfigurationBuilder(@NonNull AppConfiguration appConfiguration) {
        configuration = appConfiguration;

        /*recommended = configuration.recommended;
        verified = configuration.verified;*/
    }

    public AppConfigurationBuilder(@NonNull String packageName) {
        configuration = new AppConfiguration();
        /*configuration.packageName = packageName;
        configuration.authors = new ArrayList<>();
        configuration.observers = new ArrayList<>();
        configuration.description = new MultiLocaleEntity();
        configuration.appCategory = null;*/

        recommended = null;
        verified = null;
    }

    protected AppConfigurationBuilder(Parcel in) {
        configuration = requireNonNull(in.readParcelable(AppConfiguration.class.getClassLoader()));

        recommended = booleanFromInt(in.readInt());
        verified = booleanFromInt(in.readInt());
    }

    public AppConfigurationBuilder setRecommended(@Nullable Boolean recommended) {
        this.recommended = recommended;
        return this;
    }

    @Nullable
    public Boolean isRecommended() {
        return recommended;
    }

    public AppConfigurationBuilder setVerified(@Nullable Boolean verified) {
        this.verified = verified;
        return this;
    }

    @Nullable
    public Boolean isVerified() {
        return verified;
    }

    public boolean shouldSetBooleanValues() {
        return verified == null || recommended == null;
    }

    public AppConfigurationBuilder addObserver(@NonNull ObserverInfo observerInfo) {
        //configuration.observers.add(observerInfo);
        return this;
    }

    public AppConfigurationBuilder removeObserver(int position) {
        //configuration.observers.remove(position);
        return this;
    }

    public AppConfigurationBuilder removeObserver(@NonNull ObserverInfo object) {
        //configuration.observers.remove(object);
        return this;
    }

    @NonNull
    public List<ObserverInfo> getObservers() {
        return null;
        //return configuration.observers;
    }

    public AppConfigurationBuilder setObservers(@NonNull List<ObserverInfo> observers) {
        //configuration.observers = requireNonNull(observers);
        return this;
    }

    public AppConfigurationBuilder setReason(@NonNull String lang, @Nullable String reason) {
        /*if (reason == null) {
            configuration.description.remove(lang);
        } else {
            configuration.description.put(lang, reason);
        }*/
        return this;
    }

    @Nullable
    public String getReason(@NonNull String lang) {
        /*if (configuration.description == null) {
            return null;
        }
        return configuration.description.get(lang);*/
        return null;
    }

    @Nullable
    public List<String> getRecommendedMountDirs() {
        return null/*configuration.recommendedMountDirs*/;
    }

    public AppConfigurationBuilder setRecommendedMountDirs(@Nullable List<String> recommendedMountDirs) {
        //configuration.recommendedMountDirs = recommendedMountDirs;
        return this;
    }

    @Nullable
    public String getRecommendedRedirectTarget() {
        return null/*configuration.recommendedRedirectTarget*/;
    }

    public AppConfigurationBuilder setRecommendedRedirectTarget(@Nullable String recommendedRedirectTarget) {
        //configuration.recommendedRedirectTarget = recommendedRedirectTarget;
        return this;
    }

    @Nullable
    public String getDefaultReason() {
        return getReason(Locale.getDefault().toString());
    }

    public AppConfigurationBuilder setDefaultReason(@Nullable String defaultReason) {
        return setReason(Locale.getDefault().toString(), defaultReason);
    }

    @Nullable
    public String getAppCategory() {
        return null/*configuration.appCategory*/;
    }

    public AppConfigurationBuilder setAppCategory(@Nullable String appCategory) {
        //configuration.appCategory = appCategory;
        return this;
    }

    @NonNull
    public AppConfiguration build() {
        if (recommended == null || verified == null) {
            throw new IllegalArgumentException("Did you forget set all boolean values?");
        }

        /*configuration.recommended = recommended;
        configuration.verified = verified;*/
        return configuration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //dest.writeParcelable(configuration, flags);
        dest.writeInt(booleanToInt(recommended));
        dest.writeInt(booleanToInt(verified));
    }

}
