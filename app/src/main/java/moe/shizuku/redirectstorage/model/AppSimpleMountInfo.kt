package moe.shizuku.redirectstorage.model

import android.content.pm.ApplicationInfo
import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import com.google.gson.annotations.SerializedName
import moe.shizuku.redirectstorage.SimpleMountInfo
import rikka.internal.model.MultiLocaleEntity

class AppSimpleMountInfo {

    // online fields
    lateinit var onlineId: String

    @SerializedName("source_package")
    val sourcePackage: String

    @SerializedName("target_package")
    var targetPackage: String?

    @SerializedName("paths")
    var paths: List<String> = ArrayList()

    @SerializedName("description")
    var description: MultiLocaleEntity? = null

    // local only fields
    var id: String
    var userId = 0
    var enabled = false

    // temp fields
    @Transient
    var sourceLabel: CharSequence? = null

    @Transient
    var targetLabel: CharSequence? = null

    @Transient
    var sourceInfo: ApplicationInfo? = null

    @Transient
    var targetInfo: ApplicationInfo? = null

    @Transient
    lateinit var thisPackage: String

    @Transient
    var local = false

    @Transient
    var online = false

    @Transient
    var icon = false

    internal constructor(sourcePackage: String, targetPackage: String, paths: List<String>, id: String, userId: Int, enabled: Boolean) {
        this.sourcePackage = sourcePackage
        this.targetPackage = targetPackage
        this.paths = paths
        this.id = id
        this.userId = userId
        this.enabled = enabled
        this.local = true
    }

    fun loadDescription(): String? {
        return description?.get()
    }

    fun toSimpleMountInfo(): SimpleMountInfo {
        return SimpleMountInfo(userId, sourcePackage, targetPackage, paths).also {
            it.id = this.id
            it.enabled = this.enabled
        }
    }

    fun equalsOnlineFields(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as AppSimpleMountInfo
        if (sourcePackage != that.sourcePackage &&
                targetPackage != that.targetPackage) return false
        return run {
            if (paths.size != that.paths.size) {
                return false
            }
            for (a in paths) {
                var contains = false
                for (b in that.paths) {
                    if (a.equals(b, ignoreCase = true)) {
                        contains = true
                        break
                    }
                }
                if (!contains) {
                    return false
                }
            }
            true
        }
    }
}

fun SimpleMountInfo.toApp(): AppSimpleMountInfo {
    return AppSimpleMountInfo(sourcePackage, targetPackage, paths, id, userId, enabled)
}

fun SimpleMountInfo.newBuilder(): SimpleMountInfoBuilder {
    return SimpleMountInfoBuilder()
            .source(sourcePackage)
            .target(targetPackage)
            .paths(paths)
            .user(userId)
}

class SimpleMountInfoBuilder : Parcelable {

    var sourceName: String? = null
        private set
    var targetName: String? = null
        private set

    var paths: MutableList<String> = ArrayList()

    var userId: Int = 0

    var isChanged = false
        private set

    constructor()

    private constructor(`in`: Parcel) {
        sourceName = `in`.readString()
        targetName = `in`.readString()
        userId = `in`.readInt()
        `in`.createStringArrayList()?.let { paths = it }
        isChanged = `in`.readByte().toInt() != 0
    }

    fun source(sourcePackage: String) = apply {
        if (this.sourceName != sourcePackage) {
            isChanged = true
        }
        this.sourceName = sourcePackage
        return this
    }

    fun target(targetPackage: String) = apply {
        if (this.targetName != targetPackage) {
            isChanged = true
        }
        this.targetName = targetPackage
        return this
    }

    fun user(userId: Int) = apply {
        if (this.userId != userId) {
            isChanged = true
        }
        this.userId = userId
    }

    fun paths(paths: MutableList<String>) = apply {
        if (!this.paths.containsAll(paths) && paths.containsAll(this.paths)) {
            isChanged = true
        }
        this.paths = paths
        return this
    }

    fun build(): SimpleMountInfo {
        return SimpleMountInfo(userId, sourceName!!, targetName!!, paths)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(out: Parcel, i: Int) {
        out.writeString(sourceName)
        out.writeString(targetName)
        out.writeInt(userId)
        out.writeStringList(paths)
        out.writeByte((if (isChanged) 1 else 0).toByte())
    }

    companion object {

        @JvmField
        val CREATOR: Creator<SimpleMountInfoBuilder> = object : Creator<SimpleMountInfoBuilder> {
            override fun createFromParcel(`in`: Parcel): SimpleMountInfoBuilder {
                return SimpleMountInfoBuilder(`in`)
            }

            override fun newArray(size: Int): Array<SimpleMountInfoBuilder?> {
                return arrayOfNulls(size)
            }
        }
    }
}
