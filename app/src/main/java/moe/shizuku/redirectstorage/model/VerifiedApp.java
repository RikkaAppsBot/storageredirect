package moe.shizuku.redirectstorage.model;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Keep
@Entity(tableName = "verified_apps")
public class VerifiedApp {

    @SuppressWarnings("NullableProblems")
    @PrimaryKey
    @SerializedName(value = "package", alternate = "package_name")
    @ColumnInfo(name = "package_name")
    public @NonNull
    String packageName;
}
