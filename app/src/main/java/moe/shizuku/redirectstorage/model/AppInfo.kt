package moe.shizuku.redirectstorage.model

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Environment
import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import android.text.TextUtils
import android.widget.TextView
import kotlinx.coroutines.*
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.model.AppCategory.rawIntToStringType
import moe.shizuku.redirectstorage.utils.*
import rikka.material.app.LocaleDelegate
import kotlin.coroutines.CoroutineContext

class AppInfo : Cloneable, Parcelable, IPackageDescriptor, DiffUtilComparable, CoroutineScope {

    override val coroutineContext: CoroutineContext get() = Dispatchers.Main

    val redirectInfo: RedirectPackageInfo
    override val packageName: String get() = redirectInfo.packageName
    override val userId: Int get() = redirectInfo.userId

    override val sharedUserId get() = redirectInfo.packageInfo?.sharedUserId
    val sharedUserLabel get() = SharedUserLabelCache.get(sharedUserId)

    val packageInfo: PackageInfo get() = redirectInfo.packageInfo
    val applicationInfo: ApplicationInfo? get() = packageInfo.applicationInfo

    val label: CharSequence?

    val isSystemApp get() = redirectInfo.systemApp

    val isPackageEnabled get() = applicationInfo?.enabled ?: true

    var isRedirectEnabled
        get() = redirectInfo.enabled
        set(value) {
            this.redirectInfo.enabled = value
        }

    var configuration: AppConfiguration? = null

    /**
     * Don't store this key in StorageRedirect-assets.
     * To provide common data for all RikkaApps, read this key from "rikka.app/app_category/apps"
     */
    var categorySuggestion: String? = null

    private var fileCount = -1
    private var fileCountUpdate = System.currentTimeMillis()
    private var isVerified: Boolean? = null
    private var existsNonstandardBehavior: Boolean? = null
    private var existsNonstandardBehaviorNextCheck = System.currentTimeMillis()

    val appId get() = (applicationInfo?.uid ?: 0) % 100000

    val isRegularApp get() = appId in 10000..19999

    val writePermission get() = redirectInfo.writePermission()

    val writePermissionOrSharedUserId get() = redirectInfo.writePermission() || sharedUserId != null

    val categoryType: String?
        get() {
            var category: String? = null
            if (configuration != null) {
                category = configuration!!.categoryType
            }
            if (category == null) {
                if (categorySuggestion != null) {
                    category = categorySuggestion
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    category = rawIntToStringType(applicationInfo!!.category)
                }
            }
            return category
        }

    constructor(redirectInfo: RedirectPackageInfo, pm: PackageManager) {
        this.redirectInfo = redirectInfo
        label = loadLabel(pm)
    }

    constructor(other: AppInfo) {
        redirectInfo = RedirectPackageInfo(other.redirectInfo)
        label = other.label
        configuration = other.configuration
        categorySuggestion = other.categorySuggestion
        fileCount = other.fileCount
        fileCountUpdate = other.fileCountUpdate
        isVerified = other.isVerified
        existsNonstandardBehavior = other.existsNonstandardBehavior
        existsNonstandardBehaviorNextCheck = other.existsNonstandardBehaviorNextCheck
    }

    fun loadLabel(pm: PackageManager): CharSequence? {
        var resolvedTitle: CharSequence? = null
        /*try {
            resolvedTitle = applicationInfo?.loadLabel(pm)
        } catch (ignored: Exception) {
        }*/
        try {
            val appRes = pm.getResourcesForApplication(packageName)
            val config = Configuration()
            config.setLocale(LocaleDelegate.defaultLocale)
            appRes.updateConfiguration(config, appRes.displayMetrics)
            val labelRes = pm.getApplicationInfo(
                    packageName, PackageManager.GET_META_DATA).labelRes
            if (labelRes != 0) {
                resolvedTitle = appRes.getText(labelRes)
            }
        } catch (ignored: Exception) {
        }
        try {
            if (resolvedTitle == null) {
                resolvedTitle = redirectInfo.packageInfo.applicationInfo.loadLabel(pm)
            }
        } catch (ignored: Exception) {
        }
        if (resolvedTitle.isNullOrEmpty()) {
            resolvedTitle = packageName
        }
        return resolvedTitle
    }

    fun loadIsVerified(context: Context?) {
        isVerified = try {
            context!!.application.database
                    .verifiedAppsDao.find(packageName) != null
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            false
        }
    }

    fun isAOSP(): Boolean {
        return isSystemApp
                && (packageName.startsWith("com.android.", ignoreCase = false) || packageName == "android")
                && packageName != "com.android.theme"
                && packageName != "com.android.chrome"
                && packageName != "com.android.vending"
    }

    fun isVerified(excludeAOSP: Boolean = true): Boolean {
        return if (excludeAOSP) {
            isVerified == true && !isAOSP()
        } else {
            isVerified == true
        }
    }

    fun isVerifiedAOSP(): Boolean {
        return isVerified(false)
    }

    private fun loadFileCount() {
        if (fileCount != -1
                && System.currentTimeMillis() < fileCountUpdate) {
            return
        }
        fileCountUpdate = System.currentTimeMillis() + 1000 * 60
        try {
            fileCount = SRManager.createThrow()
                    .getNonStandardFilesCount(packageName, userId)
        } catch (ignored: Throwable) {
        }
        return
    }

    private fun loadExistsNonstandardBehavior() {
        if (existsNonstandardBehavior != null
                && System.currentTimeMillis() < existsNonstandardBehaviorNextCheck) {
            return
        }

        val srm: SRManager
        try {
            srm = SRManager.createThrow()

            // if not using enhance module, return
            if (srm.moduleStatus.versionCode <= 12) {
                existsNonstandardBehavior = false
                existsNonstandardBehaviorNextCheck = Long.MAX_VALUE
                return
            }

            /* TODO:
               Currently, we load if exists non-standard behavior from io history.
               If possible and fast enough, load by scanning redirected storage. */
            val selection = "user=? AND package=? AND path NOT like ?" +
                    " AND path != '/storage'" +
                    " AND path != '/storage/emulated'" +
                    " AND path != '/storage/emulated/" + userId + "'" +
                    " AND path != '/storage/emulated/" + userId + "/Android'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/data'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/data/" + packageName + "'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/media'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/media/" + packageName + "'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/obb'" +
                    " AND path != '/storage/emulated/" + userId + "/Android/obb/" + packageName + "'" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?" +
                    " AND path NOT like ?"
            val selectionArgs = arrayOf(userId.toString(), packageName,
                    "/storage/emulated/$userId/Android/data/$packageName/%",
                    "/storage/emulated/$userId/Android/media/$packageName/%",
                    "/storage/emulated/$userId/Android/obb/$packageName/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_ALARMS + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_DCIM + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_DOCUMENTS + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_DOWNLOADS + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_MOVIES + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_MUSIC + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_NOTIFICATIONS + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_PICTURES + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_PODCASTS + "/%",
                    "/storage/emulated/" + userId + "/" + Environment.DIRECTORY_RINGTONES + "/%")

            existsNonstandardBehavior = !srm.queryFileMonitor(selection, selectionArgs, "id DESC", "1").isEmpty()
        } catch (e: Exception) { // TODO Should throw runtime exception?
            existsNonstandardBehavior = false
            return
        } finally {
            existsNonstandardBehaviorNextCheck = System.currentTimeMillis() + 1000 * 60
        }
        return
    }

    private fun loadSummaryText(context: Context): CharSequence {
        val sb = StringBuilder()
        val resources = context.resources
        if (isRedirectEnabled) {
            val count = fileCount
            sb.append(if (count != -1) resources.getQuantityString(R.plurals.list_summary_on, count, count) else resources.getString(R.string.list_summary_on))
        } else {
            sb.append(resources.getString(if (isVerified()) R.string.list_summary_off_and_app_is_verified else R.string.list_summary_off))
            if (existsNonstandardBehavior == true) {
                sb.append('\n').append(resources.getString(R.string.list_summary_suffix_nonstandard_behavior))
            }
        }
        /*if (!isRegularApp) {
            sb.append('\n').append(resources.getString(R.string.detail_summary_system_app_non_regular))
        } else if (isSystemApp) {
            sb.append('\n').append(resources.getString(R.string.detail_summary_system_app))
        }*/
        return sb.toString()
    }

    fun loadSummaryTextToAsync(context: Context, summary: TextView): Job {
        summary.setText(if (isRedirectEnabled) R.string.list_summary_on else if (isVerified()) R.string.list_summary_off_and_app_is_verified else R.string.list_summary_off)
        return launch {
            try {
                withContext(AppIconCache.dispatcher()) {
                    loadFileCount()
                    loadExistsNonstandardBehavior()
                }
            } catch (e: CancellationException) {
                // do nothing if canceled
                return@launch
            } catch (e: Exception) {
            }


            summary.text = loadSummaryText(context)
        }
    }

    fun getSubtitle(context: Context): CharSequence {
        var subtitle = if (sharedUserLabel != null) context.getString(R.string.item_summary, sharedUserLabel, context.getString(R.string.shared_user_id)) else label.toString()
        if (UserHelper.myUserId() != userId) {
            subtitle = context.getString(R.string.app_name_with_user, subtitle, userId)
        }
        return subtitle
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val appInfo = other as AppInfo
        if (userId != appInfo.userId) return false
        if (isRedirectEnabled != appInfo.isRedirectEnabled) return false
        return if (packageName != appInfo.packageName) false else label == appInfo.label
    }

    override fun hashCode(): Int {
        var result = applicationInfo!!.packageName.hashCode()
        result = 31 * result + userId
        return result
    }

    override fun toString(): String {
        return "AppInfo{" +
                "title=" + label +
                ", userId=" + userId +
                ", enabled=" + isRedirectEnabled +
                '}'
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeParcelable(redirectInfo, flags)
        TextUtils.writeToParcel(label, dest, 0)
        dest.writeInt(fileCount)
        dest.writeLong(fileCountUpdate)
        //dest.writeParcelable(configuration, flags)
        dest.writeInt(ParcelUtils.booleanToInt(isVerified))
    }

    private constructor(`in`: Parcel) {
        redirectInfo = `in`.readParcelable(RedirectPackageInfo::class.java.classLoader)!!
        label = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(`in`)
        fileCount = `in`.readInt()
        fileCountUpdate = `in`.readLong()
        //configuration = `in`.readParcelable(AppConfiguration::class.java.classLoader)
        isVerified = ParcelUtils.booleanFromInt(`in`.readInt())
    }

    override fun itemSamesTo(other: Any): Boolean {
        if (this === other) return true
        if (javaClass != other.javaClass) return false
        return if (userId != (other as AppInfo).userId) false else packageName == other.packageName
    }

    override fun contentSamesTo(other: Any): Boolean {
        return isRedirectEnabled == (other as AppInfo).isRedirectEnabled
                && isVerified == other.isVerified
                && redirectInfo.mountDirsConfig == other.redirectInfo.mountDirsConfig
                && redirectInfo.redirectTarget == other.redirectInfo.redirectTarget
    }

    companion object {

        const val CONFIG_CHANGED_TYPE_ENABLED = 0
        const val CONFIG_CHANGED_TYPE_REDIRECT_TARGET = 1
        const val CONFIG_CHANGED_TYPE_ACCESSIBLE_FOLDERS = 2
        const val CONFIG_CHANGED_TYPE_OBSERVERS = 3

        @JvmField
        val CREATOR: Creator<AppInfo> = object : Creator<AppInfo> {
            override fun createFromParcel(source: Parcel): AppInfo {
                return AppInfo(source)
            }

            override fun newArray(size: Int): Array<AppInfo?> {
                return arrayOfNulls(size)
            }
        }
    }
}