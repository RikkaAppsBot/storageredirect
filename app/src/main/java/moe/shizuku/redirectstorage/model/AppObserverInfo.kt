package moe.shizuku.redirectstorage.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import moe.shizuku.redirectstorage.ObserverInfo
import rikka.internal.model.MultiLocaleEntity

class AppObserverInfo {

    // online fields
    lateinit var onlineId: String

    @SerializedName("title")
    var title: AppRecommendationTexts.PresetOrCustomText

    inline val summary get() = title.custom?.get() ?: "@${title.presetKey}"

    @SerializedName("description")
    var description: MultiLocaleEntity? = null

    @SerializedName("source")
    val source: String

    @SerializedName("target")
    val target: String

    @SerializedName("call_media_scan")
    var callMediaScan = false

    @SerializedName("add_to_downloads")
    var showNotification = false

    /** allow folders in source path  */
    @SerializedName("allow_child")
    var allowChild = false

    @SerializedName("allow_temp")
    var allowTemp = false

    @SerializedName("mask")
    var mask: String? = null

    // local only fields
    var id: String? = null
    var packageName: String? = null
    var userId = -1
    var enabled = false

    // tmp fields
    @Transient
    var icon = false

    @Transient
    var local = false

    @Transient
    var online = false

    @Transient
    var sourceMountDir: String? = null

    internal constructor(title: String, source: String, target: String, callMediaScan: Boolean, showNotification: Boolean, allowChild: Boolean, allowTemp: Boolean, mask: String?, packageName: String?, userId: Int, enabled: Boolean) {
        this.title = if (title.startsWith("@"))
            AppRecommendationTexts.PresetOrCustomText(presetKey = title.substring(1))
        else
            AppRecommendationTexts.PresetOrCustomText(custom = AppRecommendationTexts.PresetText().apply { put("_", title) })

        this.source = source
        this.target = target
        this.callMediaScan = callMediaScan
        this.showNotification = showNotification
        this.allowChild = allowChild
        this.allowTemp = allowTemp
        this.mask = mask
        this.packageName = packageName
        this.userId = userId
        this.enabled = enabled
    }

    fun getTitleText(): String? {
        return title.getText { key: String? ->
            AppRecommendationTexts.getExportFolderTitleText(key)
        }
    }

    fun getDescriptionText(): String? {
        return description?.get()
    }

    fun toObserverInfo(): ObserverInfo {
        return ObserverInfo(packageName, userId, enabled, summary, source, target, callMediaScan, showNotification, allowChild, allowTemp, mask)
    }

    fun equalsOnlineFields(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as AppObserverInfo
        return target.equals(that.target, ignoreCase = true) &&
                source.equals(that.source, ignoreCase = true) &&
                //callMediaScan == that.callMediaScan &&
                //showNotification == that.showNotification &&
                //allowChild == that.allowChild &&
                //allowTemp == that.allowTemp &&
                mask == that.mask &&
                summary == that.summary
    }

    companion object {

        fun getSummaryText(summary: String?): String? {
            return if (summary?.startsWith("@") == true)
                AppRecommendationTexts.getExportFolderTitleText(summary.substring(1)).get()
            else
                summary
        }
    }
}

fun ObserverInfo.toApp(): AppObserverInfo {
    return AppObserverInfo(if (summary != null) summary else "@saved_files", source, target, callMediaScan, showNotification, allowChild, allowTemp, mask, packageName, userId, enabled).apply {
        onlineId = ""
        local = true
        online = false
    }
}

class ObserverInfoBuilder : Parcelable {

    private val packageName: String?
    private val userId: Int

    var mask: String? = null
        private set

    var callMediaScan = false
        private set

    var showNotification = false
        private set

    var source: String? = null
        private set

    var target: String? = null
        private set

    var description: String? = "@saved_files"
        private set

    var allowChild = false
        private set

    var allowTemp = false
        private set

    var changed = false
        private set

    constructor(packageName: String?, userId: Int) {
        this.packageName = packageName
        this.userId = userId
    }

    constructor(oi: ObserverInfo) {
        mask = oi.mask
        callMediaScan = oi.callMediaScan
        showNotification = oi.showNotification
        allowTemp = oi.allowTemp
        allowChild = oi.allowChild
        packageName = oi.packageName
        userId = oi.userId
        source = oi.source
        target = oi.target
        description = oi.summary
    }

    protected constructor(`in`: Parcel) {
        mask = `in`.readString()
        callMediaScan = `in`.readByte().toInt() != 0
        showNotification = `in`.readByte().toInt() != 0
        packageName = `in`.readString()
        userId = `in`.readInt()
        source = `in`.readString()
        target = `in`.readString()
        description = `in`.readString()
        allowChild = `in`.readByte().toInt() != 0
        allowTemp = `in`.readByte().toInt() != 0
        changed = `in`.readByte().toInt() != 0
    }

    fun mask(mask: String?) = apply {
        if (this.mask == mask) return@apply
        this.mask = mask
        changed = true
    }

    fun callMediaScan(callMediaScan: Boolean) = apply {
        if (this.callMediaScan == callMediaScan) return@apply
        this.callMediaScan = callMediaScan
        changed = true
    }

    fun showNotification(showNotification: Boolean) = apply {
        if (this.showNotification == this.showNotification) return@apply
        this.showNotification = showNotification
        changed = true
    }

    fun source(source: String?) = apply {
        if (this.source == source) return@apply
        this.source = source
        changed = true
    }

    fun target(target: String?) = apply {
        if (this.target == target) return@apply
        this.target = target
        changed = true
    }

    fun description(description: String?) = apply {
        if (this.description == description) return@apply
        this.description = description
        changed = true
    }

    fun allowChild(allowChild: Boolean) = apply {
        if (this.allowChild == allowChild) return@apply
        this.allowChild = allowChild
        changed = true
    }

    fun allowTemp(allowTemp: Boolean) = apply {
        if (this.allowTemp == allowTemp) return@apply
        this.allowTemp = allowTemp
        changed = true
    }

    fun build(): ObserverInfo {
        return ObserverInfo(
                mask, callMediaScan, showNotification, allowTemp, allowChild,
                packageName, userId, source, target, description
        )
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(mask)
        parcel.writeByte((if (callMediaScan) 1 else 0).toByte())
        parcel.writeByte((if (showNotification) 1 else 0).toByte())
        parcel.writeString(packageName)
        parcel.writeInt(userId)
        parcel.writeString(source)
        parcel.writeString(target)
        parcel.writeString(description)
        parcel.writeByte((if (allowChild) 1 else 0).toByte())
        parcel.writeByte((if (allowTemp) 1 else 0).toByte())
        parcel.writeByte((if (changed) 1 else 0).toByte())
    }

    companion object {

        @JvmStatic
        val CREATOR: Parcelable.Creator<ObserverInfoBuilder> = object : Parcelable.Creator<ObserverInfoBuilder> {
            override fun createFromParcel(`in`: Parcel): ObserverInfoBuilder {
                return ObserverInfoBuilder(`in`)
            }

            override fun newArray(size: Int): Array<ObserverInfoBuilder?> {
                return arrayOfNulls(size)
            }
        }
    }
}