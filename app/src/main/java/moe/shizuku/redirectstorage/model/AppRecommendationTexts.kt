package moe.shizuku.redirectstorage.model

import android.content.Context
import android.graphics.drawable.Drawable
import com.google.gson.*
import com.google.gson.annotations.SerializedName
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRApplication
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import rikka.core.res.isNight
import rikka.internal.model.MultiLocaleEntity
import rikka.material.app.LocaleDelegate
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

data class AppRecommendationTexts(
        @SerializedName("colors")
        private val colors: Map<String, Color> = HashMap(),
        @SerializedName("recommendations")
        private val recommendations: Map<String, PresetText> = HashMap(),
        @SerializedName("behaviors")
        private val behaviors: Map<String, PresetText> = HashMap(),
        @SerializedName("accessible_folder")
        private val accessibleFolder: Map<String, PresetText> = HashMap(),
        @SerializedName("export_folder_title")
        private val exportFolderTitle: Map<String, PresetText> = LinkedHashMap(),
        @SerializedName("export_folder")
        private val exportFolder: Map<String, PresetText> = HashMap()
) {

    companion object {

        private val CACHE_MAX_ALIVE_TIME = if (BuildConfig.DEBUG) 1000L else 10 * 60 * 1000L

        private val ICONS = mapOf(
                "check" to R.drawable.ic_check_circle_24dp,
                "cancel" to R.drawable.ic_cancel_24dp,
                "warning" to R.drawable.ic_warning_24dp,
                "question" to R.drawable.ic_help_24dp
        )

        private var nextUpdate = 0L

        private var instance: AppRecommendationTexts? = null

        val exportFolderTitle get() = instance?.exportFolderTitle

        fun loadLocal(context: Context) {
            if (instance != null) {
                return
            }
            try {
                val reader: Reader = BufferedReader(InputStreamReader(context.assets.open("app_recommendation_data.json")))
                val builder = StringBuilder()
                val buffer = CharArray(8192)
                var read: Int
                while (reader.read(buffer, 0, buffer.size).also { read = it } > 0) {
                    builder.append(buffer, 0, read)
                }
                instance = SRApplication.GSON.fromJson(builder.toString(), AppRecommendationTexts::class.java)
            } catch (e: IOException) {
                e.printStackTrace()
                CrashReportHelper.logException(e)
            }
        }

        suspend fun loadAsync(context: Context) {
            if (nextUpdate >= System.currentTimeMillis()) {
                return
            }
            val recommendationDataResponse = context.application.rikkaAppApiService
                    .getAppRecommendationDataAsync()
            if (recommendationDataResponse.isSuccessful && recommendationDataResponse.body() != null) {
                instance = recommendationDataResponse.body()!!
                nextUpdate = System.currentTimeMillis() + CACHE_MAX_ALIVE_TIME
            }
        }

        fun getRecommendationText(key: String?): PresetText {
            return instance?.recommendations?.get(key)
                    ?: PresetText().apply { put("en", "NOT FOUND: $key") }
        }

        fun getBehaviorText(key: String?): PresetText {
            return instance?.behaviors?.get(key)
                    ?: PresetText().apply { put("en", "NOT FOUND: $key") }
        }

        fun getAccessibleFolderText(key: String?): PresetText {
            return instance?.accessibleFolder?.get(key)
                    ?: PresetText().apply { put("en", "NOT FOUND: $key") }
        }

        fun getExportFolderTitleText(key: String?): PresetText {
            return instance?.exportFolderTitle?.get(key)
                    ?: PresetText().apply { put("en", "NOT FOUND: $key") }
        }

        fun getExportFolderText(key: String?): PresetText {
            return instance?.exportFolder?.get(key)
                    ?: PresetText().apply { put("en", "NOT FOUND: $key") }
        }
    }

    data class Color(val light: Int, val dark: Int) {

        fun get(context: Context): Int {
            val night = context.resources.configuration.isNight()
            return if (night) {
                dark
            } else {
                light
            }
        }

        companion object {

            @JvmField
            val GSON_ADAPTER = GsonAdapter()

            class GsonAdapter : JsonDeserializer<Color>, JsonSerializer<Color> {

                override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Color {
                    val jsonObject = json.asJsonObject

                    val lightHex = jsonObject["light"]?.asString
                    val darkHex = jsonObject["dark"]?.asString
                    val light = try {
                        android.graphics.Color.parseColor(lightHex)
                    } catch (e: Exception) {
                        0xff000000.toInt()
                    }
                    val dark = try {
                        android.graphics.Color.parseColor(darkHex)
                    } catch (e: Exception) {
                        0xffffffff.toInt()
                    }
                    return Color(light, dark)
                }

                override fun serialize(src: Color, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
                    TODO("not implemented")
                }
            }
        }
    }

    open class PresetText(
            private val colorKey: String? = null,
            private val iconKey: String? = null) : MultiLocaleEntity() {

        fun getText(locale: Locale = LocaleDelegate.defaultLocale): String {
            return get(locale)
        }

        fun getColor(context: Context): Int? {
            return (instance?.colors?.get(colorKey))?.get(context)
        }

        fun getIcon(context: Context): Drawable? {
            return if (iconKey == null) {
                null
            } else {
                val id = ICONS[iconKey] ?: 0
                if (id != 0)
                    context.getDrawable(id)
                else null
            }
        }

        companion object {

            @JvmField
            val GSON_ADAPTER = GsonAdapter()

            class GsonAdapter : JsonDeserializer<PresetText>, JsonSerializer<PresetText> {

                override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): PresetText {
                    val jsonObject = json.asJsonObject

                    val color = jsonObject["color"]?.asString
                    val icon = jsonObject["icon"]?.asString
                    val entity = PresetText(color, icon)

                    jsonObject.entrySet()
                            .filter { it.key != "color" && it.key != "icon" }
                            .forEach {
                                entity[it.key] = it.value.asString ?: ""
                            }

                    return entity
                }

                override fun serialize(src: PresetText, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
                    TODO("not implemented")
                }
            }
        }
    }

    data class PresetOrCustomText(
            val presetKey: String? = null,
            val custom: PresetText? = null
    ) {

        fun getText(provider: (String?) -> PresetText): String {
            return when {
                presetKey != null -> {
                    provider(presetKey).getText()
                }
                custom != null -> {
                    custom.getText()
                }
                else -> {
                    "(null)"
                }
            }
        }

        fun getIcon(context: Context, provider: (String?) -> PresetText): Drawable? {
            return when {
                presetKey != null -> {
                    provider(presetKey).getIcon(context)
                }
                custom != null -> {
                    custom.getIcon(context)
                }
                else -> {
                    provider(null).getIcon(context)
                }
            }
        }

        fun getColor(context: Context, provider: (String?) -> PresetText): Int? {
            return when {
                presetKey != null -> {
                    provider(presetKey).getColor(context)
                }
                custom != null -> {
                    custom.getColor(context)
                }
                else -> {
                    provider(null).getColor(context)
                }
            }
        }

        companion object {

            @JvmField
            val GSON_ADAPTER = GsonAdapter()

            class GsonAdapter : JsonDeserializer<PresetOrCustomText>, JsonSerializer<PresetOrCustomText> {

                override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): PresetOrCustomText {
                    return if (!json.isJsonObject) {
                        val key = json.asString
                        if (key.isNotEmpty() && key[0] == '@') {
                            PresetOrCustomText(key.substring(1))
                        } else {
                            PresetOrCustomText("`$key")
                        }
                    } else {
                        PresetOrCustomText(null, context.deserialize(json, PresetText::class.java))
                    }
                }

                override fun serialize(src: PresetOrCustomText, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
                    if (src.presetKey != null) {
                        return JsonPrimitive(src.presetKey)
                    } else {
                        TODO("not implemented")
                    }
                }
            }
        }
    }

    class PresetOrCustomTextList : ArrayList<PresetOrCustomText>() {

        fun getText(provider: (String?) -> PresetText): String {
            val sb = StringBuilder()
            var lastIsRaw: Boolean
            forEachIndexed { index, item ->
                lastIsRaw = false
                if (item.presetKey != null) {
                    if (item.presetKey.isNotEmpty() && item.presetKey[0] == '`') {
                        sb.append(item.presetKey.substring(1))
                        lastIsRaw = true
                    } else {
                        sb.append(provider(item.presetKey).getText())
                    }
                } else if (item.custom != null) {
                    sb.append(item.custom.getText())
                }
                if (!lastIsRaw && index != size - 1) {
                    sb.append("<br>")
                }
            }
            return sb.toString()
        }

        companion object {

            @JvmField
            val GSON_ADAPTER = GsonAdapter()

            class GsonAdapter : JsonDeserializer<PresetOrCustomTextList>, JsonSerializer<PresetOrCustomTextList> {

                override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): PresetOrCustomTextList {
                    val list = PresetOrCustomTextList()
                    json.asJsonArray.forEach {
                        list.add(context.deserialize(it, PresetOrCustomText::class.java))
                    }
                    return list
                }

                override fun serialize(src: PresetOrCustomTextList, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
                    TODO("not implemented")
                }
            }
        }
    }
}