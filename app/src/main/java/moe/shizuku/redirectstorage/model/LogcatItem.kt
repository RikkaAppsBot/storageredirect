package moe.shizuku.redirectstorage.model

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import android.util.Log
import moe.shizuku.redirectstorage.R

class LogcatItem : Parcelable {

    companion object {

        @JvmField
        val CREATOR: Creator<LogcatItem> = object : Creator<LogcatItem> {
            override fun createFromParcel(`in`: Parcel): LogcatItem {
                return LogcatItem(`in`)
            }

            override fun newArray(size: Int): Array<LogcatItem?> {
                return arrayOfNulls(size)
            }
        }

        fun levelToInt(level: String?): Int {
            return when (level) {
                "D" -> return Log.DEBUG
                "E" -> return Log.ERROR
                "I" -> return Log.INFO
                "V" -> return Log.VERBOSE
                "W" -> return Log.WARN
                "F" -> return 100
                else -> -1
            }
        }
    }


    var date: String? = null
    var pid = -1
    var tid = -1
    var tag: String? = null
    var level: String? = null
    var msg: String? = null

    constructor()

    private constructor(`in`: Parcel) {
        level = `in`.readString()
        tag = `in`.readString()
        msg = `in`.readString()
        pid = `in`.readInt()
        tid = `in`.readInt()
        date = `in`.readString()
    }

    val colorPrimaryAttr: Int
        get() = when (level) {
            "V" -> R.attr.logcatVerbosePrimary
            "D" -> R.attr.logcatDebugPrimary
            "I" -> R.attr.logcatInfoPrimary
            "W" -> R.attr.logcatWarnPrimary
            "E" -> R.attr.logcatErrorPrimary
            "F" -> R.attr.logcatFatalPrimary
            else -> R.attr.logcatVerbosePrimary
        }

    val colorSecondaryAttr: Int
        get() = when (level) {
            "V" -> R.attr.logcatVerboseSecondary
            "D" -> R.attr.logcatDebugSecondary
            "I" -> R.attr.logcatInfoSecondary
            "W" -> R.attr.logcatWarnSecondary
            "E" -> R.attr.logcatErrorSecondary
            "F" -> R.attr.logcatFatalSecondary
            else -> R.attr.logcatVerboseSecondary
        }

    val levelInt: Int get() = levelToInt(level)

    val line: String get() = "$date $pid/$tid $level/$tag: $msg"

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(level)
        dest.writeString(tag)
        dest.writeString(msg)
        dest.writeInt(pid)
        dest.writeInt(tid)
        dest.writeString(date)
    }
}