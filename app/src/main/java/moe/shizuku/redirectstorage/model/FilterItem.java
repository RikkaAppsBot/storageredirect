package moe.shizuku.redirectstorage.model;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;
import java.util.Optional;

public class FilterItem {

    public String id;
    public int titleResourceId = 0;
    public @Nullable
    String title;

    public boolean isSelected = true;
    public boolean isEditing = false;

    public FilterItem(String id) {
        this.id = id;
    }

    public FilterItem setTitle(int titleResourceId) {
        this.title = null;
        this.titleResourceId = titleResourceId;
        return this;
    }

    public FilterItem setTitle(String title) {
        this.title = title;
        return this;
    }

    public FilterItem setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        return this;
    }

    public FilterItem setEditing(boolean isEditing) {
        this.isEditing = isEditing;
        return this;
    }

    @NonNull
    public String getTitle(@NonNull Context context) {
        return Optional.ofNullable(title).orElse(context.getString(titleResourceId));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterItem that = (FilterItem) o;
        return titleResourceId == that.titleResourceId &&
                isSelected == that.isSelected &&
                isEditing == that.isEditing &&
                Objects.equals(id, that.id) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titleResourceId, title, isSelected, isEditing);
    }
}
