package moe.shizuku.redirectstorage.model

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.format.DateUtils
import androidx.annotation.VisibleForTesting
import com.google.gson.annotations.SerializedName
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.utils.OmissionStringBuilder
import rikka.core.util.ResourceUtils
import rikka.internal.model.MultiLocaleEntity
import java.util.*

class AppConfiguration/* : Parcelable*/ {

    companion object {

        @JvmStatic
        @VisibleForTesting
        fun resolveReason(reason: MultiLocaleEntity?, locale: Locale, context: Context): String? {
            if (reason != null && reason.size > 0) {
                if (reason.containsKey("internal")) {
                    val identifier = reason["internal"]
                    return ResourceUtils.resolveString(context.resources, "reason_$identifier", identifier)
                }
                return reason.get(locale)
            }
            return null
        }

        var DEFAULT: AppConfiguration? = null

        /*@JvmField
        val CREATOR: Creator<AppConfiguration> = object : Creator<AppConfiguration> {
            override fun createFromParcel(source: Parcel): AppConfiguration {
                return AppConfiguration(source)
            }

            override fun newArray(size: Int): Array<AppConfiguration?> {
                return arrayOfNulls(size)
            }
        }*/

        init {
            DEFAULT = AppConfiguration()
            DEFAULT!!.isDefault = true
        }
    }

    @Transient
    var isDefault = false
        private set

    @SerializedName("package")
    var packageName: String? = null

    @SerializedName("authors")
    var authors: List<String>? = null

    @SerializedName("last_update")
    private val lastUpdate: LastUpdate? = null

    @VisibleForTesting
    @SerializedName("recommendation")
    val recommendation: AppRecommendationTexts.PresetOrCustomText? = null

    @SerializedName("behaviors")
    val behaviors: AppRecommendationTexts.PresetOrCustomTextList? = null

    @SerializedName("accessible_folders")
    val accessibleFolder: AccessibleFolder? = null

    @SerializedName("export_folders")
    val exportFolders: ExportFolder? = null

    /**
     * Don't store this key in StorageRedirect-assets.
     * To provide common data for all RikkaApps, read this key from "rikka.app/app_category/apps"
     */
    @SerializedName("app_category")
    var categoryType: String? = null

    class LastUpdate {

        @SerializedName("timestamp")
        val timestamp: Long = 0

        @SerializedName("version_name")
        private val versionName: String? = null

        @SerializedName("version_code")
        private val versionCode: Long = 0
    }

    class AccessibleFolder {

        @SerializedName("recommendations")
        val recommendations: AppRecommendationTexts.PresetOrCustomTextList? = null

        @SerializedName("data")
        val data: Map<String, AppSimpleMountInfo>? = null

        fun text(): String? {
            return recommendations?.getText { key: String? ->
                AppRecommendationTexts.getAccessibleFolderText(key)
            }
        }
    }

    class ExportFolder {

        @SerializedName("recommendations")
        val recommendations: AppRecommendationTexts.PresetOrCustomTextList? = null

        @SerializedName("data")
        val data: Map<String, AppObserverInfo>? = null

        fun text(): String? {
            return recommendations?.getText { key: String? ->
                AppRecommendationTexts.getExportFolderText(key)
            }
        }
    }

    fun recommendationText(): String? {
        return recommendation?.getText { key: String? ->
            AppRecommendationTexts.getRecommendationText(key)
        }
    }

    fun recommendationColor(context: Context): Int? {
        return recommendation?.getColor(context) { key: String? ->
            AppRecommendationTexts.getRecommendationText(key)
        }
    }

    fun recommendationIcon(context: Context): Drawable? {
        return recommendation?.getIcon(context) { key: String? ->
            AppRecommendationTexts.getRecommendationText(key)
        }
    }

    fun behaviors(): String? {
        return behaviors?.getText { key: String? ->
            AppRecommendationTexts.getBehaviorText(key)
        }
    }

    private fun getAuthorsListText(context: Context): String {
        val osb = OmissionStringBuilder(context)
        osb.maxCount(Int.MAX_VALUE)
        authors?.forEach { a ->
            if ("DEVELOPER" == a) {
                osb.append(String.format("<b>%s</b>", context.getString(R.string.author_developer)))
            } else {
                osb.append(String.format("<b>%s</b>", a))
            }
        }
        return osb.build()
    }

    fun authors(context: Context): String {
        return if (authors == null || authors!!.isEmpty()) {
            ""
        } else context.getString(R.string.rule_contributors, getAuthorsListText(context))
    }

    fun lastUpdate(context: Context): String {
        if (lastUpdate == null) {
            return context.getString(R.string.rule_last_update_unknown)
        }
        val time = DateUtils.getRelativeTimeSpanString(lastUpdate.timestamp * 1000, System.currentTimeMillis(), 0).toString()
        return context.getString(R.string.rule_last_update, time)
    }

    /*override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(packageName)
        dest.writeMap(description as Map<*, *>?)
        dest.writeByte(if (isRecommended) 1.toByte() else 0.toByte())
        dest.writeByte(if (isVerified) 1.toByte() else 0.toByte())
        dest.writeStringList(authors)
        dest.writeTypedList(observers)
        dest.writeByte(if (isDefaultConfiguration) 1.toByte() else 0.toByte())
        dest.writeStringList(recommendedMountDirs)
        dest.writeInt(if (observerDescriptions == null) -1 else observerDescriptions!!.size)
        if (observerDescriptions != null) {
            for (entry in observerDescriptions!!) {
                dest.writeMap(entry as Map<*, *>?)
            }
        }
        dest.writeString(categoryType)
    }

    constructor(src: AppConfiguration) {
        packageName = src.packageName
        description = src.description
        isRecommended = src.isRecommended
        authors = src.authors?.let { ArrayList(it) }
        observers = src.observers?.let { ArrayList(it) }
        isDefaultConfiguration = src.isDefaultConfiguration
        isRecommended = src.isRecommended
        recommendedMountDirs = src.recommendedMountDirs?.let { ArrayList(it) }
        observerDescriptions = src.observerDescriptions
        categoryType = src.categoryType
    }

    private constructor(`in`: Parcel) {
        packageName = `in`.readString()
        description = MultiLocaleEntity()
        `in`.readMap(description as Map<*, *>, null)
        if (description!!.size == 0) {
            description = null
        }
        isRecommended = `in`.readByte().toInt() != 0
        authors = `in`.createStringArrayList()
        observers = `in`.createTypedArrayList(ObserverInfo.CREATOR)
        isDefaultConfiguration = `in`.readByte().toInt() != 0
        recommendedMountDirs = `in`.createStringArrayList()
        val size = `in`.readInt()
        if (size == -1) {
            observerDescriptions = null
        }
        observerDescriptions = ArrayList()
        for (i in 0 until size) {
            val e = MultiLocaleEntity()
            `in`.readMap(e as Map<*, *>, null)
            observerDescriptions!!.add(e)
        }
        categoryType = `in`.readString()
    }*/
}