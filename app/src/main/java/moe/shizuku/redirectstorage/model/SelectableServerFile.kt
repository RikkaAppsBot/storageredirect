package moe.shizuku.redirectstorage.model

import android.os.Parcel
import android.os.Parcelable

class SelectableServerFile
@JvmOverloads
constructor(
        val serverFile: ServerFile,
        var selected: Boolean = false,
        var selectable: Boolean = true,
        var allowEnter: Boolean = true,
        var neverSelectable: Boolean = false,
        var selectedSubfolders: Int = 0,
        var selectedSubfiles: Int = 0,
        var errorReason: String? = null,
        var normalReason: String? = null
) : Parcelable, IServerFile<ServerFile> by serverFile {

    companion object {

        val BACK_ITEM = SelectableServerFile(
                ServerFile.BACK_ITEM,
                selectable = false,
                allowEnter = true
        )

        @JvmField
        val CREATOR = object : Parcelable.Creator<SelectableServerFile> {
            override fun createFromParcel(parcel: Parcel): SelectableServerFile {
                return SelectableServerFile(parcel)
            }

            override fun newArray(size: Int): Array<SelectableServerFile?> {
                return arrayOfNulls(size)
            }
        }

    }

    constructor(parcel: Parcel) : this(
            parcel.readParcelable<ServerFile>(ServerFile::class.java.classLoader)!!,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()
    )

    private var _selectedPartially: Boolean? = null

    var selectedPartially: Boolean
        get() = !selected && (_selectedPartially?: ((selectedSubfiles + selectedSubfolders) > 0))
        set(value) {
            _selectedPartially = value
        }

    fun contentEquals(other: SelectableServerFile): Boolean {
        return serverFile == other.serverFile &&
                selected == other.selected &&
                selectable == other.selectable &&
                allowEnter == other.allowEnter &&
                selectedSubfolders == other.selectedSubfolders &&
                selectedSubfiles == other.selectedSubfiles &&
                errorReason == other.errorReason &&
                normalReason == other.normalReason
    }

    override fun isBackItem(): Boolean {
        return this == BACK_ITEM
    }

    override fun equals(other: Any?): Boolean {
        return (other as? SelectableServerFile)?.serverFile == this.serverFile
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(serverFile, flags)
        parcel.writeByte((if (selected) 1 else 0).toByte())
        parcel.writeByte((if (selectable) 1 else 0).toByte())
        parcel.writeByte((if (allowEnter) 1 else 0).toByte())
        parcel.writeByte((if (neverSelectable) 1 else 0).toByte())
        parcel.writeInt(selectedSubfolders)
        parcel.writeInt(selectedSubfiles)
        parcel.writeString(errorReason)
        parcel.writeString(normalReason)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun hashCode(): Int {
        var result = serverFile.hashCode()
        result = 31 * result + selected.hashCode()
        result = 31 * result + selectable.hashCode()
        result = 31 * result + allowEnter.hashCode()
        result = 31 * result + neverSelectable.hashCode()
        result = 31 * result + selectedSubfolders
        result = 31 * result + selectedSubfiles
        result = 31 * result + (errorReason?.hashCode() ?: 0)
        result = 31 * result + (normalReason?.hashCode() ?: 0)
        result = 31 * result + (_selectedPartially?.hashCode() ?: 0)
        return result
    }

}