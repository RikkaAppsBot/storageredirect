package moe.shizuku.redirectstorage.model

import android.database.Cursor
import android.os.Parcelable
import android.provider.MediaStore.Images.ImageColumns
import kotlinx.android.parcel.Parcelize
import moe.shizuku.redirectstorage.ktx.getColumnIndexOrNull

@Parcelize
class MediaStoreImageRow
@JvmOverloads
constructor(
        var id: Long = 0,
        var displayName: String? = null,
        var path: String = "",
        var size: Long = 0,
        var width: Int = 0,
        var height: Int = 0,
        var dateLastModified: Long = 0,
        var bucketName: String? = null,
        var bucketId: String? = null
) : Parcelable {

    companion object {

        @JvmStatic
        val PROJECTION = arrayOf(
                ImageColumns._ID,
                ImageColumns.DATA,
                ImageColumns.DISPLAY_NAME,
                ImageColumns.SIZE,
                ImageColumns.WIDTH,
                ImageColumns.HEIGHT,
                ImageColumns.DATE_MODIFIED,
                ImageColumns.BUCKET_DISPLAY_NAME,
                ImageColumns.BUCKET_ID
        )

        @JvmStatic
        val THUMBNAILS_PROJECTION = arrayOf(
                ImageColumns._ID,
                ImageColumns.DATA,
                ImageColumns.WIDTH,
                ImageColumns.HEIGHT
        )

        @JvmStatic
        fun fromCursor(cursor: Cursor): MediaStoreImageRow {
            val row = MediaStoreImageRow()
            with(cursor) {
                getColumnIndexOrNull(ImageColumns._ID)?.let {
                    row.id = getLong(it)
                }
                getColumnIndexOrNull(ImageColumns.DATA)?.let {
                    row.path = getString(it)
                }
                getColumnIndexOrNull(ImageColumns.DISPLAY_NAME)?.let {
                    row.displayName = getString(it)
                }
                getColumnIndexOrNull(ImageColumns.SIZE)?.let {
                    row.size = getLong(it)
                }
                getColumnIndexOrNull(ImageColumns.WIDTH)?.let {
                    row.width = getInt(it)
                }
                getColumnIndexOrNull(ImageColumns.HEIGHT)?.let {
                    row.height = getInt(it)
                }
                getColumnIndexOrNull(ImageColumns.DATE_MODIFIED)?.let {
                    row.dateLastModified = getLong(it)
                }
                getColumnIndexOrNull(ImageColumns.BUCKET_DISPLAY_NAME)?.let {
                    row.bucketName = getString(it)
                }
                getColumnIndexOrNull(ImageColumns.BUCKET_ID)?.let {
                    row.bucketId = getString(it)
                }
            }
            return row
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is MediaStoreImageRow) return false
        return other.id == this.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}
