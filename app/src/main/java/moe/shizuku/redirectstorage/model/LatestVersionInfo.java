package moe.shizuku.redirectstorage.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.license.GoogleBillingHelper;
import rikka.core.util.IntentUtils;
import rikka.internal.model.MultiLocaleEntity;

@Entity(tableName = "latest_version_info")
public class LatestVersionInfo {

    private static final String DEFAULT_URL = "https://sr.rikka.app/";

    @PrimaryKey
    @ColumnInfo(name = "code")
    @SerializedName("code")
    public int code;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    public String name;

    @ColumnInfo(name = "ignoreable")
    @SerializedName("ignoreable")
    public boolean ignoreable;

    @ColumnInfo(name = "download")
    @SerializedName("download")
    public Download download;

    @ColumnInfo(name = "modules")
    @SerializedName("modules")
    public List<Module> modules;

    @ColumnInfo(name = "dialogs")
    @SerializedName("dialogs")
    public List<DialogInfo> dialogs;

    @ColumnInfo(name = "text")
    @SerializedName("text")
    public MultiLocaleEntity text;

    @ColumnInfo(name = "ignore_google_play")
    @SerializedName("ignore_google_play")
    public boolean ignore_google_play;

    @SerializedName("download_button")
    @ColumnInfo(name = "download_button")
    public MultiLocaleEntity downloadButton;

    @SerializedName("ignore_button")
    @ColumnInfo(name = "ignore_button")
    public MultiLocaleEntity ignoreButton;

    public static class Download {

        @ColumnInfo(name = "url")
        @SerializedName("url")
        public String url;

        @ColumnInfo(name = "abi")
        @SerializedName("abi")
        public HashMap<String, String> abi;
    }

    public static class Module {

        @ColumnInfo(name = "version_name")
        @SerializedName("version_name")
        public String versionName;

        @ColumnInfo(name = "version_code")
        @SerializedName("version_code")
        public int versionCode;

        @ColumnInfo(name = "min_server_version")
        @SerializedName("min_server_version")
        public int minServerVersion;
    }

    public static class DialogInfo {

        @ColumnInfo(name = "id")
        @SerializedName("id")
        public String id;

        @ColumnInfo(name = "title")
        @SerializedName("title")
        public MultiLocaleEntity title;

        @ColumnInfo(name = "message")
        @SerializedName("message")
        public MultiLocaleEntity message;

        @ColumnInfo(name = "positive_button")
        @SerializedName("positive_button")
        public ButtonInfo positiveButton;

        @ColumnInfo(name = "negative_button")
        @SerializedName("negative_button")
        public ButtonInfo negativeButton;

        @ColumnInfo(name = "neutral_button")
        @SerializedName("neutral_button")
        public ButtonInfo neutralButton;

        @ColumnInfo(name = "min_version")
        @SerializedName("min_version")
        public int minVersionCode;

        @ColumnInfo(name = "max_version")
        @SerializedName("max_version")
        public int maxVersionCode;

        @ColumnInfo(name = "cancelable")
        @SerializedName("cancelable")
        public boolean cancelable;

        @ColumnInfo(name = "ignoreable")
        @SerializedName("ignoreable")
        public boolean ignoreable;

        public static class ButtonInfo {

            @ColumnInfo(name = "text")
            @SerializedName("text")
            public MultiLocaleEntity text;

            @ColumnInfo(name = "url")
            @SerializedName("url")
            public String url;
        }
    }

    public void startActivity(Context context) {
        if (!ignore_google_play && GoogleBillingHelper.isInstallerGooglePlay(context)) {
            IntentUtils.startActivity(context, new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)), context.getString(R.string.chooser_no_apps));
        } else {
            Uri uri = null;
            String abi = Build.SUPPORTED_ABIS[0];
            if (download != null) {
                if (download.abi != null && download.abi.containsKey(abi)) {
                    uri = Uri.parse(download.abi.get(abi));
                } else if (download.url != null) {
                    uri = Uri.parse(download.url);
                }
            }

            if (uri == null) {
                uri = Uri.parse(DEFAULT_URL);
            }

            IntentUtils.startActivity(context, new Intent(Intent.ACTION_VIEW, uri), context.getString(R.string.chooser_no_apps));
        }
    }

    public boolean showUpdate() {
        return BuildConfig.VERSION_CODE < code || BuildConfig.DEBUG;
    }

    @Nullable
    public Module getModule(int serverVersion) {
        if (modules == null) {
            return null;
        }

        for (Module module : modules) {
            if (serverVersion >= module.minServerVersion) {
                return module;
            }
        }
        return null;
    }

    @NonNull
    public List<DialogInfo> getDialogs() {
        List<DialogInfo> res = new ArrayList<>();
        if (dialogs == null) {
            return res;
        }

        int versionCode = BuildConfig.VERSION_CODE;
        for (DialogInfo dialog : dialogs) {
            if ((dialog.minVersionCode == 0 || versionCode >= dialog.minVersionCode)
                    && (dialog.maxVersionCode == 0 || versionCode <= dialog.maxVersionCode)) {
                res.add(dialog);
            }
        }
        return res;
    }
}
