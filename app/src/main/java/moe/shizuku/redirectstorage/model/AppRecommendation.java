package moe.shizuku.redirectstorage.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

public final class AppRecommendation /*implements Parcelable*/ {

    /*public static final Creator<AppRecommendation> CREATOR = new Creator<AppRecommendation>() {
        @Override
        public AppRecommendation createFromParcel(Parcel in) {
            return new AppRecommendation(in);
        }

        @Override
        public AppRecommendation[] newArray(int size) {
            return new AppRecommendation[size];
        }
    };*/

    private AppRecommendation(int status) {
        this.status = status;
    }

    public static final int STATUS_NO_RESULT = 1;
    public static final int STATUS_LOADED = 3;

    public final int status;
    public AppConfiguration config;
    private String appCategorySuggestion;

    /*private AppRecommendation(Parcel in) {
        status = in.readInt();
        config = in.readParcelable(AppConfiguration.class.getClassLoader());
    }*/

    public static AppRecommendation fromAppConfiguration(@NonNull AppConfiguration configuration) {
        final AppRecommendation item = new AppRecommendation(STATUS_LOADED);
        item.config = configuration;
        return item;
    }

    /*@Nullable
    public String getAppCategory() {
        return appCategorySuggestion != null ? appCategorySuggestion :
                (config != null ? config.appCategory : null);
    }

    @Nullable
    public String getDescriptionByCategory(@NonNull Context context) {
        return AppCategory.getTypeDescription(context, getAppCategory());
    }*/

    public static AppRecommendation noResult(@Nullable String appCategorySuggestion) {
        final AppRecommendation item = new AppRecommendation(STATUS_NO_RESULT);
        item.appCategorySuggestion = appCategorySuggestion;
        return item;
    }

    /*@Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeParcelable(config, flags);
    }*/

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof AppRecommendation)) return false;
        final AppRecommendation other = (AppRecommendation) obj;
        if (this.status == other.status) {
            if (this.status == STATUS_LOADED) {
                return Objects.equals(this.config, other.config);
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
