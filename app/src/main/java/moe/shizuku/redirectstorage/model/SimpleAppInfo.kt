package moe.shizuku.redirectstorage.model

import android.content.pm.ApplicationInfo
import android.os.Parcel
import android.os.Parcelable
import moe.shizuku.redirectstorage.RedirectPackageInfo

import rikka.core.util.AppNameComparator

class SimpleAppInfo : Parcelable, Comparable<SimpleAppInfo>, IPackageDescriptor {

    val applicationInfo: ApplicationInfo?
    val label: CharSequence?
    override val sharedUserId: String?
    override val packageName: String
    override val userId: Int

    val name: String get() = if (sharedUserId != null) RedirectPackageInfo.SHARED_USER_PREFIX + sharedUserId else packageName

    constructor(packageName: String, userId: Int, applicationInfo: ApplicationInfo?, label: CharSequence, sharedUserId: String?) {
        this.applicationInfo = applicationInfo
        this.label = label
        this.sharedUserId = sharedUserId
        this.packageName = packageName
        this.userId = userId
    }

    private constructor(`in`: Parcel) {
        applicationInfo = `in`.readParcelable(ApplicationInfo::class.java.classLoader)
        label = `in`.readString()
        sharedUserId = `in`.readString()
        packageName = `in`.readString()!!
        userId = `in`.readInt()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }
        return if (other !is SimpleAppInfo) {
            false
        } else this.packageName == other.packageName && this.userId == other.userId
    }

    override fun compareTo(other: SimpleAppInfo): Int {
        return COMPARATOR.compare(this, other)
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeParcelable(applicationInfo, flags)
        dest.writeString(label?.toString())
        dest.writeString(sharedUserId)
        dest.writeString(packageName)
        dest.writeInt(userId)
    }

    override fun hashCode(): Int {
        var result = applicationInfo?.hashCode() ?: 0
        result = 31 * result + (label?.hashCode() ?: 0)
        result = 31 * result + userId
        return result
    }

    private class Comparator : java.util.Comparator<SimpleAppInfo> {

        private val mAppNameComparator: AppNameComparator<SimpleAppInfo>

        private class InfoProvider : AppNameComparator.InfoProvider<SimpleAppInfo> {

            override fun getTitle(item: SimpleAppInfo): CharSequence? {
                return item.label
            }

            override fun getPackageName(item: SimpleAppInfo): String {
                return item.packageName
            }

            override fun getUserId(item: SimpleAppInfo): Int {
                return item.userId
            }
        }

        init {
            mAppNameComparator = AppNameComparator(InfoProvider())
        }

        override fun compare(a: SimpleAppInfo, b: SimpleAppInfo): Int {
            return mAppNameComparator.compare(a, b)
        }
    }

    companion object {

        private val COMPARATOR = Comparator()

        @JvmField
        val CREATOR: Parcelable.Creator<SimpleAppInfo> = object : Parcelable.Creator<SimpleAppInfo> {
            override fun createFromParcel(`in`: Parcel): SimpleAppInfo {
                return SimpleAppInfo(`in`)
            }

            override fun newArray(size: Int): Array<SimpleAppInfo?> {
                return arrayOfNulls(size)
            }
        }
    }

}