package moe.shizuku.redirectstorage.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class SimplePackageDescriptor(
        override val packageName: String,
        override val userId: Int,
        override val sharedUserId: String?
) : IPackageDescriptor, Parcelable {

    companion object {

        fun create(packageName: String, userId: Int, sharedUserId: String?): IPackageDescriptor {
            return SimplePackageDescriptor(packageName, userId, sharedUserId)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is IPackageDescriptor) return false
        return other.packageName == packageName && other.userId == userId
    }

    override fun hashCode(): Int {
        return Objects.hash(packageName, userId)
    }

}