package moe.shizuku.redirectstorage.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectableSimpleAppInfo(
        val appInfo: SimpleAppInfo,
        var selected: Boolean = false,
        var selectable: Boolean = true,
        var summary: CharSequence? = null
) : Parcelable, IPackageDescriptor by appInfo {

    override fun equals(other: Any?): Boolean {
        if (other !is IPackageDescriptor) return false
        return other.packageName == packageName && other.userId == userId
    }

    override fun hashCode(): Int {
        return appInfo.hashCode()
    }

}