package moe.shizuku.redirectstorage.model

import android.content.Context
import androidx.annotation.Keep
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.dao.AppCategoryDao
import moe.shizuku.redirectstorage.ktx.application
import rikka.core.util.IOUtils
import rikka.internal.model.MultiLocaleEntity
import java.util.*

object AppCategory {

    // CATEGORY_UNDEFINED = null
    const val CATEGORY_AUDIO = "audio"
    const val CATEGORY_GAME = "game"
    const val CATEGORY_IMAGE = "image"
    const val CATEGORY_MAPS = "maps"
    const val CATEGORY_NEWS = "news"
    const val CATEGORY_PRODUCTIVITY = "productivity"
    const val CATEGORY_SOCIAL = "social"
    const val CATEGORY_VIDEO = "video"

    // From ApplicationInfo (API >= 26)
    const val RAW_CATEGORY_UNDEFINED = -1
    const val RAW_CATEGORY_GAME = 0
    const val RAW_CATEGORY_AUDIO = 1
    const val RAW_CATEGORY_VIDEO = 2
    const val RAW_CATEGORY_IMAGE = 3
    const val RAW_CATEGORY_SOCIAL = 4
    const val RAW_CATEGORY_NEWS = 5
    const val RAW_CATEGORY_MAPS = 6
    const val RAW_CATEGORY_PRODUCTIVITY = 7

    private const val DEFAULT_TYPE_DATA_FILENAME = "app_categories_data.json"
    private const val DEFAULT_TYPE_CATEGORIES_FILENAME = "app_categories_description.json"

    private val sTypeToRawMap = hashMapOf(
            CATEGORY_AUDIO to RAW_CATEGORY_AUDIO,
            CATEGORY_GAME to RAW_CATEGORY_GAME,
            CATEGORY_IMAGE to RAW_CATEGORY_IMAGE,
            CATEGORY_MAPS to RAW_CATEGORY_MAPS,
            CATEGORY_NEWS to RAW_CATEGORY_NEWS,
            CATEGORY_PRODUCTIVITY to RAW_CATEGORY_PRODUCTIVITY,
            CATEGORY_SOCIAL to RAW_CATEGORY_SOCIAL,
            CATEGORY_VIDEO to RAW_CATEGORY_VIDEO
    )
    private val sTypesData = mutableListOf<Item>()
    private val sDescriptionData = mutableListOf<DescriptionItem>()

    private val CACHE_MAX_ALIVE_TIME = if (BuildConfig.DEBUG) 1000L else 10 * 60 * 1000L

    @JvmStatic
    fun rawIntToStringType(rawInt: Int): String? {
        return sTypeToRawMap.entries.find { (_, value) -> rawInt == value }?.key
    }

    fun stringTypeToRawInt(stringType: String?): Int {
        return stringType?.let { sTypeToRawMap[it] } ?: RAW_CATEGORY_UNDEFINED
    }

    private fun loadTypesDataFromAssets(context: Context) {
        if (sTypesData.isNotEmpty()) {
            throw IllegalStateException("Types data has been loaded.")
        }

        context.assets.open(DEFAULT_TYPE_DATA_FILENAME).use { inputStream ->
            val json = IOUtils.toString(inputStream)
            val data = Gson().fromJson(json, Array<Item>::class.java)
            sTypesData.addAll(Arrays.asList(*data))
        }
    }

    private fun loadDescriptionDataFromAssets(context: Context) {
        if (sDescriptionData.isNotEmpty()) {
            throw IllegalStateException("Description data has been loaded.")
        }

        context.assets.open(DEFAULT_TYPE_CATEGORIES_FILENAME).use { inputStream ->
            val json = IOUtils.toString(inputStream)
            val data = Gson().fromJson(json, Array<DescriptionItem>::class.java)
            sDescriptionData.addAll(Arrays.asList(*data))
        }
    }

    private fun ensureDefaultData(context: Context) {
        if (sTypesData.isEmpty()) {
            synchronized(AppCategory::class.java) {
                if (sTypesData.isEmpty()) {
                    loadTypesDataFromAssets(context)
                }
            }
            if (sTypesData.isEmpty()) {
                throw RuntimeException("Default list should not be empty.")
            }
        }
        if (sDescriptionData.isEmpty()) {
            synchronized(AppCategory::class.java) {
                if (sDescriptionData.isEmpty()) {
                    loadDescriptionDataFromAssets(context)
                }
            }
            if (sDescriptionData.isEmpty()) {
                throw RuntimeException("Default list should not be empty.")
            }
        }
    }

    fun get(context: Context, stringType: String?): Item? {
        return stringType?.let {
            ensureDefaultData(context)
            return@let sTypesData.find { item -> it == item.type }
        }
    }

    @JvmStatic
    fun getTypeTitle(context: Context, stringType: String?): String? {
        return stringType?.let { get(context, it)?.title?.get() }
    }

    @JvmStatic
    fun getTypeDescription(context: Context, stringType: String?): String? {
        if (stringType == null) {
            return null
        }

        ensureDefaultData(context)

        return sDescriptionData.find { stringType == it.type }?.description?.get()
    }

    @Synchronized
    fun setTypesData(data: List<Item>?) {
        sTypesData.clear()
        if (data?.isNotEmpty() == true) {
            sTypesData.addAll(data)
        }
    }

    @Synchronized
    fun setDescriptionData(data: List<DescriptionItem>?) {
        sDescriptionData.clear()
        if (data?.isNotEmpty() == true) {
            sDescriptionData.addAll(data)
        }
    }

    @JvmStatic
    fun load(context: Context) = GlobalScope.launch {
        val dao = context.application.database.appCategoryDao
        val time = System.currentTimeMillis() - AppCategoryDao.getLastUpdateTime(context)
        if (time <= CACHE_MAX_ALIVE_TIME) {
            setTypesData(dao.baseItems)
            setDescriptionData(dao.descriptionItems)
            return@launch
        }

        val baseData = context.application.rikkaAppApiService.getAppCategoryBaseDataAsync()
        val desc = context.application.rikkaAppApiService.getAppCategoriesDescriptionSync()

        try {
            val baseDataRes = baseData.body()
                    ?: throw IllegalArgumentException("Failed to get app categories data.")
            val descRes = desc.body()
                    ?: throw IllegalStateException("Failed to get app categories data.")

            dao.update(context, baseDataRes, descRes)
            setTypesData(baseDataRes)
            setDescriptionData(descRes)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Keep
    @Entity(tableName = "app_category_base_item")
    class Item {

        @NonNull
        @PrimaryKey
        lateinit var type: String
        lateinit var title: MultiLocaleEntity

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other !is Item) return false

            return this.type == other.type
        }

    }

    @Keep
    @Entity(tableName = "app_category_description_item")
    class DescriptionItem {

        @NonNull
        @PrimaryKey
        lateinit var type: String
        var description: MultiLocaleEntity? = null

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other !is DescriptionItem) return false

            return this.type == other.type
        }

    }

    @Keep
    class AppSuggestion {

        @SerializedName("package")
        lateinit var packageName: String
        var category: String? = null

    }

}
