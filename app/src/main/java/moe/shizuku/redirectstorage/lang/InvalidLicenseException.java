package moe.shizuku.redirectstorage.lang;

public class InvalidLicenseException extends Exception {

    public int reason;

    public InvalidLicenseException(int reason) {
        super("invalid license (" + reason + ")");

        this.reason = reason;
    }
}
