package moe.shizuku.redirectstorage.lang;

public class NoLogDetectedException extends Exception {

    public int second;

    public NoLogDetectedException(int second) {
        super("no log detected in " + second + "s");

        this.second = second;
    }
}
