package moe.shizuku.redirectstorage.lang;

public class ServerVersionMismatchException extends Exception {

    public int currentVersion;

    public ServerVersionMismatchException(int currentVersion) {
        super("server version mismatch, current " + currentVersion);

        this.currentVersion = currentVersion;
    }
}
