package moe.shizuku.redirectstorage.lang;

public class EmulatedStorageNotFoundException extends Exception {

    public String expect;
    public String real;

    public EmulatedStorageNotFoundException(String expect, String real) {
        super("bad storage path, expect " + expect + " real " + real);

        this.expect = expect;
        this.real = real;
    }
}
