package moe.shizuku.redirectstorage.lang;

public class ModuleInstallationException extends Exception {

    public int reason;

    public ModuleInstallationException(int reason) {
        super("module installation problem (" + reason + ")");

        this.reason = reason;
    }
}
