package moe.shizuku.redirectstorage.provider;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.Configuration;
import android.provider.DocumentsProvider;

import moe.shizuku.redirectstorage.app.Settings;

public abstract class BaseDocumentsProvider extends DocumentsProvider {

    @Override
    public void attachInfo(Context context, ProviderInfo info) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(Settings.getLocale());
        context = context.createConfigurationContext(configuration);

        super.attachInfo(context, info);
    }
}
