package moe.shizuku.redirectstorage.provider;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.DeadObjectException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.provider.DocumentsContract;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsContract.Root;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import io.reactivex.Flowable;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRFileManager;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.model.AppRecommendationTexts;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.Base64;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;

public class DownloadsProvider extends BaseDocumentsProvider {

    public static final ComponentName COMPONENT_NAME = new ComponentName(
            BuildConfig.APPLICATION_ID, DownloadsProvider.class.getName()
    );

    private static final String ROOT_ID = "root";
    private static final String TAG = "SRDownloadsProvider";

    private static final String[] DEFAULT_ROOT_PROJECTION =
            new String[]{Root.COLUMN_ROOT_ID, Root.COLUMN_MIME_TYPES,
                    Root.COLUMN_FLAGS, Root.COLUMN_ICON, Root.COLUMN_TITLE,
                    Root.COLUMN_SUMMARY, Root.COLUMN_DOCUMENT_ID,
                    Root.COLUMN_AVAILABLE_BYTES,};
    private static final String[] DEFAULT_DOCUMENT_PROJECTION = new
            String[]{Document.COLUMN_DOCUMENT_ID, Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME, Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS, Document.COLUMN_SIZE,};

    private static final int DEFAULT_ROOT_SUPPORTS_FLAGS =
            Root.FLAG_SUPPORTS_SEARCH | Root.FLAG_LOCAL_ONLY | Root.FLAG_SUPPORTS_CREATE;
    private static final int DEFAULT_FILES_SUPPORTS_FLAGS =
            Document.FLAG_SUPPORTS_DELETE | Document.FLAG_SUPPORTS_RENAME
                    | Document.FLAG_SUPPORTS_WRITE;
    @RequiresApi(api = Build.VERSION_CODES.N)
    private static final int DEFAULT_FILES_SUPPORTS_FLAGS_API_24 =
            Document.FLAG_SUPPORTS_MOVE | Document.FLAG_SUPPORTS_REMOVE;
    private static final int DEFAULT_DIRS_SUPPORTS_FLAGS =
            Document.FLAG_DIR_SUPPORTS_CREATE | Document.FLAG_SUPPORTS_RENAME
                    | Document.FLAG_SUPPORTS_DELETE | Document.FLAG_SUPPORTS_WRITE;

    private static final int TYPE_UNKNOWN = -1, TYPE_FILE = 0, TYPE_DIR = 1;

    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".documents.downloads";

    private int mCachedObserversUserId = -1;
    private List<ObserverInfo> mCachedObservers = new ArrayList<>();

    @Nullable
    private SRManager mSRManager;

    @Override
    public boolean onCreate() {
        return true;
    }

    @NonNull
    private SRManager requireSRManager() throws DeadObjectException {
        if (mSRManager == null || !mSRManager.isAlive()) {
            mSRManager = SRManager.createThrow();
        }
        return mSRManager;
    }

    @NonNull
    private Context requireContextCompat() {
        Context context = getContext();
        if (context == null) {
            throw new NullPointerException();
        }
        return context;
    }

    public static int getDefaultFilesSupportsFlags() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return DEFAULT_FILES_SUPPORTS_FLAGS | DEFAULT_FILES_SUPPORTS_FLAGS_API_24;
        } else {
            return DEFAULT_FILES_SUPPORTS_FLAGS;
        }
    }

    @Override
    public Cursor queryRoots(@Nullable String[] projection) {
        Log.d(TAG, "queryRoots");

        final MatrixCursor result = new MatrixCursor(
                Optional.ofNullable(projection).orElse(DEFAULT_ROOT_PROJECTION));

        final MatrixCursor.RowBuilder row = result.newRow();
        row.add(Root.COLUMN_ROOT_ID, "sr.downloads.0");
        row.add(Root.COLUMN_ICON, R.drawable.ic_launcher);
        row.add(Root.COLUMN_TITLE, requireContextCompat().getString(R.string.downloads_provider_title));
        row.add(Root.COLUMN_SUMMARY, requireContextCompat().getString(R.string.downloads_provider_summary));
        row.add(Root.COLUMN_FLAGS, DEFAULT_ROOT_SUPPORTS_FLAGS);
        row.add(Root.COLUMN_DOCUMENT_ID, "sr.downloads.0");

        result.setNotificationUri(requireContextCompat().getContentResolver(),
                DocumentsContract.buildRootUri(AUTHORITY, "sr.downloads.0"));

        return result;
    }

    @Override
    public Cursor queryDocument(String documentId, @Nullable String[] projection) {
        Log.d(TAG, "queryDocument documentId=" + documentId);

        final SRManager srm;
        try {
            srm = requireSRManager();
        } catch (Exception e) {
            // TODO Show error
            return null;
        }

        final MatrixCursor result = new MatrixCursor(
                Optional.ofNullable(projection).orElse(DEFAULT_DOCUMENT_PROJECTION));

        DocumentIdModel model = DocumentIdModel.parse(documentId);

        if (DocumentIdModel.TYPE_DOWNLOADS.equals(model.type)) {
            if (model.isRoot) {
                final MatrixCursor.RowBuilder row = result.newRow();
                row.add(Document.COLUMN_DOCUMENT_ID, documentId);
                row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);

                result.setNotificationUri(requireContextCompat().getContentResolver(),
                        DocumentsContract.buildRootUri(AUTHORITY, model.getRootId()));
            } else {
                try {
                    final DocumentIdModel parent = model.getParentPath();
                    final ServerFile file = ServerFile.fromStorageRoot(
                            parent.downloadTarget + parent.relativePath + "/" + model.getPathLastSegment(),
                            model.userId
                    );
                    addFileRow(srm, result, parent, file, TYPE_UNKNOWN);
                } catch (RemoteException e) {
                    // should never happened
                }

                result.setNotificationUri(requireContextCompat().getContentResolver(),
                        DocumentsContract.buildDocumentUri(AUTHORITY, documentId));
            }
        }

        return result;
    }

    @Override
    public Cursor queryChildDocuments(String parentDocumentId,
                                      @Nullable String[] projection,
                                      @Nullable String sortOrder) {
        Log.d(TAG, "queryChildDocuments parentDocumentId=" + parentDocumentId);

        final SRManager srm;
        final SRFileManager fm;
        try {
            srm = requireSRManager();
            fm = srm.getFileManager();
        } catch (Exception e) {
            // TODO Show error
            return null;
        }

        final MatrixCursor result = new MatrixCursor(
                Optional.ofNullable(projection).orElse(DEFAULT_DOCUMENT_PROJECTION));
        result.setNotificationUri(requireContextCompat().getContentResolver(), DocumentsContract.buildChildDocumentsUri(AUTHORITY, parentDocumentId));

        DocumentIdModel model = DocumentIdModel.parse(parentDocumentId);

        if (DocumentIdModel.TYPE_DOWNLOADS.equals(model.type)) {
            if (model.isRoot) {
                // Root
                final PackageManager pm = requireContextCompat().getPackageManager();

                for (ObserverInfo observer : getDisplayObservers(model.userId, true)) {
                    ApplicationInfo ai;
                    try {
                        ai = srm.getApplicationInfo(observer.packageName, 0, model.userId);
                    } catch (Throwable e) {
                        continue;
                    }
                    if (ai == null) {
                        continue;
                    }

                    String description;
                    if (observer.summary != null && observer.summary.startsWith("@")) {
                        AppRecommendationTexts.Companion.loadLocal(requireContextCompat());
                        description = AppRecommendationTexts.Companion.getExportFolderTitleText(observer.summary.substring(1)).get();
                    } else {
                        description = observer.summary;
                    }
                    final MatrixCursor.RowBuilder row = result.newRow();
                    row.add(Document.COLUMN_DISPLAY_NAME,
                            ai.loadLabel(pm)
                                    + " "
                                    + description);
                    row.add(Document.COLUMN_DOCUMENT_ID,
                            parentDocumentId + ":" +
                                    Base64.encodeToString(
                                            observer.packageName + "|" + observer.target).replace("\n", ""));
                    row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);
                    row.add(Document.COLUMN_FLAGS, DEFAULT_DIRS_SUPPORTS_FLAGS);
                }
            } else {
                // Child documents
                //Log.d(TAG, "target=" + model.downloadTarget + ", queryPath=" + model.relativePath);

                try {
                    for (ServerFile file : Optional.ofNullable(fm.list(ServerFile.fromStorageRoot(model.downloadTarget + model.relativePath, model.userId), 0))
                            .orElse(new ArrayList<>())) {
                        addFileRow(srm, result, model, file, file.isDirectory() ? TYPE_DIR : TYPE_FILE);
                    }
                } catch (RemoteException e) {
                    // should never happened
                }
            }
        }

        return result;
    }

    @Override
    public ParcelFileDescriptor openDocument(String documentId,
                                             String mode,
                                             @Nullable CancellationSignal signal)
            throws FileNotFoundException {
        Log.d(TAG, "openDocument documentId=" + documentId + ", mode=" + mode);

        final SRFileManager fm;
        try {
            fm = requireSRManager().getFileManager();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        DocumentIdModel model = DocumentIdModel.parse(documentId);

        try {
            return fm.openFileDescriptor(ServerFile.fromStorageRoot(
                    model.downloadTarget + model.relativePath, model.userId),
                    mode);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileNotFoundException();
        }
    }

    @Override
    public void deleteDocument(String documentId) throws FileNotFoundException {
        Log.d(TAG, "deleteDocument documentId=" + documentId);

        final SRFileManager fm;
        try {
            fm = requireSRManager().getFileManager();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        DocumentIdModel model = DocumentIdModel.parse(documentId);

        try {
            fm.delete(ServerFile.fromStorageRoot(model.downloadTarget + model.relativePath, model.userId));
            requireContextCompat().getContentResolver()
                    .notifyChange(DocumentsContract.buildChildDocumentsUri(AUTHORITY, model.getParentPath().toString()), null, false);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileNotFoundException();
        }
    }

    @Override
    public String renameDocument(String documentId, String displayName) throws FileNotFoundException {
        Log.d(TAG, "renameDocument documentId=" + documentId + " displayName=" + displayName);

        final SRFileManager fm;
        try {
            fm = requireSRManager().getFileManager();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        DocumentIdModel model = DocumentIdModel.parse(documentId);

        try {
            final ServerFile src = ServerFile.fromStorageRoot(model.downloadTarget + model.relativePath, model.userId);
            final ServerFile dest = new ServerFile(src);
            dest.setName(displayName);
            boolean succeed = fm.move(src, dest);
            if (succeed) {
                requireContextCompat().getContentResolver()
                        .notifyChange(DocumentsContract.buildChildDocumentsUri(AUTHORITY, model.getParentPath().toString()), null, false);
                return documentId.substring(0, documentId.lastIndexOf("/")) + "/" + displayName;
            } else {
                return documentId;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileNotFoundException();
        }
    }

    @Override
    public Cursor querySearchDocuments(String rootId,
                                       String query,
                                       String[] projection) {
        Log.d(TAG, "querySearchDocuments rootId=" + rootId + " query=" + query);

        final SRManager srm;
        final SRFileManager fm;
        try {
            srm = requireSRManager();
            fm = srm.getFileManager();
        } catch (Exception e) {
            // TODO Show error
            return null;
        }

        final MatrixCursor result = new MatrixCursor(
                Optional.of(projection).orElse(DEFAULT_DOCUMENT_PROJECTION));

        for (ObserverInfo observer : getDisplayObservers(0, true)) {
            try {
                for (ServerFile file : Optional.ofNullable(fm.list(ServerFile.fromStorageRoot(observer.target, 0), 0)).orElse(new ArrayList<>())) {
                    if (file.getName().contains(query)) {
                        addFileRow(
                                srm,
                                result,
                                DocumentIdModel.parse("sr.downloads." + 0
                                        + ":"
                                        + Base64.encodeToString(
                                        observer.packageName + "|" + observer.target)
                                ),
                                file,
                                TYPE_FILE
                        );
                    }
                }
            } catch (RemoteException e) {
                // should never happened
            }
        }

        return result;
    }

    @Override
    public String moveDocument(String sourceDocumentId,
                               String sourceParentDocumentId,
                               String targetParentDocumentId) {
        Log.d(TAG, "moveDocument " +
                "sourceDocumentId= " + sourceDocumentId);

        final SRFileManager fm;
        try {
            fm = requireSRManager().getFileManager();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        DocumentIdModel source = DocumentIdModel.parse(sourceDocumentId);
        DocumentIdModel target = DocumentIdModel.parse(targetParentDocumentId).getChild(source.getPathLastSegment());

        boolean succeed = false;
        try {
            succeed = fm.move(
                    ServerFile.fromStorageRoot(source.downloadTarget + source.relativePath, source.userId),
                    ServerFile.fromStorageRoot(target.downloadTarget + "/" + target.relativePath, target.userId)
            );
        } catch (RemoteException e) {
            // should never happened
        }

        if (!succeed) {
            throw new IllegalStateException("Cannot move document. " +
                    "Source document id = " + sourceDocumentId + ", " +
                    "Target document id = " + target.toString());
        }

        requireContextCompat().getContentResolver()
                .notifyChange(DocumentsContract.buildChildDocumentsUri(AUTHORITY, source.getParentPath().toString()), null, false);
        requireContextCompat().getContentResolver()
                .notifyChange(DocumentsContract.buildChildDocumentsUri(AUTHORITY, target.getParentPath().toString()), null, false);

        return targetParentDocumentId;
    }

    @Override
    public String createDocument(String parentDocumentId,
                                 String mimeType,
                                 String displayName) {
        final SRFileManager fm;
        try {
            fm = requireSRManager().getFileManager();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        DocumentIdModel parent = DocumentIdModel.parse(parentDocumentId);

        boolean succeed = false;
        try {
            succeed = fm.create(ServerFile.fromStorageRoot(
                    parent.downloadTarget + parent.relativePath + "/" + displayName, parent.userId));
        } catch (RemoteException e) {
            // should never happened
        }

        if (!succeed) {
            throw new IllegalStateException("Failed to create new file at: " + parentDocumentId);
        }

        requireContextCompat().getContentResolver()
                .notifyChange(DocumentsContract.buildChildDocumentsUri(AUTHORITY, parent.toString()), null, false);

        return parent.getChild(displayName).toString();
    }

    @Override
    public void removeDocument(String documentId,
                               String parentDocumentId) throws FileNotFoundException {
        deleteDocument(documentId);
    }

    private static void addFileRow(SRManager srm,
                                   MatrixCursor cursor,
                                   DocumentIdModel parent,
                                   ServerFile file, int type) throws RemoteException {
        final MatrixCursor.RowBuilder row = cursor.newRow();
        row.add(Document.COLUMN_DISPLAY_NAME, file.getName());
        row.add(Document.COLUMN_DOCUMENT_ID, parent.toString() + "/" + file.getName());

        boolean isDirectory = false;
        if (type == TYPE_UNKNOWN) {
            isDirectory = file.isDirectory();
        } else if (type == TYPE_DIR) {
            isDirectory = true;
        }

        final SRFileManager fm = srm.getFileManager();

        final long lastModified = fm.getLastModified(file);
        row.add(Document.COLUMN_LAST_MODIFIED, lastModified);
        if (!isDirectory) {
            row.add(Document.COLUMN_MIME_TYPE, Optional.of(
                    FileBrowserUtils.getMimeTypeBySuffix(
                            FileBrowserUtils.getFileSuffix(file.getName()))
            ).orElse(FileBrowserUtils.MIME_TYPE_UNKNOWN));
            row.add(Document.COLUMN_FLAGS, getDefaultFilesSupportsFlags());
            final long length = fm.length(file);
            row.add(Document.COLUMN_SIZE, length);
        } else {
            row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);
            row.add(Document.COLUMN_FLAGS, DEFAULT_DIRS_SUPPORTS_FLAGS);
        }
    }

    @NonNull
    private List<ObserverInfo> getDisplayObservers(int userId, boolean forceUpdate) {
        if (mCachedObservers.isEmpty() || userId != mCachedObserversUserId || forceUpdate) {
            final SRManager srm = SRManager.create();

            if (srm == null || !srm.isAlive()) {
                return Collections.emptyList();
            }

            mCachedObservers.clear();
            try {
                mCachedObservers.addAll(Flowable.fromIterable(srm.getRedirectPackages(SRManager.MATCH_ENABLED_ONLY, userId))
                        .flatMap(info ->
                                Flowable.fromIterable(srm.getObservers(info.packageName, userId))
                                        .filter(observer -> observer.showNotification)
                                        .distinct(observer -> observer.target)
                        )
                        .toList()
                        .blockingGet());
            } catch (RemoteException e) {
                // should never happened
            }
            mCachedObserversUserId = userId;
        }

        return mCachedObservers;
    }

    static class DocumentIdModel {

        public String type;
        public int userId;

        public String packageName;

        public boolean isRoot;
        public String relativePath;

        // For downloads type
        public String downloadTarget;

        static final String TYPE_DOWNLOADS = "downloads";

        private DocumentIdModel() {

        }

        public static DocumentIdModel parse(String documentId) {
            DocumentIdModel model = new DocumentIdModel();

            final String[] paths = documentId.split(":", 2);
            final String[] rootIdArgs = paths[0].split("\\.");
            if (!"sr".equals(rootIdArgs[0])) {
                throw new IllegalArgumentException("This is not a SR document id.");
            }
            model.type = rootIdArgs[1];
            model.userId = Integer.parseInt(rootIdArgs[2]);

            if (TYPE_DOWNLOADS.equals(model.type)) {
                if (paths.length == 1) {
                    model.isRoot = true;
                } else {
                    model.isRoot = false;

                    final String[] subpaths = paths[1].split("/");

                    final String[] observerArgs = Base64.decodeToString(subpaths[0]).split("\\|");
                    model.packageName = observerArgs[0];
                    model.downloadTarget = observerArgs[1];

                    StringBuilder pathBuilder = new StringBuilder();
                    for (int i = 1; i < subpaths.length; i++) {
                        pathBuilder.append("/").append(subpaths[i]);
                    }
                    model.relativePath = pathBuilder.toString();
                }
            }

            return model;
        }

        public DocumentIdModel getParentPath() {
            DocumentIdModel model = new DocumentIdModel();
            model.packageName = packageName;
            model.type = type;
            model.userId = userId;
            model.isRoot = isRoot;
            model.downloadTarget = downloadTarget;
            if (relativePath.contains("/")) {
                model.relativePath = relativePath.substring(0, relativePath.lastIndexOf("/"));
            } else {
                model.relativePath = "";
            }
            return model;
        }

        public DocumentIdModel getChild(String name) {
            DocumentIdModel model = new DocumentIdModel();
            model.packageName = packageName;
            model.type = type;
            model.userId = userId;
            model.isRoot = isRoot;
            model.downloadTarget = downloadTarget;
            Log.d(TAG, "getChild relativePath before = " + relativePath);
            if (TextUtils.isEmpty(relativePath)) {
                model.relativePath = name;
            } else {
                model.relativePath = relativePath + "/" + name;
            }
            Log.d(TAG, "getChild relativePath after = " + model.relativePath);
            return model;
        }

        public String getPathLastSegment() {
            return relativePath.contains("/") ?
                    relativePath.substring(relativePath.indexOf("/") + 1)
                    : relativePath;
        }

        public String getRootId() {
            return String.format(Locale.ENGLISH, "sr.%1$s.%2$d", type, userId);
        }

        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(getRootId());
            if (!isRoot) {
                if (TYPE_DOWNLOADS.equals(type)) {
                    sb.append(":");
                    sb.append(Base64.encodeToString(packageName + "|" + downloadTarget));
                    if (!TextUtils.isEmpty(relativePath)) {
                        if (!relativePath.startsWith("/")) {
                            sb.append("/");
                        }
                        sb.append(relativePath);
                    }
                }
            }
            return sb.toString().replace("\n", "");
        }

    }

}
