package moe.shizuku.redirectstorage.provider;

import android.annotation.SuppressLint;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.provider.OpenableColumns;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.model.ServerFile;

public class ServerFileProvider extends ContentProvider {

    private static final String[] COLUMNS = {
            OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};

    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".ServerFileProvider";

    @Override
    public boolean onCreate() {
        return SRManager.create() != null;
    }

    @Override
    public final void attachInfo(@NonNull Context context, @NonNull ProviderInfo info) {
        super.attachInfo(context, info);

        onAttachInfo(context, info);
    }

    public void onAttachInfo(@NonNull Context context, @NonNull ProviderInfo info) {
        // Sanity check our security
        if (info.exported) {
            throw new SecurityException("Provider must not be exported");
        }
        if (!info.grantUriPermissions) {
            throw new SecurityException("Provider must grant uri permissions");
        }
    }

    /**
     * Use a content URI returned by
     * {@link #getContentUriRelative(String, int, String) getContentUriRelative()}
     * to get information about a file managed by the ServerFileProvider.
     * FileProvider reports the column names defined in {@link android.provider.OpenableColumns}:
     * <ul>
     * <li>{@link android.provider.OpenableColumns#DISPLAY_NAME}</li>
     * <li>{@link android.provider.OpenableColumns#SIZE}</li>
     * </ul>
     * For more information, see
     * {@link ContentProvider#query(Uri, String[], String, String[], String)
     * ContentProvider.query()}.
     *
     * @param uri           A content URI returned by {@link #getContentUriRelative}.
     * @param projection    The list of columns to put into the {@link Cursor}. If null all columns are
     *                      included.
     * @param selection     Selection criteria to apply. If null then all data that matches the content
     *                      URI is returned.
     * @param selectionArgs An array of {@link java.lang.String}, containing arguments to bind to
     *                      the <i>selection</i> parameter. The <i>query</i> method scans <i>selection</i> from left to
     *                      right and iterates through <i>selectionArgs</i>, replacing the current "?" character in
     *                      <i>selection</i> with the value at the current position in <i>selectionArgs</i>. The
     *                      values are bound to <i>selection</i> as {@link java.lang.String} values.
     * @param sortOrder     A {@link java.lang.String} containing the column name(s) on which to sort
     *                      the resulting {@link Cursor}.
     * @return A {@link Cursor} containing the results of the query.
     */
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SRManager rm = SRManager.create();
        if (rm == null) {
            return null;
        }

        ResolveUriResult result = ResolveUriResult.parse(uri);
        final File file = getActualFile(result.packageName, result.userId, result.relativePath);
        if (file == null) {
            return null;
        }

        if (projection == null) {
            projection = COLUMNS;
        }

        String[] cols = new String[projection.length];
        Object[] values = new Object[projection.length];
        int i = 0;
        for (String col : projection) {
            if (OpenableColumns.DISPLAY_NAME.equals(col)) {
                cols[i] = OpenableColumns.DISPLAY_NAME;
                values[i++] = file.getName();
            } else if (OpenableColumns.SIZE.equals(col)) {
                cols[i] = OpenableColumns.SIZE;
                try {
                    final ServerFile serverFile;
                    if ("sdcard".equals(result.packageName)) {
                        serverFile = ServerFile.fromStorageRoot(result.relativePath, result.userId);
                    } else {
                        serverFile = ServerFile.fromRedirectTarget(result.relativePath, result.packageName, result.userId);
                    }
                    values[i++] = rm.getFileManager().length(serverFile);
                } catch (RemoteException e) {
                    // should never happened
                }
            }
        }

        cols = copyOf(cols, i);
        values = copyOf(values, i);

        final MatrixCursor cursor = new MatrixCursor(cols, 1);
        cursor.addRow(values);
        return cursor;
    }

    /**
     * Returns the MIME type of a content URI returned by
     * {@link #getContentUriRelative(String, int, String) getContentUriRelative()}.
     *
     * @param uri A content URI returned by
     *            {@link #getContentUriRelative(String, int, String) getContentUriRelative()}.
     * @return If the associated file has an extension, the MIME type associated with that
     * extension; otherwise <code>application/octet-stream</code>.
     */
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int lastDot = uri.toString().lastIndexOf('.');
        if (lastDot >= 0) {
            final String extension = uri.toString().substring(lastDot + 1);
            final String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (mime != null) {
                return mime;
            }
        }

        return "application/octet-stream";
    }

    /**
     * By default, this method throws an {@link java.lang.UnsupportedOperationException}. You must
     * subclass FileProvider if you want to provide different functionality.
     */
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        throw new UnsupportedOperationException("No external inserts");
    }

    /**
     * Deletes the file associated with the specified content URI, as
     * returned by {@link #getContentUriRelative(String, int, String) getContentUriRelative()}.
     * Notice that this method does <b>not</b> throw an {@link java.io.IOException};
     * you must check its return value.
     *
     * @param uri           A content URI for a file, as returned by
     *                      {@link #getContentUriRelative(String, int, String) getContentUriRelative()}.
     * @param selection     Ignored. Set to {@code null}.
     * @param selectionArgs Ignored. Set to {@code null}.
     * @return 1 if the delete succeeds; otherwise, 0.
     */
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SRManager rm = SRManager.create();
        if (rm == null) {
            return 0;
        }

        ResolveUriResult result = ResolveUriResult.parse(uri);
        try {
            final ServerFile serverFile;
            if ("sdcard".equals(result.packageName)) {
                serverFile = ServerFile.fromStorageRoot(result.relativePath, result.userId);
            } else {
                serverFile = ServerFile.fromRedirectTarget(result.relativePath, result.packageName, result.userId);
            }
            return rm.getFileManager().delete(serverFile) ? 1 : 0;
        } catch (RemoteException e) {
            // should never happened
            return 0;
        }
    }

    /**
     * By default, this method throws an {@link java.lang.UnsupportedOperationException}. You must
     * subclass FileProvider if you want to provide different functionality.
     */
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new UnsupportedOperationException("No external updates");
    }

    /**
     * By default, FileProvider automatically returns the
     * {@link ParcelFileDescriptor} for a file associated with a <code>content://</code>
     * {@link Uri}. To get the {@link ParcelFileDescriptor}, call
     * {@link android.content.ContentResolver#openFileDescriptor(Uri, String)
     * ContentResolver.openFileDescriptor}.
     * <p>
     * To override this method, you must provide your own subclass of FileProvider.
     *
     * @param uri  A content URI associated with a file, as returned by
     *             {@link #getContentUriRelative(String, int, String) getContentUriRelative()}.
     * @param mode Access mode for the file. May be "r" for read-only access, "rw" for read and
     *             write access, or "rwt" for read and write access that truncates any existing file.
     * @return A new {@link ParcelFileDescriptor} with which you can access the file.
     */
    @Nullable
    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        SRManager rm = SRManager.create();
        if (rm == null) {
            return null;
        }

        ResolveUriResult result = ResolveUriResult.parse(uri);
        ParcelFileDescriptor pfd = null;
        try {
            final ServerFile serverFile;
            if ("sdcard".equals(result.packageName)) {
                serverFile = ServerFile.fromStorageRoot(result.relativePath, result.userId);
            } else {
                serverFile = ServerFile.fromRedirectTarget(result.relativePath, result.packageName, result.userId);
            }
            pfd = rm.getFileManager().openFileDescriptor(serverFile, mode);
        } catch (Exception e) {
            Log.w(AppConstants.TAG, "openFile", e);
        }
        if (pfd == null) {
            throw new FileNotFoundException();
        }
        return pfd;
    }

    private File getActualFile(String packageName, int userId, String relativePath) {
        SRManager rm = SRManager.create();
        if (rm == null) {
            return null;
        }

        try {
            final ServerFile serverFile;
            if ("sdcard".equals(packageName)) {
                serverFile = ServerFile.fromStorageRoot(relativePath, userId);
            } else {
                serverFile = ServerFile.fromRedirectTarget(relativePath, packageName, userId);
            }
            return new File(rm.getFileManager().getAbsolutePath(serverFile));
        } catch (RemoteException e) {
            // should never happened
            return null;
        }
    }

    public static Uri getContentUriRelative(@NonNull String packageName, int userId, @NonNull String relativePath) {
        while (relativePath.startsWith("/")) {
            relativePath = relativePath.substring(1);
        }
        Uri.Builder builder = new Uri.Builder().scheme("content").authority(AUTHORITY)
                .appendPath(String.valueOf(userId))
                .appendPath(packageName);
        for (String item : relativePath.split("/")) {
            builder.appendPath(item);
        }
        return builder.build();
    }

    @SuppressLint("SdCardPath")
    public static String getRelativePathFromAbsoluteStoragePath(String absolutePath, int userId) {
        String path = absolutePath;
        if (path.startsWith("/sdcard/")) {
            path = path.replace("/sdcard/", "");
        }
        final String storagePath = String.format(Locale.ENGLISH, Constants.STORAGE_PATH_FORMAT, userId) + "/";
        if (path.startsWith(storagePath)) {
            path = path.replace(storagePath, "");
        }
        while (path.startsWith("/")) {
            path = path.substring(1);
        }
        return path;
    }

    public static Uri getContentUri(@NonNull String packageName, int userId, @NonNull File absoluteFile) {
        return getContentUriRelative(packageName, userId, getRelativePathFromAbsoluteStoragePath(absoluteFile.getAbsolutePath(), userId));
    }

    public static class ResolveUriResult {

        String packageName;
        int userId;
        String relativePath;

        private ResolveUriResult() {

        }

        public ResolveUriResult(String packageName, int userId, String relativePath) {
            this.packageName = packageName;
            this.userId = userId;
            this.relativePath = relativePath;
        }

        public static ResolveUriResult parse(Uri contentUri) {
            List<String> pathSegments = contentUri.getPathSegments();
            ResolveUriResult result = new ResolveUriResult();
            result.userId = Integer.parseInt(pathSegments.get(0));
            result.packageName = pathSegments.get(1);
            StringBuilder pathBuilder = new StringBuilder();
            for (int i = 2; i < pathSegments.size(); i++) {
                pathBuilder.append("/").append(pathSegments.get(i));
            }
            result.relativePath = pathBuilder.toString();
            if (result.relativePath.startsWith("/")) {
                result.relativePath = result.relativePath.substring(1);
            }
            return result;
        }

    }

    private static String[] copyOf(String[] original, int newLength) {
        final String[] result = new String[newLength];
        System.arraycopy(original, 0, result, 0, newLength);
        return result;
    }

    private static Object[] copyOf(Object[] original, int newLength) {
        final Object[] result = new Object[newLength];
        System.arraycopy(original, 0, result, 0, newLength);
        return result;
    }
}
