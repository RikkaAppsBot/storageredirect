package moe.shizuku.redirectstorage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import moe.shizuku.redirectstorage.app.AppBarFragmentActivity;
import moe.shizuku.redirectstorage.license.GoogleBillingHelper;
import moe.shizuku.redirectstorage.utils.SRInfoHelper;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration;
import rikka.core.content.FileProvider;
import rikka.core.util.ClipboardUtils;
import rikka.html.text.HtmlCompat;
import rikka.internal.help.HelpEntity;
import rikka.internal.help.HelpProvider;
import rikka.material.app.AppBarOwner;
import rikka.material.help.HelpFragment;
import rikka.widget.borderview.BorderRecyclerView;

public class HelpActivity extends AppBarFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (handleIntent(getIntent())) {
            return;
        }

        if (savedInstanceState == null) {
            Fragment fragment = new SRHelpFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }

        if (getSupportActionBar() != null) {
            if (getIntent().getStringExtra(Intent.EXTRA_TITLE) != null) {
                getSupportActionBar().setTitle(getIntent().getStringExtra(Intent.EXTRA_TITLE));
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


    private boolean handleIntent(Intent intent) {
        if (intent != null && intent.getData() != null) {
            Uri uri = intent.getData();
            if (uri.toString().startsWith("storage-redirect-client://help/")) {
                String id = uri.toString().substring("storage-redirect-client://help/".length());
                HelpEntity help = HelpProvider.get(this, id);
                if (help != null) {
                    help.startActivity(this);
                } else {
                    Toast.makeText(this, "Can't find help \"" + id + "\".", Toast.LENGTH_SHORT).show();
                }
                finish();

                return true;
            }
        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!getIntent().hasExtra(Intent.EXTRA_TEXT)) {
            getMenuInflater().inflate(R.menu.help, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_system_info:
                Context context = this;

                String versionName;
                try {
                    PackageInfo pi = context.getPackageManager().getPackageInfo(getPackageName(), 0);
                    versionName = pi.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    versionName = "(null)";
                }

                CharSequence text = HtmlCompat.fromHtml(getString(R.string.system_info_format,
                        String.format("%s %s", Build.MANUFACTURER, Build.MODEL),
                        String.format(Locale.ENGLISH, "%s (API %d)", Build.VERSION.RELEASE, Build.VERSION.SDK_INT),
                        Build.SUPPORTED_ABIS[0],
                        versionName
                ), null, SizeTagHandler.getInstance());
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle(R.string.system_info)
                        .setMessage(text)
                        .setPositiveButton(android.R.string.ok, null)
                        .setNeutralButton(R.string.action_copy, (d, which) -> ClipboardUtils.put(HelpActivity.this, text.toString()))
                        .create();

                dialog.setOnShowListener(d -> {
                    TextView message = ((AlertDialog) d).findViewById(android.R.id.message);
                    if (message != null) {
                        int[] attrs = {android.R.attr.textColorSecondary};
                        TypedArray a = context.obtainStyledAttributes(attrs);
                        int color = a.getColor(0, 0);
                        a.recycle();

                        message.setTextColor(color);
                        message.setLineSpacing(context.getResources().getDimension(R.dimen.dialog_line_space_extra), 1);
                        message.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.dialog_message));
                    }
                });

                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class SRHelpFragment extends HelpFragment {

        private static final String GROUP_URL = "https://t.me/RikkaUserGroup";

        @Override
        public RecyclerView onCreateRecyclerView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
            BorderRecyclerView recyclerView = (BorderRecyclerView) super.onCreateRecyclerView(inflater, parent, savedInstanceState);

            recyclerView.addItemDecoration(new VerticalPaddingDecoration(recyclerView.getContext()));
            ViewGroup.LayoutParams _lp = recyclerView.getLayoutParams();
            if (_lp instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) _lp;
                lp.leftMargin = lp.rightMargin = (int) recyclerView.getContext().getResources().getDimension(R.dimen.rd_activity_horizontal_margin);
            }
            recyclerView.getBorderViewDelegate().setBorderVisibilityChangedListener((top, oldTop, bottom, oldBottom) -> ((AppBarOwner) getActivity()).getAppBar().setRaised(!top));
            return recyclerView;
        }

        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
            super.onCreatePreferences(bundle, s);

            findPreference(KEY_TELEGRAM).getIntent()
                    .setData(Uri.parse(GROUP_URL));

            if (!GoogleBillingHelper.isGooglePlayAvailable(requireContext())) {
                findPreference(KEY_TELEGRAM).setVisible(false);
            }

            findPreference(KEY_MAIL).setIntent(null);
            findPreference(KEY_MAIL).setOnPreferenceClickListener(preference -> {
                Context context = requireContext();

                String text = new SRInfoHelper(getContext()).getInfo().toString();

                File file = context.getExternalCacheDir();
                if (file != null) {
                    file = new File(file, "system_info.txt");

                    try {
                        BufferedWriter out = new BufferedWriter(new FileWriter(file));
                        out.write(text);
                        out.close();
                    } catch (IOException e) {
                        Toast.makeText(context, "Can't write file.", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                if (file == null) {
                    Toast.makeText(context, "Can't write file, storage is not available.", Toast.LENGTH_SHORT).show();
                    return true;
                }

                String appName = getString(R.string.app_name);
                ChooserActivity.startSendMail(context, BuildConfig.SUPPORT_MAIL, appName + " support", null, FileProvider.getUriForFile(context, AppConstants.SUPPORT_PROVIDER_AUTHORITY, file));
                return true;
            });

            findPreference(KEY_ISSUE).setVisible(false);/*.getIntent()
                    .setData(Uri.parse(BuildConfig.ISSUE_TRACKER));*/

            List<HelpEntity> entities = HelpProvider.getAll(requireContext());
            if (entities != null && !entities.isEmpty()) {
                for (HelpEntity entity : entities) {
                    if (!entity.hideFromList) {
                        addArticle(entity);
                    }
                }
            } else {
                if (!requireActivity().isFinishing()) {
                    new AlertDialog.Builder(requireContext())
                            .setTitle(R.string.unable_to_load_help_title)
                            .setMessage(R.string.unable_to_load_help_message)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            }
        }

        public void addArticle(HelpEntity entity) {
            addPreference(KEY_HELP, HtmlCompat.fromHtml(entity.title.get()), entity.summary == null ? null : HtmlCompat.fromHtml(entity.summary.get()), requireContext().getDrawable(R.drawable.helplib_document_24dp), null, preference -> {
                entity.startActivity(requireContext());
                return true;
            });
        }
    }
}
