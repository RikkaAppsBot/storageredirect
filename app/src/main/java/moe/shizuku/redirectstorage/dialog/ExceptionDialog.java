package moe.shizuku.redirectstorage.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ChooserActivity;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.utils.SRInfoHelper;
import moe.shizuku.redirectstorage.utils.ThrowableUtils;
import rikka.core.content.FileProvider;
import rikka.core.util.ClipboardUtils;

public class ExceptionDialog extends AlertDialogFragment {

    public static ExceptionDialog newInstance(Throwable throwable) {
        return newInstance(throwable, true, null);
    }

    public static ExceptionDialog newInstance(Throwable throwable, boolean showSend, String message) {
        Bundle args = new Bundle();
        args.putSerializable("throwable", throwable);
        args.putBoolean("show_send", showSend);
        args.putString("message", message);
        ExceptionDialog fragment = new ExceptionDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static void show(FragmentManager fragmentManager, Throwable throwable, boolean showSend, String message) {
        if (fragmentManager.isStateSaved())
            return;

        newInstance(throwable, showSend, message).show(fragmentManager, ExceptionDialog.class.getSimpleName());
    }

    private Throwable mThrowable;
    private boolean mShowSend;
    private String mMessage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mThrowable = (Throwable) args.getSerializable("throwable");
            mShowSend = args.getBoolean("show_send");
            mMessage = args.getString("message");
        }
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder, @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.something_wrong)
                .setNegativeButton(android.R.string.cancel, null)
                .setNeutralButton(R.string.view_stacktrace, this::onClick);

        if (mShowSend) {
            builder.setPositiveButton(R.string.action_send, this::onClick)
                    .setMessage(R.string.bug_report_dialog_message);
        }

        if (mMessage != null) {
            builder.setMessage(mMessage);
        }
    }

    @Override
    public void onAlertDialogCreated(@NonNull AlertDialog dialog, View contentView, @Nullable Bundle savedInstanceState) {
        dialog.setCanceledOnTouchOutside(false);
    }

    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE) {
            send();
        } else if (which == Dialog.BUTTON_NEUTRAL) {
            viewStacktrace();
        }
    }

    private void send() {
        Context context = requireContext();

        String text = new SRInfoHelper(context).getInfo()
                + "\n\n" + ThrowableUtils.messageAndStack(mThrowable);

        File file = context.getExternalCacheDir();
        if (file != null) {
            file = new File(file, "stacktrace.txt");

            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(file));
                out.write(text);
                out.close();
            } catch (IOException e) {
                Toast.makeText(context, "Can't write log.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (file == null) {
            Toast.makeText(context, "Can't write log, storage is not available.", Toast.LENGTH_SHORT).show();
            return;
        }

        ChooserActivity.startSendMail(context, BuildConfig.SUPPORT_MAIL, getString(R.string.app_name) + " bug report", getString(R.string.bug_report_mail_message), FileProvider.getUriForFile(context, AppConstants.SUPPORT_PROVIDER_AUTHORITY, file));
    }

    private void viewStacktrace() {
        Context context = getActivity();
        if (context == null) {
            return;
        }

        Dialog dialog = new AlertDialog.Builder(context)
                .setMessage(ThrowableUtils.messageAndStack(mThrowable))
                .setPositiveButton(R.string.close, null)
                .setNeutralButton(R.string.action_copy, (dialog1, which) -> ClipboardUtils.put(context, ThrowableUtils.messageAndStack(mThrowable)))
                .create();

        dialog.setOnShowListener(d -> {
            TextView message = ((AlertDialog) d).findViewById(android.R.id.message);
            message.setTextIsSelectable(true);
            message.setTypeface(Typeface.MONOSPACE);
            message.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.stacktrace_size));
        });
        dialog.show();
    }
}
