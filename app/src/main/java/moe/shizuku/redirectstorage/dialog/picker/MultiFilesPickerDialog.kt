package moe.shizuku.redirectstorage.dialog.picker

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.MultiFilesPickerAdapter
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.dialog.FullScreenDialogFragment
import moe.shizuku.redirectstorage.model.SelectableServerFile
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.SizeTagHandler
import moe.shizuku.redirectstorage.widget.DefaultRVAdapterDataObserver
import rikka.html.text.HtmlCompat
import rikka.html.widget.HtmlCompatTextView
import rikka.insets.*
import rikka.material.widget.*
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView
import java.util.*
import kotlin.collections.ArrayList

abstract class MultiFilesPickerDialog : FullScreenDialogFragment(), MultiFilesPickerAdapter.Listener {

    companion object {

        const val EXTRA_SELECTED_FILES = "${AppConstants.EXTRA_PREFIX}.SELECTED_FILES"
        const val EXTRA_ORIGINAL_SELECTED_FILES = "${AppConstants.EXTRA_PREFIX}.ORIGINAL_SELECTED_FILES"
        const val EXTRA_CURRENT_FILE = "${AppConstants.EXTRA_PREFIX}.CURRENT_FILE"
        const val EXTRA_CURRENT_LIST = "${AppConstants.EXTRA_PREFIX}.CURRENT_LIST"
    }

    interface Callback {
        fun onMultiFilesPickerDialogResult(tag: String?, list: List<ServerFile>)
    }

    private lateinit var locationBar: AppBarLayout
    private lateinit var buttonBar: View
    private lateinit var recyclerView: BorderRecyclerView
    private lateinit var emptyView: View
    private lateinit var listContainer: View
    private lateinit var progressContainer: View
    private lateinit var selectedCountText: TextView
    private lateinit var backButton: View
    private lateinit var statusText: HtmlCompatTextView

    private val listAdapter: MultiFilesPickerAdapter = MultiFilesPickerAdapter()

    private lateinit var currentFile: ServerFile
    var rootPathLabel: String = ""

    private val selectedFilesLock: Any = Any()
    private val internalSelectedFiles: MutableList<ServerFile> = mutableListOf()
    private val internalOriginalSelectedFiles: MutableList<ServerFile> = mutableListOf()
    var selectedFiles: List<ServerFile>
        get() = Collections.unmodifiableList(internalSelectedFiles)
        set(value) {
            internalSelectedFiles.clear()
            internalSelectedFiles.addAll(value)
        }

    var currentList: List<ServerFile> = emptyList()
        private set

    private val adapterObserver = DefaultRVAdapterDataObserver {
        selectedCountText.text = getString(
                R.string.multi_picker_count_status_format,
                resources.getQuantityString(
                        R.plurals.folder_amount_text_format,
                        internalSelectedFiles.size, internalSelectedFiles.size)
        )
        selectedCountText.isEnabled = internalSelectedFiles.isNotEmpty()
        if (listAdapter.itemCount == 0) {
            locationBar.elevation = 0f
            emptyView.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            emptyView.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        currentFile = arguments?.getParcelable(EXTRA_CURRENT_FILE)!!
        internalSelectedFiles.clear()
        arguments?.getParcelableArrayList<ServerFile>(EXTRA_SELECTED_FILES)?.let {
            internalSelectedFiles.addAll(it)
        }

        internalOriginalSelectedFiles.clear()
        if (arguments?.containsKey(EXTRA_ORIGINAL_SELECTED_FILES) == true) {
            requireArguments().getParcelableArrayList<ServerFile>(EXTRA_ORIGINAL_SELECTED_FILES)?.let {
                internalOriginalSelectedFiles.addAll(it)
            }
        } else
            internalOriginalSelectedFiles.addAll(internalSelectedFiles)

        savedInstanceState?.let {
            currentFile = it.getParcelable(EXTRA_CURRENT_FILE) ?: currentFile
            currentList = it.getParcelableArrayList(EXTRA_CURRENT_LIST) ?: ArrayList()
            it.getParcelableArrayList<ServerFile>(EXTRA_SELECTED_FILES)?.let { savedSelected ->
                internalSelectedFiles.clear()
                internalSelectedFiles.addAll(savedSelected)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(EXTRA_CURRENT_FILE, currentFile)
        outState.putParcelableArrayList(EXTRA_CURRENT_LIST, ArrayList(currentList))
        outState.putParcelableArrayList(EXTRA_SELECTED_FILES, ArrayList(internalSelectedFiles))
        outState.putParcelableArrayList(EXTRA_ORIGINAL_SELECTED_FILES, ArrayList(internalOriginalSelectedFiles))
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.full_dialog_multi_files_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        locationBar = view.findViewById(R.id.location_bar)
        buttonBar = view.findViewById(R.id.button_bar)
        emptyView = view.findViewById(R.id.empty_view)
        listContainer = view.findViewById(R.id.list_container)
        progressContainer = view.findViewById(R.id.progress_container)
        recyclerView = view.findViewById(android.R.id.list)
        selectedCountText = view.findViewById(R.id.selected_count_text)
        statusText = view.findViewById(R.id.status_text_folders)
        backButton = view.findViewById(R.id.back_button)

        val listPaddingBottom = recyclerView.initialPaddingBottom
        buttonBar.doOnLayout {
            recyclerView.setInitialPadding(recyclerView.initialPaddingLeft, recyclerView.initialPaddingTop, recyclerView.initialPaddingRight, listPaddingBottom + it.measuredHeight)
        }

        selectedCountText.setOnClickListener {
            ViewListDialog.newInstance(internalSelectedFiles.map {
                it.relativePath
            }).show(childFragmentManager)
        }
        backButton.setOnClickListener {
            back()
        }

        view.findViewById<View>(R.id.positive_button).setOnClickListener {
            onPositiveButtonClick()
        }

        recyclerView.adapter = listAdapter
        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            locationBar.isRaised = !top
        }

        listAdapter.registerAdapterDataObserver(adapterObserver)
        listAdapter.listener = this

        if (savedInstanceState == null) {
            updateCurrentList()
        } else {
            updateAdapterDataOnly()
        }
    }

    private fun onSendResult(callback: Callback, tag: String?, list: List<ServerFile>) {
        callback.onMultiFilesPickerDialogResult(tag, list)
    }

    open fun checkResult(list: MutableList<ServerFile>): Boolean {
        return true
    }

    private fun onPositiveButtonClick() {
        val list = internalSelectedFiles
        if (!checkResult(list)) {
            return
        }
        if (parentFragment is Callback) {
            onSendResult(parentFragment as Callback, tag, list)
        } else if (activity is Callback) {
            onSendResult(activity as Callback, tag, list)
        }
        dismiss()
    }

    private fun onNegativeButtonClick() {
        if (internalOriginalSelectedFiles.containsAll(internalSelectedFiles) && internalSelectedFiles.containsAll(internalOriginalSelectedFiles)) {
            dismiss()
            return
        }

        activity?.let {
            AlertDialog.Builder(it)
                    .setTitle(R.string.exit_confirm_dialog_title)
                    .setMessage(R.string.exit_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        this@MultiFilesPickerDialog.dismiss()
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .show()
        }
    }

    final override fun onBackPressed(): Boolean {
        if (!back()) {
            onNegativeButtonClick()
        }
        return super.onBackPressed()
    }

    open fun canBack(file: ServerFile): Boolean {
        return file.relativePath.isNotEmpty()
    }

    private fun back(): Boolean {
        if (!canBack(currentFile)) {
            return false
        }
        currentFile = onBack(currentFile)
        updateCurrentList()
        return true
    }

    open fun onBack(file: ServerFile): ServerFile {
        return file.parent
    }

    final override fun onItemCheckBoxClick(item: SelectableServerFile) {
        onCheckBoxClick(item)
    }

    override fun onItemClick(item: SelectableServerFile) {
        if (item.isDirectory) {
            if (item.isBackItem) {
                back()
            } else {
                currentFile = item.serverFile
                updateCurrentList()
            }
        }
    }

    open fun isAllowedEnterSelectedDirectories(): Boolean {
        return false
    }

    open fun onCheckBoxClick(item: SelectableServerFile) {
        // TODO Tri-state
        if (!item.selected) {
            pick(item.serverFile)
        } else {
            unpick(item.serverFile)
        }
    }

    private fun listFilesInCurrentPath(): List<ServerFile> {
        return listFiles(currentFile)
    }

    abstract fun listFiles(file: ServerFile): List<ServerFile>

    private fun showProgressBar() {
        progressContainer.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressContainer.visibility = View.GONE
    }

    fun setPickState(file: ServerFile, newState: Boolean): Boolean {
        synchronized(selectedFilesLock) {
            return internalSetPickState(file, newState)
        }
    }

    private fun internalSetPickState(file: ServerFile?, newState: Boolean): Boolean {
        if (file == null) {
            return false
        }
        if (newState) {
            if (file in internalSelectedFiles) {
                return false
            }
            internalSelectedFiles.removeAll { file.isParentOf(it) }
            internalSelectedFiles.add(file)
            return true
        } else {
            if (file !in internalSelectedFiles) {
                if (!internalSelectedFiles.any { it.isParentOf(file) }) {
                    return false
                }
                if (!internalSetPickState(file.parent, false)) {
                    return false
                }
                for (item in currentList) {
                    if (item !in internalSelectedFiles) {
                        internalSelectedFiles.add(item)
                    }
                }
                return true
            }
            internalSelectedFiles.removeAll { file.isParentOf(it) }
            internalSelectedFiles.remove(file)
            return true
        }
    }

    fun pick(file: ServerFile) {
        launch {
            try {
                val res = withContext(Dispatchers.IO) {
                    setPickState(file, true)
                }
                if (res) {
                    updateAdapterDataOnly()
                }
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    fun unpick(file: ServerFile) {
        launch {
            try {
                val res = withContext(Dispatchers.IO) {
                    setPickState(file, false)
                }
                if (res) {
                    updateAdapterDataOnly()
                }
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    fun pickAll() {
        launch {
            try {
                withContext(Dispatchers.IO) {
                    for (item in currentList) {
                        if (createAdapterItem(item).selectable) {
                            setPickState(item, true)
                        }
                    }
                }
                updateAdapterDataOnly()
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    fun unpickAll() {
        launch {
            try {
                withContext(Dispatchers.IO) {
                    for (item in currentList) {
                        if (createAdapterItem(item).selectable) {
                            setPickState(item, false)
                        }
                    }
                }
                updateAdapterDataOnly()
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    private fun updateAdapterDataOnly() {
        val items = mutableListOf<SelectableServerFile>()
        /*if (currentPath.isNotEmpty()) {
            items += SelectableServerFile.BACK_ITEM
        }*/
        onUpdateCurrentList()
        items.addAll(currentList.map(this::createAdapterItem))
        listAdapter.updateDataWithDiffUtil(items)
    }

    private fun createAdapterItem(serverFile: ServerFile): SelectableServerFile {
        return SelectableServerFile(serverFile).apply {
            this.neverSelectable = !serverFile.isDirectory
            this.selectable = !internalSelectedFiles.any { it.isParentOf(serverFile) }
            this.selected = (serverFile in internalSelectedFiles) || !selectable
            this.allowEnter = !selected || isAllowedEnterSelectedDirectories()
            for (pickedFile in internalSelectedFiles) {
                if (serverFile.isParentOf(pickedFile)) {
                    if (pickedFile.isDirectory) {
                        this.selectedSubfolders++
                    } else {
                        this.selectedSubfiles++
                    }
                }
            }
            onCreateAdapterItem(this)
        }
    }

    private fun updateCurrentList() {
        launch {
            try {
                val list = withContext(Dispatchers.IO) {
                    listFilesInCurrentPath()
                }
                currentList = list
                onUpdateCurrentList()
                updateAdapterDataOnly()
                hideProgressBar()
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
                hideProgressBar()
            }
        }
    }

    @CallSuper
    open fun onUpdateCurrentList() {
        val html: String = if (currentFile.relativePath.isEmpty()) rootPathLabel else "$rootPathLabel<br>${currentFile.relativePath}"
        statusText.setHtmlText(html)
        backButton.visibility = if (currentFile.relativePath.isNotEmpty()) View.VISIBLE else View.GONE
        locationBar.isRaised = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() > 1
    }

    open fun onCreateAdapterItem(fileItem: SelectableServerFile) {

    }

    override fun onApplyTranslucentSystemBars() {
        val window = dialog!!.window

        window?.statusBarColor = Color.TRANSPARENT
        window?.navigationBarColor = Color.TRANSPARENT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            window?.navigationBarDividerColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                window?.isNavigationBarContrastEnforced = false
            }
        }
    }

    class ViewListDialog : AlertDialogFragment() {

        private lateinit var mMountDirs: List<String>

        private val message: CharSequence
            get() = if (mMountDirs.isEmpty()) {
                getString(R.string.detail_mount_dirs_view_list_message_empty)
            } else {
                val sb = StringBuilder()
                for (path in mMountDirs) {
                    sb.append(getString(R.string.detail_mount_dirs_view_list_item_format, path))
                }
                HtmlCompat.fromHtml(sb.toString(),
                        HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM
                                or HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE,
                        null,
                        SizeTagHandler.getInstance())
            }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            mMountDirs = requireArguments().getStringArrayList(AppConstants.EXTRA_DATA)!!
        }

        override fun onBuildAlertDialog(builder: AlertDialog.Builder,
                                        savedInstanceState: Bundle?) {
            builder.setTitle(R.string.multi_picker_view_list_button)
            builder.setMessage(message)
            builder.setPositiveButton(android.R.string.ok, null)
        }

        override fun onShow(dialog: AlertDialog) {
            val r = requireContext().resources
            val add = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2f, r.displayMetrics)
            dialog.findViewById<TextView>(android.R.id.message)?.setLineSpacing(add, 1f)
        }

        companion object {

            fun newInstance(dirs: List<String>): ViewListDialog {
                return ViewListDialog().also {
                    it.arguments = Bundle().apply {
                        putStringArrayList(AppConstants.EXTRA_DATA, ArrayList(dirs))
                    }
                }
            }

        }

    }

}