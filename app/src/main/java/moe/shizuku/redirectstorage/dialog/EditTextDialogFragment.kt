package moe.shizuku.redirectstorage.dialog

import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.databinding.EditTextDialogBinding

abstract class EditTextDialogFragment : AlertDialogFragment() {

    private lateinit var binding: EditTextDialogBinding

    val textField get() = binding.textField

    val editText get() = binding.textField.editText!!

    val helperText get() = binding.text2

    open val value: String?
        get() = editText.text.toString()

    override fun onGetContentViewLayoutResource(): Int {
        return R.layout.edit_text_dialog
    }

    @CallSuper
    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        builder.setPositiveButton(android.R.string.ok) { _, _ -> onDone() }
        builder.setNegativeButton(android.R.string.cancel) { _, _ -> onCancel() }
    }

    @CallSuper
    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        binding = EditTextDialogBinding.bind(contentView)
        editText.setOnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (positiveButton.isEnabled) {
                    positiveButton.performClick()
                }
                true
            } else {
                false
            }
        }
        setupValue(if (savedInstanceState != null) {
            savedInstanceState.getCharSequence(KEY_VALUE)
        } else {
            arguments?.getCharSequence(ARG_INITIAL_VALUE)
        })
    }

    @CallSuper
    override fun onShow(dialog: AlertDialog) {
        // Fix lost input focus
        /*editText.post {
            val imm = dialog.context.getSystemService(InputMethodManager::class.java)
            imm?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }*/
    }

    open fun onDone() {}

    open fun onCancel() {}

    fun setupValue(initialValue: CharSequence?) {
        editText.setText(initialValue)
        if (!TextUtils.isEmpty(initialValue)) {
            editText.setSelection(initialValue!!.length)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putCharSequence(KEY_VALUE, editText.text)
    }

    companion object {

        const val ARG_INITIAL_VALUE = AppConstants.EXTRA_PREFIX + ".initial_value"
        private const val KEY_VALUE = AppConstants.EXTRA_PREFIX + ".value"
    }
}