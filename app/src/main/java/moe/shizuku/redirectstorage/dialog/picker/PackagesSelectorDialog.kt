package moe.shizuku.redirectstorage.dialog.picker

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.core.view.doOnLayout
import androidx.core.view.isGone
import androidx.core.view.isVisible
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.PackagesSelectorItemAdapter
import moe.shizuku.redirectstorage.dialog.FullScreenDialogFragment
import moe.shizuku.redirectstorage.model.IPackageDescriptor
import moe.shizuku.redirectstorage.model.SelectableSimpleAppInfo
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.model.SimplePackageDescriptor
import moe.shizuku.redirectstorage.widget.DefaultRVAdapterDataObserver
import rikka.insets.*
import rikka.material.widget.AppBarLayout
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView

abstract class PackagesSelectorDialog : FullScreenDialogFragment() {

    companion object {

        @JvmStatic
        val EXTRA_SELECTED_PACKAGES = PackagesSelectorDialog::class.java.name +
                ".extra.SELECTED_PACKAGES"

        protected val PackagesSelectorDialog.KEY_STATE_SEARCH_KEYWORD: String
            get() {
                return javaClass.simpleName + ".state.SEARCH_WORD"
            }
    }

    interface Callback {
        fun onPackageSelectorDialogResult(tag: String?, list: List<SimpleAppInfo>)
    }

    private lateinit var searchEdit: EditText
    private lateinit var recyclerView: BorderRecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var statusText: TextView
    private lateinit var buttonBar: View

    private lateinit var adapter: PackagesSelectorItemAdapter

    /**
     * Control the max count of selected packages. When you set 0 or negative number,
     * it means users can select unlimited packages. When you set 1, it's single choice mode.
     */
    open val maxSelectedCount: Int
        get() = 0

    /**
     * Control the max count of selected packages. When you set 0 or negative number,
     * it means users can select any number of packages
     */
    open val minSelectedCount: Int
        get() = 0

    open val expandShared: Boolean
        get() = true

    private val textWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val keyword = if (s?.isEmpty() == true) {
                null
            } else {
                s.toString()
            }
            adapter.setKeyword(keyword)
        }
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)

        if (::searchEdit.isInitialized) {
            outState.putString(KEY_STATE_SEARCH_KEYWORD, searchEdit.text?.toString())
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_packages_selector_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val searchBar = view.findViewById<AppBarLayout>(R.id.search_bar)
        searchEdit = view.findViewById(R.id.search_edit)
        recyclerView = view.findViewById(android.R.id.list)
        progressBar = view.findViewById(android.R.id.progress)
        statusText = view.findViewById(R.id.selected_count_text)
        buttonBar = view.findViewById(R.id.button_bar)
        searchEdit.addTextChangedListener(textWatcher)

        val listPaddingBottom = recyclerView.initialPaddingBottom
        buttonBar.doOnLayout {
            recyclerView.setInitialPadding(recyclerView.initialPaddingLeft, recyclerView.initialPaddingTop, recyclerView.initialPaddingRight, listPaddingBottom + it.measuredHeight)
        }

        adapter = PackagesSelectorItemAdapter(maxSelectedCount)
        adapter.onRestoreInstanceState(savedInstanceState)
        adapter.registerAdapterDataObserver(DefaultRVAdapterDataObserver {
            if (maxSelectedCount != 1) {
                statusText.text = resources.getQuantityString(
                        R.plurals.packages_selector_dialog_status_format_for_multi,
                        adapter.getCheckedItems().size,
                        adapter.getCheckedItems().size
                )
            }
        })
        recyclerView.adapter = adapter
        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            searchBar.isRaised = !top
        }

        view.findViewById<View>(R.id.positive_button).setOnClickListener {
            onPositiveButtonClick()
        }

        if (maxSelectedCount == 1) {
            view.findViewById<View>(R.id.select_all_button).visibility = View.GONE
            statusText.visibility = View.GONE
        } else {
            view.findViewById<View>(R.id.select_all_button).setOnClickListener {
                adapter.selectAll()
            }
        }

        if (savedInstanceState == null) {
            val selectedPackages: List<SimplePackageDescriptor>? =
                    arguments?.getParcelableArrayList(EXTRA_SELECTED_PACKAGES)
            internalLoadListItems(selectedPackages)
        } else {
            val savedKeyword = savedInstanceState.getString(KEY_STATE_SEARCH_KEYWORD)
            adapter.setKeyword(savedKeyword)
            searchEdit.setText(savedKeyword)
            progressBar.isGone = true
        }
    }

    open fun onSendResult(callback: Callback, tag: String?, list: List<SimpleAppInfo>) {
        callback.onPackageSelectorDialogResult(tag, list)
    }

    private fun onPositiveButtonClick() {
        val checkedItems = adapter.getCheckedItems()
        if (minSelectedCount > 0) {
            if (checkedItems.size < minSelectedCount) {
                Toast.makeText(context, resources.getQuantityString(
                        R.plurals.packages_selector_dialog_toast_min_selected_count_limit,
                        minSelectedCount, minSelectedCount
                ), Toast.LENGTH_SHORT).show()
                return
            }
        }
        val list = ArrayList(checkedItems)

        if (parentFragment is Callback) {
            onSendResult(parentFragment as Callback, tag, list)
        } else if (activity is Callback) {
            onSendResult(activity as Callback, tag, list)
        }
        dismiss()
    }

    private fun internalLoadListItems(selectedPackages: List<IPackageDescriptor>? = null) {
        progressBar.isVisible = true
        launch {
            try {
                val res = withContext(Dispatchers.IO) {
                    loadAllAvailablePackages(selectedPackages).map { createAdapterItem(it) }
                }
                adapter.setAllData(res)
                selectedPackages?.let {
                    adapter.setCheckedPackages(it)
                }
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
            }
            progressBar.isGone = true
        }
    }

    abstract fun loadAllAvailablePackages(selectedPackages: List<IPackageDescriptor>?): List<SimpleAppInfo>

    private fun createAdapterItem(item: SimpleAppInfo): SelectableSimpleAppInfo {
        return SelectableSimpleAppInfo(item).apply {
            onCreateAdapterItem(this)
        }
    }

    open fun onCreateAdapterItem(it: SelectableSimpleAppInfo) {

    }

    override fun onApplyTranslucentSystemBars() {
        val window = dialog!!.window

        window?.statusBarColor = Color.TRANSPARENT
        window?.navigationBarColor = Color.TRANSPARENT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            window?.navigationBarDividerColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                window?.isNavigationBarContrastEnforced = false
            }
        }
    }
}