package moe.shizuku.redirectstorage.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.AppBarDialogFragment;
import moe.shizuku.redirectstorage.widget.CheckedLinearLayout;
import rikka.material.app.AppBar;

public class MediaPathSelectDialog extends AppBarDialogFragment
        implements View.OnClickListener {

    private static final String EXTRA_SELECTED = BuildConfig.APPLICATION_ID + ".extra.SELECTED";
    private static final String EXTRA_ORIGINAL_SELECTED = BuildConfig.APPLICATION_ID + ".extra.ORIGINAL_SELECTED";
    private static final String EXTRA_TEXT = BuildConfig.APPLICATION_ID + ".extra.TEXT";
    private static final String EXTRA_ORIGINAL_TEXT = BuildConfig.APPLICATION_ID + ".extra.ORIGINAL_TEXT";

    public static MediaPathSelectDialog newInstance() {
        Bundle args = new Bundle();
        MediaPathSelectDialog fragment = new MediaPathSelectDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private int mSelected, mOriginalSelected;
    private String mSavedMediaPath, mOriginalSavedMediaPath;

    private String mObtainedMediaPath;

    private CheckedLinearLayout mAuto, mManual;
    private TextView mObtainedText;
    private EditText mEditText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasDialogOptionsMenu(true);

        SRManager sm = SRManager.create();
        if (sm != null) {
            try {
                mSavedMediaPath = sm.getSavedMediaPath();
                mObtainedMediaPath = sm.getDebugInfo(0).mediaPathFromNative;
            } catch (Exception e) {
                Toast.makeText(requireActivity(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                dismiss();
            }
        } else {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            dismiss();
        }

        if (savedInstanceState != null) {
            mSelected = savedInstanceState.getInt(EXTRA_SELECTED, 0);
            mOriginalSelected = savedInstanceState.getInt(EXTRA_ORIGINAL_SELECTED, 0);
            mSavedMediaPath = savedInstanceState.getString(EXTRA_TEXT);
            mOriginalSavedMediaPath = savedInstanceState.getString(EXTRA_ORIGINAL_TEXT, "");
        } else {
            mSelected = mOriginalSelected = mSavedMediaPath == null ? 0 : 1;
            mOriginalSavedMediaPath = mSavedMediaPath == null ? "" : mSavedMediaPath;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(EXTRA_SELECTED, mSelected);
        outState.putInt(EXTRA_ORIGINAL_SELECTED, mOriginalSelected);
        outState.putString(EXTRA_TEXT, mEditText.getText().toString());
        outState.putString(EXTRA_ORIGINAL_TEXT, mOriginalSavedMediaPath);
    }

    @Override
    public boolean onBackPressed() {
        if (mSelected != mOriginalSelected || (!Objects.equals(mEditText.getText().toString(), mOriginalSavedMediaPath))) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.exit_confirm_dialog_title)
                    .setMessage(R.string.exit_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        dismiss();
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setNeutralButton(R.string.exit_confirm_dialog_button_save, (dialog, which) -> {
                        onSaveClick();
                    })
                    .show();
            return false;
        }
        return super.onBackPressed();
    }

    @Override
    public void onCreateDialogOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.dialog_save, menu);
    }

    @Override
    public boolean onDialogOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            onSaveClick();
        }
        return super.onDialogOptionsItemSelected(item);
    }

    private void onSaveClick() {
        SRManager sm = SRManager.create();
        if (sm == null) {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mSelected == 0) {
            try {
                sm.setSavedMediaPath(null);
            } catch (Throwable tr) {
                Toast.makeText(requireActivity(), getString(R.string.toast_failed, Objects.toString(tr, "unknown")), Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            try {
                sm.setSavedMediaPath(mEditText.getText().toString());
            } catch (Throwable tr) {
                if ("invalid path".equals(tr.getMessage())) {
                    mEditText.setError(getString(R.string.dialog_media_path_invalid_path));
                } else {
                    Toast.makeText(requireActivity(), getString(R.string.toast_failed, Objects.toString(tr, "unknown")), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        dismiss();
    }

    @Nullable
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_media_path, container, false);
    }

    @Override
    public void onContentViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mAuto = view.findViewById(android.R.id.button1);
        mManual = view.findViewById(android.R.id.button2);
        mObtainedText = view.findViewById(android.R.id.text1);
        mEditText = view.findViewById(android.R.id.text2);

        mObtainedText.setText(mObtainedMediaPath);
        mEditText.setText(mSavedMediaPath);

        mAuto.setOnClickListener(this);
        mManual.setOnClickListener(this);
        mEditText.setOnClickListener(v -> onSelectedChanged(android.R.id.button2));

        switch (mSelected) {
            case 0:
                onSelectedChanged(android.R.id.button1);
                break;
            case 1:
                onSelectedChanged(android.R.id.button2);
                break;
        }
    }

    @Override
    public void onAppBarCreated(@NotNull AppBar appBar, @Nullable Bundle savedInstanceState) {
        appBar.setTitle(R.string.settings_media_path_title);
        appBar.setDisplayHomeAsUpEnabled(true);
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp);
    }

    private void onSelectedChanged(int id) {
        switch (id) {
            case android.R.id.button1: {
                mSelected = 0;

                mAuto.setChecked(true);
                mManual.setChecked(false);

                InputMethodManager imm = requireContext().getSystemService(InputMethodManager.class);
                if (imm != null && imm.isActive())
                    imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);

                break;
            }
            case android.R.id.button2: {
                mSelected = 1;

                mAuto.setChecked(false);
                mManual.setChecked(true);
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        onSelectedChanged(v.getId());
    }
}
