package moe.shizuku.redirectstorage.dialog;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.functions.BiConsumer;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.JobKt;
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver;
import rikka.core.compat.Optional;

/**
 * Created by Fung Gwo on 2018/1/24.
 */

public class AlertDialogFragment extends DialogFragment implements CoroutineScope {

    public static final String EXTRA_INTENT_TYPE = AlertDialogFragment.class.getName() + ".INTENT_TYPE";
    public static final int INTENT_TYPE_LOCAL_BROADCAST = 0;
    public static final int INTENT_TYPE_ACTIVITY = 1;

    protected static final String INTERNAL_BUILDER_ARGS = AlertDialogFragment.class.getName() + ".builder_args";

    private AlertDialog mDialog;
    private View mContentView = null;

    private Job mJob;

    private final List<BroadcastReceiver> mLocalBroadcastReceivers = new ArrayList<>();

    public LocalBroadcastManager getLocalBroadcastManager() {
        return LocalBroadcastManager.getInstance(requireContext());
    }

    public void registerLocalBroadcastReceiver(@NonNull ActionBroadcastReceiver receiver) {
        getLocalBroadcastManager().registerReceiver(receiver, new IntentFilter(receiver.getAction()));
        mLocalBroadcastReceivers.add(receiver);
    }

    public void registerLocalBroadcastReceiver(@NonNull BroadcastReceiver receiver, @NonNull IntentFilter intentFilter) {
        getLocalBroadcastManager().registerReceiver(receiver, intentFilter);
        mLocalBroadcastReceivers.add(receiver);
    }

    public void registerLocalBroadcastReceiver(@NonNull BiConsumer<Context, Intent> broadcastConsumer, @NonNull IntentFilter intentFilter) {
        registerLocalBroadcastReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    broadcastConsumer.accept(context, intent);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }, intentFilter);
    }

    public void unregisterAllLocalBroadcastReceivers() {
        final LocalBroadcastManager lbm = getLocalBroadcastManager();
        for (BroadcastReceiver registered : mLocalBroadcastReceivers) {
            lbm.unregisterReceiver(registered);
        }
        mLocalBroadcastReceivers.clear();
    }

    @NotNull
    @Override
    public CoroutineContext getCoroutineContext() {
        return Dispatchers.getMain().plus(mJob);
    }

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mJob = JobKt.Job(null);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterAllLocalBroadcastReceivers();
        mJob.cancel(null);
    }

    @Nullable
    @Override
    @Deprecated
    public final View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Do not override this method. Use onBuildAlertDialog to build alert dialog instead.
        return null;
    }

    public AlertDialog.Builder onCreateAlertDialogBuilder() {
        return new AlertDialog.Builder(getActivity());
    }

    private void launchIntent(Intent intent) {
        if (intent != null) {
            int type = intent.getIntExtra(EXTRA_INTENT_TYPE, INTENT_TYPE_LOCAL_BROADCAST);
            intent.removeExtra(EXTRA_INTENT_TYPE);
            switch (type) {
                case INTENT_TYPE_LOCAL_BROADCAST: {
                    getLocalBroadcastManager().sendBroadcast(intent);
                    break;
                }
                case INTENT_TYPE_ACTIVITY: {
                    startActivity(intent);
                    break;
                }
            }
        }
    }

    public LayoutInflater onCreateLayoutInflater(@NonNull Context context) {
        return LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public final Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = onCreateAlertDialogBuilder();

        final Bundle args = getArguments();
        if (args != null && args.containsKey(INTERNAL_BUILDER_ARGS)) {
            final Builder dialogBuilder = Objects.requireNonNull(args.getParcelable(INTERNAL_BUILDER_ARGS));
            builder.setTitle(dialogBuilder.title);
            builder.setMessage(dialogBuilder.message);
            if (dialogBuilder.positiveButtonTitle != null) {
                builder.setPositiveButton(dialogBuilder.positiveButtonTitle, (dialog, which) -> {
                    launchIntent(dialogBuilder.positiveButtonBroadcastIntent);
                });
            }
            if (dialogBuilder.negativeButtonTitle != null) {
                builder.setNegativeButton(dialogBuilder.negativeButtonTitle, (dialog, which) -> {
                    launchIntent(dialogBuilder.negativeButtonBroadcastIntent);
                });
            }
            if (dialogBuilder.neutralButtonTitle != null) {
                builder.setNeutralButton(dialogBuilder.neutralButtonTitle, (dialog, which) -> {
                    launchIntent(dialogBuilder.neutralButtonBroadcastIntent);
                });
            }
        }

        onBuildAlertDialog(builder, savedInstanceState);

        int contentViewLayoutRes = onGetContentViewLayoutResource();
        if (contentViewLayoutRes > 0) {
            mContentView = onCreateLayoutInflater(requireActivity()).inflate(contentViewLayoutRes, null);
            builder.setView(mContentView);
        }

        mDialog = builder.create();
        mDialog.setOnShowListener(dialog -> onShow((AlertDialog) dialog));
        onAlertDialogCreated(mDialog, mContentView, savedInstanceState);
        return mDialog;
    }

    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder, @Nullable Bundle savedInstanceState) {

    }

    public void onAlertDialogCreated(@NonNull AlertDialog dialog, View contentView, @Nullable Bundle savedInstanceState) {

    }

    public void onShow(AlertDialog dialog) {
    }

    /**
     * Get layout resource id of content view. If you don't want to set custom view, don't override it.
     *
     * @return Layout resource (or 0 for no content view)
     */
    @LayoutRes
    public int onGetContentViewLayoutResource() {
        return 0;
    }

    @Override
    public final AlertDialog getDialog() {
        // Do not use super.getDialog() . It may returns null pointer before dialog has already created.
        return mDialog;
    }

    @Nullable
    public final Button getButton(int whichButton) {
        return getDialog().getButton(whichButton);
    }

    public final Button getPositiveButton() {
        return getButton(DialogInterface.BUTTON_POSITIVE);
    }

    public final Button getNegativeButton() {
        return getButton(DialogInterface.BUTTON_NEGATIVE);
    }

    public final Button getNeutralButton() {
        return getButton(DialogInterface.BUTTON_NEUTRAL);
    }

    @Nullable
    public final View getContentView() {
        return mContentView;
    }

    public void setPositiveButtonEnabled(boolean enabled) {
        getPositiveButton().setEnabled(enabled);
    }

    public void show(FragmentManager fragmentManager) {
        if (fragmentManager.isStateSaved())
            return;

        show(fragmentManager, getClass().getSimpleName());
    }

    public static class Builder implements Parcelable {

        private CharSequence title = null;
        private CharSequence message = null;
        private CharSequence positiveButtonTitle = null;
        private CharSequence negativeButtonTitle = null;
        private CharSequence neutralButtonTitle = null;
        private Intent positiveButtonBroadcastIntent = null;
        private Intent negativeButtonBroadcastIntent = null;
        private Intent neutralButtonBroadcastIntent = null;

        public Builder() {

        }

        protected Builder(Parcel in) {
            title = in.readString();
            message = in.readString();
            positiveButtonTitle = in.readString();
            negativeButtonTitle = in.readString();
            neutralButtonTitle = in.readString();
            positiveButtonBroadcastIntent = in.readParcelable(Intent.class.getClassLoader());
            negativeButtonBroadcastIntent = in.readParcelable(Intent.class.getClassLoader());
            neutralButtonBroadcastIntent = in.readParcelable(Intent.class.getClassLoader());
        }

        public Builder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setPositiveButton(CharSequence title, Intent intent) {
            this.positiveButtonTitle = title;
            this.positiveButtonBroadcastIntent = intent;
            return this;
        }

        public Builder setNegativeButton(CharSequence title, Intent intent) {
            this.negativeButtonTitle = title;
            this.negativeButtonBroadcastIntent = intent;
            return this;
        }

        public Builder setNeutralButton(CharSequence title, Intent intent) {
            this.neutralButtonTitle = title;
            this.neutralButtonBroadcastIntent = intent;
            return this;
        }

        public AlertDialogFragment build() {
            final AlertDialogFragment fragment = new AlertDialogFragment();
            final Bundle args = new Bundle();
            args.putParcelable(INTERNAL_BUILDER_ARGS, this);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Optional.of(() -> title.toString()).get());
            dest.writeString(Optional.of(() -> message.toString()).get());
            dest.writeString(Optional.of(() -> positiveButtonTitle.toString()).get());
            dest.writeString(Optional.of(() -> negativeButtonTitle.toString()).get());
            dest.writeString(Optional.of(() -> neutralButtonTitle.toString()).get());
            dest.writeParcelable(positiveButtonBroadcastIntent, flags);
            dest.writeParcelable(negativeButtonBroadcastIntent, flags);
            dest.writeParcelable(neutralButtonBroadcastIntent, flags);
        }

        public static final Creator<Builder> CREATOR = new Creator<Builder>() {
            @Override
            public Builder createFromParcel(Parcel in) {
                return new Builder(in);
            }

            @Override
            public Builder[] newArray(int size) {
                return new Builder[size];
            }
        };
    }
}
