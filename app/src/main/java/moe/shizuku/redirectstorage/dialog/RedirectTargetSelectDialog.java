package moe.shizuku.redirectstorage.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.IFileMoveListener;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.adapter.RecyclerViewAdapterHelper;
import moe.shizuku.redirectstorage.app.AppBarDialogFragment;
import moe.shizuku.redirectstorage.event.Events;
import moe.shizuku.redirectstorage.event.GlobalConfigChangedEvent;
import moe.shizuku.redirectstorage.event.RemoteRequestHandler;
import moe.shizuku.redirectstorage.event.ToastErrorHandler;
import rikka.html.widget.HtmlCompatTextView;
import rikka.material.app.AppBar;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;
import rikka.recyclerview.RecyclerViewKt;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_SHARED_USER_ID;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID;

public class RedirectTargetSelectDialog extends AppBarDialogFragment implements GlobalConfigChangedEvent {

    public static RedirectTargetSelectDialog newInstance(String packageName, String sharedUserId, int userId) {
        Bundle args = new Bundle();
        args.putString(EXTRA_PACKAGE_NAME, packageName);
        args.putString(EXTRA_SHARED_USER_ID, sharedUserId);
        args.putInt(EXTRA_USER_ID, userId);
        RedirectTargetSelectDialog fragment = new RedirectTargetSelectDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static RedirectTargetSelectDialog newInstance() {
        Bundle args = new Bundle();
        RedirectTargetSelectDialog fragment = new RedirectTargetSelectDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private static int valueToInt(String value) {
        if (value == null) {
            return FOLLOW_DEFAULT;
        }

        switch (value) {
            case Constants.REDIRECT_TARGET_DATA:
                return DATA;
            case Constants.REDIRECT_TARGET_CACHE:
                return CACHE;
            /*case Constants.REDIRECT_TARGET_SANDBOX:
                return SANDBOX;*/
            default:
                return FOLLOW_DEFAULT;
        }
    }

    private static final String HELP_ID = "redirect_target";

    private static final String KEY_CURRENT = "current";
    private static final String KEY_IN_PROGRESS = "in_progress";
    private static final String KEY_PROGRESS = "progress";
    private static final String KEY_MAX = "max";

    private static final int FOLLOW_DEFAULT = 0;
    private static final int DATA = 1;
    private static final int CACHE = 2;
    //private static final int SANDBOX = 3;

    private Adapter mAdapter;

    private boolean mGlobally;
    private String mPackageName;
    private String mSharedUserId;
    private int mUserId;

    private int mDefault;
    private int mSelected;

    private View mContentContainer;
    private View mProgressContainer;

    private ProgressBar mProgressBar;
    private TextView mProgressText;

    private boolean mInProgress;

    private class UpdateProgressRunnable implements Runnable {
        private int progress;
        private int max;

        @Override
        public void run() {
            mProgressBar.setMax(max);
            mProgressBar.setProgress(progress);
            mProgressText.setText(requireContext().getString(R.string.progress, progress, max));

            onProgressUpdated();
        }
    }

    private UpdateProgressRunnable mUpdateProgressRunnable = new UpdateProgressRunnable();

    private IFileMoveListener mFileMoveListener = new IFileMoveListener.Stub() {

        @Override
        public void onProgress(String packageName, int userId, int progress, int max) {
            // TODO check packageName
            if (mProgressBar != null && mProgressText != null) {
                mProgressBar.setIndeterminate(false);

                mUpdateProgressRunnable.max = max;
                mUpdateProgressRunnable.progress = progress;

                mProgressText.post(mUpdateProgressRunnable);
            }
        }
    };

    private void onProgressUpdated() {
        if (mProgressBar.getProgress() == mProgressBar.getMax()) {
            /*getDialog().getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            getDialog().getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);*/
            /*invalidateOptionsMenu();
            if (getDialog() != null) {
                getDialog().setCancelable(true);
                getDialog().setCanceledOnTouchOutside(true);
            }*/
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasDialogOptionsMenu(true);

        Bundle args = getArguments();
        if (args != null) {
            mGlobally = !args.containsKey(EXTRA_PACKAGE_NAME);

            if (!mGlobally) {
                mPackageName = args.getString(EXTRA_PACKAGE_NAME);
                mSharedUserId = args.getString(EXTRA_SHARED_USER_ID);
                mUserId = args.getInt(AppConstants.EXTRA_USER_ID);

                Events.registerGlobalConfigChangedEvent(this);
            }
        } else {
            dismiss();
            return;
        }

        if (savedInstanceState != null) {
            mSelected = savedInstanceState.getInt(KEY_CURRENT);
            mInProgress = savedInstanceState.getBoolean(KEY_IN_PROGRESS);
        }

        SRManager sm = SRManager.create();
        if (sm != null) {
            try {
                if (sm.isOngoingFileMoveExists()) {
                    Toast.makeText(requireContext(), R.string.toast_file_move_exists, Toast.LENGTH_SHORT).show();
                    dismiss();
                    return;
                }

                mDefault = valueToInt(sm.getDefaultRedirectTarget());
                if (mGlobally) {
                    mSelected = mDefault;
                } else {
                    mSelected = valueToInt(sm.getRedirectTargetForPackage(mPackageName, mUserId, true, false));
                }
            } catch (Exception e) {
                Toast.makeText(requireActivity(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                dismiss();
            }
        } else {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Events.unregisterGlobalConfigChangedEvent(this);

        SRManager sm = SRManager.create();
        if (sm != null) {
            try {
                sm.unregisterFileMoveListener(mFileMoveListener);
            } catch (RemoteException e) {
                // should never happened
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_CURRENT, mSelected);
        outState.putBoolean(KEY_IN_PROGRESS, mInProgress);

        if (!mProgressBar.isIndeterminate()) {
            outState.putInt(KEY_MAX, mProgressBar.getMax());
            outState.putInt(KEY_PROGRESS, mProgressBar.getProgress());
        }
    }

    @Override
    public boolean onBackPressed() {
        /*if (mountDirsConfig != originalMountDirsConfig) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.editor_confirm_dialog_title)
                    .setMessage(R.string.editor_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        dismiss();
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();
            return false;
        }*/
        return super.onBackPressed();
    }

    @Override
    public void onCreateDialogOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.dialog_save, menu);
    }

    @Override
    public void onPrepareDialogOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.action_save).setVisible(!mInProgress);
    }

    @Override
    public boolean onDialogOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            onSaveClick();
            return true;
        }
        return super.onDialogOptionsItemSelected(item);
    }

    private void onSaveClick() {
        Dialog dialog = getDialog();
        if (dialog == null) {
            return;
        }
        if (!mInProgress) {
            if (onClick()) {
                mInProgress = true;

                /*dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(View.GONE);
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setText(R.string.dialog_hide);
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener((v1 -> dismiss()));*/

                invalidateDialogOptionsMenu();

                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);

                mContentContainer.setVisibility(View.GONE);
                mProgressContainer.setVisibility(View.VISIBLE);
            } else {
                dismiss();
            }
        } else {
            dismiss();
        }
    }

    @Override
    public void onDefaultRedirectTargetChanged(@NonNull String redirectTarget) {
        if (!mGlobally) {
            mDefault = valueToInt(redirectTarget);
            mAdapter.notifyItemChanged(0, redirectTarget);
        }
    }

    private String getSelectedString() {
        switch (mSelected) {
            case DATA:
                return Constants.REDIRECT_TARGET_DATA;
            case CACHE:
                return Constants.REDIRECT_TARGET_CACHE;
            /*case SANDBOX:
                return Constants.REDIRECT_TARGET_SANDBOX;*/
            case FOLLOW_DEFAULT:
            default:
                return null;
        }
    }

    private boolean onClick() {
        SRManager sm = SRManager.create();
        if (sm == null) {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            return false;
        }

        String path = getSelectedString();
        if (mGlobally && path == null) {
            return false;
        }
        boolean showDialog = false;
        try {
            try {
                if (mGlobally) {
                    showDialog = !Objects.equals(path, sm.getDefaultRedirectTarget());
                    showDialog &= RemoteRequestHandler.setDefaultRedirectTarget(path, new ToastErrorHandler(requireActivity()));
                } else {
                    showDialog = !Objects.equals(path, sm.getRedirectTargetForPackage(mPackageName, mUserId, true, false));
                    showDialog = RemoteRequestHandler.setRedirectTargetForPackage(path, mPackageName, mSharedUserId, mUserId, new ToastErrorHandler(requireActivity()));
                }
                if (showDialog) {
                    sm.registerFileMoveListener(mFileMoveListener);
                }
            } catch (Exception e) {
                Toast.makeText(requireActivity(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();

            Toast.makeText(requireContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return showDialog;
    }

    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_redirect_target, container, false);
    }

    @Override
    public void onContentViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mContentContainer = view.findViewById(R.id.dialog_content);
        mProgressContainer = view.findViewById(R.id.dialog_progress);
        mProgressBar = view.findViewById(android.R.id.progress);
        mProgressText = view.findViewById(android.R.id.text1);

        if (mInProgress) {
            mContentContainer.setVisibility(View.GONE);
            mProgressContainer.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressContainer.setVisibility(View.GONE);
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_MAX)) {
            mProgressBar.setMax(savedInstanceState.getInt(KEY_MAX));
            mProgressBar.setProgress(savedInstanceState.getInt(KEY_PROGRESS));

            onProgressUpdated();
        }

        RecyclerView recyclerView = view.findViewById(android.R.id.list);
        mAdapter = new Adapter();
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        RecyclerViewKt.fixEdgeEffect(recyclerView, true, true);
    }

    @Override
    public void onAppBarCreated(@NotNull AppBar appBar, @Nullable Bundle savedInstanceState) {
        appBar.setTitle(mGlobally ? R.string.settings_redirect_target : R.string.detail_redirect_target_title);
        appBar.setDisplayHomeAsUpEnabled(true);
        if (getParentFragment() == null) {
            appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp);
        }
    }

    private class Adapter extends IdBasedRecyclerViewAdapter {

        Adapter() {
            super();

            setHasStableIds(true);

            BaseViewHolder.Creator<Integer> creator = (inflater, parent) -> new ViewHolder(inflater.inflate(R.layout.radio_for_redirect_target, parent, false));
            if (!mGlobally) {
                addItem(creator, FOLLOW_DEFAULT, FOLLOW_DEFAULT);
                RecyclerViewAdapterHelper.addInformation(this, R.string.dialog_redirect_target_edit_default_help, 10);
                RecyclerViewAdapterHelper.addDivider2(this, 11);
            }
            addItem(creator, DATA, DATA);
            addItem(creator, CACHE, CACHE);
        }
    }

    private class ViewHolder extends BaseViewHolder<Integer> {

        private final Checkable radio;
        private final TextView title;
        private final HtmlCompatTextView summary;
        private final HtmlCompatTextView text;
        private final View button1;
        private final ImageView button2;
        private final View divider;

        ViewHolder(View itemView) {
            super(itemView);

            radio = itemView.findViewById(android.R.id.button3);
            title = itemView.findViewById(android.R.id.title);
            summary = itemView.findViewById(android.R.id.summary);
            text = itemView.findViewById(android.R.id.text1);
            button1 = itemView.findViewById(android.R.id.button1);
            button2 = itemView.findViewById(android.R.id.button2);
            divider = itemView.findViewById(R.id.divider);

            button1.setOnClickListener(view -> {
                mSelected = getData();
                getAdapter().notifyItemRangeChanged(0, getAdapter().getItemCount(), getData());
            });
            button2.setOnClickListener(view -> RedirectTargetSelectDialog.newInstance().show(getChildFragmentManager(), "123123"));
        }

        @Override
        public Adapter getAdapter() {
            return (Adapter) super.getAdapter();
        }

        @Override
        public void onBind() {
            Context context = itemView.getContext();
            switch (getData()) {
                case FOLLOW_DEFAULT: {
                    String targetName = "";
                    switch (mDefault) {
                        case DATA:
                            targetName = context.getString(R.string.redirect_target_data_title);
                            break;
                        case CACHE:
                            targetName = context.getString(R.string.redirect_target_cache_title);
                            break;
                        /*case SANDBOX:
                            summary.setText(R.string.redirect_target_sandbox_title);
                            break;*/
                        default:
                            break;
                    }
                    summary.setText(targetName);
                    title.setText(context.getString(R.string.redirect_target_default_title));
                    text.setVisibility(View.GONE);

                    setButtonVisible(false);
                    break;
                }
                case DATA: {
                    title.setText(R.string.redirect_target_data_title);
                    summary.setHtmlText(context.getString(R.string.redirect_target_data));
                    summary.setVisibility(View.VISIBLE);
                    text.setHtmlText(getString(R.string.redirect_target_data_summary));
                    text.setVisibility(View.VISIBLE);
                    itemView.setEnabled(true);
                    setButtonVisible(false);
                    break;
                }
                case CACHE: {
                    title.setText(R.string.redirect_target_cache_title);
                    summary.setHtmlText(context.getString(R.string.redirect_target_cache));
                    summary.setVisibility(View.VISIBLE);
                    text.setHtmlText(getString(R.string.redirect_target_cache_summary));
                    text.setVisibility(View.VISIBLE);
                    itemView.setEnabled(true);
                    setButtonVisible(false);
                    break;
                }/* case SANDBOX: {
                    title.setText(R.string.redirect_target_sandbox_title);
                    summary.setHtmlText(context.getString(R.string.redirect_target_sandbox));
                    summary.setVisibility(View.VISIBLE);
                    text.setHtmlText(getString(R.string.redirect_target_sandbox_summary));
                    text.setVisibility(View.VISIBLE);
                    itemView.setEnabled(BuildUtils.isQ() && BuildUtils.isIsolatedStorageEnabled());
                    break;
                }*/
            }

            radio.setChecked(getData() == mSelected);
        }

        @Override
        public void onBind(@NonNull List<Object> payloads) {
            for (Object payload : payloads) {
                if (payload instanceof String) {
                    onBind();
                } else {
                    radio.setChecked(getData() == mSelected);
                }
            }
        }

        private void setButtonVisible(boolean visible) {
            if (visible) {
                button2.setVisibility(View.VISIBLE);
                divider.setVisibility(View.VISIBLE);
            } else {
                button2.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
            }
        }
    }
}
