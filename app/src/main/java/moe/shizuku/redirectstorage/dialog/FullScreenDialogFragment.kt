package moe.shizuku.redirectstorage.dialog

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.CallSuper
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.reactivex.functions.BiConsumer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.BaseDialogFragment
import moe.shizuku.redirectstorage.component.ComponentActivity
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver
import moe.shizuku.redirectstorage.utils.ThemeHelper
import java.util.*
import kotlin.coroutines.CoroutineContext

open class FullScreenDialogFragment : BaseDialogFragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext get() = Dispatchers.Main + lifeJob

    private lateinit var lifeJob: Job

    private val mLocalBroadcastReceivers = ArrayList<BroadcastReceiver>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is ComponentActivity) {
            setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog_FullScreen_NoAnimation)
        } else {
            setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog_FullScreen)
        }
        lifeJob = SupervisorJob()
    }

    override fun onGetLayoutInflater(savedInstanceState: Bundle?): LayoutInflater {
        val layoutInflater = super.onGetLayoutInflater(savedInstanceState)
        layoutInflater.context.theme.applyStyle(ThemeHelper.getThemeStyleRes(context), true)
        return layoutInflater
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }

    val localBroadcastManager: LocalBroadcastManager
        get() = LocalBroadcastManager.getInstance(requireContext())

    fun registerLocalBroadcastReceiver(receiver: ActionBroadcastReceiver) {
        localBroadcastManager.registerReceiver(receiver, IntentFilter(receiver.action))
        mLocalBroadcastReceivers.add(receiver)
    }

    fun registerLocalBroadcastReceiver(receiver: BroadcastReceiver, intentFilter: IntentFilter) {
        localBroadcastManager.registerReceiver(receiver, intentFilter)
        mLocalBroadcastReceivers.add(receiver)
    }

    fun registerLocalBroadcastReceiver(broadcastConsumer: BiConsumer<Context, Intent>, intentFilter: IntentFilter) {
        registerLocalBroadcastReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    broadcastConsumer.accept(context, intent)
                } catch (e: Exception) {
                    throw RuntimeException(e)
                }

            }
        }, intentFilter)
    }

    fun unregisterAllLocalBroadcastReceivers() {
        val lbm = localBroadcastManager
        for (registered in mLocalBroadcastReceivers) {
            lbm.unregisterReceiver(registered)
        }
        mLocalBroadcastReceivers.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterAllLocalBroadcastReceivers()
        lifeJob.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

}