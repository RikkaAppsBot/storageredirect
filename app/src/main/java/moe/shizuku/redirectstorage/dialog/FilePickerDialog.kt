package moe.shizuku.redirectstorage.dialog

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.FilePickerListAdapter
import moe.shizuku.redirectstorage.model.ServerFile
import rikka.html.widget.HtmlCompatTextView
import rikka.recyclerview.fixEdgeEffect
import java.util.*

/**
 * Created by fytho on 2018/8/11.
 */
abstract class FilePickerDialog : AlertDialogFragment(), FilePickerListAdapter.Callback {

    private val adapter: FilePickerListAdapter by lazy { FilePickerListAdapter() }
    private lateinit var emptyText: TextView
    private lateinit var summaryText: TextView
    private lateinit var infoText: HtmlCompatTextView
    private lateinit var listView: RecyclerView
    private var shouldBlockClick = false
    var currentPath: String = ""
        private set

    private var shouldEnabledPositiveButton = false

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Objects.requireNonNull(arguments, "Arguments cannot be null.")
        if (savedInstanceState != null) {
            currentPath = savedInstanceState.getString(EXTRA_CURRENT_PATH) ?: ""
        } else {
            setCurrentPath(arguments?.getString(EXTRA_CURRENT_PATH, "") ?: "")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterAllLocalBroadcastReceivers()
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EXTRA_CURRENT_PATH, currentPath)
    }

    override fun onGetContentViewLayoutResource(): Int {
        return R.layout.file_picker_dialog_content
    }

    @CallSuper
    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        builder.setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface?, i: Int -> performOnPickFinished() }
        builder.setNegativeButton(android.R.string.cancel, null)
    }

    @CallSuper
    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        listView = contentView.findViewById(android.R.id.list)
        summaryText = contentView.findViewById(R.id.location_text)
        emptyText = contentView.findViewById(R.id.empty_text)
        infoText = contentView.findViewById(R.id.info_text)
        adapter.setFilePickerCallback(this)
        listView.adapter = adapter
        listView.addOnItemTouchListener(object : OnItemTouchListener {
            override fun onInterceptTouchEvent(recyclerView: RecyclerView, motionEvent: MotionEvent): Boolean {
                return shouldBlockClick
            }

            override fun onTouchEvent(recyclerView: RecyclerView, motionEvent: MotionEvent) {}
            override fun onRequestDisallowInterceptTouchEvent(b: Boolean) {}
        })
        adapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                val isEmpty = adapter.itemCount < 1
                listView.setVisibility(if (isEmpty) View.GONE else View.VISIBLE)
                emptyText.setVisibility(if (isEmpty) View.VISIBLE else View.GONE)
            }
        })
        listView.fixEdgeEffect()
        performRefresh()
    }

    override fun onShow(dialog: AlertDialog) {
        super.onShow(dialog)
        setPositiveButtonEnabled(shouldEnabledPositiveButton)
    }

    override fun onItemClick(name: String?, isFile: Boolean) {
        if (isFile) { // Ignore file item click
            return
        }
        if (name != null) {
            moveToNext(name)
        } else {
            moveToParent()
        }
        performRefresh()
    }

    fun performRefresh() {
        shouldBlockClick = true
        onPick(currentPath)

        launch {
            val list = try {
                withContext(IO) {
                    enqueueRefresh()
                }
            } catch (e: CancellationException) {
                null
            } catch (e: Throwable) {
                e.printStackTrace()
                shouldBlockClick = false
                null
            }
            if (list != null) {
                adapter.updateItems(isRoot, list)
            }
            shouldBlockClick = false
        }
    }

    private fun performOnPickFinished() {
        onPickFinished()
    }

    /**
     * Set current path when controller is initializing
     *
     * @param currentPath Current path
     */
    fun setCurrentPath(currentPath: String) {
        var currentPath = currentPath
        if (currentPath.isNotEmpty() && !currentPath.startsWith("/")) {
            currentPath = "/$currentPath"
        }
        this.currentPath = currentPath
    }

    val isRoot: Boolean
        get() = TextUtils.isEmpty(currentPath) || currentPath == "/"

    fun setSummary(text: CharSequence?) {
        summaryText.text = text
    }

    fun setInfoText(text: CharSequence?) {
        if (text == null) {
            infoText.visibility = View.GONE
            return
        } else {
            infoText.visibility = View.VISIBLE
        }
        if (text is String) {
            infoText.setHtmlText(text as String?)
        } else {
            infoText.text = text
        }
    }

    protected fun moveToParent() {
        if (!isRoot) {
            currentPath = currentPath.substring(0, currentPath.lastIndexOf("/"))
        }
        onPick(currentPath)
    }

    protected fun moveToNext(child: String) {
        currentPath = "$currentPath/$child"
        onPick(currentPath)
    }

    /**
     * Update summary part of UI
     *
     * @param currentPath Current path
     */
    @CallSuper
    open fun onPick(currentPath: String) {
        this.currentPath = currentPath
        updatePositiveButtonState()
    }

    open fun updatePositiveButtonState() {
        setPositiveButtonEnabled(currentPath.isNotEmpty() && currentPath != "/")
    }

    override fun setPositiveButtonEnabled(enabled: Boolean) {
        shouldEnabledPositiveButton = enabled
        if (positiveButton != null) {
            super.setPositiveButtonEnabled(enabled)
        }
    }

    fun startCreateFolderDialog() {
        CreateFolderDialog.newInstance().show(childFragmentManager)
    }

    open fun onCreateFolder(name: String?) {}

    abstract fun enqueueRefresh(): List<ServerFile?>

    /**
     * Finish picking. Set up value in this method.
     */
    abstract fun onPickFinished()

    class CreateFolderDialog : EditTextDialogFragment() {
        override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
            super.onBuildAlertDialog(builder, savedInstanceState)
            builder.setTitle(R.string.create_folder_dialog_title)
        }

        override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
            super.onAlertDialogCreated(dialog, contentView, savedInstanceState)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                editText.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO
            }
            editText.doAfterTextChanged {
                if (positiveButton != null) {
                    setPositiveButtonEnabled(!TextUtils.isEmpty(value))
                }
            }
        }

        override fun onShow(dialog: AlertDialog) {
            super.onShow(dialog)
            setPositiveButtonEnabled(!TextUtils.isEmpty(value))
        }

        override fun onDone() {
            if (parentFragment is FilePickerDialog) {
                (parentFragment as FilePickerDialog?)!!.onCreateFolder(value)
            }
        }

        companion object {
            fun newInstance(): CreateFolderDialog {
                return CreateFolderDialog()
            }
        }
    }

    companion object {
        const val EXTRA_CURRENT_PATH = BuildConfig.APPLICATION_ID + ".extra.CURRENT_PATH"
    }
}