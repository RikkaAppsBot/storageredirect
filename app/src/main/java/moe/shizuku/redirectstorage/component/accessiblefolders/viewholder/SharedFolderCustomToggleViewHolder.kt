package moe.shizuku.redirectstorage.component.accessiblefolders.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import moe.shizuku.redirectstorage.MountDirsConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.utils.WordsHelper
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator
import java.util.*

class SharedFolderCustomToggleViewHolder(itemView: View) : BaseListenerViewHolder<MountDirsConfig, SharedFolderCustomToggleViewHolder.Listener>(itemView), OnClickListener {

    companion object {
        val CREATOR = Creator<MountDirsConfig> { inflater: LayoutInflater, parent: ViewGroup? -> SharedFolderCustomToggleViewHolder(inflater.inflate(R.layout.accessible_folders_dialog_item_shared_folders, parent, false)) }
    }

    interface Listener {
        fun onViewCustomClick()
        fun onToggleCustomClick(newState: Boolean)
    }

    private val button1: View = itemView.findViewById(android.R.id.button1)
    private val button2: View = itemView.findViewById(android.R.id.button2)
    private val switchView: CheckBox = itemView.findViewById(R.id.switch_widget)
    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val text: TextView = itemView.findViewById(android.R.id.text1)

    init {
        button1.setOnClickListener(this)
        button2.setOnClickListener { view: View? -> onWidgetClick(view) }
    }

    override fun onClick(view: View) {
        listener!!.onViewCustomClick()
    }

    private fun onWidgetClick(view: View?) {
        onSwitchClick()
    }

    private fun onSwitchClick() {
        listener!!.onToggleCustomClick(!data!!.isCustomAllowed)
    }

    override fun onBind() {
        syncEnabledState()

        val context = itemView.context

        title.setText(R.string.dialog_accessible_folders_shared_custom)

        if (data.customDirs != null && data.customDirs!!.isNotEmpty()) {
            val list = ArrayList(data.customDirs!!)
            list.sort()
            text.text = WordsHelper.buildOmissibleFoldersListText(text.context, list, 4)
        } else {
            text.setText(R.string.mount_dirs_custom_unset)
        }

        icon.setImageDrawable(context.getDrawable(R.drawable.ic_folder_24dp))
    }

    override fun onBind(payloads: List<Any>) {
        syncEnabledState()
    }

    private fun syncEnabledState() {
        switchView.isChecked = data!!.isCustomAllowed
    }
}