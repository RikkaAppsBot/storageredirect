package moe.shizuku.redirectstorage.component.filebrowser

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.os.DeadObjectException
import android.os.Environment
import android.os.RemoteException
import android.text.TextUtils
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.util.Pair
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.filebrowser.adapter.AppFileBrowserListAdapter
import moe.shizuku.redirectstorage.component.filebrowser.dialog.FileStatDialog
import moe.shizuku.redirectstorage.databinding.AppFileBrowserContentBinding
import moe.shizuku.redirectstorage.model.BreadcrumbItem
import moe.shizuku.redirectstorage.model.ParcelStructStat
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.provider.ServerFileProvider
import moe.shizuku.redirectstorage.utils.FileBrowserUtils
import moe.shizuku.redirectstorage.utils.FileBrowserUtils.FileBrowserStyleHolder
import moe.shizuku.redirectstorage.utils.FileBrowserUtils.FileSort
import moe.shizuku.redirectstorage.utils.FileUtils
import moe.shizuku.redirectstorage.utils.ViewUtils
import moe.shizuku.redirectstorage.widget.DefaultBreadcrumbsCallback
import org.reactivestreams.Subscription
import rikka.recyclerview.addFastScroller
import rikka.recyclerview.addVerticalPadding
import rikka.recyclerview.fixEdgeEffect
import java.io.File
import java.util.*

class AppFileBrowserFragment : AppFragment(), AppFileBrowserListAdapter.Callback {

    private lateinit var binding: AppFileBrowserContentBinding

    private val addMountDirs = false

    private var srManager: SRManager? = null
    private var srFileManager: SRFileManager? = null

    private val mountDirs: MutableList<String> = ArrayList()

    private var _packageStorageRootPath: String? = null
    private var currentPathDirs: ArrayList<String?>? = null

    private inline val breadcrumbsView get() = binding.breadcrumbs
    private inline val recyclerView get() = binding.list
    private inline val emptyText get() = binding.empty
    private inline val progressView get() = binding.progress

    private val model by viewModels<FileBrowserViewModel>()
    private lateinit var adapter: AppFileBrowserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments == null) {
            throw RuntimeException("arguments is null")
        }
        srManager = SRManager.create()
        if (srManager == null) {
            requireActivity().finish()
            return
        }
        srFileManager = srManager!!.fileManager
        if (arguments != null) {
            model.packageName = requireArguments().getString(AppConstants.EXTRA_PACKAGE_NAME)
            model.userId = requireArguments().getInt(AppConstants.EXTRA_USER_ID)
            model.currentServerFile = ServerFile.fromRedirectTarget(model.currentPath, model.packageName!!, model.userId)
            mountDirs.clear()
            /*try {
                mountDirs.addAll(srManager!!.getMountDirsForPackage(model.packageName!!, model.userId))
            } catch (e: RemoteException) {
                requireActivity().finish()
            }*/
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AppFileBrowserContentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val styleHolder = FileBrowserStyleHolder(view.context)
        breadcrumbsView.callback = AppFileBrowserBreadcrumbCallback()
        if (savedInstanceState == null) {
            breadcrumbsView.addItem(BreadcrumbItem.createSimpleItem(getString(R.string.file_browser_root_path)))
        }
        adapter = AppFileBrowserListAdapter(styleHolder)
        adapter.setCallback(this)
        adapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onChanged() {
                if (model.isLoading) {
                    ViewUtils.setVisibleOrGone(emptyText, false)
                    ViewUtils.setVisibleOrGone(progressView, true)
                } else {
                    if (adapter.itemCount <= 0) {
                        emptyText.setText(if (model.isNullResult) R.string.file_browser_empty_hint_no_storage else R.string.file_browser_empty_hint)
                        ViewUtils.setVisibleOrGone(emptyText, true)
                        ViewUtils.setVisibleOrGone(progressView, false)
                    } else {
                        ViewUtils.setVisibleOrGone(emptyText, false)
                        ViewUtils.setVisibleOrGone(progressView, false)
                        recyclerView.scheduleLayoutAnimation()
                    }
                }
            }
        })
        recyclerView.adapter = adapter
        recyclerView.addVerticalPadding(8, 8)
        recyclerView.addFastScroller(null)
        recyclerView.fixEdgeEffect()
        updateItems()
        updateCurrentPath()
    }

    override fun onItemClicked(item: ServerFile) {
        if (item.isDirectory) {
            if (currentPathDirs != null) {
                val bi = BreadcrumbItem(currentPathDirs!!)
                bi.selectedItem = item.name!!
                breadcrumbsView.addItem(bi)
            }
            model.currentPath += (if (!TextUtils.isEmpty(model.currentPath)) "/" else "") + item.name
            onPathChanged(model.currentPath)
        } else {
            val intent = FileBrowserUtils.getOpenFileIntent(
                    item.packageName, item.userId,
                    model.currentPath + "/" + item.name,
                    item.nameSuffix,
                    requireContext().getString(R.string.dialog_open_file_chooser_title))
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), R.string.toast_no_external_apps_to_open, Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(requireContext(), R.string.toast_failed_to_open_file_with_external_app, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onItemShareClicked(item: ServerFile) {
        val context = requireContext()
        val uri = ServerFileProvider.getContentUriRelative(item.packageName, item.userId, item.relativePath)
        val intent = Intent(Intent.ACTION_SEND).apply {
            putExtra(Intent.EXTRA_STREAM, uri)
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(item.nameSuffix)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        startActivity(Intent.createChooser(intent, context.getString(R.string.action_share)))
        //ChooserActivity.startWithStreamNoThrow(context, context.getString(R.string.action_share), intent, uri)
    }

    @SuppressLint("CheckResult")
    override fun onItemViewPropertiesClicked(item: ServerFile) {
        Flowable.just(item)
                .map { data: ServerFile? ->
                    val srm = SRManager.createThrow()
                    val fm = srm.fileManager
                    Pair.create(fm.getAbsolutePath(item), fm.stat(item))
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pair: Pair<String?, ParcelStructStat?> ->
                    val fm = requireActivity().supportFragmentManager
                    FileStatDialog.newInstance(pair.second!!, pair.first).show(fm)
                }) { obj: Throwable -> obj.printStackTrace() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.file_browser, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                updateItems()
                return true
            }
            R.id.action_sort_by_name -> {
                model.sortBy = FileBrowserUtils.SORT_BY_NAME
                item.isChecked = true
                updateItems()
                return true
            }
            R.id.action_sort_by_modified_time -> {
                model.sortBy = FileBrowserUtils.SORT_BY_MODIFIED_TIME
                item.isChecked = true
                updateItems()
                return true
            }
            R.id.action_sort_by_last_access -> {
                model.sortBy = FileBrowserUtils.SORT_BY_LAST_ACCESS
                item.isChecked = true
                updateItems()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun updateItems() {
        try {
            val dirsGetter = FileBrowserUtils
                    .getRedirectStorageDirs(model.currentServerFile, model.sortBy, addMountDirs)
                    .doOnNext { value: ServerFile -> currentPathDirs!!.add(value.name) }
            val filesGetter = FileBrowserUtils
                    .getRedirectStorageFiles(model.currentServerFile, model.sortBy)
            mCompositeDisposable.clear()
            mCompositeDisposable.add(
                    Flowable.concat(dirsGetter, filesGetter)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map { f: ServerFile ->
                                for (s in mountDirs) {
                                    if (FileUtils.isChildOf(s, f.relativePath)) {
                                        if (mountDirs.contains(f.relativePath)) {
                                            f.isMountDir = true
                                        }
                                        f.setStorageRootType()
                                        return@map f
                                    }
                                }
                                f
                            }
                            .doOnSubscribe { disposable: Subscription? ->
                                adapter.getItems<Any>().clear()
                                currentPathDirs = ArrayList()
                                model.isLoading = true
                                model.isNullResult = false
                                adapter.notifyDataSetChanged()
                            }
                            .subscribe({ e: ServerFile -> adapter.getItems<Any>().add(e) }, { e: Throwable? ->
                                if (e is NullPointerException) {
                                    model.isLoading = false
                                    model.isNullResult = true
                                    adapter.notifyDataSetChanged()
                                }
                            }) {
                                model.isLoading = false
                                model.isNullResult = false
                                adapter.notifyDataSetChanged()
                            })
        } catch (e: DeadObjectException) {
            e.printStackTrace()
        }
    }

    private val currentPath: String
        private get() = getPath(-1)

    private fun getPath(depth: Int): String {
        var depth = depth
        val items: List<BreadcrumbItem>? = breadcrumbsView!!.items
        if (depth == -1) depth = items!!.size - 1
        val sb = StringBuilder()
        for (i in 1..depth) {
            if (sb.length != 0) sb.append("/")
            sb.append(items!![i].selectedItem)
        }
        return sb.toString()
    }

    // should never happened
    private val packageStorageRootPath: String
        private get() {
            if (_packageStorageRootPath == null) {
                val manager = SRManager.create()
                if (manager != null) {
                    var currentTarget: String? = null
                    try {
                        currentTarget = manager.defaultRedirectTarget
                    } catch (ignored: RemoteException) {
                        // should never happened
                    }
                    if (!TextUtils.isEmpty(currentTarget)
                            && currentTarget!!.contains("%s")) {
                        _packageStorageRootPath = String.format(currentTarget, model.packageName)
                    }
                }
                if (_packageStorageRootPath == null) {
                    _packageStorageRootPath = String.format(Constants.REDIRECT_TARGET_DATA, model.packageName)
                }
                _packageStorageRootPath = Environment.getExternalStorageDirectory().toString() + File.separator + _packageStorageRootPath
            }
            return _packageStorageRootPath!!
        }

    fun onBack(): Boolean {
        return if (breadcrumbsView!!.items!!.size <= 1) {
            false
        } else {
            breadcrumbsView!!.removeLastItem()
            onPathChanged(currentPath)
            true
        }
    }

    private fun onPathChanged(path: String) {
        model.currentPath = path
        var isMountDirs = false
        for (s in mountDirs) {
            if (FileUtils.isChildOf(s, path)) {
                isMountDirs = true
                break
            }
        }
        if (!isMountDirs) {
            model.currentServerFile = ServerFile.fromRedirectTarget(model.currentPath, model.packageName!!, model.userId)
        } else {
            model.currentServerFile = ServerFile.fromStorageRoot(model.currentPath, model.userId)
        }
        updateItems()
        updateCurrentPath()
    }

    private fun updateCurrentPath() {
    }

    class FileBrowserViewModel : ViewModel() {
        var packageName: String? = null
        var userId = 0
        var currentPath = ""
        var currentServerFile: ServerFile? = null

        @FileSort
        var sortBy = FileBrowserUtils.SORT_BY_NAME
        var isLoading = false
        var isNullResult = false
    }

    private inner class AppFileBrowserBreadcrumbCallback : DefaultBreadcrumbsCallback() {
        override fun onNavigateBack(currentItem: BreadcrumbItem, position: Int) {
            onPathChanged(currentPath)
        }

        override fun onNavigateNewLocation(newItem: BreadcrumbItem, changedPosition: Int) {
            onPathChanged(getPath(changedPosition - 1) + "/" + newItem.selectedItem)
        }
    }

    companion object {
        fun newInstance(packageName: String?, userId: Int): AppFileBrowserFragment {
            val fragment = AppFileBrowserFragment()
            val bundle = Bundle()
            bundle.putString(AppConstants.EXTRA_PACKAGE_NAME, packageName)
            bundle.putInt(AppConstants.EXTRA_USER_ID, userId)
            fragment.arguments = bundle
            return fragment
        }
    }

    init {
        setHasOptionsMenu(true)
    }
}