package moe.shizuku.redirectstorage.component.detail.viewholder

import android.animation.LayoutTransition
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.detail.adapter.AppDetailAdapter
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailRecommendationViewHolder.Listener
import moe.shizuku.redirectstorage.model.AppRecommendation
import moe.shizuku.redirectstorage.utils.SizeTagHandler
import moe.shizuku.redirectstorage.viewmodel.Resource
import moe.shizuku.redirectstorage.viewmodel.Status
import rikka.core.util.ResourceUtils
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailRecommendationViewHolder(itemView: View) : BaseListenerViewHolder<Resource<AppRecommendation?>, Listener?>(itemView), View.OnClickListener {

    companion object {
        val CREATOR = Creator<Resource<AppRecommendation?>> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailRecommendationViewHolder(inflater.inflate(R.layout.detail_recommendation, parent, false)) }
    }

    interface Listener {
        fun onReloadRuleClick()
    }

    private val systemApp: View = itemView.findViewById(R.id.recommendation_system_app)

    private val recommendationContainer: ViewGroup = itemView.findViewById(R.id.recommendation_content)
    private val recommendationIcon: ImageView = recommendationContainer.findViewById(android.R.id.icon)
    private val recommendationTitle: TextView = recommendationContainer.findViewById(android.R.id.title)
    private val recommendationSummary: TextView = recommendationContainer.findViewById(android.R.id.summary)

    private val contributorContainer: ViewGroup = itemView.findViewById(R.id.recommendation_contributors)
    private val contributorTitle: TextView = contributorContainer.findViewById(android.R.id.title)
    private val contributorSummary: TextView = contributorContainer.findViewById(android.R.id.summary)

    private val updateContainer: ViewGroup = itemView.findViewById(R.id.recommendation_update)
    private val updateTitle: TextView = updateContainer.findViewById(android.R.id.title)
    private val updateSummary: TextView = updateContainer.findViewById(android.R.id.summary)

    private val expandButton: View = itemView.findViewById(android.R.id.button2)
    private val retryButton: View = itemView.findViewById(android.R.id.button1)

    private val progressDrawable: CircularProgressDrawable

    init {
        recommendationSummary.highlightColor = Color.TRANSPARENT
        recommendationSummary.ellipsize = TextUtils.TruncateAt.END
        contributorSummary.movementMethod = LinkMovementMethod.getInstance()
        retryButton.setOnClickListener { listener!!.onReloadRuleClick() }

        val layoutTransition = LayoutTransition()
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        (recommendationSummary.parent as ViewGroup).layoutTransition = layoutTransition

        progressDrawable = CircularProgressDrawable(itemView.context)
        progressDrawable.centerRadius = itemView.resources.getDimension(R.dimen.circular_progress_icon_radius)
        progressDrawable.strokeWidth = itemView.resources.getDimension(R.dimen.circular_progress_icon_stroke_width)
        progressDrawable.setColorSchemeColors(ResourceUtils.resolveColor(itemView.context.theme, android.R.attr.colorAccent))
    }

    override fun getAdapter(): AppDetailAdapter {
        return super.getAdapter() as AppDetailAdapter
    }

    private fun setExpanded(expanded: Boolean, save: Boolean = false) {
        if (save) {
            adapter.recommendationExpanded = expanded
        }

        if (expanded) {
            expandButton.visibility = View.GONE
            if (data.data?.status == AppRecommendation.STATUS_LOADED) {
                updateContainer.visibility = View.VISIBLE
                contributorContainer.visibility = View.VISIBLE
            } else {
                updateContainer.visibility = View.GONE
                contributorContainer.visibility = View.GONE
            }

            recommendationSummary.movementMethod = LinkMovementMethod.getInstance()
            recommendationSummary.maxLines = Int.MAX_VALUE
            itemView.setOnClickListener(null)
            itemView.isClickable = false
        } else {
            expandButton.visibility = View.VISIBLE
            updateContainer.visibility = View.GONE
            contributorContainer.visibility = View.GONE

            recommendationSummary.movementMethod = null
            recommendationSummary.maxLines = 1
            itemView.setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        setExpanded(true, save = true)
    }

    override fun onBind() {
        val context = itemView.context
        val res = data

        when (res.status) {
            Status.LOADING -> {
                setExpanded(true)

                recommendationIcon.setImageDrawable(progressDrawable)
                if (!progressDrawable.isRunning) {
                    progressDrawable.start()
                }
                recommendationTitle.setText(R.string.rule_loading)
                recommendationSummary.visibility = View.GONE
                retryButton.visibility = View.GONE
                val theme = itemView.context.theme
                recommendationIcon.imageTintList = ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorSecondary)
                recommendationTitle.setTextColor(ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorPrimary))
                systemApp.isVisible = false
            }
            Status.ERROR -> {
                setExpanded(adapter.recommendationExpanded)

                recommendationIcon.setImageResource(R.drawable.ic_error_24dp)
                recommendationTitle.setText(R.string.rule_network_issue_title)

                val sb = StringBuilder()
                sb.append(context.getString(R.string.rule_network_issue_summary))
                if (Settings.isCustomRepoEnabled) {
                    sb.append("<p>").append(context.getString(R.string.rule_network_issue_custom_enabled_summary))
                }
                if (res.error.message != null) {
                    sb.append("<p>").append(res.error.message)
                }
                recommendationSummary.text = sb.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                recommendationSummary.visibility = View.VISIBLE
                retryButton.visibility = View.VISIBLE
                val theme = itemView.context.theme
                recommendationIcon.imageTintList = ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorSecondary)
                recommendationTitle.setTextColor(ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorPrimary))
                systemApp.isVisible = false
            }
            else -> {
                setExpanded(adapter.recommendationExpanded)

                val data = res.data
                when (data!!.status) {
                    AppRecommendation.STATUS_NO_RESULT -> {
                        recommendationIcon.setImageResource(R.drawable.ic_help_24dp)

                        if (Settings.isCustomRepoEnabled) {
                            recommendationTitle.setText(R.string.rule_not_found_custom_enabled_title)
                            recommendationSummary.text = context.getString(R.string.rule_not_found_custom_enabled_text).toHtml()
                        } else {
                            val summaryBuilder = StringBuilder()
                            /*if (data.appCategory != null) {
                                  val desc = data.getDescriptionByCategory(context)
                                  if (desc != null) {
                                      summaryBuilder.append(desc)
                                      summaryBuilder.append("<small><br><br></small>")
                                  }
                              }*/
                            summaryBuilder.append(context.getString(R.string.rule_not_fount_text))

                            recommendationTitle.setText(R.string.rule_not_found_title)
                            recommendationSummary.text = summaryBuilder.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                        }
                        recommendationSummary.visibility = View.VISIBLE
                        retryButton.visibility = View.GONE
                        val theme = itemView.context.theme
                        recommendationIcon.imageTintList = ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorSecondary)
                        recommendationTitle.setTextColor(ResourceUtils.resolveColorStateList(theme, android.R.attr.textColorPrimary))

                        contributorContainer.visibility = View.GONE

                        systemApp.isVisible = false
                    }

                    AppRecommendation.STATUS_LOADED -> {
                        retryButton.visibility = View.GONE

                        recommendationIcon.setImageDrawable(data.config.recommendationIcon(context))
                        val recommendation = data.config.recommendationText()
                        if (recommendation != null) {
                            recommendationTitle.text = HtmlCompat.fromHtml(recommendation)
                        } else {
                            recommendationTitle.text = null
                        }

                        val behaviorsText = data.config.behaviors()
                        if (behaviorsText != null) {
                            recommendationSummary.text = HtmlCompat.fromHtml(behaviorsText, HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE, null, SizeTagHandler.getInstance())
                            recommendationSummary.visibility = View.VISIBLE
                        } else {
                            recommendationSummary.visibility = View.GONE
                        }

                        val color = data.config.recommendationColor(context)
                        if (color != null) {
                            recommendationIcon.imageTintList = ColorStateList.valueOf(color)
                            recommendationTitle.setTextColor(color)
                        }

                        contributorTitle.text = HtmlCompat.fromHtml(data.config.authors(itemView.context), null, SizeTagHandler.getInstance())

                        val sb = StringBuilder()
                        sb.append(context.getString(R.string.rule_warning_text))
                        sb.append("<small><br><br></small>")
                        sb.append(context.getString(R.string.rule_report_text))

                        contributorSummary.text = HtmlCompat.fromHtml(sb.toString())

                        updateTitle.text = data.config.lastUpdate(context)

                        systemApp.isVisible = adapter.appInfo.isAOSP()
                    }
                }
            }
        }
    }

    override fun onBind(payloads: List<Any>) {
        onBind()
    }
}