package moe.shizuku.redirectstorage.component.logcat.adapter;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.logcat.viewholder.LogItemViewHolder;
import moe.shizuku.redirectstorage.model.LogcatItem;
import rikka.core.compat.OptionalInt;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;
import rikka.recyclerview.helper.FilterHelper;

/**
 * Created by Fung Gwo on 2018/2/20.
 */

public class LogcatAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    private static final int FILTER_LOG_LEVEL = R.id.action_log_level_subtitle;
    private static final int FILTER_LOG_SHOW_SERVER = R.id.action_log_type_sr_server;
    private static final int FILTER_LOG_SHOW_SE = R.id.action_log_type_se;

    private FilterHelper<LogcatItem> mFilterHelper;

    public LogcatAdapter(List<LogcatItem> data) {
        setItems(new ArrayList<>(data));

        getCreatorPool().putRule(LogcatItem.class, LogItemViewHolder.CREATOR);

        mFilterHelper = new FilterHelper<>(this, new FilterHelper.Filter<LogcatItem>() {
            @Override
            public boolean contains(String keyword, LogcatItem obj) {
                return !TextUtils.isEmpty(keyword)
                        && (obj.getTag().contains(keyword)
                        || obj.getMsg().contains(keyword)
                        || obj.getPid() == OptionalInt.of(keyword).orElse(Integer.MIN_VALUE));
            }

            @Override
            public boolean filter(int key, int value, LogcatItem obj) {
                switch (key) {
                    case FILTER_LOG_LEVEL:
                        return obj.getLevelInt() >= value;
                    /*case FILTER_LOG_SHOW_SERVER:
                        switch (value) {
                            case 2:
                                return CollectionsCompat.anyMatch(
                                        Collections.singletonList(OBSERVER_TAG),
                                        tag -> tag.equals(obj.getTag()));
                            case 1:
                                return CollectionsCompat.anyMatch(
                                        Arrays.asList(SR_TAG, NATIVE_TAG, OBSERVER_TAG),
                                        tag -> tag.equals(obj.getTag()));
                            default:
                            case 0:
                                return true;
                        }*/
                }
                return true;
            }

            /*@Override
            public boolean filter(int key, boolean value, LogItem obj) {
                switch (key) {
                    case FILTER_LOG_SHOW_SE:
                        return value || !obj.getOriginalLine().contains("avc: ");
                }
                return true;
            }*/
        });
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }

    public FilterHelper<LogcatItem> getFilterHelper() {
        return mFilterHelper;
    }

    public void setFilterLogLevel(int level) {
        getFilterHelper().putKey(FILTER_LOG_LEVEL, level);
    }

    public void setFilterShowServer(boolean b, boolean fileObserverOnly) {
        //getFilterHelper().putKey(FILTER_LOG_SHOW_SERVER, b ? (fileObserverOnly ? 2 : 1) : 0);
    }

    public void setFilterShowSE(boolean b) {
        //getFilterHelper().putKey(FILTER_LOG_SHOW_SE, b);
    }

    public void updateItems(List<LogcatItem> data) {
        getItems().clear();
        if (data != null) {
            getItems().addAll(data);
        }
        mFilterHelper.updateOriginalData();
    }

}
