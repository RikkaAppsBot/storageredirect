package moe.shizuku.redirectstorage.component.applist.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.Callback
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.applist.viewholder.AppListControllersViewHolder
import moe.shizuku.redirectstorage.component.applist.viewholder.AppListTipViewHolder
import moe.shizuku.redirectstorage.component.applist.viewholder.AppViewHolder
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.DiffUtilComparable
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder
import rikka.recyclerview.IdBasedRecyclerViewAdapter

class AppListAdapter : IdBasedRecyclerViewAdapter() {

    private var _editingFilter = false

    var isEditingFilter: Boolean
        get() = _editingFilter
        set(isEditing) {
            if (_editingFilter == isEditing) return
            _editingFilter = isEditing
            if (itemCount > 0) {
                notifyItemChanged(0, AppConstants.PAYLOAD)
            }
        }

    @Synchronized
    fun updateAppList(data: List<AppInfo>) {
        val oldList = ArrayList<Any>(getItems())
        val newList = ArrayList<Any>()
        val newData = ArrayList<AppInfo>()
        data.forEach {
            newData.add(AppInfo(it))
        }
        newList.add(this)
        newList.add(DIVIDER1)
        newList.addAll(newData)
        if (newData.isNotEmpty()) {
            newList.add(DIVIDER2)
        }
        newList.add(TIP)
        val result = DiffUtil.calculateDiff(object : Callback() {
            override fun getOldListSize(): Int {
                return oldList.size
            }

            override fun getNewListSize(): Int {
                return newList.size
            }

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val o1 = oldList[oldItemPosition]
                val o2 = newList[newItemPosition]
                return if (o1 is DiffUtilComparable && o2 is DiffUtilComparable) {
                    o1.itemSamesTo(o2)
                } else o1 == o2
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val o1 = oldList[oldItemPosition]
                val o2 = newList[newItemPosition]
                return if (o1 is DiffUtilComparable && o2 is DiffUtilComparable) {
                    o1.contentSamesTo(o2)
                } else o1 == o2
            }

            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                val o1 = oldList[oldItemPosition]
                val o2 = newList[newItemPosition]
                return if (o1 is AppInfo && o2 is AppInfo) {
                    val diff = mutableMapOf<String, Any?>()
                    if (o1.isVerified() != o2.isVerified()) {
                        diff["isVerified"] = o2.isVerified()
                    }
                    if (o1.isRedirectEnabled != o2.isRedirectEnabled) {
                        diff["isRedirectEnabled"] = o2.isRedirectEnabled
                    }
                    if (o1.redirectInfo.mountDirsConfig != o2.redirectInfo.mountDirsConfig) {
                        diff["mountDirsConfig"] = o2.redirectInfo.mountDirsConfig
                    }
                    if (o1.redirectInfo.redirectTarget != o2.redirectInfo.redirectTarget) {
                        diff["redirectTarget"] = o2.redirectInfo.redirectTarget
                    }
                    diff
                } else {
                    super.getChangePayload(oldItemPosition, newItemPosition)
                }
            }
        })
        clear()
        addItem(AppListControllersViewHolder.CREATOR, this, 0L)
        addItem(DIVIDER_CREATOR, DIVIDER1, 1L)
        for (ai in newData) {
            addItem(AppViewHolder.CREATOR, ai, ai.hashCode().toLong() shl 32)
        }
        if (newData.isNotEmpty()) {
            addItem(DIVIDER_CREATOR, DIVIDER2, 2L)
        }
        addItem(AppListTipViewHolder.CREATOR, TIP, 3L)
        result.dispatchUpdatesTo(this)
    }

    companion object {

        private val DIVIDER_CREATOR = DividerViewHolder.newCreator(R.layout.item_divider_no_margin)
        private val DIVIDER1 = Any()
        private val TIP = Any()
        private val DIVIDER2 = Any()
    }

    init {
        setHasStableIds(true)
    }
}