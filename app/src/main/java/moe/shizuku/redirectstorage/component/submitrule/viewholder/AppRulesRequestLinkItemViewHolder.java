package moe.shizuku.redirectstorage.component.submitrule.viewholder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.submitrule.SubmitRuleFragment;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

/**
 * Created by fytho on 2017/12/12.
 */

public class AppRulesRequestLinkItemViewHolder extends BaseViewHolder<ObserverInfo> {

    public static Creator<ObserverInfo> newCreator(OnDeleteListener onDeleteListener) {
        return new ItemCreator(onDeleteListener);
    }

    private final ImageView icon;
    private final TextView title, summary;

    public AppRulesRequestLinkItemViewHolder(View itemView, final OnDeleteListener onDeleteListener) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);

        itemView.findViewById(android.R.id.content).setOnClickListener(v -> {
            LocalBroadcastManager.getInstance(v.getContext())
                    .sendBroadcast(new Intent(SubmitRuleFragment.ACTION_REQUEST_EDIT_OBSERVER)
                            .putExtra(EXTRA_DATA, getData()));
        });

        ImageButton deleteButton = itemView.findViewById(android.R.id.button1);
        deleteButton.setOnClickListener(view -> {
            if (onDeleteListener != null) {
                onDeleteListener.onDelete(getData());
            }
        });
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();

        //title.setText(ObserverInfoHelper.getDescriptionTitleText(context, getData()));
        summary.setText(HtmlCompat.fromHtml(context.getString(R.string.detail_item_link_summary, getData().source, getData().target)));

        if (getAdapter().getItemAt(getAdapterPosition() - 1) instanceof ObserverInfo) {
            icon.setImageDrawable(null);
        } else {
            icon.setImageResource(R.drawable.ic_link_24dp);
        }
    }

    static class ItemCreator implements Creator<ObserverInfo> {

        private OnDeleteListener onDeleteListener;

        ItemCreator(OnDeleteListener onDeleteListener) {
            this.onDeleteListener = onDeleteListener;
        }

        @Override
        public BaseViewHolder<ObserverInfo> createViewHolder(LayoutInflater inflater, ViewGroup parent) {
            return new AppRulesRequestLinkItemViewHolder(inflater.inflate(R.layout.app_rules_request_link_item, parent, false), onDeleteListener);
        }

    }

    public interface OnDeleteListener {

        void onDelete(ObserverInfo observerInfo);

    }

}
