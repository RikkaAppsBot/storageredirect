package moe.shizuku.redirectstorage.component.accessiblefolders.adapter

import android.view.View
import android.widget.TextView
import androidx.core.view.isGone
import androidx.recyclerview.widget.DiffUtil
import moe.shizuku.redirectstorage.R
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.ClassCreatorPool

class SimpleMountPathsAdapter(
        private val isEditable: Boolean = true
) : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    interface Listener: ItemViewHolder.Listener {
    }

    init {
        creatorPool.apply {
            putRule(Int::class.javaObjectType) { inflater, parent ->
                EmptyViewHolder(inflater.inflate(
                        R.layout.mount_info_edit_dirs_empty_item, parent, false
                ))
            }
            putRule(String::class.java) { inflater, parent ->
                ItemViewHolder(inflater.inflate(
                        R.layout.mount_info_edit_dirs_path_item, parent, false
                )).apply {
                    deleteButton.isGone = !isEditable
                }
            }
        }

        setItems(listOf(0))
    }

    fun updateData(items: List<String>) {
        val newItems: List<Any> = if (items.isEmpty()) {
            listOf(0)
        } else {
            ArrayList(items.sorted())
        }
        val callback = DiffUtilCallback(getItems<Any>()
                ?: emptyList(), newItems)
        setItems(newItems)
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool()
    }

    private class DiffUtilCallback(
            private val oldItems: List<Any>,
            private val newItems: List<Any>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return areItemsTheSame(oldItemPosition, newItemPosition)
        }

    }

    class EmptyViewHolder(itemView: View) : BaseViewHolder<Int>(itemView)

    class ItemViewHolder(itemView: View) : BaseListenerViewHolder<String, ItemViewHolder.Listener>(itemView) {

        interface Listener {
            fun onDeletePathClick(path: String)
        }

        val title: TextView = itemView.findViewById(android.R.id.title)
        val deleteButton: View = itemView.findViewById(R.id.delete_button)

        init {
            deleteButton.setOnClickListener {
                listener.onDeletePathClick(data)
            }
        }

        override fun onBind() {
            title.text = data
        }

    }

}