package moe.shizuku.redirectstorage.component.exportfolders.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import moe.shizuku.redirectstorage.ObserverInfo
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.detail.appDetailViewModel
import moe.shizuku.redirectstorage.component.exportfolders.editor.ExportFolderEditFragment
import moe.shizuku.redirectstorage.event.RemoteRequestHandler
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.model.AppObserverInfo
import moe.shizuku.redirectstorage.utils.DataVerifier
import moe.shizuku.redirectstorage.utils.ObserverConflictDialog
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.core.ktx.unsafeLazy
import rikka.material.app.AppBarOwner
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView

class ExportFoldersFragment : AppFragment(), ExportFoldersAdapter.Listener, ExportFolderEditFragment.Listener {

    private val viewModel by appDetailViewModel()
    private val appInfo: AppInfo by unsafeLazy { viewModel.appInfo.value!!.data!! }

    private var adapter: ExportFoldersAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.appInfo.observe(this) { res ->
            if (res.status == Status.SUCCESS) {
                (activity as? AppBarOwner)?.appBar?.subtitle = viewModel.appInfo.value?.data?.getSubtitle(requireContext())

                if (viewModel.observers.value?.data != null) {
                    adapter?.updateData(requireContext(), viewModel)
                }

                viewModel.observers.observe(this) {
                    if (it.status == Status.SUCCESS) {
                        adapter?.updateData(requireContext(), viewModel)
                    }
                }
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.appbar_container_recycler, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val recyclerView = view as BorderRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(VerticalPaddingDecoration(context).apply { setAllowTop(false) })
        adapter = ExportFoldersAdapter()
        adapter?.listener = this
        recyclerView.adapter = adapter
        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            if (activity != null) {
                (activity as AppBarFragmentActivity).appBar?.setRaised(!top)
            }
        }
        recyclerView.fixEdgeEffect()
    }

    override fun onAddRuleClick() {
        ExportFolderEditFragment.newInstanceForAdd(appInfo).show(childFragmentManager)
    }

    override fun onViewObserverClick(info: AppObserverInfo) {
        if (!info.local) {
            ExportFolderEditFragment.newInstanceForViewOnline(appInfo, info.toObserverInfo()).show(childFragmentManager)
        } else {
            ExportFolderEditFragment.newInstanceForEdit(appInfo, info.toObserverInfo()).show(childFragmentManager)
        }
    }

    override fun onDeleteObserverClick(info: AppObserverInfo) {
        RemoteRequestHandler.removeObserver(info.toObserverInfo())
    }

    override fun onToggleObserverClick(oldInfo: AppObserverInfo) {
        val old = oldInfo.toObserverInfo()
        val info = oldInfo.toObserverInfo()
        info.enabled = !info.enabled

        onRequestUpdateObserver(info, old)
    }

    private fun showResultDialog(result: ObserverInfo.Result, info: ObserverInfo) {
        val context = this.context ?: return
        if (result.conflict != DataVerifier.ObserverInfoConflict.NONE) {
            when (result.conflict) {
                DataVerifier.ObserverInfoConflict.SOURCE -> ObserverConflictDialog.showSourceConflict(context, childFragmentManager, info, result.conflictObserver)
                DataVerifier.ObserverInfoConflict.TARGET -> ObserverConflictDialog.showTargetConflict(context, childFragmentManager, info, result.conflictObserver)
                else -> ObserverConflictDialog.showSourceTargetConflict(context, childFragmentManager, info, result.conflictObserver)
            }
        }
    }

    override fun onRequestAddObserver(info: ObserverInfo): Boolean {
        val result = RemoteRequestHandler.addObserver(info) ?: return false
        showResultDialog(result, info)
        return result.conflict == DataVerifier.ObserverInfoConflict.NONE
    }

    override fun onRequestUpdateObserver(new: ObserverInfo, old: ObserverInfo): Boolean {
        val result = RemoteRequestHandler.updateObserver(new, old) ?: return false
        showResultDialog(result, new)
        return result.conflict == DataVerifier.ObserverInfoConflict.NONE
    }

    override fun onRequestDeleteObserver(info: ObserverInfo): Boolean {
        return RemoteRequestHandler.removeObserver(info)
    }
}