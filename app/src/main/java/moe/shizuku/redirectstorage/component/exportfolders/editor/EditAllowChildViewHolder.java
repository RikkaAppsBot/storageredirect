package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.view.View;
import android.widget.CheckBox;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import moe.shizuku.redirectstorage.viewholder.CheckableViewHolder;

public class EditAllowChildViewHolder extends CheckableViewHolder<ObserverInfoBuilder> {

    public static final Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditAllowChildViewHolder holder = new EditAllowChildViewHolder(
                inflater.inflate(R.layout.observer_info_edit_checkbox_item, parent, false));
        holder.itemView.setEnabled(parent.isEnabled());
        ((CheckBox) holder.checkable).setEnabled(parent.isEnabled());
        return holder;
    };

    private EditAllowChildViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View view) {
        getData().allowChild(!getData().getAllowChild());
        super.onBind();
    }

    @Override
    public boolean isChecked() {
        return getData().getAllowChild();
    }

    @Override
    public void onBind() {
        super.onBind();
        icon.setImageResource(R.drawable.ic_linear_scale_24dp);
        title.setText(R.string.link_dialog_message_allow_child);
        summary.setText(R.string.observer_info_edit_allow_child_summary);
    }
}
