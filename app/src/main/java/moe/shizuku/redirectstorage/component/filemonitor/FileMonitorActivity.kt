package moe.shizuku.redirectstorage.component.filemonitor

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.edit
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.payment.PurchaseFragment.Companion.startActivity
import moe.shizuku.redirectstorage.component.settings.SettingsActivity
import moe.shizuku.redirectstorage.component.settings.WorkModeSettingsFragment
import moe.shizuku.redirectstorage.ktx.applyCountdown
import moe.shizuku.redirectstorage.license.License
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml

class FileMonitorActivity : AppBarFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if ("moe.shizuku.redirectstorage.action.FILE_MONITOR" != intent.action) {
            if (appBar != null) {
                appBar!!.setDisplayHomeAsUpEnabled(true)
            }
        }
        var version = -1
        var enabled = false
        try {
            val srm = SRManager.createThrow()
            val moduleStatus = srm.moduleStatus
            version = moduleStatus.versionCode
            enabled = srm.isFileMonitorEnabled
        } catch (ignored: Exception) {
        }
        if (version <= 12 || !enabled || !License.checkLocal()) {
            val messageRes: Int
            val neutralRes: Int
            val neutralListener: DialogInterface.OnClickListener
            if (!License.checkLocal()) {
                messageRes = R.string.dialog_file_monitor_not_available_message
                neutralRes = R.string.unlock_title
                neutralListener = DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                    startActivity(this)
                    finish()
                }
            } else if (version <= 12) {
                messageRes = R.string.dialog_file_monitor_not_available_message
                neutralRes = R.string.enhance_module_settings
                neutralListener = DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                    startActivity(Intent(this@FileMonitorActivity, SettingsActivity::class.java).setAction(WorkModeSettingsFragment.ACTION))
                    finish()
                }
            } else {
                messageRes = R.string.dialog_file_monitor_not_available_option_disabled_message
                neutralRes = R.string.enhance_module_settings
                neutralListener = DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                    startActivity(Intent(this@FileMonitorActivity, SettingsActivity::class.java).setAction(WorkModeSettingsFragment.ACTION))
                    finish()
                }
            }
            AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_file_monitor_not_available_title)
                    .setMessage(HtmlCompat.fromHtml(getString(messageRes)))
                    .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int -> finish() }
                    .setNeutralButton(neutralRes, neutralListener)
                    .setCancelable(false)
                    .show()
            return
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, FileMonitorFragment.buildFromIntent(intent))
                .commit()
    }
}