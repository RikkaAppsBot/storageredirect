package moe.shizuku.redirectstorage.component.restore.dialog;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import rikka.html.text.HtmlCompat;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID;

public class RestoreWizardAppsConfigPreviewDialog extends AlertDialogFragment {

    public static RestoreWizardAppsConfigPreviewDialog newInstance(@NonNull AbstractBackupModel backupModel, int toShowUserId) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, backupModel);
        args.putInt(EXTRA_USER_ID, toShowUserId);

        RestoreWizardAppsConfigPreviewDialog dialog = new RestoreWizardAppsConfigPreviewDialog();
        dialog.setArguments(args);
        return dialog;
    }

    private AbstractBackupModel mBackupModel;
    private int mToShowUserId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getArguments(), "Arguments cannot be null.");
        mBackupModel = Objects.requireNonNull(getArguments().getParcelable(EXTRA_DATA));
        mToShowUserId = getArguments().getInt(EXTRA_USER_ID);
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder, @Nullable Bundle savedInstanceState) {
        builder.setTitle(getString(R.string.restore_type_apps_config_format, mToShowUserId));
        builder.setMessage(getMessage());
        builder.setPositiveButton(android.R.string.ok, null);
    }

    @Override
    public void onShow(AlertDialog dialog) {
        Resources r = requireContext().getResources();
        float add = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, r.getDisplayMetrics());
        ((TextView) dialog.findViewById(android.R.id.message)).setLineSpacing(add, 1);
    }

    private CharSequence getMessage() {
        final StringBuilder sb = new StringBuilder(getString(R.string.dialog_restore_apps_settings_msg_head)).append("<br>");
        for (RedirectPackageInfo rpi : mBackupModel.getRedirectPackagesForUser(mToShowUserId)) {
            sb.append(getString(R.string.dialog_restore_apps_settings_item_format, rpi.packageName));
        }
        return HtmlCompat.fromHtml(sb.toString(),
                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM | HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE,
                null, SizeTagHandler.getInstance());
    }

}
