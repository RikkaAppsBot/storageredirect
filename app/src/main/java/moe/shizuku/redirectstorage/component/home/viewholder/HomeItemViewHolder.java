package moe.shizuku.redirectstorage.component.home.viewholder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import moe.shizuku.redirectstorage.HelpActivity;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.ComponentActivity;
import moe.shizuku.redirectstorage.component.filemonitor.FileMonitorActivity;
import moe.shizuku.redirectstorage.component.home.model.HomeStatus;
import moe.shizuku.redirectstorage.component.logcat.LogcatStatusActivity;
import moe.shizuku.redirectstorage.component.payment.MyPurchaseFragment;
import moe.shizuku.redirectstorage.component.payment.PurchaseFragment;
import moe.shizuku.redirectstorage.component.restore.fragment.BackupRestoreSettingsFragment;
import moe.shizuku.redirectstorage.component.settings.SettingsActivity;
import moe.shizuku.redirectstorage.license.AlipayHelper;
import moe.shizuku.redirectstorage.license.RedeemHelper;
import moe.shizuku.redirectstorage.widget.AnimatedStarView;
import rikka.internal.help.HelpEntity;
import rikka.internal.help.HelpProvider;
import rikka.recyclerview.BaseViewHolder;

public class HomeItemViewHolder extends BaseViewHolder<HomeStatus> implements View.OnClickListener {

    public static final Creator<HomeStatus> CREATOR_SECONDARY = (inflater, parent) -> new HomeItemViewHolder(inflater.inflate(R.layout.home_item, parent, false));

    public static final Creator<HomeStatus> CREATOR_PRIMARY = (inflater, parent) -> new HomeItemViewHolder(inflater.inflate(R.layout.home_item_primary, parent, false));

    public static final Creator<HomeStatus> CREATOR_UNLOCK = (inflater, parent) -> new UnlockItemViewHolder(inflater.inflate(R.layout.home_item_unlock, parent, false));

    public static final int ID_SETTINGS = 101;
    public static final int ID_HELP = ID_SETTINGS + 1;
    public static final int ID_FILE_MONITOR = ID_SETTINGS + 2;
    public static final int ID_UNLOCK = ID_SETTINGS + 3;
    public static final int ID_BACKUP = ID_SETTINGS + 4;
    public static final int ID_MY_PURCHASE = ID_SETTINGS + 5;
    public static final int ID_REPORT = ID_SETTINGS + 6;
    public static final int ID_LOGCAT = ID_SETTINGS + 7;

    private ImageView icon;
    private TextView title;
    private TextView summary;
    private ImageButton button;

    public HomeItemViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);
        button = itemView.findViewById(android.R.id.button1);

        button.setClickable(false);
        button.setFocusable(false);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();
        final HomeStatus status = getData();

        switch ((int) getItemId()) {
            case ID_UNLOCK: {
                title.setText(R.string.settings_unlock);
                summary.setText(R.string.home_unlock_summary);
                summary.setVisibility(View.VISIBLE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_unlock_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_MY_PURCHASE: {
                title.setText(R.string.my_purchase);
                if (AlipayHelper.checkLocal() || RedeemHelper.checkLocal()) {
                    summary.setText(R.string.home_my_purchase_non_google_summary);
                    summary.setVisibility(View.VISIBLE);
                } else {
                    summary.setVisibility(View.GONE);
                }
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_unlock_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_FILE_MONITOR: {
                title.setText(R.string.file_monitor);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_file_monitor_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_SETTINGS: {
                title.setText(R.string.settings);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_settings_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_HELP: {
                title.setText(R.string.helplib_title);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_help_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_BACKUP: {
                title.setText(R.string.backup_restore);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_backup_24dp));
                itemView.setEnabled(true);
                break;
            }
            case ID_REPORT: {
                title.setText(R.string.report_problems);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_bug_report_24));
                itemView.setEnabled(true);
                break;
            }
            case ID_LOGCAT: {
                title.setText(R.string.logcat_title);
                summary.setVisibility(View.GONE);
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_noti_logcat_24));
                itemView.setEnabled(true);
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        final Context context = view.getContext();
        final Intent intent = new Intent();
        switch ((int) getItemId()) {
            case ID_UNLOCK: {
                intent.setClass(context, ComponentActivity.class);
                intent.putExtra(ComponentActivity.EXTRA_CLASS, PurchaseFragment.class.getName());
                break;
            }
            case ID_MY_PURCHASE: {
                intent.setClass(context, ComponentActivity.class);
                intent.putExtra(ComponentActivity.EXTRA_CLASS, MyPurchaseFragment.class.getName());
                break;
            }
            case ID_FILE_MONITOR: {
                intent.setClass(context, FileMonitorActivity.class);
                break;
            }
            case ID_SETTINGS: {
                intent.setClass(context, SettingsActivity.class);
                break;
            }
            case ID_BACKUP: {
                intent.setClass(context, SettingsActivity.class);
                intent.setAction(BackupRestoreSettingsFragment.ACTION);
                break;
            }
            case ID_HELP: {
                intent.setClass(context, HelpActivity.class);
                break;
            }
            case ID_REPORT: {
                HelpEntity entity = HelpProvider.get(context, "how_to_report_problems");
                if (entity != null) {
                    entity.startActivity(context);
                } else {
                    Toast.makeText(context, R.string.toast_cannot_load_help, Toast.LENGTH_LONG).show();
                }
                return;
            }
            case ID_LOGCAT: {
                intent.setClass(context, LogcatStatusActivity.class);
                break;
            }
        }
        context.startActivity(intent);
    }

    static class UnlockItemViewHolder extends HomeItemViewHolder {

        AnimatedStarView starView;

        UnlockItemViewHolder(View itemView) {
            super(itemView);

            starView = itemView.findViewById(R.id.star_view);

            itemView.setOnLongClickListener(v -> {
                starView.setChecked(true);
                return true;
            });
        }

    }

}
