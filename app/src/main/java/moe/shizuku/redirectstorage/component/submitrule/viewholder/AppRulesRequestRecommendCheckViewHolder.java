package moe.shizuku.redirectstorage.component.submitrule.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder;
import rikka.html.text.HtmlCompat;

/**
 * Created by fytho on 2017/12/11.
 */

public class AppRulesRequestRecommendCheckViewHolder extends AppRulesRequestQuestionViewHolder<AppConfigurationBuilder> {

    public static final Creator<AppConfigurationBuilder> CREATOR = (inflater, parent) -> new AppRulesRequestRecommendCheckViewHolder(inflater.inflate(R.layout.app_rules_request_question, parent, false));

    public AppRulesRequestRecommendCheckViewHolder(View itemView) {
        super(itemView);

        title.setText(HtmlCompat.fromHtml(title.getResources().getString(R.string.app_rules_request_if_recommended)));
        summary.setText(HtmlCompat.fromHtml(title.getResources().getString(R.string.app_rules_request_if_recommended_summary)));
        dontKnowBtn.setVisibility(View.GONE);
        icon.setImageResource(R.drawable.ic_thumb_up_24dp);
    }

    @Override
    public void onAnswerSelected(@Nullable Boolean answer) {
        getData().setRecommended(answer);
        getAdapter().updateItems();
        // getAdapter().notifyItemChanged(getAdapterPosition(), PAYLOAD);
    }

    @Nullable
    @Override
    public Boolean getAnswer() {
        return getData().isRecommended();
    }

    @Override
    public void onBind() {
        super.onBind();
        syncEnabledState();
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        super.onBind(payloads);
        syncEnabledState();
    }

    private void syncEnabledState() {
        if (Boolean.TRUE.equals(getData().isVerified())) {
            yesBtn.setEnabled(false);
            noBtn.setEnabled(false);
            title.setEnabled(false);
        } else {
            yesBtn.setEnabled(true);
            noBtn.setEnabled(true);
            title.setEnabled(true);
        }
    }
}
