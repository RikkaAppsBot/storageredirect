package moe.shizuku.redirectstorage.component.accessiblefolders

import android.app.TaskStackBuilder
import android.os.Bundle
import androidx.fragment.app.commit
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.ktx.putPackageExtrasFrom

class AccessibleFoldersActivity : AppBarFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.fragment_container, AccessibleFoldersFragment.newInstance())
            }
        }
    }

    override fun onPrepareNavigateUpTaskStack(builder: TaskStackBuilder) {
        super.onPrepareNavigateUpTaskStack(builder)
        builder.editIntentAt(builder.intentCount - 1).putPackageExtrasFrom(intent)
    }
}