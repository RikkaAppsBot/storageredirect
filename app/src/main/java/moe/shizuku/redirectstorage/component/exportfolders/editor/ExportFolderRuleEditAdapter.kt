package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.content.Context
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.ktx.isFuseUsed
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.IdBasedRecyclerViewAdapter

class ExportFolderRuleEditAdapter(val context: Context, private val builder: ObserverInfoBuilder, label: CharSequence) : IdBasedRecyclerViewAdapter() {

    companion object {
        const val ID_DESCRIPTION = 1L
        const val ID_SOURCE_PATH = 2L
        const val ID_TARGET_PATH = 3L
        const val ID_MASK = 4L
        const val ID_SHOW_NOTIFICATION = 5L
        const val ID_CALL_MEDIA_SCAN = 6L
        const val ID_ALLOW_CHILD = 7L
        const val ID_ALLOW_TEMP = 8L
        const val ID_FUSE_HELP = 9L
    }

    interface Listener : EditSourcePathViewHolder.Listener, EditTargetPathViewHolder.Listener, EditMaskViewHolder.Listener, EditDescriptionViewHolder.Listener

    init {
        setHasStableIds(true)
        updateData(context, label)
    }

    fun updateData(context: Context, label: CharSequence) {
        clear()
        addItem(EditDescriptionViewHolder.CREATOR, builder, ID_DESCRIPTION)
        addItem(EditSourcePathViewHolder.CREATOR, builder, ID_SOURCE_PATH)
        addItem(EditTargetPathViewHolder.CREATOR, builder, ID_TARGET_PATH)
        if (!isFuseUsed) {
            addItem(EditMaskViewHolder.CREATOR, builder, ID_MASK)
            addItem(EditShowNotificationViewHolder.CREATOR, builder, ID_SHOW_NOTIFICATION)
            addItem(EditCallMediaScanViewHolder.CREATOR, builder, ID_CALL_MEDIA_SCAN)
            addItem(EditAllowChildViewHolder.CREATOR, builder, ID_ALLOW_CHILD)
            addItem(EditAllowTempViewHolder.CREATOR, builder, ID_ALLOW_TEMP)
        } else {
            addItem(EditShowNotificationViewHolder.CREATOR, builder, ID_SHOW_NOTIFICATION)
            addItem(SettingsDescriptionViewHolder.OUTLINE_STYLE_CREATOR, context.getString(R.string.export_folder_help_fuse, label.toString()).toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE), ID_FUSE_HELP)
        }
    }
}