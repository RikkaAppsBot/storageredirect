package moe.shizuku.redirectstorage.component.submitrule.adapter;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder;
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestButtonRowViewHolder;
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestLinkItemViewHolder;
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestReasonEditViewHolder;
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestRecommendCheckViewHolder;
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestVerifiedCheckViewHolder;
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder;
import moe.shizuku.redirectstorage.model.AppInfo;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

import static java.util.Objects.requireNonNull;

/**
 * Created by fytho on 2017/12/11.
 */

public class AppRulesRequestAdapter extends IdBasedRecyclerViewAdapter
        implements AppRulesRequestLinkItemViewHolder.OnDeleteListener {

    private static final int ID_HEADER = 1;
    private static final int ID_RECOMMENDATION = 2;
    private static final int ID_VERIFIED = 3;
    private static final int ID_DIVIDER_0 = 100;
    private static final int ID_DIVIDER_1 = 101;
    private static final int ID_LINK_RULES_SUBTITLE = 5;
    private static final int ID_LINK_RULES_SUMMARY = 6;
    private static final int ID_LINK_RULES_ITEM = 200;
    private static final int ID_BUTTON_ROW = 8;
    private static final int ID_REASON_EDIT = 10;

    private Context mContext;
    private AppInfo mAppInfo;
    private AppConfigurationBuilder mConfigurationBuilder;

    private int mHeaderHeight = 0;

    private WeakReference<RecyclerView> mRecyclerView;

    private final BaseViewHolder.Creator<ObserverInfo> LINK_ITEM_CREATOR =
            AppRulesRequestLinkItemViewHolder.newCreator(this);
    private final BaseViewHolder.Creator<Object> BUTTON_ROW_CREATOR;

    public AppRulesRequestAdapter(@NonNull Context context,
                                  @NonNull AppInfo appInfo,
                                  @NonNull AppConfigurationBuilder configBuilder,
                                  AppRulesRequestButtonRowViewHolder.Callback buttonCallback) {
        super();

        mContext = requireNonNull(context);
        mAppInfo = requireNonNull(appInfo);
        mConfigurationBuilder = requireNonNull(configBuilder);

        BUTTON_ROW_CREATOR = AppRulesRequestButtonRowViewHolder.newCreator(buttonCallback);

        setHasStableIds(true);

        updateItems();
    }

    public void setUpRecyclerView(@NonNull RecyclerView recyclerView) {
        mRecyclerView = new WeakReference<>(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int mScrollY;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                mScrollY += dy;

                if (mContext instanceof Activity) {
                    if (mHeaderHeight <= 0) {
                        mHeaderHeight = recyclerView.getChildAt(0).getHeight();
                    }
                    if (mScrollY <= mHeaderHeight) {
                        ((AppActivity) mContext).getSupportActionBar().setSubtitle(null);
                    } else {
                        ((AppActivity) mContext).getSupportActionBar().setSubtitle(mAppInfo.getLabel());
                    }
                }
            }
        });
    }

    @Nullable
    private RecyclerView getRecyclerView() {
        if (mRecyclerView != null) {
            return mRecyclerView.get();
        }
        return null;
    }

    public void updateItems() {
        clear();

        addHeader(mAppInfo);
        addItem(DividerViewHolder.CREATOR, null, ID_DIVIDER_0);
        addItem(AppRulesRequestVerifiedCheckViewHolder.CREATOR, mConfigurationBuilder, ID_VERIFIED);
        if (Boolean.FALSE.equals(mConfigurationBuilder.isVerified())) {
            addItem(AppRulesRequestRecommendCheckViewHolder.CREATOR, mConfigurationBuilder, ID_RECOMMENDATION);
        }
        if (!mConfigurationBuilder.shouldSetBooleanValues()) {
            addItem(AppRulesRequestReasonEditViewHolder.CREATOR, mConfigurationBuilder, ID_REASON_EDIT);
        }
        if (Boolean.TRUE.equals(mConfigurationBuilder.isRecommended())) {
            addItem(DividerViewHolder.CREATOR, null, ID_DIVIDER_1);
            addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, R.string.app_rules_request_directories_link_rules, ID_LINK_RULES_SUBTITLE);
            addItem(AppDetailSummaryViewHolder.CREATOR, new Object[]{HtmlCompat.fromHtml(mContext.getString(R.string.app_rules_request_directories_link_rules_information)), R.drawable.ic_info_outline_24dp}, ID_LINK_RULES_SUMMARY);
            addObservers(mConfigurationBuilder.getObservers());
            addItem(BUTTON_ROW_CREATOR, null, ID_BUTTON_ROW);
            RecyclerView recyclerView = getRecyclerView();
            if (recyclerView != null) {
                recyclerView.post(() -> recyclerView.smoothScrollToPosition(getItemCount() - 1));
            }
        }

        notifyDataSetChanged();
    }

    private void addHeader(AppInfo appInfo) {
        //addItem(AppDetailHeaderViewHolder.CREATOR, appInfo, ID_HEADER);
    }

    private void addObservers(List<ObserverInfo> observers) {
        for (ObserverInfo observer : observers) {
            addItem(LINK_ITEM_CREATOR, observer, ID_LINK_RULES_ITEM + observer.hashCode());
        }
    }

    @Override
    public void onDelete(ObserverInfo observerInfo) {
        List<ObserverInfo> list = mConfigurationBuilder.getObservers();
        for (int i = 0; i < list.size(); i++) {
            if (Objects.equals(observerInfo.source, list.get(i).source)) {
                list.remove(i);
                updateItems();
                break;
            }
        }
    }

}
