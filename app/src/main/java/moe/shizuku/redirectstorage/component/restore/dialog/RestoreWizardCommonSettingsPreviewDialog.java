package moe.shizuku.redirectstorage.component.restore.dialog;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.utils.SharedPreferencesXmlReader;
import rikka.html.text.HtmlCompat;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public class RestoreWizardCommonSettingsPreviewDialog extends AlertDialogFragment {

    public static RestoreWizardCommonSettingsPreviewDialog newInstance(@NonNull AbstractBackupModel backupModel) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, backupModel);

        RestoreWizardCommonSettingsPreviewDialog dialog = new RestoreWizardCommonSettingsPreviewDialog();
        dialog.setArguments(args);
        return dialog;
    }

    private AbstractBackupModel mBackupModel;
    private SharedPreferencesXmlReader mSharedPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getArguments(), "Arguments cannot be null.");
        mBackupModel = Objects.requireNonNull(getArguments().getParcelable(EXTRA_DATA));
        try {
            mSharedPref = SharedPreferencesXmlReader.parse(mBackupModel.getSRSettingsXML());
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse common settings.", e);
        }
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder, @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.restore_type_common_config);
        builder.setMessage(getMessage());
        builder.setPositiveButton(android.R.string.ok, null);
    }

    private CharSequence getMessage() {
        final StringBuilder sb = new StringBuilder();
        final String kvPairFormat = getString(R.string.dialog_restore_common_settings_kv_format);
        sb.append(String.format(kvPairFormat, getString(R.string.settings_redirect_target), mBackupModel.getDefaultRedirectTarget()));
        sb.append("<br>").append("<br>");
        // FIXME
        sb.append(String.format(kvPairFormat, getString(R.string.settings_mount_dirs_template_title), mBackupModel.getMountDirsTemplates().toString()));
        return HtmlCompat.fromHtml(sb.toString());
    }

}
