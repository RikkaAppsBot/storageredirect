package moe.shizuku.redirectstorage.component.filebrowser.adapter;

import androidx.annotation.NonNull;

import moe.shizuku.redirectstorage.component.filebrowser.viewholder.AppFileBrowserItemViewHolder;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;

public class AppFileBrowserListAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    private Callback mCallback;

    public AppFileBrowserListAdapter(@NonNull FileBrowserUtils.FileBrowserStyleHolder styleHolder) {
        getCreatorPool().putRule(
                ServerFile.class,
                AppFileBrowserItemViewHolder.newCreator(
                        styleHolder, new Callback() {
                            @Override
                            public void onItemClicked(ServerFile item) {
                                mCallback.onItemClicked(item);
                            }

                            @Override
                            public void onItemShareClicked(ServerFile item) {
                                mCallback.onItemShareClicked(item);
                            }

                            @Override
                            public void onItemViewPropertiesClicked(ServerFile item) {
                                mCallback.onItemViewPropertiesClicked(item);
                            }
                        }));
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }

    public interface Callback {

        void onItemClicked(ServerFile item);

        void onItemShareClicked(ServerFile item);

        void onItemViewPropertiesClicked(ServerFile item);

    }

}
