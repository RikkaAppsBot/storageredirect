package moe.shizuku.redirectstorage.component.applist.viewholder;

import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.applist.adapter.FilterChipsAdapter;
import moe.shizuku.redirectstorage.model.FilterItem;
import moe.shizuku.redirectstorage.widget.CheckableChipView;
import rikka.recyclerview.BaseViewHolder;

/**
 * Created by Fung Gwo on 2018/5/3.
 */
public class FilterChipViewHolder extends BaseViewHolder<FilterItem> implements CheckableChipView.OnCheckedChangeListener {

    public static final Creator<FilterItem> CREATOR = (inflater, parent) ->
            new FilterChipViewHolder(inflater.inflate(R.layout.filter_chip_layout, parent, false));

    public static final Object TOGGLE_PAYLOAD = new Object();

    private CheckableChipView mChipView;

    private FilterChipViewHolder(View itemView) {
        super(itemView);

        mChipView = itemView.findViewById(R.id.chip_view);
        mChipView.setOnCheckedChangeListener(this);
    }

    @Override
    public void onBind() {
        mChipView.setTitle(getData().getTitle(itemView.getContext()));
        mChipView.setChecked(getData().isSelected, false, false);
        mChipView.setCheckable(getData().isEditing, false);
        mChipView.updateViewState(false);
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        for (Object payload : payloads) {
            if (TOGGLE_PAYLOAD.equals(payload)) {
                mChipView.setChecked(getData().isSelected, false, false);
                mChipView.setCheckable(getData().isEditing, false);
                mChipView.updateViewState(true);
            } else {
                this.onBind();
            }
        }
    }

    @Override
    public FilterChipsAdapter getAdapter() {
        return (FilterChipsAdapter) super.getAdapter();
    }

    @Override
    public void onCheckedChanged(CheckableChipView view, boolean isChecked) {
        getData().setSelected(isChecked);
        getAdapter().updateItem(getData());
    }
}
