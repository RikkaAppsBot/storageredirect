package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.view.View;
import android.widget.CheckBox;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import moe.shizuku.redirectstorage.viewholder.CheckableViewHolder;
import rikka.html.text.HtmlCompat;

public class EditShowNotificationViewHolder extends CheckableViewHolder<ObserverInfoBuilder> {

    public static final Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditShowNotificationViewHolder holder = new EditShowNotificationViewHolder(
                inflater.inflate(R.layout.observer_info_edit_checkbox_item, parent, false));
        holder.itemView.setEnabled(parent.isEnabled());
        ((CheckBox) holder.checkable).setEnabled(parent.isEnabled());
        return holder;
    };

    private EditShowNotificationViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View view) {
        getData().showNotification(!getData().getShowNotification());
        super.onBind();
    }

    @Override
    public boolean isChecked() {
        return getData().getShowNotification();
    }

    @Override
    public void onBind() {
        super.onBind();
        icon.setImageResource(R.drawable.ic_file_download_24dp);
        title.setText(R.string.link_dialog_message_show_notification);
        summary.setText(HtmlCompat.fromHtml(getContext().getString(R.string.observer_info_edit_show_notification_summary)));
    }
}
