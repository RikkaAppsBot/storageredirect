package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class EditMaskViewHolder extends EditButtonViewHolder<ObserverInfoBuilder, EditMaskViewHolder.Listener> {

    public static final BaseViewHolder.Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditMaskViewHolder holder = new EditMaskViewHolder(
                inflater.inflate(R.layout.observer_info_edit_edit_button, parent, false));
        holder.editButton.setEnabled(parent.isEnabled());
        return holder;
    };

    public interface Listener {

        void onEditMaskClick();
    }

    private EditMaskViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public Drawable getIconDrawable() {
        return itemView.getResources().getDrawable(R.drawable.ic_observer_mask_24dp, itemView.getContext().getTheme());
    }

    @Override
    public CharSequence getTitle() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_mask);
    }

    @Override
    public CharSequence getHint() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_mask_hint);
    }

    @Override
    public void onClick() {
        getListener().onEditMaskClick();
    }

    @Override
    public void onBind() {
        super.onBind();
        if (getData().getMask() != null) {
            editButton.setContentText(HtmlCompat.fromHtml(String.format("<font face=\"monospace\">%s</font>", getData().getMask())));
        } else {
            editButton.setContentText(null);
        }
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        onBind();
    }
}
