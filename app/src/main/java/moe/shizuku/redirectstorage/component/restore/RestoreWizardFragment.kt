package moe.shizuku.redirectstorage.component.restore

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.restore.adapter.RestoreChooserAdapter
import moe.shizuku.redirectstorage.component.restore.dialog.ProcessRestoreDialog
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel.ContentSelector
import moe.shizuku.redirectstorage.component.restore.service.BackupRestoreTaskService
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.utils.ForegroundService
import moe.shizuku.redirectstorage.viewmodel.Status
import rikka.recyclerview.fixEdgeEffect
import java.util.*

class RestoreWizardFragment : AppFragment() {

    companion object {

        fun newInstance(uri: Uri): RestoreWizardFragment {
            val fragment = RestoreWizardFragment()
            val bundle = Bundle()
            bundle.putParcelable(AppConstants.EXTRA_PATH, uri)
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var uri: Uri

    private val adapter: RestoreChooserAdapter by lazy {
        RestoreChooserAdapter().apply {
            setOnChoicesChangeListener { nextButton.isEnabled = this.hasChosen() }
            registerAdapterDataObserver(object : AdapterDataObserver() {
                override fun onChanged() {
                    progressBar.isVisible = itemCount == 0
                }
            })
        }
    }

    private lateinit var progressBar: ProgressBar
    private lateinit var nextButton: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkNotNull(arguments) { "Arguments cannot be null." }

        uri = arguments!!.getParcelable(AppConstants.EXTRA_PATH)!!
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.restore_wizard_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        nextButton = view.findViewById(android.R.id.button1)
        progressBar = view.findViewById(android.R.id.progress)
        val list: RecyclerView = view.findViewById(android.R.id.list)
        list.setHasFixedSize(true)
        adapter.onRestoreInstanceState(savedInstanceState)
        list.adapter = adapter
        list.fixEdgeEffect()

        nextButton.isEnabled = adapter.hasChosen()
        nextButton.setOnClickListener { startRestore() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val model = ViewModelProvider(activity!!).get(RestoreWizardViewModel::class.java)
        if (model.backupModel.value == null) {
            model.startLoadBackup(context!!, uri)
        }
        model.backupModel.observe(this) {
            when (it?.status) {
                //Status.LOADING -> onLoading()
                Status.SUCCESS -> adapter.updateData(it.data)
                Status.ERROR -> {
                    it.error.printStackTrace()
                    InvalidBackupAlertDialog.newInstance(uri).show(childFragmentManager)
                }
                else -> {
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)
    }

    private fun startRestore() {
        val selector = ContentSelector()
        selector.selectedAppConfigurationsUserId = adapter.selectAppsConfigUserId
        selector.selectedCommonSettings = adapter.hasSelectedCommonConfig()
        ProcessRestoreDialog().show(parentFragmentManager, "process_restore")
        val intent = Intent(requireContext(), BackupRestoreTaskService::class.java)
        intent.action = AppConstants.ACTION_RESTORE
        intent.putExtra(AppConstants.EXTRA_DATA, uri)
        intent.putExtra(BackupRestoreTaskService.EXTRA_SELECTOR, selector)
        ForegroundService.startOrNotify(requireContext(), intent)
    }

    class InvalidBackupAlertDialog : AlertDialogFragment() {

        private var uriString: String? = null

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            checkNotNull(arguments) { "Arguments cannot be null." }

            uriString = Objects.requireNonNull(arguments!!.getString(AppConstants.EXTRA_DATA))
        }

        override fun onBuildAlertDialog(builder: AlertDialog.Builder,
                                        savedInstanceState: Bundle?) {
            builder.setTitle(R.string.restore_unsupported_file_title)
            builder.setMessage(getString(R.string.restore_unsupported_file_message, uriString))
            builder.setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int -> requireActivity().finish() }
            builder.setCancelable(false)
        }

        override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
            dialog.setCanceledOnTouchOutside(false)
        }

        companion object {
            fun newInstance(uri: Uri?): InvalidBackupAlertDialog {
                val args = Bundle()
                args.putString(AppConstants.EXTRA_DATA, uri.toString())
                val dialog = InvalidBackupAlertDialog()
                dialog.arguments = args
                return dialog
            }
        }
    }
}