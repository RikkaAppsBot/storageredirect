package moe.shizuku.redirectstorage.component.gallery.viewholder

import android.content.Intent
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.AppConstants.ACTION_PREFIX
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.gallery.adapter.GalleryFolderItemPreviewGridAdapter
import moe.shizuku.redirectstorage.component.gallery.adapter.GalleryFoldersListAdapter
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.event.EventIntents
import rikka.recyclerview.BaseViewHolder

class FolderItemViewHolder(itemView: View) : BaseViewHolder<GalleryFolderSimpleItem>(itemView) {

    companion object {

        const val ACTION_VIEW_FOLDER_INFO = "$ACTION_PREFIX.VIEW_GALLERY_FOLDER_INFO"

        @JvmField
        val CREATOR = BaseViewHolder.Creator<GalleryFolderSimpleItem> { inflater, parent ->
            FolderItemViewHolder(inflater.inflate(R.layout.gallery_folders_list_item, parent, false))
        }

    }

    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val summary: TextView = itemView.findViewById(android.R.id.summary)
    private val text: TextView = itemView.findViewById(android.R.id.text1)
    private val gridList: RecyclerView = itemView.findViewById(R.id.preview_list)
    private val gridAdapter = GalleryFolderItemPreviewGridAdapter(gridList)
    private val gridLayoutManager = gridList.layoutManager as GridLayoutManager

    private var isBindCommonGridPool = false

    init {
        itemView.setOnClickListener {
            LocalBroadcastManager.getInstance(it.context)
                    .sendBroadcast(EventIntents.CommonList.notifyCurrentItemClick(this))
        }
        itemView.findViewById<View>(android.R.id.button1).setOnClickListener {
            LocalBroadcastManager.getInstance(it.context)
                    .sendBroadcast(Intent(ACTION_VIEW_FOLDER_INFO).putExtra(EXTRA_DATA, data))
        }

        gridList.adapter = gridAdapter
        gridList.setHasFixedSize(true)
        gridList.setItemViewCacheSize(gridLayoutManager.spanCount * 2)
        gridList.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

            }

            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return !gridList.isEnabled
            }
        })
    }

    override fun getAdapter(): GalleryFoldersListAdapter {
        return super.getAdapter() as GalleryFoldersListAdapter
    }

    override fun onBind() {
        title.text = data.folderName
        summary.text = context.resources.getQuantityString(
                R.plurals.photo_amount_text_format, data.totalCount, data.totalCount)

        if (!isBindCommonGridPool) {
            isBindCommonGridPool = true
            gridList.setRecycledViewPool(adapter.gridViewPool)
        }
        val viewModels = data.getPreviewModels(gridLayoutManager.spanCount * 2)
        gridAdapter.setItems(viewModels)
        gridAdapter.notifyDataSetChanged()

        if (data.inMountDirs) {
            text.visibility = View.GONE
            gridList.isEnabled = true
            icon.isEnabled = true
            title.isEnabled = true
            summary.isEnabled = true
        } else {
            text.visibility = View.VISIBLE
            gridList.isEnabled = false
            icon.isEnabled = false
            title.isEnabled = false
            summary.isEnabled = false
        }
    }

}