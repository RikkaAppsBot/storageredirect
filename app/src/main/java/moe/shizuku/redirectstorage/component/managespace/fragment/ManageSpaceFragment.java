package moe.shizuku.redirectstorage.component.managespace.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.AppFragment;
import moe.shizuku.redirectstorage.component.managespace.adapter.ManageSpaceAdapter;
import moe.shizuku.redirectstorage.component.managespace.viewholder.ClickableItemViewHolder;
import moe.shizuku.redirectstorage.event.EventCallbacks;
import moe.shizuku.redirectstorage.utils.SuShell;
import rikka.recyclerview.RecyclerViewKt;

public class ManageSpaceFragment extends AppFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerLocalBroadcastReceiver(EventCallbacks.CommonList
                .whenItemClickWithInteger(ClickableItemViewHolder.class, id -> {
                    switch (id) {
                        case ManageSpaceAdapter.ID_CLIENT_RESET_DATA: {
                            onClearClientItemClick();
                            break;
                        }
                        case ManageSpaceAdapter.ID_SERVER_RESET_DATA_ONLY: {
                            onClearServerConfigOnlyItemClick();
                            break;
                        }
                        case ManageSpaceAdapter.ID_SERVER_CLEAR_ALL: {
                            onClearServerAllDataItemClick();
                            break;
                        }
                    }
                }));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = view.findViewById(android.R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ManageSpaceAdapter());
        RecyclerViewKt.addVerticalPadding(recyclerView, 8, 8);
        RecyclerViewKt.fixEdgeEffect(recyclerView, true, true);
    }

    private void onClearClientItemClick() {
        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.manage_space_client_data_reset_dialog_title)
                .setMessage(R.string.manage_space_client_data_reset_dialog_message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    try {
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec("pm clear " + BuildConfig.APPLICATION_ID);
                    } catch (Exception ignored) {
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void onClearServerConfigOnlyItemClick() {
        final SRManager sm = SRManager.create();
        if (sm == null) {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            return;
        }

        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.manage_space_server_reset_config_only_dialog_title)
                .setMessage(getString(R.string.manage_space_server_reset_config_only_dialog_message, getString(R.string.app_name)))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    try {
                        sm.exit(1);
                    } catch (Throwable ignored) {
                    }
                    requireActivity().finish();
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void onClearServerAllDataItemClick() {
        final SRManager sm = SRManager.create();
        if (sm == null) {
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_SHORT).show();
            return;
        }

        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.manage_space_server_delete_all_data_dialog_title)
                .setMessage(getString(R.string.manage_space_server_delete_all_data_dialog_message, getString(R.string.app_name)))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    try {
                        sm.exit(1);
                    } catch (Throwable ignored) {
                    }
                    requireActivity().finish();
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

}
