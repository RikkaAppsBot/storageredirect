package moe.shizuku.redirectstorage.component.webview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.StringRes
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppActivity

class WebViewActivity : AppActivity() {

    companion object {

        @JvmStatic
        fun start(context: Context, uri: Uri, @StringRes title: Int) {
            start(context, uri, context.getString(title))
        }

        @JvmStatic
        fun start(context: Context, uri: Uri, title: String) {
            context.startActivity(Intent(context, WebViewActivity::class.java)
                    .setData(uri)
                    .putExtra(Intent.EXTRA_TITLE, title))
        }

    }

    private lateinit var webView: WebView

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val targetUrl: String = intent?.data?.toString() ?: run {
            finish()
            return
        }

        supportActionBar?.let {
            intent.getStringExtra(Intent.EXTRA_TITLE)?.let { titleExtra ->
                it.title = titleExtra
            }
            it.subtitle = targetUrl

            it.setHomeAsUpIndicator(R.drawable.helplib_close_24dp)
            it.setDisplayHomeAsUpEnabled(true)
        }


        webView = WebView(this)
        webView.loadUrl(targetUrl)
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.startsWith("storage-redirect-client")) {
                    return false
                }

                supportActionBar?.subtitle = url

                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                supportActionBar?.subtitle = url
                supportActionBar?.title = view.title
            }
        }

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true

        setContentView(webView)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
