package moe.shizuku.redirectstorage.component.home.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.util.Date;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.AppBarDialogFragment;
import moe.shizuku.redirectstorage.model.DebugInfo;
import rikka.html.text.HtmlCompat;
import rikka.material.app.AppBar;

public class DebugInfoDialog extends AppBarDialogFragment {

    public static DebugInfoDialog newInstance() {
        Bundle args = new Bundle();
        DebugInfoDialog fragment = new DebugInfoDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private DebugInfo mDebugInfo;

    @Override
    public void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SRManager srm = SRManager.create();
        if (srm != null) {
            try {
                mDebugInfo = srm.getDebugInfo(0);
            } catch (RemoteException ignored) {
            }
        }

        if (mDebugInfo == null) {
            dismiss();
            Toast.makeText(requireContext(), R.string.toast_unable_connect_server, Toast.LENGTH_LONG).show();
        }
    }

    @Nullable
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_debug_info, container, false);
    }

    @Override
    public void onContentViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (mDebugInfo == null)
            return;

        final Context context = view.getContext();
        TextView textView = view.findViewById(android.R.id.text1);

        StringBuilder exceptions = new StringBuilder();
        if (mDebugInfo.exceptions != null) {
            for (String s : mDebugInfo.exceptions) {
                exceptions.append(s.replaceAll("\n", "<br>")).append("<p>");
            }
        }

        textView.setText(HtmlCompat.fromHtml(context.getString(R.string.dialog_debug_info_message,
                mDebugInfo.mediaPathFromNative,
                mDebugInfo.configFromFile ? "true" : "false",
                DateFormat.getDateTimeInstance().format(new Date(mDebugInfo.lastConfigSaved)),
                exceptions
        ), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE));
    }

    @Override
    public void onAppBarCreated(@NotNull AppBar appBar, @Nullable Bundle savedInstanceState) {
        appBar.setTitle(R.string.action_debug_info);
        appBar.setDisplayHomeAsUpEnabled(true);
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp);
    }
}
