package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.dialog.EditTextDialogFragment;
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver;
import moe.shizuku.redirectstorage.event.EventCallbacks;
import rikka.core.compat.CollectionsCompat;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.RecyclerViewKt;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public final class ObserverMaskTemplatesDialog extends AlertDialogFragment {

    private static final int CHOICE_INCLUDE_FORMATS = 0;
    private static final int CHOICE_EXCLUDE_FORMATS = 1;
    private static final int CHOICE_INCLUDE_MEDIA_FORMATS = 2;

    private static final List<String> MEDIA_FORMATS =
            Arrays.asList("jpg", "bmp", "gif", "png", "webp", "jpeg");

    private static final List<Character> REGEX_ESCAPABLE_CHARS =
            Arrays.asList('$', '(', ')', '*', '+', '.', '[', ']',
                    '?', '\\', '^', '{', '}', '|', '!', '_');

    private static String escapeKeywordForRegex(String keyword) {
        List<String> chars = new ArrayList<>(keyword.length());
        for (int i = 0; i < keyword.length(); i++) {
            char c = keyword.charAt(i);
            boolean contains = false;
            for (Character ch : REGEX_ESCAPABLE_CHARS) {
                if (c == ch) {
                    contains = true;
                    break;
                }
            }
            if (contains) {
                chars.add(i, "\\" + c);
            } else {
                chars.add(i, "" + c);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String c : chars) {
            sb.append(c);
        }
        return sb.toString();
    }

    private void sendResult(@Nullable String value) {
        if (getParentFragment() instanceof ExportFolderEditFragment) {
            ((ExportFolderEditFragment) getParentFragment()).onMaskSet(value);
        }
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                   @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.observer_mask_template_select_dialog_title);
        builder.setItems(R.array.observer_mask_templates, (dialog, which) -> {
            switch (which) {
                case CHOICE_INCLUDE_FORMATS: {
                    new IncludeFormatsTemplateDialog().show(getParentFragmentManager());
                    break;
                }
                case CHOICE_EXCLUDE_FORMATS: {
                    new ExcludeFormatsTemplateDialog().show(getParentFragmentManager());
                    break;
                }
                case CHOICE_INCLUDE_MEDIA_FORMATS: {
                    if (getParentFragment() instanceof ExportFolderEditFragment) {
                        ((ExportFolderEditFragment) getParentFragment()).onMaskSet(IncludeFormatsTemplateDialog.buildRegex(MEDIA_FORMATS));
                    }
                    break;
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
    }

    private static abstract class BaseFormatSelectorDialog extends AlertDialogFragment {

        private static final String STATE_FORMATS = BuildConfig.APPLICATION_ID +
                ".state.FORMATS";

        private List<String> mFormats;

        private TextView mSummary;
        private RecyclerView mRecyclerView;

        private final MaskTemplateFormatSelectorAdapter mAdapter =
                new MaskTemplateFormatSelectorAdapter();

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (savedInstanceState != null) {
                mFormats = savedInstanceState.getStringArrayList(STATE_FORMATS);
            } else {
                mFormats = new ArrayList<>();
            }

            registerLocalBroadcastReceiver(EventCallbacks
                    .CommonList
                    .whenItemClick(MaskTemplateFormatSelectorAdapter.AddItemViewHolder.class,
                            () -> ListAddDialog.newInstance(mFormats).show(getParentFragmentManager()))
            );
            registerLocalBroadcastReceiver(EventCallbacks
                    .CommonList
                    .whenItemClickWithString(
                            MaskTemplateFormatSelectorAdapter.FormatItemViewHolder.class,
                            value -> {
                                mFormats.remove(value);
                                setFormats(mFormats);
                            })
            );
            registerLocalBroadcastReceiver(ActionBroadcastReceiver.createReceiver(
                    ListAddDialog.ACTION_ADD_ITEM,
                    (context, intent) -> {
                        mFormats.add(intent.getStringExtra(EXTRA_DATA));
                        setFormats(mFormats);
                    })
            );
        }

        @Override
        public void onSaveInstanceState(@NonNull Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putStringArrayList(STATE_FORMATS,
                    CollectionsCompat.cloneArrayList(mFormats).get());
        }

        @Override
        public int onGetContentViewLayoutResource() {
            return R.layout.observer_info_mask_template_format_selector_dialog_content;
        }

        @CallSuper
        @Override
        public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                       @Nullable Bundle savedInstanceState) {
            builder.setTitle(R.string.observer_mask_template_apply_dialog_title);
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
                if (mFormats == null || mFormats.isEmpty()) {
                    throw new IllegalStateException("Positive button should not be clickable " +
                            "when format list is empty or null.");
                }
                onDone(mFormats);
            });
            builder.setNegativeButton(android.R.string.cancel, null);
        }

        @Override
        public void onAlertDialogCreated(@NonNull AlertDialog dialog,
                                         View contentView, @Nullable Bundle savedInstanceState) {
            mSummary = contentView.findViewById(android.R.id.summary);
            mRecyclerView = contentView.findViewById(android.R.id.list);

            RecyclerViewKt.fixEdgeEffect(mRecyclerView, true, true);

            mRecyclerView.setAdapter(mAdapter);

            mSummary.setText(getSummary());

            setFormats(mFormats);
        }

        @Override
        public void onShow(AlertDialog dialog) {
            super.onShow(dialog);
            updatePositiveButtonState();
        }

        public void setFormats(@NonNull List<String> formats) {
            // List deduplication
            mFormats = new ArrayList<>(new HashSet<>(formats));

            mAdapter.setData(mFormats);

            updatePositiveButtonState();
        }

        private void updatePositiveButtonState() {
            if (getPositiveButton() != null) {
                getPositiveButton().setEnabled(mFormats != null && !mFormats.isEmpty());
            }
        }

        /**
         * Describe this template
         *
         * @return Template description
         */
        @NonNull
        public abstract CharSequence getSummary();

        /**
         * Return selector result.
         *
         * @param formats Formats list (It won't be empty or null)
         */
        public abstract void onDone(@NonNull List<String> formats);

        public static class ListAddDialog extends EditTextDialogFragment {

            public static ListAddDialog newInstance(@NonNull List<String> currentList) {
                final Bundle args = new Bundle();
                args.putStringArrayList(EXTRA_DATA,
                        CollectionsCompat.cloneArrayList(currentList).get());

                final ListAddDialog dialog = new ListAddDialog();
                dialog.setArguments(args);
                return dialog;
            }

            private static final String ACTION_ADD_ITEM = ListAddDialog.class.getName() +
                    ".action.ADD_ITEM";

            private static final InputFilter INPUT_FILTER = (source, start, end, dest, dstart, dend) -> {
                try {
                    final StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < source.length(); i++) {
                        char c = source.charAt(i);
                        if (c == '。') {
                            sb.append('.');
                        } else if (c != '\\' && c != '?' && c != '%' && c != '*') {
                            sb.append(c);
                        }
                    }
                    return sb.toString().toLowerCase();
                } catch (Exception ignored) {

                }
                return null;
            };

            private List<String> mCurrentList;

            @Override
            public void onCreate(@Nullable Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                mCurrentList = requireArguments().getStringArrayList(EXTRA_DATA);
            }

            @Override
            public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                           @Nullable Bundle savedInstanceState) {
                super.onBuildAlertDialog(builder, savedInstanceState);
                builder.setTitle(R.string.observer_mask_add_format_suffix_dialog);
            }

            @Override
            public void onAlertDialogCreated(@NonNull AlertDialog dialog,
                                             View contentView,
                                             @Nullable Bundle savedInstanceState) {
                super.onAlertDialogCreated(dialog, contentView, savedInstanceState);
                getEditText().setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                getEditText().setImeOptions(EditorInfo.IME_FLAG_FORCE_ASCII);
                getEditText().setFilters(new InputFilter[]{
                        INPUT_FILTER,
                        new InputFilter.LengthFilter(20)
                });
                getEditText().setSingleLine();
                getEditText().addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int beg, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int beg, int before, int count) {
                        updatePositiveButtonState();
                    }
                });
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    getEditText().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO);
                }
                getHelperText().setVisibility(View.VISIBLE);
                getHelperText().setText(HtmlCompat.fromHtml(getString(R.string.observer_mask_add_format_suffix_description)));
            }

            @Override
            public void onShow(AlertDialog dialog) {
                super.onShow(dialog);
                updatePositiveButtonState();
            }

            public void updatePositiveButtonState() {
                if (getPositiveButton() != null) {
                    final String str = getValue();
                    boolean acceptable = !TextUtils.isEmpty(str)
                            && !mCurrentList.contains(str);

                    getPositiveButton().setEnabled(acceptable);
                }
            }

            @Override
            public String getValue() {
                String s = super.getValue();
                if (TextUtils.isEmpty(s)) {
                    return s;
                }
                // TODO: while or if?
                if (s.startsWith(".")) {
                    s = s.substring(1);
                }
                return s;
            }

            @Override
            public void onDone() {
                getLocalBroadcastManager().sendBroadcast(
                        new Intent(ACTION_ADD_ITEM).putExtra(EXTRA_DATA, getValue())
                );
            }

        }

    }

    public static final class IncludeFormatsTemplateDialog extends BaseFormatSelectorDialog {

        public static String buildRegex(@NonNull List<String> formats) {
            if (Objects.requireNonNull(formats, "Formats cannot be null.").isEmpty()) {
                throw new IllegalArgumentException("Formats cannot be empty");
            }
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = formats.iterator();
            String item;
            while (!TextUtils.isEmpty(item = iterator.next())) {
                sb.append(escapeKeywordForRegex(item));
                if (iterator.hasNext()) {
                    sb.append("|");
                } else {
                    break;
                }
            }
            return ".+\\.(" + sb + ")$";
        }

        @NonNull
        @Override
        public CharSequence getSummary() {
            return getString(R.string.observer_mask_include_suffix_template);
        }

        @Override
        public void onDone(@NonNull List<String> formats) {
            if (getParentFragment() instanceof ExportFolderEditFragment) {
                ((ExportFolderEditFragment) getParentFragment()).onMaskSet(buildRegex(formats));
            }
        }

    }

    public static final class ExcludeFormatsTemplateDialog extends BaseFormatSelectorDialog {

        public static String buildRegex(@NonNull List<String> formats) {
            if (Objects.requireNonNull(formats, "Formats cannot be null.").isEmpty()) {
                throw new IllegalArgumentException("Formats cannot be empty");
            }
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = formats.iterator();
            String item;
            while (!TextUtils.isEmpty(item = iterator.next())) {
                sb.append(escapeKeywordForRegex("." + item));
                if (iterator.hasNext()) {
                    sb.append("|");
                } else {
                    break;
                }
            }
            return ".+(?<!" + sb + ")$";
        }

        @NonNull
        @Override
        public CharSequence getSummary() {
            return getString(R.string.observer_mask_exclude_suffix_template);
        }

        @Override
        public void onDone(@NonNull List<String> formats) {
            if (getParentFragment() instanceof ExportFolderEditFragment) {
                ((ExportFolderEditFragment) getParentFragment()).onMaskSet(buildRegex(formats));
            }
        }

    }

}
