package moe.shizuku.redirectstorage.component.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.app.AppBarFragment
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.ComponentActivity
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.license.AlipayHelper
import moe.shizuku.redirectstorage.license.GoogleBillingHelper
import moe.shizuku.redirectstorage.license.License
import moe.shizuku.redirectstorage.utils.ItemSpacingDecoration
import moe.shizuku.redirectstorage.utils.ViewUtils
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rikka.core.util.IntentUtils
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.internal.billingclient.app.PaymentAdapter
import rikka.internal.billingclient.app.PaymentButtonInfo
import rikka.internal.billingclient.app.PaymentFeatureInfo
import rikka.internal.help.HelpProvider
import rikka.internal.payment.model.AlipayProductInfo
import rikka.material.app.AppBar
import rikka.recyclerview.fixEdgeEffect

class PurchaseFragment : AppBarFragment() {

    companion object {

        @JvmStatic
        fun startActivity(context: Context) {
            context.startActivity(ComponentActivity.newIntent(PurchaseFragment::class.java, null))
        }
    }

    private var alipayInfo: AlipayProductInfo? = null
    private val adapter: PaymentAdapter by lazy { PaymentAdapter() }
    private var alipayButton: PaymentButtonInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerLocalBroadcastReceiver(IntentFilter(AppConstants.ACTION_PURCHASED_CHANGED)) { _: Context?, _: Intent? ->
            if (activity == null)
                requireActivity().finish()
        }
        setHasOptionsMenu(true)
    }

    override fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {
        appBar.setDisplayHomeAsUpEnabled(true)
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        appBar.setTitle(R.string.unlock_title)
    }

    private fun requestAlipayInfo(context: Context) {
        context.application.paymentService.getAlipayInfo(License.PACKAGE_NAME, License.PRODUCT_ID)
                .enqueue(object : Callback<AlipayProductInfo?> {
                    override fun onResponse(call: Call<AlipayProductInfo?>, response: Response<AlipayProductInfo?>) {
                        if (response.code() != 200 || response.body() == null) {
                            return
                        }
                        alipayInfo = response.body()
                        AlipayHelper.setPrice(alipayInfo!!.price + " CNY")
                        val price = if (AlipayHelper.getPrice() != null) HtmlCompat.fromHtml(AlipayHelper.getPrice()) else ""
                        if (alipayButton != null) {
                            for (i in 0 until adapter.itemCount) {
                                if (adapter.getItemAt<Any>(i) === alipayButton) {
                                    alipayButton = PaymentButtonInfo(alipayButton!!.drawableRes, alipayButton!!.label, price, alipayButton!!.listener)
                                    adapter.getItems<Any>()[i] = alipayButton
                                    break
                                }
                            }
                            adapter.notifyDataSetChanged()
                        }
                    }

                    override fun onFailure(call: Call<AlipayProductInfo?>, t: Throwable) {}
                })
    }

    private fun onAlipayClicked(view: View) {
        val context = view.context
        if (alipayInfo == null) {
            if (activity != null && !requireActivity().isFinishing) {
                if (!(activity as AppActivity?)!!.showNetworkBlockedDialog()) {
                    try {
                        AlertDialog.Builder(requireActivity())
                                .setMessage(R.string.payment_alipay_not_ready)
                                .setPositiveButton(android.R.string.ok, null)
                                .setCancelable(false)
                                .show()
                        Settings.preferences.edit().putBoolean("db", false).apply()
                    } catch (ignored: Throwable) {
                    }
                }
            }
            return
        }
        AlipayDialogFragment.newInstance(License.PRODUCT_ID, alipayInfo).show(childFragmentManager, AlipayDialogFragment::class.java.name)
    }

    private fun onGooglePlayClicked(view: View) {
        val context = view.context
        if (!GoogleBillingHelper.isGooglePlayAvailable(context)) {
            AlertDialog.Builder(context)
                    .setMessage(R.string.billing_google_play_unavailable)
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
            return
        }
        view.isEnabled = false

        Toast.makeText(context, R.string.toast_please_wait, Toast.LENGTH_SHORT).show()

        val callback = object : Callback<ResponseBody?> {

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (isStateSaved) {
                    return
                }
                view.isEnabled = true
                if (response.code() != 200) {
                    onFailure(call, RuntimeException("Bad response, code=" + response.code()))
                    return
                }
                val activity: Activity? = activity
                if (activity != null && !activity.isFinishing) {
                    GoogleBillingHelper.startPurchase(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                if (BuildConfig.DEBUG) {
                    t.printStackTrace()
                }
                if (isStateSaved) {
                    return
                }
                view.isEnabled = true
                if (context is Activity && !context.isFinishing) {
                    try {
                        if (activity != null) {
                            (activity as AppActivity?)!!.showNetworkBlockedDialog()
                        }
                        AlertDialog.Builder(context)
                                .setMessage(context.getString(R.string.dialog_network_not_available_message) + "\n\n" + t.message)
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                        return
                    } catch (ignored: Throwable) {
                    }
                }
                Toast.makeText(context.applicationContext, R.string.dialog_network_not_available_message, Toast.LENGTH_SHORT).show()
            }
        }
        context.application.paymentService.ping().enqueue(callback)
    }

    private fun onOtherClicked() {
        val context = activity ?: return
        var download: String? = null
        val entity = HelpProvider.get(context, "download")
        if (entity != null) {
            download = entity.content.get()
        }
        if (download == null) {
            download = "https://sr.rikka.app/download.html"
        }
        val dialog = AlertDialog.Builder(context)
                .setMessage(HtmlCompat.fromHtml(context.getString(R.string.dialog_billing_method_other).replace("WEBSITE_URL", download)))
                .setPositiveButton(android.R.string.ok, null)
                .show()
        (dialog.findViewById<View>(android.R.id.message) as TextView?)!!.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun onMultipleAccountClicked() {
        val context = activity ?: return
        AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.dialog_billing_multiple_google_account).toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(android.R.string.ok, null)
                .show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.appbar_container_recycler, container, false)
        ViewUtils.setPaddingHorizontal(view, resources.getDimension(R.dimen.rd_activity_horizontal_margin).toInt())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val isGoogle = GoogleBillingHelper.isInstallerGooglePlay(context)
        val showAlipay = !isGoogle || AlipayHelper.isAlipayInstalled(context)

        val recyclerView = view as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = PaymentAdapter()
        val features = PaymentAdapter()
        features.getItems<Any>().add(getString(R.string.payment_summary))
        features.getItems<Any>().add(PaymentFeatureInfo(
                R.drawable.ic_payment_unlimted_phone_24dp,
                getString(R.string.payment_feature_4),
                getString(R.string.payment_feature_4_summary)))
        features.getItems<Any>().add(PaymentFeatureInfo(
                R.drawable.ic_payment_unlock_24dp,
                getString(R.string.payment_feature_1),
                getString(R.string.payment_feature_1_summary)))
        features.getItems<Any>().add(PaymentFeatureInfo(
                R.drawable.ic_payment_file_monitor_24dp,
                getString(R.string.payment_feature_3),
                HtmlCompat.fromHtml(getString(R.string.payment_feature_3_summary, getString(R.string.enhance_module)))))
        features.getItems<Any>().add(PaymentFeatureInfo(
                R.drawable.ic_payment_support_24dp,
                getString(R.string.payment_feature_2),
                getString(R.string.payment_feature_2_summary)))

        this.adapter.getItems<Any>().add(getString(R.string.billing_method_summary))
        this.adapter.getItems<Any>().add(PaymentButtonInfo(
                R.drawable.ic_billingclient_google_play_24dp,
                context.getString(R.string.billing_method_google_play),
                GoogleBillingHelper.getPrice(), OnClickListener { v: View -> onGooglePlayClicked(v) }))

        if (isGoogle && !showAlipay || BuildConfig.DEBUG) {
            val otherLabel: CharSequence = HtmlCompat.fromHtml(context.getString(R.string.billing_method_other))
            val otherButton = PaymentButtonInfo(
                    R.drawable.ic_billingclient_help_24dp,
                    otherLabel,
                    "") { onOtherClicked() }
            this.adapter.getItems<Any>().add(otherButton)
        }
        if (showAlipay || BuildConfig.DEBUG) {
            requestAlipayInfo(context)
            val label: CharSequence = context.getString(R.string.billing_method_alipay)
            alipayButton = PaymentButtonInfo(
                    R.drawable.ic_billingclient_alipay_24dp,
                    label,
                    if (AlipayHelper.getPrice() != null) HtmlCompat.fromHtml(AlipayHelper.getPrice()) else "", OnClickListener { v: View -> onAlipayClicked(v) })
            this.adapter.getItems<Any>().add(alipayButton)
        }
        this.adapter.getItems<Any>().add(getString(R.string.billing_have_problem))
        if (isGoogle || BuildConfig.DEBUG) {
            this.adapter.getItems<Any>().add(PaymentButtonInfo(
                    R.drawable.ic_billingclient_help_24dp,
                    context.getString(R.string.billing_multiple_google_account).toHtml(),
                    "") { onMultipleAccountClicked() })
        }
        this.adapter.getItems<Any>().add(PaymentButtonInfo(
                R.drawable.ic_help_outline_24dp,
                context.getString(R.string.billing_view_help),
                null,
                OnClickListener { v: View ->
                    val context1 = v.context
                    val entity = HelpProvider.get(context1, "payment")
                    if (entity != null) {
                        entity.startActivity(context1)
                    } else {
                        Toast.makeText(context1, R.string.toast_cannot_load_help, Toast.LENGTH_SHORT).show()
                    }
                }
        ))
        this.adapter.getItems<Any>().add(PaymentButtonInfo(
                R.drawable.ic_billingclient_email_24dp,
                context.getString(R.string.action_contact),
                null,
                OnClickListener { _: View? ->
                    val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", BuildConfig.SUPPORT_MAIL, null))
                            .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " payment support")
                    IntentUtils.startActivity(context, intent, getString(R.string.chooser_no_apps))
                }
        ))
        val redeem = PaymentAdapter()
        redeem.getItems<Any>().add(getString(R.string.billing_redeem_summary))
        redeem.getItems<Any>().add(PaymentButtonInfo(
                R.drawable.ic_billingclient_redeem_24dp,
                getString(R.string.billing_redeem),
                null,
                OnClickListener { _: View? ->
                    val dialog = RedeemDialogFragment()
                    dialog.show(parentFragmentManager, "redeem")
                }
        ))
        adapter.getItems<Any>().add(features)
        adapter.getItems<Any>().add(this.adapter)
        adapter.getItems<Any>().add(redeem)
        recyclerView.adapter = adapter
        recyclerView.fixEdgeEffect()
        recyclerView.addItemDecoration(ItemSpacingDecoration(context, 8))
    }
}