package moe.shizuku.redirectstorage.component.restore

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppActivity
import rikka.core.util.ResourceUtils

class RestoreWizardActivity : AppActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.helplib_close_24dp)

        val uri = intent.data!!
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, RestoreWizardFragment.newInstance(uri))
                    .commit()
        }
    }

    override fun onApplyTranslucentSystemBars() {
        super.onApplyTranslucentSystemBars()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.navigationBarColor = ResourceUtils.resolveColor(theme, android.R.attr.colorBackground)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            window.navigationBarDividerColor = Color.TRANSPARENT
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}