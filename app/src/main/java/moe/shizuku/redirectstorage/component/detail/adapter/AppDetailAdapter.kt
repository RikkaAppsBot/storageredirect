package moe.shizuku.redirectstorage.component.detail.adapter

import android.content.Context
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.RecyclerViewAdapterHelper
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import moe.shizuku.redirectstorage.component.detail.viewholder.*
import moe.shizuku.redirectstorage.dao.SRDatabase
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.UserHelper
import rikka.html.text.HtmlCompat
import rikka.recyclerview.IdBasedRecyclerViewAdapter

class AppDetailAdapter : IdBasedRecyclerViewAdapter() {

    companion object {
        private const val ID_START = 1L
        private const val ID_HEADER = ID_START + 1
        private const val ID_HEADER_DIVIDER = ID_START + 2
        private const val ID_NO_WRITE_APP = ID_START + 3
        private const val ID_RECOMMENDATION = ID_START + 4
        private const val ID_REDIRECT_TOGGLE = ID_START + 5
        private const val ID_REDIRECT_TOGGLE_DIVIDER = ID_START + 6
        private const val ID_BASIC_HELP = ID_START + 7
        private const val ID_REDIRECT_TARGET = ID_START + 8
        private const val ID_VERIFIED_MESSAGE = ID_START + 9
        private const val ID_OBSERVERS = ID_START + 10
        private const val ID_DIVIDER_ADVANCED = ID_START + 11
        private const val ID_DIVIDER_TITLE = ID_START + 12
        private const val ID_DIVIDER_ENHANCED_MODE = ID_START + 13
        private const val ID_ENHANCED_MODE_TITLE = ID_START + 14
        private const val ID_FIX_INTERACTION = ID_START + 15
        private const val ID_ACCESSIBLE_FOLDERS = ID_START + 16
        private const val ID_SHARED_USER_ID = ID_START + 17
        private const val ID_USER_UNAVAILABLE_DIVIDER = ID_START + 18
        private const val ID_USER_UNAVAILABLE = ID_START + 19

        private const val ID_MEDIA_BROWSER = AppDetailItemViewHolder.ID_MEDIA_BROWSER
        private const val ID_VIEW_FILE_HISTORY = AppDetailItemViewHolder.ID_VIEW_FILE_HISTORY
        private const val ID_SHARE_CONFIGURATION = AppDetailItemViewHolder.ID_SHARE_CONFIGURATION
        private const val ID_FOLDER_ANALYSIS = AppDetailItemViewHolder.ID_FOLDER_ANALYSIS
        private const val ID_FILE_BROWSER = AppDetailItemViewHolder.ID_FILE_BROWSER
        private const val ID_HELP = AppDetailItemViewHolder.ID_HELP
    }

    var ignoredVerified = false
    var recommendationExpanded = false

    init {
        setHasStableIds(true)
    }

    interface Listener : AppDetailAccessibleFoldersViewHolder.Listener,
            AppDetailExportFoldersViewHolder.Listener,
            AppDetailItemViewHolder.Listener,
            AppDetailVerifiedMessageViewHolder.Listener,
            AppDetailRecommendationViewHolder.Listener,
            AppDetailHeaderSharedUserIdViewHolder.Listener

    private lateinit var srDatabase: SRDatabase

    lateinit var appInfo: AppInfo
        private set

    private fun showVerifiedMessage(appInfo: AppInfo): Boolean {
        return appInfo.isVerified() && !ignoredVerified && !appInfo.isRedirectEnabled
    }

    @Synchronized
    fun updateData(context: Context, viewModel: AppDetailViewModel) {
        srDatabase = context.application.database
        appInfo = viewModel.appInfo.value!!.data!!

        val recommendationStatusRes = viewModel.recommendation.value

        clear()

        while (true) {
            // header
            if (appInfo.sharedUserId != null) {
                addItem(AppDetailHeaderSharedUserIdViewHolder.CREATOR, viewModel, ID_SHARED_USER_ID)
            } else {
                addItem(AppDetailHeaderViewHolder.CREATOR, appInfo, ID_HEADER)
            }
            // verified
            if (appInfo.writePermissionOrSharedUserId) {
                if (Settings.isOnlineRulesEnabled) {
                    addDivider(ID_HEADER_DIVIDER)

                    if (showVerifiedMessage(appInfo)) {
                        addItem(AppDetailVerifiedMessageViewHolder.CREATOR, null, ID_VERIFIED_MESSAGE)
                        break
                    }
                    addItem(AppDetailRecommendationViewHolder.CREATOR, recommendationStatusRes, ID_RECOMMENDATION)
                }
            } else {
                addDivider(ID_HEADER_DIVIDER)
                addItem(AppDetailSummaryViewHolder.CREATOR, arrayOf(HtmlCompat.fromHtml(context.getString(R.string.detail_no_write_permission_app), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE), R.drawable.ic_info_outline_24dp), ID_NO_WRITE_APP)
            }

            if (!viewModel.userStorageAvailable) {
                addDivider(ID_USER_UNAVAILABLE_DIVIDER)
                addItem(AppDetailSummaryViewHolder.CREATOR, arrayOf(HtmlCompat.fromHtml(context.getString(R.string.detail_user_storage_unavailable, context.getString(R.string.user_format_default, appInfo.userId)), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE), R.drawable.ic_info_outline_24dp), ID_USER_UNAVAILABLE)
            }

            // basic
            addDivider(ID_REDIRECT_TOGGLE_DIVIDER)
            addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, R.string.detail_category_basic_title, ID_BASIC_HELP)
            //addItem(AppDetailItemViewHolder.CREATOR, ID_HELP, ID_HELP.toLong())
            addItem(AppDetailRedirectToggleViewHolder.CREATOR, appInfo, ID_REDIRECT_TOGGLE)

            if (viewModel.userStorageAvailable) {
                // accessible folders
                addItem(AppDetailAccessibleFoldersViewHolder.CREATOR, viewModel, ID_ACCESSIBLE_FOLDERS)

                // observers
                if (appInfo.writePermissionOrSharedUserId || viewModel.observers.value?.data?.isNotEmpty() == true) {
                    addItem(AppDetailExportFoldersViewHolder.CREATOR, viewModel, ID_OBSERVERS)
                }
            }

            // enhanced mode
            addDivider(ID_DIVIDER_ENHANCED_MODE)
            addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, R.string.detail_enhanced_mode, ID_ENHANCED_MODE_TITLE)
            addItem(AppDetailEnhancedModeFixInteractionViewHolder.CREATOR, appInfo, ID_FIX_INTERACTION)

            // advanced
            addDivider(ID_DIVIDER_ADVANCED)
            addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, R.string.detail_category_advanced_title, ID_DIVIDER_TITLE)

            if (viewModel.userStorageAvailable) {
                if (appInfo.writePermissionOrSharedUserId) {
                    addItem(AppDetailRedirectTargetViewHolder.CREATOR, appInfo, ID_REDIRECT_TARGET)
                }
                if (appInfo.userId == UserHelper.myUserId()/* && !BuildUtils.atLeastR()*/) {
                    addItem(AppDetailItemViewHolder.CREATOR, ID_MEDIA_BROWSER, ID_MEDIA_BROWSER.toLong())
                }
                addItem(AppDetailItemViewHolder.CREATOR, ID_FILE_BROWSER, ID_FILE_BROWSER.toLong())
            }

            addItem(AppDetailItemViewHolder.CREATOR, ID_VIEW_FILE_HISTORY, ID_VIEW_FILE_HISTORY.toLong())

            if (viewModel.userStorageAvailable) {
                if (appInfo.writePermissionOrSharedUserId) {
                    addItem(AppDetailItemViewHolder.CREATOR, ID_FOLDER_ANALYSIS, ID_FOLDER_ANALYSIS.toLong())
                    //addItem(AppDetailItemViewHolder.CREATOR, ID_SHARE_CONFIGURATION, ID_SHARE_CONFIGURATION.toLong());
                }
            }

            break
        }

        notifyDataSetChanged()
    }

    private fun addDivider(id: Long) {
        RecyclerViewAdapterHelper.addDivider(this, id)
    }

    fun notifyConfigChanged(context: Context, configChangedType: Int, viewModel: AppDetailViewModel) {
        when (configChangedType) {
            AppInfo.CONFIG_CHANGED_TYPE_REDIRECT_TARGET -> {
                notifyItemChangeById(ID_REDIRECT_TARGET, AppConstants.PAYLOAD)
            }
            AppInfo.CONFIG_CHANGED_TYPE_ACCESSIBLE_FOLDERS -> {
                notifyItemChangeById(ID_ACCESSIBLE_FOLDERS, AppConstants.PAYLOAD)
            }
            AppInfo.CONFIG_CHANGED_TYPE_OBSERVERS -> {
                notifyItemChangeById(ID_OBSERVERS, AppConstants.PAYLOAD)
            }
            AppInfo.CONFIG_CHANGED_TYPE_ENABLED -> {
                updateData(context, viewModel)
            }
        }
    }
}