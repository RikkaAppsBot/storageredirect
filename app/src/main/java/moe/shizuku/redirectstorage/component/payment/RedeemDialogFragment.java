package moe.shizuku.redirectstorage.component.payment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import moe.shizuku.redirectstorage.license.License;
import moe.shizuku.redirectstorage.license.RedeemHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rikka.internal.billingclient.app.AbstractRedeemDialogFragment;
import rikka.internal.payment.model.TokenResult;

import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.MSG_FAILED_BAD_PRICE;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.MSG_FAILED_NO_CODE;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.MSG_OK;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.MSG_OK_OVERWRITE_OLD;
import static moe.shizuku.redirectstorage.component.payment.RedeemConstants.RESULT_OK;

public class RedeemDialogFragment extends AbstractRedeemDialogFragment {

    @Override
    protected void onBindDialogView(View view, EditText edit) {
        edit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
    }

    private void setSubmitting(boolean submitting, AlertDialog dialog) {
        dialog.setCanceledOnTouchOutside(!submitting);
        dialog.setCancelable(!submitting);
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(!submitting);
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(!submitting);
    }

    @Override
    public void onSubmit(String code, AlertDialog dialog) {
        final Context context = requireContext();

        // Google Play
        if (!TextUtils.isDigitsOnly(code)
                && code.length() < 28
                && !BuildConfig.DEBUG) {
            startRedeemGooglePlay(code, true);
            return;
        }

        setSubmitting(true, dialog);

        ContextKt.getApplication(context).getPaymentService().registerRedeemCode(License.PACKAGE_NAME, License.PRODUCT_ID, code)
                .enqueue(new Callback<TokenResult>() {
                    @Override
                    public void onResponse(@NonNull Call<TokenResult> call, @NonNull Response<TokenResult> response) {
                        if (response.code() != 200 || response.body() == null || response.body().code == 2) {
                            onFailure(call, new RuntimeException("bad response"));
                            return;
                        }

                        if (response.body().code == RESULT_OK) {
                            RedeemHelper.setRedeemCode(code);
                        }

                        final Context context = getContext();

                        if (context != null) {
                            CharSequence message = response.body().message;
                            if (TextUtils.isEmpty(message)) {
                                switch (response.body().msgId) {
                                    case MSG_OK:
                                        message = context.getString(R.string.billing_success_new_device, Build.DEVICE);
                                        break;
                                    case MSG_OK_OVERWRITE_OLD:
                                        message = context.getString(R.string.billing_success_change_device, Build.DEVICE);
                                        break;

                                    case MSG_FAILED_NO_CODE:
                                    case MSG_FAILED_BAD_PRICE:
                                        message = context.getString(R.string.billing_failed_invalid);
                                        break;

                                    default:
                                        message = context.getString(R.string.billing_failed_unknown_error);
                                        break;
                                }
                            }

                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                            LocalBroadcastManager.getInstance(getContext())
                                    .sendBroadcast(new Intent(AppConstants.ACTION_PURCHASED_CHANGED));

                            dialog.dismiss();
                            setSubmitting(false, dialog);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<TokenResult> call, @NonNull Throwable t) {
                        if (getContext() != null) {
                            Toast.makeText(getContext(), "bad response".equals(t.getMessage()) ?
                                    R.string.billing_failed_unknown_error : R.string.billing_failed_connection_issue, Toast.LENGTH_SHORT).show();

                            setSubmitting(false, dialog);
                            dialog.dismiss();
                        }
                    }
                });
    }

    @Override
    public boolean onCheckCodeValidity(String code) {
        int length = code.length();
        return length >= 16 || (length > 0 && BuildConfig.DEBUG);
    }
}
