package moe.shizuku.redirectstorage.component.filemonitor.dialog;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.app.Settings.PackageFilterMode;
import moe.shizuku.redirectstorage.app.SharedUserLabelCache;
import moe.shizuku.redirectstorage.component.filemonitor.adapter.FilterSettingsAppListAdapter;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog;
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver;
import moe.shizuku.redirectstorage.model.SimpleAppInfo;
import moe.shizuku.redirectstorage.utils.UserHelper;
import moe.shizuku.redirectstorage.utils.ViewUtils;

import static java.util.Objects.requireNonNull;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_FILTER_KEYWORD;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_FILTER_LIST;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_FILTER_MODE;

public class FilterSettingsDialog extends AlertDialogFragment implements PackagesSelectorDialog.Callback {

    public static FilterSettingsDialog newInstance(
            @Nullable List<String> specPackageFilterList,
            @Nullable Integer specPackageFilterMode
    ) {
        final Bundle args = new Bundle();
        if (specPackageFilterList != null) {
            args.putStringArrayList(EXTRA_PACKAGE_FILTER_LIST,
                    new ArrayList<>(specPackageFilterList));
        }
        if (specPackageFilterMode != null) {
            args.putInt(EXTRA_PACKAGE_FILTER_MODE, specPackageFilterMode);
        }

        final FilterSettingsDialog dialog = new FilterSettingsDialog();
        dialog.setArguments(args);
        return dialog;
    }

    private static final String STATE_CHECKED_PATH_FILTER = BuildConfig.APPLICATION_ID +
            ".state.CHECKED_PATH_FILTER";
    private static final String STATE_CHECKED_PACKAGE_FILTER = BuildConfig.APPLICATION_ID +
            ".state.CHECKED_PACKAGE_FILTER";
    private static final String STATE_PATH_FILTER_KEYWORD = BuildConfig.APPLICATION_ID +
            ".state.PATH_FILTER_KEYWORD";
    private static final String STATE_NEW_PACKAGE_FILTER_MODE = BuildConfig.APPLICATION_ID +
            ".state.NEW_PACKAGE_FILTER_MODE";
    private static final String STATE_NEW_PACKAGE_FILTER_LIST = BuildConfig.APPLICATION_ID +
            ".state.NEW_PACKAGE_FILTER_LIST";

    private CheckBox mPathFilterCheck, mPackageFilterCheck;
    private View mPackageFilterLayout;
    private EditText mPathFilterEdit;
    private TextView mPackageFilterSummary;
    private Button mPackageFilterButton;
    private RadioGroup mPackageFilterModeRadioGroup;
    private View mPackageFilterWhitelistLayout, mPackageFilterBlacklistLayout;
    private RadioButton mPackageFilterWhitelistRadio, mPackageFilterBlacklistRadio;

    @PackageFilterMode
    private int mNewPackageFilterMode;
    private List<String> mNewPackageFilterList;
    private String mNewPathFilterKeyword;

    @Nullable
    private List<String> mSpecifiedPackageFilterList = null;
    @Nullable
    @PackageFilterMode
    private Integer mSpecifiedPackageFilterMode = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle args = getArguments();
        Objects.requireNonNull(args);

        mSpecifiedPackageFilterList = args.getStringArrayList(EXTRA_PACKAGE_FILTER_LIST);
        if (args.containsKey(EXTRA_PACKAGE_FILTER_MODE)) {
            mSpecifiedPackageFilterMode = args.getInt(EXTRA_PACKAGE_FILTER_MODE, -1);
        }

        if (savedInstanceState != null) {
            mNewPackageFilterMode = savedInstanceState.getInt(STATE_NEW_PACKAGE_FILTER_MODE);
            mNewPackageFilterList = savedInstanceState
                    .getStringArrayList(STATE_NEW_PACKAGE_FILTER_LIST);
            mNewPathFilterKeyword = savedInstanceState.getString(STATE_PATH_FILTER_KEYWORD);
        } else {
            mNewPackageFilterMode = Settings.getFileMonitorPackageFilterMode();
            mNewPackageFilterList = Settings.getFileMonitorPackageFilterList();
            mNewPathFilterKeyword = mSpecifiedPackageFilterList != null ? "" : Settings.getFileMonitorPathFilterKeyword();
        }

        registerLocalBroadcastReceiver(PackageFilterSettingsDialog.whenSaved((mode, list) -> {
            mNewPackageFilterMode = mode;
            mNewPackageFilterList = list;
            updatePackageFilterSummary();
        }));
    }

    @Override
    public int onGetContentViewLayoutResource() {
        return R.layout.dialog_file_monitor_filter_settings;
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                   @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.action_view_options);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            String keyword = mPathFilterEdit.getText().toString();
            if (!mPathFilterCheck.isChecked() || TextUtils.isEmpty(keyword)) {
                keyword = null;
            }

            getLocalBroadcastManager().sendBroadcast(
                    new Intent(AppConstants.ACTION_FILE_MONITOR_MODE_CHANGED)
                            .putExtra(EXTRA_PACKAGE_FILTER_LIST, new ArrayList<>(mNewPackageFilterList))
                            .putExtra(EXTRA_PACKAGE_FILTER_MODE, mPackageFilterCheck.isChecked() ? mNewPackageFilterMode : PackageFilterMode.NONE)
                            .putExtra(EXTRA_FILTER_KEYWORD, keyword));
        });
        builder.setNegativeButton(android.R.string.cancel, null);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPathFilterCheck != null && mPackageFilterCheck != null) {
            outState.putBoolean(STATE_CHECKED_PATH_FILTER, mPathFilterCheck.isChecked());
            outState.putBoolean(STATE_CHECKED_PACKAGE_FILTER, mPackageFilterCheck.isChecked());
            outState.putString(STATE_PATH_FILTER_KEYWORD, mPathFilterEdit.getText().toString());
        }
        outState.putInt(STATE_NEW_PACKAGE_FILTER_MODE, mNewPackageFilterMode);
        outState.putStringArrayList(STATE_NEW_PACKAGE_FILTER_LIST,
                new ArrayList<>(mNewPackageFilterList));
    }

    @Override
    public void onAlertDialogCreated(@NonNull AlertDialog dialog, View contentView,
                                     @Nullable Bundle savedInstanceState) {
        mPathFilterCheck = contentView.findViewById(R.id.path_filter_checkbox);
        mPathFilterEdit = contentView.findViewById(R.id.path_filter_edit);
        mPackageFilterCheck = contentView.findViewById(R.id.package_filter_checkbox);
        mPackageFilterSummary = contentView.findViewById(R.id.package_filter_summary);
        mPackageFilterButton = contentView.findViewById(R.id.package_filter_change_button);
        mPackageFilterLayout = contentView.findViewById(R.id.package_filter_layout);
        mPackageFilterModeRadioGroup = contentView.findViewById(R.id.filter_mode_radio_group);
        mPackageFilterBlacklistLayout = contentView.findViewById(R.id.radio_as_blacklist_layout);
        mPackageFilterBlacklistRadio = contentView.findViewById(R.id.radio_as_blacklist);
        mPackageFilterWhitelistLayout = contentView.findViewById(R.id.radio_as_whitelist_layout);
        mPackageFilterWhitelistRadio = contentView.findViewById(R.id.radio_as_whitelist);

        contentView.findViewById(R.id.path_filter_layout)
                .setOnClickListener(v -> mPathFilterCheck.toggle());
        mPackageFilterLayout.setOnClickListener(v -> mPackageFilterCheck.toggle());
        mPackageFilterButton.setOnClickListener(v -> {
            FileMonitorFilterPackagesSelectorDialog.newInstance(
                    mNewPackageFilterMode,
                    mNewPackageFilterList
            ).show(getChildFragmentManager(), "packages_selector");
//            PackageFilterSettingsDialog
//                    .newInstance(mNewPackageFilterMode, mNewPackageFilterList)
//                    .show(getChildFragmentManager());
        });

        mPathFilterCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
            updateViewsState();
        });
        mPackageFilterCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
            updateViewsState();
            if (!isChecked) {
                mNewPackageFilterMode = PackageFilterMode.NONE;
                mNewPackageFilterList.clear();
            } else {
                int id = mPackageFilterModeRadioGroup.getCheckedRadioButtonId();
                if (R.id.radio_as_whitelist == id) {
                    mNewPackageFilterMode = PackageFilterMode.WHITELIST;
                } else if (R.id.radio_as_blacklist == id) {
                    mNewPackageFilterMode = PackageFilterMode.BLACKLIST;
                }
            }
            updatePackageFilterSummary();
        });
        mPackageFilterModeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (mPackageFilterCheck.isChecked()) {
                if (R.id.radio_as_whitelist == checkedId) {
                    mNewPackageFilterMode = PackageFilterMode.WHITELIST;
                } else if (R.id.radio_as_blacklist == checkedId) {
                    mNewPackageFilterMode = PackageFilterMode.BLACKLIST;
                } else {
                    throw new IllegalArgumentException("Unsupported checked id: " + checkedId);
                }
            }
            updatePackageFilterSummary();
        });

        contentView.findViewById(R.id.radio_as_blacklist_layout).setOnClickListener(v ->
                mPackageFilterModeRadioGroup.check(R.id.radio_as_blacklist));
        contentView.findViewById(R.id.radio_as_whitelist_layout).setOnClickListener(v ->
                mPackageFilterModeRadioGroup.check(R.id.radio_as_whitelist));

        if (savedInstanceState == null) {
            mPathFilterCheck.setChecked(!TextUtils.isEmpty(mNewPathFilterKeyword));
            mPathFilterEdit.setText(mNewPathFilterKeyword);
            mPackageFilterCheck.setChecked(
                    mNewPackageFilterMode != PackageFilterMode.NONE);
        } else {
            mPathFilterCheck.setChecked(savedInstanceState.getBoolean(STATE_CHECKED_PATH_FILTER));
            mPathFilterEdit.setText(savedInstanceState.getString(STATE_PATH_FILTER_KEYWORD, null));
            mPackageFilterCheck.setChecked(
                    savedInstanceState.getBoolean(STATE_CHECKED_PACKAGE_FILTER));
        }

        if (mSpecifiedPackageFilterList != null && mSpecifiedPackageFilterMode != null) {
            mPackageFilterCheck.setChecked(mSpecifiedPackageFilterMode != PackageFilterMode.NONE);
        }

        mPackageFilterModeRadioGroup.check(mNewPackageFilterMode == PackageFilterMode.WHITELIST ?
                R.id.radio_as_whitelist : R.id.radio_as_blacklist);

        updateViewsState();
        updatePackageFilterSummary();
    }

    private void updateViewsState() {
        mPathFilterEdit.setEnabled(mPathFilterCheck.isChecked());

        final boolean packageFilterEnabled = mPackageFilterCheck.isChecked();
        mPackageFilterModeRadioGroup.setEnabled(packageFilterEnabled);
        mPackageFilterBlacklistRadio.setEnabled(packageFilterEnabled);
        mPackageFilterBlacklistLayout.setEnabled(packageFilterEnabled);
        mPackageFilterWhitelistRadio.setEnabled(packageFilterEnabled);
        mPackageFilterWhitelistLayout.setEnabled(packageFilterEnabled);
        mPackageFilterSummary.setEnabled(packageFilterEnabled);
        mPackageFilterButton.setEnabled(packageFilterEnabled);

        if (mSpecifiedPackageFilterList != null && mSpecifiedPackageFilterMode != null) {
            mPackageFilterModeRadioGroup.setEnabled(false);
            mPackageFilterBlacklistRadio.setEnabled(false);
            mPackageFilterBlacklistLayout.setEnabled(false);
            mPackageFilterWhitelistRadio.setEnabled(false);
            mPackageFilterWhitelistLayout.setEnabled(false);
            mPackageFilterLayout.setEnabled(false);
            mPackageFilterButton.setEnabled(false);
        }
    }

    private void updatePackageFilterSummary() {
        final List<String> list = Optional.ofNullable(mSpecifiedPackageFilterList)
                .orElse(mNewPackageFilterList);
        final int mode = Optional.ofNullable(mSpecifiedPackageFilterMode)
                .orElse(mNewPackageFilterMode);
        switch (mode) {
            case PackageFilterMode.NONE: {
                mPackageFilterSummary.setText(R.string.file_monitor_package_filter_summary_unset);
                break;
            }
            case PackageFilterMode.WHITELIST: {
                if (list.size() == 1) {
                    mPackageFilterSummary.setText(getString(
                            R.string.file_monitor_package_filter_summary_show_one_app_only,
                            list.get(0)
                    ));
                } else {
                    mPackageFilterSummary.setText(getString(
                            R.string.file_monitor_package_filter_summary_include_some_apps,
                            list.size()
                    ));
                }
                break;
            }
            case PackageFilterMode.BLACKLIST: {
                if (list.size() == 1) {
                    mPackageFilterSummary.setText(getString(
                            R.string.file_monitor_package_filter_summary_exclude_one_app,
                            list.get(0)
                    ));
                } else {
                    mPackageFilterSummary.setText(getString(
                            R.string.file_monitor_package_filter_summary_exclude_some_apps,
                            list.size()
                    ));
                }
                break;
            }
        }
    }

    @Override
    public void onPackageSelectorDialogResult(@Nullable String tag, @NotNull List<SimpleAppInfo> list) {
        mNewPackageFilterList = list.stream().map(SimpleAppInfo::getPackageName).collect(Collectors.toList());
        updatePackageFilterSummary();
    }

    public static class PackageFilterSettingsDialog extends AlertDialogFragment {

        private static void sendSaveAction(@NonNull Context context,
                                           @PackageFilterMode int mode,
                                           @NonNull List<String> selectedPackages) {
            LocalBroadcastManager.getInstance(context)
                    .sendBroadcast(new Intent(ACTION_SAVE)
                            .putExtra(EXTRA_FILTER_MODE, mode)
                            .putStringArrayListExtra(EXTRA_FILTER_LIST,
                                    new ArrayList<>(selectedPackages)));
        }

        public static ActionBroadcastReceiver whenSaved(@NonNull Callback callback) {
            requireNonNull(callback);
            return new ActionBroadcastReceiver(ACTION_SAVE) {
                @Override
                public void onReceive(Context context, Intent intent) {
                    callback.onSave(intent.getIntExtra(EXTRA_FILTER_MODE, 0),
                            requireNonNull(intent.getStringArrayListExtra(EXTRA_FILTER_LIST)));
                }
            };
        }

        public static PackageFilterSettingsDialog newInstance(
                @PackageFilterMode int mode,
                @Nullable List<String> selectedPackages) {
            final Bundle args = new Bundle();
            args.putInt(EXTRA_FILTER_MODE, mode);
            if (selectedPackages != null) {
                args.putStringArrayList(EXTRA_FILTER_LIST, new ArrayList<>(selectedPackages));
            }

            final PackageFilterSettingsDialog dialog = new PackageFilterSettingsDialog();
            dialog.setArguments(args);
            return dialog;
        }

        public static final String ACTION_SAVE = PackageFilterSettingsDialog.class.getName() +
                ".action.SAVE";
        public static final String EXTRA_FILTER_MODE = PackageFilterSettingsDialog.class.getName() +
                ".extra.FILTER_MODE";
        public static final String EXTRA_FILTER_LIST = PackageFilterSettingsDialog.class.getName() +
                ".extra.FILTER_LIST";

        private static final String STATE_CHOICE = BuildConfig.APPLICATION_ID + ".state.CHOICE";
        private static final String STATE_ALL_APPS = BuildConfig.APPLICATION_ID + ".state.ALL_APPS";
        private static final String STATE_SELECTED = BuildConfig.APPLICATION_ID + ".state.SELECTED";
        private static final String STATE_APP_FILTER_KEYWORD = BuildConfig.APPLICATION_ID +
                ".state.APP_FILTER_KEYWORD";

        @PackageFilterMode
        private int mChoice;
        private List<SimpleAppInfo> mAllApps;
        private boolean[] mSelected;

        private RadioGroup mRadioGroup;
        private EditText mEditText;
        private RecyclerView mRecyclerView;
        private ProgressBar mProgressBar;
        private TextView mStatusText;
        private Button mOkButton;

        @Nullable
        private List<String> mOriginalSelected;

        private final FilterSettingsAppListAdapter mAdapter = new FilterSettingsAppListAdapter();
        private final AppListAdapterObserver mAdapterObserver = new AppListAdapterObserver();

        private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

        public PackageFilterSettingsDialog() {
            mAdapter.registerAdapterDataObserver(mAdapterObserver);
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (savedInstanceState != null) {
                mChoice = savedInstanceState.getInt(STATE_CHOICE);
                mAllApps = savedInstanceState.getParcelableArrayList(STATE_ALL_APPS);
                mSelected = savedInstanceState.getBooleanArray(STATE_SELECTED);
                mAdapter.setSearchKeyword(savedInstanceState.getString(STATE_APP_FILTER_KEYWORD));

                mOriginalSelected = null;
            } else {
                mChoice = Settings.getFileMonitorPackageFilterMode();
                if (getArguments() != null) {
                    mChoice = getArguments().getInt(EXTRA_FILTER_MODE);
                    mOriginalSelected = getArguments().getStringArrayList(EXTRA_FILTER_LIST);
                }
                if (mChoice == PackageFilterMode.NONE) {
                    mChoice = PackageFilterMode.WHITELIST;
                }
            }
        }

        @Override
        public void onSaveInstanceState(@NonNull Bundle outState) {
            super.onSaveInstanceState(outState);
            if (mAllApps != null && mSelected != null) {
                outState.putParcelableArrayList(STATE_ALL_APPS, new ArrayList<>(mAllApps));
                outState.putBooleanArray(STATE_SELECTED, mSelected);
            }
            outState.putString(STATE_APP_FILTER_KEYWORD, mAdapter.getSearchKeyword());
        }

        @Override
        public int onGetContentViewLayoutResource() {
            return R.layout.dialog_file_monitor_filter_settings_from_packages;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            mCompositeDisposable.clear();
        }

        @Override
        public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                       @Nullable Bundle savedInstanceState) {
            builder.setTitle(R.string.file_monitor_package_filter_title);
        }

        @Override
        public void onAlertDialogCreated(@NonNull AlertDialog dialog, View contentView,
                                         @Nullable Bundle savedInstanceState) {
            mRadioGroup = contentView.findViewById(R.id.radio_group);
            mEditText = contentView.findViewById(android.R.id.edit);
            mRecyclerView = contentView.findViewById(android.R.id.list);
            mStatusText = contentView.findViewById(R.id.status_text_folders);
            mOkButton = contentView.findViewById(android.R.id.button1);
            mProgressBar = contentView.findViewById(android.R.id.progress);

            mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                if (R.id.radio_as_whitelist == checkedId) {
                    mChoice = PackageFilterMode.WHITELIST;
                } else if (R.id.radio_as_blacklist == checkedId) {
                    mChoice = PackageFilterMode.BLACKLIST;
                } else {
                    throw new IllegalArgumentException("Unsupported checked id: " + checkedId);
                }
            });

            contentView.findViewById(R.id.radio_as_blacklist_layout).setOnClickListener(v ->
                    mRadioGroup.check(R.id.radio_as_blacklist));
            contentView.findViewById(R.id.radio_as_whitelist_layout).setOnClickListener(v ->
                    mRadioGroup.check(R.id.radio_as_whitelist));

            // Bind ok listener
            mOkButton.setOnClickListener(v -> {
                final List<String> resultList = new ArrayList<>();
                for (int i = 0; i < mAllApps.size(); i++) {
                    if (mSelected[i]) {
                        resultList.add(mAllApps.get(i).getApplicationInfo().packageName);
                    }
                }
                sendSaveAction(requireContext(), mChoice, resultList);
                dismiss();
            });
            // Bind cancel listener
            contentView.findViewById(android.R.id.button2).setOnClickListener(v -> {
                dismiss();
            });

            mEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mAdapter.setSearchKeyword(s.toString());
                }
            });
            mEditText.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    ViewUtils.hideKeyboard(v);
                    return true;
                }
                return false;
            });

            mRecyclerView.setAdapter(mAdapter);

            mRadioGroup.check(mChoice == PackageFilterMode.WHITELIST ?
                    R.id.radio_as_whitelist : R.id.radio_as_blacklist);

            if (mAllApps == null) {
                refresh();
            } else {
                updateAdapterData();
            }
        }

        private void refresh() {
            mRecyclerView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);

            final PackageManager pm = requireContext().getPackageManager();
            mCompositeDisposable.add(Observable
                    .fromCallable(() -> pm.getInstalledPackages(0))
                    .flatMap(Observable::fromIterable)
                    .map(pi -> {
                        if (pi.sharedUserId != null) {
                            SharedUserLabelCache.load(pm, pi.sharedUserLabel, pi.sharedUserId, pi.packageName, pi.applicationInfo);
                        }
                        return new SimpleAppInfo(pi.packageName, UserHelper.myUserId(), pi.applicationInfo, pi.applicationInfo.loadLabel(pm), pi.sharedUserId);
                    })
                    .toSortedList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((list) -> {
                        mAllApps = list;
                        mSelected = new boolean[list.size()];
                        if (mOriginalSelected != null) {
                            for (String selectedPackage : mOriginalSelected) {
                                for (int i = 0; i < mAllApps.size(); i++) {
                                    if (mAllApps.get(i).getApplicationInfo().packageName
                                            .equals(selectedPackage)) {
                                        mSelected[i] = true;
                                        break;
                                    }
                                }
                            }
                        }
                        updateAdapterData();
                    }, (tr) -> {
                    }));
        }

        private void updateAdapterData() {
            mRecyclerView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

            mAdapter.setAllData(mAllApps, mSelected);
            mAdapter.startFilter();
        }

        private class AppListAdapterObserver extends RecyclerView.AdapterDataObserver {

            AppListAdapterObserver() {
                super();
            }

            @Override
            public void onChanged() {
                int selectCount = 0;
                for (int i = 0; i < (mSelected != null ? mSelected.length : 0); i++) {
                    if (mSelected[i]) {
                        selectCount++;
                    }
                }
                mStatusText.setText(getString(
                        R.string.file_monitor_package_filter_status_text_format, selectCount));
                mOkButton.setEnabled(selectCount > 0);
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                onChanged();
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                onChanged();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                onChanged();
            }

            @Override
            public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
                onChanged();
            }
        }

        public interface Callback {

            void onSave(@PackageFilterMode int newMode, @NonNull List<String> newList);

        }

    }

}
