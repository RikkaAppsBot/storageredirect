package moe.shizuku.redirectstorage.component.foldersanalysis.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import moe.shizuku.redirectstorage.AppConstants.EXTRA_APP_INFO
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.foldersanalysis.adapter.FoldersAnalysisItemAdapter
import moe.shizuku.redirectstorage.component.foldersanalysis.viewmodel.FoldersAnalysisViewModel
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.model.FolderSizeEntry
import moe.shizuku.redirectstorage.utils.FileCalculatorListener
import rikka.material.app.AppBarOwner
import rikka.recyclerview.addFastScroller
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView

class FoldersAnalysisFragment : AppFragment() {

    companion object {

        @JvmStatic
        fun newInstance(appInfo: AppInfo): FoldersAnalysisFragment {
            return FoldersAnalysisFragment().also {
                it.arguments = bundleOf(EXTRA_APP_INFO to appInfo)
            }
        }

    }

    private val viewModel: FoldersAnalysisViewModel by lazy {
        ViewModelProvider(requireActivity()).get(FoldersAnalysisViewModel::class.java)
    }

    private lateinit var appInfo: AppInfo

    private lateinit var recyclerView: BorderRecyclerView
    private lateinit var progressBar: View
    private lateinit var emptyText: View
    private val adapter: FoldersAnalysisItemAdapter = FoldersAnalysisItemAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appInfo = arguments!!.getParcelable(EXTRA_APP_INFO)!!
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.folders_analysis_fragment_content, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(android.R.id.list)
        progressBar = view.findViewById(android.R.id.progress)
        emptyText = view.findViewById(android.R.id.text1)

        recyclerView.adapter = adapter

        recyclerView.addFastScroller()
        recyclerView.fixEdgeEffect()
        recyclerView.borderViewDelegate.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top: Boolean, _: Boolean, _: Boolean, _: Boolean -> (activity as AppBarOwner?)?.appBar?.setRaised(!top) }

        viewModel.list.observe(this) {
            if (it != null) {
                progressBar.isVisible = false
                emptyText.isVisible = it.isEmpty()
                adapter.setData(it)
            }
        }

        if (viewModel.list.value == null) {
            refresh()
        }
    }

    private fun refresh() {
        try {
            SRManager.createThrow()
                    .fileManager
                    .calculateFoldersSizeInRedirectTarget(
                            appInfo.packageName, appInfo.userId, object : FileCalculatorListener() {
                        override fun onFinished(list: MutableList<FolderSizeEntry>?) {
                            if (list != null) {
                                val res = list.filter { it.size > 0 }.sortedDescending()
                                viewModel.list.postValue(res)
                            }
                        }
                    })
        } catch (e: Exception) {
            e.printStackTrace()
            activity?.finish()
        }
    }
}