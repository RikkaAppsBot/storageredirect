package moe.shizuku.redirectstorage.component.logcat.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.BaseForegroundIntentService;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.NotificationHelper;

import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_LOGCAT;

/**
 * Created by Fung Gwo on 2018/2/23.
 */

public class LogcatHistoryService extends BaseForegroundIntentService {

    public static final String TAG = LogcatHistoryService.class.getSimpleName();

    public static final String ACTION_DELETE_FILES = BuildConfig.APPLICATION_ID + ".action.LOGCAT_HISTORY_DELETE_FILES";

    public static final String ACTION_REMOVED = BuildConfig.APPLICATION_ID + ".action.LOGCAT_HISTORY_REMOVED";

    public static final String EXTRA_FILES = "files";

    public LogcatHistoryService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null || intent.getAction() == null) return;
        switch (intent.getAction()) {
            case ACTION_DELETE_FILES:
                List<File> files = new ArrayList<>(
                        (ArrayList<File>) intent.getSerializableExtra(EXTRA_FILES));
                NotificationCompat.Builder builder = getBuilder();
                final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
                int count = 0;
                for (File file : files) {
                    builder.setContentText(getString(R.string.notification_logcat_history_deleting_info, count + 1, files.size()));
                    builder.setProgress(files.size() - 1, count, false);
                    NotificationHelper.notify(this, AppConstants.NOTIFICATION_ID_LOGCAT_DELETE_HISTORY, builder);
                    if (file.delete()) {
                        broadcastManager.sendBroadcast(new Intent(ACTION_REMOVED).putExtra(EXTRA_FILES, file));
                    }
                    count++;
                }
                NotificationHelper.cancel(this, AppConstants.NOTIFICATION_ID_LOGCAT_DELETE_HISTORY);
                stopForeground(true);
                break;
        }
    }

    private NotificationCompat.Builder getBuilder() {
        NotificationCompat.Builder builder = NotificationHelper.create(
                this,
                AppConstants.NOTIFICATION_CHANNEL_LOGCAT,
                R.string.notification_logcat_history_deleting_title);
        builder.setSubText(getString(R.string.logcat_title));
        builder.setSmallIcon(R.drawable.ic_noti_logcat_24);
        return builder;
    }

    @Override
    public int getForegroundServiceNotificationId() {
        return AppConstants.NOTIFICATION_ID_LOGCAT_DELETE_HISTORY;
    }

    @Override
    public Notification onStartForeground() {
        return getBuilder().build();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onCreateNotificationChannel(@NonNull NotificationManager notificationManager) {
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_LOGCAT, getString(R.string.notification_channel_logcat_service), NotificationManager.IMPORTANCE_LOW);
        channel.setShowBadge(false);
        if (BuildUtils.atLeast29()) {
            channel.setAllowBubbles(false);
        }

        notificationManager.createNotificationChannel(channel);
    }
}
