package moe.shizuku.redirectstorage.component.home.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.LatestVersionInfo;
import rikka.html.widget.HtmlCompatTextView;
import rikka.recyclerview.BaseViewHolder;

public class HomeUpdateViewHolder extends BaseViewHolder<LatestVersionInfo> implements View.OnClickListener {

    public static final Creator<LatestVersionInfo> CREATOR = (inflater, parent) -> new HomeUpdateViewHolder(inflater.inflate(R.layout.home_item_update, parent, false));

    private ImageView icon;
    private HtmlCompatTextView text;
    private TextView button;

    public HomeUpdateViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        text = itemView.findViewById(android.R.id.text1);
        button = itemView.findViewById(android.R.id.button1);

        icon.setImageDrawable(itemView.getContext().getDrawable(R.drawable.ic_home_new_24dp));
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        final Context context = itemView.getContext();
        final LatestVersionInfo info = getData();

        info.startActivity(context);
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();
        final LatestVersionInfo info = getData();

        text.setHtmlText(String.format(info.text.get(), info.name));
        button.setText(info.downloadButton.get());
    }
}
