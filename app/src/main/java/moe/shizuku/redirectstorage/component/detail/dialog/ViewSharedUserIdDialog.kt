package moe.shizuku.redirectstorage.component.detail.dialog

import android.content.Context
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.appDetailViewModel
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.viewmodel.observeIgnoreInitial
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml

class ViewSharedUserIdDialog : AlertDialogFragment() {

    companion object {

        fun newInstance(): ViewSharedUserIdDialog {
            return ViewSharedUserIdDialog()
        }

        fun show(fragmentManager: FragmentManager) {
            if (fragmentManager.isStateSaved) return
            newInstance().show(fragmentManager, ViewSharedUserIdDialog::class.java.simpleName)
        }
    }

    private val model by appDetailViewModel()

    private fun getTitle(context: Context): CharSequence {
        return model.appInfo.value?.data?.sharedUserLabel ?: context.getString(R.string.shared_user_id)
    }

    private fun getMessage(context: Context): CharSequence {
        val sb = StringBuilder()
        model.sharedPackages.value?.data?.sortedBy { it.first }?.forEach {
            sb.append("<font face=\"sans-serif-medium\">").append(it.second).append("</font>")
                    .append("<br>").append(it.first)
                    .append("<p>")
        }
        return sb.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
    }

    override fun onShow(dialog: AlertDialog) {
        val context = dialog.context
        dialog.findViewById<TextView>(android.R.id.message)?.setLineSpacing(context.resources.getDimension(R.dimen.dialog_line_space_extra), 1f)

        model.appInfo.observeIgnoreInitial(this) {
            if (isDetached) return@observeIgnoreInitial
            if (it?.status == Status.SUCCESS) {
                dialog.setTitle(getTitle(requireContext()))
            }
        }
        model.sharedPackages.observeIgnoreInitial(this) {
            if (isDetached) return@observeIgnoreInitial
            if (it?.status == Status.SUCCESS) {
                dialog.setMessage(getMessage(requireContext()))
            }
        }
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        builder.setTitle(getTitle(builder.context))
                .setMessage(getMessage(builder.context))
                .setPositiveButton(android.R.string.ok, null)
    }
}