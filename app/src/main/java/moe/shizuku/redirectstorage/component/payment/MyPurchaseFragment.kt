package moe.shizuku.redirectstorage.component.payment

import android.content.Context
import android.os.Bundle
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragment
import moe.shizuku.redirectstorage.component.ComponentActivity
import moe.shizuku.redirectstorage.license.AlipayHelper
import moe.shizuku.redirectstorage.license.GoogleBillingHelper
import moe.shizuku.redirectstorage.license.RedeemHelper
import rikka.core.util.ClipboardUtils
import rikka.html.text.HtmlCompat
import rikka.material.app.AppBar

class MyPurchaseFragment : AppBarFragment() {

    companion object {

        @JvmStatic
        fun startActivity(context: Context) {
            context.startActivity(ComponentActivity.newIntent(MyPurchaseFragment::class.java, null))
        }
    }

    override fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {
        appBar.setDisplayHomeAsUpEnabled(true)
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        appBar.setTitle(R.string.my_purchase)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.appbar_container_scroll, container, false) as ViewGroup
        inflater.inflate(R.layout.my_purchase, root, true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context

        if (GoogleBillingHelper.checkLocal()) {
            val v = view.findViewById<View>(R.id.google)
            v.visibility = View.VISIBLE
            v.setOnClickListener(CopyListener(GoogleBillingHelper.getOrderId()))
            val code = v.findViewById<TextView>(android.R.id.text2)
            val title = v.findViewById<TextView>(android.R.id.title)
            val summary = v.findViewById<TextView>(android.R.id.summary)
            val time = v.findViewById<TextView>(android.R.id.text1)
            code.text = GoogleBillingHelper.getOrderId()
            title.text = GoogleBillingHelper.getItemTitle()
            summary.text = GoogleBillingHelper.getItemDescription()
            time.text = DateUtils.formatDateTime(context, GoogleBillingHelper.getPurchaseTime(),
                    DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_NUMERIC_DATE or DateUtils.FORMAT_SHOW_TIME)
        }
        if (AlipayHelper.checkLocal()) {
            val v = view.findViewById<View>(R.id.alipay)
            v.visibility = View.VISIBLE
            v.setOnClickListener(CopyListener(AlipayHelper.getOrderId()))
            val code = v.findViewById<TextView>(android.R.id.text2)
            val time = v.findViewById<TextView>(android.R.id.text1)
            code.text = AlipayHelper.getOrderId()
            if (AlipayHelper.getCreateTime() != 0L) {
                time.text = DateUtils.formatDateTime(context, AlipayHelper.getCreateTime(),
                        DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_NUMERIC_DATE or DateUtils.FORMAT_SHOW_TIME)
            } else {
                time.isVisible = false
            }
        }
        if (RedeemHelper.checkLocal()) {
            val v = view.findViewById<View>(R.id.redeem)
            v.visibility = View.VISIBLE
            v.setOnClickListener(CopyListener(RedeemHelper.getRedeemCode()))
            val code = v.findViewById<TextView>(android.R.id.text2)
            code.text = RedeemHelper.getRedeemCode()
        }
    }

    private class CopyListener(private val code: String) : OnClickListener {
        override fun onClick(v: View) {
            val context = v.context
            if (ClipboardUtils.put(context, code)) {
                Toast.makeText(context, HtmlCompat.fromHtml(context.getString(R.string.toast_copied_to_clipboard_with_text, code)), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, R.string.toast_copied_to_clipboard_failed, Toast.LENGTH_SHORT).show()
            }
        }

    }
}