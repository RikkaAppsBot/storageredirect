package moe.shizuku.redirectstorage.component.accessiblefolders.viewholder

import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.view.View.OnClickListener
import android.view.View.OnCreateContextMenuListener
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.model.AppSimpleMountInfo
import moe.shizuku.redirectstorage.utils.WordsHelper
import moe.shizuku.redirectstorage.widget.CheckedLinearLayout
import rikka.core.res.resolveColor
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppFolderToggleViewHolder(itemView: View) : BaseListenerViewHolder<AppSimpleMountInfo, AppFolderToggleViewHolder.Listener>(itemView), OnClickListener, OnCreateContextMenuListener {

    companion object {
        val CREATOR = Creator<AppSimpleMountInfo> { inflater: LayoutInflater, parent: ViewGroup? -> AppFolderToggleViewHolder(inflater.inflate(R.layout.accessible_folders_dialog_item_other_app, parent, false)) }
    }

    interface Listener {
        fun onViewSimpleMountClick(info: AppSimpleMountInfo)
        fun onDeleteSimpleMountClick(info: AppSimpleMountInfo)
        fun onToggleSimpleMountClick(oldInfo: AppSimpleMountInfo)
    }

    private val button1: View = itemView.findViewById(android.R.id.button1)
    private val button2: CheckedLinearLayout = itemView.findViewById(android.R.id.button2)
    private val switchView: CheckBox = itemView.findViewById(R.id.switch_widget)
    private val addView: View = itemView.findViewById(R.id.add)
    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val text1: TextView = itemView.findViewById(android.R.id.text1)
    private val text2: TextView = itemView.findViewById(android.R.id.text2)

    init {
        button1.setOnClickListener(this)
        button1.setOnCreateContextMenuListener(this)
        button2.setOnClickListener { view: View? -> onWidgetClick(view) }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, contextMenuInfo: ContextMenuInfo?) {
        val context = v.context
        val menuInflater = MenuInflater(context)
        menuInflater.inflate(R.menu.context_menu_observer_toggle, menu)
        menu.setHeaderTitle(title.text.toString())
        val deleteAction = menu.findItem(R.id.action_delete)
        deleteAction.setOnMenuItemClickListener { item: MenuItem? -> onItemDeleteClick(item) }
        if (data != null) {
            deleteAction.isVisible = data.local || data.enabled
        }
    }

    private fun onItemDeleteClick(item: MenuItem?): Boolean {
        listener!!.onDeleteSimpleMountClick(data)
        return true
    }

    override fun onClick(view: View) {
        listener!!.onViewSimpleMountClick(data)
    }

    private fun onWidgetClick(view: View?) {
        onSwitchClick()
    }

    private fun onSwitchClick() {
        listener!!.onToggleSimpleMountClick(data)
    }

    override fun onBind() {
        syncEnabledState()
        val context = itemView.context
        val info = data
        val titleText: CharSequence
        val contentText: CharSequence
        val defaultTitleText: CharSequence
        val defaultContentText: CharSequence
        defaultTitleText = if (info.targetPackage == info.thisPackage) {
            HtmlCompat.fromHtml(context.getString(R.string.detail_simple_mount_title_from, info.sourceLabel))
        } else {
            HtmlCompat.fromHtml(context.getString(R.string.detail_simple_mount_title_to, info.targetLabel))
        }
        defaultContentText = WordsHelper.buildOmissibleFoldersListText(context, info.paths, 4)
        var description: CharSequence? = info.loadDescription()
        if (description != null) {
            description = HtmlCompat.fromHtml(description.toString(), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
        }
        if (description == null) {
            titleText = defaultTitleText
            contentText = defaultContentText
        } else {
            titleText = description
            val sb = SpannableStringBuilder()
            sb.append(defaultTitleText).append('\n').append(defaultContentText)
            contentText = sb
        }
        title.text = titleText
        text1.text = contentText
        icon.setImageDrawable(if (data.icon) context.getDrawable(R.drawable.ic_folder_share_24dp) else null)
        updateExtraSummary()
    }

    override fun onBind(payloads: List<Any>) {
        syncEnabledState()
        updateExtraSummary()
    }

    private fun syncEnabledState() {
        val candidateOnline = data.online && !data.local
        val enabled = (data.sourceInfo != null || RedirectPackageInfo.isSharedUserId(data.sourcePackage)) && data.targetInfo != null || data.enabled
        button2.isEnabled = enabled
        button2.isChecked = data.enabled
        switchView.isVisible = !candidateOnline
        addView.isVisible = candidateOnline
    }

    private fun updateExtraSummary() {
        val context = itemView.context
        val info = data
        val sb = StringBuilder()
        val theme = context.theme
        if (info.sourceInfo == null && !RedirectPackageInfo.isSharedUserId(info.sourcePackage)) {
            sb.append(context.getString(R.string.simple_mount_not_installed))
            sb.append("<br>")
        }
        if (info.targetInfo == null) {
            sb.append(context.getString(R.string.simple_mount_not_installed))
            sb.append("<br>")
        }
        if (data.online && data.local) {
            sb.append(context.getString(R.string.rule_from_online, 0xffffff and theme.resolveColor(R.attr.colorSafe)))
            sb.append("<br>")
        }
        if (!TextUtils.isEmpty(sb)) {
            text2.visibility = View.VISIBLE
            text2.text = HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE
                    or HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS)
        } else {
            text2.visibility = View.GONE
        }
    }
}