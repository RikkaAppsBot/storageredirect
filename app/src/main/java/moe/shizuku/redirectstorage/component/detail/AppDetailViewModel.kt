package moe.shizuku.redirectstorage.component.detail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.os.Build
import android.os.Parcelable
import androidx.activity.ComponentActivity
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.event.AppConfigChangedEvent
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.event.GlobalConfigChangedEvent
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.ktx.getPackageNameExtra
import moe.shizuku.redirectstorage.ktx.getUserIdExtra
import moe.shizuku.redirectstorage.ktx.logd
import moe.shizuku.redirectstorage.model.*
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.utils.FileUtils
import moe.shizuku.redirectstorage.utils.IntentCompat
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.Resource
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.viewmodel.activitySharedViewModels
import moe.shizuku.redirectstorage.viewmodel.sharedViewModels

@MainThread
fun ComponentActivity.appDetailViewModel() = sharedViewModels({ AppDetailViewModel.keyOf(intent) }, { AppDetailViewModel(this) })

@MainThread
fun Fragment.appDetailViewModel() = activitySharedViewModels({ AppDetailViewModel.keyOf(requireActivity().intent) }, { AppDetailViewModel(requireActivity()) })

class AppDetailViewModel(private val activity: Activity) : ViewModel(), GlobalConfigChangedEvent, AppConfigChangedEvent {

    companion object {

        fun keyOf(intent: Intent): String {
            val packageName = intent.getPackageNameExtra()
            val userId = intent.getUserIdExtra()
            return "${userId}:${packageName}"
        }
    }

    private val _appInfo = MutableLiveData<Resource<AppInfo>>()

    val appInfo = _appInfo as LiveData<Resource<AppInfo>>

    private val _mountDirs = MutableLiveData<Resource<Set<String>>>()

    val mountDirs = _mountDirs as LiveData<Resource<Set<String>>>

    private val _localObservers = MutableLiveData<Resource<List<AppObserverInfo>>>()

    private val _observers = MediatorLiveData<Resource<List<AppObserverInfo>>>()

    val observers = _observers as LiveData<Resource<List<AppObserverInfo>>>

    private val _localSimpleMounts = MutableLiveData<Resource<List<AppSimpleMountInfo>>>()

    private val _simpleMounts = MediatorLiveData<Resource<List<AppSimpleMountInfo>>>()

    val simpleMounts = _simpleMounts as LiveData<Resource<List<AppSimpleMountInfo>>>

    private val _recommendation = MutableLiveData<Resource<AppRecommendation>>(Resource.loading())

    val recommendation = _recommendation as LiveData<Resource<AppRecommendation>>

    private val _mountDirsConfig = MutableLiveData<Resource<MountDirsConfig>>()

    val mountDirsConfig = _mountDirsConfig as LiveData<Resource<MountDirsConfig>>

    private val _mountDirsTemplates = MutableLiveData<Resource<List<MountDirsTemplate>>>()

    val mountDirsTemplates = _mountDirsTemplates as LiveData<Resource<List<MountDirsTemplate>>>

    private val _sharedPackages = MutableLiveData<Resource<List<Pair<String, CharSequence>>>>()

    val sharedPackages = _sharedPackages as LiveData<Resource<List<Pair<String, CharSequence>>>>

    var isOnlineRulesEnabled: Boolean = true
        private set

    var userStorageAvailable: Boolean = true
        private set

    init {
        Events.registerGlobalConfigChangedEvent(this)

        load(activity, activity.intent)

        _observers.apply {
            addSource(_localObservers) {
                this@AppDetailViewModel.logd("computeObservers from localObservers")
                computeObservers()
            }
            addSource(_recommendation) {
                this@AppDetailViewModel.logd("computeObservers from recommendation")
                computeObservers()
            }
        }

        _simpleMounts.apply {
            addSource(_localSimpleMounts) {
                this@AppDetailViewModel.logd("computeSimpleMounts from localSimpleMounts")
                computeSimpleMounts()
            }
            addSource(_recommendation) {
                this@AppDetailViewModel.logd("computeSimpleMounts from recommendation")
                computeSimpleMounts()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        Events.unregisterGlobalConfigChangedEvent(this)

        if (appInfo.value?.data != null) {
            val appInfo = appInfo.value?.data!!
            Events.unregisterAppConfigChangedEvent(this, appInfo.packageName, appInfo.userId)
        }
    }

    fun load(context: Context, intent: Intent) = viewModelScope.launch(Dispatchers.IO) {
        _appInfo.postValue(Resource.loading())

        if (_recommendation.value?.status != Status.LOADING) {
            _recommendation.postValue(Resource.loading())
        }

        val packageName: String? = intent.getStringExtra(AppConstants.EXTRA_PACKAGE_NAME) ?: intent.getStringExtra(IntentCompat.EXTRA_PACKAGE_NAME)
        val userId = intent.getParcelableExtra<Parcelable>(Intent.EXTRA_USER)?.hashCode()
                ?: intent.getIntExtra(AppConstants.EXTRA_USER_ID, UserHelper.myUserId())

        val appInfo: AppInfo
        try {
            if (packageName.isNullOrEmpty()) {
                throw AppInfoParseException("Extra android.intent.extra.PACKAGE_NAME is empty.")
            }

            val srm = SRManager.createThrow()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && userId != 0) {
                userStorageAvailable = try {
                    srm.isUserStorageAvailable(userId)
                } catch (e: Throwable) {
                    true
                }
            }

            val mountDirs = srm.getMountDirsForPackage(packageName, userId, false).toSortedSet()
            appInfo = AppInfoCache[packageName, userId, context, srm]

            withContext(Dispatchers.Main) {
                _mountDirs.value = Resource.success(mountDirs)
                _appInfo.value = Resource.success(appInfo)
            }

            _localObservers.postValue(Resource.success(loadLocalObservers(srm, packageName, userId)))
            _localSimpleMounts.postValue(Resource.success(loadLocalSimpleMounts(srm, packageName, userId)))

            Events.registerAppConfigChangedEvent(this@AppDetailViewModel, appInfo.packageName, appInfo.userId)

            if (appInfo.sharedUserId != null) {
                loadSharedUserId(context, appInfo)
            }
            loadMountDirsTemplates()
        } catch (ignored: CancellationException) {
            return@launch
        } catch (e: AppInfoParseException) {
            _appInfo.postValue(Resource.error(e, null))
            return@launch
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            _appInfo.postValue(Resource.error(e, null))
            return@launch
        }

        // online data should be the last
        try {
            loadRecommendation(context, appInfo)?.let {
                _recommendation.postValue(Resource.success(it))
            }
        } catch (ignored: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            _recommendation.postValue(Resource.error(e, null))
            return@launch
        }
    }

    private fun invalidateMountDirs() = viewModelScope.launch {
        appInfo.value?.data ?: return@launch
        val appInfo = appInfo.value?.data!!
        val packageName = appInfo.packageName
        val userId = appInfo.userId
        try {
            withContext(Dispatchers.IO) {
                val srm = SRManager.createThrow()
                val mountDirs = srm.getMountDirsForPackage(packageName, userId, false).toSortedSet()
                _mountDirs.postValue(Resource.success(mountDirs))
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            Resource.error(e, null)
        }
    }

    private fun loadLocalObservers(srm: SRManager, packageName: String, userId: Int) = srm.getObservers(packageName, userId).map { it.toApp() }

    private fun computeObservers() {
        val appInfo = _appInfo.value?.data ?: return
        val localObservers = _localObservers.value?.data ?: return
        val mountDirs = _mountDirs.value?.data ?: return
        val onlineConfig = _recommendation.value?.data?.config

        try {
            viewModelScope.launch(Dispatchers.IO) {
                val combined = ArrayList(localObservers)
                val onlineObservers = onlineConfig?.exportFolders?.data

                onlineObservers?.forEach {
                    val online = it.value
                    online.onlineId = it.key
                    online.id = online.onlineId

                    online.packageName = appInfo.packageName
                    online.userId = appInfo.userId
                    online.online = true
                    online.enabled = false

                    var add = true
                    localObservers.forEach local@{ local ->
                        if (online.equalsOnlineFields(local)) {
                            local.online = true
                            local.description = online.description
                            local.title = online.title
                            add = false
                            return@local
                        } else if (online.source == local.source) {
                            local.online = false
                            online.enabled = false
                            return@local
                        }
                    }

                    if (add) {
                        combined.add(online)
                    }
                }

                combined.forEach {
                    for (s in mountDirs) {
                        if (FileUtils.isChildOf(s, it.source)) {
                            it.sourceMountDir = s
                            break
                        }
                    }
                }

                _observers.postValue(Resource.success(combined))
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
        }
    }

    fun invalidateObservers() = viewModelScope.launch {
        appInfo.value?.data ?: return@launch
        val appInfo = appInfo.value?.data!!
        val packageName = appInfo.packageName
        val userId = appInfo.userId
        try {
            withContext(Dispatchers.IO) {
                val srm = SRManager.createThrow()
                _localObservers.postValue(Resource.success(loadLocalObservers(srm, packageName, userId)))
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            Resource.error(e, null)
        }
    }

    private fun loadLocalSimpleMounts(srm: SRManager, packageName: String, userId: Int) = srm.getSimpleMounts(packageName, userId, 0).map { it.toApp() }

    private fun computeSimpleMounts() {
        val appInfo = _appInfo.value?.data ?: return
        val localSimpleMounts = _localSimpleMounts.value?.data ?: return
        val onlineConfig = _recommendation.value?.data?.config

        val context = activity
        val packageManager = context.packageManager

        try {
            viewModelScope.launch(Dispatchers.IO) {
                val srm = SRManager.createThrow()
                val combined = ArrayList(localSimpleMounts)
                val onlineSimpleMounts = onlineConfig?.accessibleFolder?.data

                onlineSimpleMounts?.forEach {
                    val online = it.value
                    online.onlineId = it.key
                    online.id = online.onlineId
                    if (online.targetPackage == null && online.sourcePackage != appInfo.packageName) {
                        online.targetPackage = appInfo.packageName
                    }

                    online.userId = appInfo.userId
                    online.online = true
                    online.enabled = false

                    var add = true
                    for (localInfo in combined) {
                        if (online.equalsOnlineFields(localInfo)) {
                            localInfo.description = online.description
                            localInfo.online = true
                            add = false
                            break
                        }
                    }
                    if (add) {
                        combined.add(online)
                    }
                }

                combined.forEach {
                    it.thisPackage = appInfo.packageName
                }

                var targetInfo: ApplicationInfo? = null
                var targetLabel: CharSequence?
                try {
                    targetInfo = srm.getApplicationInfo(appInfo.packageName, 0x00002000/*PackageManager.MATCH_UNINSTALLED_PACKAGES*/, appInfo.userId)
                    targetLabel = targetInfo?.loadLabel(packageManager) ?: appInfo.packageName
                } catch (tr: Throwable) {
                    targetLabel = appInfo.packageName
                }

                combined.forEach { smi ->
                    if (RedirectPackageInfo.isSharedUserId(smi.sourcePackage)) {
                        smi.sourceLabel = context.getString(R.string.item_summary, SharedUserLabelCache.get(RedirectPackageInfo.getRawSharedUserId(smi.sourcePackage)), context.getString(R.string.shared_user_id))
                    } else {
                        try {
                            smi.sourceInfo = srm.getApplicationInfo(smi.sourcePackage, 0x00002000/*PackageManager.MATCH_UNINSTALLED_PACKAGES*/, smi.userId)
                            smi.sourceLabel = smi.sourceInfo?.loadLabel(packageManager) ?: smi.sourcePackage
                        } catch (tr: Throwable) {
                            smi.sourceLabel = smi.sourcePackage
                        }
                    }

                    smi.targetInfo = targetInfo
                    smi.targetLabel = targetLabel
                }

                combined.sortWith(Comparator<AppSimpleMountInfo> { o1, o2 ->
                    (o2.sourceInfo != null).compareTo(o1.sourceInfo != null).let {
                        if (it != 0) return@Comparator it
                    }

                    o1.sourceLabel.toString().compareTo(o2.sourceLabel.toString(), true)
                })

                _simpleMounts.postValue(Resource.success(combined))
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
        }
    }

    fun invalidateSimpleMounts() = viewModelScope.launch {
        appInfo.value?.data ?: return@launch
        val appInfo = appInfo.value?.data!!
        val packageName = appInfo.packageName
        val userId = appInfo.userId
        try {
            withContext(Dispatchers.IO) {
                val srm = SRManager.createThrow()
                _localSimpleMounts.postValue(Resource.success(loadLocalSimpleMounts(srm, packageName, userId)))
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            Resource.error(e, null)
        }
    }

    private suspend fun loadRecommendation(context: Context, appInfo: AppInfo): AppRecommendation? {
        AppRecommendationTexts.loadLocal(context)

        isOnlineRulesEnabled = Settings.isOnlineRulesEnabled
        if (!isOnlineRulesEnabled) {
            return null
        }

        val response = if (Settings.isCustomRepoEnabled) context.application.gitHubRulesApiService
                .getConfigurationAsync(packageName = appInfo.packageName)
        else context.application.rikkaAppApiService
                .getConfigurationAsync(packageName = appInfo.packageName)

        AppRecommendationTexts.loadAsync(context)

        return if (response.isSuccessful && response.body() != null) {
            val configuration = response.body()!!
            requireNotNull(configuration.recommendation)

            appInfo.configuration = configuration

            AppRecommendation.fromAppConfiguration(configuration)
        } else {
            AppRecommendation.noResult(appInfo.categoryType)
        }
    }

    fun invalidateRecommendation(context: Context) = viewModelScope.launch {
        appInfo.value?.data ?: return@launch

        if (_recommendation.value?.status != Status.LOADING) {
            _recommendation.postValue(Resource.loading(null))
        }

        val appInfo = appInfo.value?.data!!
        try {
            withContext(Dispatchers.IO) {
                loadRecommendation(context, appInfo)?.let {
                    _recommendation.postValue(Resource.success(it))
                }
            }
        } catch (e: CancellationException) {
        } catch (e: Throwable) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            _recommendation.value = Resource.error(e, null)
        }
    }

    private fun loadMountDirsTemplates() {
        try {
            val srm = SRManager.createThrow()
            _mountDirsTemplates.postValue(Resource.success(srm.mountDirsTemplates))
        } catch (e: CancellationException) {
            return
        } catch (e: Throwable) {
            _mountDirsTemplates.postValue(Resource.error(e, null))
        }
    }

    private fun invalidateMountDirsTemplates() = viewModelScope.launch {
        if (_mountDirsTemplates.value != null && _mountDirsTemplates.value!!.status != Status.LOADING) {
            _mountDirsTemplates.postValue(Resource.loading(null))
        }

        withContext(Dispatchers.IO) {
            loadMountDirsTemplates()
        }
    }

    @SuppressLint("WrongConstant")
    private fun loadSharedUserId(context: Context, appInfo: AppInfo) {
        val sm = SRManager.create() ?: return
        if (appInfo.applicationInfo == null) return

        val packages = sm.getPackagesForUid(appInfo.packageInfo.applicationInfo.uid) ?: return

        val res = ArrayList<Pair<String, CharSequence>>()
        val pm = context.packageManager
        packages.forEach {
            val label = try {
                it to pm.getApplicationInfo(it, 0x00002000/*PackageManager.MATCH_UNINSTALLED_PACKAGES*/).loadLabel(pm)
            } catch (e: Throwable) {
                it to it
            }
            res.add(label)
        }
        _sharedPackages.postValue(Resource.success(res))
    }

    override fun onMountDirsTemplateAdded(template: MountDirsTemplate) {
        if (template.packages == null) {
            return
        }

        onMountDirsTemplateChanged(template)
    }

    override fun onMountDirsTemplateRemoved(template: MountDirsTemplate) {
        if (template.packages == null) {
            return
        }

        val removed = MountDirsTemplate(template)
        removed.packages = emptyList()
        onMountDirsTemplateChanged(removed)
    }

    override fun onMountDirsTemplateChanged(newTemplate: MountDirsTemplate, oldTemplate: MountDirsTemplate) {
        if (newTemplate.packages == null || newTemplate.packages == oldTemplate.packages) {
            return
        }

        onMountDirsTemplateChanged(newTemplate)
    }

    private fun onMountDirsTemplateChanged(template: MountDirsTemplate) {
        if (appInfo.value?.data == null) {
            return
        }

        val changed: Boolean?
        val appInfo = appInfo.value!!.data!!
        val config = appInfo.redirectInfo.mountDirsConfig
        if (template.packages!!.contains(RedirectPackageInfo(appInfo.packageName, appInfo.userId))) {
            config.isTemplatesAllowed = true
            if (config.templatesIds == null) {
                config.templatesIds = HashSet()
            }
            changed = config.templatesIds!!.add(template.id)
        } else {
            changed = config.templatesIds?.remove(template.id)
        }

        // trigger observer
        if (changed == true) {
            _mountDirsConfig.postValue(Resource.success(config))
        }

        invalidateMountDirsTemplates()
    }

    override fun onEnabledChanged(packageName: String, sharedUserId: String?, userId: Int, enabled: Boolean) {
        super.onEnabledChanged(packageName, sharedUserId, userId, enabled)
    }

    override fun onMountDirsConfigChanged(packageName: String, sharedUserId: String?, userId: Int, config: MountDirsConfig) {
        if (appInfo.value?.data == null) {
            return
        }
        val appInfo = appInfo.value!!.data!!
        val changed = appInfo.redirectInfo.mountDirsConfig == config
        appInfo.redirectInfo.mountDirsConfig = config

        if (changed) {
            invalidateMountDirs()
            invalidateObservers()
            _mountDirsConfig.postValue(Resource.success(config))
        }
    }

    override fun onRedirectTargetChanged(packageName: String, sharedUserId: String?, userId: Int, path: String?) {
        super.onRedirectTargetChanged(packageName, sharedUserId, userId, path)
    }

}
