package moe.shizuku.redirectstorage.component.filemonitor

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.*
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.schedulers.Schedulers
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.app.Settings.PackageFilterMode
import moe.shizuku.redirectstorage.app.Settings.fileMonitorPackageFilterList
import moe.shizuku.redirectstorage.app.Settings.fileMonitorPackageFilterMode
import moe.shizuku.redirectstorage.component.filemonitor.adapter.FileMonitorAdapter
import moe.shizuku.redirectstorage.component.filemonitor.dialog.FilterSettingsDialog
import moe.shizuku.redirectstorage.component.filemonitor.dialog.InvalidVerifiedAppFeedbackDialog.Companion.newInstance
import moe.shizuku.redirectstorage.component.filemonitor.viewholder.IOHistoryViewHolder
import moe.shizuku.redirectstorage.model.FileMonitorRecord
import moe.shizuku.redirectstorage.utils.CrashReportHelper.logException
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.utils.mainHandler
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.material.app.AppBarOwner
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView.OnBorderVisibilityChangedListener
import rikka.recyclerview.addFastScroller
import rikka.recyclerview.fixEdgeEffect
import java.util.*

class FileMonitorFragment : AppFragment() {

    private val items: MutableList<FileMonitorRecord> = ArrayList()
    private lateinit var recyclerView: BorderRecyclerView
    private lateinit var adapter: FileMonitorAdapter
    private var topId: Long = 0
    private var loadDisposable: Disposable? = null
    private var updateDisposable: Disposable? = null
    private val handler = mainHandler
    private var mSpecifiedPackageFilterList: List<String>? = null

    @PackageFilterMode
    private var specifiedPackageFilterMode: Int? = null
    private var specifiedPathFilterKeyword: String? = null
    private var hasSpecifiedPathFilterKeyword = false

    private fun resetSubtitle() {
        val actionBar = requireBaseActivity().supportActionBar
        if (actionBar != null) {
            if (packageFilterMode != PackageFilterMode.NONE || !TextUtils.isEmpty(fileMonitorPathFilterKeyword)) {
                actionBar.setSubtitle(R.string.file_monitor_package_filter_set)
            } else {
                actionBar.subtitle = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        if (args != null) {
            mSpecifiedPackageFilterList = args.getStringArrayList(AppConstants.EXTRA_PACKAGE_FILTER_LIST)
            if (args.containsKey(AppConstants.EXTRA_PACKAGE_FILTER_MODE)) {
                specifiedPackageFilterMode = args.getInt(AppConstants.EXTRA_PACKAGE_FILTER_MODE, -1)
            }
            if (args.containsKey(AppConstants.EXTRA_FILTER_KEYWORD)) {
                specifiedPathFilterKeyword = args.getString(AppConstants.EXTRA_FILTER_KEYWORD)
                hasSpecifiedPathFilterKeyword = true
            }
        }
        setHasOptionsMenu(true)
        registerLocalBroadcastReceiver(AppConstants.ACTION_LOAD_MORE) { context: Context, intent: Intent -> onLoadMore(context, intent) }
        registerLocalBroadcastReceiver(AppConstants.ACTION_FILE_MONITOR_MODE_CHANGED) { context: Context, intent: Intent -> onSearchModeChanged(context, intent) }
        registerLocalBroadcastReceiver(IOHistoryViewHolder.ACTION_REQUEST_SUBMIT_INVALID) { context: Context, intent: Intent? -> onRequestInvalidVerifiedSubmit(context, intent) }
    }

    private fun postRefresh(delay: Long) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (handler.hasCallbacks(refreshRunnable)) {
                return
            }
        } else {
            handler.removeCallbacks(refreshRunnable)
        }
        if (delay > 0) {
            handler.postDelayed(refreshRunnable, delay)
        } else {
            handler.post(refreshRunnable)
        }
    }

    private fun stopRefresh() {
        handler.removeCallbacks(refreshRunnable)
    }

    override fun onStart() {
        super.onStart()
        postRefresh(0)
    }

    override fun onStop() {
        super.onStop()
        stopRefresh()
    }

    override fun onResume() {
        super.onResume()
        resetSubtitle()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (updateDisposable != null) {
            updateDisposable!!.dispose()
        }
        if (loadDisposable != null) {
            loadDisposable!!.dispose()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.file_monitor_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = FileMonitorAdapter()
        recyclerView = view.findViewById(android.R.id.list)
        recyclerView.setAdapter(adapter)
        recyclerView.addItemDecoration(VerticalPaddingDecoration(view.context))
        recyclerView.fixEdgeEffect(true, true)
        recyclerView.addFastScroller(null)
        recyclerView.getBorderViewDelegate().borderVisibilityChangedListener = OnBorderVisibilityChangedListener { top: Boolean, oldTop: Boolean, bottom: Boolean, oldBottom: Boolean -> (activity as AppBarOwner?)!!.appBar!!.setRaised(!top) }
    }

    private val refreshRunnable = Runnable {
        if (isStateSaved) {
            return@Runnable
        }
        //String selection = "id > CAST(? AS INTEGER) AND user=? AND path NOT like ?";
        //String[] selectionArgs = new String[]{Long.toString(mTopId), Integer.toString(UserHelper.myUserId()), "/storage/emulated/" + UserHelper.myUserId() + "/Android/data/%"};
        val selection = "id > CAST(? AS INTEGER)"
        val selectionArgs = arrayOf(java.lang.Long.toString(topId))
        load(selection, selectionArgs, "id DESC", "100")
        postRefresh(2000)
    }

    private fun load(selection: String, selectionArgs: Array<String>, orderBy: String, limit: String) {
        loadDisposable = Observable
                .fromCallable { SRManager.createThrow().queryFileMonitor(buildSelection(selection), buildSelectionArgs(selectionArgs), orderBy, limit) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list: List<FileMonitorRecord> -> refreshData(list) }) { e: Throwable? -> }
    }

    val packageFilterList: List<String>
        get() = Optional.ofNullable(mSpecifiedPackageFilterList)
                .orElse(fileMonitorPackageFilterList)

    @get:PackageFilterMode
    val packageFilterMode: Int
        get() = Optional.ofNullable(specifiedPackageFilterMode)
                .orElse(fileMonitorPackageFilterMode)
    val fileMonitorPathFilterKeyword: String?
        get() = if (hasSpecifiedPathFilterKeyword) {
            specifiedPathFilterKeyword
        } else {
            Settings.fileMonitorPathFilterKeyword
        }

    private fun buildSelection(base: String): String {
        val selectionBuilder = StringBuilder(base)
        val filterMode = packageFilterMode
        if (filterMode != PackageFilterMode.NONE) {
            val size = packageFilterList.size
            if (size > 0) {
                selectionBuilder.append(" AND (")
                for (i in 0 until size) {
                    if (i > 0) {
                        selectionBuilder.append(" ").append(
                                if (filterMode == PackageFilterMode.WHITELIST) "OR" else "AND"
                        ).append(" ")
                    }
                    selectionBuilder
                            .append("package")
                            .append(if (filterMode == PackageFilterMode.WHITELIST) "=" else "!=")
                            .append("?")
                }
                selectionBuilder.append(")")
            }
        }
        if (!TextUtils.isEmpty(fileMonitorPathFilterKeyword)) {
            selectionBuilder.append(" AND LOWER(path) like ?")
        }
        return selectionBuilder.toString()
    }

    private fun buildSelectionArgs(base: Array<String>): Array<String> {
        val selectionArgs: MutableList<String> = ArrayList(Arrays.asList(*base))
        if (packageFilterMode != PackageFilterMode.NONE) {
            selectionArgs.addAll(packageFilterList)
        }
        val keyword = fileMonitorPathFilterKeyword
        if (!TextUtils.isEmpty(keyword)) {
            selectionArgs.add("%$keyword%")
        }
        return selectionArgs.toTypedArray()
    }

    @Synchronized
    private fun refreshData(list: List<FileMonitorRecord>) {
        items.addAll(list)
        if (updateDisposable != null && !updateDisposable!!.isDisposed) {
            return
        }
        updateDisposable = Observable
                .fromIterable(ArrayList(items))
                .map { ioHistoryInfo: FileMonitorRecord ->
                    ioHistoryInfo.count = 0
                    ioHistoryInfo
                }
                .sorted { o1: FileMonitorRecord?, o2: FileMonitorRecord? -> java.lang.Long.compare(o2!!.id, o1!!.id) }
                .filter(object : Predicate<FileMonitorRecord> {
                    private var last: FileMonitorRecord? = null
                    override fun test(item: FileMonitorRecord): Boolean {
                        if (item.id > topId) {
                            topId = item.id
                        }
                        var res = true
                        if (last != null) {
                            res = (last!!.path != item.path
                                    || last!!.packageName != item.packageName)
                            if (!res) {
                                last!!.count += item.count + 1
                                last!!.id = item.id
                            }
                        }
                        if (res) last = item
                        return res
                    }
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list: List<FileMonitorRecord?> -> updateData(list) }) { e: Throwable? -> }
    }

    private fun updateData(list: List<FileMonitorRecord?>) {
        if (isStateSaved || context == null) {
            return
        }
        val tmp: MutableList<Any?> = ArrayList(list)
        if (list.size > 0) {
            tmp.add(list[list.size - 1]!!.id)
        }
        adapter.updateItems(requireContext(), tmp)
    }

    private fun clear() {
        topId = 0
        items.clear()
        updateData(emptyList())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.file_monitor, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_clear_history -> {
                AlertDialog.Builder(requireContext())
                        .setTitle(R.string.dialog_file_monitor_clear_title)
                        .setMessage(R.string.dialog_delete_logs_message)
                        .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                            try {
                                val srm = SRManager.createThrow()
                                srm.deleteFileMonitor(null, null)
                                Toast.makeText(requireContext(), R.string.toast_history_cleared, Toast.LENGTH_SHORT).show()
                                clear()
                            } catch (e: DeadObjectException) {
                                e.printStackTrace()
                            } catch (e: Exception) {
                                e.printStackTrace()
                                logException(e)
                            }
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                true
            }
            R.id.action_filter -> {
                FilterSettingsDialog.newInstance(
                        mSpecifiedPackageFilterList,
                        specifiedPackageFilterMode
                ).show(childFragmentManager)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onLoadMore(context: Context, intent: Intent) {
        val lastId = intent.getLongExtra(AppConstants.EXTRA_DATA, 0)
        if (lastId > 0) {
            /*String selection = "id < CAST(? AS INTEGER) AND user=? AND path NOT like ?";
            String[] selectionArgs = new String[]{
                    Long.toString(lastId),
                    Integer.toString(UserHelper.myUserId()),
                    "/storage/emulated/" + UserHelper.myUserId() + "/Android/data/%"
            };*/
            val selection = "id < CAST(? AS INTEGER) AND user=?"
            val selectionArgs = arrayOf(
                    java.lang.Long.toString(lastId),
                    Integer.toString(UserHelper.myUserId())
            )
            load(selection, selectionArgs, "id DESC", "100")
        }
    }

    private fun onSearchModeChanged(context: Context, intent: Intent) {
        if (updateDisposable != null) {
            updateDisposable!!.dispose()
        }
        if (loadDisposable != null) {
            loadDisposable!!.dispose()
        }
        val list = intent.getStringArrayListExtra(AppConstants.EXTRA_PACKAGE_FILTER_LIST)
        val mode = intent.getIntExtra(AppConstants.EXTRA_PACKAGE_FILTER_MODE, -1)
        val path = intent.getStringExtra(AppConstants.EXTRA_FILTER_KEYWORD)
        if (mSpecifiedPackageFilterList == null) {
            fileMonitorPackageFilterList = list!!
        }
        if (specifiedPackageFilterMode == null) {
            fileMonitorPackageFilterMode = mode
        }
        if (!hasSpecifiedPathFilterKeyword) {
            Settings.fileMonitorPathFilterKeyword = path
        } else {
            specifiedPathFilterKeyword = path
        }
        clear()
        resetSubtitle()
        postRefresh(200)
    }

    private fun onRequestInvalidVerifiedSubmit(context: Context, intent: Intent?) {
        if (intent == null || !intent.hasExtra(AppConstants.EXTRA_PACKAGE_NAME) || !intent.hasExtra(AppConstants.EXTRA_PATH)) {
            Log.e(AppConstants.TAG, "onRequestInvalidVerifiedSubmit: Invalid intent")
            return
        }
        newInstance(
                intent.getStringExtra(AppConstants.EXTRA_PACKAGE_NAME)!!,
                intent.getStringExtra(AppConstants.EXTRA_PATH)!!
        ).show(parentFragmentManager)
    }

    companion object {
        fun buildFromIntent(intent: Intent?): FileMonitorFragment {
            val args = Bundle()
            if (intent != null) {
                args.putStringArrayList(AppConstants.EXTRA_PACKAGE_FILTER_LIST,
                        intent.getStringArrayListExtra(AppConstants.EXTRA_PACKAGE_FILTER_LIST))
                if (intent.hasExtra(AppConstants.EXTRA_PACKAGE_FILTER_MODE)) {
                    args.putInt(AppConstants.EXTRA_PACKAGE_FILTER_MODE,
                            intent.getIntExtra(AppConstants.EXTRA_PACKAGE_FILTER_MODE, -1))
                }
                if (intent.hasExtra(AppConstants.EXTRA_FILTER_KEYWORD)) {
                    args.putString(AppConstants.EXTRA_FILTER_KEYWORD, intent.getStringExtra(AppConstants.EXTRA_FILTER_KEYWORD))
                }
            }
            val fragment = FileMonitorFragment()
            fragment.arguments = args
            return fragment
        }
    }
}