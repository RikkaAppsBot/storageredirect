package moe.shizuku.redirectstorage.component.home.viewholder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.applist.AppListActivity;
import moe.shizuku.redirectstorage.component.home.model.HomeStatus;
import rikka.recyclerview.BaseViewHolder;

public class HomeAppListViewHolder extends BaseViewHolder<HomeStatus> implements View.OnClickListener {

    public static final Creator<HomeStatus> CREATOR = (inflater, parent) -> new HomeAppListViewHolder(inflater.inflate(R.layout.home_item_primary, parent, false));

    private ImageView icon;
    private TextView title;
    private TextView summary;

    public HomeAppListViewHolder(View itemView) {
        super(itemView);

        final Context context = itemView.getContext();

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);

        title.setText(R.string.apps_management);
        icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_apps_24dp));

        itemView.setOnClickListener(this);
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();
        final HomeStatus status = getData();

        if (status != null && status.isLoaded()) {
            summary.setText(context.getString(R.string.home_redirected_apps_count, getData().redirectedAppsCount));
            summary.setVisibility(View.VISIBLE);
            itemView.setEnabled(true);
        } else {
            summary.setVisibility(View.GONE);
            itemView.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        final Context context = view.getContext();
        final Intent intent = new Intent(context, AppListActivity.class);
        context.startActivity(intent);
    }
}
