package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.view.View;
import android.widget.CheckBox;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import moe.shizuku.redirectstorage.viewholder.CheckableViewHolder;

public class EditCallMediaScanViewHolder extends CheckableViewHolder<ObserverInfoBuilder> {

    public static final Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditCallMediaScanViewHolder holder = new EditCallMediaScanViewHolder(
                inflater.inflate(R.layout.observer_info_edit_checkbox_item, parent, false));
        holder.itemView.setEnabled(parent.isEnabled());
        ((CheckBox) holder.checkable).setEnabled(parent.isEnabled());
        return holder;
    };

    private EditCallMediaScanViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View view) {
        getData().callMediaScan(!getData().getCallMediaScan());
        super.onBind();
    }

    @Override
    public boolean isChecked() {
        return getData().getCallMediaScan();
    }

    @Override
    public void onBind() {
        super.onBind();
        icon.setImageResource(R.drawable.ic_perm_media_24dp);
        title.setText(R.string.link_dialog_message_call_media_scan);
        summary.setText(R.string.observer_info_edit_call_media_scan_summary);
    }
}
