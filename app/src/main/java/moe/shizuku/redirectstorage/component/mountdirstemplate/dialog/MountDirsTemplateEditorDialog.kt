package moe.shizuku.redirectstorage.component.mountdirstemplate.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.app.AppBarDialogFragment
import moe.shizuku.redirectstorage.component.accessiblefolders.dialog.MountDirsPickerDialog
import moe.shizuku.redirectstorage.component.mountdirstemplate.adapter.MountDirsTemplateContentAdapter
import moe.shizuku.redirectstorage.dialog.picker.MultiFilesPickerDialog
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog
import moe.shizuku.redirectstorage.event.RemoteRequestHandler
import moe.shizuku.redirectstorage.event.ToastErrorHandler
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.model.SimplePackageDescriptor
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.utils.ViewUtils
import moe.shizuku.redirectstorage.utils.getTitle
import rikka.core.compat.CollectionsCompat
import java.util.*
import kotlin.collections.ArrayList

class MountDirsTemplateEditorDialog : AppBarDialogFragment(), PackagesSelectorDialog.Callback, MultiFilesPickerDialog.Callback {

    private var adapter: MountDirsTemplateContentAdapter? = null

    private lateinit var folderList: RecyclerView
    private lateinit var titleEdit: EditText
    private lateinit var folderStatusTextView: TextView
    private lateinit var appsStatusTextView: TextView

    private var reason: Int = 0
    private lateinit var originalTemplate: MountDirsTemplate
    private lateinit var currentTemplate: MountDirsTemplate

    private val folderStatusText: CharSequence
        get() {
            val size = adapter!!.itemCount
            return getString(R.string.mount_dirs_template_folder_format, resources.getQuantityString(R.plurals.folder_amount_text_format, size, size))
        }

    private val appsStatusText: CharSequence
        get() {
            val size = currentTemplate.packages?.filter { it.enabled }?.size ?: 0
            return getString(R.string.mount_dirs_template_app_format, resources.getQuantityString(
                    R.plurals.app_amount_text_format, size, size))
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Objects.requireNonNull(arguments, "Arguments cannot be null")

        setHasDialogOptionsMenu(true)

        reason = requireArguments().getInt(EXTRA_REASON)

        if (savedInstanceState == null) {
            if (REASON_EDIT == reason) {
                originalTemplate = requireArguments().getParcelable(EXTRA_DATA)!!
                currentTemplate = MountDirsTemplate(Objects.requireNonNull(originalTemplate))
            } else if (REASON_NEW == reason) {
                originalTemplate = MountDirsTemplate()
                currentTemplate = MountDirsTemplate()
            }
        } else {
            val new: MountDirsTemplate? = savedInstanceState.getParcelable(EXTRA_DATA)
            if (new == null) {
                dismiss()
            }
            originalTemplate = if (reason == REASON_EDIT) {
                requireArguments().getParcelable(EXTRA_DATA)!!
            } else {
                MountDirsTemplate()
            }
            currentTemplate = new!!
        }
        if (currentTemplate.packages == null) {
            currentTemplate.packages = ArrayList()
        }
        if (originalTemplate.packages == null) {
            originalTemplate.packages = ArrayList()
        }
    }

    override fun onMultiFilesPickerDialogResult(tag: String?, list: List<ServerFile>) {
        adapter!!.updateData(CollectionsCompat.mapToList(list) { it.relativePath })
        requestPrepareDialogOptionsMenu()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (adapter != null) {
            outState.putParcelable(EXTRA_DATA, currentTemplate)
        }
    }

    override fun onBackPressed(): Boolean {
        if (currentTemplate != originalTemplate
                || currentTemplate.title != originalTemplate.title
                || currentTemplate.packages?.size != originalTemplate.packages?.size
                || !originalTemplate.packages!!.containsAll(originalTemplate.packages!!)) {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.exit_confirm_dialog_title)
                    .setMessage(R.string.exit_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        dismiss()
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .setNeutralButton(R.string.exit_confirm_dialog_button_save) { _, _ ->
                        onSaveClick()
                    }
                    .show()
            return false
        }
        return super.onBackPressed()
    }

    override fun onCreateDialogOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.dialog_delete, menu)
        inflater.inflate(R.menu.dialog_save, menu)
    }

    override fun onPrepareDialogOptionsMenu(menu: Menu) {
        menu.findItem(R.id.action_delete)?.isVisible = reason == REASON_EDIT
        menu.findItem(R.id.action_save)?.isEnabled = canSave()
    }

    override fun onDialogOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                onSaveClick()
                return true
            }
            R.id.action_delete -> {
                onDeleteClick()
                return true
            }
        }
        return super.onDialogOptionsItemSelected(item)
    }

    private fun onSaveClick() {
        if (REASON_EDIT == reason) {
            RemoteRequestHandler.updateMountDirsTemplate(currentTemplate, originalTemplate, ToastErrorHandler(requireContext()))
        } else {
            RemoteRequestHandler.addMountDirsTemplate(currentTemplate, ToastErrorHandler(requireContext()))
        }
        dismiss()
    }

    private fun onDeleteClick() {
        AlertDialog.Builder(requireContext())
                .setTitle(R.string.delete_confirm_dialog_title)
                .setMessage(R.string.delete_confirm_dialog_message)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    RemoteRequestHandler.removeMountDirsTemplate(originalTemplate, ToastErrorHandler(requireContext()))
                    dismiss()
                }
                .setNegativeButton(android.R.string.no, null)
                .show()
    }

    override fun onCreateContentView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.mount_dirs_formula_edit_dialog_content, container, false)
    }

    override fun onContentViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        appBar?.setTitle(R.string.edit_template)
        appBar?.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        appBar?.setDisplayHomeAsUpEnabled(true)

        if (adapter == null) {
            adapter = MountDirsTemplateContentAdapter()
            adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onChanged() {
                    folderStatusTextView.text = folderStatusText

                    requestPrepareDialogOptionsMenu()
                    currentTemplate.list.clear()
                    currentTemplate.list.addAll(adapter!!.getItems())

                    folderList.visibility = if (adapter!!.getItems<Any>().isEmpty()) View.GONE else View.VISIBLE
                }

                override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                    onChanged()
                }

                override fun onItemRangeChanged(positionStart: Int, itemCount: Int,
                                                payload: Any?) {
                    onChanged()
                }

                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    onChanged()
                }

                override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                    onChanged()
                }
            })
        }

        titleEdit = view.findViewById(R.id.title_edit)
        folderStatusTextView = view.findViewById(R.id.status_text_folders)
        folderStatusTextView.text = folderStatusText

        titleEdit.setText(currentTemplate.getTitle(context))
        if (savedInstanceState == null) {
            titleEdit.setSelection(currentTemplate.getTitle(context).length)
        }
        titleEdit.isEnabled = true

        titleEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                currentTemplate.title = s.toString()
                requestPrepareDialogOptionsMenu()
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        titleEdit.setOnEditorActionListener { v, actionId, _ ->
            if (EditorInfo.IME_ACTION_NEXT == actionId) {
                ViewUtils.hideKeyboard(v)
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        folderList = view.findViewById(R.id.folder_list)
        folderList.setHasFixedSize(true)
        folderList.adapter = adapter

        adapter!!.updateData(currentTemplate.list)

        view.findViewById<View>(R.id.edit_folders).setOnClickListener { v ->
            val userId = UserHelper.currentUserId()
            val dialog = MountDirsPickerDialog
                    .newInstance(userId, null, adapter!!.getItems())
            dialog.show(childFragmentManager, "mount_dir_picker")
        }

        appsStatusTextView = view.findViewById(R.id.status_text_apps)
        appsStatusTextView.text = appsStatusText

        view.findViewById<View>(R.id.edit_apps).setOnClickListener {
            val selectedPackages = currentTemplate.packages
                    ?.filter { it.enabled }
                    ?.map {
                        SimplePackageDescriptor(it.packageName, it.userId, it.sharedUserId)
                    }
            val userId = SRManager.USER_ALL
            val dialog = MountDirsTemplatePackageSelectorDialog
                    .newInstance(selectedPackages, userId)
            dialog.show(childFragmentManager, "mount_dir_apps_picker")
        }
    }

    fun canSave(): Boolean {
        return currentTemplate.getTitle(requireContext()).isNotEmpty()
                && currentTemplate.list?.isNotEmpty() == true
    }

    override fun onPackageSelectorDialogResult(tag: String?, list: List<SimpleAppInfo>) {
        if (currentTemplate.packages == null) {
            currentTemplate.packages = ArrayList(list.size)
        }
        currentTemplate.packages = list.map {
            RedirectPackageInfo(it.packageName, it.userId).apply {
                enabled = true
            }
        }
        appsStatusTextView.text = appsStatusText

        requestPrepareDialogOptionsMenu()
    }

    companion object {

        @JvmStatic
        fun createNewDialog(): MountDirsTemplateEditorDialog {
            val args = Bundle()
            args.putInt(EXTRA_REASON, REASON_NEW)
            val dialog = MountDirsTemplateEditorDialog()
            dialog.arguments = args
            return dialog
        }

        @JvmStatic
        fun createEditDialog(data: MountDirsTemplate): MountDirsTemplateEditorDialog {
            val args = Bundle()
            args.putInt(EXTRA_REASON, REASON_EDIT)
            args.putParcelable(EXTRA_DATA, data)
            val dialog = MountDirsTemplateEditorDialog()
            dialog.arguments = args
            return dialog
        }

        val TAG = MountDirsTemplateEditorDialog::class.java.simpleName

        const val EXTRA_REASON = BuildConfig.APPLICATION_ID + ".extra.REASON"

        const val REASON_NEW = 1
        const val REASON_EDIT = 2
    }
}
