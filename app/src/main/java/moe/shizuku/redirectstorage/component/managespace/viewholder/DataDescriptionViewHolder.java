package moe.shizuku.redirectstorage.component.managespace.viewholder;

import android.content.Context;
import android.view.View;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.managespace.adapter.ManageSpaceAdapter;
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder;

public class DataDescriptionViewHolder extends SimpleItemViewHolder<Integer> {

    public static final Creator<Integer> CREATOR = (inflater, parent) ->
            new DataDescriptionViewHolder(inflater.inflate(R.layout.manage_space_description_item, parent, false));

    private DataDescriptionViewHolder(View itemView) {
        super(itemView);

        itemView.setBackground(null);
    }

    @Override
    public void onBind() {
        Context context = itemView.getContext();
        switch (getData()) {
            case ManageSpaceAdapter.ID_CLIENT_DATA_INFO: {
                title.setText(R.string.manage_space_client_data_title);
                title.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_apps_24dp, 0, 0, 0);
                summary.setText(R.string.manage_space_client_data_summary);
                break;
            }
            case ManageSpaceAdapter.ID_SERVER_DATA_INFO: {
                title.setText(R.string.manage_space_server_data_title);
                title.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_dns_24dp, 0, 0, 0);
                summary.setText(context.getString(R.string.manage_space_server_data_summary, context.getString(R.string.app_name)));
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {

    }

}
