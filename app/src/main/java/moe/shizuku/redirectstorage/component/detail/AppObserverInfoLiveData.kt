package moe.shizuku.redirectstorage.component.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.model.AppObserverInfo
import moe.shizuku.redirectstorage.model.AppRecommendation
import moe.shizuku.redirectstorage.viewmodel.Resource

class AppObserverInfoLiveData(
        private val appInfo: LiveData<Resource<AppInfo>>,
        private val localObservers: LiveData<Resource<List<AppObserverInfo>>>,
        private val recommendation: LiveData<Resource<AppRecommendation>>
) : MediatorLiveData<Resource<List<AppObserverInfo>>>() {

    init {

        addSource(localObservers) { loadValue() }
        addSource(recommendation) { loadValue() }
    }

    private fun loadValue() {

    }
}