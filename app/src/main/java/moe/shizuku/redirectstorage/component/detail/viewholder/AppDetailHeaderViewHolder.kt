package moe.shizuku.redirectstorage.component.detail.viewholder

import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.AppDetailActivity
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.AppIconCache
import moe.shizuku.redirectstorage.utils.ShowAppInfoHelper
import moe.shizuku.redirectstorage.utils.UserHelper
import rikka.material.chooser.ChooserActivity
import rikka.material.chooser.ChooserFragment
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailHeaderViewHolder(itemView: View) : BaseViewHolder<AppInfo>(itemView) {

    companion object {

        val CREATOR = Creator<AppInfo> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailHeaderViewHolder(inflater.inflate(R.layout.detail_header, parent, false)) }
    }

    var icon: ImageView = itemView.findViewById(android.R.id.icon)
    var name: TextView = itemView.findViewById(android.R.id.title)
    var packageName: TextView = itemView.findViewById(android.R.id.text1)
    var versionName: TextView = itemView.findViewById(android.R.id.text2)
    var button: View = itemView.findViewById(android.R.id.button1)

    init {
        button.setOnClickListener { onShowAppInfo() }
    }

    override fun onBind() {
        val context = itemView.context
        if (UserHelper.myUserId() != data.userId) {
            name.text = context.getString(R.string.app_name_with_user, data.label, data.userId)
        } else {
            name.text = data.label
        }
        versionName.text = context.getString(R.string.detail_header_app_version, data.packageInfo.versionName ?: "null")
        packageName.text = data.packageName
        button.visibility = if (data.userId == UserHelper.myUserId()) View.VISIBLE else View.GONE
        if (data.applicationInfo != null) {
            AppIconCache.loadIconBitmapAsync(context, data.applicationInfo!!, data.userId, icon)
        } else {
            icon.setImageResource(R.mipmap.ic_default_app_icon)
        }
    }

    private fun onShowAppInfo() {
        if (data.userId == UserHelper.myUserId()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val targetIntent = Intent("android.intent.action.SHOW_APP_INFO")
                        .putExtra("android.intent.extra.PACKAGE_NAME", data.packageName)
                val appInfoIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + data.packageName))
                val intent = Intent.createChooser(targetIntent, context.getString(R.string.show_app_info_in)).apply {
                    putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, arrayOf(ComponentName.createRelative(context, AppDetailActivity::class.java.name)))
                    putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(appInfoIntent))
                }
                context.startActivity(intent)
            } else {

                val targetIntent = ShowAppInfoHelper.newIntent(context, data.packageName)
                val exclude: MutableList<ComponentName> = arrayListOf(ComponentName(context, AppDetailActivity::class.java.name))

                val intent = ChooserFragment.newIntent(
                        ComponentName.createRelative(context, ChooserActivity::class.java.name),
                        context.getString(R.string.show_app_info_in), targetIntent,
                        null, exclude)

                val args = intent.getBundleExtra(ChooserFragment.EXTRA_ARGUMENTS)!!
                val appInfoIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + data.packageName))

                var extraResolves = context.packageManager.queryIntentActivities(appInfoIntent, PackageManager.MATCH_DEFAULT_ONLY)
                if (extraResolves.size >= 1) extraResolves = extraResolves.subList(0, 1)
                if (extraResolves.isNotEmpty()) {
                    val extraIntents = ArrayList<Intent>()
                    extraIntents.add(appInfoIntent)
                    args.putParcelableArrayList(ChooserFragment.ARG_EXTRA_RESOLVE_INFO_LIST, ArrayList(extraResolves))
                    args.putParcelableArrayList(ChooserFragment.ARG_EXTRA_INTENT_LIST, extraIntents)
                }
                context.startActivity(intent)
            }

            /*val targetIntent = ShowAppInfoHelper.newIntent(context, data.packageName)
            val appInfoIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + data.packageName))
            val intent = Intent.createChooser(targetIntent, context.getString(R.string.show_app_info_in)).apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, arrayOf(ComponentName.createRelative(context, AppDetailActivity::class.java.name)))
                }
                putExtra(Intent.EXTRA_ALTERNATE_INTENTS, arrayOf(appInfoIntent))
            }
            context.startActivity(intent)*/
        }
    }
}