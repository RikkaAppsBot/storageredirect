package moe.shizuku.redirectstorage.component.mountdirstemplate

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.BaseViewModel
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.event.GlobalConfigChangedEvent
import moe.shizuku.redirectstorage.viewmodel.Resource

class MountDirsTemplateViewModel : BaseViewModel(), GlobalConfigChangedEvent {

    val templates = MutableLiveData<Resource<List<MountDirsTemplate>>>()
    private val templateList = ArrayList<MountDirsTemplate>()

    init {
        Events.registerGlobalConfigChangedEvent(this)
    }

    fun load() {
        templates.postValue(Resource.loading(null))

        viewModelScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    val srm = SRManager.createThrow()
                    val newList = srm.mountDirsTemplates
                    synchronized(templateList) {
                        templateList.clear()
                        for (template in newList) {
                            if (template != null) {
                                templateList.add(template)
                            }
                        }
                        templates.postValue(Resource.success(templateList))
                    }
                }
            } catch (e: CancellationException) {
            } catch (e: Throwable) {
                templates.postValue(Resource.error(e, null))
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        Events.unregisterGlobalConfigChangedEvent(this)
    }

    override fun onMountDirsTemplateAdded(template: MountDirsTemplate) {
        synchronized(templateList) {
            if (templateList.add(template)) {
                templates.postValue(Resource.success(templateList))
            }
        }
    }

    override fun onMountDirsTemplateChanged(newTemplate: MountDirsTemplate, oldTemplate: MountDirsTemplate) {
        synchronized(templateList) {
            val index = templateList.indexOf(oldTemplate)
            templateList[index] = newTemplate
            templates.postValue(Resource.success(templateList))
        }
    }

    override fun onMountDirsTemplateRemoved(template: MountDirsTemplate) {
        synchronized(templateList) {
            if (templateList.remove(template)) {
                templates.postValue(Resource.success(templateList))
            }
        }
    }
}