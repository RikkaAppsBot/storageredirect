package moe.shizuku.redirectstorage.component.foldersanalysis

import android.app.TaskStackBuilder
import android.os.Bundle
import androidx.fragment.app.commit
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.component.foldersanalysis.fragment.FoldersAnalysisFragment
import moe.shizuku.redirectstorage.ktx.putPackageExtrasFrom
import moe.shizuku.redirectstorage.model.AppInfo

class FoldersAnalysisActivity : AppBarFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appBar?.setDisplayHomeAsUpEnabled(true)

        val appInfo = intent.getParcelableExtra<AppInfo>(EXTRA_DATA)!!

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.fragment_container, FoldersAnalysisFragment.newInstance(appInfo))
            }
        }
    }

    override fun onPrepareNavigateUpTaskStack(builder: TaskStackBuilder) {
        super.onPrepareNavigateUpTaskStack(builder)
        builder.editIntentAt(builder.intentCount - 1).putPackageExtrasFrom(intent)
    }
}