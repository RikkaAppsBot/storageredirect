package moe.shizuku.redirectstorage.component.applist.adapter;

import java.util.ArrayList;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.adapter.BasePopupMenuAdapter;
import moe.shizuku.redirectstorage.app.Settings;

public class SortFilterPopupMenuAdapter extends BasePopupMenuAdapter {

    public static final int GID_SORT_ORDER = 1;
    public static final int ID_SORT_SUBTITLE = R.id.action_sort_subtitle;
    public static final int ID_SORT_BY_APP_NAME = R.id.action_sort_by_app_name;
    public static final int ID_SORT_BY_INSTALL_TIME = R.id.action_sort_by_install_time;
    public static final int ID_SORT_BY_UPDATE_TIME = R.id.action_sort_by_update_time;
    public static final int ID_SORT_ENABLED_FIRST = R.id.action_sort_enabled_first;
    public static final int ID_FILTER_SUBTITLE = R.id.action_filter_subtitle;

    public SortFilterPopupMenuAdapter() {
        super(new ArrayList<>());
        items.add(Item.createSubtitle(ID_SORT_SUBTITLE, R.string.action_sort));
        Group group = new Group(GID_SORT_ORDER);
        items.add(group.createItem(ID_SORT_BY_APP_NAME, R.string.action_sort_by_app_name));
        items.add(group.createItem(ID_SORT_BY_INSTALL_TIME, R.string.action_sort_by_install_time));
        items.add(group.createItem(ID_SORT_BY_UPDATE_TIME, R.string.action_sort_by_updated_time));
        items.add(Item.createSubtitle(ID_FILTER_SUBTITLE, R.string.action_options));
        items.add(Item.createMultiCheckable(ID_SORT_ENABLED_FIRST, R.string.action_enabled_first));
        updateItemsChecked();
    }

    @Override
    public void updateItemsChecked() {
        switch (Settings.getSortAccording()) {
            case Settings.SortAccording.ACCORDING_NAME:
                findItem(ID_SORT_BY_APP_NAME).setChecked(true);
                break;
            case Settings.SortAccording.ACCORDING_INSTALL_TIME:
                findItem(ID_SORT_BY_INSTALL_TIME).setChecked(true);
                break;
            case Settings.SortAccording.ACCORDING_UPDATE_TIME:
                findItem(ID_SORT_BY_UPDATE_TIME).setChecked(true);
                break;
        }
        findItem(ID_SORT_ENABLED_FIRST).setChecked(Settings.isSortEnabledFirst());
    }

}
