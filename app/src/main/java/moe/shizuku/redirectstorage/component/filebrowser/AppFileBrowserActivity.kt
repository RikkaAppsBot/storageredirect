package moe.shizuku.redirectstorage.component.filebrowser

import android.app.TaskStackBuilder
import android.os.Bundle
import androidx.fragment.app.commit
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.component.detail.appDetailViewModel
import moe.shizuku.redirectstorage.ktx.getPackageNameExtra
import moe.shizuku.redirectstorage.ktx.getUserIdExtra
import moe.shizuku.redirectstorage.ktx.putPackageExtrasFrom
import moe.shizuku.redirectstorage.viewmodel.Status

class AppFileBrowserActivity : AppActivity() {

    private lateinit var fragment: AppFileBrowserFragment

    private val model by appDetailViewModel()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.elevation = 0f
        }

        val packageName = intent.getPackageNameExtra()
        val userId = intent.getUserIdExtra()

        if (savedInstanceState == null) {
            fragment = AppFileBrowserFragment.newInstance(packageName, userId)
            supportFragmentManager.commit {
                replace(android.R.id.content, fragment)
            }
        } else {
            fragment = supportFragmentManager.findFragmentById(android.R.id.content) as AppFileBrowserFragment
        }

        model.appInfo.observe(this) { res ->
            if (res?.status == Status.SUCCESS) {
                title = res.data?.label

            }
        }
    }

    override fun onBackPressed() {
        if (!fragment.onBack()) {
            super.onBackPressed()
        }
    }

    override fun onPrepareNavigateUpTaskStack(builder: TaskStackBuilder) {
        super.onPrepareNavigateUpTaskStack(builder)
        builder.editIntentAt(builder.intentCount - 1).putPackageExtrasFrom(intent)
    }
}