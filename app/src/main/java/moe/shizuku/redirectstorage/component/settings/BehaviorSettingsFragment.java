package moe.shizuku.redirectstorage.component.settings;

import android.content.Intent;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.mountdirstemplate.MountDirsTemplateActivity;
import moe.shizuku.redirectstorage.dialog.MediaPathSelectDialog;
import moe.shizuku.redirectstorage.dialog.RedirectTargetSelectDialog;

public class BehaviorSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.BEHAVIOR";

    @Override
    public boolean isVerticalPaddingRequired() {
        return true;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_behavior);

        findPreference("redirect_target").setOnPreferenceClickListener((preference) -> {
            RedirectTargetSelectDialog.newInstance().show(getParentFragmentManager());
            return true;
        });

        findPreference("custom_mount_dirs_formula").setOnPreferenceClickListener((pref) -> {
            final Intent intent = new Intent(requireActivity(), MountDirsTemplateActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            requireActivity().startActivity(intent);
            return true;
        });

        findPreference("media_path").setOnPreferenceClickListener((pref) -> {
            MediaPathSelectDialog.newInstance().show(getParentFragmentManager());
            return true;
        });

        findPreference("show_multi_user").setOnPreferenceChangeListener((pref, o) -> {
            LocalBroadcastManager.getInstance(requireContext())
                    .sendBroadcast(new Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME));
            LocalBroadcastManager.getInstance(requireContext())
                    .sendBroadcast(new Intent(AppConstants.ACTION_REQUEST_REFRESH_LIST));
            return true;
        });
    }
}
