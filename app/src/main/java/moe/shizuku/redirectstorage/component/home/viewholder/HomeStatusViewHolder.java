package moe.shizuku.redirectstorage.component.home.viewholder;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.component.home.model.HomeStatus;
import moe.shizuku.redirectstorage.widget.ExpandableLayout;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class HomeStatusViewHolder extends BaseViewHolder<HomeStatus> implements View.OnClickListener, Checkable {

    public static final Creator<HomeStatus> CREATOR = (inflater, parent) -> new HomeStatusViewHolder(inflater.inflate(R.layout.home_status, parent, false));

    private ImageView icon;
    private TextView title;
    private TextView summary;
    private View expandableContainer;
    private TextView expandableTitle;
    private TextView expandableText;
    private CheckedTextView expandableButton;
    private ExpandableLayout expandableLayout;

    private final int okColor;
    private final int unknownColor;

    public HomeStatusViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);

        expandableButton = itemView.findViewById(android.R.id.button1);
        expandableContainer = itemView.findViewById(R.id.expandable);
        expandableTitle = itemView.findViewById(android.R.id.text2);
        expandableText = itemView.findViewById(android.R.id.text1);
        expandableText.setMovementMethod(LinkMovementMethod.getInstance());
        expandableLayout = (ExpandableLayout) expandableText.getParent();
        expandableButton.setOnClickListener(this);

        TypedArray a = itemView.getContext().obtainStyledAttributes(null, R.styleable.HomeStatusTheme, R.attr.homeStatusStyle, 0);
        okColor = a.getColor(R.styleable.HomeStatusTheme_okColor, 0);
        unknownColor = a.getColor(R.styleable.HomeStatusTheme_unknownColor, 0);
        a.recycle();

        setBackgroundColor(unknownColor);

        expandableTitle.setText(R.string.home_important_notice);
    }

    private void setBackgroundColor(@ColorInt int color) {
        itemView.setBackgroundTintList(ColorStateList.valueOf(color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            itemView.setOutlineAmbientShadowColor(color);
            itemView.setOutlineSpotShadowColor(color);
        }
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();
        final HomeStatus status = getData();

        if (status != null && status.isLoaded()) {
            title.setText(R.string.home_status_running);
            summary.setText(context.getResources().getQuantityString(R.plurals.home_redirected_actions_count, (int) (getData().redirectedProcessCount) & 0xff, getData().redirectedProcessCount));
            summary.setVisibility(View.VISIBLE);
            setBackgroundColor(status.redirectedProcessCount > 0 ? okColor : unknownColor);

            if (!status.enhanceModule) {
                expandableContainer.setVisibility(View.VISIBLE);
                expandableText.setText(HtmlCompat.fromHtml(context.getString(R.string.home_using_basic_mode)));
            } else if (status.enhanceModuleVersion < 17) {
                expandableContainer.setVisibility(View.VISIBLE);
                expandableText.setText(HtmlCompat.fromHtml(context.getString(R.string.home_using_enhanced_mode_outdated)));
            } else if (status.redirectedProcessCount == 0) {
                expandableContainer.setVisibility(View.VISIBLE);
                expandableText.setText(HtmlCompat.fromHtml(context.getString(R.string.home_using_enhanced_mode_count_zero)));
            } else {
                expandableContainer.setVisibility(View.GONE);
            }
        } else {
            title.setText(R.string.home_status_loading);
            summary.setVisibility(View.GONE);
            expandableContainer.setVisibility(View.GONE);
            setBackgroundColor(unknownColor);
        }

        syncViewState();
    }

    @Override
    public void onClick(View view) {
        setChecked(!expandableButton.isChecked());
        syncViewState();
    }

    private String getHelpPreferenceKey() {
        final HomeStatus status = getData();
        if (status == null)
            return "home_basic_mode_show_help";
        else if (!status.enhanceModule)
            return "home_basic_mode_show_help";
        else if (status.enhanceModuleVersion < 17)
            return "home_enhanced_mode_pre_v17_show_help";
        else if (status.redirectedProcessCount == 0)
            return "home_enhanced_mode_count_zero_show_help";
        else
            return "home_enhanced_mode_show_help";
    }

    public boolean isChecked() {
        return Settings.getPreferences().getBoolean(getHelpPreferenceKey(), true);
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }

    public void setChecked(boolean checked) {
        Settings.getPreferences().edit().putBoolean(getHelpPreferenceKey(), checked).apply();
    }

    private void syncViewState() {
        expandableButton.setChecked(isChecked());
        expandableButton.setText(isChecked() ? R.string.btn_collapse : R.string.btn_expand);
        expandableLayout.setExpanded(isChecked());
    }
}
