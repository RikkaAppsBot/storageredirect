package moe.shizuku.redirectstorage.component.detail.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import moe.shizuku.redirectstorage.component.detail.adapter.AppDetailAdapter
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailExportFoldersViewHolder.Listener
import moe.shizuku.redirectstorage.viewholder.SimpleItemListenerViewHolder
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailExportFoldersViewHolder(itemView: View) : SimpleItemListenerViewHolder<AppDetailViewModel, Listener?>(itemView) {

    companion object {
        val CREATOR = Creator<AppDetailViewModel> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailExportFoldersViewHolder(inflater.inflate(R.layout.detail_export_folders, parent, false)) }
    }

    interface Listener {
        fun onObserversClick()
    }

    private val text1 = itemView.findViewById<TextView>(android.R.id.text1)

    override fun onClick(view: View) {
        listener!!.onObserversClick()
    }

    override fun onBind() {
        updateSummary()
        itemView.isEnabled = adapter.appInfo.isRedirectEnabled

        val config = data.recommendation.value?.data?.config
        val rule = config?.exportFolders?.text()

        if (rule != null) {
            text1.text = HtmlCompat.fromHtml(rule)
            text1.visibility = View.VISIBLE
        } else {
            text1.visibility = View.GONE
        }
    }

    override fun onBind(payloads: List<Any>) {
        updateSummary()
    }

    private fun updateSummary() {
        val context = itemView.context
        var enabled = 0
        val list = data.observers.value?.data
        if (list != null) {
            enabled = list.filter { it.local && it.enabled }.size
        }
        summary.text = context.resources.getQuantityString(R.plurals.detail_rule_count_enabled, enabled, enabled)
    }

    override fun getAdapter(): AppDetailAdapter {
        return super.getAdapter() as AppDetailAdapter
    }
}