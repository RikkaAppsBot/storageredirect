package moe.shizuku.redirectstorage.component.submitrule.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import moe.shizuku.redirectstorage.R
import rikka.recyclerview.BaseViewHolder

/**
 * Created by fytho on 2017/12/12.
 */

class AppRulesRequestButtonRowViewHolder(itemView: View, callback: Callback) : BaseViewHolder<Any>(itemView) {

    companion object {
        @JvmStatic
        fun newCreator(callback: Callback): RowCreator {
            return RowCreator(callback)
        }
    }

    init {
        val createNewLinkButton = itemView.findViewById<View>(android.R.id.button1)
        createNewLinkButton.setOnClickListener {
            callback.onCreateNewLinkButtonClick()
        }
    }

    class RowCreator(private val mCallback: Callback) : Creator<Any> {

        override fun createViewHolder(inflater: LayoutInflater, parent: ViewGroup): BaseViewHolder<Any> {
            return AppRulesRequestButtonRowViewHolder(
                    inflater.inflate(R.layout.app_rules_request_button_row, parent, false),
                    mCallback)
        }

    }

    interface Callback {

        fun onCreateNewLinkButtonClick()

        fun onDoneButtonClick()

    }

}
