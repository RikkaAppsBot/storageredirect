package moe.shizuku.redirectstorage.component.home.model;

import moe.shizuku.redirectstorage.model.ModuleStatus;

public class HomeStatus {

    public static final HomeStatus LOADING = new HomeStatus();

    public int serverVersion;
    public boolean enhanceModule;
    public ModuleStatus enhanceModuleStatus;
    public int enhanceModuleVersion;
    public long redirectedProcessCount;
    public int redirectedAppsCount;

    public boolean isLoaded() {
        return this != LOADING;
    }
}
