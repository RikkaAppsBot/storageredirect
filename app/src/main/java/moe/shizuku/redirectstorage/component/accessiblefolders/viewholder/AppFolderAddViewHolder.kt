package moe.shizuku.redirectstorage.component.accessiblefolders.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppFolderAddViewHolder(itemView: View) : BaseListenerViewHolder<Any, AppFolderAddViewHolder.Listener>(itemView) {

    companion object {
        val CREATOR = Creator<Any> { inflater: LayoutInflater, parent: ViewGroup? -> AppFolderAddViewHolder(inflater.inflate(R.layout.dialog_mount_dirs_template_selector_add, parent, false)) }
    }

    interface Listener {

        fun onAddSimpleMountClick()
    }

    init {
        itemView.findViewById<TextView>(android.R.id.title).text = itemView.context.getString(R.string.dialog_accessible_folders_other_apps_add)
        itemView.setOnClickListener {
            listener?.onAddSimpleMountClick()
        }
    }
}