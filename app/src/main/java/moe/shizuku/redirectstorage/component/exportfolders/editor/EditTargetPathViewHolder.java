package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import rikka.recyclerview.BaseViewHolder;

public class EditTargetPathViewHolder extends EditButtonViewHolder<ObserverInfoBuilder, EditTargetPathViewHolder.Listener> {

    public static final BaseViewHolder.Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditTargetPathViewHolder holder = new EditTargetPathViewHolder(
                inflater.inflate(R.layout.observer_info_edit_edit_button, parent, false));
        holder.editButton.setEnabled(parent.isEnabled());
        return holder;
    };

    public interface Listener {

        void onEditTargetPathClick();
    }

    private EditTargetPathViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public Drawable getIconDrawable() {
        return itemView.getResources().getDrawable(R.drawable.ic_folder_open_24dp, itemView.getContext().getTheme());
    }

    @Override
    public CharSequence getTitle() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_target_path);
    }

    @Override
    public CharSequence getHint() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_target_path_hint);
    }

    @Override
    public void onClick() {
        getListener().onEditTargetPathClick();
    }

    @Override
    public void onBind() {
        super.onBind();
        editButton.setContentText(getData().getTarget());
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        onBind();
    }
}
