package moe.shizuku.redirectstorage.component.settings;

import android.os.Bundle;

import moe.shizuku.preference.Preference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;

public class MainSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.SETTINGS";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings);

        Preference modulePreference = findPreference("module");
        try {
            SRManager srm = SRManager.create();
            if (srm == null || srm.getModuleStatus().getVersionCode() < 0) {
                modulePreference.setVisible(false);
            }
        } catch (Throwable tr) {
            modulePreference.setVisible(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    public void onRefresh() {
    }

}
