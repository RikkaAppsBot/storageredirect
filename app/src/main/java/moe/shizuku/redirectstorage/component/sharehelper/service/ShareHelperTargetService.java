package moe.shizuku.redirectstorage.component.sharehelper.service;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.service.chooser.ChooserTarget;
import android.service.chooser.ChooserTargetService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import moe.shizuku.redirectstorage.component.sharehelper.ShareHelperActivity;
import moe.shizuku.redirectstorage.drawable.DrawableUtils;
import moe.shizuku.redirectstorage.utils.ShareHelperListPreferences;

public class ShareHelperTargetService extends ChooserTargetService {

    private ShareHelperListPreferences mPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        mPreferences = new ShareHelperListPreferences(this);
    }

    @Override
    public List<ChooserTarget> onGetChooserTargets(
            ComponentName targetActivityName, IntentFilter matchedFilter) {
        if (getPackageManager().getComponentEnabledSetting(getShareHelperComponentName())
                != PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                || matchedFilter.countActions() < 1
                || matchedFilter.countDataTypes() < 1) {
            return Collections.emptyList();
        }
        final List<ChooserTarget> result = new ArrayList<>();

        final String type = matchedFilter.getDataType(0);
        final Intent toShareIntent = new Intent(matchedFilter.getAction(0));
        toShareIntent.setType("*".equals(type) ? "*/*" : type);
        toShareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://fake_uri"));

        final List<ActivityInfo> activityInfo = mPreferences.getInstalledApps(toShareIntent)
                .filter(info -> info.showInDirectShare)
                .map(info -> info.activityInfo)
                .toList()
                .blockingGet();

        int count = 0;
        for (ActivityInfo item : activityInfo) {
            count++;

            final CharSequence targetTitle = item.loadLabel(getPackageManager());
            final Icon targetIcon;
            if (item.getIconResource() != 0) {
                targetIcon = Icon.createWithResource(item.packageName, item.getIconResource());
            } else {
                targetIcon = Icon.createWithBitmap(
                        DrawableUtils.toBitmap(item.loadIcon(getPackageManager())));
            }

            final Bundle extraBundle = new Bundle();
            extraBundle.putParcelable(
                    ShareHelperActivity.EXTRA_TARGET_COMPONENT,
                    ComponentName.createRelative(item.packageName, item.name)
            );

            result.add(new ChooserTarget(targetTitle, targetIcon, 0.1f * count,
                    getShareHelperComponentName(), extraBundle));

            if (count >= 4) {
                break;
            }
        }

        return result;
    }

    private ComponentName getShareHelperComponentName() {
        return ComponentName.createRelative(this, ShareHelperActivity.class.getName());
    }

}
