package moe.shizuku.redirectstorage.component.accessiblefolders.dialog

import android.os.Bundle
import androidx.core.os.bundleOf
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME
import moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog
import moe.shizuku.redirectstorage.model.IPackageDescriptor
import moe.shizuku.redirectstorage.model.SelectableSimpleAppInfo
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.model.SimplePackageDescriptor
import moe.shizuku.redirectstorage.utils.UserHelper

class MountInfoPackageSelectorDialog : PackagesSelectorDialog() {

    companion object {

        @JvmStatic
        fun newInstance(excludePackageName: String, selectedName: String?, userId: Int) =
                MountInfoPackageSelectorDialog().apply {
                    arguments = bundleOf(
                            EXTRA_USER_ID to userId,
                            EXTRA_PACKAGE_NAME to excludePackageName,
                            EXTRA_SELECTED_PACKAGES to selectedName?.let {
                                if (RedirectPackageInfo.isSharedUserId(selectedName)) {
                                    arrayListOf(SimplePackageDescriptor.create(it, UserHelper.currentUserId(), RedirectPackageInfo.getRawSharedUserId(it)))
                                } else {
                                    arrayListOf(SimplePackageDescriptor.create(it, UserHelper.currentUserId(), null))
                                }
                            }
                    )
                }
    }

    private lateinit var excludePackage: String
    private var userId: Int = 0

    override val maxSelectedCount: Int = 1

    override val minSelectedCount: Int = 1

    private var enabledPackages: List<RedirectPackageInfo>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        excludePackage = arguments?.getString(EXTRA_PACKAGE_NAME)!!
        userId = arguments?.getInt(EXTRA_USER_ID)!!
    }

    override fun loadAllAvailablePackages(selectedPackages: List<IPackageDescriptor>?): List<SimpleAppInfo> {
        val pm = context!!.packageManager
        val srm = SRManager.createThrow()
        enabledPackages = srm.getRedirectPackages(SRManager.MATCH_ENABLED_ONLY, userId)
        val list = srm.getInstalledPackages(
                // select from only redirected packages
                SRManager.GET_PACKAGE_INFO or SRManager.MATCH_ALL_PACKAGES, userId
        )

        val excludeSharedUserId = list.firstOrNull { it.packageName == excludePackage }?.sharedUserId

        return list
                .filter {
                    it.packageName != excludePackage && (excludeSharedUserId == null || it.sharedUserId != excludeSharedUserId)
                }
                .map {
                    if (it.sharedUserId != null) {
                        SharedUserLabelCache.load(pm, it.sharedUserLabel, it.sharedUserId, it.packageName, it.applicationInfo)
                    }
                    SimpleAppInfo(
                            it.packageName,
                            it.applicationInfo.uid / 100000,
                            it.applicationInfo,
                            pm.getApplicationLabel(it.applicationInfo),
                            it.sharedUserId
                    )
                }
                .map { it }
                .sortedWith(Comparator { o1, o2 ->
                    val s1 = selectedPackages?.contains(SimplePackageDescriptor(o1.packageName, o1.userId, o1.sharedUserId))
                            ?: false
                    val s2 = selectedPackages?.contains(SimplePackageDescriptor(o2.packageName, o2.userId, o2.sharedUserId))
                            ?: false
                    val s = s2.compareTo(s1)
                    if (s != 0) {
                        return@Comparator s
                    }

                    val e1 = enabledPackages?.contains(RedirectPackageInfo(o1.packageName, o1.userId))
                            ?: false
                    val e2 = enabledPackages?.contains(RedirectPackageInfo(o2.packageName, o2.userId))
                            ?: false
                    val e = e2.compareTo(e1)
                    if (e != 0) {
                        return@Comparator e
                    }

                    o1.compareTo(o2)
                })
    }

    override fun onCreateAdapterItem(it: SelectableSimpleAppInfo) {
        val summary = StringBuilder()
        if (enabledPackages?.contains(RedirectPackageInfo(it.packageName, it.userId)) == true) {
            summary.append(getString(R.string.redirect_enabled)).append('\n')
        }
        if (it.appInfo.sharedUserId != null) {
            summary.append(context!!.getString(R.string.shared_user_id_name, SharedUserLabelCache.get(it.appInfo.sharedUserId))).append('\n')
        }
        if (summary.isNotBlank()) {
            it.summary = summary.trim().toString()
        }
    }
}