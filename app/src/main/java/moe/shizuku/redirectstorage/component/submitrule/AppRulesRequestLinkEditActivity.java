package moe.shizuku.redirectstorage.component.submitrule;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.component.exportfolders.editor.ExportFolderEditFragment;
import moe.shizuku.redirectstorage.component.submitrule.fragment.AppRulesRequestObserverEditFragment;
import moe.shizuku.redirectstorage.model.AppInfo;

public class AppRulesRequestLinkEditActivity extends AppActivity {

    public static final String EXTRA_EDIT_REASON = BuildConfig.APPLICATION_ID + "..extra.REASON";
    public static final String EXTRA_APP_INFO = BuildConfig.APPLICATION_ID + ".extra.APP_INFO";
    public static final String EXTRA_OBSERVER = BuildConfig.APPLICATION_ID + ".extra.OBSERVER";
    public static final String STATE_BUILDER = BuildConfig.APPLICATION_ID + ".state.BUILDER";

    public static final int EDIT_REASON_ADD = 0;
    public static final int EDIT_REASON_EDIT = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        final int editReason = intent.getIntExtra(EXTRA_EDIT_REASON, -1);
        final AppInfo appInfo = intent.getParcelableExtra(EXTRA_APP_INFO);
        final ObserverInfo observer = intent.getParcelableExtra(EXTRA_OBSERVER);

        final String title;
        switch (editReason) {
            case EDIT_REASON_ADD:
                title = getString(R.string.observer_info_edit_dialog_add_title);
                break;
            case EDIT_REASON_EDIT:
                title = getString(R.string.observer_info_edit_dialog_edit_title);
                break;
            default:
                throw new IllegalArgumentException();
        }
        setTitle(title);
        final ActionBar actionBar = Objects.requireNonNull(getSupportActionBar());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp);

        if (savedInstanceState == null) {
            final Fragment fragment;
            switch (editReason) {
                case EDIT_REASON_ADD:
                    fragment = AppRulesRequestObserverEditFragment.createForAdd(appInfo);
                    break;
                case EDIT_REASON_EDIT:
                    fragment = AppRulesRequestObserverEditFragment.createForEdit(appInfo, observer);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        final ExportFolderEditFragment fragment = (AppRulesRequestObserverEditFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
        if (fragment != null && fragment.getBuilder().getChanged()) {
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.observer_info_edit_confirm_exit)
                    .setPositiveButton(android.R.string.yes, (d, w) -> super.onBackPressed())
                    .setNegativeButton(android.R.string.cancel, null)
                    .create();
            dialog.setOnShowListener(d -> {
                dialog.<TextView>findViewById(android.R.id.message).setLineSpacing(0f, 1.2f);
            });
            dialog.show();
        } else {
            super.onBackPressed();
        }
    }
}
