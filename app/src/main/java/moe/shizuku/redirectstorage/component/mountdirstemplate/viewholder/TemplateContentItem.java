package moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder;

import android.view.View;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class TemplateContentItem extends BaseViewHolder<String> {

    public static final Creator<String> CREATOR = (inflater, parent) ->
            new TemplateContentItem(inflater.inflate(R.layout.mount_dirs_item_simple_layout, parent, false));

    private TextView title;

    public TemplateContentItem(View itemView) {
        super(itemView);

        title = itemView.findViewById(android.R.id.title);
    }

    @Override
    public void onBind() {
        title.setText(HtmlCompat.fromHtml(String.format("<li gap=\"16sp\">%s</li>", getData()), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE));
    }
}
