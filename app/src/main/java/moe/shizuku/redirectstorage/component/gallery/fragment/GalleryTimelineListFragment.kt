package moe.shizuku.redirectstorage.component.gallery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.gallery.adapter.GalleryTimelineListAdapter
import moe.shizuku.redirectstorage.component.gallery.model.PreviewModel
import moe.shizuku.redirectstorage.dialog.ExceptionDialog
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.MediaStoreUtils
import rikka.recyclerview.addFastScroller
import java.util.*

class GalleryTimelineListFragment : AppFragment() {

    companion object {

        @JvmStatic
        fun newInstance(appInfo: AppInfo): GalleryTimelineListFragment {
            return GalleryTimelineListFragment().apply {
                arguments = bundleOf(EXTRA_DATA to appInfo)
            }
        }

    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: View

    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var adapter: GalleryTimelineListAdapter

    private lateinit var appInfo: AppInfo
    private lateinit var mountDirs: MutableList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appInfo = arguments?.getParcelable(EXTRA_DATA)!!

        try {
            mountDirs = SRManager.createThrow()
                    .getMountDirsForPackage(appInfo.packageName, appInfo.userId)
                    .toMutableList()
        } catch (tr: Throwable) {
        }

        val iterate = mountDirs.listIterator()
        while (iterate.hasNext()) {
            val value = iterate.next()
            iterate.set("/storage/emulated/${appInfo.userId}/" + value.toLowerCase())
        }

        /*if (BuildUtils.isQ() && BuildUtils.isIsolatedStorageEnabled()) {
            mMountDirs.add("/storage/emulated/${mAppInfo.userId}/Android/sandbox".toLowerCase())
        }*/
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.gallery_folders_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progress = view.findViewById(android.R.id.progress)
        recyclerView = view.findViewById(android.R.id.list)

        recyclerView.addFastScroller()
        recyclerView.setHasFixedSize(true)

        adapter = GalleryTimelineListAdapter(recyclerView)
        gridLayoutManager = GridLayoutManager(view.context, 4)

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (adapter.getItems<Any>()[position] is Date) {
                    gridLayoutManager.spanCount
                } else {
                    1
                }
            }
        }

        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = adapter

        refreshList()
    }

    private fun refreshList() {
        progress.isVisible = true

        launch {
            try {
                val res = withContext(Dispatchers.IO) {
                    val list = MediaStoreUtils.listImages(appInfo.userId)!!
                    MediaStoreUtils.groupByDate(list).map { item ->
                        if (item is PreviewModel) {
                            item.inMountDirs = false
                            for (mountDir in mountDirs) {
                                if (item.data.path.toLowerCase(Locale.ENGLISH)
                                                .startsWith("$mountDir/")) {
                                    item.inMountDirs = true
                                    break
                                }
                            }
                        }
                        item
                    }
                }

                progress.isVisible = false

                adapter.setItems(res)
                adapter.notifyDataSetChanged()
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
                progress.isVisible = false
                ExceptionDialog.show(parentFragmentManager, e, false, "This may be due to the modification of your system which can't be solved by us.")
            }
        }
    }
}