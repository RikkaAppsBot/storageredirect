package moe.shizuku.redirectstorage.component.submitrule

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.api.GitHubApi
import moe.shizuku.redirectstorage.app.Settings

class GitHubAuthorizationReceiverActivity : Activity() {

    companion object {

        const val URI_START_WIZARD = "storage-redirect-client://start-rules-request-wizard"
        const val URI_AUTHORIZED_CALLBACK = "storage-redirect-client://callback"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val uri = intent?.data
        if (uri?.toString()?.startsWith(URI_AUTHORIZED_CALLBACK) == true) {
            GlobalScope.launch {
                try {
                    val token = GitHubApi.requestAccessToken(uri.getQueryParameter("code")!!)

                    requireNotNull(token) { "Returned token is null." }

                    Settings.githubToken = token
                    GitHubApi.loginWithoutVerifying(token)

                    LocalBroadcastManager.getInstance(applicationContext)
                            .sendBroadcast(Intent(AppConstants.ACTION_GITHUB_AUTHORIZED))
                } catch (e: Exception) {
                    e.printStackTrace()
                    LocalBroadcastManager.getInstance(applicationContext)
                            .sendBroadcast(Intent(AppConstants.ACTION_GITHUB_AUTHORIZED))
                }

                finish()
            }
        }
    }

}
