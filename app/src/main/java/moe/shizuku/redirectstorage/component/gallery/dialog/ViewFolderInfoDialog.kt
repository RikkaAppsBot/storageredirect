package moe.shizuku.redirectstorage.component.gallery.dialog

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PREFIX
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import rikka.html.widget.HtmlCompatTextView

class ViewFolderInfoDialog : AlertDialogFragment() {

    companion object {

        const val EXTRA_APP_NAME = "$EXTRA_PREFIX.APP_NAME"
        const val EXTRA_RELATIVE_LOCATION = "$EXTRA_PREFIX.RELATIVE_LOCATION"
        const val EXTRA_REAL_LOCATION = "$EXTRA_PREFIX.REAL_LOCATION"
        const val EXTRA_COUNT = "$EXTRA_PREFIX.COUNT"

        @JvmStatic
        fun newInstance(appName: String, relativeLocation: String,
                        realLocation: String, count: Int): ViewFolderInfoDialog {
            return ViewFolderInfoDialog().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_APP_NAME, appName)
                    putString(EXTRA_RELATIVE_LOCATION, relativeLocation)
                    putString(EXTRA_REAL_LOCATION, realLocation)
                    putInt(EXTRA_COUNT, count)
                }
            }
        }

    }

    private lateinit var mRelativeLocationTitle: HtmlCompatTextView
    private lateinit var mRelativeLocationText: TextView
    //private lateinit var mRealLocationText: TextView
    private lateinit var mCountText: TextView

    private lateinit var mAppName: String
    private lateinit var mRelativeLocation: String
    private lateinit var mRealLocation: String
    private var mCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireArguments().let {
            mAppName = it.getString(EXTRA_APP_NAME)!!
            mRelativeLocation = it.getString(EXTRA_RELATIVE_LOCATION)!!
            mRealLocation = it.getString(EXTRA_REAL_LOCATION)!!
            mCount = it.getInt(EXTRA_COUNT)
        }
    }

    override fun onGetContentViewLayoutResource(): Int {
        return R.layout.gallery_folder_item_info_dialog_layout
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        with (builder) {
            setTitle(R.string.gallery_folder_item_info_dialog_title)
            setPositiveButton(android.R.string.ok, null)
        }
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View,
                                      savedInstanceState: Bundle?) {
        mRelativeLocationTitle = contentView.findViewById(R.id.relative_location_title)
        mRelativeLocationText = contentView.findViewById(R.id.relative_location_text)
        //mRealLocationText = contentView.findViewById(R.id.real_location_text)
        mCountText = contentView.findViewById(R.id.photos_count_text)

        mRelativeLocationTitle.setHtmlText(getString(R.string.gallery_folder_item_info_dialog_location))
        mRelativeLocationText.text = mRelativeLocation
        //mRealLocationText.text = mRealLocation
        mCountText.text = resources.getQuantityString(
                R.plurals.photo_amount_text_format, mCount, mCount)
    }

}