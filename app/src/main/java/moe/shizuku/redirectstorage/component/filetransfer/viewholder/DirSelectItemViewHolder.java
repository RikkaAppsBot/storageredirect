package moe.shizuku.redirectstorage.component.filetransfer.viewholder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.filetransfer.adapter.FileTransferAdapter;
import moe.shizuku.redirectstorage.viewholder.MasterCheckableViewHolder;

public class DirSelectItemViewHolder extends MasterCheckableViewHolder<FileTransferAdapter.ObserverInfoItem> {

    public static final Creator<FileTransferAdapter.ObserverInfoItem> CREATOR = (inflater, parent) ->
            new DirSelectItemViewHolder(inflater.inflate(R.layout.file_transfer_item_observer, parent, false));

    private DirSelectItemViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind() {
        super.onBind();

        final Context context = itemView.getContext();
        title.setText(getData().observer.source);
        summary.setText(context.getString(R.string.file_transfer_observer_item_summary, getData().files.size()));
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        super.onBind(payloads);
        onBind();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSwitchClick(View view) {
        getData().checked = !getData().checked;
        getAdapter().notifyItemChanged(getAdapterPosition(), AppConstants.PAYLOAD);
    }

    @Override
    public boolean isChecked() {
        return getData().checked;
    }

}
