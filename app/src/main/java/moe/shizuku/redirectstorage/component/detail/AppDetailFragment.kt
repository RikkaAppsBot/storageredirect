package moe.shizuku.redirectstorage.component.detail

import android.app.Activity
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.AppConstants.*
import moe.shizuku.redirectstorage.adapter.LoadingAdapter
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.accessiblefolders.AccessibleFoldersActivity
import moe.shizuku.redirectstorage.component.detail.adapter.AppDetailAdapter
import moe.shizuku.redirectstorage.component.detail.dialog.ViewSharedUserIdDialog
import moe.shizuku.redirectstorage.component.exportfolders.list.ExportFoldersActivity
import moe.shizuku.redirectstorage.component.filebrowser.AppFileBrowserActivity
import moe.shizuku.redirectstorage.component.filemonitor.FileMonitorActivity
import moe.shizuku.redirectstorage.component.filetransfer.FileTransferActivity
import moe.shizuku.redirectstorage.component.foldersanalysis.FoldersAnalysisActivity
import moe.shizuku.redirectstorage.component.gallery.ViewGalleryActivity
import moe.shizuku.redirectstorage.component.submitrule.UploadDialogFragment
import moe.shizuku.redirectstorage.databinding.AppDetailBinding
import moe.shizuku.redirectstorage.event.AppConfigChangedEvent
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.event.GlobalConfigChangedEvent
import moe.shizuku.redirectstorage.ktx.makeToast
import moe.shizuku.redirectstorage.license.License
import moe.shizuku.redirectstorage.model.AppConfiguration
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.Logger.LOGGER
import moe.shizuku.redirectstorage.utils.SubtitleListener
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.viewmodel.observeIgnoreInitial
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.core.ktx.unsafeLazy
import rikka.internal.help.HelpProvider
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderView
import kotlin.reflect.KClass

class AppDetailFragment : AppFragment(), GlobalConfigChangedEvent, AppConfigChangedEvent {

    companion object {

        fun newInstance(): AppDetailFragment {
            return AppDetailFragment()
        }
    }

    private val model by appDetailViewModel()

    private inline val appInfo get() = model.appInfo.value!!.data!!

    private val adapter by unsafeLazy { AppDetailAdapter() }

    private val loadingAdapter: LoadingAdapter by unsafeLazy { LoadingAdapter() }

    private val adapterListener by lazy {

        object : AppDetailAdapter.Listener {

            override fun onAccessibleFoldersClick() {
                startActivity(createIntentOfChild(AccessibleFoldersActivity::class))
            }

            override fun onObserversClick() {
                startActivity(createIntentOfChild(ExportFoldersActivity::class))
            }

            override fun onFileBrowserClick() {
                onShowFileBrowser()
            }

            override fun onMediaBrowserClick() {
                onShowMediaBrowser()
            }

            override fun onMustReadHelpClick() {
                with(requireContext()) {
                    val entity = HelpProvider.get(this, "must_read")
                    if (entity != null) {
                        entity.startActivity(this)
                    } else {
                        makeToast(R.string.toast_cannot_load_help).show()
                    }
                }
            }

            override fun onFileHistoryClick() {
                onShowFileMonitor()
            }

            override fun onShareClick() {
                onShowShareMenu()
            }

            override fun onFolderAnalysisClick() {
                onShowFolderAnalysis()
            }

            override fun onIgnoreVerifiedClick() {
                adapter.ignoredVerified = true
                refreshData()
            }

            override fun onReloadRuleClick() {
                model.invalidateRecommendation(requireContext())
            }

            override fun onSharedUserIdClick() {
                ViewSharedUserIdDialog.show(parentFragmentManager)
            }
        }
    }

    private var nfcAdapter: NfcAdapter? = null

    private var subtitleListener: SubtitleListener? = null

    private lateinit var binding: AppDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            License.checkLocal()
        }

        val context = requireContext()

        model.appInfo.observe(this) { res ->
            LOGGER.d("${this::class.java.simpleName}: AppInfo")

            when (res.status) {
                Status.SUCCESS -> {
                    binding.list.adapter = adapter
                    if (!Settings.reduceHelp) {
                        binding.helpButton.root.show()
                    }

                    val appInfo = res.data!!
                    if (appInfo.configuration == null) {
                        appInfo.configuration = AppConfiguration.DEFAULT
                    }
                    refreshData()

                    Events.registerGlobalConfigChangedEvent(this)
                    Events.registerAppConfigChangedEvent(this, appInfo.packageName, appInfo.userId)

                    setHasOptionsMenu(true)
                    activity?.invalidateOptionsMenu()

                    subtitleListener?.setSubtitle(appInfo.getSubtitle(context))
                }
                Status.ERROR -> {
                }
                Status.LOADING -> {
                    binding.list.adapter = loadingAdapter
                }
            }
        }
        model.observers.observe(this) {
            LOGGER.d("${this::class.java.simpleName}: AppInfo observers")

            if (it.status == Status.SUCCESS) {
                adapter.notifyConfigChanged(context, AppInfo.CONFIG_CHANGED_TYPE_OBSERVERS, model)
            }
        }
        model.simpleMounts.observe(this) {
            LOGGER.d("${this::class.java.simpleName}: AppInfo simpleMounts")

            if (it.status == Status.SUCCESS) {
                adapter.notifyConfigChanged(context, AppInfo.CONFIG_CHANGED_TYPE_ACCESSIBLE_FOLDERS, model)
            }
        }
        model.mountDirs.observe(this) {
            LOGGER.d("${this::class.java.simpleName}: AppInfo mountDirs")

            adapter.notifyConfigChanged(context, AppInfo.CONFIG_CHANGED_TYPE_ACCESSIBLE_FOLDERS, model)
        }
        model.recommendation.observeIgnoreInitial(this) {
            LOGGER.d("${this::class.java.simpleName}: AppInfo recommendation")

            if (it.status == Status.SUCCESS) {
                activity?.invalidateOptionsMenu()
                refreshData()
            } else if (it.status == Status.ERROR) {
                refreshData()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (model.appInfo.value?.data != null) {
            Events.unregisterGlobalConfigChangedEvent(this)
            Events.unregisterAppConfigChangedEvent(this, appInfo.packageName, appInfo.userId)
        }
    }

    private fun refreshData() {
        if (model.appInfo.value?.data == null) {
            return
        }
        LOGGER.d("${this::class.java.simpleName}: refreshData")
        adapter.updateData(requireContext(), model)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = AppDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val recyclerView = binding.list
        val helpButton = binding.helpButton.root

        recyclerView.addItemDecoration(VerticalPaddingDecoration(context))
        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            if (activity != null) {
                (activity as AppBarFragmentActivity).appBar?.setRaised(!top)
            }
        }
        recyclerView.fixEdgeEffect()

        subtitleListener = SubtitleListener.attach(recyclerView, this, null, savedInstanceState)

        if (!Settings.reduceHelp) {
            binding.helpButton.root.setOnClickListener {
                onShowHelp()
            }
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0)
                        helpButton.hide()
                    else if (dy < 0)
                        helpButton.show()
                }
            })
        } else {
            helpButton.isVisible = false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        /*if (applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(applicationContext)
            nfcAdapter?.setNdefPushMessageCallback(null, requireActivity())
        }*/

        adapter.listener = adapterListener
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        subtitleListener?.onSavedInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val loaded = model.appInfo.value?.data != null
        val enabled = loaded && appInfo.isRedirectEnabled
        val storageAvailable = model.userStorageAvailable
        menu.findItem(R.id.action_file_transfer)?.isEnabled = enabled && storageAvailable
        menu.findItem(R.id.action_folders_analysis)?.isEnabled = enabled && storageAvailable
        menu.findItem(R.id.action_view_gallery)?.isEnabled = enabled && storageAvailable
        menu.findItem(R.id.action_view_redirect_storage)?.isEnabled = enabled && storageAvailable
        menu.findItem(R.id.action_upload)?.isEnabled = model.recommendation.value?.status == Status.SUCCESS
        menu.findItem(R.id.action_help)?.isVisible = Settings.reduceHelp

        // TODO Remove DEBUG check
        menu.findItem(R.id.action_file_transfer)?.isVisible = BuildConfig.DEBUG
                && loaded && appInfo.configuration?.isDefault == false
        menu.findItem(R.id.action_view_gallery)?.isVisible = loaded && appInfo.userId == UserHelper.myUserId()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_file_transfer -> {
                val intent = Intent(requireActivity(), FileTransferActivity::class.java)
                intent.putExtra(EXTRA_DATA, appInfo)
                startActivity(intent)
                return true
            }
            R.id.action_folders_analysis -> {
                onShowFolderAnalysis()
                return true
            }
            R.id.action_view_iohistory -> {
                onShowFileMonitor()
                return true
            }
            R.id.action_view_redirect_storage -> {
                onShowFileBrowser()
                return true
            }
            R.id.action_view_gallery -> {
                onShowMediaBrowser()
                return true
            }
            R.id.action_upload -> {
                onShowUpload()
                return true
            }
            R.id.action_help -> {
                onShowHelp()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onEnabledChanged(packageName: String, sharedUserId: String?, userId: Int, enabled: Boolean) {
        if (appInfo.packageName != packageName || appInfo.userId != userId) return
        appInfo.isRedirectEnabled = enabled
        appInfo.redirectInfo.enabled = enabled
        adapter.notifyConfigChanged(requireContext(), AppInfo.CONFIG_CHANGED_TYPE_ENABLED, model)

        activity?.invalidateOptionsMenu()
    }

    override fun onRedirectTargetChanged(packageName: String, sharedUserId: String?, userId: Int, redirectTarget: String?) {
        appInfo.redirectInfo.redirectTarget = redirectTarget
        adapter.notifyConfigChanged(requireContext(), AppInfo.CONFIG_CHANGED_TYPE_REDIRECT_TARGET, model)
    }

    override fun onMountDirsConfigChanged(packageName: String, sharedUserId: String?, userId: Int, data: MountDirsConfig) {
        appInfo.redirectInfo.mountDirsConfig = data
        //model.loadLocalData(applicationContext, false)
        adapter.notifyConfigChanged(requireContext(), AppInfo.CONFIG_CHANGED_TYPE_ACCESSIBLE_FOLDERS, model)
    }

    override fun onObserverAdded(oi: ObserverInfo) {
        if (appInfo.packageName != oi.packageName || appInfo.userId != oi.userId) return
        model.invalidateObservers()
    }

    override fun onObserverChanged(oi: ObserverInfo, old: ObserverInfo) {
        if (appInfo.packageName != oi.packageName || appInfo.userId != oi.userId) return
        model.invalidateObservers()
    }

    override fun onObserverRemoved(oi: ObserverInfo) {
        if (appInfo.packageName != oi.packageName || appInfo.userId != oi.userId) return
        model.invalidateObservers()
    }

    override fun onSimpleMountAdded(smi: SimpleMountInfo) {
        if (appInfo.packageName != smi.targetPackage || appInfo.userId != smi.userId) return
        model.invalidateSimpleMounts()
    }

    override fun onSimpleMountChanged(smi: SimpleMountInfo, old: SimpleMountInfo) {
        if (appInfo.packageName != smi.targetPackage || appInfo.userId != smi.userId) return
        model.invalidateSimpleMounts()
    }

    override fun onSimpleMountRemoved(smi: SimpleMountInfo) {
        if (appInfo.packageName != smi.targetPackage || appInfo.userId != smi.userId) return
        model.invalidateSimpleMounts()
    }

    override fun onDefaultRedirectTargetChanged(redirectTarget: String) {
        adapter.notifyConfigChanged(requireContext(), AppInfo.CONFIG_CHANGED_TYPE_REDIRECT_TARGET, model)
    }

    private fun onShowFileBrowser() {
        startActivity(createIntentOfChild(AppFileBrowserActivity::class))
    }

    private fun onShowMediaBrowser() {
        startActivity(createIntentOfChild(ViewGalleryActivity::class).putExtra(EXTRA_DATA, appInfo))
    }

    private fun onShowFileMonitor() {
        context?.let {
            startActivity(Intent(it, FileMonitorActivity::class.java)
                    .putExtra(EXTRA_PACKAGE_FILTER_LIST, arrayListOf(appInfo.packageName))
                    .putExtra(EXTRA_PACKAGE_FILTER_MODE, Settings.PackageFilterMode.WHITELIST)
                    .putExtra(EXTRA_FILTER_KEYWORD, ""))
        }
    }

    private fun onShowFolderAnalysis() {
        startActivity(createIntentOfChild(FoldersAnalysisActivity::class).putExtra(EXTRA_DATA, appInfo))
    }

    private fun onShowUpload() {
        UploadDialogFragment.show(parentFragmentManager)
        /*val context = requireContext()
        startActivity(createIntentOfChild(SubmitRuleActivity::class))*/
    }

    private fun onShowShareMenu() {
        /*val items = ArrayList<String>()
        items.add(getString(R.string.share_configuration_read_from_clipboard))
        if (nfcAdapter != null) {
            items.add(getString(R.string.share_configuration_via_nfc))
        }
        items.add(getString(R.string.share_configuration_via_other_apps))
        AlertDialog.Builder(activity)
                .setItems(items.toTypedArray()) { _, which ->
                    when {
                        which == 0 -> startReadFromClipboard()
                        which == 1 && nfcAdapter != null -> onShareViaNfc()
                        (which == 1 && nfcAdapter == null) || which == 2 -> onShareViaApps()
                    }
                }
                .show()*/
    }

    private fun onShowHelp() = with(requireContext()) {
        val entity = HelpProvider.get(this, "must_read")
        if (entity != null) {
            entity.startActivity(this)
        } else {
            makeToast(R.string.toast_cannot_load_help).show()
        }
    }

    private fun createIntentOfChild(cls: KClass<out Activity>): Intent {
        val activity = requireActivity()
        val intent = Intent(requireActivity().intent)
        intent.action = null
        intent.setClassName(activity, cls.java.name)
        return intent
    }

    /*private fun onShareViaApps() = launch {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT, makeConfigurationUrl())
        intent.type = "text/plain"
        startActivity(Intent.createChooser(
                intent, getString(R.string.share_configuration_via_other_apps)))
    }

    private fun onShareViaNfc() = launch {
        val record = NdefRecord.createUri(makeConfigurationUrl())
        NdefMessagePushDialog.newInstance(NdefMessage(record)).show(parentFragmentManager)
    }

    private suspend fun makeConfigurationUrl(): String = withContext(Dispatchers.Default) {
        val srm = SRManager.createThrow()
        val config = AppConfiguration.Builder(appInfo).apply {
            recommendedMountDirs = ArrayList(srm.getMountDirsForPackage(
                    appInfo.redirectInfo.packageName, appInfo.userId, true))

            recommendedRedirectTarget = appInfo.redirectInfo.redirectTarget
            observers = srm.getObservers(appInfo.packageName, appInfo.userId)
            isRecommended = recommendationStatus.config?.isRecommended
                    ?: appInfo.redirectInfo.enabled
            isVerified = recommendationStatus.config?.isVerified == true
        }.build()

        val configJson = Gson().toJson(config)
        return@withContext Uri.Builder()
                .scheme("storage-redirect-client")
                .authority("app-configuration")
                .appendPath(Base64.encodeToString(configJson))
                .toString()
    }

    private fun startReadFromClipboard() = launch {
        val cm = context?.getSystemService(ClipboardManager::class.java) ?: run {
            Log.e(TAG, "Cannot access clipboard")
            return@launch
        }
        val clipData = cm.primaryClip
        if (clipData == null || clipData.itemCount < 1) {
            context?.makeToast(R.string.toast_clipboard_empty)?.show()
            return@launch
        }

        val received: AppConfiguration
        try {
            val uri = Uri.parse(clipData.getItemAt(0)
                    .text.toString())
            if ("app-configuration" == uri.authority) {
                received = Gson().fromJson(
                        Base64.decodeToString(uri.lastPathSegment),
                        AppConfiguration::class.java
                ) ?: throw NullPointerException("Cannot resolve configuration from uri")
            } else {
                throw IllegalArgumentException("Unsupported app configuration uri")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            context?.makeToast(R.string.toast_illegal_share_configuration)?.show()
            return@launch
        }

        if (received.packageName != appInfo.packageName) {
            context?.makeToast(R.string.toast_share_configuration_not_the_same_package)?.show()
            return@launch
        }

        try {
            withContext(Dispatchers.IO) {
                val srm = SRManager.createThrow()
                // TODO
                /*srm.setMountDirsCustomForPackage(received.recommendedMountDirs ?: emptyList(),
                        mAppInfo.packageName, mAppInfo.userId)*/
                srm.setRedirectTargetForPackage(received.recommendedRedirectTarget,
                        appInfo.packageName, appInfo.userId)
                for (oi in Optional.safeList(received.observers)) {
                    oi.userId = appInfo.userId
                    oi.packageName = appInfo.packageName
                    try {
                        srm.updateObserver(oi)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            context?.makeToast(getString(R.string.toast_failed, e.message))?.show()
            return@launch
        }

        Events.postAppConfigChangedEvent(appInfo.packageName, appInfo.userId) {
            it.onRedirectTargetChanged(appInfo.packageName, appInfo.userId,
                    received.recommendedRedirectTarget)
        }
        /*sendBroadcast(EventIntents.AppRedirectConfig.notifyMountDirs(
                mAppInfo, received.recommendedMountDirs
        ))*/
        model.loadLocalData(applicationContext, false)

        context?.makeToast(R.string.toast_load_configuration_finished)?.show()
    }

    class NdefMessagePushDialog : AlertDialogFragment() {

        companion object {

            fun newInstance(msg: NdefMessage): NdefMessagePushDialog {
                return NdefMessagePushDialog().also { it.arguments = bundleOf(EXTRA_DATA to msg) }
            }

        }

        private lateinit var message: NdefMessage

        private var mNfcAdapter: NfcAdapter? = null

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            message = arguments?.getParcelable(EXTRA_DATA)!!
        }

        override fun onGetContentViewLayoutResource(): Int {
            return R.layout.dialog_ndef_message_push_content_view
        }

        override fun onBuildAlertDialog(builder: AlertDialog.Builder,
                                        savedInstanceState: Bundle?) {
            builder.setNegativeButton(android.R.string.cancel, null)
        }

        override fun onAlertDialogCreated(dialog: AlertDialog,
                                          contentView: View, savedInstanceState: Bundle?) {
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)

            val summary = contentView.findViewById<HtmlCompatTextView>(android.R.id.summary)

            summary.setHtmlText(requireContext().getString(R.string.share_configuration_nfc_dialog_summary,
                    requireContext().getString(R.string.app_name)))
            summary.movementMethod = LinkMovementMethod.getInstance()
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            mNfcAdapter = NfcAdapter.getDefaultAdapter(requireActivity())
            try {
                mNfcAdapter?.setNdefPushMessageCallback(
                        NfcAdapter.CreateNdefMessageCallback { message },
                        requireActivity()
                )
            } catch (ignored: Exception) {

            }
        }

        override fun onDetach() {
            try {
                mNfcAdapter?.setNdefPushMessageCallback(null, requireActivity())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            super.onDetach()
        }

    }*/

}
