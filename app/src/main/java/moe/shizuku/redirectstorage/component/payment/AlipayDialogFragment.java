package moe.shizuku.redirectstorage.component.payment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import moe.shizuku.redirectstorage.license.AlipayHelper;
import rikka.internal.payment.AlipayPaymentDialogFragment;
import rikka.internal.payment.api.PaymentService;
import rikka.internal.payment.model.AlipayProductInfo;

import static moe.shizuku.redirectstorage.AppConstants.ACTION_PURCHASED_CHANGED;

public class AlipayDialogFragment extends AlipayPaymentDialogFragment {

    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;

    private static final String KEY_PRODUCT_ID = "a";
    private static final String KEY_PRODUCT_INFO = "b";

    public static AlipayDialogFragment newInstance(String productId, AlipayProductInfo productInfo) {
        Bundle args = new Bundle();
        args.putString(KEY_PRODUCT_ID, productId);
        args.putParcelable(KEY_PRODUCT_INFO, productInfo);
        AlipayDialogFragment fragment = new AlipayDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String mProductId;
    private AlipayProductInfo mProductInfo;
    private Context mApplicationContext;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mApplicationContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getArguments() != null;

        mProductId = getArguments().getString(KEY_PRODUCT_ID);
        mProductInfo = getArguments().getParcelable(KEY_PRODUCT_INFO);
    }

    @NonNull
    @Override
    public String getPackageName() {
        return PACKAGE_NAME;
    }

    @NonNull
    @Override
    public String getProductId() {
        return mProductId;
    }

    @NonNull
    @Override
    public AlipayProductInfo getProductInfo() {
        return mProductInfo;
    }

    @NonNull
    @Override
    public PaymentService getPaymentService() {
        return ContextKt.getApplication(mApplicationContext).getPaymentService();
    }

    @Nullable
    @Override
    public String getOrderId() {
        return AlipayHelper.getLastOrderId();
    }

    @Override
    public void onOrderIdUpdated(String orderId) {
        AlipayHelper.setLastOrderId(orderId);
    }

    @Override
    public void onPurchaseSucceed(String orderId, long createTime) {
        AlipayHelper.setOrderId(orderId);
        AlipayHelper.setCreateTime(createTime);

        if (getContext() != null) {
            LocalBroadcastManager.getInstance(mApplicationContext)
                    .sendBroadcast(new Intent(ACTION_PURCHASED_CHANGED));
        }
    }

    @Override
    public void onContentViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onContentViewCreated(view, savedInstanceState);
    }
}
