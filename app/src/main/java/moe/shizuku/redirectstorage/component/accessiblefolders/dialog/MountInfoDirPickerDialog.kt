package moe.shizuku.redirectstorage.component.accessiblefolders.dialog

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PREFIX
import moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRFileManager
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.dialog.picker.MultiFilesPickerDialog
import moe.shizuku.redirectstorage.ktx.applyAll
import moe.shizuku.redirectstorage.model.SelectableServerFile
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.FileUtils
import rikka.html.text.HtmlCompat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class MountInfoDirPickerDialog : MultiFilesPickerDialog() {

    companion object {

        private const val EXTRA_SOURCE_PACKAGE = "${EXTRA_PREFIX}.SOURCE_PACKAGE"
        private const val EXTRA_TARGET_PACKAGE = "${EXTRA_PREFIX}.TARGET_PACKAGE"
        private const val EXTRA_SOURCE_LABEL = "${EXTRA_PREFIX}.SOURCE_LABEL"
        private const val EXTRA_TARGET_LABEL = "${EXTRA_PREFIX}.TARGET_LABEL"

        @JvmStatic
        fun newInstance(sourcePackageName: String,
                        targetPackageName: String,
                        sourceLabel: String,
                        targetLabel: String,
                        userId: Int,
                        selected: List<String>? = null
        ): MountInfoDirPickerDialog {
            return MountInfoDirPickerDialog().apply {
                arguments = bundleOf(
                        EXTRA_CURRENT_FILE to ServerFile.fromRedirectTarget("", sourcePackageName, userId),
                        EXTRA_SOURCE_PACKAGE to sourcePackageName,
                        EXTRA_TARGET_PACKAGE to targetPackageName,
                        EXTRA_USER_ID to userId,
                        EXTRA_SOURCE_LABEL to sourceLabel,
                        EXTRA_TARGET_LABEL to targetLabel,
                        EXTRA_SELECTED_FILES to selected?.map {
                            val file = ServerFile.fromRedirectTarget(it, sourcePackageName, userId)
                            file.setIsDirectory(true)
                            if (FileUtils.isChildOf("Android/data/$sourcePackageName", it)
                                    || FileUtils.isChildOf("Android/media/$sourcePackageName", it)
                                    || FileUtils.isChildOf("Android/obb/$sourcePackageName", it)) {
                                file.packageName = null
                                file.setStorageRootType()
                            }
                            file
                        }?.let { ArrayList(it) }
                )
            }
        }

    }

    private lateinit var fileManager: SRFileManager

    private lateinit var sourcePackageName: String
    private var sourceEnabled: Boolean = false
    private lateinit var targetPackageName: String
    private lateinit var sourceLabel: String
    private lateinit var targetLabel: String
    private var userId: Int = 0

    private var sourceMountDirs: MutableList<String>? = null
    private var targetMountDirs: MutableList<String>? = null
    private val targetObserverDirs: MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments!!.let {
            sourcePackageName = it.getString(EXTRA_SOURCE_PACKAGE)!!
            targetPackageName = it.getString(EXTRA_TARGET_PACKAGE)!!
            sourceLabel = it.getString(EXTRA_SOURCE_LABEL)!!
            targetLabel = it.getString(EXTRA_TARGET_LABEL)!!
            userId = it.getInt(EXTRA_USER_ID)
        }

        try {
            val srm = SRManager.createThrow()
            sourceEnabled = srm.getRedirectPackageInfo(sourcePackageName, SRManager.MATCH_ENABLED_ONLY, userId) != null
            sourceMountDirs = ArrayList(srm.getMountDirsForPackage(sourcePackageName, userId))
            targetMountDirs = ArrayList(srm.getMountDirsForPackage(targetPackageName, userId))
            for (observer in srm.getObservers(targetPackageName, userId)) {
                targetObserverDirs.add(observer.source)
            }
        } catch (e: Throwable) {
            // TODO Show error
            dismiss()
            return
        }

        sourceMountDirs?.applyAll { it.toLowerCase(Locale.ENGLISH) }
        targetMountDirs?.applyAll { it.toLowerCase(Locale.ENGLISH) }
        targetObserverDirs.applyAll { it.toLowerCase(Locale.ENGLISH) }

        fileManager = SRManager.create()?.fileManager ?: run {
            // TODO Show error
            dismiss()
            return
        }

        rootPathLabel = requireContext().getString(R.string.mount_info_edit_dialog_root_path, sourceLabel)
    }

    override fun checkResult(list: MutableList<ServerFile>): Boolean {
        var res = true
        var message: CharSequence? = null
        list.forEach loop@{
            val relativePath = it.relativePath.toLowerCase(Locale.ENGLISH)
            targetMountDirs?.forEach { mountDir ->
                if (FileUtils.isChildOf(mountDir, relativePath)) {
                    res = false
                    message = HtmlCompat.fromHtml(getString(R.string.dialog_mount_info_mount_dir_selected,
                            sourceLabel,
                            targetLabel,
                            "<font face=\"sans-serif-condensed-medium\">${it.relativePath}</font>"
                    ), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                    return@loop
                }
            }
            targetObserverDirs.forEach { observed ->
                if (FileUtils.isChildOf(observed, relativePath)
                        || FileUtils.isChildOf(relativePath, observed)) {
                    res = false
                    message = HtmlCompat.fromHtml(getString(R.string.dialog_mount_info_observer_selected,
                            sourceLabel,
                            targetLabel,
                            "<font face=\"sans-serif-condensed-medium\">${it.relativePath}</font>",
                            "<font face=\"sans-serif-condensed-medium\">${observed}</font>"
                    ), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                    return@loop
                }
            }
        }

        if (!res) {
            AlertDialog.Builder(context!!)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, null)
                    .show()
        }

        return res
    }

    @SuppressLint("SdCardPath")
    override fun listFiles(file: ServerFile): List<ServerFile> {
        var list = fileManager.list(file, SRFileManager.FLAG_AS_PACKAGE)
                ?.filter {
                    val lowPath = it.relativePath.toLowerCase(Locale.ENGLISH)
                    /*if (!file.isStorageRoot) {
                        sourceMountDirs?.forEach { mountDir ->
                            if (FileUtils.isChildOf(mountDir, lowPath)) {
                                return@filter false
                            }
                        }
                    }*/
                    return@filter !lowPath.equals("Android/data/$sourcePackageName/sdcard", true)
                            && !lowPath.equals("Android/data/$sourcePackageName/cache/sdcard", true)
                }
                ?.toMutableList()
        if (list == null) {
            list = ArrayList()
        }

        return list.sortedWith(Comparator { o1, o2 ->
            if (o1.isDirectory && !o2.isDirectory)
                -1
            else if (!o1.isDirectory && o2.isDirectory)
                1
            else
                o1.name!!.compareTo(o2.name!!)
        })
    }

    override fun onBack(file: ServerFile): ServerFile {
        val parent = super.onBack(file)
        if (parent.relativePath.equals("Android", true)
                || parent.relativePath.equals("Android/data", true)
                || parent.relativePath.equals("Android/media", true)
                || parent.relativePath.equals("Android/obb", true)) {
            parent.packageName = sourcePackageName
            parent.userId = userId
        }
        return parent
    }

    override fun isAllowedEnterSelectedDirectories(): Boolean {
        return false
    }

    override fun onCreateAdapterItem(fileItem: SelectableServerFile) {
        if (!fileItem.isDirectory) {
            return
        }

        val path = fileItem.relativePath.toLowerCase(Locale.ENGLISH)
        val selected = fileItem.selected

        val res = when {
            path.equals("Android/data/$sourcePackageName", true) -> getString(R.string.dir_summary_android_data_package, sourceLabel)
            path.equals("Android/media/$sourcePackageName", true) -> getString(R.string.dir_summary_android_media_package, sourceLabel)
            path.equals("Android/obb/$sourcePackageName", true) -> getString(R.string.dir_summary_android_obb_package, sourceLabel)
            else -> null
        }
        if (res != null) {
            fileItem.normalReason = res
        }

        if (path.equals("Android", true)
                || path.equals("Android/data", true)
                || path.equals("Android/media", true)
                || path.equals("Android/obb", true)) {
            fileItem.selectable = false
            fileItem.allowEnter = true
            fileItem.normalReason = getString(R.string.mount_info_edit_dialog_disabled_reason_android)
        }
    }
}