package moe.shizuku.redirectstorage.component.accessiblefolders.adapter

import android.content.Context
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.RecyclerViewAdapterHelper
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.accessiblefolders.viewholder.AppFolderAddViewHolder
import moe.shizuku.redirectstorage.component.accessiblefolders.viewholder.AppFolderToggleViewHolder
import moe.shizuku.redirectstorage.component.accessiblefolders.viewholder.SharedFolderCustomToggleViewHolder
import moe.shizuku.redirectstorage.component.accessiblefolders.viewholder.SharedFolderToggleViewHolder
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder
import moe.shizuku.redirectstorage.component.detail.viewholder.CollapsibleSummaryViewHolder
import moe.shizuku.redirectstorage.ktx.isFuseUsed
import moe.shizuku.redirectstorage.ktx.isSdcardfsUsed
import moe.shizuku.redirectstorage.model.AppSimpleMountInfo
import moe.shizuku.redirectstorage.viewholder.DetailedErrorViewHolder
import moe.shizuku.redirectstorage.viewmodel.Status
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.IdBasedRecyclerViewAdapter
import java.util.*

class AccessibleFoldersAdapter : IdBasedRecyclerViewAdapter() {

    interface Listener : AppFolderAddViewHolder.Listener,
            AppFolderToggleViewHolder.Listener,
            SharedFolderCustomToggleViewHolder.Listener,
            SharedFolderToggleViewHolder.Listener

    init {
        setHasStableIds(true)
    }

    private val idProvider: MutableList<String> = ArrayList()

    private fun generateSimpleMountId(info: AppSimpleMountInfo): Long {
        val key = java.lang.String.format("sn %s", info.id)
        val index: Int = idProvider.indexOf(key)
        if (index != -1) return (index + ID_START + 100)
        idProvider.add(key)
        return idProvider.size - 1 + ID_START + 100
    }

    fun reloadItems(context: Context, viewModel: AppDetailViewModel) {
        clear()

        val appInfo = viewModel.appInfo.value!!.data!!
        val mountDirsConfig = appInfo.redirectInfo.mountDirsConfig
        val simpleMounts = viewModel.simpleMounts.value?.data
        val templates = viewModel.mountDirsTemplates.value?.data

        // mount dirs
        addItem(CollapsibleSummaryViewHolder.CREATOR, arrayOf<Any>(
                context.getString(R.string.dialog_accessible_folders_shared),
                HtmlCompat.fromHtml(context.getString(R.string.dialog_accessible_folders_shared_help), HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM or HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE),
                "accessible_folders_shared_show_help"
        ), ID_SHARED_HEADER)

        // templates
        templates?.forEachIndexed { index, template ->
            addItem(SharedFolderToggleViewHolder.CREATOR, SharedFolder(template, index == 0, enabled = mountDirsConfig?.templatesIds?.contains(template.id) == true), template.hashCode().toLong() shl 32)
        }
        addItem(AppDetailSummaryViewHolder.INFORMATION_CREATOR, context.getString(R.string.dialog_accessible_folders_shared_template_edit_help), ID_SHARED_ADD_TEMPLATE)

        // custom
        RecyclerViewAdapterHelper.addDivider2(this, ID_SHARED_CUSTOM_DIVIDER)
        if (mountDirsConfig != null) {
            addItem(SharedFolderCustomToggleViewHolder.CREATOR, mountDirsConfig, ID_SHARED_CUSTOM)
        }

        // simple mounts
        RecyclerViewAdapterHelper.addDivider(this, ID_DIVIDER)

        if (isFuseUsed && !isSdcardfsUsed) {
            addItem(CollapsibleSummaryViewHolder.CREATOR, arrayOf<Any>(
                    context.getString(R.string.dialog_accessible_folders_other_apps),
                    HtmlCompat.fromHtml(context.getString(R.string.dialog_accessible_folders_other_apps_help_fuse), HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM or HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE),
                    "accessible_folders_other_apps_show_help_fuse"
            ), ID_OTHER_HEADER)
        } else {
            addItem(CollapsibleSummaryViewHolder.CREATOR, arrayOf<Any>(
                    context.getString(R.string.dialog_accessible_folders_other_apps),
                    HtmlCompat.fromHtml(context.getString(R.string.dialog_accessible_folders_other_apps_help), HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM or HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE),
                    "accessible_folders_other_apps_show_help"
            ), ID_OTHER_HEADER)

            val local: MutableList<AppSimpleMountInfo> = ArrayList()
            val online: MutableList<AppSimpleMountInfo> = ArrayList()

            simpleMounts?.forEach {
                if (it.local) {
                    local.add(it)
                } else if (it.targetPackage == appInfo.packageName) {
                    online.add(it)
                }
            }

            // local
            local.forEachIndexed { index, info ->
                info.icon = index == 0
                addItem(AppFolderToggleViewHolder.CREATOR, info, generateSimpleMountId(info))
            }
            if (viewModel.isOnlineRulesEnabled && local.isNotEmpty()) {
                addItem(AppDetailSummaryViewHolder.CREATOR, context.getString(R.string.rule_upload_subrule_message).toHtml(), ID_OTHER_ONLINE_UPLOAD)
            }
            addItem(AppFolderAddViewHolder.CREATOR, null, ID_SHARED_ADD_TEMPLATE)

            // online
            if (viewModel.isOnlineRulesEnabled) {
                RecyclerViewAdapterHelper.addDivider2(this, ID_OTHER_ONLINE_DIVIDER)
                RecyclerViewAdapterHelper.addSubtitle2(this, context.getString(R.string.detail_online_rules_title), ID_OTHER_ONLINE_HEADER)
                when {
                    online.isNotEmpty() -> {
                        for ((index, info) in online.withIndex()) {
                            info.icon = index == 0
                            addItem(AppFolderToggleViewHolder.CREATOR, info, generateSimpleMountId(info))
                        }
                    }
                    viewModel.recommendation.value?.status == Status.ERROR -> {
                        val sb = StringBuilder()
                        if (Settings.isCustomRepoEnabled) {
                            sb.append(context.getString(R.string.rule_network_issue_custom_enabled_summary))
                        }

                        val error = viewModel.recommendation.value?.error!!
                        addItem(DetailedErrorViewHolder.LIST_ITEM_STYLE_CREATOR, DetailedErrorViewHolder.Data(
                                error,
                                null,
                                context.getString(R.string.rule_network_issue).toHtml(),
                                sb.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                        ), ID_OTHER_ONLINE_ERROR)
                    }
                    else -> {
                        addItem(AppDetailSummaryViewHolder.CREATOR, context.getString(R.string.rule_empty_for_type_title).toHtml(), ID_OTHER_ONLINE_EMPTY)
                    }
                }
            }
        }

        notifyDataSetChanged()
    }

    data class SharedFolder(val template: MountDirsTemplate, val icon: Boolean, val enabled: Boolean)

    companion object {

        private const val ID_START: Long = 0
        private const val ID_SHARED_HEADER = ID_START + 1
        private const val ID_SHARED_CUSTOM_DIVIDER = ID_START + 2
        private const val ID_SHARED_CUSTOM = ID_START + 3
        private const val ID_DIVIDER = ID_START + 4
        private const val ID_SHARED_ADD_TEMPLATE = ID_START + 5
        private const val ID_OTHER_HEADER = ID_START + 6
        private const val ID_OTHER_ONLINE_DIVIDER = ID_START + 7
        private const val ID_OTHER_ONLINE_HEADER = ID_START + 8
        private const val ID_OTHER_ONLINE_ERROR = ID_START + 9
        private const val ID_OTHER_ONLINE_EMPTY = ID_START + 10
        private const val ID_OTHER_ONLINE_UPLOAD = ID_START + 11
    }
}