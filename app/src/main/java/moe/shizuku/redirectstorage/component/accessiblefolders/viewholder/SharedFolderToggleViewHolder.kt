package moe.shizuku.redirectstorage.component.accessiblefolders.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.accessiblefolders.adapter.AccessibleFoldersAdapter
import moe.shizuku.redirectstorage.utils.WordsHelper
import moe.shizuku.redirectstorage.utils.getTitle
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class SharedFolderToggleViewHolder(itemView: View) : BaseListenerViewHolder<AccessibleFoldersAdapter.SharedFolder, SharedFolderToggleViewHolder.Listener>(itemView), OnClickListener {

    companion object {
        val CREATOR = Creator<AccessibleFoldersAdapter.SharedFolder> { inflater: LayoutInflater, parent: ViewGroup? -> SharedFolderToggleViewHolder(inflater.inflate(R.layout.accessible_folders_dialog_item_shared_folders, parent, false)) }
    }

    interface Listener {
        fun onViewTemplateClick(template: MountDirsTemplate)
        fun onToggleTemplateClick(template: MountDirsTemplate, newState: Boolean)
    }

    private val button1: View = itemView.findViewById(android.R.id.button1)
    private val button2: View = itemView.findViewById(android.R.id.button2)
    private val switchView: CheckBox = itemView.findViewById(R.id.switch_widget)
    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val text: TextView = itemView.findViewById(android.R.id.text1)

    init {
        button1.setOnClickListener(this)
        button2.setOnClickListener { view: View? -> onWidgetClick(view) }
    }

    override fun onClick(view: View) {
        listener!!.onViewTemplateClick(data.template)
    }

    private fun onWidgetClick(view: View?) {
        onSwitchClick()
    }

    private fun onSwitchClick() {
        listener!!.onToggleTemplateClick(data.template, !data!!.enabled)
    }

    override fun onBind() {
        syncEnabledState()

        val context = itemView.context

        title.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_accessible_folders_shared_template, data.template.getTitle(context)))
        text.text = WordsHelper.buildOmissibleFoldersListText(context, data.template.list, 4)
        icon.setImageDrawable(if (data.icon) context.getDrawable(R.drawable.ic_folder_link_24dp) else null)
    }

    override fun onBind(payloads: List<Any>) {
        syncEnabledState()
    }

    private fun syncEnabledState() {
        switchView.isChecked = data!!.enabled
    }
}