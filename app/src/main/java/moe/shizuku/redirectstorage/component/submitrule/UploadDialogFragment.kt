package moe.shizuku.redirectstorage.component.submitrule

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import com.google.gson.GsonBuilder
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.appDetailViewModel
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.ktx.longVersionCodeCompat
import rikka.core.util.ClipboardUtils
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml

class UploadDialogFragment : AlertDialogFragment() {

    companion object {

        fun newInstance(): UploadDialogFragment {
            return UploadDialogFragment()
        }

        fun show(fragmentManager: FragmentManager) {
            if (fragmentManager.isStateSaved) return
            newInstance().show(fragmentManager, UploadDialogFragment::class.java.simpleName)
        }

        private val GSON = GsonBuilder().setPrettyPrinting().create()
    }

    private val model by appDetailViewModel()

    override fun onShow(dialog: AlertDialog) {
        val context = dialog.context
        dialog.findViewById<TextView>(android.R.id.message)?.setLineSpacing(context.resources.getDimension(R.dimen.dialog_line_space_extra), 1f)
        dialog.findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun onCopyClick() {
        val context = context ?: return

        val appInfo = model.appInfo.value?.data ?: return
        val observers = model.observers.value?.data
                ?.filter { !it.online && it.enabled }
                ?.mapIndexed { index, it ->
                    val id = index.toString()
                    id to mapOf(
                            "summary" to it.summary,
                            "source" to it.source,
                            "target" to it.target,
                            "call_media_scan" to it.callMediaScan,
                            "add_to_downloads" to it.showNotification,
                            "allow_child" to it.allowChild
                    )
                }?.toMap() ?: return
        val simpleMounts = model.simpleMounts.value?.data
                ?.filter { !it.online && it.enabled }
                ?.map {
                    id to mapOf(
                            "source" to it.sourcePackage,
                            "target" to it.targetPackage,
                            "paths" to it.paths
                    )
                }?.toMap() ?: return

        val map = mutableMapOf(
                "label" to appInfo.label,
                "package" to appInfo.packageName,
                "authors" to listOf(""),
                "last_update" to mapOf(
                        "timestamp" to System.currentTimeMillis() / 1000,
                        "version_name" to appInfo.packageInfo.versionName,
                        "version_code" to appInfo.packageInfo.longVersionCodeCompat
                ),
                "recommendation" to if (appInfo.isRedirectEnabled) "@recommended" else "@unnecessary",
                "behaviors" to arrayListOf(if (appInfo.isRedirectEnabled) "@abuse" else "@standard")
        )
        if (simpleMounts.isNotEmpty()) {
            map["accessible_folders"] = mapOf("data" to simpleMounts)
        }
        if (observers.isNotEmpty()) {
            map["export_folders"] = mapOf("data" to observers)
        }
        ClipboardUtils.put(context, GSON.toJson(map))
        Toast.makeText(context, context.getString(R.string.toast_copied_to_clipboard), Toast.LENGTH_SHORT).show()
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        val context = builder.context
        builder.setTitle(R.string.rule_upload)
                .setMessage(context.getString(R.string.rule_upload_dialog_message, "storage-redirect-client://help/rule_contribute").toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(android.R.string.ok, null)
                .setNeutralButton(R.string.rule_upload_dialog_copy) { _, _ ->
                    try {
                        onCopyClick()
                    } catch (e: Throwable) {
                    }
                }
    }
}