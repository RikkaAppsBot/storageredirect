package moe.shizuku.redirectstorage.component.mountdirstemplate.dialog

import androidx.core.os.bundleOf
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog
import moe.shizuku.redirectstorage.ktx.toArrayList
import moe.shizuku.redirectstorage.model.IPackageDescriptor
import moe.shizuku.redirectstorage.model.SelectableSimpleAppInfo
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.model.SimplePackageDescriptor
import moe.shizuku.redirectstorage.utils.UserHelper

class MountDirsTemplatePackageSelectorDialog : PackagesSelectorDialog() {

    companion object {

        @JvmStatic
        fun newInstance(
                selectedPackages: Collection<SimplePackageDescriptor>? = null,
                userId: Int
        ): MountDirsTemplatePackageSelectorDialog {
            return MountDirsTemplatePackageSelectorDialog().apply {
                arguments = bundleOf(
                        AppConstants.EXTRA_USER_ID to userId,
                        EXTRA_SELECTED_PACKAGES to selectedPackages?.toArrayList()
                )
            }
        }
    }

    private val userId: Int by lazy {
        arguments?.getInt(AppConstants.EXTRA_USER_ID, UserHelper.myUserId()) ?: UserHelper.myUserId()
    }

    override val maxSelectedCount = Int.MAX_VALUE

    override fun loadAllAvailablePackages(selectedPackages: List<IPackageDescriptor>?): List<SimpleAppInfo> {
        val pm = requireContext().packageManager
        return SRManager.createThrow()
                .getRedirectPackages(
                        // select from only redirected packages
                        SRManager.GET_PACKAGE_INFO or SRManager.MATCH_ENABLED_ONLY, userId
                )
                .map {
                    if (it.packageInfo.sharedUserId != null) {
                        SharedUserLabelCache.load(pm, it.packageInfo.sharedUserLabel, it.packageInfo.sharedUserId, it.packageName, it.packageInfo.applicationInfo)
                    }
                    SimpleAppInfo(
                            it.packageName,
                            it.userId,
                            it.packageInfo.applicationInfo,
                            pm.getApplicationLabel(it.packageInfo.applicationInfo),
                            it.packageInfo.sharedUserId
                    )
                }
                .sortedWith(Comparator { o1, o2 ->
                    val b1 = selectedPackages?.contains(SimplePackageDescriptor(o1.packageName, o1.userId, o1.sharedUserId)) ?: false
                    val b2 = selectedPackages?.contains(SimplePackageDescriptor(o2.packageName, o2.userId, o2.sharedUserId)) ?: false
                    if (b1 == b2) {
                        o1.compareTo(o2)
                    } else {
                        if (b1 && !b2) {
                            -1
                        } else if (!b1 && b2) {
                            1
                        } else {
                            0
                        }
                    }
                })
    }

    override fun onCreateAdapterItem(it: SelectableSimpleAppInfo) {
        val summary = StringBuilder()
        if (it.appInfo.sharedUserId != null) {
            summary.append(requireContext().getString(R.string.shared_user_id_name, SharedUserLabelCache.get(it.appInfo.sharedUserId)))
        }
        if (summary.isNotBlank()) {
            it.summary = summary.trim().toString()
        }
    }
}
