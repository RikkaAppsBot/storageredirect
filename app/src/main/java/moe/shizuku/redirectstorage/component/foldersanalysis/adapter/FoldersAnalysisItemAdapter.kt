package moe.shizuku.redirectstorage.component.foldersanalysis.adapter

import android.graphics.Paint
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.ArcShape
import android.graphics.drawable.shapes.OvalShape
import android.os.Parcelable
import android.text.format.Formatter
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.parcel.Parcelize
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.model.FolderSizeEntry
import rikka.core.res.resolveColor
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.ClassCreatorPool
import java.math.RoundingMode
import java.text.DecimalFormat

class FoldersAnalysisItemAdapter : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    init {
        creatorPool.apply {
            putRule(Item::class.java) { inflater, parent ->
                ItemViewHolder(inflater.inflate(
                        R.layout.folders_analysis_entry_item, parent, false))
            }
        }
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool()
    }

    fun setData(list: List<FolderSizeEntry>) {
        setItems(emptyList<FolderSizeEntry>())
        val rootEntry = list.maxBy { it.size }
        if (rootEntry != null) {
            setItems(list.filter { it != rootEntry }.map {
                Item(it, it.size.toDouble() / rootEntry.size.toDouble())
            })
        }
        notifyDataSetChanged()
    }

    @Parcelize
    data class Item(val data: FolderSizeEntry, val percent: Double) : Parcelable

    class ItemViewHolder(itemView: View) : BaseViewHolder<Item>(itemView) {

        companion object {

            private val decimalFormat = DecimalFormat().apply {
                maximumFractionDigits = 2
                groupingSize = 0
                roundingMode = RoundingMode.FLOOR
            }

        }

        val icon: ImageView = itemView.findViewById(android.R.id.icon)
        val title: TextView = itemView.findViewById(android.R.id.title)
        val summary: TextView = itemView.findViewById(android.R.id.summary)

        private val arcDrawable = ShapeDrawable().apply {
            paint.apply {
                color = context.theme.resolveColor(android.R.attr.colorAccent)
                isAntiAlias = true
                style = Paint.Style.FILL_AND_STROKE
            }
        }

        private val bgDrawable = ShapeDrawable(OvalShape()).apply {
            paint.apply {
                color = context.theme.resolveColor(android.R.attr.colorControlNormal)
                isAntiAlias = true
                style = Paint.Style.FILL_AND_STROKE
            }
        }

        private val layerDrawable = LayerDrawable(arrayOf(bgDrawable, arcDrawable))

        init {
            icon.background = layerDrawable
        }

        override fun onBind() {
            arcDrawable.shape = ArcShape(-90F, 360F * data.percent.toFloat())

            title.text = data.data.file.relativePath
            summary.text = context.getString(
                    R.string.folders_analysis_entry_item_summary,
                    Formatter.formatFileSize(context, data.data.size),
                    decimalFormat.format(data.percent * 100)
            )
        }

    }

}