package moe.shizuku.redirectstorage.component.applist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.adapter.BasePopupMenuAdapter;
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.app.Settings.SortAccording;
import moe.shizuku.redirectstorage.component.applist.adapter.SortFilterPopupMenuAdapter;
import moe.shizuku.redirectstorage.license.License;
import moe.shizuku.redirectstorage.utils.UserHelper;

public class AppListActivity extends AppBarFragmentActivity {

    private BroadcastReceiver mPurchaseChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            resetActionBarSubtitle();
            invalidateOptionsMenu();
        }
    };

    private ListPopupWindow sortFilterMenu;
    private BasePopupMenuAdapter sortFilterMenuAdapter;

    public ListPopupWindow getSortFilterMenu() {
        if (sortFilterMenu == null) {
            createSortFilterMenu(this);
        }
        return sortFilterMenu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mPurchaseChangedReceiver, new IntentFilter(AppConstants.ACTION_PURCHASED_CHANGED));

        super.onCreate(savedInstanceState);

        createSortFilterMenu(this);

        if (savedInstanceState == null) {
            int userId = Settings.isShowMultiUser() ? SRManager.USER_ALL : UserHelper.myUserId();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, AppListFragment.newInstance(userId))
                    .commit();
        }

        if (getAppBar() != null) {
            getAppBar().setDisplayHomeAsUpEnabled(true);
        }
        resetActionBarSubtitle();
    }

    private void resetActionBarSubtitle() {
        if (getAppBar() != null) {
            getAppBar().setSubtitle(License.checkLocal() ? null : getString(R.string.app_subtitle_trail));
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mPurchaseChangedReceiver);

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_list, menu);
        return true;
    }

    private void prepareViewOptionsMenuItem(boolean retry) {
        View menuItemView = findViewById(R.id.action_view_options);
        if (menuItemView != null) {
            getSortFilterMenu().setAnchorView(menuItemView);
            menuItemView.setOnTouchListener(getSortFilterMenu().createDragToOpenListener(menuItemView));
        } else if (!isFinishing()) {
            getWindow().getDecorView().postDelayed(() -> prepareViewOptionsMenuItem(false), 500);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_reset_tips).setVisible(BuildConfig.DEBUG);

        prepareViewOptionsMenuItem(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_options:
                // Fix Fabric Issues #256
                try {
                    if (getSortFilterMenu().getAnchorView() == null) {
                        getSortFilterMenu().setAnchorView(findViewById(R.id.action_view_options));
                    }
                    getSortFilterMenu().show();
                } catch (Exception e) {
                    runOnUiThread(() -> {
                        try {
                            getSortFilterMenu().show();
                        } catch (Exception ignored) {

                        }
                    });
                }
                return true;
            case R.id.action_sort_by_app_name:
                item.setChecked(true);
                Settings.setSortAccording(SortAccording.ACCORDING_NAME);
                return false;
            case R.id.action_sort_by_install_time:
                item.setChecked(true);
                Settings.setSortAccording(SortAccording.ACCORDING_INSTALL_TIME);
                return false;
            case R.id.action_sort_by_update_time:
                item.setChecked(true);
                Settings.setSortAccording(SortAccording.ACCORDING_UPDATE_TIME);
                return false;
            case R.id.action_sort_enabled_first:
                item.setChecked(!item.isChecked());
                Settings.setSortEnabledFirst(item.isChecked());
                return false;
            case R.id.action_filter_show_system:
                item.setChecked(!item.isChecked());
                Settings.setFilterShowSystem(item.isChecked());
                return false;
            case R.id.action_filter_hide_verified:
                item.setChecked(!item.isChecked());
                Settings.setFilterHideVerified(item.isChecked());
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createSortFilterMenu(Context context) {
        sortFilterMenu = new ListPopupWindow(context, null, 0, R.style.Widget_ListPopupWindow_Overflow);
        if (sortFilterMenuAdapter == null) {
            sortFilterMenuAdapter = new SortFilterPopupMenuAdapter();
        }
        sortFilterMenu.setModal(true);
        sortFilterMenu.setAdapter(sortFilterMenuAdapter);
        sortFilterMenu.setContentWidth(sortFilterMenuAdapter.getContentWidth(context));
        sortFilterMenu.setOnItemClickListener((parent, view, position, id) -> {
            BasePopupMenuAdapter.Item item = sortFilterMenuAdapter.getItem(position);
            super.onMenuItemSelected(0, item.getMenuItem());
            sortFilterMenu.dismiss();
        });
    }
}
