package moe.shizuku.redirectstorage.component.restore.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;

@Keep
public class BackupModel extends AbstractBackupModel {

    public final int version = 6;

    public int serverVersion = -1;

    @SerializedName("redirect_package")
    public List<RedirectPackageInfo> redirectPackage;

    @SerializedName("observer_info")
    public List<ObserverInfo> observers;

    @SerializedName("simple_mount")
    public List<SimpleMountInfo> simpleMounts;

    @SerializedName("default_redirect_target")
    public String defaultRedirectTarget;

    @SerializedName("mount_dirs_templates")
    public List<MountDirsTemplate> mountDirectoriesTemplates;

    @SerializedName("sr_settings_xml")
    public String srSettingsXml;

    @SerializedName("package_labels")
    public Map<String, String> packageLabels;

    public BackupModel() {

    }

    private BackupModel(Parcel in) {
        serverVersion = in.readInt();
        redirectPackage = Arrays.asList(Optional.ofNullable(in.createTypedArray(RedirectPackageInfo.CREATOR)).orElse(new RedirectPackageInfo[0]));
        observers = Arrays.asList(Optional.ofNullable(in.createTypedArray(ObserverInfo.CREATOR)).orElse(new ObserverInfo[0]));
        defaultRedirectTarget = in.readString();
        mountDirectoriesTemplates = Arrays.asList(Optional.ofNullable(in.createTypedArray(MountDirsTemplate.CREATOR)).orElse(new MountDirsTemplate[0]));
        srSettingsXml = in.readString();
        packageLabels = new ArrayMap<>();
        in.readMap(packageLabels, String.class.getClassLoader());
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public List<RedirectPackageInfo> getRedirectPackage() {
        return redirectPackage;
    }

    @Override
    public List<ObserverInfo> getObservers() {
        return observers;
    }

    public List<SimpleMountInfo> getSimpleMounts() {
        return simpleMounts;
    }

    @Override
    public String getDefaultRedirectTarget() {
        return defaultRedirectTarget;
    }

    @Override
    public List<MountDirsTemplate> getMountDirsTemplates() {
        return mountDirectoriesTemplates;
    }

    @Override
    public String getSRSettingsXML() {
        return srSettingsXml;
    }

    @Override
    public Map<String, String> getPackageLabelMap() {
        return packageLabels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(serverVersion);
        dest.writeTypedArray(redirectPackage.toArray(new Parcelable[0]), flags);
        dest.writeTypedArray(observers.toArray(new Parcelable[0]), flags);
        dest.writeString(defaultRedirectTarget);
        dest.writeTypedArray(mountDirectoriesTemplates.toArray(new Parcelable[0]), flags);
        dest.writeString(srSettingsXml);
        dest.writeMap(packageLabels);
    }

    public static final Creator<BackupModel> CREATOR = new Creator<BackupModel>() {
        @Override
        public BackupModel createFromParcel(Parcel source) {
            return new BackupModel(source);
        }

        @Override
        public BackupModel[] newArray(int size) {
            return new BackupModel[size];
        }
    };

    public static class Builder implements AbstractBackupModel.Builder {

        private final BackupModel model = new BackupModel();

        public Builder() {

        }

        @Override
        public Builder setServerVersion(int serverVersion) {
            model.serverVersion = serverVersion;
            return this;
        }

        @Override
        public Builder setRedirectPackage(List<RedirectPackageInfo> redirectPackage) {
            model.redirectPackage = redirectPackage;
            return this;
        }

        @Override
        public Builder setObservers(List<ObserverInfo> observers) {
            model.observers = observers;
            return this;
        }

        @Override
        public Builder setSimpleMounts(List<SimpleMountInfo> simpleMounts) {
            model.simpleMounts = simpleMounts;
            return this;
        }

        @Override
        public Builder setDefaultRedirectTarget(String defaultRedirectTarget) {
            model.defaultRedirectTarget = defaultRedirectTarget;
            return this;
        }

        @Override
        public Builder setMountDirsTemplates(List<MountDirsTemplate> mountDirectoriesTemplates) {
            model.mountDirectoriesTemplates = mountDirectoriesTemplates;
            return this;
        }

        @Override
        public Builder setSRSettingsXML(String xml) {
            model.srSettingsXml = xml;
            return this;
        }

        @Override
        public Builder setPackageLabelMap(Map<String, String> map) {
            model.packageLabels = map;
            return this;
        }

        @Override
        public BackupModel build() {
            return model;
        }

    }

}
