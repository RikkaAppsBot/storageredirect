package moe.shizuku.redirectstorage.component.detail.dialog;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.collection.MountDirsSet;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.model.AppInfo;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import rikka.html.text.HtmlCompat;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;
import static moe.shizuku.redirectstorage.AppConstants.TAG;

public class ViewMountDirsListDialog extends AlertDialogFragment {

    public static ViewMountDirsListDialog newInstance(@Nullable Collection<String> mountDirs) {
        Bundle args = new Bundle();
        if (mountDirs != null) {
            args.putStringArrayList(EXTRA_DATA, new ArrayList<>(mountDirs));
        }

        ViewMountDirsListDialog dialog = new ViewMountDirsListDialog();
        dialog.setArguments(args);
        return dialog;
    }

    public static ViewMountDirsListDialog newInstance(@NonNull AppInfo appInfo) {
        try {
            MountDirsSet mountDirs;
            mountDirs = SRManager.createThrow().getMountDirsForPackage(appInfo.getPackageName(), appInfo.getUserId(), false);
            return newInstance(mountDirs);
        } catch (Throwable e) {
            Log.w(TAG, e.getMessage(), e);
            return newInstance(Collections.emptyList());
        }
    }

    private List<String> mMountDirs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getArguments(), "Arguments cannot be null.");
        mMountDirs = getArguments().getStringArrayList(EXTRA_DATA);
        if (mMountDirs == null) {
            Toast.makeText(requireActivity(), R.string.toast_unable_connect_server, Toast.LENGTH_LONG).show();
            dismiss();
        }
        Collections.sort(mMountDirs);
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                   @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.view_list_button);
        builder.setMessage(getMessage());
        builder.setPositiveButton(android.R.string.ok, null);
    }

    @Override
    public void onShow(AlertDialog dialog) {
        Resources r = requireContext().getResources();
        float add = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, r.getDisplayMetrics());
        ((TextView) dialog.findViewById(android.R.id.message)).setLineSpacing(add, 1);
    }

    private CharSequence getMessage() {
        if (mMountDirs.isEmpty()) {
            return getString(R.string.detail_mount_dirs_view_list_message_empty);
        } else {
            final StringBuilder sb = new StringBuilder();
            for (String path : mMountDirs) {
                sb.append(getString(R.string.detail_mount_dirs_view_list_item_format, path));
            }
            return HtmlCompat.fromHtml(sb.toString(),
                    HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM | HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE,
                    null, SizeTagHandler.getInstance());
        }
    }

}
