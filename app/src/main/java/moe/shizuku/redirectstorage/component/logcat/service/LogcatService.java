package moe.shizuku.redirectstorage.component.logcat.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ILogcatObserver;
import moe.shizuku.redirectstorage.ILogcatService;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.BaseService;
import moe.shizuku.redirectstorage.component.logcat.LogcatActivity;
import moe.shizuku.redirectstorage.component.logcat.LogcatParser;
import moe.shizuku.redirectstorage.model.LogcatItem;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.LogcatCommandBuilder;
import moe.shizuku.redirectstorage.utils.NotificationHelper;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;
import moe.shizuku.redirectstorage.utils.SRInfoHelper;
import moe.shizuku.redirectstorage.utils.SuShell;

import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_LOGCAT;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_ID_LOGCAT;
import static moe.shizuku.redirectstorage.app.ServiceKtxKt.startForegroundOrNotify;

public class LogcatService extends BaseService {

    public static final String TAG = LogcatService.class.getSimpleName();

    public static final String ACTION_START = BuildConfig.APPLICATION_ID + ".action.START_LOGCAT";
    public static final String ACTION_STOP = BuildConfig.APPLICATION_ID + ".action.STOP_LOGCAT";

    public static final String ACTION_UPDATE_STATUS = BuildConfig.APPLICATION_ID + ".action.LOGCAT_UPDATE_STATUS";

    private boolean isRunning = false;

    @Nullable
    private File mCurrentFile = null;
    @Nullable
    private PrintWriter mPrintWriter = null;

    private boolean mMayEncrypted;

    private volatile Looper mIOLooper;
    private volatile Handler mIOHandler;
    private volatile Handler mMainHandler;

    private final RemoteCallbackList<ILogcatObserver> mLogObservers = new RemoteCallbackList<>();

    private final ILogcatServiceImpl mBinderImpl = new ILogcatServiceImpl();

    private long mStartTime = 0;
    private final LinkedList<LogcatItem> mLogCache = new LinkedList<>();
    private int mClientIndex = 0;
    private StringBuilder mWriteBuffer = new StringBuilder();

    private SuShell shell;

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("LogcatIOHandlerThread");
        thread.start();
        mIOLooper = thread.getLooper();
        mIOHandler = new IOHandler(mIOLooper);
        mMainHandler = new MainHandler(getMainLooper());
        shell = new SuShell(2);
    }

    private void startForeground() {
        NotificationManager notificationManager = getSystemService(NotificationManager.class);

        if (notificationManager != null
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_LOGCAT, getString(R.string.notification_channel_logcat_service), NotificationManager.IMPORTANCE_LOW);
            channel.setShowBadge(false);
            if (BuildUtils.atLeast29()) {
                channel.setAllowBubbles(false);
            }

            notificationManager.createNotificationChannel(channel);
        }

        startForegroundOrNotify(this, NOTIFICATION_ID_LOGCAT, createNotificationBuilder().build());
    }

    private NotificationCompat.Builder createNotificationBuilder() {
        NotificationCompat.Builder builder = NotificationHelper.create(
                this, NOTIFICATION_CHANNEL_LOGCAT,
                R.string.notification_logcat_service_running_title);
        builder.setSmallIcon(R.drawable.ic_noti_logcat_24);
        builder.setOngoing(true);
        builder.setSubText(getString(R.string.logcat_title));
        builder.addAction(0, getString(R.string.action_stop),
                PendingIntent.getService(this, 0, new Intent(this, LogcatService.class).setAction(ACTION_STOP), PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, LogcatActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            builder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        return builder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || intent.getAction() == null) {
            return START_NOT_STICKY;
        }
        switch (intent.getAction()) {
            case ACTION_START:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mMayEncrypted = Intent.ACTION_LOCKED_BOOT_COMPLETED
                            .equals(intent.getStringExtra(AppConstants.EXTRA_SOURCE_ACTION));
                }
                if (isRunning) {
                    Log.d(TAG, "Logcat has already been running.");
                } else {
                    startWatching();
                }
                return START_STICKY;
            case ACTION_STOP:
                stopWatching();
                return START_NOT_STICKY;
        }
        return START_NOT_STICKY;
    }

    private void startWatching() {
        mLogCache.clear();

        isRunning = true;

        mStartTime = System.currentTimeMillis();
        mIOHandler.sendEmptyMessage(IOHandler.MSG_WHAT_START_OUTPUT);

        shell.run(
                new LogcatCommandBuilder()
                        .setFormat("threadtime")
                        .build(),
                new SuShell.Callback() {
                    @Override
                    public void onLine(String line) {
                        if (mCurrentFile == null || mPrintWriter == null) {
                            mIOHandler.sendEmptyMessage(IOHandler.MSG_WHAT_START_OUTPUT);
                        }
                        if (mPrintWriter != null) {
                            mWriteBuffer.append(line).append("\n");
                            if (!mIOHandler.hasMessages(IOHandler.MSG_WHAT_FLUSH_OUTPUT)) {
                                mIOHandler.sendEmptyMessageDelayed(IOHandler.MSG_WHAT_FLUSH_OUTPUT, IOHandler.WRITE_DELAY);
                            }
                        }

                        LogcatItem logcatItem = LogcatParser.parse(line);
                        if (logcatItem != null
                                && !TextUtils.isEmpty(logcatItem.getDate())) {
                            onNewLog(logcatItem);
                        }
                    }

                    @Override
                    public void onResult(int exitCode) {
                        stopWatching();
                    }
                });

        startForeground();
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_UPDATE_STATUS).putExtra("status", isRunning));
    }

    @SuppressWarnings("UnusedReturnValue")
    private synchronized boolean openFileIfPossible() {
        if (mCurrentFile != null && mPrintWriter != null) {
            return true;
        }

        File directory;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && mMayEncrypted) {
            directory = createDeviceProtectedStorageContext().getFilesDir();
        } else {
            directory = getExternalFilesDir(null);
            if (directory == null) {
                directory = getFilesDir();
            }
        }
        directory = new File(directory, "log");

        try {
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                    Log.e(AppConstants.TAG, "LogcatService: unable mkdirs " + directory);
                    return false;
                }
            }

            mCurrentFile = new File(directory,
                    DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.ENGLISH)
                            .format(new Date(System.currentTimeMillis())) + ".log");
            if (!mCurrentFile.createNewFile()) {
                Log.e(AppConstants.TAG, "LogcatService: unable create " + mCurrentFile);
                return false;
            }

            FileOutputStream outputStream = new FileOutputStream(mCurrentFile);
            mPrintWriter = new PrintWriter(outputStream);
            Log.i(AppConstants.TAG, "LogcatService: writing logs to " + mCurrentFile);
            String infoText = new SRInfoHelper(this).getInfo().toString();
            mPrintWriter.write(infoText);
            mPrintWriter.write("\n\n");
            mPrintWriter.flush();
        } catch (Exception ignored) {
        }

        return mCurrentFile != null && mPrintWriter != null;
    }

    private void onNewLog(LogcatItem item) {
        synchronized (mLogCache) {
            mLogCache.add(item);
        }

        if (!mIOHandler.hasMessages(IOHandler.MSG_WHAT_SEND_CLIENT)) {
            mIOHandler.sendEmptyMessageDelayed(IOHandler.MSG_WHAT_SEND_CLIENT, IOHandler.SEND_DELAY);
        }
    }

    private void stopWatching() {
        isRunning = false;
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(ACTION_UPDATE_STATUS).putExtra("status", isRunning));

        mIOHandler.sendEmptyMessage(IOHandler.MSG_WHAT_CLOSE_OUTPUT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;

        shell.close();

        mIOLooper.quit();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinderImpl;
    }

    public void registerObserver(@NonNull ILogcatObserver observer) {
        mLogObservers.register(observer);

        if (!mIOHandler.hasMessages(IOHandler.MSG_WHAT_SEND_CLIENT)) {
            mIOHandler.sendEmptyMessageDelayed(IOHandler.MSG_WHAT_SEND_CLIENT, 300);
        }
    }

    public void unregisterObserver(@NonNull ILogcatObserver observer) {
        mLogObservers.unregister(observer);
    }

    public boolean isRunning() {
        return isRunning;
    }

    private class MainHandler extends Handler {

        static final int MSG_WHAT_STOP_SERVICE = 0;

        MainHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_WHAT_STOP_SERVICE:
                    stopForeground(true);
                    stopSelf();
                    break;
            }
        }
    }

    private class IOHandler extends Handler {

        static final int MSG_WHAT_CLOSE_OUTPUT = 0;
        static final int MSG_WHAT_FLUSH_OUTPUT = 1;
        static final int MSG_WHAT_START_OUTPUT = 2;
        static final int MSG_WHAT_SEND_CLIENT = 3;

        static final int WRITE_DELAY = 3000;
        static final int SEND_DELAY = 300;
        static final int SEND_MAX_COUNT = 10000;

        IOHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_WHAT_START_OUTPUT:
                    openFileIfPossible();
                    break;
                case MSG_WHAT_CLOSE_OUTPUT:
                    if (mPrintWriter != null) {
                        try {
                            mPrintWriter.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mPrintWriter = null;
                    }

                    if (mCurrentFile != null) {
                        try {
                            if (mCurrentFile.length() <= 0) {
                                //noinspection ResultOfMethodCallIgnored
                                mCurrentFile.delete();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mCurrentFile = null;
                    }
                    shell.close();
                    mMainHandler.sendEmptyMessage(MainHandler.MSG_WHAT_STOP_SERVICE);
                    break;
                case MSG_WHAT_FLUSH_OUTPUT:
                    if (mPrintWriter != null) {
                        mPrintWriter.append(mWriteBuffer.toString());
                        mPrintWriter.flush();
                        mWriteBuffer = new StringBuilder();
                    }
                    break;
                case MSG_WHAT_SEND_CLIENT: {
                    synchronized (mLogCache) {
                        if (mLogCache.isEmpty()) {
                            return;
                        }

                        boolean reschedule = false;

                        int currentCount = 0;
                        List<LogcatItem> list = new ArrayList<>(SEND_MAX_COUNT);
                        ListIterator<LogcatItem> iterator = mLogCache.listIterator(mClientIndex);
                        while (iterator.hasNext()) {
                            list.add(iterator.next());
                            currentCount++;
                            if (currentCount == SEND_MAX_COUNT) {
                                reschedule = true;
                                break;
                            }
                        }
                        mClientIndex += currentCount;

                        ParceledListSlice<LogcatItem> parceledListSlice = new ParceledListSlice<>(list);

                        final int count = mLogObservers.beginBroadcast();
                        for (int index = 0; index < count; index++) {
                            try {
                                mLogObservers.getBroadcastItem(index).onNewLines(parceledListSlice);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                        mLogObservers.finishBroadcast();

                        if (reschedule) {
                            mIOHandler.sendEmptyMessageDelayed(MSG_WHAT_SEND_CLIENT, SEND_DELAY);
                        }
                    }
                }
            }
        }
    }

    class ILogcatServiceImpl extends ILogcatService.Stub {

        @Override
        public boolean isRunning() {
            return LogcatService.this.isRunning();
        }

        @Override
        public void setIndex(int index) {
            mClientIndex = index;
        }

        @Override
        public void registerObserver(ILogcatObserver observer) {
            LogcatService.this.registerObserver(observer);
        }

        @Override
        public void unregisterObserver(ILogcatObserver observer) {
            LogcatService.this.unregisterObserver(observer);
        }
    }
}
