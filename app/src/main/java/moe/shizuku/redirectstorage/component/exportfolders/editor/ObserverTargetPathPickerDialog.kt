package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AlertDialog
import io.reactivex.Flowable
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.FilePickerDialog
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.FileBrowserUtils
import moe.shizuku.redirectstorage.utils.FileUtils
import rikka.html.text.HtmlCompat
import java.util.*

class ObserverTargetPathPickerDialog : FilePickerDialog() {

    companion object {
        fun newInstance(excludeDirs: List<String>, userId: Int, currentPath: String?): ObserverTargetPathPickerDialog {
            val args = Bundle()
            args.putStringArrayList(EXTRA_EXCLUDE_DIRS, ArrayList(excludeDirs))
            args.putInt(EXTRA_USER_ID, userId)
            args.putString(EXTRA_CURRENT_PATH, currentPath)
            val dialog = ObserverTargetPathPickerDialog()
            dialog.arguments = args
            return dialog
        }

        private const val EXTRA_EXCLUDE_DIRS = "exclude_dirs"
        private const val EXTRA_USER_ID = "user_id"
    }

    private var excludeDirs: List<String>? = null
    private var userId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        excludeDirs = arguments!!.getStringArrayList(EXTRA_EXCLUDE_DIRS)
        userId = arguments!!.getInt(EXTRA_USER_ID)
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        super.onBuildAlertDialog(builder, savedInstanceState)
        builder.setTitle(R.string.observer_info_edit_dialog_target_path)
        builder.setNeutralButton(R.string.create_folder_dialog_title, null)
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        super.onAlertDialogCreated(dialog, contentView, savedInstanceState)
        setInfoText(getString(R.string.dialog_observer_target_picker_message))
    }

    override fun onShow(dialog: AlertDialog) {
        super.onShow(dialog)
        neutralButton.setOnClickListener { v: View? -> startCreateFolderDialog() }
    }

    override fun enqueueRefresh(): List<ServerFile?> {
        return Flowable.just(currentPath)
                .flatMap { currentPath: String? ->
                    val parent = ServerFile.fromStorageRoot(currentPath!!, userId)
                    val dirsGetter = FileBrowserUtils
                            .getRedirectStorageDirs(parent, FileBrowserUtils.SORT_BY_NAME)
                    val filesGetter = FileBrowserUtils
                            .getRedirectStorageFiles(parent, FileBrowserUtils.SORT_BY_NAME)
                    Flowable.concat(dirsGetter, filesGetter)
                }
                .filter { serverFile: ServerFile -> !FileUtils.isChildOf("Android", serverFile.relativePath) }
                .filter { item: ServerFile -> item.name == null || !item.name!!.startsWith(".") }
                .onErrorResumeNext { e: Throwable? -> Flowable.empty() }
                .blockingIterable()
                .toList()
    }

    override fun onCreateFolder(name: String?) {
        moveToNext(name!!)
        performRefresh()
    }

    override fun onPick(currentPath: String) {
        super.onPick(currentPath)
        setSummary(HtmlCompat.fromHtml(requireContext().getString(R.string.dialog_choose_target_current_path) + currentPath))
    }

    override fun updatePositiveButtonState() {
        var enabled = currentPath.length >= 1 && currentPath != "/"
        // Filter out exclude dirs
        for (excludeDir in excludeDirs!!) {
            if (currentPath.startsWith(excludeDir) || !enabled) {
                enabled = false
                break
            }
        }
        // Filter out root path
        val pathSegments = currentPath.split("/").dropLastWhile { it.isEmpty() }.toTypedArray()
        enabled = enabled and (pathSegments.size > 2)
        // Filter out /sdcard/Android
        for (pathSegment in pathSegments) {
            if (TextUtils.isEmpty(pathSegment)) {
                continue
            }
            enabled = enabled and ("Android" != pathSegment)
            break
        }
        setPositiveButtonEnabled(enabled)
    }

    override fun onPickFinished() {
        var currentPath = currentPath
        if (currentPath.startsWith("/")) currentPath = currentPath.replaceFirst("/".toRegex(), "")
        if (parentFragment is ExportFolderEditFragment) {
            (parentFragment as ExportFolderEditFragment?)!!.onTargetPathPick(currentPath)
        }
    }
}