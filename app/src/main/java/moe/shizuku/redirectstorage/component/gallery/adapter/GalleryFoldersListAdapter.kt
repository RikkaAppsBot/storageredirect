package moe.shizuku.redirectstorage.component.gallery.adapter

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.component.gallery.viewholder.FolderItemViewHolder
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.ClassCreatorPool

class GalleryFoldersListAdapter : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    val gridViewPool: RecyclerView.RecycledViewPool = RecyclerView.RecycledViewPool()

    companion object {
        private val TIP = Any()
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool().apply {
            putRule(GalleryFolderSimpleItem::class.java, FolderItemViewHolder.CREATOR)
            putRule(Any::class.java, TipViewHolder.CREATOR)
        }
    }

    fun updateDate(list: Collection<GalleryFolderSimpleItem>) {
        getItems<Any>().clear()
        getItems<Any>().addAll(list)
        getItems<Any>().add(TIP)
        notifyDataSetChanged()
    }

    private class TipViewHolder(itemView: View) : BaseViewHolder<Any>(itemView) {

        companion object {
            val CREATOR = { inflater: LayoutInflater, parent: ViewGroup -> TipViewHolder(inflater.inflate(R.layout.item_list_tip, parent, false)) }
        }

        init {
            val text = itemView.findViewById<TextView>(android.R.id.text1)
            text.text = HtmlCompat.fromHtml(text.context.getString(R.string.media_store_tip), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
            text.movementMethod = LinkMovementMethod.getInstance()
        }
    }
}