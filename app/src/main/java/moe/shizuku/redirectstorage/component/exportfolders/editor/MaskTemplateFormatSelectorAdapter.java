package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.event.EventIntents;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.ClassCreatorPool;

public class MaskTemplateFormatSelectorAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    private List<String> mData;

    public MaskTemplateFormatSelectorAdapter() {
        super();

        updateItems();
    }

    public void updateItems() {
        getItems().clear();

        if (mData != null) {
            getItems().addAll(mData);
        }
        getItems().add(AddItemViewHolder.Item.INSTANCE);

        notifyDataSetChanged();
    }

    public void setData(@Nullable List<String> data) {
        mData = data;

        updateItems();
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool()
                .putRule(AddItemViewHolder.Item.class, AddItemViewHolder.CREATOR)
                .putRule(String.class, FormatItemViewHolder.CREATOR);
    }

    public static class AddItemViewHolder extends BaseViewHolder<AddItemViewHolder.Item> {

        public static final Creator<Item> CREATOR = (inflater, parent) ->
                new AddItemViewHolder(inflater.inflate(R.layout.mask_template_format_add_item, parent, false));

        private AddItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this::onClick);
        }

        public void onClick(View view) {
            LocalBroadcastManager.getInstance(view.getContext())
                    .sendBroadcastSync(EventIntents.CommonList.notifyItemClick(getClass()));
        }

        static final class Item {

            public static final Item INSTANCE = new Item();

            private Item() {
            }

        }

    }

    public static class FormatItemViewHolder extends BaseViewHolder<String> {

        public static final Creator<String> CREATOR = (inflater, parent) ->
                new FormatItemViewHolder(inflater.inflate(R.layout.mask_template_format_item, parent, false));

        private final TextView text;

        private FormatItemViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(android.R.id.title);
            itemView.findViewById(android.R.id.button1).setOnClickListener(v ->
                    LocalBroadcastManager.getInstance(v.getContext())
                            .sendBroadcast(EventIntents.CommonList
                                    .notifyCurrentStringItemClick(this))
            );
        }

        @Override
        public void onBind() {
            text.setText(text.getResources().getString(
                    R.string.observer_mask_format_suffix_item_format,
                    "." + getData().replace("\\.", ".")
            ));
        }

    }

}
