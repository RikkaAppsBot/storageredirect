package moe.shizuku.redirectstorage.component.submitrule.dialog

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import io.reactivex.Flowable
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.FilePickerDialog
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.FileBrowserUtils
import rikka.html.text.HtmlCompat

class RulesLinkPickerDialog : FilePickerDialog() {

    companion object {
        fun newInstance(packageName: String, userId: Int): RulesLinkPickerDialog {
            val args = Bundle()
            args.putString(AppConstants.EXTRA_PACKAGE_NAME, packageName)
            args.putInt(AppConstants.EXTRA_USER_ID, userId)
            val dialog = RulesLinkPickerDialog()
            dialog.arguments = args
            return dialog
        }

        const val ACTION_RULES_LINK_PICKED = BuildConfig.APPLICATION_ID +
                ".action.RULES_LINK_PICKED"
    }

    private var mPackageName: String? = null
    private var mUserId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPackageName = arguments!!.getString(AppConstants.EXTRA_PACKAGE_NAME)
        mUserId = arguments!!.getInt(AppConstants.EXTRA_USER_ID)
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        super.onBuildAlertDialog(builder, savedInstanceState)
        builder.setTitle(R.string.dialog_create_new_link_choose_source_title)
    }

    override fun enqueueRefresh(): List<ServerFile> {
        return Flowable.just(currentPath)
                .flatMap<ServerFile> { currentPath: String? ->
                    val file = ServerFile.fromRedirectTarget(currentPath!!, mPackageName!!, mUserId)
                    val dirsGetter = FileBrowserUtils.getRedirectStorageDirs(file, FileBrowserUtils.SORT_BY_NAME)
                    val filesGetter = FileBrowserUtils.getRedirectStorageFiles(file, FileBrowserUtils.SORT_BY_NAME)
                    Flowable.concat(dirsGetter, filesGetter)
                }
                .blockingIterable()
                .toList()
    }

    override fun onPick(currentPath: String) {
        super.onPick(currentPath)
        setSummary(HtmlCompat.fromHtml(requireContext().getString(R.string.dialog_create_new_link_choose_source_current_path) + currentPath))
    }

    override fun onPickFinished() {
        localBroadcastManager
                .sendBroadcastSync(Intent(ACTION_RULES_LINK_PICKED)
                        .putExtra(AppConstants.EXTRA_DATA, currentPath))
    }
}