package moe.shizuku.redirectstorage.component.sharehelper.adapter;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.utils.ShareHelperListPreferences;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder;
import rikka.material.app.LocaleDelegate;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

import static moe.shizuku.redirectstorage.AppConstants.TAG;

public class ShareHelperListPreferencesAdapter extends IdBasedRecyclerViewAdapter {

    public static final Object PAYLOAD_CHECKBOX_STATE = new Object();

    private static final int ID_DESCRIPTION = 0;
    private static final int ID_DIVIDER = 1;
    private static final int ID_APP_INFO_BASE_INDEX = 10;

    private final Context mContext;

    private final ShareHelperListPreferences mPreferences;

    private final ItemTouchHelper mItemTouchHelper;

    private List<ShareHelperListPreferences.Item> mRawData = null;

    private final BaseViewHolder.Creator<ShareHelperListPreferences.Item> VIEW_HOLDER_CREATOR =
            (inflater, parent) -> new ShareHelperListPreferenceItemViewHolder(
                    inflater.inflate(
                            R.layout.share_helper_list_preferences_item,
                            parent, false
                    )
            );

    public ShareHelperListPreferencesAdapter(@NonNull Context context,
                                             @NonNull ShareHelperListPreferences preferences,
                                             @NonNull RecyclerView recyclerView) {
        super();

        this.mContext = context;
        this.mPreferences = preferences;

        setHasStableIds(false);

        ItemTouchHelper.Callback itemTouchHelperCallback = new AdapterItemTouchHelperCallback();
        mItemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

        updateData(null);
    }

    private void addDescription(@NonNull CharSequence description) {
        addItem(SettingsDescriptionViewHolder.CREATOR, description, ID_DESCRIPTION);
    }

    private void addDivider() {
        addItem(DividerViewHolder.CREATOR, null, ID_DIVIDER);
    }

    private void addAppItem(@NonNull ShareHelperListPreferences.Item item, int index) {
        addItem(VIEW_HOLDER_CREATOR, item, ID_APP_INFO_BASE_INDEX + index);
    }

    public void updateData(@Nullable List<ShareHelperListPreferences.Item> newData) {
        updateData(newData, true);
    }

    public void updateData(@Nullable List<ShareHelperListPreferences.Item> newData,
                           boolean notify) {
        clear();

        addDescription(mContext.getString(R.string.settings_direct_share_preferences_description));
        addDivider();
        mRawData = newData;
        if (newData != null) {
            for (int i = 0; i < newData.size(); i++) {
                addAppItem(newData.get(i), i);
            }
        }

        if (notify) {
            notifyDataSetChanged();
        }
    }

    private int getEnabledCount() {
        int enabledCount = 0;
        for (Object o : getItems()) {
            if (o instanceof ShareHelperListPreferences.Item) {
                final ShareHelperListPreferences.Item item = (ShareHelperListPreferences.Item) o;
                if (item.showInDirectShare) {
                    enabledCount++;
                }
            }
        }
        return enabledCount;
    }

    private void onDataUpdate(@NonNull ShareHelperListPreferences.Item item) {
        mPreferences
                .editPreferences(items -> {
                    if (items.contains(item)) {
                        items.set(items.indexOf(item), item);
                    } else {
                        items.add(item);
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        notifyItemRangeChanged(0, getItemCount(), PAYLOAD_CHECKBOX_STATE);
    }

    class AdapterItemTouchHelperCallback extends ItemTouchHelper.Callback {

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (viewHolder instanceof ShareHelperListPreferenceItemViewHolder) {
                return makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
            }
            return 0;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView,
                              RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            viewHolder.itemView.performHapticFeedback(HapticFeedbackConstants.CLOCK_TICK);
            Collections.swap(getItems(),
                    viewHolder.getAdapterPosition(),
                    target.getAdapterPosition());
            Collections.swap(mRawData,
                    viewHolder.getAdapterPosition() - 2,
                    target.getAdapterPosition() - 2);
            for (int i = 0; i < mRawData.size(); i++) {
                mRawData.get(i).rank = mRawData.size() - i;
            }
            mPreferences.setPreferences(mRawData);
            notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean canDropOver(RecyclerView recyclerView,
                                   RecyclerView.ViewHolder current,
                                   RecyclerView.ViewHolder target) {
            return target instanceof ShareHelperListPreferenceItemViewHolder;
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return false;
        }

        @Override
        public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
            if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                if (viewHolder instanceof ShareHelperListPreferenceItemViewHolder) {
                    ((ShareHelperListPreferenceItemViewHolder) viewHolder).raiseUp();
                }
            }
        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (viewHolder instanceof ShareHelperListPreferenceItemViewHolder) {
                ((ShareHelperListPreferenceItemViewHolder) viewHolder).liftDown();
            }
        }
    }

    class ShareHelperListPreferenceItemViewHolder
            extends BaseViewHolder<ShareHelperListPreferences.Item> {

        private final View mDragHandle;
        private final ImageView mIcon;
        private final TextView mTitle;
        private final CheckBox mCheckBox;

        private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

        @Nullable
        private Animator mDragAnimator = null;

        @SuppressLint("ClickableViewAccessibility")
        public ShareHelperListPreferenceItemViewHolder(View itemView) {
            super(itemView);

            mDragHandle = itemView.findViewById(R.id.drag_handle);
            mIcon = itemView.findViewById(android.R.id.icon);
            mTitle = itemView.findViewById(android.R.id.title);
            mCheckBox = itemView.findViewById(android.R.id.checkbox);

            itemView.setOnClickListener(v -> {
                getData().showInDirectShare = !getData().showInDirectShare;
                onDataUpdate(getData());
            });

            mDragHandle.setOnTouchListener((view, event) -> {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    mItemTouchHelper.startDrag(this);
                    return true;
                }
                return false;
            });
        }

        void raiseUp() {
            if (mDragAnimator != null) {
                mDragAnimator.cancel();
            }
            mDragAnimator = ObjectAnimator.ofFloat(
                    itemView, "elevation",
                    0, itemView.getResources().getDimension(R.dimen.padding_8dp))
                    .setDuration(itemView.getResources()
                            .getInteger(android.R.integer.config_shortAnimTime) / 2);
            mDragAnimator.start();
        }

        void liftDown() {
            if (mDragAnimator != null) {
                mDragAnimator.cancel();
            }
            mDragAnimator = ObjectAnimator.ofFloat(
                    itemView, "elevation",
                    itemView.getResources().getDimension(R.dimen.padding_8dp), 0)
                    .setDuration(itemView.getResources()
                            .getInteger(android.R.integer.config_shortAnimTime) / 2);
            mDragAnimator.start();
        }

        @Override
        public void onBind() {
            final Context context = itemView.getContext();
            final PackageManager packageManager = context.getPackageManager();

            syncCheckBoxState();

            mCompositeDisposable.addAll(
                    Single.fromCallable(() -> {
                        final ActivityInfo info = getData().loadActivityInfo(packageManager);

                        CharSequence resolvedActivityTitle = null;
                        CharSequence resolvedApplicationTitle = null;
                        try {
                            final Configuration config = new Configuration();
                            config.setLocale(LocaleDelegate.getDefaultLocale());
                            final Resources appRes = packageManager
                                    .getResourcesForApplication(getData().packageName);
                            appRes.updateConfiguration(config, appRes.getDisplayMetrics());

                            final int activityLabelRes = info.labelRes;
                            if (activityLabelRes != 0) {
                                resolvedActivityTitle = appRes.getString(activityLabelRes);
                            }
                            final int applicationLabelRes = info.applicationInfo.labelRes;
                            if (applicationLabelRes != 0) {
                                resolvedApplicationTitle = appRes.getString(applicationLabelRes);
                            }
                        } catch (Exception ignored) {

                        }
                        if (resolvedActivityTitle == null) {
                            resolvedActivityTitle = info.loadLabel(packageManager);
                        }
                        if (resolvedApplicationTitle == null) {
                            resolvedApplicationTitle = info.applicationInfo
                                    .loadLabel(packageManager);
                        }

                        if (!resolvedApplicationTitle.equals(resolvedActivityTitle)) {
                            resolvedActivityTitle = context.getString(R.string.item_summary, resolvedActivityTitle, resolvedApplicationTitle);
                        }

                        return resolvedActivityTitle;
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(mTitle::setText, err -> mTitle.setText(getData().activityName)),
                    Single.fromCallable(() ->
                            getData().loadActivityInfo(packageManager).loadIcon(packageManager))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(mIcon::setImageDrawable, err -> mIcon.setImageResource(R.mipmap.ic_default_app_icon))
            );
        }

        @Override
        public void onBind(@NonNull List<Object> payloads) {
            for (Object payload : payloads) {
                if (payload == PAYLOAD_CHECKBOX_STATE) {
                    syncCheckBoxState();
                } else {
                    Log.d(TAG, "ShareHelperListPreferenceItemViewHolder: Unsupported payload " + payload);
                }
            }
        }

        private void syncCheckBoxState() {
            final boolean shouldEnabled = getData().showInDirectShare || getEnabledCount() < 4;
            mCheckBox.setChecked(getData().showInDirectShare);
            mCheckBox.setEnabled(shouldEnabled);
            itemView.setEnabled(shouldEnabled);
        }

        @Override
        public void onRecycle() {
            mCompositeDisposable.clear();
        }

    }

}