package moe.shizuku.redirectstorage.component.home.adapter

import android.content.Context
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.api.LatestVersionInfoProvider
import moe.shizuku.redirectstorage.component.home.model.HomeStatus
import moe.shizuku.redirectstorage.component.home.viewholder.*
import moe.shizuku.redirectstorage.ktx.whenPaid
import moe.shizuku.redirectstorage.license.License
import moe.shizuku.redirectstorage.viewmodel.Resource
import rikka.recyclerview.IdBasedRecyclerViewAdapter

class HomeAdapter : IdBasedRecyclerViewAdapter() {

    companion object {
        private const val ID_STATUS: Long = 1
        private const val ID_APP_LIST: Long = 2
        private const val ID_WORKING_MODE: Long = 3
    }

    init {
        setHasStableIds(true)
    }

    fun updateData(context: Context, homeStatusResource: Resource<HomeStatus>) {
        val homeStatus = homeStatusResource.data
        clear()
        val info = LatestVersionInfoProvider.get(context)
        if (info.code > 0 && info.showUpdate()) {
            addItem(HomeUpdateViewHolder.CREATOR, info, ID_STATUS)
        }
        addItem(HomeStatusViewHolder.CREATOR, homeStatus, ID_STATUS)
        addItem(HomeWorkingModeViewHolder.CREATOR, homeStatus, ID_WORKING_MODE)
        addItem(HomeAppListViewHolder.CREATOR, homeStatus, ID_APP_LIST)
        if (BuildConfig.DEBUG || !License.checkLocal()) {
            addItem(
                    if (BuildConfig.DEBUG) HomeItemViewHolder.CREATOR_UNLOCK else HomeItemViewHolder.CREATOR_SECONDARY,
                    homeStatus,
                    HomeItemViewHolder.ID_UNLOCK
                            .toLong())
        }
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_FILE_MONITOR.toLong())
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_SETTINGS.toLong())
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_BACKUP.toLong())
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_REPORT.toLong())
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_LOGCAT.toLong())
        addItem(HomeItemViewHolder.CREATOR_SECONDARY, homeStatus, HomeItemViewHolder.ID_HELP.toLong())
        whenPaid {
            addItem(HomeItemViewHolder.CREATOR_SECONDARY,
                    homeStatus,
                    HomeItemViewHolder.ID_MY_PURCHASE.toLong())
        }
        notifyDataSetChanged()
    }
}