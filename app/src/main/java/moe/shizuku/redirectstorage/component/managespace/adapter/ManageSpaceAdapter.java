package moe.shizuku.redirectstorage.component.managespace.adapter;

import moe.shizuku.redirectstorage.component.managespace.viewholder.ClickableItemViewHolder;
import moe.shizuku.redirectstorage.component.managespace.viewholder.DataDescriptionViewHolder;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

public class ManageSpaceAdapter extends IdBasedRecyclerViewAdapter {

    public static final int ID_CLIENT_DATA_INFO = 1;
    public static final int ID_CLIENT_RESET_DATA = 2;
    public static final int ID_SERVER_DATA_INFO = 3;
    public static final int ID_SERVER_RESET_DATA_ONLY = 4;
    public static final int ID_SERVER_CLEAR_ALL = 5;
    public static final int ID_DIVIDER = 6;

    public ManageSpaceAdapter() {
        super();

        setHasStableIds(true);

        updateItems();
    }

    public void updateItems() {
        clear();

        addItem(DataDescriptionViewHolder.CREATOR, ID_CLIENT_DATA_INFO, ID_CLIENT_DATA_INFO);
        addItem(ClickableItemViewHolder.CREATOR, ID_CLIENT_RESET_DATA, ID_CLIENT_RESET_DATA);
        addItem(DividerViewHolder.CREATOR, null, ID_DIVIDER);
        addItem(DataDescriptionViewHolder.CREATOR, ID_SERVER_DATA_INFO, ID_SERVER_DATA_INFO);
        addItem(ClickableItemViewHolder.CREATOR, ID_SERVER_RESET_DATA_ONLY, ID_SERVER_RESET_DATA_ONLY);
        addItem(ClickableItemViewHolder.CREATOR, ID_SERVER_CLEAR_ALL, ID_SERVER_CLEAR_ALL);

        notifyDataSetChanged();
    }

}
