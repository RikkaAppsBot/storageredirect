package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.component.settings.ModuleSettingsFragment;
import moe.shizuku.redirectstorage.component.settings.SettingsActivity;
import moe.shizuku.redirectstorage.component.settings.WorkModeSettingsFragment;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.model.AppInfo;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.utils.ModuleUtils;
import moe.shizuku.redirectstorage.viewholder.SimpleTwoButtonViewHolder;
import rikka.core.util.ContextUtils;
import rikka.html.text.HtmlCompat;

public class AppDetailEnhancedModeFixInteractionViewHolder extends SimpleTwoButtonViewHolder<AppInfo> {

    public static final Creator<AppInfo> CREATOR = (inflater, parent) ->
            new AppDetailEnhancedModeFixInteractionViewHolder(inflater.inflate(R.layout.detail_enhanced_mode_fix_interaction, parent, false));

    private Switch switchWidget;

    private Boolean enabled;

    public AppDetailEnhancedModeFixInteractionViewHolder(View itemView) {
        super(itemView);

        switchWidget = itemView.findViewById(R.id.switch_widget);
    }

    @Override
    public void onPrimaryButtonClick(View view) {
        Context context = getContext();
        FragmentActivity activity = ContextUtils.getActivity(getContext());
        if (activity == null) {
            return;
        }

        new AlertDialogFragment.Builder()
                .setTitle(context.getString(R.string.settings_enhance_module_fix_interaction))
                .setMessage(HtmlCompat.fromHtml(context.getString(R.string.dialog_fix_interaction_message), HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM | HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(context.getString(android.R.string.ok), null)
                .build()
                .show(activity.getSupportFragmentManager());
    }

    @Override
    public void onSecondaryButtonClick(View view) {
        Context context = getContext();
        FragmentActivity activity = ContextUtils.getActivity(getContext());
        if (activity == null) {
            return;
        }

        boolean newValue = enabled == null || !enabled;
        AppInfo appInfo = getData();
        try {
            SRManager srm = SRManager.createThrow();
            ModuleStatus moduleStatus= srm.getModuleStatus();
            int moduleVersion = moduleStatus.getVersionCode();
            if (moduleVersion < ModuleUtils.V23_0_B1) {
                new AlertDialogFragment.Builder()
                        .setMessage(context.getString(R.string.dialog_fix_interaction_module_version_too_low_message, context.getString(R.string.settings_enhance_module_fix_interaction), context.getString(R.string.working_mode_enhance_mode), ModuleUtils.versionName(context, ModuleUtils.V23_0_B1), context.getString(R.string.enhance_module)))
                        .setPositiveButton(context.getString(android.R.string.ok), null)
                        .setNeutralButton(context.getString(R.string.home_working_mode),
                                new Intent(WorkModeSettingsFragment.ACTION)
                                        .setComponent(ComponentName.createRelative(context, SettingsActivity.class.getName()))
                                        .putExtra(AlertDialogFragment.EXTRA_INTENT_TYPE, AlertDialogFragment.INTENT_TYPE_ACTIVITY))
                        .build()
                        .show(activity.getSupportFragmentManager());
                return;
            }

            if (!srm.isFixInteractionEnabled()) {
                new AlertDialogFragment.Builder()
                        .setMessage(context.getString(R.string.dialog_fix_interaction_disabled, context.getString(R.string.settings_enhance_module_fix_interaction), context.getString(R.string.enhance_module_settings)))
                        .setPositiveButton(context.getString(android.R.string.ok), null)
                        .setNeutralButton(context.getString(R.string.enhance_module_settings),
                                new Intent(ModuleSettingsFragment.ACTION)
                                        .setComponent(ComponentName.createRelative(context, SettingsActivity.class.getName()))
                                        .putExtra(AlertDialogFragment.EXTRA_INTENT_TYPE, AlertDialogFragment.INTENT_TYPE_ACTIVITY))
                        .build()
                        .show(activity.getSupportFragmentManager());
                return;
            }

            srm.setFixInteractionEnabledForPackage(appInfo.getPackageName(), appInfo.getUserId(), newValue);

            enabled = newValue;
            switchWidget.setChecked(enabled);
        } catch (Throwable tr) {
            tr.printStackTrace();

            Toast.makeText(context, context.getString(R.string.toast_failed, Objects.toString(tr, "unknown")), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBind() {
        syncSwitchState();

        itemView.setEnabled(getData().isRedirectEnabled());
        button1.setEnabled(getData().isRedirectEnabled());
        button2.setEnabled(getData().isRedirectEnabled());
        switchWidget.setEnabled(getData().isRedirectEnabled());
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        syncSwitchState();
    }

    private void syncSwitchState() {
        if (enabled == null) {
            AppInfo appInfo = getData();
            try {
                SRManager srm = SRManager.createThrow();
                if (srm.isFixInteractionEnabled()) {
                    ModuleStatus moduleStatus= srm.getModuleStatus();
                    int moduleVersion = moduleStatus.getVersionCode();
                    if (moduleVersion >= ModuleUtils.V18) {
                        if (moduleVersion >= ModuleUtils.V20_0) {
                            enabled = srm.isFixInteractionEnabledForPackage(appInfo.getPackageName(), appInfo.getUserId());
                        } else {
                            enabled = true;
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            if (enabled == null) {
                enabled = false;
            }
        }

        switchWidget.setChecked(enabled);
    }
}
