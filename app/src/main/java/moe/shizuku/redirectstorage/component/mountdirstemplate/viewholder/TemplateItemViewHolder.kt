package moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder

import android.view.*
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.mountdirstemplate.adapter.MountDirsTemplateAdapter
import moe.shizuku.redirectstorage.event.RemoteRequestHandler
import moe.shizuku.redirectstorage.event.ToastErrorHandler
import moe.shizuku.redirectstorage.utils.WordsHelper
import moe.shizuku.redirectstorage.utils.getTitle
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder

class TemplateItemViewHolder private constructor(itemView: View) : SimpleItemViewHolder<MountDirsTemplate>(itemView), View.OnCreateContextMenuListener {

    init {
        val context = itemView.context

        icon.setImageDrawable(context.getDrawable(R.drawable.ic_template_24dp))
        itemView.setOnCreateContextMenuListener(this)
    }

    override fun getAdapter(): MountDirsTemplateAdapter {
        return super.getAdapter() as MountDirsTemplateAdapter
    }

    override fun onBind() {
        title.text = data.getTitle(itemView.context)

        val size = data.packages?.filter { it.enabled }?.size ?: 0
        summary.text = String.format("%s\n%s", WordsHelper.buildOmissibleFoldersListText(summary.context, data.list, 4),
                context.getString(R.string.mount_dirs_template_app_format, context.resources.getQuantityString(R.plurals.app_amount_text_format, size, size)))
    }

    override fun onClick(view: View) {
        adapter.listener?.onClick(data)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        MenuInflater(v.context).inflate(R.menu.context_menu_mount_dirs_formula, menu)
        menu.setHeaderTitle(data.getTitle(v.context))
        menu.findItem(R.id.action_edit).setOnMenuItemClickListener {
            adapter.listener?.onClick(data)
            true
        }
        menu.findItem(R.id.action_delete).setOnMenuItemClickListener {
            AlertDialog.Builder(v.context)
                    .setTitle(R.string.delete_confirm_dialog_title)
                    .setMessage(R.string.delete_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        RemoteRequestHandler.removeMountDirsTemplate(data, ToastErrorHandler(v.context))
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .show()
            true
        }
    }

    companion object {

        val CREATOR = { inflater: LayoutInflater, parent: ViewGroup -> TemplateItemViewHolder(inflater.inflate(R.layout.detail_edit_simple_item, parent, false)) }
    }

}
