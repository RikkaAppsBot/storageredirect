package moe.shizuku.redirectstorage.component.restore.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.OpenableColumns;
import android.util.ArrayMap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.app.BaseForegroundIntentService;
import moe.shizuku.redirectstorage.component.restore.dialog.ProcessRestoreDialog;
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel;
import moe.shizuku.redirectstorage.component.restore.model.BackupModel;
import moe.shizuku.redirectstorage.license.License;
import moe.shizuku.redirectstorage.utils.BackupUtils;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.CrashReportHelper;
import moe.shizuku.redirectstorage.utils.NotificationHelper;
import moe.shizuku.redirectstorage.utils.UserHelper;

import static moe.shizuku.redirectstorage.AppConstants.ACTION_BACKUP;
import static moe.shizuku.redirectstorage.AppConstants.ACTION_BACKUP_FINISHED;
import static moe.shizuku.redirectstorage.AppConstants.ACTION_RESTORE;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_BACKUP_RESTORE;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_ID_BACKUP;
import static moe.shizuku.redirectstorage.app.ServiceKtxKt.startForegroundOrNotify;

public class BackupRestoreTaskService extends BaseForegroundIntentService {

    public static final String EXTRA_SELECTOR = BuildConfig.APPLICATION_ID +
            ".extra.CONTENT_SELECTOR";

    private static final int MSG_WHAT_NOTIFY = 1;

    private static final Gson GSON_OUT = new GsonBuilder().setVersion(11).create();
    private Handler mHandler;

    private NotificationManager mNotiManager;

    private LocalBroadcastManager mLocalBroadcastManager;
    private ProgressInfo mProgressInfo;

    public BackupRestoreTaskService() {
        super("BackupRestoreTask");

        mHandler = new Handler(Looper.getMainLooper(), msg -> {
            if (msg.what == MSG_WHAT_NOTIFY) {
                ProgressInfo info = mProgressInfo;
                updateRestoreProgress(info.n, info.progress, info.max);
                return true;
            }
            return false;
        });
    }

    private void handleRestore(@NonNull Intent intent) {
        if (!License.checkLocal()) {
            return;
        }

        final NotificationCompat.Builder n = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.restore_process);
        n.setProgress(1, 0, true);

        startForegroundOrNotify(this, NOTIFICATION_ID_BACKUP, n.build());

        final SRManager srm = SRManager.create();
        if (srm == null) {
            stopForeground(true);
            NotificationCompat.Builder b = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.restore_failed_format)
                    .setContentText(getString(R.string.toast_unable_connect_server));
            mNotiManager.notify(NOTIFICATION_ID_BACKUP, b.build());
            mLocalBroadcastManager.sendBroadcast(
                    new Intent(ProcessRestoreDialog.ACTION_UPDATE_RESTORE_DIALOG_UI)
                            .putExtra(ProcessRestoreDialog.EXTRA_WHAT, ProcessRestoreDialog.MSG_FAILED_TO_CONNECT_SERVICE)
            );
            return;
        }

        final Uri uri = intent.getParcelableExtra(EXTRA_DATA);
        final AbstractBackupModel.ContentSelector selector = intent.getParcelableExtra(EXTRA_SELECTOR);

        Objects.requireNonNull(uri);
        Objects.requireNonNull(selector);

        AbstractBackupModel data = null;
        try {
            data = AbstractBackupModel.fromJson(
                    BackupUtils.readString(Objects.requireNonNull(getContentResolver().openInputStream(uri))));
        } catch (Throwable e) {
            e.printStackTrace();
        }

        if (data == null) {
            stopForeground(true);
            cancelRestoreProgressUpdate();
            return;
        }

        List<MountDirsTemplate> mountDirs = data.getMountDirsTemplates();
        if (mountDirs != null) {
            for (MountDirsTemplate template : mountDirs) {
                try {
                    srm.updateMountDirsTemplate(template);
                } catch (Throwable e) {
                    Log.w(AppConstants.TAG, "handleRestore", e);
                    CrashReportHelper.logException(e, "handleRestore: updateMountDirsTemplate");
                }
            }
        }

        final List<RedirectPackageInfo> packages = data.getRedirectPackagesForUser(selector.selectedAppConfigurationsUserId);
        final List<ObserverInfo> observers = data.getObserversForUser(selector.selectedAppConfigurationsUserId);
        final List<SimpleMountInfo> simpleMounts = data.getSimpleMountsForUser(selector.selectedAppConfigurationsUserId);

        int progress = 0;
        int progressMax = 0;

        if (selector.selectedAppConfigurationsUserId != -1) {
            progressMax += packages.size();
            progressMax += observers.size();
            progressMax += simpleMounts.size();
        }
        if (selector.selectedCommonSettings) {
            progressMax += 1;
        }

        if (selector.selectedAppConfigurationsUserId != -1) {
            for (RedirectPackageInfo item : packages) {
                enqueueRestoreProgressUpdate(n, ++progress, progressMax);

                item.userId = UserHelper.currentUserId();
                try {
                    srm.addRedirectPackage(item, SRManager.FLAG_KILL_PACKAGE);
                } catch (Throwable e) {
                    Log.w(AppConstants.TAG, "handleRestore", e);
                    CrashReportHelper.logException(e, "handleRestore: addRedirectPackage");
                }
            }
            for (ObserverInfo item : observers) {
                enqueueRestoreProgressUpdate(n, ++progress, progressMax);

                item.userId = UserHelper.currentUserId();
                try {
                    srm.updateObserver(item);
                } catch (Throwable e) {
                    Log.w(AppConstants.TAG, "handleRestore", e);
                    CrashReportHelper.logException(e, "handleRestore: updateObserver");
                }
            }
            for (SimpleMountInfo item : simpleMounts) {
                enqueueRestoreProgressUpdate(n, ++progress, progressMax);

                item.userId = UserHelper.currentUserId();
                try {
                    srm.updateSimpleMount(item, SRManager.FLAG_FORCE_ENABLE_REDIRECT);
                } catch (Throwable e) {
                    Log.w(AppConstants.TAG, "handleRestore", e);
                    CrashReportHelper.logException(e, "handleRestore: updateSimpleMount");
                }
            }
        }

        if (selector.selectedCommonSettings) {
            enqueueRestoreProgressUpdate(n, ++progress, progressMax);

            String redirectTarget = data.getDefaultRedirectTarget();
            if (redirectTarget != null) {
                try {
                    srm.setDefaultRedirectTarget(redirectTarget);
                } catch (Throwable e) {
                    Log.w(AppConstants.TAG, "handleRestore", e);
                    CrashReportHelper.logException(e, "handleRestore: setDefaultRedirectTarget");
                }
            }

            /*final SharedPreferences.Editor editor = Settings.getPreferences().edit();
            try {
                SharedPreferencesXmlReader xml = SharedPreferencesXmlReader.parse(data.getSRSettingsXML());
                for (Map.Entry<String, Integer> entry : xml.intValues.entrySet()) {
                    if (BackupUtils.shouldIgnoreSharedPrefsItem(entry.getKey())) {
                        continue;
                    }
                    editor.putInt(entry.getKey(), entry.getValue());
                }
                for (Map.Entry<String, Long> entry : xml.longValues.entrySet()) {
                    if (BackupUtils.shouldIgnoreSharedPrefsItem(entry.getKey())) {
                        continue;
                    }
                    editor.putLong(entry.getKey(), entry.getValue());
                }
                for (Map.Entry<String, Boolean> entry : xml.boolValues.entrySet()) {
                    if (BackupUtils.shouldIgnoreSharedPrefsItem(entry.getKey())) {
                        continue;
                    }
                    editor.putBoolean(entry.getKey(), entry.getValue());
                }
                for (Map.Entry<String, String> entry : xml.stringValues.entrySet()) {
                    if (BackupUtils.shouldIgnoreSharedPrefsItem(entry.getKey())) {
                        continue;
                    }
                    editor.putString(entry.getKey(), entry.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            editor.apply();*/
        }

        stopForeground(true);
        cancelRestoreProgressUpdate();

        mLocalBroadcastManager.sendBroadcast(
                new Intent(ProcessRestoreDialog.ACTION_UPDATE_RESTORE_DIALOG_UI)
                        .putExtra(ProcessRestoreDialog.EXTRA_WHAT, ProcessRestoreDialog.MSG_FINISHED)
        );

        mLocalBroadcastManager.sendBroadcast(new Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME));
        if (!mLocalBroadcastManager.sendBroadcast(new Intent(AppConstants.ACTION_REQUEST_REFRESH_LIST))) {
            mLocalBroadcastManager.sendBroadcast(new Intent(AppConstants.ACTION_REQUEST_CLEAR_APP_LIST));
        }
    }

    @Override
    public int getForegroundServiceNotificationId() {
        return NOTIFICATION_ID_BACKUP;
    }

    @Override
    public Notification onStartForeground() {
        NotificationCompat.Builder n = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.notification_backup_process_title);
        n.setProgress(1, 0, true);
        return n.build();
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onCreateNotificationChannel(@NonNull NotificationManager notificationManager) {
        NotificationChannel channel = new NotificationChannel(
                NOTIFICATION_CHANNEL_BACKUP_RESTORE,
                getString(R.string.notification_channel_backup_restore),
                NotificationManager.IMPORTANCE_LOW);
        channel.enableLights(false);
        channel.enableVibration(false);
        if (BuildUtils.atLeast29()) {
            channel.setAllowBubbles(false);
        }

        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public void onCreate() {
        mNotiManager = getSystemService(NotificationManager.class);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);

        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }
        switch (intent.getAction()) {
            case ACTION_BACKUP:
                handleBackup(intent);
                break;
            case ACTION_RESTORE:
                handleRestore(intent);
                break;
        }
    }

    private void handleBackup(@NonNull Intent intent) {
        final SRManager srm = SRManager.create();
        if (srm == null) {
            stopForeground(true);
            NotificationCompat.Builder b = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.notification_backup_failed_title)
                    .setContentText(getString(R.string.toast_unable_connect_server));
            mNotiManager.notify(NOTIFICATION_ID_BACKUP, b.build());
            return;
        }

        final Uri backupUri = Objects.requireNonNull(intent.getData());
        try (final OutputStream out = getContentResolver().openOutputStream(backupUri)) {
            if (out == null) {
                throw new IOException("Cannot open output stream");
            }
            /*final InputStream spInput = new FileInputStream(
                    new File(SRApplication.getPreferenceStorageContext(this)
                            .getFilesDir()
                            .getParentFile(),
                            "shared_prefs/" + Settings.PREFERENCE_NAME + ".xml"));*/

            List<RedirectPackageInfo> pi = srm.getRedirectPackages(SRManager.MATCH_DEFAULT | SRManager.GET_PACKAGE_INFO, SRManager.USER_ALL);
            List<ObserverInfo> observers = srm.getObservers();
            List<SimpleMountInfo> simpleMounts = srm.getSimpleMounts();

            PackageManager pm = getPackageManager();
            Map<String, String> labels = new ArrayMap<>();
            for (RedirectPackageInfo i : pi) {
                if (!labels.containsKey(i.packageName)) {
                    try {
                        labels.put(i.packageName, i.packageInfo.applicationInfo.loadLabel(pm).toString());
                    } catch (Throwable tr) {
                        Log.w(AppConstants.TAG, "loadLabel " + i.packageName + " " + i.userId, tr);
                    }
                }
            }

            BackupModel backupModel = new BackupModel.Builder()
                    .setServerVersion(srm.getVersion())
                    .setRedirectPackage(pi)
                    .setObservers(observers)
                    .setSimpleMounts(simpleMounts)
                    .setDefaultRedirectTarget(srm.getDefaultRedirectTarget())
                    .setMountDirsTemplates(srm.getMountDirsTemplates())
                    //.setSRSettingsXML(IOUtils.toString(spInput))
                    .setPackageLabelMap(labels)
                    .build();

            //spInput.close();

            byte[] json = GSON_OUT.toJson(backupModel).getBytes(StandardCharsets.UTF_8);
            for (int i = 0; i < json.length; i++) {
                json[i] ^= AppConstants.BACKUP_XOR;
            }

            ByteArrayOutputStream is = new ByteArrayOutputStream();
            is.write(AppConstants.BACKUP_MAGIC);
            is.write(json);

            out.write(is.toByteArray());
        } catch (Exception e) {
            Log.w(AppConstants.TAG, "handleBackup", e);

            NotificationCompat.Builder b = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.notification_backup_failed_title)
                    .setContentText(getString(R.string.toast_failed, Objects.toString(e, "unknown")));
            mNotiManager.notify(NOTIFICATION_ID_BACKUP, b.build());
        }

        stopForeground(true);

        try (Cursor cursor = getContentResolver().query(
                backupUri, null, null, null, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                String contentText = getString(R.string.notification_backup_finished_summary, displayName);
                Notification doneNoti = NotificationHelper.create(this, NOTIFICATION_CHANNEL_BACKUP_RESTORE, R.string.notification_backup_finished_title)
                        .setContentText(contentText)
                        .build();
                mNotiManager.notify(NOTIFICATION_ID_BACKUP, doneNoti);
                mLocalBroadcastManager
                        .sendBroadcast(new Intent(ACTION_BACKUP_FINISHED).putExtra("message", contentText));
            }
        }
    }

    private void enqueueRestoreProgressUpdate(NotificationCompat.Builder n, int progress, int max) {
        mProgressInfo = new ProgressInfo(n, progress, max);
        if (mHandler.hasMessages(MSG_WHAT_NOTIFY)) {
            return;
        }
        mHandler.sendMessageDelayed(Message.obtain(mHandler, MSG_WHAT_NOTIFY), 300);
    }

    private void cancelRestoreProgressUpdate() {
        mHandler.removeMessages(MSG_WHAT_NOTIFY);
        mNotiManager.cancel(NOTIFICATION_ID_BACKUP);
    }

    private void updateRestoreProgress(NotificationCompat.Builder n, int progress, int max) {
        String message = getString(R.string.restore_process_format, progress, max);

        n.setOngoing(true);
        n.setProgress(max, progress, false);
        n.setContentText(message);

        mNotiManager.notify(NOTIFICATION_ID_BACKUP, n.build());

        Intent intent = new Intent(ProcessRestoreDialog.ACTION_UPDATE_RESTORE_DIALOG_UI);
        intent.putExtra(ProcessRestoreDialog.EXTRA_PROGRESS, progress);
        intent.putExtra(ProcessRestoreDialog.EXTRA_MAX, max);
        intent.putExtra(ProcessRestoreDialog.EXTRA_WHAT, ProcessRestoreDialog.MSG_UPDATE_PROGRESS);
        intent.putExtra(ProcessRestoreDialog.EXTRA_MESSAGE, message);
        mLocalBroadcastManager.sendBroadcast(intent);
    }

    private static class ProgressInfo {

        NotificationCompat.Builder n;
        int progress;
        int max;

        ProgressInfo(NotificationCompat.Builder n, int progress, int max) {
            this.n = n;
            this.progress = progress;
            this.max = max;
        }
    }
}
