package moe.shizuku.redirectstorage.component.exportfolders.list

import android.text.TextUtils
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.view.View.OnClickListener
import android.view.View.OnCreateContextMenuListener
import androidx.core.view.isVisible
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.exportfolders.list.ExportFolderToggleViewHolder.Listener
import moe.shizuku.redirectstorage.databinding.ObserverToggleBinding
import moe.shizuku.redirectstorage.model.AppObserverInfo
import rikka.core.res.resolveColor
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class ExportFolderToggleViewHolder(private val binding: ObserverToggleBinding) : BaseListenerViewHolder<AppObserverInfo, Listener?>(binding.root), OnCreateContextMenuListener, OnClickListener {

    companion object {
        val CREATOR = Creator<AppObserverInfo> { inflater: LayoutInflater, parent: ViewGroup? ->
            ExportFolderToggleViewHolder(ObserverToggleBinding.inflate(inflater, parent, false))
        }
    }

    interface Listener {
        fun onViewObserverClick(info: AppObserverInfo)
        fun onDeleteObserverClick(info: AppObserverInfo)
        fun onToggleObserverClick(oldInfo: AppObserverInfo)
    }

    private inline val icon get() = binding.icon
    private inline val title get() = binding.title
    private inline val content get() = binding.text1
    private inline val summary get() = binding.summary
    private inline val button1 get() = binding.button1
    private inline val button2 get() = binding.button2
    private inline val switchView get() = binding.switchWidget
    private inline val addView get() = binding.add

    init {
        button1.setOnClickListener(this)
        button1.setOnCreateContextMenuListener(this)
        button2.setOnClickListener { onWidgetClick() }
    }

    override fun onClick(view: View) {
        listener!!.onViewObserverClick(data)
    }

    private fun onWidgetClick() {
        onSwitchClick()
    }

    private fun onSwitchClick() {
        listener!!.onToggleObserverClick(data)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View,
                                     menuInfo: ContextMenuInfo?) {
        val context = v.context
        val menuInflater = MenuInflater(context)
        menuInflater.inflate(R.menu.context_menu_observer_toggle, menu)
        menu.setHeaderTitle(getTitle().toString())
        val deleteAction = menu.findItem(R.id.action_delete)
        deleteAction.setOnMenuItemClickListener { onItemDeleteClick() }
        deleteAction.isVisible = data.local
    }

    private fun onItemDeleteClick(): Boolean {
        listener!!.onDeleteObserverClick(data)
        return true
    }

    override fun onBind() {
        syncEnabledState()
        val context = itemView.context
        title.text = getTitle()
        content.text = HtmlCompat.fromHtml(context.getString(R.string.detail_item_link_summary, data.source, data.target))
        if (data.icon) {
            icon.setImageDrawable(context.getDrawable(R.drawable.ic_folder_sync_24dp))
        } else {
            icon.setImageDrawable(null)
        }
        updateExtraSummary()
    }

    private fun updateExtraSummary() {
        val context = itemView.context
        val sb = StringBuilder()
        val theme = context.theme
        val description = data.getDescriptionText()
        if (description != null) {
            sb.append(context.getString(R.string.link_custom_text_format,
                    0xffffff and theme.resolveColor(R.attr.colorWarning),
                    description))
            sb.append("<br>")
        }
        if (data.online && data.local) {
            sb.append(context.getString(R.string.rule_from_online, 0xffffff and theme.resolveColor(R.attr.colorSafe)))
            sb.append("<br>")
        }
        if (data.sourceMountDir != null) {
            sb.append(context.getString(R.string.link_conflict,
                    0xffffff and theme.resolveColor(R.attr.colorAlert),
                    context.getString(R.string.detail_accessible_folders),
                    data.sourceMountDir))
            sb.append("<br>")
        }
        if (!TextUtils.isEmpty(sb)) {
            summary.visibility = View.VISIBLE
            summary.text = HtmlCompat.fromHtml(sb.toString(), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE
                    or HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS)
        } else {
            summary.visibility = View.GONE
        }
    }

    override fun onBind(payloads: List<Any>) {
        syncEnabledState()
        updateExtraSummary()
    }

    private fun syncEnabledState() {
        val candidateOnline = data.online && !data.local
        button2.isChecked = data.enabled
        switchView.isVisible = !candidateOnline
        addView.isVisible = candidateOnline
    }

    private fun getTitle(): CharSequence {
        return HtmlCompat.fromHtml(context.getString(R.string.detail_item_link_title, data.getTitleText()))
    }

}