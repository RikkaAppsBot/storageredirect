package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppObserverInfo;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import rikka.recyclerview.BaseViewHolder;

public class EditDescriptionViewHolder extends EditButtonViewHolder<ObserverInfoBuilder, EditDescriptionViewHolder.Listener> {

    public static final BaseViewHolder.Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditDescriptionViewHolder holder = new EditDescriptionViewHolder(
                inflater.inflate(R.layout.observer_info_edit_edit_button, parent, false));
        holder.editButton.setEnabled(parent.isEnabled());
        holder.editButton.setMaxLines(Integer.MAX_VALUE);
        return holder;
    };

    public interface Listener {

        void onEditDescriptionClick(String defaultChoice);
    }

    private EditDescriptionViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public Drawable getIconDrawable() {
        return itemView.getResources().getDrawable(R.drawable.ic_help_outline_24dp, itemView.getContext().getTheme());
    }

    @Override
    public CharSequence getTitle() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_description);
    }

    @Override
    public void onClick() {
        getListener().onEditDescriptionClick(getData().getDescription());
    }

    @Override
    public void onBind() {
        super.onBind();

        editButton.setContentText(AppObserverInfo.Companion.getSummaryText(getData().getDescription()));
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        onBind();
    }
}
