package moe.shizuku.redirectstorage.component.home

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.DeadObjectException
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.edit
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.applist.AppListActivity
import moe.shizuku.redirectstorage.component.managespace.ManageSpaceActivity
import moe.shizuku.redirectstorage.component.payment.PurchaseFragment
import moe.shizuku.redirectstorage.lang.*
import moe.shizuku.redirectstorage.license.AlipayHelper
import moe.shizuku.redirectstorage.license.GoogleBillingHelper
import moe.shizuku.redirectstorage.license.RedeemHelper
import moe.shizuku.redirectstorage.utils.SizeTagHandler
import moe.shizuku.redirectstorage.utils.ViewUtils
import rikka.core.util.ContextUtils
import rikka.core.util.ResourceUtils
import rikka.html.text.HtmlCompat
import rikka.internal.help.HelpProvider
import java.util.*

class FailedFragment : AppFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.failed_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (parentFragment == null) return
        val model = ViewModelProvider(requireParentFragment()).get(FailedInfoViewModel::class.java)
        if (isSupportedThrowable(model.throwable)) {
            val holder = createViewHolder(model.throwable, view)
            holder.fragment = this
            holder.onBind()
        }
        if (view is ScrollView) {
            view.setOnScrollChangeListener { _: View?, _: Int, scrollY: Int, _: Int, _: Int -> (requireActivity() as HomeActivity).homeHeader.isRaised = scrollY > 0 }
        }
    }

    private abstract class BaseViewHolder<T> internal constructor(val itemView: View) {

        val title: TextView = itemView.findViewById(android.R.id.title)
        val text1: TextView = itemView.findViewById(android.R.id.text1)
        val buttonBar: View = itemView.findViewById(R.id.button_bar)
        val button1: TextView = itemView.findViewById(android.R.id.button1)
        val button2: TextView = itemView.findViewById(android.R.id.button2)
        val button3: TextView = itemView.findViewById(android.R.id.button3)
        val button4: TextView = itemView.findViewById(R.id.button4)

        init {
            text1.movementMethod = LinkMovementMethod.getInstance()
        }

        var fragment: FailedFragment? = null
        var data: T? = null
            private set

        abstract fun onBind()

        fun setData(data: T) {
            this.data = data
        }

        internal fun setButton(button: TextView, @StringRes text: Int, @DrawableRes drawable: Int) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                ViewUtils.setCompoundDrawableStartWithIntrinsicBounds(button, drawable)
            } else {
                val d = itemView.context.getDrawable(drawable)
                if (d != null) {
                    val color = ResourceUtils.resolveColor(itemView.context.theme, android.R.attr.colorAccent)
                    d.setTint(color)
                }
                ViewUtils.setCompoundDrawableStartWithIntrinsicBounds(button, d)
            }
            button.setText(text)
        }
    }

    private class CannotConnectServiceItemHolder(itemView: View) : BaseViewHolder<Any?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.setText(R.string.something_wrong)
            text1.text = HtmlCompat.fromHtml(
                    context.getString(R.string.dialog_cannot_connect_service_message,
                            context.getString(R.string.logcat_title),
                            context.getString(R.string.action_refresh),
                            context.getString(R.string.logcat_title),
                            context.getString(R.string.supported_su),
                            context.getString(R.string.unsupported_su)))
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.GONE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
            setButton(button1, R.string.action_refresh, R.drawable.ic_refresh_24dp)
        }

        init {
            button1.setOnClickListener { v: View ->
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
        }
    }

    private class NewVersionItemHolder(itemView: View) : BaseViewHolder<Int>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.setText(R.string.dialog_update_server_title)
            text1.text = HtmlCompat.fromHtml(
                    context.getString(R.string.dialog_update_server_message,
                            String.format(Locale.ENGLISH, "v%d", Constants.SERVER_VERSION),
                            String.format(Locale.ENGLISH, "v%d", data)),
                    null,
                    SizeTagHandler.getInstance())
            setButton(button1, R.string.action_upgrade, R.drawable.ic_update_24dp)
            setButton(button2, R.string.action_changelog, R.drawable.ic_description_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                val activity = ContextUtils.getActivity<AppActivity>(v.context)
                activity?.startStarter()
            }
            button2.setOnClickListener { v: View -> HelpProvider.get(v.context, "changelog")?.startActivity(v.context) }
        }
    }

    private class InvalidLicenseItemHolder(itemView: View) : BaseViewHolder<Int?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            val license = if (!TextUtils.isEmpty(GoogleBillingHelper.getToken())) String.format("Google: %s<br>Token: %s", GoogleBillingHelper.getOrderId(), GoogleBillingHelper.getToken()) else if (!TextUtils.isEmpty(RedeemHelper.getRedeemCode())) String.format("Redeem: %s", RedeemHelper.getRedeemCode()) else if (!TextUtils.isEmpty(AlipayHelper.getOrderId())) String.format("Alipay: %s", AlipayHelper.getOrderId()) else "Trail license<br>Have you enabled more than 3 apps?"
            title.text = context.getString(R.string.dialog_invalid_license_title, data)
            text1.text = HtmlCompat.fromHtml(
                    context.getString(R.string.dialog_invalid_license_message,
                            license, context.getString(R.string.helplib_title), URL_GOOGLE, URL),
                    HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE,
                    null,
                    SizeTagHandler.getInstance())
            text1.movementMethod = LinkMovementMethod.getInstance()
            setButton(button1, R.string.dialog_invalid_license_pay, R.drawable.ic_payment_unlock_24dp)
            setButton(button2, R.string.dialog_invalid_license_restart, R.drawable.ic_update_24dp)
            setButton(button3, R.string.dialog_invalid_license_reset_config, R.drawable.ic_settings_restore_24dp)
            setButton(button4, R.string.helplib_title, R.drawable.ic_help_outline_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.VISIBLE
            button4.visibility = View.VISIBLE
        }

        companion object {
            private const val URL = "https://sr.rikka.app/"
            private const val URL_GOOGLE = "https://play.google.com/store/apps/details?id=moe.shizuku.redirectstorage"
        }

        init {
            button1.setOnClickListener { v: View ->
                PurchaseFragment.startActivity(v.context)
            }
            button2.setOnClickListener { v: View ->
                val activity = ContextUtils.getActivity<AppActivity>(v.context)
                activity?.startStarter()
            }
            button3.setOnClickListener { v: View ->
                val context = v.context
                DeviceSpecificSettings.preferences.edit {
                    remove(GoogleBillingHelper.KEY_TOKEN)
                    remove(AlipayHelper.KEY_ALIPAY_ORDER_ID)
                    remove(RedeemHelper.KEY_REDEEM_CODE)
                }
                context.startActivity(Intent(context, ManageSpaceActivity::class.java))
            }
            button4.setOnClickListener { v: View ->
                val context = v.context
                context.startActivity(Intent(context, HelpActivity::class.java))
            }
        }
    }

    private class NotRunningItemHolder(itemView: View) : BaseViewHolder<Any?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.setText(R.string.dialog_introduction_title)
            text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_introduction_message, context.getString(R.string.app_name)), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
            setButton(button1, R.string.action_start_service, R.drawable.ic_settings_start_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.GONE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                val activity = ContextUtils.getActivity<AppActivity>(v.context)
                activity?.startStarter()
            }
        }
    }

    private class BadStorageItemHolder(itemView: View) : BaseViewHolder<Array<String>>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.setText(R.string.dialog_bad_storage_title)
            text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_bad_storage_summary,
                    data!![0], data!![1]))
            setButton(button1, R.string.dialog_unsupported_continue_once, R.drawable.ic_run_24dp)
            setButton(button2, R.string.dialog_unsupported_continue, R.drawable.ic_run_forever_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                Settings.setIgnoreUnsupportedStorage(newValue = true, forever = false)
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
            button2.setOnClickListener { v: View ->
                Settings.setIgnoreUnsupportedStorage(newValue = true, forever = true)
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
        }
    }

    private class NoLogDetectedViewHolder(itemView: View) : BaseViewHolder<Int?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.text = context.getString(R.string.dialog_no_log_title)
            text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_no_log_summary,
                    context.getString(R.string.working_mode_basic_mode), context.getString(R.string.working_mode_enhance_mode)))
            setButton(button1, R.string.dialog_unsupported_continue_once, R.drawable.ic_run_24dp)
            setButton(button2, R.string.dialog_unsupported_continue, R.drawable.ic_run_forever_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                Settings.setIgnoreIgnoreNoLog(true, false)
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
            button2.setOnClickListener { v: View ->
                Settings.setIgnoreIgnoreNoLog(true, true)
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
        }
    }

    private class ModuleInstallationViewHolder(itemView: View) : BaseViewHolder<Int?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.text = context.getString(R.string.dialog_module_not_installed_correctly_title, -data!!)
            when (data) {
                SRManager.MODULE_NOT_IN_MEMORY, SRManager.MODULE_NOT_IN_64_MEMORY -> text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_module_not_in_memory), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                SRManager.MODULE_NO_64BIT_LIB -> text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_module_no_64_bit), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                else -> text1.text = null
            }
            setButton(button1, R.string.dialog_unsupported_continue_once, R.drawable.ic_run_24dp)
            setButton(button2, R.string.action_refresh, R.drawable.ic_refresh_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.GONE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                Settings.isIgnoreModuleIssue = true
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
            button2.setOnClickListener { v: View ->
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
        }
    }

    private class ExceededTrialViewHolder(itemView: View) : BaseViewHolder<Any?>(itemView) {

        override fun onBind() {
            val context = itemView.context
            title.text = context.getString(R.string.dialog_exceeded_trial_title)
            text1.text = HtmlCompat.fromHtml(context.getString(R.string.dialog_exceeded_trial_message), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
            setButton(button1, R.string.dialog_invalid_license_pay, R.drawable.ic_payment_unlock_24dp)
            setButton(button2, R.string.apps_management, R.drawable.ic_home_apps_24dp)
            setButton(button3, R.string.action_refresh, R.drawable.ic_refresh_24dp)
            buttonBar.visibility = View.VISIBLE
            button1.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            button3.visibility = View.VISIBLE
            button4.visibility = View.GONE
        }

        init {
            button1.setOnClickListener { v: View ->
                val context = v.context
                PurchaseFragment.startActivity(context)
            }
            button2.setOnClickListener { v: View ->
                val context = v.context
                context.startActivity(Intent(context, AppListActivity::class.java))
            }
            button3.setOnClickListener { v: View ->
                LocalBroadcastManager.getInstance(v.context)
                        .sendBroadcast(Intent(AppConstants.ACTION_REQUEST_REFRESH_HOME))
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(): FailedFragment {
            return FailedFragment()
        }

        @JvmStatic
        fun isSupportedThrowable(throwable: Throwable?): Boolean {
            if (throwable is RuntimeException
                    && "SELinux" == throwable.message) {
                return true
            } else if (throwable is ServerVersionMismatchException) {
                return true
            } else if (throwable is InvalidLicenseException) {
                return true
            } else if (throwable is EmulatedStorageNotFoundException) {
                return true
            } else if (throwable is NoLogDetectedException) {
                return true
            } else if (throwable is ModuleInstallationException) {
                return true
            } else if (throwable is DeadObjectException
                    && "service not running" == throwable.message) {
                return true
            }
            return false
        }

        private fun createViewHolder(throwable: Throwable?, itemView: View): BaseViewHolder<*> {
            return if (throwable is RuntimeException
                    && "SELinux" == throwable.message) {
                CannotConnectServiceItemHolder(itemView)
            } else if (throwable is ServerVersionMismatchException) {
                val holder = NewVersionItemHolder(itemView)
                holder.setData(throwable.currentVersion)
                holder
            } else if (throwable is InvalidLicenseException) {
                val holder = InvalidLicenseItemHolder(itemView)
                holder.setData(throwable.reason)
                holder
            } else if (throwable is EmulatedStorageNotFoundException) {
                val holder = BadStorageItemHolder(itemView)
                holder.setData(arrayOf(
                        throwable.real,
                        throwable.expect))
                holder
            } else if (throwable is NoLogDetectedException) {
                val holder = NoLogDetectedViewHolder(itemView)
                holder.setData(throwable.second)
                holder
            } else if (throwable is ModuleInstallationException) {
                val holder = ModuleInstallationViewHolder(itemView)
                holder.setData(throwable.reason)
                holder
            } else if (throwable is DeadObjectException
                    && "service not running" == throwable.message) {
                NotRunningItemHolder(itemView)
            } else {
                require(throwable == null) { "unsupported exception " + throwable?.javaClass?.name + ": " + throwable?.message }
                throw NullPointerException()
            }
        }
    }
}