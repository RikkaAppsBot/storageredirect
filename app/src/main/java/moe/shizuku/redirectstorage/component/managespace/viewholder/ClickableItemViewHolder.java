package moe.shizuku.redirectstorage.component.managespace.viewholder;

import android.view.View;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.managespace.adapter.ManageSpaceAdapter;
import moe.shizuku.redirectstorage.event.EventIntents;
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder;
import rikka.core.util.ResourceUtils;

public class ClickableItemViewHolder extends SimpleItemViewHolder<Integer> {

    public static final Creator<Integer> CREATOR = (inflater, parent) ->
            new ClickableItemViewHolder(inflater.inflate(R.layout.detail_simple_item, parent, false));

    private ClickableItemViewHolder(View itemView) {
        super(itemView);

        title.setTextColor(ResourceUtils.resolveColor(itemView.getContext().getTheme(), android.R.attr.colorAccent));
        summary.setVisibility(View.GONE);
    }

    @Override
    public void onBind() {
        switch (getData()) {
            case ManageSpaceAdapter.ID_CLIENT_RESET_DATA: {
                title.setText(R.string.manage_space_client_data_reset_button);
                break;
            }
            case ManageSpaceAdapter.ID_SERVER_RESET_DATA_ONLY: {
                title.setText(R.string.manage_space_server_data_reset_config_only);
                break;
            }
            case ManageSpaceAdapter.ID_SERVER_CLEAR_ALL: {
                title.setText(R.string.manage_space_server_data_delete_all);
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        LocalBroadcastManager.getInstance(view.getContext())
                .sendBroadcast(EventIntents.CommonList.notifyItemClick(getClass(), getData()));
    }
}
