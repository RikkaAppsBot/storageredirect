package moe.shizuku.redirectstorage.component.sharehelper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.Optional;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.provider.ServerFileProvider;
import rikka.core.util.IntentUtils;
import rikka.material.chooser.ChooserFragment;

public class ShareHelperActivity extends AppActivity {

    public static final String EXTRA_TARGET_COMPONENT = BuildConfig.APPLICATION_ID + ".extra.target_component";

    public static void start(Context context, Uri uri, @Nullable String mimeType) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setComponent(ComponentName.createRelative(context, "moe.shizuku.redirectstorage.InternalShareHelperActivity"));
        intent.setType(mimeType);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(intent);
    }

    @Override
    public void onApplyUserThemeResource(@NonNull Resources.Theme theme, boolean isDecorView) {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Ignore null intent and wrong action
        if (getIntent() == null
                || !Intent.ACTION_SEND.equals(getIntent().getAction())
                || getIntent().getExtras() == null
                || !getIntent().getExtras().containsKey(Intent.EXTRA_STREAM)) {
            Log.d(AppConstants.TAG, "This type of intent received in ShareHelperActivity " +
                    "is not expected. Please set ACTION_SEND as action.");
            finish();
            return;
        }

        String referrer = getActualReferrer();
        Uri sourceUri = getIntent().getParcelableExtra(Intent.EXTRA_STREAM);
        Uri convertedUri;
        if ("file".equalsIgnoreCase(sourceUri.getScheme())) {
            SRManager rsm = SRManager.create();
            if (rsm == null) {
                finish();
                return;
            }

            convertedUri = ServerFileProvider.getContentUri(
                    referrer, 0, new File(sourceUri.getPath()));
        } else {
            convertedUri = sourceUri;
        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setFlags(getIntent().getFlags());
        sharingIntent.putExtra(Intent.EXTRA_STREAM, convertedUri);
        sharingIntent.setType(Optional.ofNullable(getIntent().getType()).orElse("*/*"));

        if (getIntent().hasExtra(EXTRA_TARGET_COMPONENT)) {
            Intent intent = new Intent(sharingIntent);
            intent.setComponent(getIntent().getParcelableExtra(EXTRA_TARGET_COMPONENT));
            IntentUtils.startActivity(this, intent, getString(R.string.chooser_no_apps));
            finish();
        } else if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, ChooserFragment.newInstance(
                            getString(R.string.dialog_share_file_chooser_title), sharingIntent))
                    .commit();
        }
    }

}
