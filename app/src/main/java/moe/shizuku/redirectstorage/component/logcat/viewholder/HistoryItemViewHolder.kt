package moe.shizuku.redirectstorage.component.logcat.viewholder

import android.content.Intent
import android.text.format.Formatter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.logcat.dialog.LogViewDialog
import rikka.core.content.FileProvider
import rikka.core.util.ContextUtils
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator
import java.io.File

class HistoryItemViewHolder private constructor(itemView: View) : BaseViewHolder<File>(itemView) {

    companion object {
        @JvmField
        val CREATOR = Creator<File> { inflater: LayoutInflater, parent: ViewGroup? -> HistoryItemViewHolder(inflater.inflate(R.layout.logcat_history_item_layout, parent, false)) }
    }

    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val text1: TextView = itemView.findViewById(android.R.id.text1)
    private val text2: TextView = itemView.findViewById(android.R.id.text2)
    private val viewButton = itemView
    private val sendButton: Button = itemView.findViewById(android.R.id.button2)
    private val deleteButton = itemView.findViewById<Button>(android.R.id.button3)

    init {
        viewButton.setOnClickListener { v: View ->
            val activity = ContextUtils.requireActivity<FragmentActivity>(v.context)
            LogViewDialog.newInstance(data).show(activity.supportFragmentManager)
        }
        sendButton.setOnClickListener { v: View ->
            val context = v.context
            val uri = FileProvider.getUriForFile(context, AppConstants.SUPPORT_PROVIDER_AUTHORITY, this@HistoryItemViewHolder.data)
            val intent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_STREAM, uri)
                type = "text/*"
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            //ChooserActivity.startWithStreamNoThrow(context, context.getString(R.string.action_send), intent, uri)
            context.startActivity(Intent.createChooser(intent, context.getString(R.string.action_send)))
        }
        deleteButton.setOnClickListener {
            data.delete()
            adapter.notifyItemRemoved(adapterPosition)
            adapter.getItems<Any>().remove(data)
        }
    }

    override fun onBind() {
        super.onBind()

        title.text = data.name.substring(0, data.name.lastIndexOf("."))
        val length = data.length()

        text1.text = data.absolutePath
        text2.text = Formatter.formatFileSize(itemView.context, length)
        sendButton.isVisible = length > 0
    }
}