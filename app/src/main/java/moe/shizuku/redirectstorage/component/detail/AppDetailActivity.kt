package moe.shizuku.redirectstorage.component.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.event.PackageInstallationEvent
import moe.shizuku.redirectstorage.utils.IntentCompat
import moe.shizuku.redirectstorage.utils.Logger.LOGGER
import moe.shizuku.redirectstorage.viewmodel.Status

class AppDetailActivity : AppBarFragmentActivity(), PackageInstallationEvent {

    private val viewModel by appDetailViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.appInfo.observe(this) {
            LOGGER.d("${this::class.java.simpleName}: AppInfo")

            when (it.status) {
                Status.SUCCESS -> {

                }
                Status.ERROR -> {
                    val message = if (!it.error.message.isNullOrBlank()) it.error.message else it.error.javaClass.name
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                }
                Status.LOADING -> {

                }
            }
        }

        appBar!!.setTitle(R.string.app_settings)
        appBar!!.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, AppDetailFragment.newInstance())
                    .commit()
        }

        Events.registerPackageInstallationEvent(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent == null) return
        if (actualReferrer == packageName && !intent.hasExtra(IntentCompat.EXTRA_PACKAGE_NAME)) return

        /*val tb = TaskStackBuilder.create(this)
        onCreateNavigateUpTaskStack(tb)
        onPrepareNavigateUpTaskStack(tb)
        tb.startActivities()
        startActivity(intent)*/

        viewModel.load(this, intent)

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, AppDetailFragment.newInstance())
                .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        Events.unregisterPackageInstallationEvent(this)
    }

    override fun onPackageRemoved(packageName: String, userId: Int) {
        val appInfo = viewModel.appInfo.value?.data
        if (appInfo != null && packageName == appInfo.packageName && userId == appInfo.userId) {
            finish()
        }
    }
}