package moe.shizuku.redirectstorage.component.filemonitor.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import kotlinx.coroutines.Job;
import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.SimpleAppInfo;
import moe.shizuku.redirectstorage.utils.AppIconCache;
import moe.shizuku.redirectstorage.utils.FilterHelper;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.ClassCreatorPool;

public class FilterSettingsAppListAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool>
        implements FilterHelper.FilterItemsProvider<SimpleAppInfo> {

    private List<SimpleAppInfo> mAllApps = new ArrayList<>();
    private boolean[] mSelected = new boolean[0];

    private String mKeyword = null;

    private final FilterHelper<SimpleAppInfo> mFilterHelper;

    public FilterSettingsAppListAdapter() {
        super();

        mFilterHelper = new FilterHelper<>(this, new FilterHelper.Filter<SimpleAppInfo>() {
            @Override
            public boolean contains(String keyword, SimpleAppInfo appInfo) {
                return TextUtils.isEmpty(keyword) ||
                        appInfo.getApplicationInfo().packageName.toLowerCase()
                                .contains(keyword.toLowerCase()) ||
                        appInfo.getLabel().toString().toLowerCase().contains(keyword.toLowerCase());
            }
        });
        mFilterHelper.setSearching(true);
    }

    public void setAllData(@NonNull List<SimpleAppInfo> allApps, @NonNull boolean[] selected) {
        mAllApps = allApps;
        mSelected = selected;
    }

    private void setDisplayData(@NonNull List<SimpleAppInfo> displayApps) {
        getItems().clear();
        getItems().addAll(displayApps);
        notifyDataSetChanged();
    }

    public void startFilter() {
        mFilterHelper.filter();
    }

    public void setSearchKeyword(@Nullable String keyword) {
        mFilterHelper.setKeyword(mKeyword = keyword);
    }

    @Nullable
    public String getSearchKeyword() {
        return mKeyword;
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool().putRule(SimpleAppInfo.class, (inflater, parent) ->
                new ViewHolder(inflater.inflate(R.layout.checkbox_with_icon, parent ,false)));
    }

    @Override
    public List<SimpleAppInfo> onFilterStart() {
        return mAllApps;
    }

    @Override
    public void onFilterFinish(List<SimpleAppInfo> items) {
        setDisplayData(items);
    }

    private class ViewHolder extends BaseViewHolder<SimpleAppInfo> {

        private final CheckBox checkBox;
        private final TextView label;
        private final ImageView icon;

        private Job mJob;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(android.R.id.button1);
            label = itemView.findViewById(android.R.id.title);
            icon = itemView.findViewById(android.R.id.icon);

            itemView.setOnClickListener(v -> checkBox.toggle());
            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                final int index = mAllApps.indexOf(getData());
                if (mSelected[index] != isChecked) {
                    mSelected[index] = isChecked;
                    notifyItemChanged(getAdapterPosition(), AppConstants.PAYLOAD);
                }
            });
        }

        @Override
        public void onBind() {
            syncViewsState();
            label.setText(getData().getLabel());

            mJob = AppIconCache.loadIconBitmapAsync(
                    itemView.getContext(),
                    getData().getApplicationInfo(),
                    getData().getUserId(),
                    icon
            );
        }

        @Override
        public void onBind(@NonNull List<Object> payloads) {
            syncViewsState();
        }

        @Override
        public void onRecycle() {
            if (mJob != null && mJob.isActive()) {
                mJob.cancel(null);
            }
        }

        private void syncViewsState() {
            checkBox.setChecked(mSelected[mAllApps.indexOf(getData())]);
        }
    }

}
