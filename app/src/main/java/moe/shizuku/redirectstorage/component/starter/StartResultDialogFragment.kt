package moe.shizuku.redirectstorage.component.starter

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import rikka.html.text.toHtml
import rikka.internal.help.HelpProvider

class StartResultDialogFragment : AlertDialogFragment() {

    companion object {
        fun newInstance(code: Int): StartResultDialogFragment {
            val dialog = StartResultDialogFragment()
            dialog.arguments = Bundle().apply {
                putInt(AppConstants.EXTRA_EXIT_CODE, code)
            }
            return dialog
        }
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        val context = builder.context
        val exitCode = arguments?.getInt(AppConstants.EXTRA_EXIT_CODE, Int.MIN_VALUE) ?: Int.MIN_VALUE

        val title = when (exitCode) {
            StarterConstants.EXIT_SUCCESS -> null
            else -> context.getString(R.string.something_wrong)
        }
        val text = when (exitCode) {
            StarterConstants.EXIT_SUCCESS -> context.getString(R.string.starter_dialog_starter_exit)
            StarterConstants.EXIT_FATAL_IO_EXCEPTION, StarterConstants.EXIT_FATAL_UNABLE_OPEN_ROOT_SHELL -> context.getString(R.string.starter_dialog_unable_open_root_shell)
            StarterConstants.EXIT_STARTER_IS_RUNNING -> context.getString(R.string.starter_dialog_stater_is_running)
            StarterConstants.EXIT_FATAL_EXEC_DENIED -> {
                var download: String? = null
                val entity = HelpProvider.get(context, "download")
                if (entity != null) {
                    download = entity.content.get()
                }
                if (download == null) {
                    download = "https://sr.rikka.app/download.html"
                }
                context.getString(R.string.starter_dialog_exec_perm, download).toHtml()
            }
            else -> context.getString(R.string.starter_dialog_starter_exit_with_code, exitCode)
        }

        builder.setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(false)
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View?, savedInstanceState: Bundle?) {
        dialog.setCanceledOnTouchOutside(false)
    }

    override fun onShow(dialog: AlertDialog?) {
        (dialog?.findViewById<View>(android.R.id.message) as TextView?)?.movementMethod = LinkMovementMethod.getInstance()
    }
}