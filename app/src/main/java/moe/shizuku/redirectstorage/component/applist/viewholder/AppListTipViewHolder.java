package moe.shizuku.redirectstorage.component.applist.viewholder;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class AppListTipViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) ->
            new AppListTipViewHolder(inflater.inflate(R.layout.item_list_tip, parent, false));

    public AppListTipViewHolder(View itemView) {
        super(itemView);

        TextView text = itemView.findViewById(android.R.id.text1);
        text.setText(HtmlCompat.fromHtml(text.getContext().getString(R.string.storage_permission_only_tip), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE));
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
