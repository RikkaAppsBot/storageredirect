package moe.shizuku.redirectstorage.component.settings;

import android.os.Bundle;

import moe.shizuku.preference.Preference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.api.LatestVersionInfoProvider;
import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.model.LatestVersionInfo;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.utils.ModuleUtils;
import moe.shizuku.redirectstorage.utils.Riru;
import rikka.html.text.HtmlCompat;

public class WorkModeSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.WORK_MODE";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_working_mode);

        Preference installPreference = findPreference("module_install");
        Preference enhancedModeWhy = findPreference("module_why");
        Preference statusPreference = findPreference("module_status");

        enhancedModeWhy.setOnPreferenceClickListener(preference -> {
            ModuleUtils.showWhy(requireContext());
            return true;
        });

        installPreference.setOnPreferenceClickListener(preference -> {
            ModuleUtils.showInstall(requireContext());
            return true;
        });

        ModuleStatus module = null;
        ModuleStatus riru = null;
        int serverVersion = 0;
        boolean activated = false;
        SRManager srm = SRManager.create();

        if (srm != null) {
            try {
                module = srm.getModuleStatus();
                riru = srm.getRiruStatus();
                serverVersion = srm.getVersion();
                activated = module.getVersionCode() > 0;
            } catch (Exception ignored) {
            }
            ((AppActivity) requireActivity()).setActionBarSubtitle(activated ? R.string.working_mode_enhance_mode : R.string.working_mode_basic_mode);
        }

        final int latestModuleVersionCode;
        final String latestModuleVersionName;

        if (activated) {
            final LatestVersionInfo latestVersionInfo = LatestVersionInfoProvider.get(requireContext());
            final LatestVersionInfo.Module latestModuleInfo = latestVersionInfo.getModule(serverVersion);
            latestModuleVersionCode = latestModuleInfo != null ? latestModuleInfo.versionCode : Integer.MIN_VALUE;
            latestModuleVersionName = latestModuleInfo != null ? latestModuleInfo.versionName : null;
        } else {
            latestModuleVersionCode = Integer.MIN_VALUE;
            latestModuleVersionName = null;
        }

        int moduleVersion = module != null ? module.getVersionCode() : -1;
        String status;
        boolean latest = moduleVersion >= latestModuleVersionCode;

        if (activated && !latest) {
            status = requireContext().getString(R.string.settings_enhanced_mode_status_upgrade,
                    Riru.versionName(requireContext(), riru),
                    ModuleUtils.versionName(requireContext(), module),
                    latestModuleVersionName);
        } else {
            status = requireContext().getString(R.string.settings_enhanced_mode_status,
                    Riru.versionName(requireContext(), riru),
                    ModuleUtils.versionName(requireContext(), module));
        }

        statusPreference.setSummary(HtmlCompat.fromHtml(status, HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE));

        statusPreference.setIcon(activated ? R.drawable.ic_module_ok_24 : R.drawable.ic_module_not_ok_24);
    }
}