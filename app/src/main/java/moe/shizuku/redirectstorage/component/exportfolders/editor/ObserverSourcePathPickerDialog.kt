package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import io.reactivex.Flowable
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.FilePickerDialog
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.FileBrowserUtils
import moe.shizuku.redirectstorage.utils.FileUtils
import rikka.html.text.HtmlCompat

class ObserverSourcePathPickerDialog : FilePickerDialog() {

    companion object {
        fun newInstance(appInfo: AppInfo, currentPath: String?): ObserverSourcePathPickerDialog {
            val args = Bundle()
            args.putParcelable(EXTRA_APP_INFO, appInfo)
            args.putString(EXTRA_CURRENT_PATH, currentPath)
            val dialog = ObserverSourcePathPickerDialog()
            dialog.arguments = args
            return dialog
        }

        private const val EXTRA_APP_INFO = "app_info"
    }
    
    private lateinit var appInfo: AppInfo
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appInfo = requireArguments().getParcelable(EXTRA_APP_INFO)!!
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        super.onBuildAlertDialog(builder, savedInstanceState)
        builder.setTitle(R.string.observer_info_edit_dialog_source_path)
        //builder.setNeutralButton(R.string.create_folder_dialog_title, null);
    }

    override fun onShow(dialog: AlertDialog) {
        super.onShow(dialog)
        neutralButton.setOnClickListener { v: View? -> startCreateFolderDialog() }
    }

    override fun enqueueRefresh(): List<ServerFile> {
        return Flowable.just(currentPath)
                .flatMap { currentPath: String? ->
                    val file = ServerFile.fromRedirectTarget(currentPath!!, appInfo.packageName, appInfo.userId)
                    val dirsGetter = FileBrowserUtils.getRedirectStorageDirs(file, FileBrowserUtils.SORT_BY_NAME)
                    val filesGetter = FileBrowserUtils.getRedirectStorageFiles(file, FileBrowserUtils.SORT_BY_NAME)
                    Flowable.concat(dirsGetter, filesGetter)
                }
                .filter { serverFile: ServerFile -> !FileUtils.isChildOf("Android", serverFile.relativePath) }
                .filter { serverFile: ServerFile -> serverFile.name?.startsWith(".") == false}
                .onErrorResumeNext { e: Throwable? -> Flowable.empty() }
                .blockingIterable().toList()
    }

    override fun onPick(currentPath: String) {
        super.onPick(currentPath)
        setSummary(HtmlCompat.fromHtml(requireContext().getString(R.string.dialog_create_new_link_choose_source_current_path) + currentPath))
    }

    override fun onPickFinished() {
        var currentPath = currentPath
        if (currentPath.startsWith("/")) currentPath = currentPath.replaceFirst("/".toRegex(), "")
        if (parentFragment is ExportFolderEditFragment) {
            (parentFragment as ExportFolderEditFragment?)!!.onSourcePathPick(currentPath)
        }
    }
}