package moe.shizuku.redirectstorage.component.filetransfer.fragment;

import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.AppFragment;
import moe.shizuku.redirectstorage.component.filetransfer.adapter.FileTransferAdapter;
import moe.shizuku.redirectstorage.model.AppInfo;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public class FileTransferFragment extends AppFragment {

    public static FileTransferFragment newInstance(@NonNull AppInfo config) {
        final Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, config);

        final FileTransferFragment fragment = new FileTransferFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static final String STATE_OBSERVERS = BuildConfig.APPLICATION_ID + ".state.OBSERVERS";

    private AppInfo mData;

    private FileTransferAdapter mAdapter;

    private ArrayList<FileTransferAdapter.ObserverInfoItem> mObservers;

    public FileTransferFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getArguments(), "Arguments should not be null.");

        mData = getArguments().getParcelable(EXTRA_DATA);

        if (savedInstanceState != null) {
            mObservers = savedInstanceState.getParcelableArrayList(STATE_OBSERVERS);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_OBSERVERS, mObservers);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = view.findViewById(android.R.id.list);

        if (mAdapter == null) {
            mAdapter = new FileTransferAdapter(requireContext());
        }
        recyclerView.setAdapter(mAdapter);

        if (mObservers == null) {
            mCompositeDisposable.add(
                    Single.fromCallable(() -> {
                        final SRManager srm = SRManager.create();

                        if (srm == null) {
                            throw new NullPointerException();
                        }

                        if (BuildConfig.DEBUG) {
                            try {
                                Thread.sleep(2000);
                            } catch (Exception ignored) {
                            }
                        }

                        final ArrayList<FileTransferAdapter.ObserverInfoItem> list = new ArrayList<>();
                        for (ObserverInfo observerInfo : new ArrayList<ObserverInfo>()/*mData.getConfiguration().getObservers()*/) {
                            list.add(new FileTransferAdapter.ObserverInfoItem(
                                    observerInfo,
                                    listFile(observerInfo.source)
                            ));
                        }

                        return list;
                    })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(list -> mAdapter.setData(mObservers = list), Throwable::printStackTrace)
            );
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.file_transfer, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_start_transfer) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<File> listFile(@NonNull String relativePath) throws DeadObjectException {
        final List<File> files = new ArrayList<>();
        final File extRoot = Environment.getExternalStorageDirectory();

        final List<ServerFile> dirs = FileBrowserUtils.getRedirectStorageDirs(
                ServerFile.fromStorageRoot(relativePath, mData.getUserId()),
                FileBrowserUtils.SORT_BY_NAME
        ).toList().blockingGet();

        for (ServerFile fileItem : dirs) {
            files.addAll(listFile(fileItem.getRelativePath()));
        }

        files.addAll(
                FileBrowserUtils.getRedirectStorageFiles(
                        ServerFile.fromStorageRoot(relativePath, mData.getUserId()),
                        FileBrowserUtils.SORT_BY_NAME)
                        .map(fileItem -> new File(extRoot, fileItem.getRelativePath()))
                        .toList().blockingGet()
        );

        return files;
    }

}
