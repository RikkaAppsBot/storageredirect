package moe.shizuku.redirectstorage.component.gallery.adapter

import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.component.gallery.model.PreviewModel
import moe.shizuku.redirectstorage.component.gallery.viewholder.PhotoPreviewGridViewHolder
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.ClassCreatorPool

class GalleryFolderItemPreviewGridAdapter(rv: RecyclerView)
    : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    init {
        creatorPool.putRule(PreviewModel::class.java,
                PhotoPreviewGridViewHolder.newCreator(rv))
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool()
    }
}