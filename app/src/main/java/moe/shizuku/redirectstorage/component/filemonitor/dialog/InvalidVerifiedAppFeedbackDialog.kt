package moe.shizuku.redirectstorage.component.filemonitor.dialog

import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PATH
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.api.GitHubApi
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.submitrule.GitHubLoginDialog
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import java.util.*

class InvalidVerifiedAppFeedbackDialog : AlertDialogFragment() {

    companion object {

        @JvmStatic
        fun newInstance(packageName: String, path: String): InvalidVerifiedAppFeedbackDialog {
            return InvalidVerifiedAppFeedbackDialog().also {
                it.arguments = bundleOf(EXTRA_PACKAGE_NAME to packageName, EXTRA_PATH to path)
            }
        }

        private val EXTRA_STATE = BuildConfig.APPLICATION_ID + "." +
                InvalidVerifiedAppFeedbackDialog::class.java.simpleName + ".extra.STATE"

    }

    private lateinit var mMessage: TextView
    private lateinit var mProgressBar: ProgressBar

    private var mState = false

    private lateinit var mPackageName: String
    private lateinit var mPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPackageName = arguments?.getString(EXTRA_PACKAGE_NAME)!!
        mPath = arguments?.getString(EXTRA_PATH)!!

        registerLocalBroadcastReceiver({ _, _ ->
            if (positiveButton != null) {
                setPositiveButtonEnabled(true)
            }
        }, IntentFilter(AppConstants.ACTION_GITHUB_AUTHORIZED))
        registerLocalBroadcastReceiver({ _, _ ->
            if (positiveButton != null) {
                positiveButton.postDelayed({ dismiss() }, 200)
            }
        }, IntentFilter(AppConstants.ACTION_GITHUB_AUTHORIZE_FAILED))

        if (savedInstanceState != null) {
            mState = savedInstanceState.getBoolean(EXTRA_STATE)
        }
    }

    override fun onGetContentViewLayoutResource(): Int {
        return R.layout.invalid_verified_app_feedback_dialog_content
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder,
                                    savedInstanceState: Bundle?) {
        builder.setTitle(R.string.action_report_nonstandard_behavior)
        builder.setPositiveButton(android.R.string.ok, null)
        builder.setNegativeButton(android.R.string.cancel, null)
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View,
                                      savedInstanceState: Bundle?) {
        mMessage = contentView.findViewById(android.R.id.message)
        mProgressBar = contentView.findViewById(android.R.id.progress)

        mMessage.text = getString(
                R.string.report_nonstandard_behavior_dialog_message,
                mPackageName, mPath
        )

        updateViewStates(mState)
    }

    override fun onShow(dialog: AlertDialog) {
        if (Settings.githubToken != null) {
            GitHubApi.loginWithoutVerifying(Settings.githubToken!!)
        }
        if (!GitHubApi.isLoginSuccess) {
            GitHubLoginDialog.newInstance().show(fragmentManager)
            positiveButton.isEnabled = false
        }

        positiveButton.setOnClickListener { startSubmit() }
    }

    private fun updateViewStates(newState: Boolean) {
        mState = newState
        mMessage.alpha = if (mState) 0.2f else 1f
        mProgressBar.visibility = if (mState) View.VISIBLE else View.GONE
        if (positiveButton != null) {
            setPositiveButtonEnabled(!mState)
        }
    }

    private fun startSubmit() = launch {
        updateViewStates(true)

        var submitted = false
        try {
            val repo = GitHubApi.getSRRulesRepository()
            val issues = GitHubApi.getIssues(repo)
            val newIssue = GitHubApi.buildInvalidVerifiedAppIssue(requireContext(), mPackageName, mPath)
            val existingIssue = issues.find { newIssue.title == it.title }
            if (existingIssue != null) {
                GitHubApi.createIssueComment(repo, existingIssue.number, newIssue.body)
            } else {
                GitHubApi.createIssue(repo, newIssue)
            }
            submitted = true
        } catch (e : Exception) {
            e.printStackTrace()
            if (activity != null && !activity!!.isFinishing) {
                Toast.makeText(
                        activity,
                        getString(R.string.toast_failed, Objects.toString(e, "unknown")),
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

        updateViewStates(false)
        if (submitted) {
            if (activity != null && !activity!!.isFinishing) {
                Toast.makeText(activity, R.string.toast_submit_successfully, Toast.LENGTH_LONG)
                        .show()
                activity?.onBackPressed()
            }
        }
    }

}
