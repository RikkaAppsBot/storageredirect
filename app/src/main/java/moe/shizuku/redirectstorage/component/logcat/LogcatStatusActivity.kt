package moe.shizuku.redirectstorage.component.logcat

import android.os.Bundle
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity

class LogcatStatusActivity : AppBarFragmentActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if ("moe.shizuku.redirectstorage.action.LOGCAT" != intent.action) {
            appBar?.setDisplayHomeAsUpEnabled(true)
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, LogcatStatusFragment())
                    .commit()
        }
    }
}