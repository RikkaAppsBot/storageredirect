package moe.shizuku.redirectstorage.component.logcat.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;

public class EmptyHistoryViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) ->
            new EmptyHistoryViewHolder(inflater.inflate(R.layout.logcat_history_empty_layout, parent, false));

    private EmptyHistoryViewHolder(View itemView) {
        super(itemView);
    }
}
