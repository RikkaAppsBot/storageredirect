package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import rikka.recyclerview.BaseViewHolder;

public class EditSourcePathViewHolder extends EditButtonViewHolder<ObserverInfoBuilder, EditSourcePathViewHolder.Listener> {

    public static final BaseViewHolder.Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditSourcePathViewHolder holder = new EditSourcePathViewHolder(
                inflater.inflate(R.layout.observer_info_edit_edit_button, parent, false));
        holder.editButton.setEnabled(parent.isEnabled());
        return holder;
    };

    public interface Listener {

        void onEditSourcePathClick();
    }

    private EditSourcePathViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public Drawable getIconDrawable() {
        return itemView.getResources().getDrawable(R.drawable.ic_folder_24dp, itemView.getContext().getTheme());
    }

    @Override
    public CharSequence getTitle() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_source_path);
    }

    @Override
    public CharSequence getHint() {
        return itemView.getResources().getString(R.string.observer_info_edit_dialog_source_path_hint);
    }

    @Override
    public void onClick() {
        getListener().onEditSourcePathClick();
    }

    @Override
    public void onBind() {
        super.onBind();
        editButton.setContentText(getData().getSource());
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        onBind();
    }
}
