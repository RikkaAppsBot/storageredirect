package moe.shizuku.redirectstorage.component.filemonitor.viewholder;

import android.content.Intent;
import android.view.View;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;

public class IOHistoryLoadMoreViewHolder extends BaseViewHolder<Long> {

    public static final Creator<Long> CREATOR = (inflater, parent) -> new IOHistoryLoadMoreViewHolder(inflater.inflate(R.layout.file_monitor_load_more, parent, false));

    private long lastBroadcastId = Long.MIN_VALUE;

    private IOHistoryLoadMoreViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind() {
        super.onBind();

        if (lastBroadcastId != getData()) {
            if (LocalBroadcastManager.getInstance(itemView.getContext())
                    .sendBroadcast(new Intent(AppConstants.ACTION_LOAD_MORE).putExtra(AppConstants.EXTRA_DATA, getData()))) {
                lastBroadcastId = getData();
            }
        }
    }
}
