package moe.shizuku.redirectstorage.component.restore.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import rikka.core.compat.CollectionsCompat;

public abstract class AbstractBackupModel implements Parcelable {

    public abstract int getVersion();

    public abstract List<RedirectPackageInfo> getRedirectPackage();

    public abstract List<ObserverInfo> getObservers();

    public abstract List<SimpleMountInfo> getSimpleMounts();

    public abstract String getDefaultRedirectTarget();

    public abstract List<MountDirsTemplate> getMountDirsTemplates();

    public abstract String getSRSettingsXML();

    public abstract Map<String, String> getPackageLabelMap();


    public List<RedirectPackageInfo> getRedirectPackagesForUser(int userId) {
        if (getRedirectPackage() == null) {
            return Collections.emptyList();
        }
        return CollectionsCompat.filterToList(getRedirectPackage(), info -> info.userId == userId);
    }

    public List<ObserverInfo> getObserversForUser(int userId) {
        if (getObservers() == null) {
            return Collections.emptyList();
        }
        return CollectionsCompat.filterToList(getObservers(), oi -> oi.userId == userId);
    }

    public List<SimpleMountInfo> getSimpleMountsForUser(int userId) {
        if (getSimpleMounts() == null) {
            return Collections.emptyList();
        }
        return CollectionsCompat.filterToList(getSimpleMounts(), smi -> smi.userId == userId);
    }

    public List<Integer> getUserIds() {
        Set<Integer> userIdSet = new HashSet<>();
        for (RedirectPackageInfo rpi : Optional.ofNullable(getRedirectPackage()).orElse(new ArrayList<>())) {
            userIdSet.add(rpi.userId);
        }
        return new ArrayList<>(userIdSet);
    }

    @NonNull
    @Override
    public String toString() {
        return "AbstractBackupModel{" +
                "version=" + getVersion() +
                ", redirectPackage.size=" + getRedirectPackage().size() +
                ", observers.size=" + getObservers().size() +
                ", defaultRedirectTarget='" + getDefaultRedirectTarget() + '\'' +
                ", defaultMountDirectories='" + getMountDirsTemplates() + '\'' +
                ", userCount='" + getUserIds() + '\'' +
                '}';
    }

    public interface Builder {

        Builder setServerVersion(int serverVersion);

        Builder setRedirectPackage(List<RedirectPackageInfo> redirectPackages);

        Builder setObservers(List<ObserverInfo> observers);

        Builder setSimpleMounts(List<SimpleMountInfo> simpleMounts);

        Builder setDefaultRedirectTarget(String defaultRedirectTarget);

        Builder setMountDirsTemplates(List<MountDirsTemplate> mountDirectoriesTemplates);

        Builder setSRSettingsXML(String xml);

        Builder setPackageLabelMap(Map<String, String> map);

        AbstractBackupModel build();

    }

    /**
     * Used for selecting what parts will be restored from backup files.
     */
    public static class ContentSelector implements Parcelable {

        public int selectedAppConfigurationsUserId = -1;

        public boolean selectedCommonSettings = true;

        public ContentSelector() {

        }

        protected ContentSelector(Parcel in) {
            selectedAppConfigurationsUserId = in.readInt();
            selectedCommonSettings = in.readByte() != 0;
        }

        public static final Creator<ContentSelector> CREATOR = new Creator<ContentSelector>() {
            @Override
            public ContentSelector createFromParcel(Parcel in) {
                return new ContentSelector(in);
            }

            @Override
            public ContentSelector[] newArray(int size) {
                return new ContentSelector[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(selectedAppConfigurationsUserId);
            dest.writeByte((byte) (selectedCommonSettings ? 1 : 0));
        }

    }

    public static AbstractBackupModel fromJson(@NonNull String json) {
        final JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
        final int version = jsonObject.getAsJsonPrimitive("version").getAsInt();
        final Gson gson = new Gson();
        switch (version) {
            case 6:
                return gson.fromJson(json, BackupModel.class);
            case 5: {
                BackupModel model = gson.fromJson(json, BackupModel.class);
                if (model.observers != null) {
                    for (ObserverInfo oi : model.observers) {
                        if (!TextUtils.isEmpty(oi.summary)) {
                            oi.summary = "@" + oi.summary;
                        }
                    }
                }
                return model;
            }
            case 4:
            case 3: {
                BackupModel model = gson.fromJson(json, BackupModel.class);
                boolean defaultIsEmpty = false;
                if (model.mountDirectoriesTemplates != null) {
                    for (MountDirsTemplate template : new ArrayList<>(model.mountDirectoriesTemplates)) {
                        if (MountDirsTemplate.DEFAULT_ID.equals(template.id)) {
                            defaultIsEmpty = template.list.isEmpty();

                            // remove empty default
                            if (defaultIsEmpty) {
                                model.mountDirectoriesTemplates.remove(template);
                            }
                            break;
                        }
                    }
                }

                if (model.redirectPackage != null) {
                    for (RedirectPackageInfo info : model.redirectPackage) {
                        if (info.mountDirsConfig != null)
                            continue;

                        int type;
                        if (info.mountDirs != null) {
                            type = info.mountDirs.isEmpty() ? MountDirsConfig.FLAG_NONE : MountDirsConfig.FLAG_ALLOW_CUSTOM;
                        } else {
                            if ((Objects.equals(info.mountDirsTemplateId, MountDirsTemplate.DEFAULT_ID) && defaultIsEmpty)
                                    || info.mountDirsTemplateId == null) {
                                type = MountDirsConfig.FLAG_NONE;
                                info.mountDirsTemplateId = null;
                            } else {
                                type = MountDirsConfig.FLAG_ALLOW_TEMPLATES;
                            }
                        }

                        info.mountDirsConfig = new MountDirsConfig();
                        info.mountDirsConfig.flags = type;
                        if (info.mountDirs != null) {
                            info.mountDirsConfig.customDirs = new HashSet<>(info.mountDirs);
                        }
                        if (info.mountDirsTemplateId != null) {
                            info.mountDirsConfig.templatesIds = new HashSet<>();
                            info.mountDirsConfig.templatesIds.add(info.mountDirsTemplateId);
                        }
                    }
                }
                return model;
            }
            case 2:
                BackupModel modelV3 = gson.fromJson(json, BackupModel.class);
                JsonArray array = jsonObject.getAsJsonArray("default_mount_dirs");
                if (array != null) {
                    ArrayList<String> list = new ArrayList<>();
                    for (JsonElement e : array) {
                        list.add(e.getAsString());
                    }
                    //noinspection ArraysAsListWithZeroOrOneArgument
                    modelV3.mountDirectoriesTemplates = Arrays.asList(MountDirsTemplate.createWithId(MountDirsTemplate.DEFAULT_ID, list));
                }
                return modelV3;
            default:
                throw new IllegalArgumentException("invalid json");
        }
    }

}
