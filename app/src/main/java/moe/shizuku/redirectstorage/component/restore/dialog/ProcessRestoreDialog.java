package moe.shizuku.redirectstorage.component.restore.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;

public class ProcessRestoreDialog extends DialogFragment {

    public static final String ACTION_UPDATE_RESTORE_DIALOG_UI = BuildConfig.APPLICATION_ID +
            ".action.UPDATE_RESTORE_DIALOG_UI";
    public static final String EXTRA_WHAT = BuildConfig.APPLICATION_ID +
            ".extra.WHAT";
    public static final String EXTRA_PROGRESS = BuildConfig.APPLICATION_ID +
            ".extra.PROGRESS";
    public static final String EXTRA_MAX = BuildConfig.APPLICATION_ID +
            ".extra.MAX";
    public static final String EXTRA_MESSAGE = BuildConfig.APPLICATION_ID +
            ".extra.FORMAT";

    public static final int MSG_FAILED_TO_CONNECT_SERVICE = -1;
    public static final int MSG_UPDATE_PROGRESS = 0;
    public static final int MSG_FINISHED = 1;

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || !ACTION_UPDATE_RESTORE_DIALOG_UI.equals(intent.getAction())) {
                return;
            }
            handleMessage(intent);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver, new IntentFilter(ACTION_UPDATE_RESTORE_DIALOG_UI));
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(mMessageReceiver);

        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ProgressDialog dialog = new ProgressDialog(requireActivity());
        dialog.setMessage(getString(R.string.restore_process));
        dialog.setCancelable(false);
        dialog.setIndeterminate(false);
        dialog.setMax(1);
        dialog.setProgress(0);
        return dialog;
    }

    @Override
    public ProgressDialog getDialog() {
        return (ProgressDialog) super.getDialog();
    }

    private void setFailedMessage(CharSequence text) {
        getDialog().setProgress(0);
        getDialog().setMessage(getString(R.string.restore_failed_format, text));
        getDialog().setButton(DialogInterface.BUTTON_NEGATIVE, getString(android.R.string.cancel), (DialogInterface.OnClickListener) null);
    }

    private void handleMessage(Intent intent) {
        switch (intent.getIntExtra(EXTRA_WHAT, 233)) {
            case MSG_FAILED_TO_CONNECT_SERVICE: {
                setFailedMessage(getString(R.string.toast_unable_connect_server));
                break;
            }
            case MSG_UPDATE_PROGRESS: {
                final int progress = intent.getIntExtra(EXTRA_PROGRESS, 0);
                final int max = intent.getIntExtra(EXTRA_MAX, 0);
                final String message = intent.getStringExtra(EXTRA_MESSAGE);
                getDialog().setMax(max);
                getDialog().setProgress(progress);
                getDialog().setMessage(message);
                break;
            }
            case MSG_FINISHED: {
                requireActivity().finish();
                break;
            }
        }
    }

}
