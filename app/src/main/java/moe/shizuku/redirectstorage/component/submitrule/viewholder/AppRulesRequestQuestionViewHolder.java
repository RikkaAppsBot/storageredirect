package moe.shizuku.redirectstorage.component.submitrule.viewholder;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Method;
import java.util.List;

import moe.shizuku.redirectstorage.component.submitrule.adapter.AppRulesRequestAdapter;
import rikka.recyclerview.BaseViewHolder;

public abstract class AppRulesRequestQuestionViewHolder<T> extends BaseViewHolder<T> {

    protected ImageView icon;
    protected TextView title;
    protected TextView summary;
    protected RadioButton yesBtn, noBtn, dontKnowBtn;

    public static final Object PAYLOAD_HIGHLIGHT_ONCE = new Object();

    public AppRulesRequestQuestionViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);
        yesBtn = itemView.findViewById(android.R.id.button1);
        noBtn = itemView.findViewById(android.R.id.button2);
        dontKnowBtn = itemView.findViewById(android.R.id.button3);

        yesBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked && !Boolean.TRUE.equals(getAnswer())) {
                onAnswerSelected(true);
            }
        });
        noBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked && !Boolean.FALSE.equals(getAnswer())) {
                onAnswerSelected(false);
            }
        });
        dontKnowBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked && getAnswer() != null) {
                onAnswerSelected(null);
            }
        });
    }

    public abstract void onAnswerSelected(@Nullable Boolean answer);

    @Nullable
    public abstract Boolean getAnswer();

    @CallSuper
    @Override
    public void onBind() {
        syncEnabledState();
    }

    @SuppressLint("PrivateApi")
    @Override
    public void onBind(@NonNull List<Object> payloads) {
        syncEnabledState();
        for (Object payload : payloads) {
            if (PAYLOAD_HIGHLIGHT_ONCE.equals(payload)) {
                if (getAnswer() == null) {
                    Drawable background = itemView.getBackground();
                    if (background instanceof RippleDrawable) {
                        try {
                            final RippleDrawable rd = (RippleDrawable) background;
                            final Method setRippleActive = RippleDrawable.class
                                    .getDeclaredMethod("setRippleActive", boolean.class);

                            setRippleActive.setAccessible(true);
                            setRippleActive.invoke(rd, true);

                            itemView.postDelayed(() -> {
                                try {
                                    setRippleActive.invoke(rd, false);
                                } catch (Exception ignored) {
                                }
                            }, 500);
                        } catch (Exception ignored) {
                        }
                    }
                }
            }
        }
    }

    @Override
    public AppRulesRequestAdapter getAdapter() {
        return (AppRulesRequestAdapter) super.getAdapter();
    }

    private void syncEnabledState() {
        if (getAnswer() == null) {
            yesBtn.setChecked(false);
            noBtn.setChecked(false);
            dontKnowBtn.setChecked(true);
        } else {
            yesBtn.setChecked(getAnswer());
            noBtn.setChecked(!getAnswer());
            dontKnowBtn.setChecked(false);
        }
    }
}