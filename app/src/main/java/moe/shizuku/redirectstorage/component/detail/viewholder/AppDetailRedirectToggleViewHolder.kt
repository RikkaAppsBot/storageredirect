package moe.shizuku.redirectstorage.component.detail.viewholder

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.detail.adapter.AppDetailAdapter
import moe.shizuku.redirectstorage.component.payment.PurchaseFragment
import moe.shizuku.redirectstorage.component.settings.SettingsActivity
import moe.shizuku.redirectstorage.component.settings.WorkModeSettingsFragment
import moe.shizuku.redirectstorage.event.RemoteRequestHandler
import moe.shizuku.redirectstorage.event.ToastErrorHandler
import moe.shizuku.redirectstorage.ktx.applyCountdown
import moe.shizuku.redirectstorage.license.License
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.SizeTagHandler
import moe.shizuku.redirectstorage.viewholder.CheckableViewHolder
import rikka.core.util.ResourceUtils
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailRedirectToggleViewHolder(itemView: View) : CheckableViewHolder<AppInfo>(itemView), OnClickListener {

    companion object {
        val CREATOR = Creator<AppInfo> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailRedirectToggleViewHolder(inflater.inflate(R.layout.detail_toggle, parent, false)) }
    }

    init {
        title.setText(R.string.detail_enable_redirect)
        icon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_redirect_24dp))
    }

    override fun getAdapter(): AppDetailAdapter {
        return super.getAdapter() as AppDetailAdapter
    }

    private fun toggle(context: Context, skipNonRegularDialog: Boolean, skipSharedUserIdDialog: Boolean) {
        if (!data.isRedirectEnabled && !skipNonRegularDialog && (!data.isRegularApp || data.packageName == "com.android.systemui")) {
            val enhancedMode = try {
                SRManager.create()?.moduleStatus?.versionCode ?: 0 > 0
            } catch (e: Throwable) {
                false
            }
            if (enhancedMode) {
                val countdownSecond = 3
                val dialog = AlertDialog.Builder(context)
                        .setTitle(R.string.dialog_not_regular_app_title)
                        .setMessage(context.getString(R.string.dialog_not_regular_app_message, "storage-redirect-client://help/enhanced_troubleshooting").toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                        .setPositiveButton(context.getString(R.string.item_summary, context.getString(R.string.button_iam_sure), countdownSecond.toString()).toHtml()) { _, _ ->
                            toggle(context, true, skipSharedUserIdDialog)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .create()

                dialog.setOnShowListener {
                    dialog.findViewById<TextView>(android.R.id.message)!!.movementMethod = LinkMovementMethod.getInstance()
                    dialog.applyCountdown(AlertDialog.BUTTON_POSITIVE, countdownSecond, context.getString(R.string.button_iam_sure))
                }
                dialog.show()
            } else {
                AlertDialog.Builder(context)
                        .setTitle(R.string.dialog_not_regular_app_basic_title)
                        .setMessage(context.getString(R.string.dialog_not_regular_app_basic_message).toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                        .setPositiveButton(R.string.home_working_mode) { _, _ ->
                            val intent = Intent()
                            intent.setClass(context, SettingsActivity::class.java)
                            intent.action = WorkModeSettingsFragment.ACTION
                            context.startActivity(intent)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
            }
            return
        }

        if (!data.isRedirectEnabled && !skipSharedUserIdDialog && data.sharedUserId != null) {
            val countdownSecond = 3
            val dialog = AlertDialog.Builder(context)
                    .setTitle(R.string.dialog_app_group_title)
                    .setMessage(context.getString(R.string.dialog_app_group_message, "storage-redirect-client://help/shared_user_id", "storage-redirect-client://help/enhanced_troubleshooting").toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                    .setPositiveButton(context.getString(R.string.item_summary, context.getString(R.string.button_iam_sure), countdownSecond.toString()).toHtml()) { _, _ ->
                        toggle(context, skipNonRegularDialog, true)
                    }
                    .setNeutralButton(R.string.review_all_apps_in_app_group, null)
                    .setNegativeButton(android.R.string.cancel, null)
                    .create()

            dialog.setOnShowListener {
                dialog.findViewById<TextView>(android.R.id.message)!!.movementMethod = LinkMovementMethod.getInstance()
                dialog.applyCountdown(AlertDialog.BUTTON_POSITIVE, countdownSecond, context.getString(R.string.button_iam_sure))
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener {
                    (listener as AppDetailHeaderSharedUserIdViewHolder.Listener).onSharedUserIdClick()
                }
            }
            dialog.show()
            return
        }

        val data = data
        val enabled = !data.isRedirectEnabled

        if (RemoteRequestHandler.setEnabled(data.packageName, data.sharedUserId, data.userId, enabled, NotPaidHandler(context))) {
            if (enabled && Settings.preferences.getBoolean("first_redirect_tip", true)) {
                val countdownSecond = 5
                val dialog = AlertDialog.Builder(context)
                        .setTitle(R.string.dialog_first_redirect_title)
                        .setMessage(HtmlCompat.fromHtml(context.getString(R.string.dialog_first_redirect_message),
                                HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM or HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE,
                                null, SizeTagHandler.getInstance()))
                        .setPositiveButton(android.R.string.ok, null)
                        .setNeutralButton(context.getString(R.string.item_summary, context.getString(R.string.dont_show_again), countdownSecond.toString()).toHtml()) { _: DialogInterface?, _: Int -> Settings.preferences.edit().putBoolean("first_redirect_tip", false).apply() }
                        .setCancelable(false)
                        .create()

                dialog.setOnShowListener {
                    dialog.applyCountdown(AlertDialog.BUTTON_NEUTRAL, countdownSecond, context.getString(R.string.dont_show_again))
                }
                dialog.setCanceledOnTouchOutside(false)
                dialog.show()
                return
            }
        }
    }

    override fun onClick(view: View) {
        toggle(view.context, false, false)
    }

    override fun isChecked(): Boolean {
        return data.isRedirectEnabled
    }

    override fun onBind() {
        super.onBind()
        val context = itemView.context
        if (!data.isRegularApp) {
            summary.setText(R.string.detail_summary_system_app_non_regular)
            summary.setTextColor(ResourceUtils.resolveColor(context.theme, R.attr.colorWarning))
            summary.typeface = Typeface.DEFAULT_BOLD
        } else if (data.isSystemApp) {
            summary.setText(R.string.detail_summary_system_app)
            summary.typeface = Typeface.DEFAULT_BOLD
        } else {
            summary.visibility = View.GONE
        }
        itemView.isEnabled = data.isRedirectEnabled || adapter.appInfo.writePermissionOrSharedUserId
    }

    inner class NotPaidHandler(context: Context) : ToastErrorHandler(context) {

        override fun invoke(throwable: Throwable) {
            if (throwable.message == "") {
                if (License.checkLocal()) {
                    AlertDialog.Builder(context)
                            .setTitle(R.string.dialog_purchase_verification_title)
                            .setMessage(context.getString(R.string.dialog_purchase_verification_message).toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                            .setPositiveButton(android.R.string.ok, null)
                            .setCancelable(false)
                            .show()
                } else {
                    AlertDialog.Builder(context)
                            .setTitle(R.string.dialog_trial_title)
                            .setMessage(R.string.dialog_trial_message)
                            .setPositiveButton(R.string.dialog_trial_button_buy) { _: DialogInterface?, _: Int -> PurchaseFragment.startActivity(context) }
                            .setNegativeButton(android.R.string.cancel, null)
                            .setCancelable(false)
                            .show()
                }
            } else {
                super.invoke(throwable)
            }
        }
    }
}