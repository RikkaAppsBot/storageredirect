package moe.shizuku.redirectstorage.component.restore

import android.content.Context
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.app.BaseViewModel
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel
import moe.shizuku.redirectstorage.utils.BackupUtils
import moe.shizuku.redirectstorage.viewmodel.Resource

class RestoreWizardViewModel : BaseViewModel() {

    val backupModel = MutableLiveData<Resource<AbstractBackupModel>>()

    fun startLoadBackup(context: Context, uri: Uri) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val stream = context.contentResolver
                    .openInputStream(uri) ?: throw NullPointerException("can't openInputStream")
            val model = AbstractBackupModel.fromJson(BackupUtils.readString(stream))
            backupModel.postValue(Resource.success(model))
        } catch (e: CancellationException) {

        } catch (e: Throwable) {
            backupModel.postValue(Resource.error(e, null))
        }
    }
}