package moe.shizuku.redirectstorage.component.detail.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import moe.shizuku.redirectstorage.ktx.isFuseUsed
import moe.shizuku.redirectstorage.ktx.isSdcardfsUsed
import moe.shizuku.redirectstorage.utils.OmissionStringBuilder
import moe.shizuku.redirectstorage.utils.WordsHelper
import moe.shizuku.redirectstorage.viewholder.SimpleTwoButtonViewHolder
import rikka.html.text.HtmlCompat
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailAccessibleFoldersViewHolder(itemView: View) : SimpleTwoButtonViewHolder<AppDetailViewModel>(itemView) {

    private val text1 = itemView.findViewById<TextView>(android.R.id.text1)

    interface Listener {
        fun onAccessibleFoldersClick()
    }

    override fun onPrimaryButtonClick(view: View) {
        (adapter.listener as Listener).onAccessibleFoldersClick()
    }

    override fun onSecondaryButtonClick(view: View) {}

    private inline val appInfo get() = data.appInfo.value!!.data!!
    private inline val mountDirs get() = data.mountDirs.value!!.data!!
    private inline val redirectInfo get() = appInfo.redirectInfo

    override fun onBind() {
        updateSummary()

        itemView.isEnabled = appInfo.isRedirectEnabled
        button1.isEnabled = appInfo.isRedirectEnabled
        button2.isEnabled = appInfo.isRedirectEnabled

        // TODO
        button2.visibility = View.GONE
        divider.visibility = View.GONE
    }

    override fun onBind(payloads: List<Any?>) {
        updateSummary()
    }

    private fun generateFoldersString(maxDisplayCount: Int = 3): String {
        val comma = WordsHelper.getComma(context)
        val semicolon = WordsHelper.getSemicolon(context)
        val osb = OmissionStringBuilder(context).maxCount(maxDisplayCount)
        val dirs = mountDirs

        dirs.forEachIndexed { index, path ->
            osb.append("<font face=\"$FONT_FACE\">$path</font>", if (index == dirs.size - 1) semicolon else comma)
        }

        val map = LinkedHashMap<Pair<String, CharSequence>, MutableList<String>?>()
        if (!isFuseUsed || isSdcardfsUsed) {
            data.simpleMounts.value?.data
                    ?.filter { it.local && it.enabled && it.targetPackage == redirectInfo.packageName }
                    ?.forEach { it ->
                        val key = it.sourcePackage to it.sourceLabel!!
                        if (map[key] == null) {
                            map[key] = ArrayList()
                        }
                        map[key]!!.addAll(it.paths)
                    }
        }

        map.values.forEach { it?.sort() }

        for ((key, paths) in map) {
            val label = key.second
            paths!!.forEachIndexed { index, path ->
                when (index) {
                    0 -> {
                        osb.append(context.getString(R.string.detail_accessible_folders_summary_other_app_format, "<font face=\"$FONT_FACE\">$path</font>", label))
                    }
                    paths.size - 1 -> {
                        osb.append("<font face=\"$FONT_FACE\">$path</font>", semicolon)
                    }
                    else -> {
                        osb.append("<font face=\"$FONT_FACE\">$path</font>", semicolon)
                    }
                }
            }
        }

        return osb.build()
    }

    private fun updateSummary() {
        summary.text = HtmlCompat.fromHtml(generateFoldersString())

        val config = data.recommendation.value?.data?.config
        val rule = config?.accessibleFolder?.text()

        if (rule != null) {
            text1.text = HtmlCompat.fromHtml(rule)
            text1.visibility = View.VISIBLE
        } else {
            text1.visibility = View.GONE
        }
    }

    companion object {

        private const val FONT_FACE = "sans-serif"

        val CREATOR = Creator<AppDetailViewModel> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailAccessibleFoldersViewHolder(inflater.inflate(R.layout.detail_accessible_folders, parent, false)) }
    }
}