package moe.shizuku.redirectstorage.component.home.viewholder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.api.LatestVersionInfoProvider;
import moe.shizuku.redirectstorage.component.home.model.HomeStatus;
import moe.shizuku.redirectstorage.component.settings.SettingsActivity;
import moe.shizuku.redirectstorage.component.settings.WorkModeSettingsFragment;
import moe.shizuku.redirectstorage.model.LatestVersionInfo;
import moe.shizuku.redirectstorage.utils.ModuleUtils;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class HomeWorkingModeViewHolder extends BaseViewHolder<HomeStatus> implements View.OnClickListener {

    public static final Creator<HomeStatus> CREATOR = (inflater, parent) -> new HomeWorkingModeViewHolder(inflater.inflate(R.layout.home_item_primary, parent, false));

    private ImageView icon;
    private TextView title;
    private TextView summary;
    private ImageButton button;

    public HomeWorkingModeViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        summary = itemView.findViewById(android.R.id.summary);
        button = itemView.findViewById(android.R.id.button1);

        button.setClickable(false);
        button.setFocusable(false);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onBind() {
        final Context context = itemView.getContext();
        final HomeStatus status = getData();

        title.setText(R.string.home_working_mode);
        icon.setImageDrawable(context.getDrawable(R.drawable.ic_home_module_24dp));

        boolean widget = false;

        if (status != null && status.isLoaded()) {
            final LatestVersionInfo latestVersionInfo = LatestVersionInfoProvider.get(context);
            final LatestVersionInfo.Module module = latestVersionInfo.getModule(status.serverVersion);
            final int latestModuleVersionCode = module != null ? module.versionCode : 0;
            final String latestModuleVersionName = module != null ? module.versionName : null;

            if (status.enhanceModule) {
                if (status.enhanceModuleVersion < latestModuleVersionCode) {
                    widget = true;
                    button.setImageDrawable(context.getDrawable(R.drawable.ic_home_new_24dp));
                    summary.setText(context.getString(R.string.working_mode_enhance_mode_upgrade_format, context.getString(R.string.working_mode_enhance_mode_long), ModuleUtils.versionName(context, status.enhanceModuleStatus), latestModuleVersionName));
                } else
                    summary.setText(context.getString(R.string.working_mode_enhance_mode_format, context.getString(R.string.working_mode_enhance_mode_long), ModuleUtils.versionName(context, status.enhanceModuleStatus)));
            } else {
                widget = true;
                button.setImageDrawable(context.getDrawable(R.drawable.ic_home_new_24dp));
                summary.setText(HtmlCompat.fromHtml(context.getString(R.string.working_mode_basic_mode_long)));
            }
            summary.setVisibility(View.VISIBLE);
            itemView.setEnabled(true);
        } else {
            summary.setVisibility(View.GONE);
            itemView.setEnabled(false);
        }

        button.setBackground(null);
        button.setVisibility(widget ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View view) {
        final Context context = view.getContext();
        final Intent intent = new Intent();
        intent.setClass(context, SettingsActivity.class);
        intent.setAction(WorkModeSettingsFragment.ACTION);
        context.startActivity(intent);
    }
}
