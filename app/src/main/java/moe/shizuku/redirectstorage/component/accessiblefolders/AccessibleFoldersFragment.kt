package moe.shizuku.redirectstorage.component.accessiblefolders

import android.os.Bundle
import android.util.ArraySet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.SimpleMountInfo
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.accessiblefolders.adapter.AccessibleFoldersAdapter
import moe.shizuku.redirectstorage.component.accessiblefolders.dialog.MountDirsPickerDialog
import moe.shizuku.redirectstorage.component.detail.appDetailViewModel
import moe.shizuku.redirectstorage.component.detail.dialog.ViewMountDirsListDialog
import moe.shizuku.redirectstorage.dialog.picker.MultiFilesPickerDialog
import moe.shizuku.redirectstorage.event.RemoteRequestHandler
import moe.shizuku.redirectstorage.event.ToastErrorHandler
import moe.shizuku.redirectstorage.model.AppSimpleMountInfo
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.core.ktx.unsafeLazy
import rikka.material.app.AppBarOwner
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView

class AccessibleFoldersFragment : AppFragment(), AccessibleFoldersAdapter.Listener, MultiFilesPickerDialog.Callback, SimpleMountEditFragment.Listener {

    companion object {

        fun newInstance(): AccessibleFoldersFragment {
            return AccessibleFoldersFragment()
        }
    }

    private val viewModel by appDetailViewModel()
    private val appInfo by unsafeLazy {
        viewModel.appInfo.value!!.data!!
    }
    private val packageName by lazy { appInfo.packageName }
    private val sharedUserId by lazy { appInfo.sharedUserId }
    private val userId by lazy { appInfo.userId }

    private val adapter by lazy {
        AccessibleFoldersAdapter().apply {
            listener = this@AccessibleFoldersFragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.appInfo.observe(viewLifecycleOwner) { res ->
            if (res.status == Status.SUCCESS) {
                (activity as? AppBarOwner)?.appBar?.subtitle = viewModel.appInfo.value?.data?.getSubtitle(requireContext())

                adapter.reloadItems(requireContext(), viewModel)

                viewModel.simpleMounts.observe(viewLifecycleOwner) {
                    if (it.status == Status.SUCCESS) {
                        adapter.reloadItems(requireContext(), viewModel)
                    }
                }
                viewModel.mountDirsConfig.observe(viewLifecycleOwner) {
                    if (it.status == Status.SUCCESS) {
                        adapter.reloadItems(requireContext(), viewModel)
                    }
                }
                viewModel.mountDirsTemplates.observe(viewLifecycleOwner) {
                    if (it.status == Status.SUCCESS) {
                        adapter.reloadItems(requireContext(), viewModel)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.appbar_container_recycler, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val recyclerView = view as BorderRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(VerticalPaddingDecoration(context).apply { setAllowTop(false) })
        recyclerView.adapter = adapter
        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            if (activity != null) {
                (activity as AppBarFragmentActivity).appBar?.setRaised(!top)
            }
        }
        recyclerView.fixEdgeEffect()
    }

    override fun onViewTemplateClick(template: MountDirsTemplate) {
        ViewMountDirsListDialog.newInstance(template.list)
                .show(parentFragmentManager)
    }

    override fun onToggleTemplateClick(template: MountDirsTemplate, newState: Boolean) {
        val mountDirsConfig = appInfo.redirectInfo.mountDirsConfig
        mountDirsConfig.isTemplatesAllowed = true
        if (newState) {
            if (mountDirsConfig.templatesIds == null) {
                mountDirsConfig.templatesIds = ArraySet()
            }
            mountDirsConfig.templatesIds!!.add(template.id)
        } else {
            mountDirsConfig.templatesIds?.remove(template.id)
        }
        RemoteRequestHandler.setMountDirsForPackage(mountDirsConfig, packageName, sharedUserId, userId)
    }

    override fun onViewCustomClick() {
        val mountDirsConfig = appInfo.redirectInfo.mountDirsConfig
        MountDirsPickerDialog.newInstance(userId, packageName, mountDirsConfig.customDirs)
                .show(childFragmentManager, "mount_dir_picker")
    }

    override fun onToggleCustomClick(newState: Boolean) {
        val mountDirsConfig = appInfo.redirectInfo.mountDirsConfig
        mountDirsConfig.isCustomAllowed = newState
        RemoteRequestHandler.setMountDirsForPackage(mountDirsConfig, packageName, sharedUserId, userId)
    }

    override fun onMultiFilesPickerDialogResult(tag: String?, list: List<ServerFile>) {
        val mountDirsConfig = appInfo.redirectInfo.mountDirsConfig
        val previousEmpty = mountDirsConfig.customDirs.isNullOrEmpty()
        mountDirsConfig.customDirs = list.map { it.relativePath }.toHashSet()
        if (mountDirsConfig.customDirs!!.isNotEmpty()) {
            // enable current not empty
            mountDirsConfig.isCustomAllowed = true
        } else if (!previousEmpty && mountDirsConfig.customDirs!!.isEmpty()) {
            // disable if not empty -> empty
            mountDirsConfig.isCustomAllowed = false
        }
        RemoteRequestHandler.setMountDirsForPackage(mountDirsConfig, packageName, sharedUserId, userId)
    }

    override fun onAddSimpleMountClick() {
        SimpleMountEditFragment.newCreateInstance(appInfo).show(childFragmentManager)
    }

    override fun onViewSimpleMountClick(info: AppSimpleMountInfo) {
        if (info.online && !info.enabled) {
            SimpleMountEditFragment.newViewOnlineInstance(appInfo, info.toSimpleMountInfo())
        } else {
            SimpleMountEditFragment.newEditInstance(appInfo, info.toSimpleMountInfo())
        }.show(childFragmentManager)
    }

    override fun onDeleteSimpleMountClick(info: AppSimpleMountInfo) {
        onRequestDeleteSimpleMount(info.toSimpleMountInfo())
    }

    override fun onToggleSimpleMountClick(oldInfo: AppSimpleMountInfo) {
        val old = oldInfo.toSimpleMountInfo()
        val smi = oldInfo.toSimpleMountInfo()
        smi.enabled = !smi.enabled

        onRequestUpdateSimpleMount(smi, old)
    }

    override fun onRequestAddSimpleMount(info: SimpleMountInfo) {
        viewModel.simpleMounts.value?.data?.forEach {
            if (it.toSimpleMountInfo().equalsContent(info)) {
                //requireContext().makeToast(R.string.exists)
                return
            }
        }

        RemoteRequestHandler.addSimpleMount(info, SRManager.FLAG_KILL_PACKAGE or SRManager.FLAG_FORCE_ENABLE_REDIRECT, ToastErrorHandler(requireContext()))
    }

    override fun onRequestUpdateSimpleMount(new: SimpleMountInfo, old: SimpleMountInfo) {
        RemoteRequestHandler.updateSimpleMount(new, old, SRManager.FLAG_KILL_PACKAGE or SRManager.FLAG_FORCE_ENABLE_REDIRECT, ToastErrorHandler(requireContext()))
    }

    override fun onRequestDeleteSimpleMount(info: SimpleMountInfo) {
        RemoteRequestHandler.removeSimpleMount(info, ToastErrorHandler(requireContext()))
    }
}