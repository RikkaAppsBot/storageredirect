package moe.shizuku.redirectstorage.component.sharehelper.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.AppFragment;
import moe.shizuku.redirectstorage.component.settings.SettingsActivity;
import moe.shizuku.redirectstorage.component.sharehelper.adapter.ShareHelperListPreferencesAdapter;
import moe.shizuku.redirectstorage.utils.ShareHelperListPreferences;
import moe.shizuku.redirectstorage.utils.ViewUtils;
import rikka.widget.borderview.BorderRecyclerView;

public class ShareHelperListPreferencesFragment extends AppFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage" +
            ".settings.DIRECT_SHARE_PREFERENCES";

    private ShareHelperListPreferences mPreferences;

    private BorderRecyclerView mListView;
    private LinearLayoutManager mLayoutManager;
    private ShareHelperListPreferencesAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences = new ShareHelperListPreferences(requireContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.preference_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mListView = view.findViewById(android.R.id.list);
        ViewUtils.setPaddingVertical(mListView,
                getResources().getDimensionPixelSize(R.dimen.padding_8dp));

        if (mLayoutManager == null) {
            mLayoutManager = new LinearLayoutManager(requireContext());
        }
        if (mAdapter == null) {
            mAdapter = new ShareHelperListPreferencesAdapter(requireContext(), mPreferences, mListView);
        }

        mListView.getBorderViewDelegate().setBorderVisibilityChangedListener((top, oldTop, bottom, oldBottom) -> ((SettingsActivity) getActivity()).getAppBar().setRaised(!top));

        mListView.setLayoutManager(mLayoutManager);
        mListView.setHasFixedSize(true);
        mListView.setAdapter(mAdapter);

        mCompositeDisposable.add(mPreferences.getInstalledApps()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe(data -> mAdapter.updateData(data)));
    }
}
