package moe.shizuku.redirectstorage.component.settings;

import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import moe.shizuku.preference.ListPreference;
import moe.shizuku.preference.Preference;
import moe.shizuku.preference.SwitchPreference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.utils.ThemeHelper;
import rikka.core.util.ResourceUtils;
import rikka.html.text.HtmlCompat;
import rikka.material.app.LocaleDelegate;

public class UserInterfaceSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.USER_INTERFACE";

    public static final String KEY_LANGUAGE = Settings.LANGUAGE;
    public static final String KEY_NIGHT_MODE = Settings.NIGHT_MODE;
    public static final String KEY_LIGHT_THEME = ThemeHelper.KEY_LIGHT_THEME;
    public static final String KEY_BLACK_NIGHT_THEME = ThemeHelper.KEY_BLACK_NIGHT_THEME;

    private ListPreference languagePreference;
    private ListPreference lightThemePreference;
    private Preference nightModePreference;
    private SwitchPreference blackNightThemePreference;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_user_interface);

        languagePreference = (ListPreference) findPreference(KEY_LANGUAGE);
        lightThemePreference = (ListPreference) findPreference(KEY_LIGHT_THEME);
        nightModePreference = findPreference(KEY_NIGHT_MODE);
        //accentColorPreference = (ListPreference) findPreference(KEY_ACCENT_COLOR);
        blackNightThemePreference = (SwitchPreference) findPreference(KEY_BLACK_NIGHT_THEME);

        languagePreference.setOnPreferenceChangeListener((preference, newValue) -> {
            if (newValue instanceof String) {
                Locale locale;
                if ("SYSTEM".equals(newValue)) {
                    locale = LocaleDelegate.getSystemLocale();
                } else {
                    locale = Locale.forLanguageTag((String) newValue);
                }
                LocaleDelegate.setDefaultLocale(locale);
                recreateActivity();
            }
            return true;
        });

        String tag = languagePreference.getValue();
        int index = Arrays.asList(languagePreference.getEntryValues()).indexOf(tag);

        List<String> localeName = new ArrayList<>();
        List<String> localeNameUser = new ArrayList<>();
        Locale userLocale = Settings.getLocale();
        for (int i = 1; i < languagePreference.getEntries().length; i++) {
            Locale locale = Locale.forLanguageTag(languagePreference.getEntries()[i].toString());
            localeName.add(!TextUtils.isEmpty(locale.getScript()) ? locale.getDisplayScript(locale) : locale.getDisplayName(locale));
            localeNameUser.add(!TextUtils.isEmpty(locale.getScript()) ? locale.getDisplayScript(userLocale) : locale.getDisplayName(userLocale));
        }
        for (int i = 1; i < languagePreference.getEntries().length; i++) {
            if (index != i) {
                languagePreference.getEntries()[i] = HtmlCompat.fromHtml(
                        String.format("%s - %s",
                                localeName.get(i - 1),
                                localeNameUser.get(i - 1)
                        ));
            } else {
                languagePreference.getEntries()[i] = localeNameUser.get(i - 1);
            }
        }


        if (TextUtils.isEmpty(tag) || "SYSTEM".equals(tag)) {
            languagePreference.setSummary(getString(R.string.follow_system));
        } else if (index != -1) {
            String name = localeNameUser.get(index - 1);
            languagePreference.setSummary(name);
        }

        lightThemePreference.setOnPreferenceChangeListener((preference, o) -> {
            if (o instanceof String) {
                String theme = o.toString();
                if (!ThemeHelper.getTheme(requireContext()).equals(theme)) {
                    ThemeHelper.setLightTheme(theme);
                    recreateActivity();
                }
            }
            return true;
        });

        nightModePreference.setOnPreferenceChangeListener((preference, o) -> {
            if (o instanceof Integer) {
                int mode = (int) o;
                if (Settings.getNightMode() != mode) {
                    Settings.setNightMode(mode);
                    recreateActivity();
                }
            }
            return true;
        });

        blackNightThemePreference.setOnPreferenceChangeListener(((preference, newValue) -> {
            if (ResourceUtils.isNightMode(requireContext().getResources().getConfiguration()))
                recreateActivity();
            return true;
        }));
    }

    @Override
    public boolean isVerticalPaddingRequired() {
        return false;
    }
}
