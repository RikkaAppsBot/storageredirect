package moe.shizuku.redirectstorage.component.submitrule

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.AppConstants.EXTRA_OLD_DATA
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.ObserverInfo
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.api.GitHubApi
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.submitrule.adapter.AppRulesRequestAdapter
import moe.shizuku.redirectstorage.component.submitrule.fragment.AppRulesRequestObserverEditFragment
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestButtonRowViewHolder
import moe.shizuku.redirectstorage.component.submitrule.viewholder.AppRulesRequestQuestionViewHolder
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.event.EventCallbacks
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.ObserverConflictDialog
import moe.shizuku.redirectstorage.utils.ViewUtils
import moe.shizuku.redirectstorage.widget.BottomSpacingDecoration
import rikka.html.text.HtmlCompat

class SubmitRuleFragment : AppFragment(), AppRulesRequestButtonRowViewHolder.Callback {

    companion object {

        private val FRAGMENT_ACTION_PREFIX = BuildConfig.APPLICATION_ID + "." +
                AppRulesRequestObserverEditFragment::class.java.simpleName + ".action"

        @JvmField
        val ACTION_REQUEST_EDIT_OBSERVER = "$FRAGMENT_ACTION_PREFIX.REQUEST_EDIT_OBSERVER"

        private val ACTION_START_SUBMIT = "$FRAGMENT_ACTION_PREFIX.START_SUBMIT"

        private const val KEY_APP_INFO = BuildConfig.APPLICATION_ID + ".appInfo"
        private const val KEY_CONFIGURATION = BuildConfig.APPLICATION_ID + ".config"

        private const val REQUEST_CODE_ADD_OBSERVER = 10
        private const val REQUEST_CODE_EDIT_OBSERVER = 11
    }

    private lateinit var mAppInfo: AppInfo
    private lateinit var mConfigBuilder: AppConfigurationBuilder

    private lateinit var adapter: AppRulesRequestAdapter
    private lateinit var recyclerView: RecyclerView

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_APP_INFO)) {
            mAppInfo = savedInstanceState.getParcelable(KEY_APP_INFO)!!
            mConfigBuilder = savedInstanceState.getParcelable(KEY_CONFIGURATION)!!
        } else {
            mAppInfo = requireArguments().getParcelable(EXTRA_DATA)!!
            mConfigBuilder = AppConfigurationBuilder(mAppInfo)
        }

        registerLocalBroadcastReceiver(ACTION_REQUEST_EDIT_OBSERVER) { _, intent ->
            val addIntent = Intent(requireContext(), AppRulesRequestLinkEditActivity::class.java)
            addIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            addIntent.putExtra(AppRulesRequestLinkEditActivity.EXTRA_EDIT_REASON,
                    AppRulesRequestLinkEditActivity.EDIT_REASON_EDIT)
            addIntent.putExtra(AppRulesRequestLinkEditActivity.EXTRA_APP_INFO,
                    mAppInfo)
            addIntent.putExtra(AppRulesRequestLinkEditActivity.EXTRA_OBSERVER,
                    intent.getParcelableExtra<ObserverInfo>(EXTRA_DATA))
            startActivityForResult(addIntent, REQUEST_CODE_EDIT_OBSERVER)
        }
        registerLocalBroadcastReceiver(ACTION_START_SUBMIT) { _, _ ->
            startSubmitTask()
        }
        registerLocalBroadcastReceiver(EventCallbacks.AppRedirectConfig.whenRequestRemoveObserver {
            mConfigBuilder.removeObserver(it)
            adapter.updateItems()
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(KEY_APP_INFO, mAppInfo)
        outState.putParcelable(KEY_CONFIGURATION, mConfigBuilder)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(android.R.id.list)
        ViewUtils.setPaddingTop(recyclerView, resources.getDimensionPixelSize(R.dimen.padding_8dp))
        adapter = AppRulesRequestAdapter(
                requireActivity(), mAppInfo, mConfigBuilder, this)
        adapter.setUpRecyclerView(recyclerView)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(BottomSpacingDecoration(requireContext()))
    }

    override fun onCreateNewLinkButtonClick() {
        if (activity is SubmitRuleActivity) {
            val intent = Intent(requireActivity(), AppRulesRequestLinkEditActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            intent.putExtra(AppRulesRequestLinkEditActivity.EXTRA_EDIT_REASON,
                    AppRulesRequestLinkEditActivity.EDIT_REASON_ADD)
            intent.putExtra(AppRulesRequestLinkEditActivity.EXTRA_APP_INFO, mAppInfo)
            startActivityForResult(intent, REQUEST_CODE_ADD_OBSERVER)
        }
    }

    override fun onDoneButtonClick() {
        if (mConfigBuilder.shouldSetBooleanValues()) {
            Toast.makeText(activity, R.string.toast_need_make_choices, Toast.LENGTH_SHORT)
                    .show()
            adapter.notifyItemRangeChanged(0, adapter.itemCount,
                    AppRulesRequestQuestionViewHolder.PAYLOAD_HIGHLIGHT_ONCE)
            return
        }

        ConfirmDialogFragment.newInstance().show(childFragmentManager)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_ADD_OBSERVER -> {
                if (RESULT_OK == resultCode && data != null) {
                    val observer = data.getParcelableExtra<ObserverInfo>(EXTRA_DATA)!!
                    val index = mConfigBuilder.observers.indexOf(observer)
                    if (index != -1) {
                        val t = mConfigBuilder.observers[index]
                        ObserverConflictDialog.showSourceConflict(requireActivity(), requireActivity().supportFragmentManager, observer, t)
                    } else {
                        mConfigBuilder.addObserver(observer)
                        adapter.updateItems()
                    }
                }
            }
            REQUEST_CODE_EDIT_OBSERVER -> {
                if (RESULT_OK == resultCode && data != null) {
                    val observer = data.getParcelableExtra<ObserverInfo>(EXTRA_DATA)!!
                    val oldObserver = data.getParcelableExtra<ObserverInfo>(EXTRA_OLD_DATA)!!
                    mConfigBuilder.addObserver(observer)
                    mConfigBuilder.removeObserver(oldObserver)
                    adapter.updateItems()
                }
            }
        }
    }

    private fun startSubmitTask() = launch {
        val progressDialog = ProgressDialog(requireActivity()).also {
            it.setMessage(getString(R.string.dialog_submitting_title))
            it.setCancelable(false)
        }
        progressDialog.show()

        var submitted = false
        try {
            val repo = GitHubApi.getSRRulesRepository()
            val issues = GitHubApi.getIssues(repo)
            val requestIssue = GitHubApi.buildRulesRequestIssue(requireActivity(), mAppInfo, mConfigBuilder)

            val existingIssue = issues.find { requestIssue.title == it.title }
            if (existingIssue != null) {
                GitHubApi.createIssueComment(repo, existingIssue.number, requestIssue.body)
            } else {
                GitHubApi.createIssue(repo, requestIssue)
            }

            submitted = true
        } catch (e: Exception) {
            e.printStackTrace()
            if (activity != null && !requireActivity().isFinishing) {
                Toast.makeText(activity, getString(R.string.toast_failed, e), Toast.LENGTH_SHORT)
                        .show()
            }
        }

        progressDialog.cancel()
        if (submitted) {
            if (activity != null && !requireActivity().isFinishing) {
                Toast.makeText(activity, R.string.toast_submit_successfully, Toast.LENGTH_LONG)
                        .show()
                activity?.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.app_rules_request, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_done) {
            onDoneButtonClick()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    class ConfirmDialogFragment : AlertDialogFragment() {

        override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
            builder.setTitle(R.string.dialog_confirm_rules_request_title)
            builder.setPositiveButton(android.R.string.ok) { _, _ ->
                localBroadcastManager.sendBroadcast(Intent(ACTION_START_SUBMIT))
            }
            builder.setNegativeButton(android.R.string.cancel, null)
        }

        override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View,
                                          savedInstanceState: Bundle?) {
            contentView.findViewById<TextView>(android.R.id.message).text =
                    HtmlCompat.fromHtml(getString(R.string.dialog_confirm_rules_request_message))
        }

        override fun onShow(dialog: AlertDialog) {
            val checkBox = contentView!!.findViewById<CheckBox>(android.R.id.checkbox)
            setPositiveButtonEnabled(checkBox.isChecked)
            checkBox.setOnCheckedChangeListener { _, _ ->
                setPositiveButtonEnabled(checkBox.isChecked)
            }
        }

        override fun onGetContentViewLayoutResource(): Int {
            return R.layout.app_rules_request_confirm_dialog_content_view
        }

        companion object {

            @JvmStatic
            internal fun newInstance(): ConfirmDialogFragment {
                return ConfirmDialogFragment()
            }

        }
    }

}
