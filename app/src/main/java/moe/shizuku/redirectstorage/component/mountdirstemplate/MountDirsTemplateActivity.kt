package moe.shizuku.redirectstorage.component.mountdirstemplate

import android.os.Bundle
import android.view.MenuItem
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity

class MountDirsTemplateActivity : AppBarFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appBar?.setDisplayHomeAsUpEnabled(true)
        appBar?.setHomeAsUpIndicator(R.drawable.helplib_close_24dp)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, MountDirsTemplateFragment())
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}