package moe.shizuku.redirectstorage.component.detail.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.adapter.AppDetailAdapter
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailItemViewHolder.Listener
import moe.shizuku.redirectstorage.viewholder.SimpleItemListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailItemViewHolder(itemView: View?) : SimpleItemListenerViewHolder<Int, Listener>(itemView) {

    companion object {
        val CREATOR = Creator<Int> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailItemViewHolder(inflater.inflate(R.layout.detail_simple_item, parent, false)) }

        private const val ID_START = 100
        const val ID_MEDIA_BROWSER = ID_START + 1
        const val ID_VIEW_FILE_HISTORY = ID_START + 2
        const val ID_SHARE_CONFIGURATION = ID_START + 3
        const val ID_FOLDER_ANALYSIS = ID_START + 4
        const val ID_FILE_BROWSER = ID_START + 5
        const val ID_HELP = ID_START + 6
    }

    interface Listener {
        fun onFileBrowserClick()
        fun onMediaBrowserClick()
        fun onMustReadHelpClick()
        fun onFileHistoryClick()
        fun onShareClick()
        fun onFolderAnalysisClick()
    }

    override fun getAdapter(): AppDetailAdapter {
        return super.getAdapter() as AppDetailAdapter
    }

    override fun onClick(view: View) {
        when (data) {
            ID_FILE_BROWSER -> {
                listener.onFileBrowserClick()
            }
            ID_MEDIA_BROWSER -> {
                listener.onMediaBrowserClick()
            }
            ID_HELP -> {
                listener.onMustReadHelpClick()
            }
            ID_VIEW_FILE_HISTORY -> {
                listener.onFileHistoryClick()
            }
            ID_SHARE_CONFIGURATION -> {
                listener.onShareClick()
            }
            ID_FOLDER_ANALYSIS -> {
                listener.onFolderAnalysisClick()
            }
        }
    }

    override fun onBind() {
        when (data) {
            ID_FILE_BROWSER -> {
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_folder_24dp))
                title.setText(R.string.detail_view_redirect_storage_title)
                summary.setText(R.string.detail_view_redirect_storage_summary)
                itemView.isEnabled = adapter.appInfo.isRedirectEnabled
            }
            ID_MEDIA_BROWSER -> {
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_media_24dp))
                title.setText(R.string.detail_gallery_preview_title)
                summary.setText(R.string.detail_gallery_preview_summary)
                itemView.isEnabled = adapter.appInfo.isRedirectEnabled
            }
            ID_HELP -> {
                icon.setImageDrawable(context.getDrawable(R.drawable.ic_help_outline_24dp))
                title.setText(R.string.detail_must_read_help_title)
                summary.setText(R.string.detail_must_read_help_summary)
                itemView.isEnabled = true
            }
            ID_VIEW_FILE_HISTORY -> {
                icon.setImageResource(R.drawable.ic_file_monitor_24dp)
                title.setText(R.string.detail_view_file_history)
                summary.setText(R.string.detail_view_file_history_summary)
                itemView.isEnabled = true
            }
            ID_SHARE_CONFIGURATION -> {
                icon.setImageResource(R.drawable.ic_share_24dp)
                title.setText(R.string.detail_share_configuration)
                summary.setText(R.string.detail_share_configuration_summary)
                itemView.isEnabled = adapter.appInfo.isRedirectEnabled
            }
            ID_FOLDER_ANALYSIS -> {
                icon.setImageResource(R.drawable.ic_folder_search_outline_24dp)
                title.setText(R.string.folders_analysis_title)
                summary.setText(R.string.folders_analysis_summary)
                itemView.isEnabled = adapter.appInfo.isRedirectEnabled
            }
        }
    }
}