package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.view.View;
import android.widget.CheckBox;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder;
import moe.shizuku.redirectstorage.viewholder.CheckableViewHolder;

public class EditAllowTempViewHolder extends CheckableViewHolder<ObserverInfoBuilder> {

    public static final Creator<ObserverInfoBuilder> CREATOR = (inflater, parent) -> {
        EditAllowTempViewHolder holder = new EditAllowTempViewHolder(
                inflater.inflate(R.layout.observer_info_edit_checkbox_item, parent, false));
        holder.itemView.setEnabled(parent.isEnabled());
        ((CheckBox) holder.checkable).setEnabled(parent.isEnabled());
        return holder;
    };


    private EditAllowTempViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View view) {
        getData().allowTemp(!getData().getAllowTemp());
        super.onBind();
    }

    @Override
    public boolean isChecked() {
        return getData().getAllowTemp();
    }

    @Override
    public void onBind() {
        super.onBind();
        icon.setImageDrawable(null);
        title.setText(R.string.link_dialog_message_allow_temp);
        summary.setVisibility(View.GONE);
    }
}
