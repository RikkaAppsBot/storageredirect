package moe.shizuku.redirectstorage.component.starter.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.BaseService;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.component.starter.StarterConstants;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.NotificationHelper;
import moe.shizuku.redirectstorage.utils.ServerBootHelper;
import moe.shizuku.redirectstorage.utils.SuShell;

import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_STATUS;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_ID_STATUS;
import static moe.shizuku.redirectstorage.app.ServiceKtxKt.startForegroundOrNotify;

public class StarterService extends BaseService {

    private static final int MAX_START_DELAYED_TIME = 10;

    private static String[] createCommandsArray(boolean fromUI) {
        return ServerBootHelper.getCmd(fromUI);
    }

    private static NotificationCompat.Builder createStartNowNotificationBuilder(Context context) {
        NotificationCompat.Builder builder = NotificationHelper.create(context, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start, R.string.notification_service_start_open_root_shell);
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT < 26) {
            builder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        return builder;
    }

    private static NotificationCompat.Builder createCountdownNotificationBuilder(Context context,
                                                                                 @SuppressWarnings("SameParameterValue") int countdown) {
        NotificationCompat.Builder builder = NotificationHelper.create(context, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_ready_to_start);
        builder.setContentText(context.getResources().getQuantityString(R.plurals.notification_service_start_countdown, countdown, countdown));
        builder.setProgress(countdown, 0, false);
        builder.setShowWhen(false);
        Intent cancelIntent = new Intent(context, StarterService.class).setAction(AppConstants.ACTION_CANCEL);
        builder.addAction(R.drawable.ic_cancel_24dp, context.getString(android.R.string.cancel), PendingIntent.getService(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT < 26) {
            builder.setPriority(NotificationCompat.PRIORITY_LOW);
        }
        return builder;
    }

    private Handler mainHandler;

    private Thread checkTaskThread;
    private Thread startCountdownThread;

    private NotificationCompat.Builder notificationBuilder;

    private boolean fromUI;

    private static boolean starting = false;

    private SuShell shell;

    private void startForeground() {
        NotificationManager notificationManager = getSystemService(NotificationManager.class);

        if (notificationManager != null
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_STATUS, getString(R.string.channel_service_status), NotificationManager.IMPORTANCE_LOW);
            channel.setShowBadge(false);
            if (BuildUtils.atLeast29()) {
                channel.setAllowBubbles(false);
            }

            notificationManager.createNotificationChannel(channel);
        }

        // TODO start from UI
        if (Settings.isDelayedBoot()) {
            notificationBuilder = createCountdownNotificationBuilder(this, MAX_START_DELAYED_TIME);
        } else {
            notificationBuilder = createStartNowNotificationBuilder(this);
        }

        startForegroundOrNotify(this, NOTIFICATION_ID_STATUS, notificationBuilder.build());
    }

    @Override
    public void onCreate() {
        if (mainHandler == null) {
            mainHandler = new Handler(getMainLooper());
        }
        startForeground();
        shell = new SuShell(1);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            Log.wtf(AppConstants.TAG, "onStartCommand intent is null");
            stopSelf();
            return START_NOT_STICKY;
        }

        if (AppConstants.ACTION_CANCEL.equals(intent.getAction())) {
            if (startCountdownThread != null && !startCountdownThread.isInterrupted()) {
                startCountdownThread.interrupt();
            }
            stopAndCancelNotification();
            return START_NOT_STICKY;
        }

        if (starting) {
            Log.i(AppConstants.TAG, "rootSession is not null. skip onStartCommand");

            LocalBroadcastManager.getInstance(this)
                    .sendBroadcast(new Intent(AppConstants.ACTION_STARTER_EXIT).putExtra(AppConstants.EXTRA_EXIT_CODE, StarterConstants.EXIT_STARTER_IS_RUNNING));

            return START_REDELIVER_INTENT;
        }

        fromUI = intent.getBooleanExtra(AppConstants.EXTRA_FROM_UI, false);

        if (Settings.isDelayedBoot() && !fromUI) {
            if (startCountdownThread != null && !startCountdownThread.isInterrupted()) {
                startCountdownThread.interrupt();
            }
            startCountdownThread = new Thread(new StartCountdownTask());
            startCountdownThread.start();
            return START_REDELIVER_INTENT;
        }

        startServer();

        return START_REDELIVER_INTENT;
    }

    private void startServer() {
        final Context context = this;

        if (!ServerBootHelper.copyFiles(this)) {
            mainHandler.post(StarterService.this::stopAndPublishCopyFileFailedNotification);
            return;
        }

        starting = true;

        NotificationHelper.notify(this, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start, R.string.notification_service_start_starter);

        shell.run(createCommandsArray(fromUI), new SuShell.Callback() {
            @Override
            public void onLine(String line) {
                if (line.startsWith("info: storage_redirect started. ")) {
                    SRManager.create();
                }

                LocalBroadcastManager.getInstance(context)
                        .sendBroadcast(new Intent(AppConstants.ACTION_STARTER_LINE).putExtra(Intent.EXTRA_TEXT, line));
            }

            @Override
            public void onResult(int exitCode) {
                Log.i(AppConstants.TAG, "onCommandResult: exitCode=" + exitCode);

                if (exitCode == 0) {
                    NotificationHelper.notify(context, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start, R.string.notification_service_start_waiting);

                    LocalBroadcastManager.getInstance(context)
                            .sendBroadcast(new Intent(AppConstants.ACTION_STARTER_EXIT).putExtra(AppConstants.EXTRA_EXIT_CODE, exitCode));

                    startCheckServiceAliveTask();
                } else {
                    stopSelf();

                    if (!SuShell.available() || exitCode == Integer.MIN_VALUE) {
                        NotificationHelper.notify(context, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start_failed, R.string.notification_service_start_failed_open_root_shell);

                        LocalBroadcastManager.getInstance(context)
                                .sendBroadcast(new Intent(AppConstants.ACTION_STARTER_EXIT).putExtra(AppConstants.EXTRA_EXIT_CODE, -1));
                    } else {
                        NotificationHelper.notify(context, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start_failed, getString(R.string.notification_service_start_failed_starter, exitCode));

                        LocalBroadcastManager.getInstance(context)
                                .sendBroadcast(new Intent(AppConstants.ACTION_STARTER_EXIT).putExtra(AppConstants.EXTRA_EXIT_CODE, exitCode));
                    }
                }
            }
        });
    }

    private void startCheckServiceAliveTask() {
        if (checkTaskThread != null) {
            checkTaskThread.interrupt();
        }
        checkTaskThread = new Thread(new CheckServiceAliveTask());
        checkTaskThread.start();
    }

    private void stopAndCancelNotification() {
        stopSelf();
        NotificationHelper.cancel(StarterService.this, NOTIFICATION_ID_STATUS);
    }

    private void stopAndPublishFailedNotification() {
        stopSelf();
        NotificationHelper.notify(this, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start_failed, R.string.notification_service_start_failed_timeout);
    }

    private void stopAndPublishCopyFileFailedNotification() {
        stopSelf();
        NotificationHelper.notify(this, NOTIFICATION_ID_STATUS, NOTIFICATION_CHANNEL_STATUS, R.string.notification_service_start_failed, R.string.notification_service_start_failed_copy_files);
    }

    private void kill() {
        starting = false;
        shell.close();
    }

    @Override
    public void onDestroy() {
        kill();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class CheckServiceAliveTask implements Runnable {

        private static final int DELAY_TIME = 500;
        private static final int MAX_FAILED_TIME = 30 * 1000;

        @Override
        public void run() {
            Log.i(AppConstants.TAG, "CheckServiceAliveTask: Start checking...");
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime <= MAX_FAILED_TIME) {
                if (SRManager.create() != null) {
                    Log.i(AppConstants.TAG, "CheckServiceAliveTask: service detected. Finish");
                    LocalBroadcastManager.getInstance(StarterService.this)
                            .sendBroadcast(new Intent(AppConstants.ACTION_SERVER_STARTED));

                    mainHandler.post(StarterService.this::stopAndCancelNotification);
                    return;
                }

                try {
                    Thread.sleep(DELAY_TIME);
                } catch (InterruptedException e) {
                    // thread is interrupted.
                }
            }
            Log.i(AppConstants.TAG, "CheckServiceAliveTask: Timed out. Finish");
            mainHandler.post(StarterService.this::stopAndPublishFailedNotification);
        }

    }

    private class StartCountdownTask implements Runnable {

        private int remainWaitTime;

        @Override
        public void run() {
            Log.i(AppConstants.TAG, "StartCountdown: Countdown start!");
            remainWaitTime = MAX_START_DELAYED_TIME;
            while (remainWaitTime > 0) {
                SRManager sm = SRManager.create();
                int version = -1;
                if (sm != null) {
                    try {
                        version = sm.getVersion();
                    } catch (Throwable ignored) {
                    }
                }
                if (sm != null && version == Constants.SERVER_VERSION) {
                    Log.i(AppConstants.TAG, "StartCountdown: service detected. Finish");
                    LocalBroadcastManager.getInstance(StarterService.this)
                            .sendBroadcast(new Intent(AppConstants.ACTION_SERVER_STARTED));

                    mainHandler.post(StarterService.this::stopAndCancelNotification);
                    return;
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Log.d(AppConstants.TAG, "StartCountdown: Countdown interrupted.");
                    return;
                }
                mainHandler.post(() -> {
                    notificationBuilder.setContentText(getResources().getQuantityString(R.plurals.notification_service_start_countdown, remainWaitTime, remainWaitTime));
                    notificationBuilder.setProgress(MAX_START_DELAYED_TIME, MAX_START_DELAYED_TIME - remainWaitTime, false);
                    NotificationHelper.notify(StarterService.this, NOTIFICATION_ID_STATUS, notificationBuilder);
                });
                remainWaitTime--;
            }
            mainHandler.post(() -> {
                NotificationHelper.notify(StarterService.this, NOTIFICATION_ID_STATUS, notificationBuilder = createStartNowNotificationBuilder(StarterService.this));
                startServer();
            });
        }
    }
}
