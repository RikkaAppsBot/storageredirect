package moe.shizuku.redirectstorage.component.exportfolders.list

import android.content.Context
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.adapter.RecyclerViewAdapterHelper
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder
import moe.shizuku.redirectstorage.component.detail.viewholder.CollapsibleSummaryViewHolder
import moe.shizuku.redirectstorage.model.AppObserverInfo
import moe.shizuku.redirectstorage.viewholder.DetailedErrorViewHolder
import moe.shizuku.redirectstorage.viewmodel.Status
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.IdBasedRecyclerViewAdapter
import java.util.*
import kotlin.collections.ArrayList

class ExportFoldersAdapter : IdBasedRecyclerViewAdapter() {

    companion object {
        private const val ID_DESCRIPTION = 1
        private const val ID_DIVIDER = 4
        private const val ID_IMPORT_HEADER = 5
        private const val ID_ADD = 6
        private const val ID_ITEMS_FIRST = 100

        private const val ID_OTHER_ONLINE_ERROR = 9L
        private const val ID_OTHER_ONLINE_EMPTY = 10L
        private const val ID_OTHER_ONLINE_UPLOAD = 11L
    }

    init {
        setHasStableIds(true)
    }

    interface Listener : AddExportFolderRuleButtonViewHolder.Listener,
            ExportFolderToggleViewHolder.Listener

    private val idProvider: MutableList<String> = ArrayList()

    fun updateData(context: Context, viewModel: AppDetailViewModel) {
        clear()
        addItems(context, viewModel)
        notifyDataSetChanged()
    }

    private fun generateObserverId(info: AppObserverInfo): Long {
        val key = String.format(Locale.ENGLISH, "oi %s %s %s %s", info.packageName, info.source, info.target, info.onlineId, info.summary)
        val index = idProvider.indexOf(key)
        if (index != -1) return (index + ID_ITEMS_FIRST).toLong()
        idProvider.add(key)
        return (idProvider.size - 1 + ID_ITEMS_FIRST).toLong()
    }

    private fun addItems(context: Context, viewModel: AppDetailViewModel) {
        addItem(CollapsibleSummaryViewHolder.CREATOR, arrayOf<Any>(
                context.getString(R.string.detail_category_synced_folders_title),
                HtmlCompat.fromHtml(context.getString(R.string.detail_synced_folders_description, "storage-redirect-client://help/technical_details_export_isolated_files"), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE),
                "detail_observers_dialog_show_help"
        ), ID_DESCRIPTION.toLong())

        val observers = viewModel.observers.value?.data
        val local: MutableList<AppObserverInfo> = ArrayList()
        val online: MutableList<AppObserverInfo> = ArrayList()

        if (observers != null) {
            for (oi in observers) {
                if (oi.local) local.add(oi) else online.add(oi)
            }
        }
        if (local.isNotEmpty()) {
            for (oi in local) {
                oi.icon = false
                addItem(ExportFolderToggleViewHolder.CREATOR, oi, generateObserverId(oi))
            }
            local[0].icon = true

            if (viewModel.isOnlineRulesEnabled) {
                addItem(AppDetailSummaryViewHolder.CREATOR, context.getString(R.string.rule_upload_subrule_message).toHtml(), ID_OTHER_ONLINE_UPLOAD)
            }
        }

        addItem(AddExportFolderRuleButtonViewHolder.CREATOR, null, ID_ADD.toLong())

        if (viewModel.isOnlineRulesEnabled) {
            RecyclerViewAdapterHelper.addDivider2(this, ID_DIVIDER.toLong())
            addItem(AppDetailSummaryViewHolder.SUBTITLE2_CREATOR, R.string.detail_online_rules_title, ID_IMPORT_HEADER.toLong())
            when {
                online.isNotEmpty() -> {
                    for (oi in online) {
                        oi.icon = false
                        addItem(ExportFolderToggleViewHolder.CREATOR, oi, generateObserverId(oi))
                    }
                    online[0].icon = true
                }
                viewModel.recommendation.value?.status == Status.ERROR -> {
                    val sb = StringBuilder()
                    if (Settings.isCustomRepoEnabled) {
                        sb.append(context.getString(R.string.rule_network_issue_custom_enabled_summary))
                    }

                    val error = viewModel.recommendation.value?.error!!
                    addItem(DetailedErrorViewHolder.LIST_ITEM_STYLE_CREATOR, DetailedErrorViewHolder.Data(
                            error,
                            null,
                            context.getString(R.string.rule_network_issue).toHtml(),
                            sb.toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
                    ), ID_OTHER_ONLINE_ERROR)
                }
                else -> {
                    addItem(AppDetailSummaryViewHolder.CREATOR, context.getString(R.string.rule_empty_for_type_title).toHtml(), ID_OTHER_ONLINE_EMPTY)
                }
            }
        }
    }
}