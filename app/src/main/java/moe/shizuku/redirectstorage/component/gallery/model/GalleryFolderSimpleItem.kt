package moe.shizuku.redirectstorage.component.gallery.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import moe.shizuku.redirectstorage.model.MediaStoreImageRow

@Parcelize
class GalleryFolderSimpleItem(
        var folderName: String,
        var path: String,
        var userId: Int,
        var totalCount: Int = 0,
        var inMountDirs: Boolean = false,
        var previewItems: List<MediaStoreImageRow> = emptyList()
) : Parcelable, Comparable<GalleryFolderSimpleItem> {

    override fun equals(other: Any?): Boolean {
        if (other !is GalleryFolderSimpleItem) return false
        return other.folderName == this.folderName && other.path == this.path
    }

    fun contentEquals(other: GalleryFolderSimpleItem): Boolean {
        if (!equals(other)) {
            return false
        }
        return other.totalCount == this.totalCount
                && other.previewItems.containsAll(this.previewItems)
                && other.previewItems.size == this.previewItems.size
    }

    fun getPreviewModels(maxSize: Int = totalCount): List<PreviewModel> {
        if (previewItems.size <= maxSize) {
            return previewItems.map { PreviewModel(it) }
        } else {
            return previewItems.subList(0, maxSize).map {
                PreviewModel(it)
            }.also {
                it.last().moreCount = previewItems.size - maxSize + 1
            }
        }
    }

    override fun compareTo(other: GalleryFolderSimpleItem): Int {
        return this.folderName.compareTo(other.folderName)
    }

}