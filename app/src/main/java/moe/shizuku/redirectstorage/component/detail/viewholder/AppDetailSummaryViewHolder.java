package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppRecommendationTexts;
import rikka.material.app.LocaleDelegate;
import rikka.recyclerview.BaseViewHolder;


public class AppDetailSummaryViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) -> new AppDetailSummaryViewHolder(inflater.inflate(R.layout.detail_summary, parent, false));

    public static final Creator<Object> END_SPACING_CREATOR = (inflater, parent) -> new AppDetailSummaryViewHolder(inflater.inflate(R.layout.detail_end_spacing_summay, parent, false));

    public static final Creator<Object> INFORMATION_CREATOR = (inflater, parent) -> new AppDetailSummaryViewHolder(inflater.inflate(R.layout.detail_information, parent, false));

    public static final Creator<Object> SUBTITLE_CREATOR = (inflater, parent) -> new AppDetailSummaryViewHolder(inflater.inflate(R.layout.detail_subtitle, parent, false));

    public static final Creator<Object> SUBTITLE2_CREATOR = (inflater, parent) -> new AppDetailSummaryViewHolder(inflater.inflate(R.layout.detail_subtitle2, parent, false));

    private ImageView icon;
    private TextView text1;

    public AppDetailSummaryViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        text1 = itemView.findViewById(android.R.id.text1);

        text1.setHighlightColor(Color.TRANSPARENT);
        text1.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onBind() {
        Context context = itemView.getContext();

        if (getData() instanceof Integer) {
            text1.setText((Integer) getData());
        } else if (getData() instanceof CharSequence) {
            text1.setText((CharSequence) getData());
        } else if (getData() instanceof Object[]) {
            Object[] data = (Object[]) getData();

            text1.setText((CharSequence) data[0]);
            if (data.length == 2
                    && data[1] instanceof Integer) {
                if (icon != null) {
                    icon.setImageDrawable(context.getDrawable((Integer) data[1]));
                }
            }
        } else if (getData() instanceof AppRecommendationTexts.PresetText) {
            AppRecommendationTexts.PresetText data = (AppRecommendationTexts.PresetText) getData();
            Integer color = data.getColor(context);

            text1.setText(data.getText(LocaleDelegate.getDefaultLocale()));
            if (color != null) {
                text1.setTextColor(color);
            }
            if (icon != null) {
                icon.setImageDrawable(data.getIcon(context));
                if (color != null) {
                    icon.setImageTintList(ColorStateList.valueOf(color));
                }
            }
        }
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        onBind();
    }
}