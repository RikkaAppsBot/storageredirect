package moe.shizuku.redirectstorage.component.gallery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.gallery.ViewGalleryActivity
import moe.shizuku.redirectstorage.component.gallery.adapter.GalleryFoldersListAdapter
import moe.shizuku.redirectstorage.component.gallery.dialog.ViewFolderInfoDialog
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.component.gallery.viewholder.FolderItemViewHolder
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.dialog.ExceptionDialog
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver
import moe.shizuku.redirectstorage.event.EventCallbacks
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.MediaStoreUtils
import rikka.html.text.HtmlCompat
import rikka.recyclerview.addFastScroller
import java.util.*

class GalleryFoldersListFragment : AppFragment() {

    companion object {

        @JvmStatic
        fun newInstance(appInfo: AppInfo): GalleryFoldersListFragment {
            return GalleryFoldersListFragment().apply {
                arguments = bundleOf(EXTRA_DATA to appInfo)
            }
        }

    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var progress: View

    private val adapter: GalleryFoldersListAdapter = GalleryFoldersListAdapter()

    private lateinit var appInfo: AppInfo
    private lateinit var mountDirs: MutableList<String>

    private val parentActivity: ViewGalleryActivity? get() = activity as? ViewGalleryActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appInfo = arguments?.getParcelable(EXTRA_DATA)!!

        try {
            mountDirs = SRManager.createThrow()
                    .getMountDirsForPackage(appInfo.packageName, appInfo.userId)
                    .toMutableList()
        } catch (tr: Throwable) {
        }

        val iterate = mountDirs.listIterator()
        while (iterate.hasNext()) {
            val value = iterate.next()
            iterate.set("/storage/emulated/${appInfo.userId}/" + value.toLowerCase(Locale.ENGLISH))
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.gallery_folders_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progress = view.findViewById(android.R.id.progress)

        recyclerView = view.findViewById(android.R.id.list)
        recyclerView.adapter = adapter
        recyclerView.addFastScroller()
        recyclerView.addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
        recyclerView.setHasFixedSize(true)

        registerLocalBroadcastReceiver(EventCallbacks.CommonList.whenItemClickWithParcelable(
                FolderItemViewHolder::class.java, this::onFolderItemClick))
        registerLocalBroadcastReceiver(ActionBroadcastReceiver
                .createReceiver(FolderItemViewHolder.ACTION_VIEW_FOLDER_INFO) { _, intent ->
                    startViewFolderInfoDialog(intent.getParcelableExtra(EXTRA_DATA)!!)
                })

        refreshList()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unregisterAllLocalBroadcastReceivers()
    }

    private fun refreshList() {
        progress.isVisible = true

        launch {
            try {
                val res = withContext(Dispatchers.IO) {
                    val list = MediaStoreUtils.listImages(appInfo.userId)!!
                    MediaStoreUtils.groupByFolder(list, appInfo.userId).map { item ->
                        for (mountDir in mountDirs) {
                            if (item.path.equals(mountDir, true)
                                    || item.path.toLowerCase(Locale.ENGLISH).startsWith("$mountDir/")
                                    || !item.path.toLowerCase(Locale.ENGLISH).startsWith("/storage/emulated/${appInfo.userId}/")) {
                                item.inMountDirs = true
                                break
                            }
                        }
                        item
                    }
                }

                progress.isVisible = false

                parentActivity?.spinnerList = res
                adapter.updateDate(res)
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                e.printStackTrace()
                progress.visibility = View.GONE
                ExceptionDialog.show(parentFragmentManager, e, false, "This may be due to the modification of your system which can\'t be solved by us.")
            }
        }
    }

    private fun onFolderItemClick(item: GalleryFolderSimpleItem) {
        if (!item.inMountDirs) {
            startViewFolderInfoDialog(item)
        } else {
            parentActivity?.selectedItem = item
            childFragmentManager.beginTransaction()
                    .setCustomAnimations(R.animator.dir_enter, R.animator.dir_leave, R.animator.dir_enter, R.animator.dir_leave)
                    .add(R.id.fragment_container, GalleryPhotosListFragment.newInstance(item))
                    .addToBackStack(null)
                    .commit()
            parentActivity?.setSpinnerVisible(true)
        }
    }

    private fun startViewFolderInfoDialog(item: GalleryFolderSimpleItem) {
        if (!item.inMountDirs) {
            AlertDialogFragment.Builder()
                    .setMessage(HtmlCompat.fromHtml(getString(R.string.dialog_gallery_not_in_mount_dirs_message, item.path.replaceFirst("/storage/emulated/${item.userId}/", "")), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                    .setPositiveButton(getString(android.R.string.ok), null)
                    .build()
                    .show(childFragmentManager)
        } else {
            ViewFolderInfoDialog.newInstance(
                    appInfo.label.toString(),
                    item.path,
                    item.path,
                    item.totalCount
            ).show(childFragmentManager)
        }
    }

}