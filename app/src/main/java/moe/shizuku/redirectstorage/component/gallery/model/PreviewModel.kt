package moe.shizuku.redirectstorage.component.gallery.model

import android.content.Context
import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import moe.shizuku.redirectstorage.model.MediaStoreImageRow
import moe.shizuku.redirectstorage.utils.MediaStoreUtils

@Parcelize
class PreviewModel(
        var data: MediaStoreImageRow,
        var moreCount: Int = 0,
        var orderInCategory: Int? = null,
        var inMountDirs: Boolean = true
) : Parcelable {

    fun getThumbnail(previewSize: Int): Bitmap? {
        return MediaStoreUtils.getThumbnail(data.id, previewSize)
    }

    fun getThumbnailPath(context: Context): String? {
        return MediaStoreUtils.queryThumbnail(data.id)?.path
    }

}