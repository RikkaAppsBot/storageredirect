package moe.shizuku.redirectstorage.component.logcat.adapter;

import android.util.Log;

import java.util.ArrayList;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.adapter.BasePopupMenuAdapter;

public class LogcatFilterPopupMenuAdapter extends BasePopupMenuAdapter {

    private static final int GID_SERVER = 1;
    private static final int GID_LOG_FILTER = 2;

    private static final Group LOG_SERVER_GROUP = new Group(GID_SERVER);
    private static final Group LOG_LEVEL_GROUP = new Group(GID_LOG_FILTER);

    private static final Item LOG_TYPE_SUBTITLE =
            Item.createSubtitle(R.id.action_log_type_subtitle, R.string.action_log_type_subtitle);
    private static final Item LOG_TYPE_ALL =
            LOG_SERVER_GROUP.createItem(R.id.action_log_type_all, R.string.action_log_type_whole_device);
    private static final Item LOG_TYPE_SERVER_LOGS =
            LOG_SERVER_GROUP.createItem(R.id.action_log_type_sr_server, R.string.action_log_type_server_logs);
    private static final Item LOG_TYPE_FILE_OBSERVER_LOGS =
            LOG_SERVER_GROUP.createItem(R.id.action_log_type_file_observer, R.string.action_log_type_file_observer);
    private static final Item LOG_TYPE_SE =
            Item.createMultiCheckable(R.id.action_log_type_se, R.string.action_log_type_se);
    private static final Item LOG_LEVEL_SUBTITLE =
            Item.createSubtitle(R.id.action_log_level_subtitle, R.string.action_log_level_filter);
    private static final Item LOG_LEVEL_VERBOSE =
            LOG_LEVEL_GROUP.createItem(R.id.action_log_level_verbose, R.string.log_level_verbose);
    private static final Item LOG_LEVEL_DEBUG =
            LOG_LEVEL_GROUP.createItem(R.id.action_log_level_debug, R.string.log_level_debug);
    private static final Item LOG_LEVEL_INFO =
            LOG_LEVEL_GROUP.createItem(R.id.action_log_level_info, R.string.log_level_info);
    private static final Item LOG_LEVEL_WARN =
            LOG_LEVEL_GROUP.createItem(R.id.action_log_level_warn, R.string.log_level_warn);
    private static final Item LOG_LEVEL_ERROR =
            LOG_LEVEL_GROUP.createItem(R.id.action_log_level_error, R.string.log_level_error);

    private boolean mShowServerLogs = true;
    private boolean mShowSE = true;
    private boolean mShowFileObserver = true;
    private int mLogLevel = Log.VERBOSE;

    public LogcatFilterPopupMenuAdapter() {
        super(new ArrayList<>());
        /*items.add(LOG_TYPE_SUBTITLE);
        items.add(LOG_TYPE_ALL);
        items.add(LOG_TYPE_SERVER_LOGS);
        items.add(LOG_TYPE_FILE_OBSERVER_LOGS);
        items.add(LOG_TYPE_SE);*/
        items.add(LOG_LEVEL_SUBTITLE);
        items.add(LOG_LEVEL_VERBOSE);
        items.add(LOG_LEVEL_DEBUG);
        items.add(LOG_LEVEL_INFO);
        items.add(LOG_LEVEL_WARN);
        items.add(LOG_LEVEL_ERROR);
        updateItemsChecked();
    }

    @Override
    public void updateItemsChecked() {
        switch (mLogLevel) {
            case Log.VERBOSE:
                LOG_LEVEL_VERBOSE.setChecked(true);
                break;
            case Log.DEBUG:
                LOG_LEVEL_DEBUG.setChecked(true);
                break;
            case Log.INFO:
                LOG_LEVEL_INFO.setChecked(true);
                break;
            case Log.WARN:
                LOG_LEVEL_WARN.setChecked(true);
                break;
            case Log.ERROR:
                LOG_LEVEL_ERROR.setChecked(true);
                break;
        }
        /*LOG_TYPE_ALL.setChecked(!mShowServerLogs && !mShowFileObserver);
        LOG_TYPE_SERVER_LOGS.setChecked(mShowServerLogs && !mShowFileObserver);
        LOG_TYPE_FILE_OBSERVER_LOGS.setChecked(mShowServerLogs && mShowFileObserver);
        LOG_TYPE_SE.setChecked(mShowSE);*/
    }

    public void setShowServerLogs(boolean showServerLogs, boolean onlyFileObserver) {
        /*this.mShowServerLogs = showServerLogs;
        this.mShowFileObserver = onlyFileObserver;
        updateItemsChecked();*/
    }

    public void setShowSE(boolean showSE) {
        /*this.mShowSE = showSE;
        updateItemsChecked();*/
    }

    public void setLogLevel(int logLevel) {
        this.mLogLevel = logLevel;
        updateItemsChecked();
    }
}
