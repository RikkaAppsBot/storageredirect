package moe.shizuku.redirectstorage.component.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.preference.PreferenceFragment
import moe.shizuku.preference.SimpleMenuPreference
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.Settings.preferences
import moe.shizuku.redirectstorage.utils.SharedPreferenceDataStore
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView


abstract class SettingsFragment : PreferenceFragment() {

    companion object {
        init {
            SimpleMenuPreference.setLightFixEnabled(true)
        }
    }

    @CallSuper
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.defaultPackages = arrayOf("moe.shizuku.redirectstorage.preference.")
        preferenceManager.preferenceDataStore = SharedPreferenceDataStore(preferences)
    }

    override fun onCreateItemDecoration(): DividerDecoration? {
        return CategoryDivideDividerDecoration()
    }

    open val isVerticalPaddingRequired: Boolean
        get() = true

    override fun onCreateRecyclerView(inflater: LayoutInflater, parent: ViewGroup, savedInstanceState: Bundle?): RecyclerView {
        val recyclerView = super.onCreateRecyclerView(inflater, parent, savedInstanceState) as BorderRecyclerView
        recyclerView.fixEdgeEffect()
        if (isVerticalPaddingRequired) {
            recyclerView.addItemDecoration(VerticalPaddingDecoration(recyclerView.context))
        }
        (recyclerView.layoutParams as? FrameLayout.LayoutParams)?.let { lp ->
            lp.rightMargin = recyclerView.context.resources.getDimension(R.dimen.rd_activity_horizontal_margin).toInt()
            lp.leftMargin = lp.rightMargin
        }
        recyclerView.borderViewDelegate.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top: Boolean, _: Boolean, _: Boolean, _: Boolean -> (activity as SettingsActivity?)?.appBar?.setRaised(!top) }

        return recyclerView
    }

    protected fun recreateActivity() {
        if (activity != null) {
            activity!!.recreate()
        }
    }
}