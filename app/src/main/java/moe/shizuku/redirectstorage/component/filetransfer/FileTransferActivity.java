package moe.shizuku.redirectstorage.component.filetransfer;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.component.filetransfer.fragment.FileTransferFragment;
import moe.shizuku.redirectstorage.model.AppInfo;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public class FileTransferActivity extends AppActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final AppInfo data = getIntent().getParcelableExtra(EXTRA_DATA);

        // TODO Should support user defined links?
        if (data == null || data.getConfiguration() == null) {
            Toast.makeText(getApplicationContext(), R.string.file_transfer_toast_no_configs_warn_msg, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, FileTransferFragment.newInstance(data))
                    .commit();
        }
    }

}
