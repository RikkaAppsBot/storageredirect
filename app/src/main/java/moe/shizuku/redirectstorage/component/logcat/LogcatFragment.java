package moe.shizuku.redirectstorage.component.logcat;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.ILogcatObserver;
import moe.shizuku.redirectstorage.ILogcatService;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.adapter.BasePopupMenuAdapter;
import moe.shizuku.redirectstorage.app.AppFragment;
import moe.shizuku.redirectstorage.component.logcat.adapter.LogcatAdapter;
import moe.shizuku.redirectstorage.component.logcat.adapter.LogcatFilterPopupMenuAdapter;
import moe.shizuku.redirectstorage.component.logcat.service.LogcatService;
import moe.shizuku.redirectstorage.model.LogcatItem;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;
import rikka.core.widget.SearchViewCallback;
import rikka.core.widget.SearchViewHelper;
import rikka.material.app.AppBar;
import rikka.material.app.AppBarOwner;

public class LogcatFragment extends AppFragment implements SearchViewCallback {

    private static final String STATE_AUTO_SCROLL_TO_BOTTOM = BuildConfig.APPLICATION_ID + ".state.AUTO_SCROLL";
    private static final String STATE_CURRENT_LOG_LEVEL = BuildConfig.APPLICATION_ID + ".state.CURRENT_LOG_LV";
    private static final String STATE_SHOW_SERVER = BuildConfig.APPLICATION_ID + ".state.SHOW_SERVER";
    private static final String STATE_SHOW_FILE_OBSERVER = BuildConfig.APPLICATION_ID + ".state.SHOW_FILE_OBSERVER";
    private static final String STATE_SHOW_SE = BuildConfig.APPLICATION_ID + ".state.SHOW_SE";
    private static final String STATE_SHOULD_SHOW_DIALOG = BuildConfig.APPLICATION_ID + ".state.SHOULD_SHOW_DIALOG";

    private static final SparseIntArray sActionIdToLogLevel;

    static {
        sActionIdToLogLevel = new SparseIntArray();
        sActionIdToLogLevel.put(R.id.action_log_level_verbose, Log.VERBOSE);
        sActionIdToLogLevel.put(R.id.action_log_level_debug, Log.DEBUG);
        sActionIdToLogLevel.put(R.id.action_log_level_info, Log.INFO);
        sActionIdToLogLevel.put(R.id.action_log_level_warn, Log.WARN);
        sActionIdToLogLevel.put(R.id.action_log_level_error, Log.ERROR);
    }

    private final List<LogcatItem> logs = new ArrayList<>();
    private Handler handler = new Handler(Looper.getMainLooper());
    private RecyclerView recyclerView;
    private LogcatAdapter adapter;
    private Switch toggle;
    private TextView toggleText;
    private FloatingActionButton toTopFAB, toBottomFAB;

    private ListPopupWindow mLogcatFilterMenu;
    private final LogcatFilterPopupMenuAdapter mLogcatFilterAdapter = new LogcatFilterPopupMenuAdapter();

    private SearchViewHelper mSearchViewHelper;

    @Nullable
    private ILogcatService mService;
    private final Runnable hideFabRunnable = () -> {
        hideToTop();
        hideToBottom();
    };

    private boolean autoScrollToBottom = true;
    private int mCurrentLevel = Log.VERBOSE;
    private boolean mShowServer = true;
    private boolean mShowFileObserver = true;
    private boolean mShowSE = true;
    private boolean mShouldShowDialog = true;

    private BroadcastReceiver mStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateSwitchState();
        }
    };

    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton v, boolean isChecked) {
            Intent intent = new Intent(v.getContext(), LogcatService.class);
            intent.setAction(!isChecked ? LogcatService.ACTION_STOP : LogcatService.ACTION_START);
            v.getContext().startService(intent);
            if (!isChecked) {
                logs.clear();
                adapter.updateItems(logs);
                hideToTop();
                hideToBottom();
            }
        }
    };

    public LogcatFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createLogcatFilterMenu();

        mSearchViewHelper = SearchViewHelper.create(savedInstanceState, R.id.action_search);

        if (savedInstanceState != null) {
            autoScrollToBottom = savedInstanceState.getBoolean(STATE_AUTO_SCROLL_TO_BOTTOM);
            mCurrentLevel = savedInstanceState.getInt(STATE_CURRENT_LOG_LEVEL);
            mShowServer = savedInstanceState.getBoolean(STATE_SHOW_SERVER);
            mShowFileObserver = savedInstanceState.getBoolean(STATE_SHOW_FILE_OBSERVER);
            mShowSE = savedInstanceState.getBoolean(STATE_SHOW_SE);
            mShouldShowDialog = savedInstanceState.getBoolean(STATE_SHOULD_SHOW_DIALOG);
        }

        mLogcatFilterAdapter.setShowServerLogs(mShowServer, mShowFileObserver);
        mLogcatFilterAdapter.setShowSE(mShowSE);
        mLogcatFilterAdapter.setLogLevel(mCurrentLevel);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logcat_fragment, container, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_AUTO_SCROLL_TO_BOTTOM, autoScrollToBottom);
        outState.putInt(STATE_CURRENT_LOG_LEVEL, mCurrentLevel);
        outState.putBoolean(STATE_SHOW_SERVER, mShowServer);
        outState.putBoolean(STATE_SHOW_FILE_OBSERVER, mShowFileObserver);
        outState.putBoolean(STATE_SHOW_SE, mShowSE);
        outState.putBoolean(STATE_SHOULD_SHOW_DIALOG, mShouldShowDialog);
        mSearchViewHelper.onSaveInstanceState(outState);
    }

    private ILogcatObserver mLogObserver = new ILogcatObserver.Stub() {
        @Override
        public void onNewLines(ParceledListSlice items) throws RemoteException {
            try {
                //noinspection unchecked
                List<LogcatItem> list = items.getList();
                logs.addAll(list);
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        adapter.updateItems(logs);
                        if (autoScrollToBottom) {
                            scrollToBottom(true);
                        }
                    });
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ILogcatService.Stub.asInterface(service);

            try {
                logs.clear();
                mService.setIndex(0);
                if (!isDetached()) {
                    adapter.updateItems(logs);
                    updateSwitchState();
                    mService.registerObserver(mLogObserver);
                }
            } catch (Throwable e) {
                // TODO Show failed
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            if (getContext() != null) {
                getContext().unbindService(this);
            }
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (adapter == null) {
            adapter = new LogcatAdapter(logs);
        }
        adapter.setFilterLogLevel(mCurrentLevel);
        adapter.setFilterShowSE(mShowSE);
        adapter.setFilterShowServer(mShowServer, mShowFileObserver);

        recyclerView = view.findViewById(android.R.id.list);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int state;

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (state == RecyclerView.SCROLL_STATE_DRAGGING) {
                    // drag stopped
                    handler.postDelayed(hideFabRunnable, 3000);
                }
                state = newState;
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (state != RecyclerView.SCROLL_STATE_DRAGGING) {
                    return;
                }
                if (dy > 0) {
                    hideToTop();
                    showToBottom();
                    handler.removeCallbacks(hideFabRunnable);
                } else if (dy < 0) {
                    showToTop();
                    hideToBottom();
                    handler.removeCallbacks(hideFabRunnable);
                }
            }
        });

        //RecyclerViewsKt.addFastScroller(recyclerView, null);
        //RecyclerViewHelper.fixOverScroll(recyclerView);

        toTopFAB = view.findViewById(R.id.toTopButton);
        toTopFAB.setOnClickListener(v -> {
            scrollToTop(true);
            autoScrollToBottom = false;
            hideToTop();
        });

        toBottomFAB = view.findViewById(R.id.toBottomButton);
        toBottomFAB.setOnClickListener(v -> {
            scrollToBottom(true);
            autoScrollToBottom = true;
            hideToBottom();
        });
    }

    @SuppressLint("InflateParams")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AppBar appBar = ((AppBarOwner) requireActivity()).getAppBar();
        if (appBar != null) {
            View view = LayoutInflater.from(requireContext()).inflate(R.layout.logcat_menu_toggle, null);
            toggle = view.findViewById(R.id.switchWidget);
            toggleText = view.findViewById(android.R.id.text1);
            appBar.setCustomView(view, new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL | Gravity.END));
            appBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM);

            toggle.setChecked(true);
            toggleText.setText(R.string.toggle_on);
            toggle.setOnCheckedChangeListener(mOnCheckedChangeListener);
        }
    }

    private void scrollToTop(boolean animate) {
        if (adapter.getItemCount() <= 0) return;
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!animate || layoutManager.findLastVisibleItemPosition() > 20) {
            recyclerView.scrollToPosition(0);
        } else {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    private void scrollToBottom(boolean animate) {
        if (adapter.getItemCount() <= 0) return;
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!animate || adapter.getItemCount() - layoutManager.findLastVisibleItemPosition() > 20) {
            recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        } else {
            recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        }
    }

    private void showToTop() {
        if (adapter.getItemCount() <= 0) {
            return;
        }

        if (!toTopFAB.isOrWillBeShown()) {
            /*TransitionManager.endTransitions(listContainer);
            TransitionManager.beginDelayedTransition(listContainer);*/
            toTopFAB.show();
        }

        autoScrollToBottom = false;
    }

    private void hideToTop() {
        if (!toTopFAB.isOrWillBeHidden()) {
            toTopFAB.hide();
        }
    }

    private void showToBottom() {
        if (adapter.getItemCount() <= 0) {
            return;
        }

        if (!toBottomFAB.isOrWillBeShown()) {
            /*TransitionManager.endTransitions(listContainer);
            TransitionManager.beginDelayedTransition(listContainer);*/
            toBottomFAB.show();
        }

        autoScrollToBottom = false;
    }

    private void hideToBottom() {
        if (!toBottomFAB.isOrWillBeHidden()) {
            toBottomFAB.hide();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mService != null) {
            try {
                mService.unregisterObserver(mLogObserver);
            } catch (RemoteException ignored) {
            }
            requireContext().unbindService(mServiceConnection);
        }
    }

    private void updateSwitchState() {
        try {
            toggle.setOnCheckedChangeListener(null);
            toggle.setChecked(mService != null && mService.isRunning());
            toggle.setOnCheckedChangeListener(mOnCheckedChangeListener);
            toggleText.setText(toggle.isChecked() ? R.string.toggle_on : R.string.toggle_off);
            //switchBar.setChecked(mService != null && mService.isRunning());
        } catch (RemoteException ignored) {
            toggle.setOnCheckedChangeListener(null);
            toggle.setChecked(false);
            toggle.setOnCheckedChangeListener(mOnCheckedChangeListener);
            toggleText.setText(R.string.toggle_off);
            //switchBar.setChecked(false);
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (mService == null) {
            context.bindService(new Intent(context, LogcatService.class), mServiceConnection, Service.BIND_AUTO_CREATE);
        } else {
            try {
                logs.clear();
                mService.setIndex(0);
                adapter.updateItems(logs);
                if (autoScrollToBottom) {
                    scrollToBottom(false);
                }
                updateSwitchState();
                mService.registerObserver(mLogObserver);
            } catch (RemoteException e) {
                // TODO Show failed
            }
        }
        registerLocalBroadcastReceiver(new IntentFilter(LogcatService.ACTION_UPDATE_STATUS), mStatusReceiver);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.logcat, menu);
        super.onCreateOptionsMenu(menu, inflater);
        mSearchViewHelper.onCreateOptionsMenu(this, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.logcat_search_hint));
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        View menuItemView = requireActivity().findViewById(R.id.action_log_filter);
        if (menuItemView != null) {
            mLogcatFilterMenu.setAnchorView(menuItemView);
            menuItemView.setOnTouchListener(mLogcatFilterMenu.createDragToOpenListener(menuItemView));
        } else {
            new Handler().postDelayed(requireActivity()::invalidateOptionsMenu, 500);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_log_filter:
                try {
                    mLogcatFilterMenu.show();
                } catch (Exception e) {
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(() -> {
                            try {
                                mLogcatFilterMenu.show();
                            } catch (Exception ignored) {

                            }
                        });
                    }
                }
                return true;
            case R.id.action_log_type_all:
                setFilterShowServer(false, false);
                return true;
            case R.id.action_log_type_sr_server:
                setFilterShowServer(true, false);
                return true;
            case R.id.action_log_type_file_observer:
                setFilterShowServer(true, true);
                return true;
            case R.id.action_log_type_se:
                setFilterShowSE(!item.isChecked());
                return true;
            case R.id.action_log_level_verbose:
            case R.id.action_log_level_debug:
            case R.id.action_log_level_info:
            case R.id.action_log_level_warn:
            case R.id.action_log_level_error:
                item.setChecked(true);
                mCurrentLevel = sActionIdToLogLevel.get(item.getItemId());
                mLogcatFilterAdapter.setLogLevel(mCurrentLevel);
                adapter.setFilterLogLevel(mCurrentLevel);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFilterShowServer(boolean serverOnly, boolean fileObserverOnly) {
        mShowServer = serverOnly;
        mShowFileObserver = fileObserverOnly;
        mLogcatFilterAdapter.setShowServerLogs(mShowServer, mShowFileObserver);
        adapter.setFilterShowServer(mShowServer, mShowFileObserver);
    }

    private void setFilterShowSE(boolean b) {
        mShowSE = b;
        mLogcatFilterAdapter.setShowSE(mShowSE);
        adapter.setFilterShowSE(mShowSE);
    }

    @Override
    public void onSearchExpand() {
        //switchBar.setVisibility(View.GONE);
        adapter.getFilterHelper().setSearching(true);
    }

    @Override
    public void onSearchCollapse() {
        //switchBar.setVisibility(View.VISIBLE);
        adapter.getFilterHelper().setSearching(false);
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onSearchTextChange(String newText) {
        adapter.getFilterHelper().setKeyword(newText);
    }

    private void createLogcatFilterMenu() {
        mLogcatFilterMenu = new ListPopupWindow(requireContext(), null, 0, R.style.Widget_ListPopupWindow_Overflow);
        mLogcatFilterMenu.setModal(true);
        mLogcatFilterMenu.setAdapter(mLogcatFilterAdapter);
        mLogcatFilterMenu.setContentWidth(mLogcatFilterAdapter.getContentWidth(requireContext()));
        mLogcatFilterMenu.setOnItemClickListener((parent, view, position, id) -> {
            BasePopupMenuAdapter.Item item = mLogcatFilterAdapter.getItem(position);
            requireActivity().onMenuItemSelected(0, item.getMenuItem());
            mLogcatFilterMenu.dismiss();
        });
    }
}
