package moe.shizuku.redirectstorage.component.logcat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder
import moe.shizuku.redirectstorage.component.logcat.viewholder.EmptyHistoryViewHolder
import moe.shizuku.redirectstorage.component.logcat.viewholder.HistoryItemViewHolder
import moe.shizuku.redirectstorage.component.logcat.viewholder.LogcatStatusItemHolder
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.IdBasedRecyclerViewAdapter
import java.io.File

/**
 * Created by Fung Gwo on 2018/2/21.
 */
class LogcatHistoryAdapter : IdBasedRecyclerViewAdapter() {

    companion object {
        const val HISTORY_ITEM_START_POSITION = 3
        private const val ID_STATUS = 1L
        private const val ID_HELP = 2L
        private const val ID_HISTORY_SUBTITLE = 3L
        private const val ID_FILE_PREFIX = 100L

        val HELP_CREATOR = BaseViewHolder.Creator<Any?> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailSummaryViewHolder(inflater.inflate(R.layout.logcat_history_help, parent, false)) }
        val SUBTITLE_CREATOR = BaseViewHolder.Creator<Any?> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailSummaryViewHolder(inflater.inflate(R.layout.logcat_history_subtitle_layout, parent, false)) }
    }

    init {
        setHasStableIds(true)
    }

    private var historyFiles: List<File>? = null
    var isLogcatEnabled = false
        set(logcatEnabled) {
            field = logcatEnabled
            notifyItemChangeById(ID_STATUS, AppConstants.PAYLOAD)
        }

    fun updateItems() {
        clear()
        addItem(LogcatStatusItemHolder.CREATOR, null, ID_STATUS)
        addItem(HELP_CREATOR, R.string.logcat_help, ID_HELP)
        addItem(SUBTITLE_CREATOR, R.string.logcat_history_subtitle, ID_HISTORY_SUBTITLE)
        if (historyFiles.isNullOrEmpty()) {
            addItem(EmptyHistoryViewHolder.CREATOR, null, ID_FILE_PREFIX)
        } else {
            for (file in historyFiles!!) {
                addItem(HistoryItemViewHolder.CREATOR, file, ID_FILE_PREFIX + file.hashCode().toLong())
            }
        }
        notifyDataSetChanged()
    }

    fun setHistoryFiles(historyFiles: List<File>?) {
        this.historyFiles = historyFiles
        updateItems()
    }
}