package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.EditTextDialogFragment
import rikka.html.text.toHtml
import java.util.regex.Pattern
import java.util.regex.PatternSyntaxException

class ObserverMaskEditDialog : EditTextDialogFragment() {

    private var mask: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mask = requireArguments().getString(AppConstants.EXTRA_DATA)
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        super.onBuildAlertDialog(builder, savedInstanceState)
        builder.setTitle(R.string.observer_info_edit_dialog_mask)
        builder.setNeutralButton(R.string.observer_mask_apply_template_button
        ) { _, _ ->
            ObserverMaskTemplatesDialog()
                    .show(parentFragmentManager)
        }
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        super.onAlertDialogCreated(dialog, contentView, savedInstanceState)
        setupValue(mask)
        textField.hint = getString(R.string.regular_expression)
        editText.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            editText.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO
        }
        editText.doOnTextChanged { _, _, _, _ ->
            textField.error = null
        }
        helperText.isVisible = true
        helperText.text = getString(R.string.online_rules_preview_mask_description).toHtml()
    }

    override fun onShow(dialog: AlertDialog) {
        super.onShow(dialog)

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { v ->
            val s = value
            if (!TextUtils.isEmpty(s)) {
                try {
                    Pattern.compile(s!!)
                    onDone()
                } catch (e: PatternSyntaxException) {
                    textField.error = v.context.getString(R.string.toast_bad_mask)
                    //Toast.makeText(v.getContext(), R.string.toast_bad_make, Toast.LENGTH_SHORT).show();
                }

            } else {
                onDone()
            }
        }
    }

    override fun onDone() {
        (parentFragment as ExportFolderEditFragment).onMaskSet(value)

        dialog!!.dismiss()
    }

    companion object {

        internal fun newInstance(mask: String?): ObserverMaskEditDialog {
            val args = Bundle()
            args.putString(AppConstants.EXTRA_DATA, mask)

            val dialog = ObserverMaskEditDialog()
            dialog.arguments = args
            return dialog
        }
    }
}