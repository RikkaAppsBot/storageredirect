package moe.shizuku.redirectstorage.component.logcat

import android.app.Service
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog.Builder
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.ILogcatService
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.logcat.adapter.LogcatHistoryAdapter
import moe.shizuku.redirectstorage.component.logcat.service.LogcatHistoryService
import moe.shizuku.redirectstorage.component.logcat.service.LogcatService
import moe.shizuku.redirectstorage.utils.ForegroundService
import moe.shizuku.redirectstorage.utils.ItemSpacingDecoration
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.core.compat.CollectionsCompat
import rikka.internal.help.HelpProvider
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView
import java.io.File
import java.util.*

class LogcatStatusFragment : AppFragment() {

    private var service: ILogcatService? = null

    private val adapter: LogcatHistoryAdapter by lazy {
        LogcatHistoryAdapter().apply {
            registerAdapterDataObserver(object : AdapterDataObserver() {
                override fun onItemRangeRemoved(positionStart: Int, _itemCount: Int) {
                    // When items are cleared, notify adapter globally changed to
                    // show a empty view.
                    if (itemCount <= LogcatHistoryAdapter.HISTORY_ITEM_START_POSITION + 1) {
                        setHistoryFiles(emptyList())
                    }
                }
            })
        }
    }

    private val mServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            service = ILogcatService.Stub.asInterface(binder)

            if (!isDetached) {
                try {
                    adapter.isLogcatEnabled = service?.isRunning == true
                } catch (e: Throwable) {
                    // TODO Show failed
                }
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            service = null
            context?.unbindService(this)
        }

    }

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_appbar_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView: BorderRecyclerView = view.findViewById(android.R.id.list)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(ItemSpacingDecoration(recyclerView.context, 8))
        recyclerView.addItemDecoration(VerticalPaddingDecoration(recyclerView.context))
        recyclerView.fixEdgeEffect()

        recyclerView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            if (activity != null) {
                (activity as AppBarFragmentActivity).appBar?.setRaised(!top)
            }
        }
    }

    private fun refreshList() {
        lifecycleScope.launchWhenResumed {
            val res: List<File>? = try {
                withContext(IO) {
                    val res = ArrayList<File>()
                    val directories: MutableList<File> = ArrayList()
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        directories.add(requireContext().createDeviceProtectedStorageContext().filesDir)
                    }
                    requireContext().getExternalFilesDir(null)?.let {
                        directories.add(it)
                    }
                    directories.add(requireContext().filesDir)

                    for (directory in directories) {
                        File(directory, "log")
                                .listFiles { file: File -> file.isFile && file.name.endsWith(".log") }
                                ?.let {
                                    res.addAll(it)
                                }
                    }

                    res.sortDescending()
                    res
                }
            } catch (e: Throwable) {
                null
            }
            if (res != null) {
                adapter.setHistoryFiles(res)
            }
        }
    }

    private fun deleteAllLogs() {
        val context = requireContext()
        ForegroundService.startOrNotify(context, Intent(context, LogcatHistoryService::class.java)
                .setAction(LogcatHistoryService.ACTION_DELETE_FILES)
                .putExtra(LogcatHistoryService.EXTRA_FILES,
                        CollectionsCompat.filterTo(
                                adapter.getItems(),
                                ArrayList<Any>(),
                                { item: Any? -> item is File }
                        )
                ))
    }

    override fun onResume() {
        super.onResume()
        if (service != null) {
            try {
                adapter.isLogcatEnabled = service?.isRunning == true
            } catch (e: RemoteException) { // TODO Show failed
            }
        }
        refreshList()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (service == null) {
            context.bindService(Intent(context, LogcatService::class.java), mServiceConnection, Service.BIND_AUTO_CREATE)
        } else {
            try {
                adapter.isLogcatEnabled = service?.isRunning == true
            } catch (e: RemoteException) { // TODO Show failed
            }
        }
        registerLocalBroadcastReceiver(LogcatHistoryService.ACTION_REMOVED) { context: Context, intent: Intent -> onLogFileRemoved(context, intent) }
    }

    override fun onDetach() {
        super.onDetach()
        if (service != null) {
            requireContext().unbindService(mServiceConnection)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.logcat_history, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_help -> {
                val context = requireContext()
                val entity = HelpProvider.get(context, "how_to_report_problems")
                if (entity != null) {
                    entity.startActivity(context)
                } else {
                    Toast.makeText(context, R.string.toast_cannot_load_help, Toast.LENGTH_SHORT).show()
                }
                return true
            }
            R.id.action_clear_logs -> {
                Builder(requireContext())
                        .setTitle(R.string.dialog_delete_logs_title)
                        .setMessage(R.string.dialog_delete_logs_message)
                        .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int -> deleteAllLogs() }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onLogFileRemoved(context: Context, intent: Intent) {
        Log.i(AppConstants.TAG, "onLogFileRemoved")
        if (!intent.hasExtra(LogcatHistoryService.EXTRA_FILES)) {
            return
        }
        val removedFile = intent.getSerializableExtra(LogcatHistoryService.EXTRA_FILES) as File
        Log.i(AppConstants.TAG, removedFile.toString())
        val index = adapter.getItems<Any>().indexOf(removedFile)
        if (index >= 0) {
            adapter.getItems<Any>().removeAt(index)
            adapter.notifyItemRemoved(index)
        }
    }
}