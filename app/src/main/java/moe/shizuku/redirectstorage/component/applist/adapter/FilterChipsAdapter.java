package moe.shizuku.redirectstorage.component.applist.adapter;

import java.util.List;

import moe.shizuku.redirectstorage.component.applist.viewholder.FilterChipViewHolder;
import moe.shizuku.redirectstorage.model.FilterItem;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;

public class FilterChipsAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    private FilterChipsCallback mCallback;
    private boolean isEditing = false;

    public FilterChipsAdapter(FilterChipsCallback callback) {
        super();

        mCallback = callback;

        getCreatorPool().putRule(FilterItem.class, FilterChipViewHolder.CREATOR);

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return ((FilterItem) getItemAt(position)).titleResourceId;
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }

    public void updateItems(List<FilterItem> items, boolean notify) {
        getItems().clear();
        getItems().addAll(items);
        if (notify) {
            notifyDataSetChanged();
        }
    }

    public final void updateItem(FilterItem item) {
        mCallback.onItemUpdated(item);
    }

    public boolean isEditing() {
        return isEditing;
    }

    public void setEditing(boolean editing) {
        isEditing = editing;
    }

    public interface FilterChipsCallback {

        void onItemUpdated(FilterItem item);

        void onItemDeleted(FilterItem item);

    }

}
