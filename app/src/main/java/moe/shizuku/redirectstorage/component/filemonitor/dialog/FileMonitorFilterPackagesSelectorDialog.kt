package moe.shizuku.redirectstorage.component.filemonitor.dialog

import android.os.Bundle
import androidx.core.os.bundleOf
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.Settings.PackageFilterMode
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog
import moe.shizuku.redirectstorage.model.IPackageDescriptor
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.model.SimplePackageDescriptor
import moe.shizuku.redirectstorage.utils.UserHelper

class FileMonitorFilterPackagesSelectorDialog : PackagesSelectorDialog() {

    companion object {

        @JvmStatic
        val EXTRA_FILTER_MODE = FileMonitorFilterPackagesSelectorDialog::class.java.name +
                ".extra.FILTER_MODE"

        @JvmStatic
        fun newInstance(
                @PackageFilterMode mode: Int,
                selectedPackages: List<String>? = null
        ): FileMonitorFilterPackagesSelectorDialog {
            return FileMonitorFilterPackagesSelectorDialog().apply {
                arguments = bundleOf(
                        EXTRA_FILTER_MODE to mode,
                        EXTRA_SELECTED_PACKAGES to selectedPackages?.map {
                            SimplePackageDescriptor.create(it, UserHelper.currentUserId(), null)
                        }?.let {
                            ArrayList(it)
                        }
                )
            }
        }

    }

    @PackageFilterMode
    private var mFilterMode: Int = PackageFilterMode.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFilterMode = arguments?.getInt(EXTRA_FILTER_MODE) ?: PackageFilterMode.NONE
    }

    override fun loadAllAvailablePackages(selectedPackages: List<IPackageDescriptor>?): List<SimpleAppInfo> {
        val list = ArrayList<SimpleAppInfo>()
        val srm = SRManager.createThrow()
        val pm = requireContext().packageManager
        //val um = context!!.getSystemService(UserManager::class.java)
        //for (userHandle in um?.userProfiles?: emptyList()) {
            list.addAll(srm.getInstalledPackages(0, UserHelper.currentUserId()/*userHandle.hashCode()*/).map {
                if (it.sharedUserId != null) {
                    SharedUserLabelCache.load(pm, it.sharedUserLabel, it.sharedUserId, it.packageName, it.applicationInfo)
                }
                SimpleAppInfo(it.packageName, UserHelper.currentUserId(), it.applicationInfo, it.applicationInfo.loadLabel(pm), it.sharedUserId)
            })
        //}
        return list.sortedWith(Comparator { o1, o2 ->
            val b1 = selectedPackages?.contains(SimplePackageDescriptor(o1.packageName, o1.userId, o1.sharedUserId))
                    ?: false
            val b2 = selectedPackages?.contains(SimplePackageDescriptor(o2.packageName, o2.userId, o1.sharedUserId))
                    ?: false
            if (b1 == b2) {
                o1.compareTo(o2)
            } else {
                if (b1 && !b2) {
                    -1
                } else if (!b1 && b2) {
                    1
                } else {
                    0
                }
            }
        })
    }

}