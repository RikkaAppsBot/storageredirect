package moe.shizuku.redirectstorage.component.exportfolders.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.viewholder.SimpleItemListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AddExportFolderRuleButtonViewHolder private constructor(itemView: View) : SimpleItemListenerViewHolder<Any?, AddExportFolderRuleButtonViewHolder.Listener?>(itemView) {

    companion object {
        val CREATOR = Creator<Any> { inflater: LayoutInflater, parent: ViewGroup? -> AddExportFolderRuleButtonViewHolder(inflater.inflate(R.layout.detail_edit_simple_item, parent, false)) }
    }

    interface Listener {
        fun onAddRuleClick()
    }

    init {
        icon.setImageResource(R.drawable.ic_add_24dp)
        title.setText(R.string.detail_edit_add_link)
        summary.visibility = View.GONE
    }

    override fun onClick(view: View) {
        listener!!.onAddRuleClick()
    }
}