package moe.shizuku.redirectstorage.component.submitrule.viewholder;

import android.view.View;

import androidx.annotation.Nullable;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder;
import rikka.html.text.HtmlCompat;

public class AppRulesRequestVerifiedCheckViewHolder extends AppRulesRequestQuestionViewHolder<AppConfigurationBuilder> {

    public static final Creator<AppConfigurationBuilder> CREATOR = (inflater, parent) -> new AppRulesRequestVerifiedCheckViewHolder(inflater.inflate(R.layout.app_rules_request_question, parent, false));

    public AppRulesRequestVerifiedCheckViewHolder(View itemView) {
        super(itemView);

        title.setText(HtmlCompat.fromHtml(title.getResources().getString(R.string.app_rules_request_if_having_bad_behavior)));
        summary.setText(HtmlCompat.fromHtml(title.getResources().getString(R.string.app_rules_request_if_having_bad_behavior_summary)));

        icon.setImageResource(R.drawable.ic_bug_24dp);
    }

    @Override
    public void onAnswerSelected(@Nullable Boolean answer) {
        getData().setVerified(answer != null ? !answer : null);
        if (Boolean.FALSE.equals(answer)) {
            getData().setRecommended(false);
        } else {
            getData().setRecommended(null);
        }
        getAdapter().updateItems();
        // getAdapter().notifyItemChanged(getAdapterPosition(), PAYLOAD);
    }

    @Nullable
    @Override
    public Boolean getAnswer() {
        Boolean isVerified = getData().isVerified();
        return isVerified != null ? !isVerified : null;
    }
}