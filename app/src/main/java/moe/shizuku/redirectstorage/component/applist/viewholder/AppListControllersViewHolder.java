package moe.shizuku.redirectstorage.component.applist.viewholder;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.component.applist.adapter.AppListAdapter;
import moe.shizuku.redirectstorage.component.applist.adapter.FilterChipsAdapter;
import moe.shizuku.redirectstorage.model.FilterItem;
import moe.shizuku.redirectstorage.widget.SafeFlexboxLayoutManager;
import rikka.core.util.ResourceUtils;
import rikka.core.util.Singleton;
import rikka.recyclerview.BaseViewHolder;

public class AppListControllersViewHolder extends BaseViewHolder<AppListAdapter>
        implements FilterChipsAdapter.FilterChipsCallback {

    public static final Creator<AppListAdapter> CREATOR = (inflater, parent) ->
            new AppListControllersViewHolder(inflater.inflate(R.layout.app_list_controllers_layout, parent, false));

    private static final class DiffCallback extends DiffUtil.Callback {

        private List<FilterItem> oldItems;
        private List<FilterItem> newItems;

        public DiffCallback(List<FilterItem> oldItems, List<FilterItem> newItems) {
            this.oldItems = oldItems;
            this.newItems = newItems;
        }

        @Override
        public int getOldListSize() {
            return oldItems.size();
        }

        @Override
        public int getNewListSize() {
            return newItems.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItems.get(oldItemPosition).titleResourceId == newItems.get(newItemPosition).titleResourceId;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return Objects.equals(oldItems.get(oldItemPosition), newItems.get(newItemPosition));
        }

        @NonNull
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            return FilterChipViewHolder.TOGGLE_PAYLOAD;
        }
    }

    private TextView mEmptyText;
    private RecyclerView mFilterChipsList;
    private View mAddFilterButtonLayout;
    private ImageView mAddFilterButton, mFinishFilterButton;

    private int mAnimationDuration;

    private FilterChipsAdapter mFilterChipsAdapter;

    private static final Object TOGGLE_PAYLOAD = new Object();

    private AppListControllersViewHolder(View itemView) {
        super(itemView);

        mAnimationDuration = itemView.getResources().getInteger(android.R.integer.config_mediumAnimTime);

        mEmptyText = itemView.findViewById(R.id.empty_text);
        mFilterChipsList = itemView.findViewById(android.R.id.list);
        mAddFilterButtonLayout = itemView.findViewById(R.id.add_filter_button_layout);
        mAddFilterButton = itemView.findViewById(R.id.add_filter_button);
        mFinishFilterButton = itemView.findViewById(R.id.finish_filter_button);

        FlexboxLayoutManager flexboxLayoutManager = new SafeFlexboxLayoutManager(itemView.getContext());
        mFilterChipsList.setLayoutManager(flexboxLayoutManager);
        // Items 反复增减动画会使得实际间隔效果异常，暂时在 item 的 layout 里单独设置 margin start 来解决
        // mFilterChipsList.addItemDecoration(new SpacingItemDecoration(itemView.getContext(), 16, LinearLayoutManager.HORIZONTAL));
        mFilterChipsAdapter = new FilterChipsAdapter(this);
        mFilterChipsAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                boolean isEmpty = mFilterChipsAdapter.getItems().isEmpty();
                mEmptyText.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
            }
        });
        DefaultItemAnimator animator = new DefaultItemAnimator() {

        };
        animator.setSupportsChangeAnimations(false);
        mFilterChipsList.setItemAnimator(animator);
        mFilterChipsList.setAdapter(mFilterChipsAdapter);

        mAddFilterButtonLayout.setOnClickListener(v -> {
            getAdapter().setEditingFilter(!getAdapter().isEditingFilter());
            getAdapter().notifyItemChanged(0, TOGGLE_PAYLOAD);
        });
    }

    private class WidthCalculator implements Runnable {

        private boolean ignoredRemovedItems;

        WidthCalculator(boolean ignoredRemovedItems) {
            this.ignoredRemovedItems = ignoredRemovedItems;
        }

        @Override
        public void run() {
            int width = mFilterChipsList.getPaddingStart() + mFilterChipsList.getPaddingEnd();
            for (int i = 0; i < mFilterChipsList.getChildCount(); i++) {
                View view = mFilterChipsList.getChildAt(i);
                if (ignoredRemovedItems && mFilterChipsList.getChildAdapterPosition(view) == RecyclerView.NO_POSITION) {
                    continue;
                }
                View icon = view.findViewById(R.id.check_icon_dot);
                RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
                width += view.getMeasuredWidth() - ((icon.getVisibility() != View.GONE) ? icon.getMeasuredWidth() : 0);
                width += lp.getMarginStart() + lp.getMarginEnd();
            }
            mFilterChipsList.getLayoutParams().width = width;
            mFilterChipsList.requestLayout();
        }
    }


    @Override
    public void onBind() {
        List<FilterItem> newItems = new ArrayList<>();
        if (!getAdapter().isEditingFilter()) {
            mFilterChipsList.post(new WidthCalculator(false));
            // recalculate after animation
            mFilterChipsList.postDelayed(new WidthCalculator(true), mFilterChipsList.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime) + 100);

            mAddFilterButton.setRotation(0f);
            mAddFilterButton.setAlpha(1f);
            mFinishFilterButton.setRotation(0f);
            mFinishFilterButton.setAlpha(0f);
            // mAddFilterButton.setImageResource(R.drawable.ic_action_filter_24dp);
            // mAddFilterButton.setImageTintList(mNormalColorState.get());
            if (Settings.isFilterShowSystem()) {
                newItems.add(new FilterItem(Settings.LIST_FILTER_SHOW_SYSTEM)
                        .setTitle(R.string.action_filter_show_system));
            }
            if (Settings.isFilterShowDisabled()) {
                newItems.add(new FilterItem(Settings.LIST_FILTER_SHOW_DISABLED)
                        .setTitle(R.string.action_filter_show_disabled));
            }
            if (Settings.isFilterHideVerified()) {
                newItems.add(new FilterItem(Settings.LIST_FILTER_HIDE_VERIFIED)
                        .setTitle(R.string.action_filter_hide_verified));
            }
            if (newItems.isEmpty()) {
                mEmptyText.setVisibility(View.VISIBLE);
            } else {
                mEmptyText.setVisibility(View.GONE);
            }
        } else {
            mFilterChipsList.getLayoutParams().width = FrameLayout.LayoutParams.MATCH_PARENT;
            mFilterChipsList.requestLayout();

            mAddFilterButton.setRotation(0f);
            mAddFilterButton.setAlpha(0f);
            mFinishFilterButton.setRotation(0f);
            mFinishFilterButton.setAlpha(1f);
            // mAddFilterButton.setImageResource(R.drawable.ic_done_24dp);
            // mAddFilterButton.setImageTintList(mActivatedColorState.get());
            newItems.add(new FilterItem(Settings.LIST_FILTER_SHOW_SYSTEM)
                    .setTitle(R.string.action_filter_show_system)
                    .setSelected(Settings.isFilterShowSystem())
                    .setEditing(true));
            newItems.add(new FilterItem(Settings.LIST_FILTER_SHOW_DISABLED)
                    .setTitle(R.string.action_filter_show_disabled)
                    .setSelected(Settings.isFilterShowDisabled())
                    .setEditing(true));
            newItems.add(new FilterItem(Settings.LIST_FILTER_HIDE_VERIFIED)
                    .setTitle(R.string.action_filter_hide_verified)
                    .setSelected(Settings.isFilterHideVerified())
                    .setEditing(true));
            mEmptyText.setVisibility(View.GONE);
        }

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffCallback(mFilterChipsAdapter.getItems(), newItems));
        mFilterChipsAdapter.updateItems(newItems, false);
        diffResult.dispatchUpdatesTo(mFilterChipsAdapter);


        // 直接调用 notifyItemRangeChanged 可能会出问题，
        // 无数的 if-else 比较愚蠢，代码可读性有待优化。
        /*if (getAdapter().isEditingFilter() != mFilterChipsAdapter.isEditing()) {
            //DiffUtil.calculateDiff()
            if (getAdapter().isEditingFilter()) {
                if (SRSettings.isFilterShowSystem() && SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemRangeChanged(0, 2, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else if (SRSettings.isFilterShowSystem() && !SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemInserted(1);
                    mFilterChipsAdapter.notifyItemChanged(0, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else if (!SRSettings.isFilterShowSystem() && SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemInserted(0);
                    mFilterChipsAdapter.notifyItemChanged(1, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else {
                    mFilterChipsAdapter.notifyItemRangeInserted(0, 2);
                }
            } else {
                if (SRSettings.isFilterShowSystem() && SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemRangeChanged(0, 2, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else if (SRSettings.isFilterShowSystem() && !SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemRemoved(1);
                    mFilterChipsAdapter.notifyItemChanged(0, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else if (!SRSettings.isFilterShowSystem() && SRSettings.isFilterHideVerified()) {
                    mFilterChipsAdapter.notifyItemRemoved(0);
                    mFilterChipsAdapter.notifyItemChanged(0, FilterChipViewHolder.TOGGLE_PAYLOAD);
                } else {
                    mFilterChipsAdapter.notifyItemRangeRemoved(0, 2);
                }
            }
        } else {
            mFilterChipsAdapter.notifyDataSetChanged();
        }*/
        mFilterChipsAdapter.setEditing(getAdapter().isEditingFilter());
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        for (Object payload : payloads) {
            if (TOGGLE_PAYLOAD.equals(payload)) {
                if (getAdapter().isEditingFilter()) {
                    mFinishFilterButton.setRotation(-90f);
                    mFinishFilterButton.setAlpha(0f);
                    mAddFilterButton.setRotation(0f);
                    mAddFilterButton.setAlpha(1f);
                    mFinishFilterButton.animate()
                            .alpha(1f)
                            .rotation(0f)
                            .setDuration(mAnimationDuration / 2)
                            .start();
                    mAddFilterButton.animate()
                            .alpha(0f)
                            .rotation(90f)
                            .setDuration(mAnimationDuration / 2)
                            .start();
                } else {
                    mFinishFilterButton.setRotation(0f);
                    mFinishFilterButton.setAlpha(1f);
                    mAddFilterButton.setRotation(-90f);
                    mAddFilterButton.setAlpha(0f);
                    mFinishFilterButton.animate()
                            .alpha(0f)
                            .rotation(90f)
                            .setDuration(mAnimationDuration / 2)
                            .start();
                    mAddFilterButton.animate()
                            .alpha(1f)
                            .rotation(0f)
                            .setDuration(mAnimationDuration / 2)
                            .start();
                }
            } else {
                this.onBind();
            }
        }
    }

    @Override
    public void onItemUpdated(FilterItem item) {
        switch (item.id) {
            case Settings.LIST_FILTER_HIDE_VERIFIED:
                Settings.setFilterHideVerified(item.isSelected);
                break;
            case Settings.LIST_FILTER_SHOW_SYSTEM:
                Settings.setFilterShowSystem(item.isSelected);
                break;
            case Settings.LIST_FILTER_SHOW_DISABLED:
                Settings.setFilterShowDisabled(item.isSelected);
                break;
        }
    }

    @Override
    public void onItemDeleted(FilterItem item) {
        switch (item.id) {
            case Settings.LIST_FILTER_HIDE_VERIFIED:
                Settings.setFilterHideVerified(false);
                break;
            case Settings.LIST_FILTER_SHOW_SYSTEM:
                Settings.setFilterShowSystem(false);
                break;
            case Settings.LIST_FILTER_SHOW_DISABLED:
                Settings.setFilterShowDisabled(false);
                break;
        }
    }

    @Override
    public AppListAdapter getAdapter() {
        return (AppListAdapter) super.getAdapter();
    }

    private final Singleton<ColorStateList> mActivatedColorState = new Singleton<ColorStateList>() {
        @Override
        protected ColorStateList create() {
            return ColorStateList.valueOf(ResourceUtils.resolveColor(
                    itemView.getContext().getTheme(), android.R.attr.colorControlActivated));
        }
    };

    private final Singleton<ColorStateList> mNormalColorState = new Singleton<ColorStateList>() {
        @Override
        protected ColorStateList create() {
            return ColorStateList.valueOf(ResourceUtils.resolveColor(
                    itemView.getContext().getTheme(), android.R.attr.textColorPrimary));
        }
    };
}
