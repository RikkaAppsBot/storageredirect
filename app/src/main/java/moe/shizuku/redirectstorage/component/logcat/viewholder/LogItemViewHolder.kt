package moe.shizuku.redirectstorage.component.logcat.viewholder

import android.annotation.SuppressLint
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.view.View.OnCreateContextMenuListener
import android.widget.TextView
import android.widget.Toast
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.model.LogcatItem
import rikka.core.res.resolveColor
import rikka.core.res.resolveColorStateList
import rikka.core.util.ClipboardUtils
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator
import java.util.*

class LogItemViewHolder private constructor(itemView: View) : BaseViewHolder<LogcatItem>(itemView), OnCreateContextMenuListener {

    companion object {

        @JvmField
        val CREATOR = Creator<LogcatItem> { inflater: LayoutInflater, parent: ViewGroup? -> LogItemViewHolder(inflater.inflate(R.layout.logcat_item_layout, parent, false)) }
    }

    private val level: TextView = itemView.findViewById(R.id.logcat_level)
    private val tag: TextView = itemView.findViewById(R.id.logcat_tag)
    private val msg: TextView = itemView.findViewById(R.id.logcat_msg)
    private val date: TextView = itemView.findViewById(R.id.logcat_date)
    private val content: View = itemView.findViewById(android.R.id.content)

    init {
        itemView.setOnCreateContextMenuListener(this)
        itemView.isLongClickable = false
        itemView.setOnClickListener {
            it.showContextMenu()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBind() {
        level.text = data.level
        tag.text = data.tag
        msg.text = data.msg
        date.text = "%s%6d%6d".format(Locale.ENGLISH, data.date, data.pid, data.tid)

        level.backgroundTintList = context.theme.resolveColorStateList(data.colorPrimaryAttr)
        content.setBackgroundColor(context.theme.resolveColor(data.colorSecondaryAttr))
    }

    override fun onCreateContextMenu(
            menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        menu.setHeaderTitle(data.tag)

        MenuInflater(v.context).inflate(R.menu.context_menu_log_item, menu)

        val line = data.line
        menu.findItem(R.id.action_copy).setOnMenuItemClickListener {
            ClipboardUtils.put(itemView.context, line)
            Toast.makeText(itemView.context, R.string.toast_copied_to_clipboard, Toast.LENGTH_SHORT).show()
            return@setOnMenuItemClickListener true
        }
    }
}