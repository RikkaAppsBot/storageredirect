package moe.shizuku.redirectstorage.component.detail

import android.content.Context
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.ktx.logd
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.WeakCache

class AppInfoParseException(message: String) : RuntimeException(message)

object AppInfoCache {

    private inline fun <P, V> createCache(
            noinline keyProducer: (parameter: P) -> String,
            noinline valueProducer: (parameter: P) -> V
    ) = WeakCache(keyProducer, valueProducer)

    private data class AppInfoArgs(val packageName: String, val userId: Int, val context: Context?, val srManager: SRManager?)

    private val appInfoCache = createCache<AppInfoArgs, AppInfo>({
        it.packageName + it.userId
    }, {
        val packageName = it.packageName
        val userId = it.userId
        val context = it.context!!
        val packageManager = context.packageManager
        val sm = it.srManager ?: throw AppInfoParseException("Server is not running.")

        logd("load $userId:$packageName")

        val appInfo: AppInfo
        var info: RedirectPackageInfo? = null
        try {
            info = sm.getRedirectPackageInfo(packageName, SRManager.MATCH_ALL_PACKAGES or SRManager.FLAG_NO_PERMISSION_CHECK or SRManager.GET_PACKAGE_INFO, userId)
        } catch (ignored: Throwable) {
        }

        if (info == null) {
            throw AppInfoParseException("Package $packageName not exists.")
        }

        if (info.packageInfo == null) {
            throw AppInfoParseException("Package $packageName dose not have info.")
        }

        if (!info.writePermission() && !info.readPermission()) {
            var label = info.packageInfo?.applicationInfo?.loadLabel(context.packageManager)
            if (label == null) {
                label = packageName
            }
            throw AppInfoParseException(context.getString(R.string.toast_no_storage_permissions, label))
        }

        appInfo = AppInfo(info, packageManager)

        if (appInfo.sharedUserId != null) {
            SharedUserLabelCache.load(context.packageManager, appInfo.packageInfo.sharedUserLabel, appInfo.sharedUserId!!,
                    appInfo.packageName, appInfo.applicationInfo!!)
        }

        appInfo
    })

    operator fun set(packageName: String, userId: Int, value: AppInfo) {
        logd("put $userId:$packageName")
        appInfoCache[AppInfoArgs(packageName, userId, null, null)] = value
    }

    operator fun get(packageName: String, userId: Int, context: Context, sm: SRManager): AppInfo {
        return appInfoCache[AppInfoArgs(packageName, userId, context, sm)]
    }
}
