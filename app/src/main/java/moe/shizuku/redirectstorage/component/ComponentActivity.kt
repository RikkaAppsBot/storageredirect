package moe.shizuku.redirectstorage.component

import android.content.ComponentName
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity

class ComponentActivity : AppBarFragmentActivity() {

    companion object {

        const val EXTRA_CLASS = BuildConfig.APPLICATION_ID + ".extra.CLASS"
        const val EXTRA_ARGUMENT = BuildConfig.APPLICATION_ID + ".extra.ARGUMENT"

        @JvmStatic
        fun newIntent(cls: Class<out Fragment>, arguments: Bundle?): Intent {
            return Intent()
                    .setComponent(ComponentName(BuildConfig.APPLICATION_ID, ComponentActivity::class.java.name))
                    .putExtra(EXTRA_CLASS, cls.name)
                    .putExtra(EXTRA_ARGUMENT, arguments)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            val className = intent.getStringExtra(EXTRA_CLASS)
            val arguments = intent.getBundleExtra(EXTRA_ARGUMENT)
            if (className == null) {
                finish()
                return
            }

            val fragment: Fragment
            try {
                fragment = Class.forName(className).newInstance() as Fragment
            } catch (e: ReflectiveOperationException) {
                e.printStackTrace()
                finish()
                return
            }

            fragment.arguments = arguments

            if (fragment is DialogFragment) {
                fragment.show(supportFragmentManager, "dialog")
            } else {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun shouldApplyTranslucentSystemBars(): Boolean {
        return true
    }

    override fun onApplyTranslucentSystemBars() {
        super.onApplyTranslucentSystemBars()
        window?.statusBarColor = Color.TRANSPARENT
    }
}
