package moe.shizuku.redirectstorage.component.filemonitor.adapter;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.detail.viewholder.CollapsibleSummaryViewHolder;
import moe.shizuku.redirectstorage.component.filemonitor.viewholder.IOHistoryLoadMoreViewHolder;
import moe.shizuku.redirectstorage.component.filemonitor.viewholder.IOHistoryViewHolder;
import moe.shizuku.redirectstorage.model.FileMonitorRecord;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.ClassCreatorPool;

public class FileMonitorAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    public FileMonitorAdapter() {
        super();

        setHasStableIds(true);
        getCreatorPool()
                .putRule(FileMonitorRecord.class, IOHistoryViewHolder.CREATOR)
                .putRule(Long.class, IOHistoryLoadMoreViewHolder.CREATOR)
                .putRule(CharSequence.class, (BaseViewHolder.Creator) SettingsDescriptionViewHolder.NO_ICON_STYLE_CREATOR)
                .putRule(Integer.class, (BaseViewHolder.Creator) SettingsDescriptionViewHolder.NO_ICON_STYLE_CREATOR)
                .putRule(Object[].class, (BaseViewHolder.Creator) CollapsibleSummaryViewHolder.CREATOR)
                .putRule(Object.class, DividerViewHolder.CREATOR);
    }

    @Override
    public long getItemId(int position) {
        Object item = getItemAt(position);
        if (item instanceof FileMonitorRecord) {
            return ((FileMonitorRecord) item).id;
        } else if (item instanceof Long) {
            return -3 - (long) item;
        } else if (item instanceof CharSequence) {
            return -2;
        }
        return 0;
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }

    public void updateItems(@NonNull Context context, @NonNull List<?> data) {
        getItems().clear();
        getItems().add(new Object[]{
                context.getString(R.string.file_monitor_help_title),
                HtmlCompat.fromHtml(context.getString(R.string.file_monitor_help_content), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE | HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM, null, SizeTagHandler.getInstance()),
                "file_monitor_show_help"});
        getItems().add(new Object());
        getItems().addAll(data);
        notifyDataSetChanged();
    }
}
