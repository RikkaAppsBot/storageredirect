package moe.shizuku.redirectstorage.component.logcat.dialog

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import me.zhanghai.android.fastscroll.FastScrollWebView
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarDialogFragment
import moe.shizuku.redirectstorage.ktx.addFastScroller
import rikka.core.res.isNight
import rikka.material.app.AppBar
import java.io.File

class LogViewDialog : AppBarDialogFragment() {

    companion object {

        @JvmStatic
        fun newInstance(data: File): LogViewDialog {
            val dialog = LogViewDialog()
            val bundle = Bundle()
            bundle.putString(EXTRA_DATA, data.absolutePath)
            dialog.arguments = bundle
            return dialog
        }
    }

    private var path: String? = null
    private lateinit var webView: FastScrollWebView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        path = arguments!!.getString(EXTRA_DATA)
    }

    override fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {
        appBar.setTitle(R.string.view_log)
        appBar.setDisplayHomeAsUpEnabled(true)
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        appBar.setRaised(true)
    }

    override fun onCreateContainerView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.logcat_history_view_dialog_content, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onContainerViewCreated(view: View, savedInstanceState: Bundle?) {
        progressBar = view.findViewById(android.R.id.progress)
        webView = view.findViewById(R.id.webview)
        webView.addFastScroller()
        webView.setBackgroundColor(Color.TRANSPARENT)
        webView.settings.allowFileAccess = true
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                val color = if (webView.resources.configuration.isNight()) "white" else "black"
                webView.loadUrl("""javascript:document.body.style.setProperty("color", "$color");""")
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar.visibility = View.GONE
            }
        }
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                progressBar.progress = progress
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        webView.loadUrl("file:///$path")
    }
}