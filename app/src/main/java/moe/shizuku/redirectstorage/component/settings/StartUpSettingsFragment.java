package moe.shizuku.redirectstorage.component.settings;

import android.os.Bundle;
import android.widget.Toast;

import moe.shizuku.preference.SwitchPreference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import rikka.core.util.BuildUtils;

public class StartUpSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.START_UP";

    @Override
    public boolean isVerticalPaddingRequired() {
        return true;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_start);

        SwitchPreference killMediaStorageOnStartPreference = (SwitchPreference) findPreference("kill_media_storage_on_start");
        if (BuildUtils.INSTANCE.getAtLeast30()) {
            killMediaStorageOnStartPreference.setVisible(false);
        } else {
            SRManager srm = SRManager.create();
            if (srm == null) {
                killMediaStorageOnStartPreference.setEnabled(false);
            } else {
                try {
                    killMediaStorageOnStartPreference.setChecked(srm.isKillMediaStorageOnStartEnabled());
                } catch (Throwable ignored) {
                }
            }

            killMediaStorageOnStartPreference.setOnPreferenceChangeListener(((preference, newValue) -> {
                if (srm == null || !(newValue instanceof Boolean))
                    return false;

                boolean enabled = (boolean) newValue;
                try {
                    srm.setKillMediaStorageOnStartEnabled(enabled);
                } catch (Throwable throwable) {
                    Toast.makeText(requireContext(), getString(R.string.toast_failed, throwable.toString()), Toast.LENGTH_SHORT).show();
                    return false;
                }
                return true;
            }));
        }
    }
}
