package moe.shizuku.redirectstorage.component.restore.fragment;

import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.component.restore.RestoreWizardActivity;
import moe.shizuku.redirectstorage.component.restore.service.BackupRestoreTaskService;
import moe.shizuku.redirectstorage.component.settings.SettingsFragment;
import moe.shizuku.redirectstorage.utils.ForegroundService;
import rikka.internal.help.HelpEntity;
import rikka.internal.help.HelpProvider;

import static android.app.Activity.RESULT_OK;
import static moe.shizuku.redirectstorage.AppConstants.ACTION_BACKUP;
import static moe.shizuku.redirectstorage.AppConstants.ACTION_BACKUP_FINISHED;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_ID_BACKUP;
import static moe.shizuku.redirectstorage.AppConstants.TAG;

public class BackupRestoreSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.BACKUP_RESTORE";

    private static final String BACKUP_FILE_DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";

    private static final int REQUEST_CODE_CREATE = 10;
    private static final int REQUEST_CODE_RESTORE = 11;

    private final BroadcastReceiver mFinishActionReceiver = new FinishActionReceiver();

    @Override
    public boolean isVerticalPaddingRequired() {
        return false;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_backup_restore);

        SRManager mSRManager = SRManager.create();
        if (mSRManager == null) {
            new AlertDialog.Builder(requireActivity())
                    .setTitle(R.string.dialog_cannot_backup_title)
                    .setMessage(R.string.toast_unable_connect_server)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .setOnDismissListener(dialog -> requireActivity().finish())
                    .show();
        }

        findPreference("create_backup").setOnPreferenceClickListener(p -> {
            startCreateBackup();
            return true;
        });
        findPreference("restore").setOnPreferenceClickListener(p -> {
            startRestoreBackup();
            return true;
        });
        findPreference("help").setOnPreferenceClickListener(p -> {
            Context context = requireContext();
            HelpEntity entity = HelpProvider.get(context, "how_to_document");
            if (entity != null) {
                entity.startActivity(context);
            } else {
                Toast.makeText(context, R.string.toast_cannot_load_help, Toast.LENGTH_SHORT).show();
            }
            return true;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(requireContext())
                .registerReceiver(mFinishActionReceiver, new IntentFilter(ACTION_BACKUP_FINISHED));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(requireContext())
                .unregisterReceiver(mFinishActionReceiver);
    }

    public void startCreateBackup() {
        SimpleDateFormat format = new SimpleDateFormat(BACKUP_FILE_DATE_FORMAT, Locale.getDefault());

        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_TITLE, "StorageIsolation_"
                + format.format(Calendar.getInstance().getTime())
                + ".srbackup");
        try {
            startActivityForResult(intent, REQUEST_CODE_CREATE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            // TODO Show error for users
        }
    }

    private void handleCreateBackup(@NonNull Uri uri) {
        Intent intent = new Intent(requireContext(), BackupRestoreTaskService.class);
        intent.setAction(ACTION_BACKUP);
        intent.setData(uri);
        ForegroundService.startOrNotify(requireContext(), intent);
    }

    public void startRestoreBackup() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        try {
            startActivityForResult(intent, REQUEST_CODE_RESTORE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            // TODO Show error for users
        }
    }

    private void handleRestoreBackup(@NonNull Uri uri) {
        Intent intent = new Intent(requireContext(), RestoreWizardActivity.class);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (REQUEST_CODE_CREATE == requestCode && RESULT_OK == resultCode && data != null) {
            Uri uri = data.getData();
            if (uri == null) {
                // TODO Show error for users
                return;
            }
            Log.d(TAG, "Create result uri: " + uri.toString());
            handleCreateBackup(uri);
        } else if (REQUEST_CODE_RESTORE == requestCode && RESULT_OK == resultCode && data != null) {
            Uri uri = data.getData();
            if (uri == null) {
                // TODO Show error for users
                return;
            }
            Log.d(TAG, "Open for restore result uri: " + uri.toString());
            handleRestoreBackup(uri);
        }
    }

    private class FinishActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context appContext, Intent intent) {
            if (intent == null || !ACTION_BACKUP_FINISHED.equals(intent.getAction())) {
                return;
            }
            new AlertDialog.Builder(requireActivity())
                    .setTitle(R.string.notification_backup_finished_title)
                    .setMessage(intent.getStringExtra("message"))
                    .setPositiveButton(android.R.string.ok, null)
                    .setOnDismissListener(dialog -> {
                        try {
                            NotificationManager nm = appContext.getSystemService(NotificationManager.class);
                            if (nm != null) {
                                nm.cancel(NOTIFICATION_ID_BACKUP);
                            }
                        } catch (Exception ignored) {
                        }
                    })
                    .show();
        }

    }
}
