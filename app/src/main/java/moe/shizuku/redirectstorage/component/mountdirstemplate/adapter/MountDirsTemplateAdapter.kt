package moe.shizuku.redirectstorage.component.mountdirstemplate.adapter

import android.content.Context
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder.AddButtonViewHolder
import moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder.TemplateItemViewHolder
import moe.shizuku.redirectstorage.viewholder.ErrorViewHolder
import moe.shizuku.redirectstorage.viewholder.LoadingViewHolder
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder
import moe.shizuku.redirectstorage.viewmodel.Resource
import moe.shizuku.redirectstorage.viewmodel.Status
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.IdBasedRecyclerViewAdapter

class MountDirsTemplateAdapter : IdBasedRecyclerViewAdapter() {

    interface Listener {

        fun onAddClick()

        fun onClick(data: MountDirsTemplate)
    }

    var listener: Listener? = null

    fun setData(context: Context, data: Resource<List<MountDirsTemplate>>) {
        clear()

        addItem(SettingsDescriptionViewHolder.OUTLINE_STYLE_CREATOR, context.getString(R.string.custom_mount_dirs_formula_description, context.getString(R.string.detail_accessible_folders)).toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE), ID_DESCRIPTION.toLong())
        when (data.status) {
            Status.SUCCESS -> {
                addItem(AddButtonViewHolder.CREATOR, null, ID_NEW_ITEM.toLong())
                if (!data.data.isNullOrEmpty()) {
                    addItems(TemplateItemViewHolder.CREATOR, data.data, data.data.map { ID_ITEM_PREFIX.toLong() + it.hashCode() })
                }
            }
            Status.ERROR -> {
                addItem(ErrorViewHolder.LIST_ITEM_STYLE_CREATOR, null, ID_ERROR.toLong())
            }
            Status.LOADING -> {
                addItem(LoadingViewHolder.LIST_ITEM_STYLE_CREATOR, null, ID_LOADING.toLong())
            }
        }


        notifyDataSetChanged()
    }

    companion object {

        const val ID_DESCRIPTION = 1
        const val ID_NEW_ITEM = 2
        const val ID_LOADING = 3
        const val ID_ERROR = 4
        const val ID_ITEM_PREFIX = 100
    }

}
