package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.ObserverInfo
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarDialogFragment
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.model.ObserverInfoBuilder
import rikka.material.app.AppBar
import rikka.recyclerview.fixEdgeEffect

open class ExportFolderEditFragment : AppBarDialogFragment(),
        ObserverDescriptionChooseDialog.Listener,
        ExportFolderRuleEditAdapter.Listener {

    interface Listener {

        fun onRequestAddObserver(info: ObserverInfo): Boolean

        fun onRequestUpdateObserver(new: ObserverInfo, old: ObserverInfo): Boolean

        fun onRequestDeleteObserver(info: ObserverInfo): Boolean
    }

    private var appInfo: AppInfo? = null
    private var observerInfo: ObserverInfo? = null
    private var reason: Int = 0

    lateinit var builder: ObserverInfoBuilder
        protected set

    private lateinit var adapter: ExportFolderRuleEditAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasDialogOptionsMenu(true)

        val arguments = requireArguments()

        appInfo = arguments.getParcelable(EXTRA_DATA)
        observerInfo = arguments.getParcelable(EXTRA_OBSERVER)
        reason = arguments.getInt(EXTRA_EDIT_REASON)

        builder = if (savedInstanceState == null) {
            if (observerInfo == null) {
                ObserverInfoBuilder(appInfo!!.packageName, appInfo!!.userId)
            } else {
                ObserverInfoBuilder(observerInfo!!)
            }
        } else {
            savedInstanceState.getParcelable(STATE_BUILDER)!!
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(STATE_BUILDER, builder)
    }

    override fun onBackPressed(): Boolean {
        if (reason != EDIT_REASON_VIEW_ONLINE && builder.changed) {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.exit_confirm_dialog_title)
                    .setMessage(R.string.exit_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        dismiss()
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .setNeutralButton(R.string.exit_confirm_dialog_button_save) { _, _ ->
                        onSaveClick()
                    }
                    .show()
            return false
        }
        return super.onBackPressed()
    }

    override fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        appBar.setDisplayHomeAsUpEnabled(true)

        val title: Int = when (reason) {
            EDIT_REASON_ADD -> R.string.observer_info_edit_dialog_add_title
            EDIT_REASON_EDIT -> R.string.observer_info_edit_dialog_edit_title
            EDIT_REASON_VIEW_ONLINE -> R.string.observer_info_edit_dialog_view_online_title
            else -> throw IllegalArgumentException()
        }
        appBar.setTitle(title)
    }

    override fun onCreateContainerView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.appbar_container_recycler, container, false)
    }

    override fun onContainerViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val recyclerView = view.findViewById<RecyclerView>(android.R.id.content)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.isEnabled = reason != EDIT_REASON_VIEW_ONLINE

        adapter = ExportFolderRuleEditAdapter(context, builder, appInfo?.label ?: "")
        adapter.listener = this
        recyclerView.adapter = adapter
        recyclerView.fixEdgeEffect()
    }

    override fun onCreateDialogOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.observer_info_edit_menu, menu)
    }

    override fun onPrepareDialogOptionsMenu(menu: Menu) {
        val delete = menu.findItem(R.id.action_delete)
        val save = menu.findItem(R.id.action_save)
        val add = menu.findItem(R.id.action_add)
        when (reason) {
            EDIT_REASON_ADD -> {
                delete.isVisible = false
                save.isVisible = true
                add.isVisible = false
            }
            EDIT_REASON_EDIT -> {
                delete.isVisible = true
                save.isVisible = true
                add.isVisible = false
            }
            EDIT_REASON_VIEW_ONLINE -> {
                delete.isVisible = false
                save.isVisible = false
                add.isVisible = true
            }
        }
    }

    override fun onDialogOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                onSaveClick()
                true
            }
            R.id.action_delete -> {
                onDeleteClick()
                true
            }
            R.id.action_add -> {
                onSaveClick()
                true
            }
            else -> super.onDialogOptionsItemSelected(item)
        }
    }

    private fun onSaveClick() {
        var res = false
        if (!checkRequiredValuesAndToast()) {
            return
        }

        if (reason == EDIT_REASON_VIEW_ONLINE) {
            observerInfo!!.enabled = true
            if (parentFragment is Listener) {
                res = (parentFragment as Listener).onRequestAddObserver(observerInfo!!)
            }
        } else {
            val result = builder.build()
            if (observerInfo != null) {
                result.enabled = observerInfo!!.enabled
                if (parentFragment is Listener) {
                    res = (parentFragment as Listener).onRequestUpdateObserver(result, observerInfo!!)
                }
            } else {
                result.enabled = true
                if (parentFragment is Listener) {
                    res = (parentFragment as Listener).onRequestAddObserver(result)
                }
            }
        }

        if (res) {
            dismiss()
        }
    }

    private fun onDeleteClick() {
        if (reason == EDIT_REASON_EDIT) {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.delete_confirm_dialog_title)
                    .setMessage(R.string.delete_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        if (parentFragment is Listener) {
                            if ((parentFragment as Listener).onRequestDeleteObserver(observerInfo!!)) {
                                dismiss()
                            }
                        }
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .show()
        } else {
            Log.w(AppConstants.TAG, "Delete operation is only supported in edit mode.",
                    UnsupportedOperationException())
            dismiss()
        }
    }

    protected open fun checkRequiredValuesAndToast(): Boolean {
        if (builder.source == null) {
            Toast.makeText(requireContext(), R.string.toast_source_path_is_empty, Toast.LENGTH_SHORT).show()
            return false
        }
        if (builder.target == null) {
            Toast.makeText(requireContext(), R.string.toast_target_path_is_empty, Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    override fun onObserverDescriptionChoose(description: String) {
        builder.description(description)
        adapter.notifyItemChangeById(ExportFolderRuleEditAdapter.ID_DESCRIPTION.toLong())
    }

    override fun onEditDescriptionClick(defaultChoice: String) {
        ObserverDescriptionChooseDialog.newInstance(defaultChoice)
                .show(childFragmentManager)
    }

    override fun onEditSourcePathClick() {
        ObserverSourcePathPickerDialog.newInstance(appInfo!!, builder.source)
                .show(childFragmentManager)
    }

    override fun onEditTargetPathClick() {
        ObserverTargetPathPickerDialog.newInstance(emptyList(), appInfo!!.userId, builder.target)
                .show(childFragmentManager)
    }

    override fun onEditMaskClick() {
        ObserverMaskEditDialog.newInstance(builder.mask).show(childFragmentManager)
    }

    fun onSourcePathPick(source: String) {
        builder.source(source)
        adapter.notifyItemChangeById(ExportFolderRuleEditAdapter.ID_SOURCE_PATH.toLong())
    }

    fun onTargetPathPick(target: String) {
        builder.target(target)
        adapter.notifyItemChangeById(ExportFolderRuleEditAdapter.ID_TARGET_PATH.toLong())
    }

    fun onMaskSet(mask: String?) {
        builder.mask(if (TextUtils.isEmpty(mask)) null else mask)
        adapter.notifyItemChangeById(ExportFolderRuleEditAdapter.ID_MASK.toLong())
    }

    companion object {

        const val EXTRA_EDIT_REASON = BuildConfig.APPLICATION_ID + "..extra.REASON"
        const val EXTRA_APP_INFO = BuildConfig.APPLICATION_ID + ".extra.APP_INFO"
        const val EXTRA_OBSERVER = BuildConfig.APPLICATION_ID + ".extra.OBSERVER"
        const val STATE_BUILDER = BuildConfig.APPLICATION_ID + ".state.BUILDER"

        const val EDIT_REASON_ADD = 0
        const val EDIT_REASON_EDIT = 1
        const val EDIT_REASON_VIEW_ONLINE = 2

        fun newInstanceForAdd(appInfo: AppInfo): ExportFolderEditFragment {
            val args = Bundle()
            args.putParcelable(EXTRA_DATA, appInfo)
            args.putInt(EXTRA_EDIT_REASON, EDIT_REASON_ADD)

            val dialog = ExportFolderEditFragment()
            dialog.arguments = args
            return dialog
        }

        fun newInstanceForEdit(appInfo: AppInfo, observer: ObserverInfo): ExportFolderEditFragment {
            val args = Bundle()
            args.putParcelable(EXTRA_DATA, appInfo)
            args.putParcelable(EXTRA_OBSERVER, observer)
            args.putInt(EXTRA_EDIT_REASON, EDIT_REASON_EDIT)

            val dialog = ExportFolderEditFragment()
            dialog.arguments = args
            return dialog
        }

        fun newInstanceForViewOnline(appInfo: AppInfo, observer: ObserverInfo): ExportFolderEditFragment {
            val args = Bundle()
            args.putParcelable(EXTRA_DATA, appInfo)
            args.putParcelable(EXTRA_OBSERVER, observer)
            args.putInt(EXTRA_EDIT_REASON, EDIT_REASON_VIEW_ONLINE)

            val dialog = ExportFolderEditFragment()
            dialog.arguments = args
            return dialog
        }
    }
}
