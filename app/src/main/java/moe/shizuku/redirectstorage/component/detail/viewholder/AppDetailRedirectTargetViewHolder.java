package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.dialog.RedirectTargetSelectDialog;
import moe.shizuku.redirectstorage.model.AppInfo;
import moe.shizuku.redirectstorage.utils.WordsHelper;
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder;
import rikka.core.util.ContextUtils;

public class AppDetailRedirectTargetViewHolder extends SimpleItemViewHolder<AppInfo> {

    public static final Creator<AppInfo> CREATOR = (inflater, parent) -> new AppDetailRedirectTargetViewHolder(inflater.inflate(R.layout.detail_simple_item, parent, false));

    public AppDetailRedirectTargetViewHolder(View itemView) {
        super(itemView);

        final Context context = itemView.getContext();
        title.setText(R.string.detail_redirect_target_title);
        icon.setImageDrawable(context.getDrawable(R.drawable.ic_detail_view_redirect_storage_24dp));
    }

    @Override
    public void onClick(View view) {
        Context context = view.getContext();
        FragmentActivity activity = ContextUtils.getActivity(context);
        if (activity != null) {
            RedirectTargetSelectDialog.newInstance(getData().getRedirectInfo().packageName, getData().getSharedUserId(), getData().getUserId())
                    .show(activity.getSupportFragmentManager());
        }
    }

    @Override
    public void onBind() {
        updateSummary();

        itemView.setEnabled(getData().isRedirectEnabled());
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        updateSummary();
    }

    private void updateSummary() {
        Context context = itemView.getContext();
        summary.setText(WordsHelper.getHumanReadableRedirectTargetTitle(context, getData().getRedirectInfo()));
    }
}
