package moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.mountdirstemplate.adapter.MountDirsTemplateAdapter
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder

class AddButtonViewHolder private constructor(itemView: View) : SimpleItemViewHolder<Any>(itemView) {

    init {
        icon.setImageResource(R.drawable.ic_add_24dp)
        title.setText(R.string.custom_mount_dirs_formula_add)
        summary.visibility = View.GONE
    }

    override fun getAdapter(): MountDirsTemplateAdapter {
        return super.getAdapter() as MountDirsTemplateAdapter
    }

    override fun onClick(view: View) {
        adapter.listener?.onAddClick()
    }

    companion object {

        val CREATOR = { inflater: LayoutInflater, parent: ViewGroup -> AddButtonViewHolder(inflater.inflate(R.layout.detail_edit_simple_item, parent, false)) }
    }

}
