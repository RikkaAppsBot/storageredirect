package moe.shizuku.redirectstorage.component.exportfolders.editor

import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doOnTextChanged
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment
import moe.shizuku.redirectstorage.dialog.EditTextDialogFragment
import moe.shizuku.redirectstorage.ktx.AlertDialogChoiceAdapter
import moe.shizuku.redirectstorage.ktx.setSingleChoiceItems
import moe.shizuku.redirectstorage.model.AppRecommendationTexts

class ObserverDescriptionChooseDialog : AlertDialogFragment() {

    interface Listener {
        fun onObserverDescriptionChoose(description: String)
    }

    private lateinit var initialChoiceValue: String
    private lateinit var choiceValue: String
    private var choiceIndex: Int = -1
    private lateinit var choicesValues: MutableList<String>
    private lateinit var choices: MutableList<String>
    private lateinit var adapter: AlertDialogChoiceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppRecommendationTexts.loadLocal(requireContext())

        initialChoiceValue = requireArguments().getString(KEY_VALUE) ?: ""
        choiceValue = savedInstanceState?.getString(KEY_VALUE) ?: initialChoiceValue
        choiceIndex = savedInstanceState?.getInt(KEY_CHOICE, choiceIndex) ?: choiceIndex
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_VALUE, choiceValue)
        outState.putInt(KEY_CHOICE, choiceIndex)
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        choicesValues = AppRecommendationTexts.exportFolderTitle!!.keys.map { "@$it" }.toMutableList()
        choices = AppRecommendationTexts.exportFolderTitle!!.values.map { it.get() }.toMutableList()

        if (!choiceValue.startsWith("@")) {
            choicesValues.add(0, choiceValue)
            choices.add(0, choiceValue)
        } else {
            choicesValues.add(0, "")
            choices.add(0, getString(R.string.export_folder_rule_description_custom))
        }

        if (choiceIndex == -1) {
            choiceIndex = choicesValues.indexOf(choiceValue)
        }

        builder.setTitle(R.string.observer_info_edit_dialog_description)
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            val parent = parentFragment
            if (parent is Listener && choiceIndex != -1) {
                parent.onObserverDescriptionChoose(choiceValue)
            }
        }
        builder.setNegativeButton(android.R.string.cancel, null)

        adapter = builder.setSingleChoiceItems(choices, choiceIndex) { which: Int ->
            if (which == 0) {
                ExportFolderRuleCustomDescriptionDialogFragment.newInstance(choicesValues[0]).show(childFragmentManager)
            } else {
                choiceIndex = which
                choiceValue = choicesValues[which]
                setPositiveButtonEnabled(choiceValue.isNotBlank())
            }
        }
    }

    override fun onShow(dialog: AlertDialog) {
        setPositiveButtonEnabled(choiceIndex != -1)
    }

    fun onCustomChanged(newValue: String?) {
        newValue ?: return

        choiceValue = newValue
        choicesValues[0] = newValue
        choices[0] = newValue

        adapter.setAll(choices)
        //dialog!!.listView.adapter = createAdapter(dialog!!.context)
    }

    companion object {

        private const val KEY_VALUE = AppConstants.EXTRA_PREFIX + ".value"
        private const val KEY_CHOICE = AppConstants.EXTRA_PREFIX + ".choice"

        fun newInstance(defaultChoice: String): ObserverDescriptionChooseDialog {
            val args = Bundle()
            args.putString(KEY_VALUE, defaultChoice)
            val dialog = ObserverDescriptionChooseDialog()
            dialog.arguments = args
            return dialog
        }
    }
}

class ExportFolderRuleCustomDescriptionDialogFragment : EditTextDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        super.onBuildAlertDialog(builder, savedInstanceState)
        builder.setTitle(R.string.custom)
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        super.onAlertDialogCreated(dialog, contentView, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            editText.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO
        }

        editText.imeOptions = EditorInfo.IME_ACTION_DONE or EditorInfo.IME_FLAG_NO_ENTER_ACTION
        editText.filters = arrayOf(object : InputFilter {
            override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence {
                return source.toString().replace("\n", "").replace("\r", "")
            }

        })
        editText.doOnTextChanged { _, _, _, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = !value.isNullOrBlank()
        }
    }

    override fun onShow(dialog: AlertDialog) {
        super.onShow(dialog)

        if (value.isNullOrBlank()) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        }
    }

    override fun onDone() {
        super.onDone()

        (parentFragment as? ObserverDescriptionChooseDialog)?.onCustomChanged(value)
    }

    companion object {

        fun newInstance(value: String): ExportFolderRuleCustomDescriptionDialogFragment {
            val args = Bundle()
            args.putCharSequence(ARG_INITIAL_VALUE, value)
            val dialog = ExportFolderRuleCustomDescriptionDialogFragment()
            dialog.arguments = args
            return dialog
        }
    }
}