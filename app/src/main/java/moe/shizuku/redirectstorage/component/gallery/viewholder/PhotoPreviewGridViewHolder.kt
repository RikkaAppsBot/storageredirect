package moe.shizuku.redirectstorage.component.gallery.viewholder

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.gallery.model.PreviewModel
import moe.shizuku.redirectstorage.utils.AppIconCache
import moe.shizuku.redirectstorage.utils.MediaStoreUtils
import moe.shizuku.redirectstorage.viewholder.BaseCoroutinesViewHolder
import rikka.core.res.resolveColor
import rikka.recyclerview.BaseViewHolder.Creator

class PhotoPreviewGridViewHolder(
        recyclerView: RecyclerView,
        itemView: View
) : BaseCoroutinesViewHolder<PreviewModel, Any?>(itemView) {

    companion object {

        fun newCreator(recyclerView: RecyclerView) =
                Creator<PreviewModel> { inflater, parent ->
                    PhotoPreviewGridViewHolder(recyclerView, inflater.inflate(
                            R.layout.gallery_folders_list_photo_preview_grid_item, parent, false))
                }

    }

    private val imageView: ImageView = itemView.findViewById(R.id.image_view)
    private val indicatorLayout: View = itemView.findViewById(R.id.indicator_layout)
    private val invisibleIndicator: ImageView = itemView.findViewById(R.id.invisible_indicator)
    private val moreIndicator: TextView = itemView.findViewById(R.id.more_indicator)

    private var imageSize: Int = 0
    private val recyclerView: RecyclerView

    init {
        itemView.setOnClickListener {
            if ((data?.moreCount ?: 0) > 0) {
                return@setOnClickListener
            }
            data?.let {
                try {
                    context.startActivity(MediaStoreUtils.getViewIntent(it.data.id))
                } catch (e: Exception) {
                    // TODO Show toast
                    e.printStackTrace()
                }
            }
        }

        this.recyclerView = recyclerView
        calcImageSize()
    }

    private fun loadImage() {
        launch {
            try {
                val bitmap = withContext(AppIconCache.dispatcher()) {
                    data.getThumbnail(imageSize)
                }
                imageView.setImageBitmap(bitmap)
            } catch (e: CancellationException) {
                // do nothing if canceled
            } catch (e: Exception) {
                e.printStackTrace()
                imageView.setImageBitmap(null)
            }
        }
    }

    private fun calcImageSize(loadImage: Boolean = false) {
        if (imageSize != 0) {
            if (loadImage)
                loadImage()
            return
        }

        recyclerView.doOnLayout {
            val gridLayoutManager = recyclerView.layoutManager as GridLayoutManager
            imageSize = recyclerView.measuredWidth / gridLayoutManager.spanCount
            imageView.layoutParams.height = imageSize

            if (loadImage)
                loadImage()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBind() {
        calcImageSize(true)

        val gridLayoutManager = recyclerView.layoutManager as GridLayoutManager
        val spanCount = gridLayoutManager.spanCount
        val x = data.orderInCategory ?: adapterPosition
        val y = ((x / spanCount) * ((spanCount + 1) % 2) + x) % 2
        itemView.setBackgroundColor(getPlaceholderBackground(y == 0))


        when {
            data.moreCount > 0 -> {
                indicatorLayout.visibility = View.VISIBLE
                invisibleIndicator.visibility = View.GONE
                moreIndicator.visibility = View.VISIBLE
                moreIndicator.text = "+${data.moreCount}"
            }
            !data.inMountDirs -> {
                indicatorLayout.visibility = View.VISIBLE
                invisibleIndicator.visibility = View.VISIBLE
                moreIndicator.visibility = View.GONE
            }
            else -> {
                indicatorLayout.visibility = View.GONE
            }
        }

        imageView.setImageBitmap(null)
    }

    @ColorInt
    private fun getPlaceholderBackground(light: Boolean = true): Int {
        return context.theme.resolveColor(
                if (light)
                    R.attr.galleryPlaceholderBackgroundLighter
                else
                    R.attr.galleryPlaceholderBackgroundDarker
        )
    }

}