package moe.shizuku.redirectstorage.component.applist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.transition.TransitionManager
import android.view.*
import android.view.MenuItem.OnActionExpandListener
import android.view.animation.Animation
import android.widget.Button
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import moe.shizuku.redirectstorage.AppConstants.*
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.adapter.LoadingAdapter
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.applist.adapter.AppListAdapter
import moe.shizuku.redirectstorage.component.home.FailedFragment
import moe.shizuku.redirectstorage.component.settings.SettingsActivity
import moe.shizuku.redirectstorage.component.settings.StartUpSettingsFragment
import moe.shizuku.redirectstorage.dialog.ExceptionDialog
import moe.shizuku.redirectstorage.ktx.setItemsVisible
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.SourcedResource
import moe.shizuku.redirectstorage.viewmodel.Status.*
import moe.shizuku.redirectstorage.viewmodel.activitySharedViewModels
import moe.shizuku.redirectstorage.widget.SingleOnClickListener
import rikka.core.ktx.unsafeLazy
import rikka.core.res.resolveColor
import rikka.core.res.resolveDimension
import rikka.core.widget.SearchViewCallback
import rikka.html.widget.HtmlCompatTextView
import rikka.recyclerview.addFastScroller
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderView
import java.util.*

class AppListFragment : AppFragment(), SearchViewCallback {

    companion object {

        @JvmStatic
        fun newInstance(userId: Int): AppListFragment {
            return AppListFragment().also { it.arguments = bundleOf(EXTRA_USER_ID to userId) }
        }
    }

    private var userId: Int = 0

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private val listAdapter: AppListAdapter by unsafeLazy { AppListAdapter() }
    private val loadingAdapter: LoadingAdapter by unsafeLazy { LoadingAdapter() }
    private val model: AppListViewModel by activitySharedViewModels { AppListViewModel() }
    private lateinit var banner: ViewGroup

    private val deathListener = SRManager.DeathListener {
        refresh()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        userId = arguments?.getInt(EXTRA_USER_ID) ?: UserHelper.currentUserId()

        model.userId = userId

        if (model.bannerCreators == null) {
            model.bannerCreators = LinkedList<Class<BannerCreator>>()
        }

        registerLocalBroadcastReceiver(ACTION_REQUEST_REFRESH_LIST, this::onRefresh)
        registerLocalBroadcastReceiver(ACTION_SERVER_STARTED, this::onServerStarted)

        SRManager.addDeathListener(deathListener)
    }

    override fun onDestroy() {
        super.onDestroy()

        SRManager.removeDeathListener(deathListener)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        model.isEditingFilter = listAdapter.isEditingFilter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.app_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context

        swipeRefreshLayout = view.findViewById(R.id.swiperefresh)
        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }
        swipeRefreshLayout.setColorSchemeColors(
                context.theme.resolveColor(android.R.attr.colorAccent))
        val actionBarSize = context.theme.resolveDimension(R.attr.actionBarSize, 0f).toInt()
        swipeRefreshLayout.setProgressViewOffset(false, actionBarSize, (64 * resources.displayMetrics.density + actionBarSize).toInt())

        listAdapter.isEditingFilter = model.isEditingFilter

        recyclerView = view.findViewById(android.R.id.list)
        recyclerView.apply {
            (itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
            addFastScroller(swipeRefreshLayout)

            (this as BorderView).borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
                if (activity != null) {
                    (activity as AppBarFragmentActivity).appBar?.setRaised(!top)
                }
            }

            layoutAnimationListener = object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {
                    layoutManager?.findViewByPosition(0)?.clearAnimation()
                    layoutManager?.findViewByPosition(1)?.clearAnimation()
                }

                override fun onAnimationEnd(animation: Animation?) {}
                override fun onAnimationRepeat(animation: Animation?) {}
            }
        }
        recyclerView.fixEdgeEffect()


        banner = view.findViewById(R.id.banner)
        if (model.appList.value == null || model.appList.value?.status == ERROR) {
            refresh()
        }

        model.appList.observe(viewLifecycleOwner) {
            when (it?.status) {
                LOADING -> onLoading()
                SUCCESS -> onSuccess(it)
                ERROR -> onError(it.error)
            }
        }
    }

    private fun refresh() {
        model.reload(requireContext())

        if (Settings.isOnlineRulesEnabled) {
            /*launch {
                if (!GitHubApi.getGitHubApiStatus()) {
                    addBanner(GitHubApiErrorBannerCreator())
                }
            }*/
        }
    }

    private fun onLoading() {
        swipeRefreshLayout.isEnabled = false
        swipeRefreshLayout.isRefreshing = false

        hideBanner()

        recyclerView.adapter = loadingAdapter
    }

    private fun onError(e: Throwable) {
        if (FailedFragment.isSupportedThrowable(e)) {
            if (context != null) {
                localBroadcastManager.sendBroadcast(
                        Intent(ACTION_REQUEST_REFRESH_HOME))
            }

            activity?.finish()
        } else if (context != null) {
            e.printStackTrace()
            CrashReportHelper.logException(e)
            ExceptionDialog.newInstance(e).show(parentFragmentManager)
        }
    }

    private fun onSuccess(data: SourcedResource<List<AppInfo>?, String?>) {
        swipeRefreshLayout.isEnabled = true
        swipeRefreshLayout.isRefreshing = false

        if (data.data != null) {

            listAdapter.updateAppList(data.data)
            if (recyclerView.adapter != listAdapter) {
                recyclerView.adapter = listAdapter
            }

            invalidateBannerView()

            if (data.data.isNotEmpty() && data.source == "refresh") {
                recyclerView.scheduleLayoutAnimation()
            }
        }

        prepareBanners()
    }

    private fun hideBanner() {
        banner.visibility = View.GONE
    }

    private fun popBanner() {
        model.bannerCreators?.poll()
        invalidateBannerView()
    }

    private fun addBanner(creator: BannerCreator) {
        if (model.bannerCreators?.contains(creator.javaClass) == false) {
            model.bannerCreators?.offer(creator.javaClass)
            try {
                invalidateBannerView()
            } catch (ignored: Exception) {

            }
        }
    }

    private fun prepareBanners() {
        val sm = SRManager.create()

        if (sm != null) {
            var version = -1
            try {
                version = sm.moduleStatus.versionCode
            } catch (ignored: Throwable) {
            }

            if (version < 0 && Settings.isDelayedBoot &&
                    Settings.preferences.getBoolean("delay_start_tip", true)) {
                addBanner(DelayStartTipBannerCreator())
            }
        }
    }

    private fun invalidateBannerView() {
        if (activity == null || activity?.isFinishing == true)
            return

        TransitionManager.beginDelayedTransition(banner)

        if (!model.bannerCreators.isNullOrEmpty()) {
            val creatorClass = model.bannerCreators!!.peek()!!
            val creator = creatorClass.getConstructor(AppListFragment::class.java).newInstance(this@AppListFragment)

            banner.visibility = View.VISIBLE

            banner.findViewById<HtmlCompatTextView>(android.R.id.text1)
                    .setHtmlText(creator.text.toString())

            val button1 = banner.findViewById<Button>(android.R.id.button1)
            val button2 = banner.findViewById<Button>(android.R.id.button2)

            creator.button1Settings?.let { (button1Text, button1Callback) ->
                button1.visibility = View.VISIBLE
                button1.text = button1Text
                button1.setOnClickListener(button1Callback)
            } ?: run {
                button1.visibility = View.GONE
            }

            creator.button2Settings?.let { (button2Text, button2Callback) ->
                button2.visibility = View.VISIBLE
                button2.text = button2Text
                button2.setOnClickListener(button2Callback)
            } ?: run {
                button2.visibility = View.GONE
            }
        } else {
            banner.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val item = menu.findItem(R.id.action_search)

        item.setOnActionExpandListener(object : OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                onSearchExpand()
                menu.setItemsVisible(item, false)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                onSearchCollapse()
                menu.setItemsVisible(item, true)
                return true
            }
        })

        val searchView = item.actionView as SearchView
        if (model.isSearching) {
            item.expandActionView()
            searchView.setQuery(model.keyword, false)
            onSearchTextChange(model.keyword)
        }

        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onSearchTextChange(newText)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                refresh()
                return true
            }
            R.id.action_sort_by_app_name,
            R.id.action_sort_by_install_time,
            R.id.action_sort_by_update_time,
            R.id.action_sort_enabled_first -> {
                model.invalidateList("sort")
                return true
            }
            R.id.action_reset_tips -> {
                prepareBanners()
                invalidateBannerView()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onRefresh(context: Context, intent: Intent) {
        val userId = if (Settings.isShowMultiUser) SRManager.USER_ALL else UserHelper.myUserId()
        if (userId != this.userId) {
            this.userId = userId
            arguments?.putInt(EXTRA_USER_ID, this.userId)
            requireActivity().recreate()
            return
        }

        refresh()
    }

    private fun onServerStarted(context: Context, intent: Intent) {
        refresh()
    }

    override fun onSearchExpand() {
        model.isSearching = true
        model.invalidateList("search")
        hideBanner()
    }

    override fun onSearchCollapse() {
        model.isSearching = false
        model.invalidateList("search")
        activity?.let {
            invalidateBannerView()
            it.invalidateOptionsMenu()
        }
    }

    override fun onSearchTextChange(newText: String) {
        model.keyword = newText
        model.invalidateList("search")
    }

    abstract inner class BannerCreator {

        abstract val text: CharSequence

        abstract val button1Settings: Pair<CharSequence, View.OnClickListener>?

        abstract val button2Settings: Pair<CharSequence, View.OnClickListener>?

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            return if (other !is BannerCreator) false else javaClass.name == other.javaClass.name
        }

        override fun hashCode(): Int {
            var result = text.hashCode()
            result = 31 * result + (button1Settings?.hashCode() ?: 0)
            result = 31 * result + (button2Settings?.hashCode() ?: 0)
            return result
        }

    }

    private inner class DelayStartTipBannerCreator : BannerCreator() {

        override val text: CharSequence by lazy {
            getString(R.string.disable_delay_start_tip,
                    getString(R.string.settings_delayed_boot))
        }

        override val button1Settings: Pair<CharSequence, View.OnClickListener> by lazy {
            getString(R.string.dont_show_again) to object : SingleOnClickListener() {

                override fun onClick(v: View?) {
                    super.onClick(v)
                    Settings.preferences.edit().putBoolean("delay_start_tip", false).apply()
                    popBanner()
                }
            }
        }

        override val button2Settings: Pair<CharSequence, View.OnClickListener> by lazy {
            getString(R.string.button_take_me_there) to object : SingleOnClickListener() {

                override fun onClick(v: View?) {
                    super.onClick(v)

                    val context = context
                    context?.startActivity(Intent(context, SettingsActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .setAction(StartUpSettingsFragment.ACTION))
                    popBanner()
                }
            }
        }
    }

    private inner class GitHubApiErrorBannerCreator : BannerCreator() {

        override val text: CharSequence by lazy {
            getString(R.string.github_api_error_banner_text)
        }

        override val button1Settings: Pair<CharSequence, View.OnClickListener> by lazy {
            getString(R.string.ignore_this_time_only) to object : SingleOnClickListener() {

                override fun onClick(v: View?) {
                    super.onClick(v)
                    popBanner()

                }
            }
        }

        override val button2Settings: Pair<CharSequence, View.OnClickListener>? = null

    }

}
