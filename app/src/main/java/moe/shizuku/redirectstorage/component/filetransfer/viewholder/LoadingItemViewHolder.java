package moe.shizuku.redirectstorage.component.filetransfer.viewholder;

import android.view.View;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import rikka.recyclerview.BaseViewHolder;

public class LoadingItemViewHolder extends BaseViewHolder<Object> {

    public static final Creator<Object> CREATOR = (inflater, parent) ->
            new LoadingItemViewHolder(inflater.inflate(R.layout.file_transfer_item_loading, parent, false));

    private TextView title;

    private LoadingItemViewHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(android.R.id.title);
    }

    @Override
    public void onBind() {
        title.setText(R.string.file_transfer_loading_text);
    }
}
