package moe.shizuku.redirectstorage.component.payment;

public class RedeemConstants {

    public static final int RESULT_OK = 0;
    public static final int RESULT_INVALID = 1;
    public static final int RESULT_SERVER_ERROR = 2;

    public static final int MSG_FAILED_NO_CODE = -1;
    public static final int MSG_FAILED_BAD_PRICE = -2;
    public static final int MSG_FAILED_DEVICE_NOT_MATCH = -3;

    public static final int MSG_OK = 0;
    public static final int MSG_OK_OVERWRITE_OLD = 1;

}
