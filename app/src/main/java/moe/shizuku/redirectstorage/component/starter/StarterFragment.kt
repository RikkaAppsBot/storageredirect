package moe.shizuku.redirectstorage.component.starter

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.starter.service.StarterService
import moe.shizuku.redirectstorage.utils.ForegroundService
import rikka.html.text.toHtml
import rikka.internal.help.HelpProvider
import rikka.material.app.AppBarOwner
import rikka.widget.borderview.BorderNestedScrollView
import rikka.widget.borderview.BorderView

class StarterFragment : AppFragment() {

    companion object {
        private const val STARTED = "STARTED"
        private const val EXIT_CODE = "EXIT_CODE"
        private const val LINES = "LINES"

        private const val MSG_WHAT_UPDATE_TEXT = 1
    }

    private val lines = StringBuilder()
    private var started = false
    private var exitCode = Int.MIN_VALUE
    private lateinit var content: TextView
    //private lateinit var title: TextView
    //private lateinit var banner: View
    private lateinit var scrollView: BorderNestedScrollView

    private val messageHandler: MessageHandler by lazy {
        MessageHandler(Looper.getMainLooper())
    }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(applicationContext: Context, intent: Intent) {
            val context = requireContext()
            if (AppConstants.ACTION_STARTER_LINE == intent.action) {
                val text = intent.getStringExtra(Intent.EXTRA_TEXT)
                if (!TextUtils.isEmpty(lines)) {
                    lines.append('\n')
                }
                lines.append(text)

                if (!messageHandler.hasMessages(MSG_WHAT_UPDATE_TEXT)) {
                    messageHandler.sendMessageDelayed(Message.obtain(messageHandler, MSG_WHAT_UPDATE_TEXT), 300)
                }
            } else if (AppConstants.ACTION_STARTER_EXIT == intent.action) {
                exitCode = intent.getIntExtra(AppConstants.EXTRA_EXIT_CODE, Int.MIN_VALUE)
                val text = when (exitCode) {
                    StarterConstants.EXIT_SUCCESS -> context.getString(R.string.starter_dialog_starter_exit)
                    StarterConstants.EXIT_FATAL_IO_EXCEPTION, StarterConstants.EXIT_FATAL_UNABLE_OPEN_ROOT_SHELL -> context.getString(R.string.starter_dialog_unable_open_root_shell)
                    StarterConstants.EXIT_STARTER_IS_RUNNING -> context.getString(R.string.starter_dialog_stater_is_running)
                    StarterConstants.EXIT_FATAL_EXEC_DENIED -> {
                        var download: String? = null
                        val entity = HelpProvider.get(context, "download")
                        if (entity != null) {
                            download = entity.content.get()
                        }
                        if (download == null) {
                            download = "https://sr.rikka.app/download.html"
                        }
                        context.getString(R.string.starter_dialog_exec_perm, download).toHtml().toString()
                    }
                    else -> context.getString(R.string.starter_dialog_starter_exit_with_code, exitCode)
                }

                lines.append('\n').append(text)

                if (!messageHandler.hasMessages(MSG_WHAT_UPDATE_TEXT)) {
                    messageHandler.sendMessageDelayed(Message.obtain(messageHandler, MSG_WHAT_UPDATE_TEXT), 300)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val thread = HandlerThread("StarterFragmentThread")
        thread.start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_starter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        if (savedInstanceState != null) {
            started = savedInstanceState.getBoolean(STARTED, false)
            exitCode = savedInstanceState.getInt(EXIT_CODE, Int.MIN_VALUE)
            if (started) {
                lines.append(savedInstanceState.getString(LINES))
            }
        } else {
            lines.append(context.getString(R.string.starter_dialog_starting)).append('\n')
        }
        content = view.findViewById(android.R.id.text1)
        //title = view.findViewById(android.R.id.title)
        scrollView = view.findViewById(android.R.id.list)
        /*banner = view.findViewById(R.id.banner)
        banner.addOnLayoutChangeListener { v, _, _, _, _, _, _, _, _ ->
            val lp = v.layoutParams as ViewGroup.MarginLayoutParams
            val paddingBottom = v.measuredHeight + lp.topMargin
            scrollView.setInitialPadding(scrollView.initialPaddingLeft, scrollView.initialPaddingTop, scrollView.initialPaddingRight,
                    scrollView.initialPaddingBottom + paddingBottom)
        }*/
        if (!TextUtils.isEmpty(lines)) {
            content.text = lines
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(AppConstants.ACTION_STARTER_EXIT)
        intentFilter.addAction(AppConstants.ACTION_STARTER_LINE)
        LocalBroadcastManager.getInstance(requireContext())
                .registerReceiver(broadcastReceiver, intentFilter)
        if (!started && exitCode == Int.MIN_VALUE) {
            ForegroundService.startOrNotify(context,
                    Intent(context, StarterService::class.java).putExtra(AppConstants.EXTRA_FROM_UI, true))
            started = true
        }
        scrollView.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top, _, _, _ ->
            (activity as AppBarOwner?)?.appBar?.setRaised(!top)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(requireContext())
                .unregisterReceiver(broadcastReceiver)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(STARTED, started)
        outState.putInt(EXIT_CODE, exitCode)
        outState.putString(LINES, lines.toString())
    }

    private inner class MessageHandler internal constructor(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message) {
            if (msg.what == MSG_WHAT_UPDATE_TEXT) {
                content.text = lines
            }
        }
    }
}