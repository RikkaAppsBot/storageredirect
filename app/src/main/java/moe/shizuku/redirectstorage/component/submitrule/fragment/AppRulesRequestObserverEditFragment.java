package moe.shizuku.redirectstorage.component.submitrule.fragment;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.exportfolders.editor.ExportFolderEditFragment;
import moe.shizuku.redirectstorage.model.AppInfo;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA;

public class AppRulesRequestObserverEditFragment extends ExportFolderEditFragment {

    public static AppRulesRequestObserverEditFragment createForAdd(@NonNull AppInfo appInfo) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, appInfo);
        args.putInt(EXTRA_EDIT_REASON, EDIT_REASON_ADD);

        AppRulesRequestObserverEditFragment dialog = new AppRulesRequestObserverEditFragment();
        dialog.setArguments(args);
        return dialog;
    }

    public static AppRulesRequestObserverEditFragment createForEdit(@NonNull AppInfo appInfo, @NonNull ObserverInfo observer) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, appInfo);
        args.putParcelable(EXTRA_OBSERVER, observer);
        args.putInt(EXTRA_EDIT_REASON, EDIT_REASON_EDIT);

        AppRulesRequestObserverEditFragment dialog = new AppRulesRequestObserverEditFragment();
        dialog.setArguments(args);
        return dialog;
    }

    protected boolean checkRequiredValuesAndToast() {
        if (getBuilder().getSource() == null) {
            Toast.makeText(requireContext(), R.string.toast_source_path_is_empty, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getBuilder().getTarget() == null) {
            Toast.makeText(requireContext(), R.string.toast_target_path_is_empty, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
