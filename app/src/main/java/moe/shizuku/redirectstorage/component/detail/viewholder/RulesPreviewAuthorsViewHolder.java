package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.view.View;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppConfiguration;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

public class RulesPreviewAuthorsViewHolder extends BaseViewHolder<AppConfiguration> {

    public static final Creator<AppConfiguration> CREATOR = (inflater, parent) ->
            new RulesPreviewAuthorsViewHolder(inflater.inflate(R.layout.online_rules_preview_authors_item, parent, false));

    private final TextView text;

    private RulesPreviewAuthorsViewHolder(View itemView) {
        super(itemView);

        text = itemView.findViewById(android.R.id.text1);
    }

    @Override
    public void onBind() {
        text.setText(HtmlCompat.fromHtml(getData().authors(itemView.getContext()), null, SizeTagHandler.getInstance()));
    }
}
