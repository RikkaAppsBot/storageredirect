package moe.shizuku.redirectstorage.component.filebrowser.viewholder;

import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.os.ParcelFileDescriptor;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRFileManager;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.component.filebrowser.adapter.AppFileBrowserListAdapter;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.AppIconCache;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;
import moe.shizuku.redirectstorage.utils.MediaStoreUtils;
import rikka.recyclerview.BaseViewHolder;

/**
 * Created by Fung Gwo on 2018/1/26.
 */

public class AppFileBrowserItemViewHolder extends BaseViewHolder<ServerFile>
        implements View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {

    public static ItemCreator newCreator(FileBrowserUtils.FileBrowserStyleHolder styleHolder,
                                         AppFileBrowserListAdapter.Callback callback) {
        return new ItemCreator(styleHolder, callback);
    }

    private ImageView icon, imagePreview;
    private TextView title;

    private AppFileBrowserListAdapter.Callback callback;

    private FileBrowserUtils.FileBrowserStyleHolder styleHolder;

    private final CompositeDisposable mDisposables = new CompositeDisposable();

    private int mPreviewSize;

    public AppFileBrowserItemViewHolder(View itemView,
                                        FileBrowserUtils.FileBrowserStyleHolder styleHolder,
                                        AppFileBrowserListAdapter.Callback callback) {
        super(itemView);

        this.styleHolder = styleHolder;
        this.callback = callback;

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        imagePreview = itemView.findViewById(R.id.image_preview);

        mPreviewSize = itemView.getResources()
                .getDimensionPixelSize(R.dimen.file_manager_preview_size);

        itemView.setOnClickListener(v -> this.callback.onItemClicked(getData()));
        itemView.setOnCreateContextMenuListener(this);
    }

    @Override
    public void onBind() {
        super.onBind();

        icon.setImageResource(FileBrowserUtils.getFileIconResources(getData()));
        icon.setColorFilter(styleHolder.getFileTypeTintColor(getData()), PorterDuff.Mode.SRC_IN);
        title.setText(getData().getName());
        if (!getData().isDirectory() && FileBrowserUtils.getFileTypeByName(getData().getName()) == FileBrowserUtils.TYPE_IMAGES) {
            mDisposables.add(
                    Maybe.fromCallable(() -> {
                        final ServerFile data = getData();
                        if (data == null) {
                            throw new IllegalStateException("data is null.");
                        }
                        SRFileManager fm = SRManager.createThrow().getFileManager();
                        ParcelFileDescriptor pfd = fm.openFileDescriptor(data, "r");
                        BitmapFactory.Options outOpt = new BitmapFactory.Options();
                        outOpt.inJustDecodeBounds = true;
                        int sampleSize;
                        try {
                            InputStream is = new FileInputStream(pfd.getFileDescriptor());
                            BitmapFactory.decodeStream(is, null, outOpt);
                            sampleSize = MediaStoreUtils.findSampleSize(mPreviewSize, outOpt.outWidth, outOpt.outHeight);
                        } finally {
                            pfd.close();
                        }
                        if (sampleSize < 1) {
                            throw new IllegalArgumentException("Sample size is lower than 1.");
                        }
                        pfd = fm.openFileDescriptor(data, "r");
                        BitmapFactory.Options inOpt = new BitmapFactory.Options();
                        inOpt.inSampleSize = sampleSize;
                        try {
                            InputStream is = new FileInputStream(pfd.getFileDescriptor());
                            return BitmapFactory.decodeStream(is, null, inOpt);
                        } finally {
                            pfd.close();
                        }
                    })
                            .subscribeOn(AppIconCache.scheduler())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(res -> imagePreview.setImageBitmap(res), Throwable::printStackTrace)
            );
        }
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        this.onBind();
    }

    @Override
    public void onRecycle() {
        mDisposables.clear();
        imagePreview.setImageBitmap(null);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(getData().getName());
        new MenuInflater(v.getContext()).inflate(R.menu.context_menu_file_browser_item, menu);
        menu.findItem(R.id.action_share).setOnMenuItemClickListener(this);
        menu.findItem(R.id.action_view_properties).setOnMenuItemClickListener(this);
        menu.findItem(R.id.action_share).setVisible(!getData().isDirectory());
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                this.callback.onItemShareClicked(getData());
                return true;
            case R.id.action_view_properties:
                this.callback.onItemViewPropertiesClicked(getData());
                return true;
        }
        return false;
    }

    static class ItemCreator implements Creator<ServerFile> {

        private FileBrowserUtils.FileBrowserStyleHolder styleHolder;
        private AppFileBrowserListAdapter.Callback callback;

        ItemCreator(FileBrowserUtils.FileBrowserStyleHolder styleHolder,
                    AppFileBrowserListAdapter.Callback callback) {
            this.styleHolder = styleHolder;
            this.callback = callback;
        }

        @Override
        public BaseViewHolder<ServerFile> createViewHolder(LayoutInflater inflater, ViewGroup parent) {
            return new AppFileBrowserItemViewHolder(
                    inflater.inflate(R.layout.app_file_browser_item, parent, false),
                    styleHolder, callback);
        }

    }
}
