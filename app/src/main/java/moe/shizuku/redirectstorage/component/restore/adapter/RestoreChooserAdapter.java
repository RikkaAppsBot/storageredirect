package moe.shizuku.redirectstorage.component.restore.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder;
import moe.shizuku.redirectstorage.component.restore.model.AbstractBackupModel;
import moe.shizuku.redirectstorage.component.restore.viewholder.RestoreAppsConfigChoiceItemViewHolder;
import moe.shizuku.redirectstorage.component.restore.viewholder.RestoreAppsConfigNoChoiceItemViewHolder;
import moe.shizuku.redirectstorage.component.restore.viewholder.RestoreChoiceItemViewHolder;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;

public class RestoreChooserAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    private static final String STATE_SELECTED_APPS_CONFIG_USER_ID = RestoreChooserAdapter.class.getSimpleName()
            + ".state.SELECTED_APPS_CONFIG_USER_ID";
    private static final String STATE_CHOOSE_STATES = RestoreChooserAdapter.class.getSimpleName()
            + ".state.CHOOSE_STATES";

    public static final int TYPE_COMMON_CONFIG = 0;

    private int mSelectAppsConfigUserId = -1;
    private boolean[] mCheckBoxStates = new boolean[1];

    private List<Integer> mAppsConfigUserItemIndexes = new ArrayList<>();

    @Nullable
    private AbstractBackupModel mBackupModel;

    @Nullable
    private OnChoicesChangeListener mChangeListener;

    @SuppressWarnings("unchecked")
    public RestoreChooserAdapter() {
        super();

        getCreatorPool().putRule(CheckBoxViewModel.class, RestoreChoiceItemViewHolder.CREATOR);
        getCreatorPool().putRule(AppsConfigNoUserItemViewModel.class, RestoreAppsConfigNoChoiceItemViewHolder.CREATOR);
        getCreatorPool().putRule(AppsConfigUserItemViewModel.class, RestoreAppsConfigChoiceItemViewHolder.CREATOR);
        getCreatorPool().putRule(Object.class, AppDetailSummaryViewHolder.SUBTITLE_CREATOR);
    }

    public void setOnChoicesChangeListener(OnChoicesChangeListener listener) {
        mChangeListener = listener;
    }

    public int getSelectAppsConfigUserId() {
        return mSelectAppsConfigUserId;
    }

    public boolean hasSelectedCommonConfig() {
        return mCheckBoxStates[TYPE_COMMON_CONFIG];
    }

    public boolean hasChosen() {
        if (mSelectAppsConfigUserId != -1) {
            return true;
        }
        for (boolean state : mCheckBoxStates) {
            if (state) {
                return true;
            }
        }
        return false;
    }

    public void updateData(@Nullable AbstractBackupModel backupModel) {
        mBackupModel = backupModel;
        final List<Object> items = getItems();
        items.clear();
        mAppsConfigUserItemIndexes.clear();
        if (mBackupModel != null) {
            items.add(R.string.restore_category_apps_config);
            mAppsConfigUserItemIndexes.add(items.size());
            items.add(new AppsConfigNoUserItemViewModel());
            for (int userId : mBackupModel.getUserIds()) {
                mAppsConfigUserItemIndexes.add(items.size());
                items.add(new AppsConfigUserItemViewModel(userId));
            }
            /*items.add(R.string.restore_category_other_config);
            items.add(new CheckBoxViewModel(TYPE_COMMON_CONFIG));*/
        }
        notifyDataSetChanged();
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(STATE_SELECTED_APPS_CONFIG_USER_ID, mSelectAppsConfigUserId);
        outState.putBooleanArray(STATE_CHOOSE_STATES, mCheckBoxStates);
    }

    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }
        mSelectAppsConfigUserId = savedInstanceState.getInt(STATE_SELECTED_APPS_CONFIG_USER_ID);
        mCheckBoxStates = savedInstanceState.getBooleanArray(STATE_CHOOSE_STATES);
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }

    public final class CheckBoxViewModel {

        public final int type;

        CheckBoxViewModel(int type) {
            this.type = type;
        }

        public boolean isChecked() {
            return mCheckBoxStates[type];
        }

        public void setChecked(boolean newState) {
            mCheckBoxStates[type] = newState;
            if (mChangeListener != null) {
                mChangeListener.onChange();
            }
        }

        @NonNull
        public AbstractBackupModel getBackupModel() {
            return Objects.requireNonNull(mBackupModel);
        }

    }

    public class AppsConfigUserItemViewModel {

        public final int userId;

        AppsConfigUserItemViewModel(int userId) {
            this.userId = userId;
        }

        public boolean isChecked() {
            return mSelectAppsConfigUserId == userId;
        }

        public void select() {
            mSelectAppsConfigUserId = userId;
            for (int i : mAppsConfigUserItemIndexes) {
                notifyItemChanged(i);
            }
            if (mChangeListener != null) {
                mChangeListener.onChange();
            }
        }

        @NonNull
        public AbstractBackupModel getBackupModel() {
            return Objects.requireNonNull(mBackupModel);
        }

    }

    public class AppsConfigNoUserItemViewModel extends AppsConfigUserItemViewModel {

        AppsConfigNoUserItemViewModel() {
            super(-1);
        }
    }

    public interface OnChoicesChangeListener {

        void onChange();

    }

}
