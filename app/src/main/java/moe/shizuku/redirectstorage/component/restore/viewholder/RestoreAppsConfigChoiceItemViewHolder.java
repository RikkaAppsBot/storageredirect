package moe.shizuku.redirectstorage.component.restore.viewholder;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.restore.adapter.RestoreChooserAdapter;
import moe.shizuku.redirectstorage.component.restore.dialog.RestoreWizardAppsConfigPreviewDialog;
import rikka.core.util.ContextUtils;
import rikka.recyclerview.BaseViewHolder;

public class RestoreAppsConfigChoiceItemViewHolder extends BaseViewHolder<RestoreChooserAdapter.AppsConfigUserItemViewModel> implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final Creator<RestoreChooserAdapter.AppsConfigUserItemViewModel> CREATOR = (inflater, parent) ->
            new RestoreAppsConfigChoiceItemViewHolder(inflater.inflate(R.layout.restore_single_choice_item, parent, false));

    final ImageView mIcon;
    final TextView mTitle, mSummary;
    final RadioButton mRadioBox;
    final View mDivider;

    public RestoreAppsConfigChoiceItemViewHolder(View itemView) {
        super(itemView);

        mIcon = itemView.findViewById(android.R.id.icon);
        mTitle = itemView.findViewById(android.R.id.title);
        mSummary = itemView.findViewById(android.R.id.summary);
        mRadioBox = itemView.findViewById(android.R.id.checkbox);
        mDivider = itemView.findViewById(R.id.two_target_divider);

        mRadioBox.setOnCheckedChangeListener(this);

        itemView.findViewById(android.R.id.content).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        RestoreWizardAppsConfigPreviewDialog.newInstance(getData().getBackupModel(), getData().userId)
                .show(ContextUtils.<FragmentActivity>requireActivity(itemView.getContext())
                        .getSupportFragmentManager());
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked == getData().isChecked()) {
            return;
        }
        if (isChecked) {
            getData().select();
        }
    }

    @Override
    public void onBind() {
        itemView.setBackground(null);

        mIcon.setImageDrawable(null);
        mTitle.setText(mTitle.getResources().getString(R.string.restore_type_apps_config_format, getData().userId));
        mSummary.setText(mSummary.getResources()
                .getString(R.string.restore_type_apps_config_summary_format,
                        getData().getBackupModel().getRedirectPackagesForUser(getData().userId).size()));
        mSummary.setVisibility(View.VISIBLE);
        mDivider.setVisibility(View.VISIBLE);

        mRadioBox.setChecked(getData().isChecked());
    }
}

