package moe.shizuku.redirectstorage.component.submitrule

import android.app.TaskStackBuilder
import android.os.Bundle
import androidx.fragment.app.commit
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.ktx.putPackageExtrasFrom

class SubmitRuleActivity : AppActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //by appDetailViewModel()
        appBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            /*if (Settings.githubToken != null) {
                GitHubApi.loginWithoutVerifying(Settings.githubToken!!)
            }
            if (!GitHubApi.isLoginSuccess) {
                GitHubLoginDialog.newInstance().show(supportFragmentManager)
            }*/
            supportFragmentManager.commit {
                replace(android.R.id.content, SubmitRuleFragment())
            }
        }
    }

    override fun onPrepareNavigateUpTaskStack(builder: TaskStackBuilder) {
        super.onPrepareNavigateUpTaskStack(builder)
        builder.editIntentAt(builder.intentCount - 1).putPackageExtrasFrom(intent)
    }
}