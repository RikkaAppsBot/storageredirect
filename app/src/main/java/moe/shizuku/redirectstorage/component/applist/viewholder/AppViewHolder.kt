package moe.shizuku.redirectstorage.component.applist.viewholder

import android.content.Context
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.coroutines.Job
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.MountDirsConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.detail.AppDetailActivity
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.AppIconCache
import moe.shizuku.redirectstorage.utils.UserHelper
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppViewHolder(itemView: View) : BaseViewHolder<AppInfo>(itemView), OnClickListener {

    companion object {
        val CREATOR = Creator<AppInfo> { inflater: LayoutInflater, parent: ViewGroup? -> AppViewHolder(inflater.inflate(R.layout.item_app, parent, false)) }

        private val grayColorFilter = ColorMatrixColorFilter(ColorMatrix().apply { setSaturation(0f) })

        private val VERIFIED_LISTENER = OnClickListener { v: View ->
            val context = v.context
            Toast.makeText(context, "${context.getString(R.string.verified_message_title)}\n${context.getString(R.string.verified_message_summary)}", Toast.LENGTH_SHORT).show()
        }

        private val SANS_SERIF_MEDIUM = Typeface.create("sans-serif-medium", Typeface.NORMAL)
    }

    private val icon: ImageView = itemView.findViewById(android.R.id.icon)
    private val title: TextView = itemView.findViewById(android.R.id.title)
    private val summary: TextView = itemView.findViewById(android.R.id.summary)
    private val text1: TextView = itemView.findViewById(android.R.id.text1)
    private val verified: View = itemView.findViewById(android.R.id.button1)

    private var loadIconJob: Job? = null
    private var loadSummaryJob: Job? = null

    init {
        verified.setOnClickListener(VERIFIED_LISTENER)
        itemView.setOnClickListener(this)
    }

    override fun onBind() {
        if (UserHelper.myUserId() != data.userId) {
            title.text = context.getString(R.string.app_name_with_user, data.label, data.userId)
        } else {
            title.text = data.label
        }

        if (data.applicationInfo != null) {
            loadIconJob = AppIconCache.loadIconBitmapAsync(context, data.applicationInfo!!, data.userId, icon)
        } else {
            icon.setImageResource(R.mipmap.ic_default_app_icon)
        }
        loadSummaryJob = data.loadSummaryTextToAsync(context, summary)

        syncEnabledState(context)
        syncVerifiedState(context)
        syncVerifiedSystemState(context)
    }

    private fun onBind(payloadMap: Map<*, *>) {
        for ((key, value) in payloadMap) {
            Log.d(AppConstants.TAG, "AppViewHolder: Update $key")

            when (key) {
                "isRedirectEnabled" -> {
                    syncEnabledState(context)
                    syncVerifiedSystemState(context)
                }
                "isVerified" -> {
                    syncVerifiedState(context)
                    syncVerifiedSystemState(context)
                }
                "mountDirsConfig" -> {
                    data.redirectInfo.mountDirsConfig = value as MountDirsConfig?
                }
                "redirectTarget" -> {
                    data.redirectInfo.redirectTarget = value as String?
                }
            }
        }
    }

    override fun onBind(payloads: List<Any>) {
        for (payload in payloads) {
            when (payload) {
                is Map<*, *> -> {
                    onBind(payload)
                }
                else -> {
                    Log.d(AppConstants.TAG, "AppViewHolder: Unsupported payload $payload")
                }
            }
        }
    }

    private fun syncVerifiedSystemState(context: Context) {
        if (!data.isRedirectEnabled && !data.isVerified() && data.isVerifiedAOSP()) {
            // verified system
            text1.text = context.getString(R.string.list_summary_verified_system)
            text1.visibility = View.VISIBLE
        } else {
            text1.visibility = View.GONE
        }
    }

    private fun syncEnabledState(context: Context) {
        if (loadSummaryJob?.isActive == false) {
            loadSummaryJob = data.loadSummaryTextToAsync(context, summary)
        }
        if (data.isRedirectEnabled) {
            summary.isSelected = true
            summary.typeface = SANS_SERIF_MEDIUM
            title.typeface = SANS_SERIF_MEDIUM
            text1.typeface = SANS_SERIF_MEDIUM
        } else {
            summary.isSelected = false
            summary.typeface = Typeface.SANS_SERIF
            title.typeface = Typeface.SANS_SERIF
            text1.typeface = Typeface.SANS_SERIF
        }
    }

    private fun syncVerifiedState(context: Context) {
        if (!Settings.isOnlineRulesEnabled) {
            return
        }

        title.isEnabled = !data.isVerified()
        summary.isEnabled = data.isRedirectEnabled || !data.isVerified()
        verified.visibility = if (data.isVerified()) View.VISIBLE else View.GONE
        if (!data.isRedirectEnabled) {
            if (loadSummaryJob?.isActive == false) {
                loadSummaryJob = data.loadSummaryTextToAsync(context, summary)
            }
        }

        if (data.isVerified()) {
            icon.colorFilter = grayColorFilter
            icon.alpha = 0.5f
        } else {
            icon.colorFilter = null
            icon.alpha = 1f
        }
    }

    override fun onClick(view: View) {
        val context = itemView.context
        context.startActivity(Intent(context, AppDetailActivity::class.java)
                .putExtra(AppConstants.EXTRA_PACKAGE_NAME, data.packageName)
                .putExtra(AppConstants.EXTRA_USER_ID, data.userId))
    }

    override fun onRecycle() {
        if (loadIconJob?.isActive == true) {
            loadIconJob?.cancel()
        }
        if (loadSummaryJob?.isActive == true) {
            loadSummaryJob?.cancel()
        }
    }
}