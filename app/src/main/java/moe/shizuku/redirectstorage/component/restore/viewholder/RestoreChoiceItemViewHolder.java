package moe.shizuku.redirectstorage.component.restore.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.restore.adapter.RestoreChooserAdapter;
import moe.shizuku.redirectstorage.component.restore.dialog.RestoreWizardCommonSettingsPreviewDialog;
import rikka.core.util.ContextUtils;
import rikka.recyclerview.BaseViewHolder;

public class RestoreChoiceItemViewHolder extends BaseViewHolder<RestoreChooserAdapter.CheckBoxViewModel> {

    public static final Creator<RestoreChooserAdapter.CheckBoxViewModel> CREATOR = (inflater, parent) ->
            new RestoreChoiceItemViewHolder(inflater.inflate(R.layout.restore_choice_item, parent, false));

    private final ImageView mIcon;
    private final TextView mTitle, mSummary;
    private final CheckBox mCheckBox;

    public RestoreChoiceItemViewHolder(View itemView) {
        super(itemView);

        mIcon = itemView.findViewById(android.R.id.icon);
        mTitle = itemView.findViewById(android.R.id.title);
        mSummary = itemView.findViewById(android.R.id.summary);
        mCheckBox = itemView.findViewById(android.R.id.checkbox);

        itemView.findViewById(android.R.id.content).setOnClickListener(v -> {
            switch (getData().type) {
                case RestoreChooserAdapter.TYPE_COMMON_CONFIG: {
                    RestoreWizardCommonSettingsPreviewDialog.newInstance(getData().getBackupModel())
                            .show(ContextUtils.<FragmentActivity>requireActivity(itemView.getContext())
                                    .getSupportFragmentManager());
                    break;
                }
            }
        });

        mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked == getData().isChecked()) {
                return;
            }
            getData().setChecked(isChecked);
        });
    }

    @Override
    public void onBind() {
        switch (getData().type) {
            case RestoreChooserAdapter.TYPE_COMMON_CONFIG: {
                mIcon.setImageResource(R.drawable.ic_settings_applications_24dp);
                mTitle.setText(R.string.restore_type_common_config);
                mSummary.setText(R.string.restore_type_common_config_summary);
                break;
            }
        }

        mCheckBox.setChecked(getData().isChecked());
    }
}
