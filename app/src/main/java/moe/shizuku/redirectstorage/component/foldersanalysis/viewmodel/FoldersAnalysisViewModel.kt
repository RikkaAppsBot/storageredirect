package moe.shizuku.redirectstorage.component.foldersanalysis.viewmodel

import androidx.lifecycle.MutableLiveData
import moe.shizuku.redirectstorage.app.BaseViewModel
import moe.shizuku.redirectstorage.model.FolderSizeEntry

class FoldersAnalysisViewModel : BaseViewModel() {

    val list: MutableLiveData<List<FolderSizeEntry>?> = MutableLiveData(null)
}
