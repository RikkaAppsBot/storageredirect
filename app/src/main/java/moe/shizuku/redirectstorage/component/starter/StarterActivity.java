package moe.shizuku.redirectstorage.component.starter;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity;

public class StarterActivity extends AppBarFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getAppBar() != null) {
            getAppBar().setDisplayHomeAsUpEnabled(true);
            getAppBar().setHomeAsUpIndicator(R.drawable.ic_close_24dp);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new StarterFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
