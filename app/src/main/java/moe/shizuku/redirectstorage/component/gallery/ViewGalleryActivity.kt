package moe.shizuku.redirectstorage.component.gallery

import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.annotation.IntDef
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.gallery.fragment.GalleryFoldersListFragment
import moe.shizuku.redirectstorage.component.gallery.fragment.GalleryPhotosListFragment
import moe.shizuku.redirectstorage.component.gallery.fragment.GalleryTimelineListFragment
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.ktx.putPackageExtrasFrom
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.UserHelper

class ViewGalleryActivity : AppActivity() {

    companion object {

        @JvmStatic
        val EXTRA_SORT_MODE = ViewGalleryActivity::class.java.name + ".extra.SORT_MODE"

        const val SORT_MODE_FOLDERS = 0
        const val SORT_MODE_TIMELINE = 1

    }

    @IntDef(SORT_MODE_FOLDERS, SORT_MODE_TIMELINE)
    @Retention(AnnotationRetention.SOURCE)
    annotation class SortMode

    private lateinit var spinner: Spinner

    // For GalleryFoldersListFragment
    var spinnerList: List<GalleryFolderSimpleItem> = emptyList()
        set(value) {
            field = value
            spinner.adapter = ArrayAdapter<String>(this,
                    R.layout.simple_spinner_item_for_toolbar,
                    android.R.id.text1,
                    value.map { it.folderName }.toTypedArray()).also {
                it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
        }
    var selectedItem: GalleryFolderSimpleItem? = null
        set(value) {
            if (field != value) {
                field = value
                try {
                    spinner.setSelection(spinnerList.indexOf(value))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                LocalBroadcastManager.getInstance(this)
                        .sendBroadcast(Intent(GalleryPhotosListFragment.ACTION_ON_FOLDER_CHANGE)
                                .putExtra(EXTRA_DATA, value))
            }
        }

    @SortMode
    var sortMode: Int = SORT_MODE_FOLDERS
    private lateinit var appInfo: AppInfo

    private var spinnerVisible: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sortMode = if (intent.hasExtra(EXTRA_SORT_MODE)) {
            intent.getIntExtra(EXTRA_SORT_MODE, SORT_MODE_FOLDERS)
        } else {
            Settings.viewGalleryDefaultMode
        }
        appInfo = intent.getParcelableExtra(EXTRA_DATA)!!

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var subtitle = appInfo.label.toString()
        if (UserHelper.myUserId() != appInfo.userId) {
            subtitle = getString(R.string.app_name_with_user, subtitle, appInfo.userId)
        }
        supportActionBar?.subtitle = subtitle

        supportActionBar?.customView = LayoutInflater.from(supportActionBar!!.themedContext)
                .inflate(R.layout.toolbar_customview_spinner, null)
                .apply {
                    spinner = findViewById(R.id.spinner)
                    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                        override fun onItemSelected(parent: AdapterView<*>?,
                                                    view: View?, position: Int, id: Long) {
                            selectedItem = spinnerList[position]
                            LocalBroadcastManager.getInstance(this@ViewGalleryActivity)
                                    .sendBroadcast(GalleryPhotosListFragment.notifyFolderChanged(selectedItem!!))
                        }
                    }
                }

        if (savedInstanceState == null) {
            replaceContentFragment()
        }

        checkSpinnerVisibility()
    }

    private fun replaceContentFragment() {
        supportFragmentManager.commit {
            replace(android.R.id.content, when (sortMode) {
                SORT_MODE_FOLDERS -> GalleryFoldersListFragment.newInstance(appInfo)
                SORT_MODE_TIMELINE -> GalleryTimelineListFragment.newInstance(appInfo)

                // Do not throw any exception but do a fallback
                else -> GalleryFoldersListFragment.newInstance(appInfo)
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.view_gallery, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val viewOptionsItem = menu.findItem(R.id.action_view_options)
        viewOptionsItem.isVisible = !spinnerVisible

        when (sortMode) {
            SORT_MODE_FOLDERS -> menu.findItem(R.id.action_folders_mode).isChecked = true
            SORT_MODE_TIMELINE -> menu.findItem(R.id.action_timeline_mode).isChecked = true
        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_folders_mode -> {
                if (sortMode != SORT_MODE_FOLDERS) {
                    sortMode = SORT_MODE_FOLDERS
                    Settings.viewGalleryDefaultMode = sortMode
                    item.isChecked = true
                    replaceContentFragment()
                }
                return true
            }
            R.id.action_timeline_mode -> {
                if (sortMode != SORT_MODE_TIMELINE) {
                    sortMode = SORT_MODE_TIMELINE
                    Settings.viewGalleryDefaultMode = sortMode
                    item.isChecked = true
                    replaceContentFragment()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setSpinnerVisible(visible: Boolean) {
        spinnerVisible = visible
        supportActionBar?.setDisplayShowTitleEnabled(!visible)
        supportActionBar?.setDisplayShowCustomEnabled(visible)
        invalidateOptionsMenu()
    }

    private fun checkSpinnerVisibility(
            fragmentManager: FragmentManager? = supportFragmentManager): Boolean {
        if (sortMode == SORT_MODE_FOLDERS) {
            fragmentManager?.fragments?.forEach {
                if (it is GalleryPhotosListFragment) {
                    setSpinnerVisible(true)
                    return true
                }
                if (checkSpinnerVisibility(it.childFragmentManager)) {
                    return true
                }
            }
        }
        setSpinnerVisible(false)
        return false
    }

    private fun onBackPressed(fragmentManager: FragmentManager?,
                              isChildFragmentManager: Boolean): Boolean {
        if (fragmentManager == null || fragmentManager.isStateSaved)
            return false

        if (isChildFragmentManager && fragmentManager.backStackEntryCount > 0)
            return fragmentManager.popBackStackImmediate()

        for (fragment in fragmentManager.fragments) {
            if (fragment == null)
                continue

            if (fragment.isVisible && onBackPressed(fragment.childFragmentManager, true))
                return true
        }
        return false
    }

    override fun onBackPressed() {
        if (onBackPressed(supportFragmentManager, false)) {
            checkSpinnerVisibility()
            return
        }

        super.onBackPressed()
    }

    override fun onPrepareNavigateUpTaskStack(builder: TaskStackBuilder) {
        super.onPrepareNavigateUpTaskStack(builder)
        builder.editIntentAt(builder.intentCount - 1).putPackageExtrasFrom(intent)
    }
}