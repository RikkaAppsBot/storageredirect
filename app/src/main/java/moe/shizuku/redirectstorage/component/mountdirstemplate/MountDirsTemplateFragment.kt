package moe.shizuku.redirectstorage.component.mountdirstemplate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.viewModels
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.mountdirstemplate.adapter.MountDirsTemplateAdapter
import moe.shizuku.redirectstorage.component.mountdirstemplate.dialog.MountDirsTemplateEditorDialog
import moe.shizuku.redirectstorage.dialog.ExceptionDialog
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.material.app.AppBarOwner
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import rikka.widget.borderview.BorderView


class MountDirsTemplateFragment : AppFragment(), MountDirsTemplateAdapter.Listener {

    private val viewModel by viewModels<MountDirsTemplateViewModel>({ requireActivity() })

    private val adapter by lazy { MountDirsTemplateAdapter() }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_appbar_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView = view.findViewById<BorderRecyclerView>(android.R.id.list)
        recyclerView.fixEdgeEffect()
        recyclerView.adapter = adapter
        adapter.listener = this
        recyclerView.addItemDecoration(VerticalPaddingDecoration(recyclerView.context))
        recyclerView.borderViewDelegate.borderVisibilityChangedListener = BorderView.OnBorderVisibilityChangedListener { top: Boolean, _: Boolean, _: Boolean, _: Boolean -> (activity as AppBarOwner?)?.appBar?.setRaised(!top) }
        val lp = recyclerView.layoutParams
        if (lp is FrameLayout.LayoutParams) {
            lp.rightMargin = recyclerView.context.resources.getDimension(R.dimen.rd_activity_horizontal_margin).toInt()
            lp.leftMargin = lp.rightMargin
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (viewModel.templates.value == null) {
            viewModel.load()
        }

        viewModel.templates.observe(viewLifecycleOwner) { res ->
            if (res != null) {
                adapter.setData(requireContext(), res)
            }
            if (res?.status == Status.ERROR) {
                res.error.printStackTrace()
                CrashReportHelper.logException(res.error)
                ExceptionDialog.newInstance(res.error).show(parentFragmentManager)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.listener = null
    }

    override fun onAddClick() {
        MountDirsTemplateEditorDialog.createNewDialog()
                .show(childFragmentManager)
    }

    override fun onClick(data: MountDirsTemplate) {
        MountDirsTemplateEditorDialog.createEditDialog(data)
                .show(childFragmentManager)
    }
}
