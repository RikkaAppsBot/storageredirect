package moe.shizuku.redirectstorage.component.submitrule

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.api.GitHubApi
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment

/**
 * Created by fytho on 2017/12/14.
 */

class GitHubLoginDialog : AlertDialogFragment() {

    companion object {

        fun newInstance(): GitHubLoginDialog {
            return GitHubLoginDialog()
        }
    }

    private lateinit var loginInfoLayout: View
    private lateinit var progressBar: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        registerLocalBroadcastReceiver({ _, _ ->
            setLoginProcessing(false)
            positiveButton.postDelayed({
                try {
                    dismiss()
                } catch (ignored: Exception) {

                }
            }, 1000)
        }, IntentFilter(AppConstants.ACTION_GITHUB_AUTHORIZED))
        registerLocalBroadcastReceiver({ _, _ ->
            if (activity != null && !requireActivity().isFinishing) {
                Toast.makeText(activity, R.string.toast_login_failed, Toast.LENGTH_SHORT).show()
            }
            setLoginProcessing(false)
        }, IntentFilter(AppConstants.ACTION_GITHUB_AUTHORIZE_FAILED))
    }

    override fun onGetContentViewLayoutResource(): Int {
        return R.layout.github_login_dialog_content
    }

    override fun onBuildAlertDialog(builder: AlertDialog.Builder, savedInstanceState: Bundle?) {
        builder.setPositiveButton(R.string.dialog_github_need_login_login, null)
        builder.setNegativeButton(android.R.string.cancel) { _, _ ->
            localBroadcastManager.sendBroadcast(
                    Intent(AppConstants.ACTION_GITHUB_AUTHORIZE_FAILED))
        }
        builder.setCancelable(false)
    }

    override fun onAlertDialogCreated(dialog: AlertDialog, contentView: View, savedInstanceState: Bundle?) {
        dialog.setCanceledOnTouchOutside(false)
        loginInfoLayout = contentView.findViewById(R.id.login_info_layout)
        progressBar = contentView.findViewById(R.id.progress_bar)
    }

    override fun onShow(dialog: AlertDialog) {
        // Overrides listener for retaining dialog
        positiveButton.setOnClickListener {
            if (!isStateSaved) {
                GitHubApi.requestAuthorize(requireActivity())
                setLoginProcessing(true)
            }
        }
        setLoginProcessing(false)
    }

    private fun setLoginProcessing(isProcessing: Boolean) {
        setPositiveButtonEnabled(!isProcessing)
        loginInfoLayout.alpha = if (isProcessing) 0.3f else 1f
        progressBar.visibility = if (isProcessing) View.VISIBLE else View.GONE
    }

}
