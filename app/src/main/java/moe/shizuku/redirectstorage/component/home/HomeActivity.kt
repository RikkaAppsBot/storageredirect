package moe.shizuku.redirectstorage.component.home

import android.content.*
import android.content.res.Resources.Theme
import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.edit
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppActivity
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.home.dialog.DebugInfoDialog
import moe.shizuku.redirectstorage.ktx.applyCountdown
import moe.shizuku.redirectstorage.ktx.createDeviceProtectedStorageContextCompat
import moe.shizuku.redirectstorage.ktx.whenPaid
import moe.shizuku.redirectstorage.utils.ABIHelper
import moe.shizuku.redirectstorage.utils.RomUtils
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.widget.RaisedLinearLayout
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.internal.help.HelpProvider
import java.io.File

abstract class HomeActivity : AppActivity() {

    private val purchaseChangedReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            resetActionBarSubtitle()
        }
    }

    private var popupMenu: PopupMenu? = null

    private val actionBarSubtitleView: View by lazy {
        findViewById<View>(android.R.id.summary)
    }

    val homeHeader: RaisedLinearLayout by lazy {
        findViewById<RaisedLinearLayout>(R.id.home_header)
    }

    override fun onApplyUserThemeResource(theme: Theme, isDecorView: Boolean) {
        super.onApplyUserThemeResource(theme, isDecorView)
        theme.applyStyle(R.style.ThemeOverlay_LightActionBar, true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(purchaseChangedReceiver, IntentFilter(AppConstants.ACTION_PURCHASED_CHANGED))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        resetActionBarSubtitle()
        prepareOptionsMenu()
        if (!checkUser()) {
            return
        }
        checkDevice()
        checkVersion()
        if (savedInstanceState == null) {
            val userId = if (Settings.isShowMultiUser) SRManager.USER_ALL else UserHelper.myUserId()
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, HomeFragment.newInstance(userId))
                    .commit()
        }
    }

    private fun resetActionBarSubtitle() {
        whenPaid({
            actionBarSubtitleView.visibility = View.GONE
        }, {
            actionBarSubtitleView.visibility = View.VISIBLE
        })
    }

    private fun prepareOptionsMenu() {
        val moreButton = findViewById<View>(android.R.id.button1)
        moreButton.setOnClickListener { popupMenu!!.show() }
        popupMenu = PopupMenu(moreButton.context, moreButton, Gravity.END or Gravity.TOP, 0, R.style.Widget_PopupMenu_Overflow)
        popupMenu!!.inflate(R.menu.home)
        popupMenu!!.setOnMenuItemClickListener { item: MenuItem -> onOptionsItemSelected(item) }
        onPrepareOptionsMenu(popupMenu!!.menu)
        moreButton.setOnTouchListener(popupMenu!!.dragToOpenListener)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.action_reset_tips)?.isVisible = BuildConfig.DEBUG
        menu.findItem(R.id.action_debug_info)?.isVisible = BuildConfig.DEBUG
        menu.findItem(R.id.action_changelog)?.isVisible = false
        menu.findItem(R.id.action_my_purchase)?.isVisible = false
        whenPaid({
            menu.findItem(R.id.action_unlock)?.isVisible = BuildConfig.DEBUG
        }, {
            menu.findItem(R.id.action_unlock)?.isVisible = Settings.preferences.getBoolean("show_unlock", false) || BuildConfig.DEBUG
        })

        if (SRManager.create() != null) {
            menu.findItem(R.id.action_report)?.isVisible = BuildConfig.DEBUG
            menu.findItem(R.id.action_logcat)?.isVisible = BuildConfig.DEBUG
        } else {
            menu.findItem(R.id.action_report)?.isVisible = true
            menu.findItem(R.id.action_logcat)?.isVisible = true
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_restart -> {
                showRestartDialog()
                return true
            }
            R.id.action_refresh -> {
                val fragments = supportFragmentManager.fragments
                for (fragment in fragments) {
                    if (fragment is HomeFragment) fragment.refresh()
                }
                return true
            }
            R.id.action_debug_info -> {
                showDebugInfoDialog()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkDevice() {
        try {
            val abi = ABIHelper.getABI(this)
            if (("arm64-v8a" == abi || "universal" == abi) && RomUtils.isSamsung() && !Settings.preferences.getBoolean("ignore_samsung_arm64", false)) {
                val entity = HelpProvider.get(this, "download")
                val url = if (entity != null) entity.content.get() else "https://sr.rikka.app/"
                val dialog: AlertDialog = AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_samsung_title)
                        .setMessage(HtmlCompat.fromHtml(getString(R.string.dialog_samsung_message, url)))
                        .setPositiveButton(android.R.string.ok, null)
                        .setNeutralButton(R.string.dont_show_again) { _: DialogInterface?, _: Int ->
                            Settings.preferences.edit {
                                putBoolean("ignore_samsung_arm64", true)
                            }
                        }
                        .setCancelable(false)
                        .create()
                dialog.setOnShowListener { d: DialogInterface -> (d as AlertDialog).findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance() }
                dialog.setCanceledOnTouchOutside(false)
                dialog.show()
            } else if (RomUtils.isMeizu() && !Settings.preferences.getBoolean("ignore_meizu", false)) {
                val countdownSecond = 5
                val dialog: AlertDialog = AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_meizu_title)
                        .setMessage(HtmlCompat.fromHtml(getString(R.string.dialog_meizu_message)))
                        .setPositiveButton(R.string.dialog_button_view_document) { _: DialogInterface?, _: Int ->
                            val entity = HelpProvider.get(this, "compatibility_meizu")
                            entity?.startActivity(this)
                        }
                        .setNeutralButton(R.string.dont_show_again) { _: DialogInterface?, _: Int ->
                            Settings.preferences.edit {
                                putBoolean("ignore_meizu", true)
                            }
                        }
                        .setCancelable(false)
                        .create()

                dialog.setOnShowListener {
                    dialog.findViewById<TextView>(android.R.id.message)!!.movementMethod = LinkMovementMethod.getInstance()
                    dialog.applyCountdown(AlertDialog.BUTTON_NEUTRAL, countdownSecond, getString(R.string.dont_show_again))
                }
                dialog.setCanceledOnTouchOutside(false)
                dialog.show()
            }/* else if (RomUtils.isMiui() && !Settings.preferences.getBoolean("ignore_miui", false)) {
                val countdownSecondDontShowAgain = 15
                val countdownSecondClose = 5
                val dialog = AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_miui_title)
                        .setMessage(getString(R.string.dialog_miui_message, "storage-redirect-client://help/compatibility_miui").toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                        .setPositiveButton(getString(R.string.item_summary, getString(R.string.dont_show_again), countdownSecondDontShowAgain.toString())) { _: DialogInterface?, _: Int ->
                            Settings.preferences.edit {
                                putBoolean("ignore_miui", true)
                            }
                        }
                        .setNegativeButton(getString(R.string.item_summary, getString(R.string.close), countdownSecondClose.toString()), null)
                        .setCancelable(false)
                        .create()
                dialog.setOnShowListener {
                    dialog.findViewById<TextView>(android.R.id.message)!!.movementMethod = LinkMovementMethod.getInstance()
                    dialog.applyCountdown(AlertDialog.BUTTON_POSITIVE, countdownSecondDontShowAgain, getString(R.string.dont_show_again))
                    dialog.applyCountdown(AlertDialog.BUTTON_NEGATIVE, countdownSecondClose, getString(R.string.close))
                }

                dialog.setCanceledOnTouchOutside(false)
                dialog.show()
            }*/
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun checkUser(): Boolean {
        if (UserHelper.myUserId() != 0) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_installed_in_other_user_title)
                    .setMessage(getString(R.string.dialog_installed_in_other_user_message))
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int -> finish() }
                    .setCancelable(false)
                    .show()
            return false
        }
        return true
    }

    private fun checkVersion() {
        val oldVersion = Settings.lastInstalledVersion
        if (oldVersion < 2069) {
            var apiCache = File(cacheDir, "api")
            if (apiCache.exists()) {
                apiCache.delete()
            }
            apiCache = File(createDeviceProtectedStorageContextCompat().cacheDir, "api")
            if (apiCache.exists()) {
                apiCache.delete()
            }
        }
        Settings.lastInstalledVersion = BuildConfig.VERSION_CODE
    }

    private fun showDebugInfoDialog() {
        DebugInfoDialog.newInstance().show(supportFragmentManager)
    }

    override fun shouldApplyTranslucentSystemBars(): Boolean {
        return true
    }

    override fun onApplyTranslucentSystemBars() {
        super.onApplyTranslucentSystemBars()
        window?.statusBarColor = Color.TRANSPARENT
    }
}