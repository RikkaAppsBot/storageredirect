package moe.shizuku.redirectstorage.component.filemonitor.viewholder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kotlinx.coroutines.Job;
import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.component.detail.AppDetailActivity;
import moe.shizuku.redirectstorage.dao.VerifiedAppsDao;
import moe.shizuku.redirectstorage.ktx.ContextKt;
import moe.shizuku.redirectstorage.model.FileMonitorRecord;
import moe.shizuku.redirectstorage.utils.AppIconCache;
import moe.shizuku.redirectstorage.utils.DataVerifier;
import moe.shizuku.redirectstorage.utils.IntentCompat;
import moe.shizuku.redirectstorage.utils.UserHelper;
import rikka.core.util.ClipboardUtils;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PATH;
import static moe.shizuku.redirectstorage.AppConstants.TAG;

public class IOHistoryViewHolder extends BaseViewHolder<FileMonitorRecord>
        implements View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {

    public static final Creator<FileMonitorRecord> CREATOR = (inflater, parent) ->
            new IOHistoryViewHolder(inflater.inflate(R.layout.file_monitor_item, parent, false));

    private static final List<String> STANDARD_FOLDERS = Arrays.asList(
            "Android/data",
            "Android/media",
            "Android/obb",
            Environment.DIRECTORY_ALARMS,
            Environment.DIRECTORY_DCIM,
            Environment.DIRECTORY_DOCUMENTS,
            Environment.DIRECTORY_DOWNLOADS,
            Environment.DIRECTORY_MOVIES,
            Environment.DIRECTORY_MUSIC,
            Environment.DIRECTORY_NOTIFICATIONS,
            Environment.DIRECTORY_PICTURES,
            Environment.DIRECTORY_PODCASTS,
            Environment.DIRECTORY_RINGTONES
    );

    private static final List<String> WHITELIST_PACKAGES_FOR_BEHAVIORS = Arrays.asList(
            "com.android.providers.media"
    );

    public static final String ACTION_REQUEST_SUBMIT_INVALID = BuildConfig.APPLICATION_ID + "."
            + IOHistoryViewHolder.class.getSimpleName() + ".action.REQUEST_SUBMIT_INVALID";

    public static boolean isPathInStandardFolders(@NonNull String path) {
        File runtimeStoragePathFile;
        String runtimeStoragePath = null;
        try {
            runtimeStoragePathFile = Environment.getExternalStorageDirectory();
        } catch (Exception e) {
            runtimeStoragePathFile = null;
        }
        if (runtimeStoragePathFile != null) {
            runtimeStoragePath = runtimeStoragePathFile.getAbsolutePath();
        }
        return DataVerifier.isPathInStandardFolders(STANDARD_FOLDERS, runtimeStoragePath, path);
    }

    public static boolean isPackageInWhitelistForBehaviors(@NonNull String packageName) {
        // TODO In the future, we also determine it through if it's a gallery app / music app / etc
        return WHITELIST_PACKAGES_FOR_BEHAVIORS.contains(packageName);
    }

    private final ImageView icon;
    private final TextView title;
    private final TextView time;
    private final TextView content;
    private final View warningVerifiedView;

    private final VerifiedAppsDao mVerifiedApps;

    private final MenuInflater mMenuInflater;
    private MenuItem mReportMenuItem;

    private Job mJob;

    // TODO move to other place
    private static final Map<String, ApplicationInfo> INFO_CACHE = new HashMap<>();
    private static final Map<String, CharSequence> LABEL_CACHE = new HashMap<>();

    public IOHistoryViewHolder(View itemView) {
        super(itemView);

        icon = itemView.findViewById(android.R.id.icon);
        title = itemView.findViewById(android.R.id.title);
        time = itemView.findViewById(android.R.id.text1);
        content = itemView.findViewById(android.R.id.content);
        warningVerifiedView = itemView.findViewById(R.id.warning_verified_view);

        mVerifiedApps = ContextKt.getApplication(itemView.getContext()).getDatabase().getVerifiedAppsDao();

        mMenuInflater = new MenuInflater(itemView.getContext());

        itemView.setOnCreateContextMenuListener(this);
        itemView.setOnClickListener(View::showContextMenu);
        itemView.setHapticFeedbackEnabled(false);
        itemView.setOnLongClickListener(v -> true);
    }

    @Override
    public void onBind() {
        FileMonitorRecord data = getData();
        Context context = itemView.getContext();
        PackageManager pm = context.getPackageManager();

        // Set up app label & icon views
        ApplicationInfo ai = INFO_CACHE.computeIfAbsent(data.packageName, (pair -> {
            try {
                return SRManager.createThrow().getApplicationInfo(data.packageName, 0, data.user);
            } catch (Throwable e) {
                return null;
            }
        }));

        String name;
        if (ai != null) {
            CharSequence label = LABEL_CACHE.computeIfAbsent(data.packageName, (pair -> ai.loadLabel(pm)));
            name = label.toString();
            mJob = AppIconCache.loadIconBitmapAsync(context, ai, data.user, icon);
        } else {
            name = data.packageName;
            icon.setImageDrawable(context.getDrawable(R.mipmap.ic_default_app_icon));
        }

        if (UserHelper.myUserId() != data.user) {
            title.setText(context.getString(R.string.app_name_with_user, name, data.user));
        } else {
            title.setText(name);
        }

        // Set up path & count text
        String text;
        if (data.count > 0) {
            text = String.format(Locale.ENGLISH, "%s\n* %s",
                    data.path,
                    context.getResources().getQuantityString(R.plurals.file_monitor_omit, data.count, data.count));
        } else {
            text = data.path;
        }
        content.setText(text);

        // Set up time text
        int flags = DateUtils.FORMAT_SHOW_TIME;

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        if (data.timestamp * 1000 < c.getTimeInMillis()) {
            flags |= DateUtils.FORMAT_SHOW_DATE;
        }

        time.setText(DateUtils.formatDateTime(context, data.timestamp * 1000, flags));

        // Set up warning views
        warningVerifiedView.setVisibility(shouldShowWarningVerified()
                ? View.VISIBLE : View.GONE);
        if (mReportMenuItem != null) {
            mReportMenuItem.setVisible(shouldShowWarningVerified());
        }
    }

    private boolean shouldShowWarningVerified() {
        return false;
        /*if (getData() == null) {
            return false;
        }
        if (isPackageInWhitelistForBehaviors(getData().packageName)) {
            return false;
        }
        boolean isVerified = false;
        try {
            isVerified = mVerifiedApps.find(getData().packageName) != null;
        } catch (Throwable e) {
            e.printStackTrace();
            CrashReportHelper.logException(e);
        }
        return isVerified && !isPathInStandardFolders(getData().path);*/
    }

    @Override
    public void onRecycle() {
        if (mJob != null && mJob.isActive()) {
            mJob.cancel(null);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(HtmlCompat.fromHtml(String.format("<small>%s<small>", getData().path)));
        mMenuInflater.inflate(R.menu.context_menu_io_history_item, menu);
        menu.findItem(R.id.action_open_redirect_details).setOnMenuItemClickListener(this);
        menu.findItem(R.id.action_copy_path).setOnMenuItemClickListener(this);
        mReportMenuItem = menu.findItem(R.id.action_report_nonstandard_behavior);
        mReportMenuItem.setOnMenuItemClickListener(this);
        mReportMenuItem.setVisible(shouldShowWarningVerified());
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (getData() == null) {
            Log.e(TAG, "The context data has been recycled. Ignored user operation.");
            return false;
        }

        switch (item.getItemId()) {
            case R.id.action_copy_path: {
                ClipboardUtils.put(getContext(), getData().path);
                Toast.makeText(getContext(), getContext().getString(R.string.toast_copied, getData().path), Toast.LENGTH_SHORT).show();
                return true;
            }
            case R.id.action_open_redirect_details: {
                final Intent intent = new Intent(IntentCompat.ACTION_SHOW_APP_INFO);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, AppDetailActivity.class.getName()));
                intent.putExtra(AppConstants.EXTRA_PACKAGE_NAME, getData().packageName);
                intent.putExtra(AppConstants.EXTRA_USER_ID, getData().user);
                getContext().startActivity(intent);
                return true;
            }
            case R.id.action_report_nonstandard_behavior: {
                final Intent intent = new Intent(ACTION_REQUEST_SUBMIT_INVALID);
                intent.putExtra(EXTRA_PACKAGE_NAME, getData().packageName);
                intent.putExtra(EXTRA_PATH, getData().path);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                return true;
            }
        }
        return false;
    }

}
