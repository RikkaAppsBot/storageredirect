package moe.shizuku.redirectstorage.component.detail.viewholder

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.component.detail.AppDetailViewModel
import rikka.html.text.HtmlCompat
import rikka.html.text.toHtml
import rikka.recyclerview.BaseListenerViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class AppDetailHeaderSharedUserIdViewHolder(itemView: View) : BaseListenerViewHolder<AppDetailViewModel, AppDetailHeaderSharedUserIdViewHolder.Listener>(itemView), View.OnClickListener {

    interface Listener {
        fun onSharedUserIdClick()
    }

    companion object {

        val CREATOR = Creator<AppDetailViewModel> { inflater: LayoutInflater, parent: ViewGroup? -> AppDetailHeaderSharedUserIdViewHolder(inflater.inflate(R.layout.detail_header_shared_user, parent, false)) }
    }

    var icon: ImageView = itemView.findViewById(android.R.id.icon)
    var title: TextView = itemView.findViewById(android.R.id.title)
    var summary: TextView = itemView.findViewById(android.R.id.summary)
    var button: View = itemView.findViewById(android.R.id.button1)

    init {
        summary.movementMethod = LinkMovementMethod.getInstance()
        button.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        listener?.onSharedUserIdClick()
    }

    override fun onBind() {
        val context = itemView.context
        val appInfo = data.appInfo.value!!.data!!

        val appLabel = appInfo.label
        val sharedUserName = SharedUserLabelCache.load(context.packageManager, appInfo.packageInfo.sharedUserLabel, appInfo.sharedUserId!!,
                appInfo.packageName, appInfo.applicationInfo!!)

        title.text = context.getString(R.string.item_summary, appInfo.sharedUserLabel, context.getString(R.string.shared_user_id))
        summary.text = context.getString(R.string.detail_shared_user_id_summary, appLabel, sharedUserName, "storage-redirect-client://help/shared_user_id").toHtml(HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE)
    }
}