package moe.shizuku.redirectstorage.component.home

import android.content.Context
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.api.LatestVersionInfoProvider
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.home.model.HomeStatus
import moe.shizuku.redirectstorage.event.AppConfigChangedEvent
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.lang.ConfigIOException
import moe.shizuku.redirectstorage.model.LatestVersionInfo
import moe.shizuku.redirectstorage.model.ModuleStatus
import moe.shizuku.redirectstorage.utils.CommonCheck
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.Resource
import moe.shizuku.redirectstorage.viewmodel.Status

class HomeViewModel : ViewModel(), AppConfigChangedEvent {

    companion object {

        private const val REDIRECTED_PROCESS_UPDATE_INTERVAL = 10 * 1000L
    }

    val status = MutableLiveData<Resource<HomeStatus>>(null)
    val latestVersionInfo = MutableLiveData<Resource<LatestVersionInfo>>()

    private var userId: Int = UserHelper.currentUserId()
    private var nextLoadCount: Long = 0

    private var loadDataJob: Job? = null
    private var loadCountJob: Job? = null

    init {
        Events.registerAppConfigChangedEvent(this)
    }

    override fun onEnabledChanged(packageName: String, sharedUserId: String?, userId: Int, enabled: Boolean) {
        loadData(userId)
    }

    fun loadData(userId: Int) {
        this.userId = userId

        if (status.value == null) {
            status.postValue(Resource.loading(HomeStatus.LOADING))
        }

        loadDataJob = viewModelScope.launch(Dispatchers.IO) {
            try {
                var srm = CommonCheck.getSRManagerWithCheck()
                // TODO combine with AppListViewModel

                var redirectPackages: List<RedirectPackageInfo>
                while (true) {
                    try {
                        redirectPackages = srm.getRedirectPackages(
                                SRManager.MATCH_ENABLED_ONLY or SRManager.GET_PACKAGE_INFO, userId)
                    } catch (e: NullPointerException) {
                        Thread.sleep(1000)

                        srm = CommonCheck.getSRManagerWithCheck()
                        continue
                    }
                    break
                }

                var moduleStatus = srm.moduleStatus
                if (moduleStatus == null) {
                    moduleStatus = ModuleStatus(-1, null)
                }

                val data = HomeStatus().apply {
                    redirectedAppsCount = redirectPackages.size
                    serverVersion = srm.version
                    redirectedProcessCount = srm.redirectedProcessCount
                    enhanceModuleStatus = moduleStatus
                    enhanceModuleVersion = moduleStatus.versionCode
                    enhanceModule = enhanceModuleVersion > 0
                }

                val debugInfo = srm.getDebugInfo(0)
                if (debugInfo.exceptions != null && debugInfo.exceptions.isNotEmpty()) {
                    val hash = buildString {
                        for (exception in debugInfo.exceptions) {
                            append(exception).append("\n\n")
                        }
                    }.hashCode().toLong()

                    if (hash != Settings.preferences
                                    .getLong("last_reported_exception_hash", 0)) {
                        CrashReportHelper.logException(ConfigIOException(), null, debugInfo.exceptions)

                        Settings.preferences.edit {
                            putLong("last_reported_exception_hash", hash)
                        }
                    }
                }

                status.postValue(Resource.success(data))
                nextLoadCount = System.currentTimeMillis() + REDIRECTED_PROCESS_UPDATE_INTERVAL
                scheduleUpdate()
            } catch (e: CancellationException) {
                return@launch
            } catch (e: Throwable) {
                status.postValue(Resource.error(e, null))
            }
        }
    }

    fun loadLatestVersionInfo(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
            LatestVersionInfoProvider.load(context)

            val info = LatestVersionInfoProvider.get(context)
            latestVersionInfo.postValue(Resource.success(info))

            if (status.value?.status == Status.SUCCESS) {
                status.postValue(status.value)
            }
        }
    }

    fun scheduleUpdate() {
        if (status.value?.status != Status.SUCCESS
                || status.value?.data == null
                || loadCountJob?.isActive == true) {
            return
        }

        val data = status.value?.data!!
        loadCountJob = viewModelScope.launch(Dispatchers.IO) {
            try {
                while (true) {
                    delay(REDIRECTED_PROCESS_UPDATE_INTERVAL)

                    val srm = CommonCheck.getSRManagerWithCheck()
                    data.redirectedProcessCount = srm.redirectedProcessCount

                    status.postValue(Resource.success(data))
                }
            } catch (e: CancellationException) {
                return@launch
            } catch (e: Throwable) {
                status.postValue(Resource.error(e, null))
            }
        }
    }

    fun unscheduleUpdate() {
        loadCountJob?.cancel()
    }

    override fun onCleared() {
        Events.unregisterAppConfigChangedEvent(this)
        loadDataJob?.cancel()
        loadCountJob?.cancel()
    }

}
