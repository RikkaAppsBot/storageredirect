package moe.shizuku.redirectstorage.component.managespace;

import android.os.Bundle;

import androidx.annotation.Nullable;

import moe.shizuku.redirectstorage.app.AppActivity;
import moe.shizuku.redirectstorage.component.managespace.fragment.ManageSpaceFragment;

public class ManageSpaceActivity extends AppActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new ManageSpaceFragment())
                    .commit();
        }
    }

}
