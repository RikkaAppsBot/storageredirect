package moe.shizuku.redirectstorage.component.settings

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity
import moe.shizuku.redirectstorage.component.restore.fragment.BackupRestoreSettingsFragment
import moe.shizuku.redirectstorage.component.sharehelper.fragment.ShareHelperListPreferencesFragment

class SettingsActivity : AppBarFragmentActivity() {

    private val action: String get() = intent?.action ?: ""

    private val titleResourceId: Int
        @StringRes
        get() = when (action) {
            BehaviorSettingsFragment.ACTION ->
                R.string.settings_behavior
            StartUpSettingsFragment.ACTION ->
                R.string.settings_start
            UserInterfaceSettingsFragment.ACTION ->
                R.string.settings_user_interface
            OnlineRulesSettingsFragment.ACTION ->
                R.string.settings_online_rules
            WorkModeSettingsFragment.ACTION ->
                R.string.home_working_mode
            ShareHelperListPreferencesFragment.ACTION ->
                R.string.settings_direct_share_preferences
            BackupRestoreSettingsFragment.ACTION ->
                R.string.backup_restore
            ModuleSettingsFragment.ACTION ->
                R.string.working_mode_enhance_mode
            MoreSettingsFragment.ACTION ->
                R.string.settings_more
            else -> R.string.settings
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appBar?.setDisplayHomeAsUpEnabled(true)
        appBar?.setTitle(titleResourceId)

        if (savedInstanceState == null) {
            val fragment = createFragment()
            supportFragmentManager.commit {
                replace(R.id.fragment_container, fragment)
                setMaxLifecycle(fragment, Lifecycle.State.RESUMED)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return true
    }

    private fun createFragment(): Fragment {
        return when (action) {
            BehaviorSettingsFragment.ACTION ->
                BehaviorSettingsFragment()
            StartUpSettingsFragment.ACTION ->
                StartUpSettingsFragment()
            UserInterfaceSettingsFragment.ACTION ->
                UserInterfaceSettingsFragment()
            OnlineRulesSettingsFragment.ACTION ->
                OnlineRulesSettingsFragment()
            WorkModeSettingsFragment.ACTION ->
                WorkModeSettingsFragment()
            ShareHelperListPreferencesFragment.ACTION ->
                ShareHelperListPreferencesFragment()
            BackupRestoreSettingsFragment.ACTION ->
                BackupRestoreSettingsFragment()
            ModuleSettingsFragment.ACTION ->
                ModuleSettingsFragment()
            MoreSettingsFragment.ACTION ->
                MoreSettingsFragment()
            else -> MainSettingsFragment()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home && titleResourceId != R.string.settings) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
