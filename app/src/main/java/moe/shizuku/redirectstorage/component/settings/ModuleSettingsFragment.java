package moe.shizuku.redirectstorage.component.settings;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import java.util.Objects;

import moe.shizuku.preference.SwitchPreference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.preference.MemoryPreferenceDataStore;
import moe.shizuku.redirectstorage.utils.CrashReportHelper;
import moe.shizuku.redirectstorage.utils.ModuleUtils;

public class ModuleSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.MODULE";

    private SRManager mSRManager;

    private static void showDialog(Context context, int message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        getPreferenceManager().setPreferenceDataStore(new MemoryPreferenceDataStore());
        addPreferencesFromResource(R.xml.settings_module);

        SwitchPreference stopPreference = (SwitchPreference) findPreference("module_stop");
        SwitchPreference earlyStartPreference = (SwitchPreference) findPreference("module_early_start");
        SwitchPreference fileMonitorPreference = (SwitchPreference) findPreference("module_file_monitor");
        SwitchPreference fixRenamePreference = (SwitchPreference) findPreference("module_fix_rename");
        SwitchPreference fixInteractionPreference = (SwitchPreference) findPreference("module_fix_interaction");
        SwitchPreference blockRemountPreference = (SwitchPreference) findPreference("module_block_remount");
        SwitchPreference disableExportNotification = (SwitchPreference) findPreference("module_disable_export_notification");
        SwitchPreference newAppNotification = (SwitchPreference) findPreference("new_app_notification");

        stopPreference.setOnPreferenceChangeListener((preference, newValue) -> false);
        earlyStartPreference.setOnPreferenceChangeListener((preference, newValue) -> false);
        blockRemountPreference.setOnPreferenceChangeListener((preference, newValue) -> false);

        try {
            mSRManager = SRManager.createThrow();
        } catch (Exception ignored) {
        }

        if (mSRManager == null)
            return;

        int version;
        boolean activated;

        try {
            version = mSRManager.getModuleStatus().getVersionCode();
            activated = version > 0;

            stopPreference.setEnabled(activated);
            stopPreference.setChecked(activated);
            earlyStartPreference.setEnabled(activated);
            earlyStartPreference.setChecked(activated);
            blockRemountPreference.setEnabled(activated);
            blockRemountPreference.setChecked(activated);
            fileMonitorPreference.setChecked(version >= ModuleUtils.V23_0_B1 && mSRManager.isFileMonitorEnabled());
            fileMonitorPreference.setEnabled(activated);
            fixRenamePreference.setChecked(version >= ModuleUtils.V23_0_B1 && mSRManager.isFixRenameEnabled());
            fixRenamePreference.setEnabled(activated);
            fixInteractionPreference.setChecked(version >= ModuleUtils.V23_0_B1 && mSRManager.isFixInteractionEnabled());
            fixInteractionPreference.setEnabled(activated);
            disableExportNotification.setChecked(activated && mSRManager.isDisableExportNotificationEnabled());
            disableExportNotification.setEnabled(activated);
            newAppNotification.setChecked(version >= ModuleUtils.V23_0_B1 && mSRManager.isNewAppNotificationEnabled());
            newAppNotification.setEnabled(activated);
        } catch (Exception ignored) {
            return;
        }

        if (!activated) {
            new AlertDialog.Builder(requireContext())
                    .setMessage(R.string.dialog_module_settings_not_installed_message)
                    .setPositiveButton(android.R.string.ok, null)
                    .setOnDismissListener(dialog -> {
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    })
                    .show();
        }

        fileMonitorPreference.setOnPreferenceChangeListener((preference, newValue) -> {
            try {
                if (ModuleUtils.showDialogWhenVersionTooLow(requireContext(), mSRManager.getModuleStatus().getVersionCode(), ModuleUtils.V23_0_B1)) {
                    return false;
                }

                mSRManager.setFileMonitorEnabled((Boolean) newValue);
                showDialog(requireContext(), R.string.toast_change_option_require_force_stop_app);
                return true;
            } catch (Throwable e) {
                e.printStackTrace();

                CrashReportHelper.logException(e);
                Toast.makeText(requireContext(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        fixRenamePreference.setOnPreferenceChangeListener((preference, newValue) -> {
            try {
                if (ModuleUtils.showDialogWhenVersionTooLow(requireContext(), mSRManager.getModuleStatus().getVersionCode(), ModuleUtils.V23_0_B1)) {
                    return false;
                }

                mSRManager.setFixRenameEnabled((Boolean) newValue);
                showDialog(requireContext(), R.string.toast_change_option_require_force_stop_app);
                return true;
            } catch (Throwable e) {
                e.printStackTrace();

                CrashReportHelper.logException(e);
                Toast.makeText(requireContext(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        fixInteractionPreference.setOnPreferenceChangeListener((preference, newValue) -> {
            try {
                if (ModuleUtils.showDialogWhenVersionTooLow(requireContext(), mSRManager.getModuleStatus().getVersionCode(), ModuleUtils.V23_0_B1)) {
                    return false;
                }

                mSRManager.setFixInteractionEnabled((Boolean) newValue);
                showDialog(requireContext(), R.string.toast_change_option_require_force_stop_app);
                return true;
            } catch (Throwable e) {
                e.printStackTrace();

                CrashReportHelper.logException(e);
                Toast.makeText(requireContext(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        disableExportNotification.setOnPreferenceChangeListener((preference, newValue) -> {
            try {
                mSRManager.setDisableExportNotificationEnabled((Boolean) newValue);
                return true;
            } catch (Throwable e) {
                e.printStackTrace();

                CrashReportHelper.logException(e);
                Toast.makeText(requireContext(), getString(R.string.toast_failed, Objects.toString(e, "unknown")), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        newAppNotification.setOnPreferenceChangeListener((preference, newValue) -> {
            try {
                if (ModuleUtils.showDialogWhenVersionTooLow(requireContext(), mSRManager.getModuleStatus().getVersionCode(), ModuleUtils.V23_0_B1)) {
                    return false;
                }

                mSRManager.setNewAppNotificationEnabled((Boolean) newValue);
                return true;
            } catch (Throwable e) {
                e.printStackTrace();
                return false;
            }
        });
    }
}