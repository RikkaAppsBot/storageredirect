package moe.shizuku.redirectstorage.component.applist

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.*
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.component.detail.AppInfoCache
import moe.shizuku.redirectstorage.dao.VerifiedAppsDao
import moe.shizuku.redirectstorage.event.AppConfigChangedEvent
import moe.shizuku.redirectstorage.event.Events
import moe.shizuku.redirectstorage.event.GlobalConfigChangedEvent
import moe.shizuku.redirectstorage.event.PackageInstallationEvent
import moe.shizuku.redirectstorage.ktx.application
import moe.shizuku.redirectstorage.model.AppInfo
import moe.shizuku.redirectstorage.utils.AppInfoComparator
import moe.shizuku.redirectstorage.utils.CommonCheck
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.SourcedResource
import moe.shizuku.redirectstorage.viewmodel.Status
import java.util.*
import kotlin.collections.ArrayList

class AppListViewModel : ViewModel(), GlobalConfigChangedEvent, AppConfigChangedEvent, PackageInstallationEvent {

    companion object {
        private const val FILTER_SHOW_SYSTEM = R.id.action_filter_show_system
        private const val FILTER_SHOW_DISABLED = R.id.action_filter_show_disabled
        private const val FILTER_HIDE_VERIFIED = R.id.action_filter_hide_verified
    }

    val appList = MutableLiveData<SourcedResource<List<AppInfo>, String?>>(null)

    private val fullList = Collections.synchronizedList(ArrayList<AppInfo>())

    var isSearching: Boolean = false
    var keyword = ""
    private val filterParams = mutableMapOf(
            FILTER_SHOW_SYSTEM to Settings.isFilterShowSystem,
            FILTER_SHOW_DISABLED to Settings.isFilterShowDisabled,
            FILTER_HIDE_VERIFIED to Settings.isFilterHideVerified
    )

    private val filter: (AppInfo) -> Boolean = {
        (filterParams[FILTER_SHOW_SYSTEM] == true || !it.isSystemApp) &&
                (filterParams[FILTER_SHOW_DISABLED] == true || it.isPackageEnabled) &&
                (filterParams[FILTER_HIDE_VERIFIED] == false || !it.isVerified() || it.isRedirectEnabled) &&
                (!isSearching || keyword.isNotBlank() && (it.label?.contains(keyword, ignoreCase = true) == true
                        || it.packageName.contains(keyword, ignoreCase = true)))
    }

    var userId: Int = 0
    var isEditingFilter: Boolean = false
    var bannerCreators: Queue<Class<AppListFragment.BannerCreator>>? = null

    private val preferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            Settings.LIST_FILTER_SHOW_SYSTEM -> {
                filterParams[FILTER_SHOW_SYSTEM] = Settings.isFilterShowSystem
                invalidateList("filter")
            }
            Settings.LIST_FILTER_SHOW_DISABLED -> {
                filterParams[FILTER_SHOW_DISABLED] = Settings.isFilterShowDisabled
                invalidateList("filter")
            }
            Settings.LIST_FILTER_HIDE_VERIFIED -> {
                filterParams[FILTER_HIDE_VERIFIED] = Settings.isFilterHideVerified
                invalidateList("filter")
            }
        }
    }

    init {
        Events.registerGlobalConfigChangedEvent(this)
        Events.registerAppConfigChangedEvent(this)
        Events.registerPackageInstallationEvent(this)
        Settings.preferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    override fun onCleared() {
        Events.unregisterGlobalConfigChangedEvent(this)
        Events.unregisterAppConfigChangedEvent(this)
        Events.unregisterPackageInstallationEvent(this)
        Settings.preferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    fun invalidateList(source: String) {
        if (appList.value?.status != Status.SUCCESS) {
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            handleList(source)
        }
    }

    private fun handleList(source: String) {
        val list = fullList.filter(filter).sortedWith(AppInfoComparator.createFromSRSettings()).toList()

        appList.postValue(SourcedResource.success(list, source))
    }

    fun reload(context: Context) {
        val source = "refresh"
        appList.postValue(SourcedResource.loading(null, source))

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val srm = CommonCheck.getSRManagerWithCheck()

                val pm = context.packageManager
                val flags = SRManager.MATCH_ALL_PACKAGES or SRManager.GET_PACKAGE_INFO
                val result = srm.getRedirectPackages(flags, userId)
                        .map { rpi ->
                            val ai = AppInfo(rpi, pm).also {
                                it.loadIsVerified(context)
                                SharedUserLabelCache.load(context.packageManager, it)
                            }
                            AppInfoCache[ai.packageName, ai.userId] = ai
                            ai
                        }


                fullList.clear()
                fullList.addAll(result)

                handleList(source)

                if (Settings.isOnlineRulesEnabled) {
                    reloadVerifiedList(context)
                }
            } catch (e: CancellationException) {

            } catch (e: Throwable) {
                appList.postValue(SourcedResource.error(e, null, source))
            }
        }
    }

    private suspend fun reloadVerifiedList(context: Context) {
        val verifiedLastUpdateTime = VerifiedAppsDao.getLastUpdateTime(context)
        if (System.currentTimeMillis() - verifiedLastUpdateTime
                <= AppConstants.VERIFIED_APPS_CACHE_MAX_ALIVE_TIME) {
            return
        }

        try {
            val response = if (Settings.isCustomRepoEnabled) context.application.rikkaAppApiService
                    .getVerifiedAppsSync()
            else context.application.rikkaAppApiService
                    .getVerifiedAppsSync()

            if (response.isSuccessful) {
                val verifiedApps = response.body()
                verifiedApps?.let { result ->
                    context.application.database
                            .verifiedAppsDao
                            .update(context, result)
                }

                fullList.forEach { it.loadIsVerified(context) }

                invalidateList("verified_updated")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onEnabledChanged(packageName: String, sharedUserId: String?, userId: Int, enabled: Boolean) {
        fullList.filter { it.userId == userId && (it.packageName == packageName || (sharedUserId != null && it.sharedUserId == sharedUserId)) }
                .forEach { it.isRedirectEnabled = enabled }

        // trigger observer
        appList.value = appList.value
    }

    override fun onMountDirsConfigChanged(packageName: String, sharedUserId: String?, userId: Int, config: MountDirsConfig) {
        fullList.filter { it.userId == userId && (it.packageName == packageName || (sharedUserId != null && it.sharedUserId == sharedUserId)) }
                .forEach { it.redirectInfo.mountDirsConfig = config }

        // trigger observer
        appList.value = appList.value
    }

    override fun onRedirectTargetChanged(packageName: String, sharedUserId: String?, userId: Int, path: String?) {
        fullList.filter { it.userId == userId && (it.packageName == packageName || (sharedUserId != null && it.sharedUserId == sharedUserId)) }
                .forEach { it.redirectInfo.redirectTarget = path }

        // trigger observer
        appList.value = appList.value
    }

    override fun onMountDirsTemplateAdded(template: MountDirsTemplate) {
        if (template.packages == null) {
            return
        }

        onMountDirsTemplateChanged(template)
    }

    override fun onMountDirsTemplateRemoved(template: MountDirsTemplate) {
        if (template.packages == null) {
            return
        }

        val removed = MountDirsTemplate(template)
        removed.packages = emptyList()
        onMountDirsTemplateChanged(removed)
    }

    override fun onMountDirsTemplateChanged(newTemplate: MountDirsTemplate, oldTemplate: MountDirsTemplate) {
        if (newTemplate.packages == null || newTemplate.packages == oldTemplate.packages) {
            return
        }

        onMountDirsTemplateChanged(newTemplate)
    }

    private fun onMountDirsTemplateChanged(template: MountDirsTemplate) {
        if (appList.value?.status != Status.SUCCESS) {
            return
        }

        appList.value?.data?.forEach {
            it.redirectInfo.mountDirsConfig.apply {
                if (template.packages!!.contains(it.redirectInfo)) {
                    if (templatesIds == null) {
                        templatesIds = HashSet()
                    }
                    templatesIds!!.add(template.id)
                } else {
                    if (templatesIds != null && templatesIds!!.remove(template.id)) {
                        if (templatesIds!!.isEmpty()) {
                            templatesIds = null
                        }
                    }
                }
                isTemplatesAllowed = !templatesIds.isNullOrEmpty()
            }
        }

        // trigger observer
        appList.value = appList.value
    }

    override fun onPackageAdded(info: RedirectPackageInfo) {
        if (appList.value?.status != Status.SUCCESS) {
            return
        }
        if (!Settings.isShowMultiUser && info.userId != UserHelper.myUserId()) {
            return
        }

        val ai = AppInfo(info, ApplicationContext.get().packageManager)
        if (fullList.contains(ai)) {
            return
        }
        fullList.add(ai)
        invalidateList("package_add")
    }

    override fun onPackageRemoved(packageName: String?, userId: Int) {
        if (appList.value?.status != Status.SUCCESS) {
            return
        }
        if (packageName == null) {
            return
        }

        fullList.removeAll {
            it.packageName == packageName && it.userId == userId
        }
        invalidateList("package_remove")
    }
}
