package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.TextView;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.widget.ExpandableLayout;
import rikka.recyclerview.BaseViewHolder;

public class CollapsibleSummaryViewHolder extends BaseViewHolder<Object[]> implements View.OnClickListener, Checkable {

    public static final Creator<Object[]> CREATOR = (inflater, parent) -> new CollapsibleSummaryViewHolder(inflater.inflate(R.layout.collapsible_summary, parent, false));

    public static final Creator<Object[]> CREATOR2 = (inflater, parent) -> new CollapsibleSummaryViewHolder(inflater.inflate(R.layout.collapsible_summary2, parent, false));

    private TextView text1;
    private TextView text2;
    private CheckedTextView button;
    private ExpandableLayout expandableLayout;

    public CollapsibleSummaryViewHolder(View itemView) {
        super(itemView);

        text1 = itemView.findViewById(android.R.id.text1);
        text2 = itemView.findViewById(android.R.id.text2);
        button = itemView.findViewById(android.R.id.button1);
        expandableLayout = itemView.findViewById(R.id.expandable);
        expandableLayout.setExpanded(true, false);

        button.setOnClickListener(this);
        text2.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    public void onBind() {
        text1.setText((CharSequence) getData()[0]);
        text2.setText((CharSequence) getData()[1]);

        syncViewState(false);
    }

    @Override
    public void onClick(View v) {
        setChecked(!button.isChecked());
        syncViewState(true);
    }

    public boolean isChecked() {
        return Settings.getPreferences().getBoolean((String) getData()[2], true);
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }

    public void setChecked(boolean checked) {
        Settings.getPreferences().edit().putBoolean((String) getData()[2], checked).apply();
    }

    private void syncViewState(boolean animate) {
        button.setChecked(isChecked());
        button.setText(isChecked() ? R.string.btn_collapse_help : R.string.btn_expand_help);
        expandableLayout.setExpanded(isChecked(), animate);
    }
}
