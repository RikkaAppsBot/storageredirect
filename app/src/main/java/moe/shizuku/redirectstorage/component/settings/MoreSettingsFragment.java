package moe.shizuku.redirectstorage.component.settings;

import android.os.Bundle;

import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import moe.shizuku.preference.SwitchPreference;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.provider.DownloadsProvider;
import moe.shizuku.redirectstorage.utils.PackageManagerUtils;

public class MoreSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.MORE";

    @Override
    public boolean isVerticalPaddingRequired() {
        return false;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.setting_more);

        SwitchPreference enableCrashReport = (SwitchPreference) findPreference("enable_crash_report");
        SwitchPreference showFiles = (SwitchPreference) findPreference("show_files_from_redirected_storage");

        showFiles.setChecked(PackageManagerUtils.isComponentEnabled(
                requireContext(), DownloadsProvider.COMPONENT_NAME));
        showFiles.setOnPreferenceChangeListener((pref, o) -> {
            boolean b = (boolean) o;
            PackageManagerUtils.setComponentEnabled(
                    requireContext(), DownloadsProvider.COMPONENT_NAME, b);
            return true;
        });

        Crashes.isEnabled().thenAccept(value -> {
            enableCrashReport.setChecked(value);
            enableCrashReport.setOnPreferenceChangeListener((preference, newValue) -> {
                if (!(newValue instanceof Boolean)) {
                    return false;
                }
                boolean value1 = (boolean) newValue;
                Crashes.setEnabled(value1).get();
                Analytics.setEnabled(value1).get();
                return Crashes.isEnabled().get() == value1;
            });
        });
    }
}
