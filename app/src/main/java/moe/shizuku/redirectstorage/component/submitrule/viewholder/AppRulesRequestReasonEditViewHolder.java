package moe.shizuku.redirectstorage.component.submitrule.viewholder;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppConfigurationBuilder;
import moe.shizuku.redirectstorage.utils.SpannableEditorSelectionActionModeCallback;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.BaseViewHolder;

/**
 * Created by fytho on 2018/1/21.
 */

public class AppRulesRequestReasonEditViewHolder extends BaseViewHolder<AppConfigurationBuilder> {

    public static final Creator<AppConfigurationBuilder> CREATOR = (inflater, parent) -> new AppRulesRequestReasonEditViewHolder(inflater.inflate(R.layout.app_rules_request_reason_edit, parent, false));

    private EditText mEditText;

    public AppRulesRequestReasonEditViewHolder(View itemView) {
        super(itemView);

        mEditText = itemView.findViewById(android.R.id.edit);
        ActionMode.Callback callback = new SpannableEditorSelectionActionModeCallback(mEditText, this::putDataToBuilder);
        mEditText.setCustomSelectionActionModeCallback(callback);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                putDataToBuilder();
            }
        });
    }

    private void putDataToBuilder() {
        getData().setDefaultReason(SpannableEditorSelectionActionModeCallback.toHtml(mEditText.getText()));
    }

    @Override
    public void onBind() {
        super.onBind();

        syncData();
    }

    @Override
    public void onBind(@NonNull List<Object> payloads) {
        super.onBind(payloads);

        syncData();
    }

    public void syncData() {
        String data = getData().getDefaultReason();
        if (data == null) data = "";
        mEditText.setText(HtmlCompat.fromHtml(data));
    }
}
