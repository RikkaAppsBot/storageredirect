package moe.shizuku.redirectstorage.component.logcat

import android.os.Bundle
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppBarFragmentActivity

class LogcatActivity : AppBarFragmentActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (appBar != null) {
            appBar!!.setDisplayHomeAsUpEnabled(true)
            appBar!!.setRaised(true)
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, LogcatFragment())
                    .commit()
        }
    }
}