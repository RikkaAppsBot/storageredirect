package moe.shizuku.redirectstorage.component.filebrowser.dialog;

import android.os.Bundle;
import android.text.format.Formatter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.model.ParcelStructStat;
import rikka.html.text.HtmlCompat;

public class FileStatDialog extends AlertDialogFragment {

    private static final String EXTRA_FILE_STAT = BuildConfig.APPLICATION_ID + ".extra.FILE_STAT";
    private static final String EXTRA_FILE_URI = BuildConfig.APPLICATION_ID + ".extra.FILE_URI";

    private static final Set<Character> DIGIT_CHARS =
            new HashSet<>(Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'));

    public static FileStatDialog newInstance(@NonNull ParcelStructStat structStat, @Nullable String fileUri) {
        FileStatDialog dialog = new FileStatDialog();
        Bundle arguments = new Bundle();
        arguments.putParcelable(EXTRA_FILE_STAT, structStat);
        arguments.putString(EXTRA_FILE_URI, fileUri);
        dialog.setArguments(arguments);
        return dialog;
    }

    private ParcelStructStat mStructStat;
    private @Nullable String mFileUri;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null) {
            throw new IllegalArgumentException();
        }

        mStructStat = getArguments().getParcelable(EXTRA_FILE_STAT);
        if (getArguments().containsKey(EXTRA_FILE_URI)) {
            mFileUri = getArguments().getString(EXTRA_FILE_URI);
        }
    }

    @Override
    public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder,
                                   @Nullable Bundle savedInstanceState) {
        builder.setTitle(R.string.action_view_properties);
        builder.setMessage(HtmlCompat.fromHtml(buildMessageText(), HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS));
        builder.setPositiveButton(android.R.string.ok, null);
    }

    private String buildMessageText() {
        final Object[] args = new Object[7];
        args[0] = formatLongToTime(mStructStat.st_atime * 1000);
        args[1] = formatLongToTime(mStructStat.st_mtime * 1000);
        args[2] = formatLongToTime(mStructStat.st_ctime * 1000);
        String fileSize = Formatter.formatFileSize(requireContext(), mStructStat.st_size);
        // Fix missing spacing between number and unit
        try {
            if (fileSize != null && !fileSize.contains(" ")) {
                int lastNumIndex = -1;
                for (int index = 0; index < fileSize.length(); index++) {
                    if (!DIGIT_CHARS.contains(fileSize.charAt(index))) {
                        break;
                    }
                    lastNumIndex = index;
                }
                if (lastNumIndex != -1
                        && (lastNumIndex != fileSize.length() - 1)
                        && fileSize.charAt(lastNumIndex + 1) != ' ') {
                    fileSize = fileSize.substring(0, lastNumIndex + 1)
                            + " "
                            + fileSize.substring(lastNumIndex + 1);
                }
            }
        } catch (Exception ignored) {

        }
        args[3] = fileSize;
        //noinspection OctalInteger
        args[4] = mStructStat.st_mode & 0777;
        args[5] = mStructStat.st_uid;
        args[6] = mStructStat.st_gid;

        StringBuilder sb = new StringBuilder();
        if (mFileUri != null) {
            sb.append(getString(R.string.dialog_file_stat_message_file_format, mFileUri));
            sb.append("\n\n");
        }
        sb.append(getString(R.string.dialog_file_stat_message_format, args));
        return sb.toString();
    }

    private static String formatLongToTime(long time) {
        return SimpleDateFormat.getDateTimeInstance().format(new Date(time));
    }

}
