package moe.shizuku.redirectstorage.component.settings;


import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import moe.shizuku.preference.EditTextPreference;
import moe.shizuku.preference.Preference;
import moe.shizuku.preference.PreferenceCategory;
import moe.shizuku.preference.SwitchPreference;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.api.GitHubApi;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import moe.shizuku.redirectstorage.utils.SharedPreferenceDataStore;

public class OnlineRulesSettingsFragment extends SettingsFragment {

    public static final String ACTION = "moe.shizuku.redirectstorage.settings.ONLINE_RULES";

    private Preference customRepoSetPreference;
    private Preference customRepoBranchPreference;

    private PreferenceCategory githubCategory;
    private Preference clearAccessTokenPreference;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);
        addPreferencesFromResource(R.xml.settings_online_rules);

        SwitchPreference enablePref = (SwitchPreference) findPreference("custom_repo_enabled");
        enablePref.setOnPreferenceChangeListener((pref, newValue) -> {
            customRepoSetPreference.setEnabled((Boolean) newValue);
            customRepoBranchPreference.setEnabled((Boolean) newValue);
            return true;
        });
        customRepoSetPreference = findPreference("custom_repo_set");
        customRepoBranchPreference = findPreference("custom_repo_branch");
        customRepoSetPreference.setEnabled(Settings.isCustomRepoEnabled());
        customRepoBranchPreference.setEnabled(Settings.isCustomRepoEnabled());
        updateCustomRulesSummary();

        customRepoSetPreference.setOnPreferenceClickListener(pref -> {
            CustomRulesSetDialog
                    .newInstance(dialog -> updateCustomRulesSummary())
                    .show(getChildFragmentManager());
            return true;
        });

        EditTextPreference branchPref = (EditTextPreference) findPreference("custom_repo_branch");
        //noinspection ConstantConditions
        ((SharedPreferenceDataStore) getPreferenceManager().getPreferenceDataStore()).getSharedPreferences()
                .registerOnSharedPreferenceChangeListener((sp, key) -> {
                    if (Settings.CUSTOM_REPO_BRANCH.equals(key)) {
                        if (TextUtils.isEmpty(sp.getString(key, null))) {
                            sp.edit().putString(key, Settings.DEFAULT_REPO_BRANCH).apply();
                            branchPref.setText(Settings.DEFAULT_REPO_BRANCH);
                            branchPref.setSummary(Settings.DEFAULT_REPO_BRANCH);
                        }
                    }
                });

        githubCategory = (PreferenceCategory) findPreference("github");
        clearAccessTokenPreference = findPreference("clear_github_token");

        clearAccessTokenPreference.setOnPreferenceClickListener(pref -> {
            Settings.setGithubToken("");
            GitHubApi.logout();
            updateGitHubPreferences();
            return true;
        });

        updateGitHubPreferences();
    }

    private void updateCustomRulesSummary() {
        customRepoSetPreference.setSummary(
                Settings.getCustomRulesRepoOwner() + "/" + Settings.getCustomRulesRepoName());
    }

    private void updateGitHubPreferences() {
        clearAccessTokenPreference.setVisible(!TextUtils.isEmpty(Settings.getGithubToken()));
        githubCategory.setVisible(clearAccessTokenPreference.isVisible());
    }

    public static class CustomRulesSetDialog extends AlertDialogFragment {

        private static final NumberKeyListener GITHUB_USERNAME_KEY_LISTENER =
                new NumberKeyListener() {
                    @NonNull
                    @Override
                    protected char[] getAcceptedChars() {
                        return ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                                "abcdefghijklmnopqrstuvwxyz" +
                                "0123456789" +
                                "-").toCharArray();
                    }

                    @Override
                    public int getInputType() {
                        return InputType.TYPE_CLASS_TEXT;
                    }
                };

        private static final NumberKeyListener GITHUB_REPONAME_KEY_LISTENER =
                new NumberKeyListener() {
                    @NonNull
                    @Override
                    protected char[] getAcceptedChars() {
                        return ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                                "abcdefghijklmnopqrstuvwxyz" +
                                "0123456789" +
                                "-_").toCharArray();
                    }

                    @Override
                    public int getInputType() {
                        return InputType.TYPE_CLASS_TEXT;
                    }
                };

        private EditText mRepoOwnerEdit, mRepoNameEdit;
        private @Nullable
        DialogInterface.OnDismissListener mOnDismissListener;

        public static CustomRulesSetDialog newInstance(
                @Nullable DialogInterface.OnDismissListener onDismissListener) {
            CustomRulesSetDialog dialog = new CustomRulesSetDialog();
            dialog.setOnDismissListener(onDismissListener);
            return dialog;
        }

        public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            mOnDismissListener = onDismissListener;
        }

        @Override
        public int onGetContentViewLayoutResource() {
            return R.layout.online_rules_custom_repo_set_dialog_content;
        }

        @Override
        public void onBuildAlertDialog(@NonNull AlertDialog.Builder builder, @Nullable Bundle savedInstanceState) {
            builder.setTitle(R.string.settings_custom_repo_dialog_title);
            builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
                Settings.setCustomRepoOwner(mRepoOwnerEdit.getText().toString());
                Settings.setCustomRepoName(mRepoNameEdit.getText().toString());
            });
            builder.setNegativeButton(android.R.string.cancel, null);
        }

        @Override
        public void onAlertDialogCreated(@NonNull AlertDialog dialog, View contentView, @Nullable Bundle savedInstanceState) {
            mRepoOwnerEdit = contentView.findViewById(R.id.repo_owner_edit);
            mRepoNameEdit = contentView.findViewById(R.id.repo_name_edit);
            mRepoOwnerEdit.setText(Settings.getCustomRulesRepoOwner());
            mRepoNameEdit.setText(Settings.getCustomRulesRepoName());
            mRepoOwnerEdit.setHint(getString(R.string.settings_custom_repo_dialog_edit_repo_owner_hint, BuildConfig.RULE_REPO_USER));
            mRepoNameEdit.setHint(getString(R.string.settings_custom_repo_dialog_edit_repo_name_hint, BuildConfig.RULE_REPO_NAME));

            mRepoOwnerEdit.setKeyListener(GITHUB_USERNAME_KEY_LISTENER);
            mRepoNameEdit.setKeyListener(GITHUB_REPONAME_KEY_LISTENER);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (mOnDismissListener != null) {
                mOnDismissListener.onDismiss(dialog);
            }
        }
    }
}
