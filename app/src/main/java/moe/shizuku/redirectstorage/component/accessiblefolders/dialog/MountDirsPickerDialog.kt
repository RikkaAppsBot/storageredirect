package moe.shizuku.redirectstorage.component.accessiblefolders.dialog

import android.os.Bundle
import android.os.Environment
import androidx.core.os.bundleOf
import moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME
import moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRFileManager
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.dialog.picker.MultiFilesPickerDialog
import moe.shizuku.redirectstorage.model.SelectableServerFile
import moe.shizuku.redirectstorage.model.ServerFile
import moe.shizuku.redirectstorage.utils.UserHelper
import java.util.*

class MountDirsPickerDialog : MultiFilesPickerDialog() {

    companion object {

        @JvmStatic
        fun newInstance(userId: Int,
                        packageName: String? = null,
                        selectedItems: Collection<String>? = null)
                : MountDirsPickerDialog {
            return MountDirsPickerDialog().apply {
                arguments = bundleOf(
                        EXTRA_CURRENT_FILE to ServerFile.fromStorageRoot("", userId),
                        EXTRA_USER_ID to userId,
                        EXTRA_PACKAGE_NAME to packageName,
                        EXTRA_SELECTED_FILES to
                                selectedItems?.map { item ->
                                    val file = ServerFile.fromStorageRoot(item, userId)
                                    file.setIsDirectory(true)
                                    file
                                }?.let { ArrayList(it) }
                )
            }
        }

    }

    private lateinit var fileManager: SRFileManager

    private val isApp: Boolean get() = packageName != null
    private var packageName: String? = null
    private var userId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments!!.let {
            packageName = it.getString(EXTRA_PACKAGE_NAME)
            userId = it.getInt(EXTRA_USER_ID, UserHelper.currentUserId())
//            val title = it.get(EXTRA_TITLE)
//            when (title) {
//                is String -> setTitle(title)
//                is Int -> setTitle(title)
//            }
//            val subtitle = it.get(EXTRA_SUBTITLE)
//            when (subtitle) {
//                is String -> setSubtitle(subtitle)
//                is Int -> setSubtitle(subtitle)
//            }
        }

        fileManager = SRManager.create()?.fileManager ?: run {
            // TODO Show error
            dismiss()
            return
        }

        rootPathLabel = requireContext().getString(R.string.mount_dirs_custom_dialog_root_path)
    }

    override fun listFiles(file: ServerFile): List<ServerFile> {
        return fileManager.list(file, 0)
                ?.sortedWith(Comparator { o1, o2 ->
                    if (o1.isDirectory && !o2.isDirectory)
                        -1
                    else if (!o1.isDirectory && o2.isDirectory)
                        1
                    else
                        o1.name!!.compareTo(o2.name!!)
                }) ?: ArrayList()
    }

    override fun isAllowedEnterSelectedDirectories(): Boolean {
        return false
    }

    override fun onCreateAdapterItem(fileItem: SelectableServerFile) {
        if (!fileItem.isDirectory) {
            fileItem.selectable = false
            return
        }

        val path = fileItem.relativePath.toLowerCase(Locale.ENGLISH)
        if (fileItem.isDirectory) {
            val stdFolderNameRes = when {
                path.equals(Environment.DIRECTORY_DCIM, true) -> R.string.std_folder_dcim
                path.equals(Environment.DIRECTORY_DOWNLOADS, true) -> R.string.std_folder_downloads
                path.equals(Environment.DIRECTORY_DOCUMENTS, true) -> R.string.std_folder_documents
                path.equals(Environment.DIRECTORY_PICTURES, true) -> R.string.std_folder_pictures
                path.equals(Environment.DIRECTORY_RINGTONES, true) -> R.string.std_folder_ringtones
                path.equals(Environment.DIRECTORY_NOTIFICATIONS, true) -> R.string.std_folder_notifications
                path.equals(Environment.DIRECTORY_ALARMS, true) -> R.string.std_folder_alarms
                path.equals(Environment.DIRECTORY_MUSIC, true) -> R.string.std_folder_music
                path.equals(Environment.DIRECTORY_MOVIES, true) -> R.string.std_folder_movies
                path.equals(Environment.DIRECTORY_PODCASTS, true) -> R.string.std_folder_podcasts
                else -> null
            }
            if (stdFolderNameRes != null) {
                fileItem.normalReason = getString(stdFolderNameRes)
            }

            val res = when {
                path.equals("Android/data", true) -> getString(R.string.dir_summary_android_data)
                path.equals("Android/media", true) -> getString(R.string.dir_summary_android_media)
                path.equals("Android/obb", true) -> getString(R.string.dir_summary_android_obb)
                else -> null
            }
            if (res != null) {
                fileItem.normalReason = res
                if (isApp)
                    fileItem.selectedPartially = true
            }
        }

        if (path.equals("Android", true)) {
            fileItem.selectable = false
            fileItem.selectedPartially = true
            fileItem.allowEnter = true
            fileItem.normalReason = getString(R.string.mount_dirs_template_android_disabled_reason)
        }

        if (isApp) {
            if (path.equals("Android/data/$packageName", true)
                    || path.equals("Android/media/$packageName", true)
                    || path.equals("Android/obb/$packageName", true)) {

                fileItem.normalReason = getString(
                        R.string.mount_dirs_template_android_data_disabled_reason
                )
                fileItem.selected = true
                fileItem.selectable = false
                fileItem.allowEnter = false
            }/* else if (BuildUtils.isQ() && fileItem.relativePath.startsWith("Android/sandbox/", true)) {
                fileItem.selected = true
                fileItem.selectable = false
                fileItem.allowEnter = false
            }*/
        }
    }

}