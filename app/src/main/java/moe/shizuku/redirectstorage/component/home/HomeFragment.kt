package moe.shizuku.redirectstorage.component.home

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.edit
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.MainActivity
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.component.applist.AppListViewModel
import moe.shizuku.redirectstorage.component.home.FailedFragment.Companion.isSupportedThrowable
import moe.shizuku.redirectstorage.component.home.FailedFragment.Companion.newInstance
import moe.shizuku.redirectstorage.component.home.adapter.HomeAdapter
import moe.shizuku.redirectstorage.dialog.ExceptionDialog
import moe.shizuku.redirectstorage.model.LatestVersionInfo
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.utils.UserHelper
import moe.shizuku.redirectstorage.viewmodel.Status
import moe.shizuku.redirectstorage.viewmodel.activitySharedViewModels
import moe.shizuku.redirectstorage.viewmodel.observeChanged
import moe.shizuku.redirectstorage.widget.VerticalPaddingDecoration
import rikka.core.ktx.unsafeLazy
import rikka.recyclerview.fixEdgeEffect

class HomeFragment : AppFragment() {

    companion object {
        private const val KEY_USER = "USER"

        fun newInstance(userId: Int): HomeFragment {
            val args = Bundle()
            args.putInt(KEY_USER, userId)
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val userId by unsafeLazy {
        if (arguments != null) {
            requireArguments().getInt(KEY_USER)
        } else {
            UserHelper.currentUserId()
        }
    }

    private val adapter: HomeAdapter by unsafeLazy { HomeAdapter() }

    private val homeModel: HomeViewModel by activitySharedViewModels { HomeViewModel() }

    private val appsModel: AppListViewModel by activitySharedViewModels { AppListViewModel() }

    private val deathListener = SRManager.DeathListener {
        refresh()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appsModel.userId = userId
        if (homeModel.status.value == null) {
            homeModel.loadData(userId)
            homeModel.loadLatestVersionInfo(requireContext())
        }
    }

    fun refresh() {
        val userId = if (Settings.isShowMultiUser) SRManager.USER_ALL else UserHelper.myUserId()
        if (userId != this.userId) {
            if (arguments != null) {
                requireArguments().putInt(KEY_USER, this.userId)
            }
            requireActivity().recreate()
            return
        }
        homeModel.loadData(this.userId)
    }

    private fun popFailedFragment() {
        if (!childFragmentManager.isStateSaved) {
            @Suppress("ControlFlowWithEmptyBody")
            while (childFragmentManager.backStackEntryCount > 0 && childFragmentManager.popBackStackImmediate());
        }
    }

    override fun onStart() {
        super.onStart()
        if (homeModel.status.value?.status == Status.SUCCESS) {
            homeModel.scheduleUpdate()
        }
    }

    override fun onResume() {
        super.onResume()
        if (homeModel.status.value?.status == Status.SUCCESS) {
            popFailedFragment()
        }
    }

    override fun onStop() {
        super.onStop()
        homeModel.unscheduleUpdate()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val context = view.context
        val recyclerView: RecyclerView = view.findViewById(android.R.id.list)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(VerticalPaddingDecoration(context))
        recyclerView.fixEdgeEffect()
        (requireActivity() as MainActivity).homeHeader.isRaised = recyclerView.computeVerticalScrollOffset() > 0
        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                (requireActivity() as MainActivity).homeHeader.isRaised = recyclerView.computeVerticalScrollOffset() > 0
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        homeModel.status.observe(viewLifecycleOwner) {
            when (it?.status) {
                Status.LOADING -> {
                    adapter.updateData(requireContext(), it)
                }
                Status.SUCCESS -> {
                    popFailedFragment()
                    adapter.updateData(requireContext(), it)
                    homeModel.scheduleUpdate()
                }
                Status.ERROR -> {
                    if (isSupportedThrowable(it.error)) {
                        ViewModelProvider(this).get(FailedInfoViewModel::class.java).throwable = it.error
                        childFragmentManager.beginTransaction()
                                .add(R.id.fragment_container, newInstance())
                                .addToBackStack(null)
                                .commit()
                    } else {
                        it.error.printStackTrace()
                        CrashReportHelper.logException(it.error)
                        if (!it.error.javaClass.name.contains("JobCancellationException")) {
                            ExceptionDialog.newInstance(it.error).show(parentFragmentManager)
                        }
                    }
                }
            }
        }

        homeModel.latestVersionInfo.observeChanged(this) {
            if (it?.status == Status.SUCCESS && it.data?.dialogs != null) {
                showDialogs(it.data.getDialogs())
            }
        }
        registerLocalBroadcastReceiver(IntentFilter(AppConstants.ACTION_PURCHASED_CHANGED)) { _: Context, _: Intent ->
            if (context == null) return@registerLocalBroadcastReceiver
            if (homeModel.status.value != null) {
                adapter.updateData(requireContext(), homeModel.status.value!!)
            }
        }
        registerLocalBroadcastReceiver(AppConstants.ACTION_REQUEST_REFRESH_HOME) { _: Context, _: Intent ->
            if (context == null) return@registerLocalBroadcastReceiver
            refresh()
        }
        registerLocalBroadcastReceiver(AppConstants.ACTION_REQUEST_CLEAR_APP_LIST) { context: Context, _: Intent ->
            appsModel.appList.postValue(null)
            appsModel.reload(context)
        }
        registerLocalBroadcastReceiver(AppConstants.ACTION_SERVER_STARTED) { _: Context, _: Intent ->
            if (context == null) return@registerLocalBroadcastReceiver
            refresh()
        }
        SRManager.addDeathListener(deathListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        SRManager.removeDeathListener(deathListener)
    }

    private fun showDialogs(dialogs: List<LatestVersionInfo.DialogInfo>) {
        val context = requireContext()

        for (info in dialogs) {
            if (info.id == null) {
                continue
            }
            if (info.ignoreable && Settings.preferences.getBoolean("dialog_ignored_${info.id}", false)) {
                continue
            }

            try {
                val builder = AlertDialog.Builder(context)
                if (info.title != null) {
                    builder.setTitle(info.title.get())
                }
                if (info.message != null) {
                    builder.setMessage(info.message.get())
                }
                info.positiveButton?.let {
                    val text = when (it.text.get()) {
                        "@ok" -> context.getString(android.R.string.ok)
                        "@cancel" -> context.getString(android.R.string.cancel)
                        else -> it.text.get()
                    }
                    builder.setPositiveButton(text) { _, _ ->
                        if (it.url != null) {
                            try {
                                context.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(it.url)))
                            } catch (e: Exception) {
                            }
                        }
                    }
                }
                info.negativeButton?.let {
                    val text = when (it.text.get()) {
                        "@ok" -> context.getString(android.R.string.ok)
                        "@cancel" -> context.getString(android.R.string.cancel)
                        else -> it.text.get()
                    }
                    builder.setNegativeButton(text) { _, _ ->
                        if (it.url != null) {
                            try {
                                context.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(it.url)))
                            } catch (e: Exception) {
                            }
                        }
                    }
                }
                if (!info.ignoreable) {
                    info.neutralButton?.let {
                        val text = when (it.text.get()) {
                            "@ok" -> context.getString(android.R.string.ok)
                            "@cancel" -> context.getString(android.R.string.cancel)
                            else -> it.text.get()
                        }
                        builder.setNeutralButton(text) { _, _ ->
                            if (it.url != null) {
                                try {
                                    context.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(it.url)))
                                } catch (e: Exception) {
                                }
                            }
                        }
                    }
                } else {
                    builder.setNeutralButton(R.string.dont_show_again) { _, _ ->
                        Settings.preferences.edit { putBoolean("dialog_ignored_${info.id}", true) }
                    }
                }

                val dialog = builder.create()
                dialog.setOnShowListener {
                    dialog.setCancelable(info.cancelable)
                    dialog.setCanceledOnTouchOutside(info.cancelable)

                    dialog.findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance()
                    dialog.findViewById<TextView>(android.R.id.message)?.setLineSpacing(context.resources.getDimension(R.dimen.dialog_line_space_extra), 1f)
                }
                dialog.show()
            } catch (tr: Throwable) {
            }
        }
    }
}