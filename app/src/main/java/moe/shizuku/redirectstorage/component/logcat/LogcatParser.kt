package moe.shizuku.redirectstorage.component.logcat

import moe.shizuku.redirectstorage.model.LogcatItem
import java.util.regex.Matcher
import java.util.regex.Pattern

object LogcatParser {

    private val PATTER = Pattern.compile(
            """
                ^(\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{3})
                (?:\s+[0-9A-Za-z]+)?\s+(\d+)\s+(\d+)\s+([A-Z])\s+
                (.+?)\s*: (.*)${'$'}
            """.trimIndent().replace("\n", ""))

    @JvmStatic
    fun parse(line: String): LogcatItem? {
        if ("" == line.trim()) {
            return null
        }
        val m: Matcher = PATTER.matcher(line)
        if (m.matches()) {
            val item = LogcatItem()
            item.date = m.group(1)
            item.pid = m.group(2)!!.toInt()
            item.tid = m.group(3)!!.toInt()
            item.level = m.group(4)
            item.tag = m.group(5)
            item.msg = m.group(6)
            return item
        }
        return null
    }
}