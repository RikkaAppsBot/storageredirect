package moe.shizuku.redirectstorage.component.starter;

public class StarterConstants {

    public static final int EXIT_STARTER_IS_RUNNING = -99;
    public static final int EXIT_FATAL_IO_EXCEPTION = -2;
    public static final int EXIT_FATAL_UNABLE_OPEN_ROOT_SHELL = -1;
    public static final int EXIT_SUCCESS = 0;

    // keep sync with exitcode.h
    public static final int EXIT_FATAL_CANNOT_ACCESS_DEX = 1;
    public static final int EXIT_FATAL_SET_CLASSPATH = 2;
    public static final int EXIT_FATAL_FORK = 3;
    public static final int EXIT_FATAL_APP_PROCESS = 4;
    public static final int EXIT_FATAL_WRITE_DEX = 5;
    public static final int EXIT_WARN_START_TIMEOUT = 6;
    public static final int EXIT_WARN_SERVER_STOP = 7;
    public static final int EXIT_FATAL_SELINUX = 8;
    public static final int EXIT_FATAL_NO_MNT_NS = 9;
    public static final int EXIT_FATAL_EXEC_DENIED = 10;
}
