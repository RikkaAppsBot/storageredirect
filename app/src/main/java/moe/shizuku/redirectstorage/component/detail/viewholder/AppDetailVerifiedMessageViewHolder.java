package moe.shizuku.redirectstorage.component.detail.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.AppConfiguration;
import rikka.recyclerview.BaseListenerViewHolder;
import rikka.recyclerview.BaseViewHolder;

public class AppDetailVerifiedMessageViewHolder extends BaseListenerViewHolder<AppConfiguration, AppDetailVerifiedMessageViewHolder.Listener> {

    public static final BaseViewHolder.Creator<AppConfiguration> CREATOR = (inflater, parent) ->
            new AppDetailVerifiedMessageViewHolder(inflater.inflate(R.layout.detail_verified_message, parent, false));

    public AppDetailVerifiedMessageViewHolder(View itemView) {
        super(itemView);

        itemView.findViewById(android.R.id.closeButton)
                .setOnClickListener(v -> getListener().onIgnoreVerifiedClick());
    }

    public interface Listener {

        void onIgnoreVerifiedClick();
    }
}
