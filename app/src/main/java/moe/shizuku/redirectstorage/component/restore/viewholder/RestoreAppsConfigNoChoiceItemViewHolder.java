package moe.shizuku.redirectstorage.component.restore.viewholder;

import android.view.View;

import moe.shizuku.redirectstorage.R;

public class RestoreAppsConfigNoChoiceItemViewHolder extends RestoreAppsConfigChoiceItemViewHolder {

    public static final Creator CREATOR = (inflater, parent) ->
            new RestoreAppsConfigNoChoiceItemViewHolder(inflater.inflate(R.layout.restore_single_choice_item, parent, false));

    public RestoreAppsConfigNoChoiceItemViewHolder(View itemView) {
        super(itemView);

        View content = itemView.findViewById(android.R.id.content);
        content.setOnClickListener(null);
        content.setClickable(false);
        content.setBackground(null);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mRadioBox.performClick();
    }

    @Override
    public void onBind() {
        mIcon.setImageResource(R.drawable.ic_apps_24dp);
        mTitle.setText(R.string.restore_type_apps_config_no_selected);
        mSummary.setVisibility(View.GONE);
        mDivider.setVisibility(View.GONE);

        itemView.setOnClickListener(this);
        itemView.findViewById(android.R.id.content).setOnClickListener(null);
        itemView.findViewById(android.R.id.content).setClickable(false);

        mRadioBox.setChecked(getData().isChecked());
    }
}
