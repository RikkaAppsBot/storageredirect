package moe.shizuku.redirectstorage.component.logcat.viewholder

import android.content.Intent
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.card.MaterialCardView
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.component.logcat.LogcatActivity
import moe.shizuku.redirectstorage.component.logcat.adapter.LogcatHistoryAdapter
import moe.shizuku.redirectstorage.viewholder.SimpleItemViewHolder
import rikka.recyclerview.BaseViewHolder.Creator

class LogcatStatusItemHolder private constructor(itemView: View) : SimpleItemViewHolder<Any?>(itemView) {

    companion object {

        val CREATOR = Creator<Any> { inflater: LayoutInflater, parent: ViewGroup? -> LogcatStatusItemHolder(inflater.inflate(R.layout.logcat_status_layout, parent, false)) }
    }

    private val card: MaterialCardView = itemView as MaterialCardView
    private var okColor = 0
    private var unknownColor = 0

    init {
        val a = itemView.context.obtainStyledAttributes(null, R.styleable.HomeStatusTheme, R.attr.homeStatusStyle, 0)
        okColor = a.getColor(R.styleable.HomeStatusTheme_okColor, 0)
        unknownColor = a.getColor(R.styleable.HomeStatusTheme_unknownColor, 0)
        a.recycle()
    }

    override fun onClick(view: View) {
        val intent = Intent(view.context, LogcatActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        view.context.startActivity(intent)
    }

    override fun getAdapter(): LogcatHistoryAdapter {
        return super.getAdapter() as LogcatHistoryAdapter
    }

    override fun onBind() {
        val color: Int
        if (adapter.isLogcatEnabled) {
            color = okColor
            title.setText(R.string.logcat_status_on)
            summary.setText(R.string.logcat_status_summary_on)
        } else {
            color = unknownColor
            title.setText(R.string.logcat_status_off)
            summary.setText(R.string.logcat_status_summary_off)
        }
        card.setCardBackgroundColor(color)
        val rippleColor = color and 0xffffff or 0x33000000
        card.rippleColor = ColorStateList.valueOf(rippleColor)
    }
}