package moe.shizuku.redirectstorage.component.accessiblefolders

import android.annotation.SuppressLint
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.AppConstants.*
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.RedirectPackageInfo
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.SimpleMountInfo
import moe.shizuku.redirectstorage.app.AppBarDialogFragment
import moe.shizuku.redirectstorage.app.SharedUserLabelCache
import moe.shizuku.redirectstorage.component.accessiblefolders.adapter.SimpleMountPathsAdapter
import moe.shizuku.redirectstorage.component.accessiblefolders.dialog.MountInfoDirPickerDialog
import moe.shizuku.redirectstorage.component.accessiblefolders.dialog.MountInfoPackageSelectorDialog
import moe.shizuku.redirectstorage.dialog.picker.MultiFilesPickerDialog
import moe.shizuku.redirectstorage.dialog.picker.PackagesSelectorDialog
import moe.shizuku.redirectstorage.model.*
import moe.shizuku.redirectstorage.widget.EditButton
import rikka.material.app.AppBar

class SimpleMountEditFragment : AppBarDialogFragment(), MultiFilesPickerDialog.Callback, PackagesSelectorDialog.Callback {

    companion object {

        private const val REASON_EDIT = 0
        private const val REASON_CREATE = 1
        private const val REASON_VIEW_ONLINE = 2

        private const val EXTRA_REASON = "$EXTRA_PREFIX.REASON"
        private const val EXTRA_APP_INFO = "$EXTRA_PREFIX.APP_INFO"
        private const val EXTRA_BUILDER = "$EXTRA_PREFIX.BUILDER"

        private const val TAG_PACKAGE_SELECTOR_SOURCE = "mount_info_source_package"
        private const val TAG_PACKAGE_SELECTOR_TARGET = "mount_info_target_package"

        @JvmStatic
        fun newCreateInstance(ai: AppInfo): SimpleMountEditFragment {
            return SimpleMountEditFragment().apply {
                arguments = bundleOf(
                        EXTRA_REASON to REASON_CREATE,
                        EXTRA_APP_INFO to ai
                )
            }
        }

        @JvmStatic
        fun newEditInstance(ai: AppInfo, data: SimpleMountInfo): SimpleMountEditFragment {
            return SimpleMountEditFragment().apply {
                arguments = bundleOf(
                        EXTRA_REASON to REASON_EDIT,
                        EXTRA_APP_INFO to ai,
                        EXTRA_OLD_DATA to data
                )
            }
        }

        @JvmStatic
        fun newViewOnlineInstance(ai: AppInfo, data: SimpleMountInfo): SimpleMountEditFragment {
            return SimpleMountEditFragment().apply {
                arguments = bundleOf(
                        EXTRA_REASON to REASON_VIEW_ONLINE,
                        EXTRA_APP_INFO to ai,
                        EXTRA_OLD_DATA to data
                )
            }
        }

    }

    interface Listener {

        fun onRequestAddSimpleMount(info: SimpleMountInfo)

        fun onRequestUpdateSimpleMount(new: SimpleMountInfo, old: SimpleMountInfo)

        fun onRequestDeleteSimpleMount(info: SimpleMountInfo)
    }

    private var reason: Int = -1
    private lateinit var appInfo: AppInfo
    private var oldData: SimpleMountInfo? = null
    private lateinit var builder: SimpleMountInfoBuilder

    private lateinit var sourcePackageEdit: EditButton
    private lateinit var targetPackageEdit: EditButton
    private lateinit var pathsList: RecyclerView
    private lateinit var pathsStatus: TextView

    private lateinit var pathsAdapter: SimpleMountPathsAdapter

    private var sourceLabel: CharSequence = ""
    private var targetLabel: CharSequence = ""
    private var installed: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasDialogOptionsMenu(true)

        requireArguments().let {
            appInfo = it.getParcelable(EXTRA_APP_INFO)!!
            reason = it.getInt(EXTRA_REASON, -1)
            oldData = it.getParcelable(EXTRA_OLD_DATA)

            builder = if (savedInstanceState != null) {
                savedInstanceState.getParcelable(EXTRA_BUILDER)!!
            } else {
                when (reason) {
                    REASON_CREATE -> SimpleMountInfoBuilder().apply {
                        target(appInfo.packageName)
                        user(appInfo.userId)
                    }
                    REASON_EDIT -> oldData!!.newBuilder()
                    REASON_VIEW_ONLINE -> oldData!!.newBuilder()
                    else -> throw IllegalArgumentException("Unsupported reason: $reason")
                }
            }
        }
    }

    override fun onPackageSelectorDialogResult(tag: String?, list: List<SimpleAppInfo>) {
        if (tag == TAG_PACKAGE_SELECTOR_SOURCE) {
            if (builder.sourceName != list.first().name) {
                builder.source(list.first().name)
                builder.paths.clear()
                pathsAdapter.updateData(builder.paths)
                updateViewsState()
            }
        } else if (tag == TAG_PACKAGE_SELECTOR_TARGET) {
            if (builder.targetName != list.first().name) {
                builder.target(list.first().name)
                updateViewsState()
            }
        }
    }

    override fun onMultiFilesPickerDialogResult(tag: String?, list: List<ServerFile>) {
        builder.paths = list.map { item -> item.relativePath }.toMutableList()
        pathsAdapter.updateData(builder.paths)
        updateViewsState()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(EXTRA_BUILDER, builder)
    }

    override fun onAppBarCreated(appBar: AppBar, savedInstanceState: Bundle?) {
        appBar.setDisplayHomeAsUpEnabled(true)
        appBar.setHomeAsUpIndicator(R.drawable.ic_close_24dp)
        when (reason) {
            REASON_CREATE -> {
                appBar.setTitle(R.string.mount_info_edit_dialog_title_for_create)
            }
            REASON_EDIT -> {
                appBar.setTitle(R.string.mount_info_edit_dialog_title_for_edit)
            }
            REASON_VIEW_ONLINE -> {
                appBar.setTitle(R.string.mount_info_edit_dialog_title_for_view_online)
            }
        }
    }

    override fun onCreateContentView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_mount_info_edit_fragment_layout, container, false)
    }

    override fun onContentViewCreated(view: View, savedInstanceState: Bundle?) {
        sourcePackageEdit = view.findViewById(R.id.source_package_edit)
        targetPackageEdit = view.findViewById(R.id.target_package_edit)
        pathsList = view.findViewById(R.id.paths_list)
        pathsStatus = view.findViewById(R.id.paths_status)

        pathsAdapter = SimpleMountPathsAdapter(reason != REASON_VIEW_ONLINE)
        pathsAdapter.listener = object : SimpleMountPathsAdapter.Listener {

            override fun onDeletePathClick(path: String) {
                builder.paths.remove(path)
                pathsAdapter.updateData(builder.paths)
                updateViewsState()
            }
        }
        pathsAdapter.updateData(builder.paths)
        pathsList.adapter = pathsAdapter
        pathsList.isNestedScrollingEnabled = false

        view.findViewById<View>(R.id.paths_status_layout).isGone = reason == REASON_VIEW_ONLINE

        sourcePackageEdit.setOnClickListener {
            val fragment = MountInfoPackageSelectorDialog.newInstance(
                    appInfo.packageName,
                    builder.sourceName,
                    appInfo.userId
            )
            fragment.show(childFragmentManager, TAG_PACKAGE_SELECTOR_SOURCE)
        }
        targetPackageEdit.setOnClickListener {
            val fragment = MountInfoPackageSelectorDialog.newInstance(
                    appInfo.packageName,
                    builder.targetName,
                    appInfo.userId
            )
            fragment.show(childFragmentManager, TAG_PACKAGE_SELECTOR_TARGET)
        }

        updateViewsState()

        view.findViewById<View>(R.id.paths_add_button).setOnClickListener {
            if (TextUtils.isEmpty(builder.sourceName)) {
                Toast.makeText(view.context, R.string.toast_mount_info_edit_dialog_choose_source_first, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            MountInfoDirPickerDialog.newInstance(
                    builder.sourceName!!, builder.targetName!!, sourceLabel.toString(), targetLabel.toString(), appInfo.userId, builder.paths
            ).show(childFragmentManager, "mount_info_dir_picker")
        }
    }

    override fun onResume() {
        super.onResume()

        updatePackageState()
    }

    @SuppressLint("WrongConstant")
    private fun loadLabel(name: String, pm: PackageManager): Pair<CharSequence, Boolean> {
        var installed = true
        var label: CharSequence
        if (name.startsWith(RedirectPackageInfo.SHARED_USER_PREFIX)) {
            val sharedUser = RedirectPackageInfo.getRawSharedUserId(name)
            try {
                val sm = SRManager.createThrow()
                val uid = sm.getUidForSharedUser(sharedUser, appInfo.userId)
                val packages = sm.getPackagesForUid(uid)

                packages?.forEach {
                    try {
                        val pi = pm.getPackageInfo(it, 0x00002000/*PackageManager.MATCH_UNINSTALLED_PACKAGES*/)
                        if (pi.sharedUserId != null) {
                            SharedUserLabelCache.load(pm, pi.sharedUserLabel, pi.sharedUserId, pi.packageName, pi.applicationInfo)
                        }
                    } catch (e: Throwable) {
                    }
                }
            } catch (e: Throwable) {
                installed = false
            }
            label = requireContext().getString(R.string.item_summary, SharedUserLabelCache.get(sharedUser), requireContext().getString(R.string.shared_user_id))
        } else {
            try {
                val pi: PackageInfo? = SRManager.createThrow().getPackageInfo(name, 0, appInfo.userId)
                if (pi?.sharedUserId != null) {
                    return loadLabel(RedirectPackageInfo.SHARED_USER_PREFIX + pi.sharedUserId, pm)
                }
                label = pi!!.applicationInfo!!.loadLabel(pm)
            } catch (e: Throwable) {
                label = name
                installed = false
            }
        }
        return Pair(label, installed)
    }


    private fun updatePackageState() {
        val pm = requireContext().packageManager

        var installed = true
        if (builder.sourceName != null) {
            val res = loadLabel(builder.sourceName!!, pm)
            sourceLabel = res.first
            sourcePackageEdit.setContentText(res.first)
            installed = installed and res.second
        } else {
            sourcePackageEdit.setContentText(null)
            installed = false
        }

        if (builder.targetName != null) {
            val res = loadLabel(builder.targetName!!, pm)
            targetLabel = res.first
            targetPackageEdit.setContentText(res.first)
            installed = installed and res.second
        } else {
            targetPackageEdit.setContentText(null)
            installed = false
        }

        sourcePackageEdit.isEnabled = reason != REASON_VIEW_ONLINE && builder.sourceName != appInfo.packageName
        targetPackageEdit.isEnabled = reason != REASON_VIEW_ONLINE && builder.targetName != appInfo.packageName

        if (this.installed == null || this.installed != installed) {
            this.installed = installed
            requestPrepareDialogOptionsMenu()
        }
    }

    private fun updateViewsState() {
        updatePackageState()

        val size = builder.paths.size
        pathsStatus.text = resources.getQuantityString(R.plurals.select_mode_format, size, size)
    }

    override fun onBackPressed(): Boolean {
        if (reason != REASON_VIEW_ONLINE
                && builder.sourceName != null
                && builder.targetName != null
                && oldData?.equalsContent(builder.build()) != true) {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.exit_confirm_dialog_title)
                    .setMessage(R.string.exit_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        dismiss()
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .setNeutralButton(R.string.exit_confirm_dialog_button_save) { _, _ ->
                        onSaveClick()
                    }
                    .show()
            return false
        }
        return super.onBackPressed()
    }

    override fun onCreateDialogOptionsMenu(menu: Menu, inflater: MenuInflater) {
        when (reason) {
            REASON_CREATE -> {
                inflater.inflate(R.menu.dialog_save, menu)
            }
            REASON_EDIT -> {
                inflater.inflate(R.menu.dialog_delete, menu)
                inflater.inflate(R.menu.dialog_save, menu)
            }
            REASON_VIEW_ONLINE -> {
                inflater.inflate(R.menu.dialog_add, menu)
            }
        }
    }

    override fun onPrepareDialogOptionsMenu(menu: Menu) {
        if (reason == REASON_VIEW_ONLINE) {
            menu.findItem(R.id.action_add).isEnabled = installed == true
        } else {
            menu.findItem(R.id.action_save).isEnabled = installed == true
        }
    }

    override fun onDialogOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_save) {
            onSaveClick()
        } else if (item.itemId == R.id.action_delete) {
            onDeleteClick()
        }
        return super.onDialogOptionsItemSelected(item)
    }

    private fun onSaveClick() {
        if (builder.sourceName == null) {
            Toast.makeText(
                    context,
                    R.string.toast_mount_info_check_source_package,
                    Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (builder.targetName == null) {
            Toast.makeText(
                    context,
                    R.string.toast_mount_info_check_target_package,
                    Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (builder.paths.isEmpty()) {
            Toast.makeText(
                    context,
                    R.string.toast_mount_info_check_paths,
                    Toast.LENGTH_SHORT
            ).show()
            return
        }
        if (installed == null || installed == false)
            return

        if (parentFragment is Listener) {
            if (oldData == null) {
                (parentFragment as Listener).onRequestAddSimpleMount(builder.build())
            } else {
                val new = builder.build()
                new.id = oldData!!.id
                new.enabled = oldData!!.enabled
                (parentFragment as Listener).onRequestUpdateSimpleMount(new, oldData!!)
            }
        }

        dismiss()
    }

    private fun onDeleteClick() {
        if (oldData != null && reason == REASON_EDIT) {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.delete_confirm_dialog_title)
                    .setMessage(R.string.delete_confirm_dialog_message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        if (parentFragment is Listener) {
                            (parentFragment as Listener).onRequestDeleteSimpleMount(oldData!!)
                        }
                        dismiss()
                    }
                    .setNegativeButton(android.R.string.no, null)
                    .show()
        } else {
            Log.w(TAG, "Delete operation is only supported in edit mode.",
                    UnsupportedOperationException())
            dismiss()
        }
    }
}