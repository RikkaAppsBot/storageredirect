package moe.shizuku.redirectstorage.component.filetransfer.adapter;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.component.filetransfer.viewholder.DirSelectItemViewHolder;
import moe.shizuku.redirectstorage.component.filetransfer.viewholder.LoadingItemViewHolder;
import moe.shizuku.redirectstorage.utils.SizeTagHandler;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder;
import rikka.core.compat.CollectionsCompat;
import rikka.html.text.HtmlCompat;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

public class FileTransferAdapter extends IdBasedRecyclerViewAdapter {

    private static final int ID_DESCRIPTION = 1;
    private static final int ID_DESC_DIVIDER = 2;
    private static final int ID_LOADING = 3;
    private static final int ID_OBSERVER_ITEMS = 100;

    private final Context mContext;

    private List<ObserverInfoItem> mData;

    public FileTransferAdapter(Context context) {
        super();

        mContext = context;

        setHasStableIds(true);

        updateData();
    }

    public void setData(List<ObserverInfoItem> data) {
        mData = data;
        updateData();
    }

    public void updateData() {
        clear();

        addItem(SettingsDescriptionViewHolder.CREATOR, HtmlCompat.fromHtml(mContext.getString(R.string.file_transfer_description), null, SizeTagHandler.getInstance()), ID_DESCRIPTION);
        addItem(DividerViewHolder.CREATOR, null, ID_DESC_DIVIDER);

        if (mData == null) {
            addItem(LoadingItemViewHolder.CREATOR, null, ID_LOADING);
        } else {
            for (ObserverInfoItem item : mData) {
                addItem(DirSelectItemViewHolder.CREATOR, item, ID_OBSERVER_ITEMS + item.hashCode());
            }
        }

        notifyDataSetChanged();
    }

    public static class ObserverInfoItem implements Parcelable {

        public final ObserverInfo observer;
        public final List<File> files;
        public boolean checked = false;

        public ObserverInfoItem(@NonNull ObserverInfo observer, @NonNull List<File> files) {
            this.observer = Objects.requireNonNull(observer);
            this.files = Objects.requireNonNull(files);
        }

        private ObserverInfoItem(Parcel in) {
            this.observer = Objects.requireNonNull(in.readParcelable(ObserverInfo.class.getClassLoader()));
            this.files = CollectionsCompat.mapToList(Objects.requireNonNull(in.createStringArrayList()), File::new);
            this.checked = in.readByte() != 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(observer, flags);
            dest.writeStringList(CollectionsCompat.mapToList(files, File::getAbsolutePath));
            dest.writeByte(checked ? (byte) 1 : (byte) 0);
        }

        public static final Creator<ObserverInfoItem> CREATOR = new Creator<ObserverInfoItem>() {
            @Override
            public ObserverInfoItem createFromParcel(Parcel source) {
                return new ObserverInfoItem(source);
            }

            @Override
            public ObserverInfoItem[] newArray(int size) {
                return new ObserverInfoItem[size];
            }
        };

    }

}
