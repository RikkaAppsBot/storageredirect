package moe.shizuku.redirectstorage.component.mountdirstemplate.adapter;

import java.util.List;

import moe.shizuku.redirectstorage.component.mountdirstemplate.viewholder.TemplateContentItem;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;

public class MountDirsTemplateContentAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool().putRule(String.class, TemplateContentItem.CREATOR);
    }

    public void updateData(List<String> data) {
        getItems().clear();
        getItems().addAll(data);
        notifyDataSetChanged();
    }
}
