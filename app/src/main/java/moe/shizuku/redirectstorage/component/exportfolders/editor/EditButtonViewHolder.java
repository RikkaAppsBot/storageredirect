package moe.shizuku.redirectstorage.component.exportfolders.editor;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.CallSuper;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.widget.EditButton;
import rikka.recyclerview.BaseListenerViewHolder;

public abstract class EditButtonViewHolder<T, L> extends BaseListenerViewHolder<T, L> {

    protected ImageView icon;
    protected EditButton editButton;

    public EditButtonViewHolder(View itemView) {
        super(itemView);
        icon = itemView.findViewById(android.R.id.icon);
        editButton = itemView.findViewById(android.R.id.edit);

        editButton.setOnClickListener(v -> onClick());
    }

    public abstract Drawable getIconDrawable();

    public abstract CharSequence getTitle();

    public CharSequence getHint() {
        return "";
    }

    public Drawable getEditIconDrawable() {
        return itemView.getResources().getDrawable(R.drawable.ic_edit_24dp, itemView.getContext().getTheme());
    }

    public abstract void onClick();

    @CallSuper
    @Override
    public void onBind() {
        icon.setImageDrawable(getIconDrawable());
        editButton.setTitle(getTitle());
        editButton.setContentHint(getHint());
        editButton.setIconDrawable(getEditIconDrawable());
    }
}
