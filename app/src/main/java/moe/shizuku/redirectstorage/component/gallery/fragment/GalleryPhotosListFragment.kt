package moe.shizuku.redirectstorage.component.gallery.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.AppConstants.ACTION_PREFIX
import moe.shizuku.redirectstorage.AppConstants.EXTRA_DATA
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.app.AppFragment
import moe.shizuku.redirectstorage.component.gallery.adapter.GalleryPhotosListAdapter
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.event.ActionBroadcastReceiver
import rikka.core.res.resolveColor
import rikka.recyclerview.addFastScroller

class GalleryPhotosListFragment : AppFragment() {

    companion object {

        const val ACTION_ON_FOLDER_CHANGE = "$ACTION_PREFIX.ON_FOLDER_CHANGE"

        @JvmStatic
        fun newInstance(data: GalleryFolderSimpleItem): GalleryPhotosListFragment {
            return GalleryPhotosListFragment().apply {
                arguments = bundleOf(EXTRA_DATA to data)
            }
        }

        @JvmStatic
        fun notifyFolderChanged(newItem: GalleryFolderSimpleItem): Intent {
            return Intent(ACTION_ON_FOLDER_CHANGE).putExtra(EXTRA_DATA, newItem)
        }

    }

    private lateinit var mData: GalleryFolderSimpleItem

    private lateinit var mRecyclerView: RecyclerView

    private lateinit var mAdapter: GalleryPhotosListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mData = arguments?.getParcelable(EXTRA_DATA)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_list_fastscroll, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.setBackgroundColor(view.context.theme.resolveColor(android.R.attr.windowBackground))

        mRecyclerView = view.findViewById(android.R.id.list)
        mRecyclerView.layoutManager = GridLayoutManager(context, 3)
        mRecyclerView.addFastScroller()
        mAdapter = GalleryPhotosListAdapter(mRecyclerView)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)

        registerLocalBroadcastReceiver(ActionBroadcastReceiver
                .createReceiver(ACTION_ON_FOLDER_CHANGE) { _, intent ->
                    onFolderChanged(intent.getParcelableExtra(EXTRA_DATA)!!)
                })

        refreshListData()
    }

    private fun onFolderChanged(newItem: GalleryFolderSimpleItem) {
        arguments = bundleOf(EXTRA_DATA to newItem)
        mData = newItem
        refreshListData()
    }

    private fun refreshListData() {
        mAdapter.setItems(mData.getPreviewModels())
        mAdapter.notifyDataSetChanged()
    }

}