package moe.shizuku.redirectstorage.component.gallery.adapter

import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.component.gallery.model.PreviewModel
import moe.shizuku.redirectstorage.component.gallery.viewholder.PhotoPreviewGridViewHolder
import moe.shizuku.redirectstorage.component.gallery.viewholder.TimelineSubtitleViewHolder
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.ClassCreatorPool
import java.util.*

class GalleryTimelineListAdapter(recyclerView: RecyclerView)
    : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    init {
        creatorPool.apply {
            putRule(Date::class.java, TimelineSubtitleViewHolder.CREATOR)
            putRule(PreviewModel::class.java,
                    PhotoPreviewGridViewHolder.newCreator(recyclerView))
        }
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool()
    }

}