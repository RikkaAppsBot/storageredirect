package moe.shizuku.redirectstorage.component.gallery.viewholder

import android.view.View
import android.widget.TextView
import moe.shizuku.redirectstorage.R
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.BaseViewHolder.Creator
import java.text.DateFormat
import java.util.*

class TimelineSubtitleViewHolder(itemView: View) : BaseViewHolder<Date>(itemView) {

    companion object {

        @JvmStatic
        val CREATOR: Creator<Date> = Creator<Date> { inflater, parent ->
            TimelineSubtitleViewHolder(inflater.inflate(R.layout.gallery_timeline_date_subtitle, parent, false))
        }

    }

    val title: TextView = itemView.findViewById(android.R.id.text1)

    override fun onBind() {
        title.text = DateFormat.getDateInstance().format(data)
    }

}