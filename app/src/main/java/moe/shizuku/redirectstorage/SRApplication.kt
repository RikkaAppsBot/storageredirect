package moe.shizuku.redirectstorage

import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.os.ConfigurationCompat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.shizuku.redirectstorage.api.GitHubRulesApiService
import moe.shizuku.redirectstorage.api.HelpImpl
import moe.shizuku.redirectstorage.api.RikkaAppApiService
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings
import moe.shizuku.redirectstorage.app.Reflection
import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.dao.SRDatabase
import moe.shizuku.redirectstorage.ktx.DispatchersNoThrow
import moe.shizuku.redirectstorage.ktx.createDeviceProtectedStorageContextCompat
import moe.shizuku.redirectstorage.ktx.isSdcardfsUsed
import moe.shizuku.redirectstorage.ktx.processNameCompat
import moe.shizuku.redirectstorage.license.AlipayHelper
import moe.shizuku.redirectstorage.license.GoogleBillingHelper
import moe.shizuku.redirectstorage.license.License
import moe.shizuku.redirectstorage.license.RedeemHelper
import moe.shizuku.redirectstorage.model.AppRecommendationTexts
import moe.shizuku.redirectstorage.model.AppRecommendationTexts.PresetOrCustomText
import moe.shizuku.redirectstorage.model.AppRecommendationTexts.PresetOrCustomTextList
import moe.shizuku.redirectstorage.model.AppRecommendationTexts.PresetText
import moe.shizuku.redirectstorage.utils.CrashReportHelper
import moe.shizuku.redirectstorage.utils.NetworkCheckInterceptor
import moe.shizuku.redirectstorage.utils.OfflineCacheInterceptor
import moe.shizuku.redirectstorage.utils.ServerBootHelper
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rikka.core.util.BuildUtils
import rikka.core.util.ResourceUtils
import rikka.html.text.HtmlCompat
import rikka.internal.help.HelpEntity
import rikka.internal.help.HelpProvider
import rikka.internal.payment.PaymentRetrofitSingleton
import rikka.internal.payment.api.PaymentService
import rikka.material.app.DayNightDelegate
import rikka.material.app.LocaleDelegate
import rikka.sui.Sui
import java.io.File
import java.util.*

private var _suiAvailable: Boolean = false

val suiAvailable get() = _suiAvailable

class SRApplication : Application() {

    companion object {

        private const val API_BASE = "https://api.rikka.app/"
        private const val API_SECURE_BASE = "https://api-secure.rikka.app/"

        val GSON: Gson = GsonBuilder()
                .registerTypeAdapter(AppRecommendationTexts.Color::class.java, AppRecommendationTexts.Color.GSON_ADAPTER)
                .registerTypeAdapter(PresetText::class.java, PresetText.GSON_ADAPTER)
                .registerTypeAdapter(PresetOrCustomText::class.java, PresetOrCustomText.GSON_ADAPTER)
                .registerTypeAdapter(PresetOrCustomTextList::class.java, PresetOrCustomTextList.GSON_ADAPTER)
                .create()

        init {
            _suiAvailable = Sui.init(BuildConfig.APPLICATION_ID)

            HelpProvider.setImpl(HelpImpl)
            HelpEntity.setLaunchFailedHandler { context: Context, entity: HelpEntity ->
                val url: String = entity.content.get()
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                try {
                    context.startActivity(intent)
                } catch (tr: Throwable) {
                    try {
                        context.getSystemService(ClipboardManager::class.java)!!.setPrimaryClip(ClipData(ClipData.newPlainText("label", url)))
                        AlertDialog.Builder(context)
                                .setTitle(R.string.dialog_cannot_open_browser_title)
                                .setMessage(HtmlCompat.fromHtml(context.getString(R.string.toast_copied_to_clipboard_with_text, url)))
                                .setPositiveButton(android.R.string.ok, null)
                                .show()
                    } catch (ignored: Throwable) {
                    }
                }
            }
            ResourceUtils.setPackageName(BuildConfig.APPLICATION_ID)
            RxJavaPlugins.setErrorHandler { throwable: Throwable ->
                if (BuildConfig.DEBUG) {
                    val currentThread = Thread.currentThread()
                    currentThread.uncaughtExceptionHandler?.uncaughtException(currentThread, throwable)
                } else {
                    throwable.printStackTrace()
                    CrashReportHelper.logException(throwable)
                }
            }
            SRManager.setLogBinderCalls(BuildConfig.DEBUG_LOG_BINDER_CALLS)

            SRManager.addReceivedListener {
                License.resetNativeLicense()

                // call twice to ensure instance id is generated and saved by native
                DeviceSpecificSettings.getInstanceId()
                DeviceSpecificSettings.getInstanceId()

                try {
                    isSdcardfsUsed = SRManager.create()?.isSdcardfsUsed ?: false
                } catch (e: Exception) {
                }
            }
        }
    }

    val database by lazy {
        SRDatabase.newBuilder(this)
                .allowMainThreadQueries()
                .build()
    }

    val gitHubRulesApiService: GitHubRulesApiService by lazy {
        val context = createDeviceProtectedStorageContextCompat()
        val cache = Cache(File(context.cacheDir, "raw_githubusercontent"), 1024 * 1024 * 100)
        val client = OkHttpClient.Builder()
                .cache(cache)
                .addNetworkInterceptor(OfflineCacheInterceptor(this@SRApplication))
                .build()
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .baseUrl("https://raw.githubusercontent.com/")
                .client(client)
                .build()
        retrofit.create(GitHubRulesApiService::class.java)
    }

    val rikkaAppApiService: RikkaAppApiService by lazy {
        val context = createDeviceProtectedStorageContextCompat()
        val pi: PackageInfo
        pi = try {
            context.packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            throw RuntimeException(e)
        }
        val versionCode: String
        versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            pi.longVersionCode.toString()
        } else {
            pi.versionCode.toString()
        }
        val versionName = pi.versionName
        val cache = Cache(File(context.cacheDir, "api"), 1024 * 1024 * 100)
        val builder = OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(OfflineCacheInterceptor(this@SRApplication))
                .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                    val rb = chain.request().newBuilder()
                    rb.header("Package-Name", packageName)
                    rb.header("Version-Name", versionName)
                    rb.header("Version-Code", versionCode)
                    rb.header("Api-Version", "2")
                    chain.proceed(rb.build())
                })
                .addInterceptor(NetworkCheckInterceptor())
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.HEADERS
            builder.addNetworkInterceptor(logging)
        }
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .baseUrl(API_BASE)
                .client(builder.build())
                .build()
        retrofit.create(RikkaAppApiService::class.java)
    }

    val paymentService: PaymentService by lazy {
        val retrofit = object : PaymentRetrofitSingleton() {
            override fun getContext(): Context {
                return this@SRApplication
            }

            override fun getInstanceId(): String {
                return DeviceSpecificSettings.getInstanceId()
            }

            override fun getBaseUrl(): String {
                return API_BASE
            }

            override fun getBaseUrlSecure(): String {
                return API_SECURE_BASE
            }

            override fun getCertificate(): ByteArray {
                return byteArrayOf(45, 45, 45, 45, 45, 66, 69, 71, 73, 78, 32, 67, 69, 82, 84, 73, 70, 73, 67, 65, 84, 69, 45, 45, 45, 45, 45, 10, 77, 73, 73, 68, 83, 84, 67, 67, 65, 106, 69, 67, 70, 69, 122, 72, 119, 72, 53, 102, 77, 52, 73, 65, 103, 102, 108, 122, 86, 114, 73, 70, 76, 71, 73, 103, 81, 118, 89, 110, 77, 65, 48, 71, 67, 83, 113, 71, 83, 73, 98, 51, 68, 81, 69, 66, 67, 119, 85, 65, 77, 71, 69, 120, 10, 67, 122, 65, 74, 66, 103, 78, 86, 66, 65, 89, 84, 65, 107, 70, 86, 77, 82, 77, 119, 69, 81, 89, 68, 86, 81, 81, 73, 68, 65, 112, 84, 98, 50, 49, 108, 76, 86, 78, 48, 89, 88, 82, 108, 77, 81, 52, 119, 68, 65, 89, 68, 86, 81, 81, 75, 68, 65, 86, 83, 97, 87, 116, 114, 10, 89, 84, 69, 79, 77, 65, 119, 71, 65, 49, 85, 69, 67, 119, 119, 70, 85, 109, 108, 114, 97, 50, 69, 120, 72, 84, 65, 98, 66, 103, 78, 86, 66, 65, 77, 77, 70, 71, 70, 119, 97, 83, 49, 122, 90, 87, 78, 49, 99, 109, 85, 117, 99, 109, 108, 114, 97, 50, 69, 117, 89, 88, 66, 119, 10, 77, 66, 52, 88, 68, 84, 69, 53, 77, 68, 103, 120, 77, 68, 69, 122, 78, 68, 85, 119, 77, 70, 111, 88, 68, 84, 73, 53, 77, 68, 103, 119, 78, 122, 69, 122, 78, 68, 85, 119, 77, 70, 111, 119, 89, 84, 69, 76, 77, 65, 107, 71, 65, 49, 85, 69, 66, 104, 77, 67, 81, 86, 85, 120, 10, 69, 122, 65, 82, 66, 103, 78, 86, 66, 65, 103, 77, 67, 108, 78, 118, 98, 87, 85, 116, 85, 51, 82, 104, 100, 71, 85, 120, 68, 106, 65, 77, 66, 103, 78, 86, 66, 65, 111, 77, 66, 86, 74, 112, 97, 50, 116, 104, 77, 81, 52, 119, 68, 65, 89, 68, 86, 81, 81, 76, 68, 65, 86, 83, 10, 97, 87, 116, 114, 89, 84, 69, 100, 77, 66, 115, 71, 65, 49, 85, 69, 65, 119, 119, 85, 89, 88, 66, 112, 76, 88, 78, 108, 89, 51, 86, 121, 90, 83, 53, 121, 97, 87, 116, 114, 89, 83, 53, 104, 99, 72, 65, 119, 103, 103, 69, 105, 77, 65, 48, 71, 67, 83, 113, 71, 83, 73, 98, 51, 10, 68, 81, 69, 66, 65, 81, 85, 65, 65, 52, 73, 66, 68, 119, 65, 119, 103, 103, 69, 75, 65, 111, 73, 66, 65, 81, 67, 121, 54, 98, 83, 99, 112, 105, 106, 100, 70, 85, 74, 43, 47, 48, 49, 101, 119, 53, 101, 49, 47, 122, 72, 65, 47, 119, 50, 104, 72, 85, 84, 49, 110, 113, 82, 68, 10, 113, 101, 51, 86, 69, 81, 106, 52, 114, 79, 57, 118, 56, 70, 82, 72, 52, 47, 75, 65, 108, 50, 97, 107, 107, 114, 107, 82, 55, 97, 56, 115, 65, 77, 75, 112, 81, 50, 65, 89, 73, 82, 118, 101, 111, 72, 72, 81, 108, 117, 116, 102, 116, 118, 99, 79, 73, 83, 72, 48, 70, 99, 84, 72, 10, 84, 67, 100, 77, 115, 49, 104, 111, 110, 85, 114, 105, 47, 116, 67, 116, 75, 80, 57, 71, 73, 119, 55, 47, 102, 106, 109, 73, 72, 97, 67, 82, 89, 112, 66, 65, 107, 104, 103, 76, 76, 105, 78, 112, 83, 87, 118, 88, 97, 114, 48, 77, 54, 85, 101, 120, 70, 76, 108, 103, 55, 50, 101, 75, 10, 74, 87, 49, 111, 90, 82, 122, 117, 49, 107, 51, 49, 88, 110, 49, 83, 88, 85, 57, 119, 71, 118, 70, 88, 70, 67, 65, 87, 82, 49, 82, 66, 111, 99, 85, 79, 103, 52, 80, 53, 85, 54, 50, 53, 105, 80, 89, 103, 84, 81, 43, 110, 90, 99, 117, 103, 106, 114, 48, 115, 70, 56, 84, 65, 10, 77, 69, 90, 121, 83, 48, 68, 86, 82, 55, 47, 84, 82, 56, 56, 117, 71, 54, 114, 110, 113, 43, 89, 109, 57, 118, 115, 51, 78, 122, 122, 70, 103, 104, 122, 87, 75, 105, 70, 57, 83, 82, 52, 87, 99, 43, 101, 107, 77, 97, 55, 86, 67, 118, 109, 47, 83, 73, 86, 84, 86, 67, 72, 108, 10, 116, 111, 80, 81, 48, 80, 102, 48, 57, 48, 72, 47, 111, 56, 119, 90, 72, 97, 100, 86, 74, 100, 113, 86, 68, 111, 74, 102, 80, 49, 48, 80, 73, 75, 114, 115, 65, 102, 54, 98, 82, 72, 53, 69, 118, 55, 68, 84, 65, 103, 77, 66, 65, 65, 69, 119, 68, 81, 89, 74, 75, 111, 90, 73, 10, 104, 118, 99, 78, 65, 81, 69, 76, 66, 81, 65, 68, 103, 103, 69, 66, 65, 75, 107, 80, 43, 115, 99, 115, 120, 105, 109, 106, 47, 116, 98, 104, 81, 97, 77, 80, 84, 85, 80, 50, 114, 55, 79, 50, 104, 120, 70, 48, 112, 57, 43, 90, 114, 69, 76, 98, 121, 80, 112, 73, 74, 103, 116, 111, 10, 78, 109, 56, 75, 97, 71, 102, 121, 89, 109, 84, 50, 105, 115, 112, 52, 71, 98, 82, 120, 117, 103, 101, 78, 120, 71, 114, 70, 119, 90, 79, 121, 121, 98, 90, 109, 113, 83, 89, 75, 67, 109, 87, 83, 66, 98, 70, 74, 68, 110, 51, 86, 69, 111, 57, 83, 51, 99, 105, 82, 75, 112, 72, 106, 10, 98, 83, 53, 122, 107, 106, 54, 115, 108, 86, 82, 56, 66, 79, 67, 121, 47, 119, 121, 122, 119, 75, 116, 102, 103, 109, 88, 98, 113, 56, 101, 98, 49, 122, 107, 49, 99, 102, 111, 109, 55, 109, 55, 83, 79, 87, 82, 113, 48, 76, 72, 49, 70, 109, 116, 118, 106, 47, 98, 120, 113, 122, 119, 71, 10, 57, 74, 68, 120, 49, 85, 88, 75, 69, 51, 112, 77, 50, 106, 113, 121, 102, 122, 89, 108, 50, 108, 87, 106, 106, 66, 88, 102, 79, 113, 55, 90, 89, 70, 76, 105, 112, 54, 108, 75, 47, 80, 99, 116, 99, 82, 43, 70, 83, 102, 114, 119, 100, 56, 100, 75, 74, 57, 106, 70, 107, 88, 97, 105, 10, 77, 88, 99, 74, 106, 89, 109, 53, 118, 111, 90, 112, 86, 68, 118, 56, 78, 55, 69, 70, 54, 80, 113, 55, 77, 54, 79, 100, 49, 65, 81, 114, 69, 73, 105, 102, 103, 83, 98, 117, 101, 118, 70, 51, 77, 74, 43, 47, 71, 71, 86, 66, 106, 85, 97, 98, 121, 76, 99, 48, 48, 50, 101, 75, 10, 112, 70, 66, 81, 116, 114, 100, 75, 112, 118, 104, 43, 122, 99, 105, 84, 120, 112, 114, 83, 52, 109, 80, 80, 118, 101, 113, 111, 89, 66, 47, 57, 119, 74, 99, 120, 43, 90, 56, 61, 10, 45, 45, 45, 45, 45, 69, 78, 68, 32, 67, 69, 82, 84, 73, 70, 73, 67, 65, 84, 69, 45, 45, 45, 45, 45)
            }

            override fun onCreateOkHttpClient(builder: OkHttpClient.Builder) {
                builder.addInterceptor(NetworkCheckInterceptor())
            }
        }
        retrofit.get().create(PaymentService::class.java)
    }

    override fun onCreate() {
        super.onCreate()

        ApplicationContext.set(this)
        CrashReportHelper.init(this)

        val asyncMainThreadScheduler = AndroidSchedulers.from(Looper.getMainLooper(), true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { asyncMainThreadScheduler }
        RxAndroidPlugins.setMainThreadSchedulerHandler { asyncMainThreadScheduler }

        HtmlCompat.setContext(this)
        LocaleDelegate.defaultLocale = Settings.locale
        DayNightDelegate.setApplicationContext(this)
        DayNightDelegate.setDefaultNightMode(Settings.nightMode)
        if (BuildConfig.DEBUG) {
            ServerBootHelper.copyFiles(this)
        }
        if (DeviceSpecificSettings.firstInstallTime == 0L) {
            DeviceSpecificSettings.firstInstallTime = System.currentTimeMillis()
        }
        if (!processNameCompat.contains(":")) {
            if (BuildConfig.DEBUG || GoogleBillingHelper.isInstallerGooglePlay(this)) {
                GoogleBillingHelper.check(this, false)
            }
            if (GoogleBillingHelper.checkLocal()) {
                GoogleBillingHelper.checkToken(this)
            }
            RedeemHelper.check(this)
            AlipayHelper.check(this)

            GlobalScope.launch(DispatchersNoThrow.IO) {
                AppRecommendationTexts.loadAsync(this@SRApplication)
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val newLocale = ConfigurationCompat.getLocales(newConfig)[0]
        if (LocaleDelegate.systemLocale != newLocale) {
            Log.i(Constants.TAG, "System locale changed.")
            Locale.setDefault(Settings.locale)
            LocaleDelegate.systemLocale = newLocale
            LocaleDelegate.defaultLocale = Settings.locale
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        if (!BuildUtils.atLeast30) {
            Reflection.exemptAll()
        }
        System.loadLibrary("sr")
        attachBaseContext()
        DeviceSpecificSettings.initialize(this)
        Settings.initialize(this)
    }

    @Suppress("KotlinJniMissingFunction")
    private external fun attachBaseContext()
}

object ApplicationContext {

    private var context: Context? = null

    fun get(): Context {
        return context!!
    }

    fun set(context: Context) {
        this.context = context
    }
}