package moe.shizuku.redirectstorage.preference

import moe.shizuku.preference.PreferenceDataStore
import java.util.*

class MemoryPreferenceDataStore : PreferenceDataStore() {
    private val store: MutableMap<String, Any?> = HashMap()
    override fun putString(key: String, value: String?) {
        store[key] = value
    }

    override fun putStringSet(key: String, values: Set<String>?) {
        store[key] = values
    }

    override fun putInt(key: String, value: Int) {
        store[key] = value
    }

    override fun putLong(key: String, value: Long) {
        store[key] = value
    }

    override fun putFloat(key: String, value: Float) {
        store[key] = value
    }

    override fun putBoolean(key: String, value: Boolean) {
        store[key] = value
    }

    override fun getString(key: String, defValue: String?): String? {
        return store[key] as? String ?: defValue
    }

    override fun getStringSet(key: String, defValues: Set<String>?): Set<String>? {
        @Suppress("UNCHECKED_CAST")
        return store[key] as? Set<String> ?: defValues
    }

    override fun getInt(key: String, defValue: Int): Int {
        return store[key] as? Int ?: defValue
    }

    override fun getLong(key: String, defValue: Long): Long {
        return store[key] as? Long ?: defValue
    }

    override fun getFloat(key: String, defValue: Float): Float {
        return store[key] as? Float ?: defValue
    }

    override fun getBoolean(key: String, defValue: Boolean): Boolean {
        return store[key] as? Boolean ?: defValue
    }
}