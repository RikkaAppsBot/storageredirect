package moe.shizuku.redirectstorage.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import moe.shizuku.preference.PreferenceViewHolder;
import moe.shizuku.preference.SwitchPreference;

public class DisabledSwitchPreference extends SwitchPreference {

    public DisabledSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public DisabledSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DisabledSwitchPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DisabledSwitchPreference(Context context) {
        super(context);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        View switchView = holder.findViewById(moe.shizuku.preference.R.id.switchWidget);
        switchView.setEnabled(false);
    }
}
