package moe.shizuku.redirectstorage.ktx

import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.os.Build
import android.os.UserManager
import android.widget.Toast
import androidx.annotation.StringRes
import moe.shizuku.redirectstorage.SRApplication
import moe.shizuku.redirectstorage.compat.DummySharedPreferencesImpl

val Context.application: SRApplication
    get() {
        return if (this is SRApplication) this else applicationContext as SRApplication
    }

fun Context.createDeviceProtectedStorageContextCompat(): Context {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        createDeviceProtectedStorageContext()
    } else {
        this
    }
}

fun Context.createDeviceProtectedStorageContextCompatWhenLocked(): Context {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && getSystemService(UserManager::class.java)?.isUserUnlocked != true) {
        createDeviceProtectedStorageContext()
    } else {
        this
    }
}

fun Context.makeToast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(this, text, duration)
}

fun Context.makeToast(@StringRes textRes: Int, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(this, textRes, duration)
}

fun Context.createPreferenceContext(): Context {
    var storageContext = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        this.createDeviceProtectedStorageContext()
    } else {
        this
    }
    storageContext = object : ContextWrapper(storageContext) {
        override fun getSharedPreferences(name: String, mode: Int): SharedPreferences {
            return try {
                super.getSharedPreferences(name, mode)
            } catch (e: IllegalStateException) {
                DummySharedPreferencesImpl()
            }
        }
    }
    return storageContext
}