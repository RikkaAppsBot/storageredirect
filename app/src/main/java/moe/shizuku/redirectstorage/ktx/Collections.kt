package moe.shizuku.redirectstorage.ktx

/**
 * Returns a [java.util.ArrayList] filled with all elements of this collection.
 */
fun <T> Collection<T>.toArrayList(): MutableList<T> {
    if (this is ArrayList) {
        return this
    }
    return java.util.ArrayList(this)
}

fun <T> MutableList<T>.applyAll(operator: (s: T) -> T) {
    val li = this.listIterator()
    while (li.hasNext()) {
        li.set(operator.invoke(li.next()))
    }
}