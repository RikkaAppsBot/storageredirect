package moe.shizuku.redirectstorage.ktx

import android.util.Log
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers

object DispatchersNoThrow {

    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val cause = if (throwable.cause != throwable && throwable.cause != null) throwable.cause else throwable
        Log.e(Thread.currentThread().name, Log.getStackTraceString(throwable))
    }

    val Main get() = Dispatchers.Main + exceptionHandler
    val IO get() = Dispatchers.IO + exceptionHandler
}