package moe.shizuku.redirectstorage.ktx

import android.animation.Animator
import android.animation.TimeAnimator
import android.annotation.SuppressLint
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

@SuppressLint("NewApi")
fun Animation.toAnimator(target: View): Animator {
    val transformation = Transformation()
    val animation = this
    animation.initialize(target.measuredWidth, target.measuredHeight, target.measuredWidth, target.measuredHeight)
    val duration = animation.duration
    animation.startTime = System.currentTimeMillis()

    val animator = TimeAnimator()
    animator.duration = duration
    animator.setTarget(target)
    animator.setTimeListener { _, totalTime, _ ->
        if (totalTime > duration) {
            animator.cancel()
        }
        animation.getTransformation(System.currentTimeMillis(), transformation)
        target.alpha = transformation.alpha
        target.animationMatrix = transformation.matrix
        if (transformation.hasClipRect) {
            target.clipBounds = transformation.clipRect
        }
    }
    return animator
}