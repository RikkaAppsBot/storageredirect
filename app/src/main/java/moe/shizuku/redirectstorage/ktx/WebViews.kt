package moe.shizuku.redirectstorage.ktx

import me.zhanghai.android.fastscroll.FastScrollWebView
import me.zhanghai.android.fastscroll.FastScrollerBuilder

fun FastScrollWebView.addFastScroller() {
    val builder = FastScrollerBuilder(this).useMd2Style()
    builder.build()
}