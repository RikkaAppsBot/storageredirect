package moe.shizuku.redirectstorage.ktx

import android.content.pm.PackageInfo
import moe.shizuku.redirectstorage.utils.BuildUtils

val PackageInfo.longVersionCodeCompat: Long
    get() {
        return if (BuildUtils.atLeast28())
            longVersionCode
        else
            versionCode.toLong()
    }