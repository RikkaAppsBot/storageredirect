package moe.shizuku.redirectstorage.ktx

import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.R
import rikka.core.res.resolveDrawable
import rikka.html.text.toHtml
import rikka.recyclerview.fixEdgeEffect
import rikka.widget.borderview.BorderRecyclerView
import kotlin.math.roundToInt

@Suppress("UNCHECKED_CAST")
fun AlertDialog.addOnDismissListener(listener: (DialogInterface) -> Unit) {
    synchronized(this) {
        val view = findViewById<View>(R.id.parentPanel)!!
        var listeners = view.getTag(R.id.tag_ondismisslistener) as MutableList<(DialogInterface) -> Unit>?
        if (listeners == null) {
            listeners = ArrayList()
            view.setTag(R.id.tag_ondismisslistener, listeners)

            setOnDismissListener {
                for (l in listeners) {
                    l.invoke(this)
                }
            }
        }
        listeners.add(listener)
    }
}

fun AlertDialog.applyCountdown(whichButton: Int, countdownSecond: Int, text: CharSequence) {
    val countdownRunnable = object : Runnable {
        override fun run() {
            val button = getButton(whichButton)
            var countdown = button.getTag(R.id.tag_countdown) as Int
            countdown -= 1
            button.setTag(R.id.tag_countdown, countdown)
            if (countdown == 0) {
                button.isEnabled = true
                button.text = text
            } else {
                button.text = context.getString(R.string.item_summary, text, countdown.toString()).toHtml()
                button.postDelayed(this, 1000)
            }
        }
    }

    val button = getButton(whichButton)
    button.isEnabled = false
    button.postDelayed(countdownRunnable, 1000)
    button.setTag(R.id.tag_countdown, countdownSecond)

    addOnDismissListener {
        getButton(whichButton).removeCallbacks(countdownRunnable)
    }
}

private typealias OnClickListener = (index: Int) -> Unit

fun AlertDialog.Builder.setSingleChoiceItems(items: List<CharSequence>, checkedItem: Int, listener: OnClickListener): AlertDialogChoiceAdapter {
    val a = context.obtainStyledAttributes(null, R.styleable.AlertDialog, R.attr.alertDialogStyle, 0)
    val singleChoiceItemLayout = a.getResourceId(R.styleable.AlertDialog_singleChoiceItemLayout, 0)
    a.recycle()

    val backgroundDrawable = context.theme.resolveDrawable(android.R.attr.selectableItemBackground)
    val adapter = SingleChoiceAdapter(items.toMutableList(), checkedItem, listener, singleChoiceItemLayout, backgroundDrawable)

    val view = setChoiceView()
    view.adapter = adapter

    return adapter
}

private fun AlertDialog.Builder.setChoiceView(): BorderRecyclerView {
    val dividerDrawable = context.applicationContext.theme.resolveDrawable(android.R.attr.dividerVertical)

    val view = BorderRecyclerView(context)
    view.fixEdgeEffect()
    view.borderTopDrawable = dividerDrawable
    view.borderBottomDrawable = dividerDrawable
    view.layoutManager = LinearLayoutManager(context)

    val margin = (context.resources.displayMetrics.density * 8).roundToInt()
    setView(view, 0, margin, 0, 0)

    return view
}

abstract class AlertDialogChoiceAdapter internal constructor(
        private val items: MutableList<CharSequence>,
        private val listener: OnClickListener,
        private val layout: Int,
        private val backgroundDrawable: Drawable?) : RecyclerView.Adapter<ChoiceViewHolder>() {

    open fun setAll(newItems: List<CharSequence>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChoiceViewHolder {
        return ChoiceViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false)) { index ->
            onClick(index, items[index])
            listener.invoke(index)
        }.apply {
            itemView.foreground = backgroundDrawable?.mutate()?.constantState?.newDrawable()
        }
    }

    @CallSuper
    override fun onBindViewHolder(holder: ChoiceViewHolder, position: Int) {
        holder.text.text = items[position]
        holder.text.isChecked = isItemChecked(position)
    }

    override fun onBindViewHolder(holder: ChoiceViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            for (payload in payloads) {
                if (payload is Boolean) {
                    holder.text.isChecked = payload
                }
            }
        }
    }

    abstract fun isItemChecked(index: Int): Boolean

    abstract fun onClick(index: Int, value: CharSequence)
}

private class SingleChoiceAdapter(items: MutableList<CharSequence>,
                                  private var choiceIndex: Int,
                                  listener: OnClickListener,
                                  layout: Int,
                                  backgroundDrawable: Drawable?) : AlertDialogChoiceAdapter(items, listener, layout, backgroundDrawable) {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun setAll(newItems: List<CharSequence>) {
        super.setAll(newItems)
    }

    override fun onClick(index: Int, value: CharSequence) {
        if (index != choiceIndex) {
            notifyItemChanged(index, true)
            notifyItemChanged(choiceIndex, false)
            choiceIndex = index
        }
    }

    override fun isItemChecked(index: Int): Boolean {
        return index == choiceIndex
    }
}

class ChoiceViewHolder(itemView: View, private val listener: OnClickListener) : RecyclerView.ViewHolder(itemView) {

    val text: CheckedTextView = itemView.findViewById(android.R.id.text1)

    init {
        itemView.setOnClickListener {
            listener.invoke(adapterPosition)
        }
    }
}

