package moe.shizuku.redirectstorage.ktx

import android.database.Cursor

fun Cursor.getColumnIndexOrNull(columnName: String): Int? {
    val index = getColumnIndex(columnName)
    return if (index < 0) null else index
}