package moe.shizuku.redirectstorage.ktx

import moe.shizuku.redirectstorage.utils.BuildUtils
import rikka.core.ktx.unsafeLazy

val isFuseUsed by unsafeLazy {
    BuildUtils.isFuse()
}

var isSdcardfsUsed = false