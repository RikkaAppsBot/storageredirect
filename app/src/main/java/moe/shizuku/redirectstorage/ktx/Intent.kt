package moe.shizuku.redirectstorage.ktx

import android.content.Intent
import android.os.Parcelable
import moe.shizuku.redirectstorage.AppConstants
import moe.shizuku.redirectstorage.utils.IntentCompat
import moe.shizuku.redirectstorage.utils.UserHelper

fun Intent.getPackageNameExtra(): String? {
    return getStringExtra(AppConstants.EXTRA_PACKAGE_NAME) ?: getStringExtra(IntentCompat.EXTRA_PACKAGE_NAME)
}

fun Intent.getUserIdExtra(): Int {
    return getParcelableExtra<Parcelable>(Intent.EXTRA_USER)?.hashCode()
            ?: getIntExtra(AppConstants.EXTRA_USER_ID, UserHelper.myUserId())
}

fun Intent.putPackageExtrasFrom(source: Intent) {
    putExtra(AppConstants.EXTRA_PACKAGE_NAME, source.getPackageNameExtra())
    putExtra(AppConstants.EXTRA_USER_ID, source.getUserIdExtra())
}