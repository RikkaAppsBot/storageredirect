@file:JvmName("ParcelsExtensions")

package moe.shizuku.redirectstorage.ktx

import android.os.Parcel
import android.os.Parcelable

fun Parcel.readBoolean(): Boolean {
    return readByte() != 0.toByte()
}

fun Parcel.writeBoolean(boolean: Boolean) {
    writeByte(if (boolean) 1 else 0)
}

inline fun <reified T : Parcelable> Parcel.readParcelable(): T? {
    return readParcelable<T?>(T::class.java.classLoader)
}