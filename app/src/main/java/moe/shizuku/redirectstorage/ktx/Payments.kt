package moe.shizuku.redirectstorage.ktx

import moe.shizuku.redirectstorage.license.License

inline fun whenPaid(crossinline lambda: () -> Unit): Boolean {
    if (License.checkLocal()) {
        lambda.invoke()
        return true
    }
    return false
}

inline fun whenPaid(crossinline paid: () -> Unit, crossinline notPaid: () -> Unit) {
    if (License.checkLocal()) {
        paid.invoke()
    } else {
        notPaid.invoke()
    }
}

inline fun whenNotPaid(crossinline lambda: () -> Unit) {
    if (!License.checkLocal()) {
        lambda.invoke()
    }
}