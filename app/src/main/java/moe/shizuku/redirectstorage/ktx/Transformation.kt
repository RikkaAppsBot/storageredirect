package moe.shizuku.redirectstorage.ktx

import android.graphics.Rect
import android.view.animation.Transformation

private val getClipRectMethod by lazy {
    Transformation::class.java.getDeclaredMethod("getClipRect")
}

private val hasClipRectMethod by lazy {
    Transformation::class.java.getDeclaredMethod("hasClipRect")
}

val Transformation.clipRect: Rect? get() {
    return try {
        getClipRectMethod.invoke(this) as Rect
    } catch (e: Throwable) {
        null
    }
}

val Transformation.hasClipRect: Boolean get() {
    return try {
        return hasClipRectMethod.invoke(this) as Boolean
    } catch (e: Throwable) {
        false
    }
}