@file:Suppress("NOTHING_TO_INLINE")

package moe.shizuku.redirectstorage.ktx

inline operator fun Runnable.invoke() = run()