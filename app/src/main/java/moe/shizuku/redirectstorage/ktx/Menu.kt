package moe.shizuku.redirectstorage.ktx

import android.view.Menu
import android.view.MenuItem

fun Menu.setItemsVisible(exception: MenuItem, visible: Boolean) {
    for (i in 0 until size()) {
        val item: MenuItem = getItem(i)
        if (item !== exception) {
            item.isVisible = visible
        }
    }
}