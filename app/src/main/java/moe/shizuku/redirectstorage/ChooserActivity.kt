package moe.shizuku.redirectstorage

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import rikka.material.chooser.ChooserActivity
import rikka.material.chooser.ChooserFragment

object ChooserActivity {

    fun startNoThrow(context: Context, title: String, intent: Intent) {
        try {
            val c = ChooserFragment.newIntent(ComponentName(context, ChooserActivity::class.java.name), title, intent)
            context.startActivity(c)
        } catch (ignored: Throwable) {
        }
    }

    fun startWithStreamNoThrow(context: Context, title: String, intent: Intent, uri: Uri, modeFlags: Int = Intent.FLAG_GRANT_READ_URI_PERMISSION) {
        try {
            val packageManager = context.packageManager
            val activities = packageManager.queryIntentActivities(intent, 0)
            for (ri in activities) {
                val packageName = ri.activityInfo.packageName
                context.grantUriPermission(packageName, uri, modeFlags)
            }
        } catch (ignored: Throwable) {
            return
        }

        startNoThrow(context, title, intent)
    }

    @JvmStatic
    fun startSendMail(context: Context, address: String, subject: String?, body: String?, attachment: Uri?) {
        val resolveIntent = Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", address, null))

        val targetIntent = Intent(Intent.ACTION_SEND,
                Uri.fromParts("mailto", address, null))
                .setType("vnd.android.cursor.dir/email")
                .putExtra(Intent.EXTRA_EMAIL, arrayOf(address))

        if (!TextUtils.isEmpty(body)) {
            targetIntent.putExtra(Intent.EXTRA_TEXT, body)
        }

        if (!TextUtils.isEmpty(subject)) {
            targetIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        }

        val excludeComponentNames = arrayListOf<ComponentName>()
        if (attachment != null) {
            val packageManager = context.packageManager
            val activities = packageManager.queryIntentActivities(resolveIntent, 0)
            for (ri in activities) {
                if (ri.activityInfo.packageName[0] == 'c' &&
                        ri.activityInfo.packageName[1] == 'o' &&
                        ri.activityInfo.packageName[2] == 'm' &&
                        ri.activityInfo.packageName[3] == '.' &&
                        ri.activityInfo.packageName[4] == 'p' &&
                        ri.activityInfo.packageName[5] == 'a' &&
                        ri.activityInfo.packageName[6] == 'y' &&
                        ri.activityInfo.packageName[7] == 'p' &&
                        ri.activityInfo.packageName[8] == 'a' &&
                        ri.activityInfo.packageName[9] == 'l' &&
                        ri.activityInfo.packageName[10] == '.') {
                    excludeComponentNames.add(ComponentName.createRelative(ri.activityInfo.packageName, ri.activityInfo.name))
                    continue
                }
                val packageName = ri.activityInfo.packageName
                context.grantUriPermission(packageName, attachment, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }

            targetIntent.putExtra(Intent.EXTRA_STREAM, attachment)
        }

        context.startActivity(ChooserFragment.newIntent(
                ComponentName(context, ChooserActivity::class.java.name),
                context.getString(R.string.action_send), targetIntent, resolveIntent, excludeComponentNames))
    }
}
