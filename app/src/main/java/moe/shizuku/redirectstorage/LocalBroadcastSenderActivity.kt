package moe.shizuku.redirectstorage

import android.content.Intent
import android.os.Bundle

import androidx.localbroadcastmanager.content.LocalBroadcastManager
import moe.shizuku.redirectstorage.app.AppActivity

class LocalBroadcastSenderActivity : AppActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (actualReferrer != packageName) {
            throw IllegalArgumentException("Only Storage Isolation client can call this activity.")
        }

        val uri = intent.data!!
        if ("storage-redirect-client" != uri.scheme && "broadcast" != uri.host) {
            throw IllegalArgumentException("Unsupported uri: $uri")
        }

        val action = uri.lastPathSegment ?: throw NullPointerException("Action cannot be null.")

        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(action))

        finish()
    }

}
