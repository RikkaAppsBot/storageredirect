package moe.shizuku.redirectstorage.adapter

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import kotlinx.coroutines.Job
import moe.shizuku.redirectstorage.AppConstants.PAYLOAD
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.model.IPackageDescriptor
import moe.shizuku.redirectstorage.model.SelectableSimpleAppInfo
import moe.shizuku.redirectstorage.model.SimpleAppInfo
import moe.shizuku.redirectstorage.utils.AppIconCache
import moe.shizuku.redirectstorage.utils.FilterHelper
import moe.shizuku.redirectstorage.utils.UserHelper
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.BaseViewHolder
import rikka.recyclerview.ClassCreatorPool
import java.util.*
import kotlin.collections.ArrayList

class PackagesSelectorItemAdapter(
        private val maxSelectedCount: Int = 0
) : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    companion object {

        @JvmStatic
        private val STATE_PACKAGES = PackagesSelectorItemAdapter::class.java.name +
                ".state.PACKAGES"

    }

    private var allData: MutableList<SelectableSimpleAppInfo> = mutableListOf()
    private var keyword: String? = null

    private val mFilterHelper: FilterHelper<SelectableSimpleAppInfo> = FilterHelper(
            object : FilterHelper.FilterItemsProvider<SelectableSimpleAppInfo> {
                override fun onFilterStart(): MutableList<SelectableSimpleAppInfo> {
                    return allData
                }

                override fun onFilterFinish(items: MutableList<SelectableSimpleAppInfo>) {
                    setDisplayData(items)
                }
            },
            object : FilterHelper.Filter<SelectableSimpleAppInfo>() {
                override fun contains(keyword: String?, item: SelectableSimpleAppInfo): Boolean {
                    return keyword == null ||
                            TextUtils.isEmpty(keyword) ||
                            keyword.toLowerCase(Locale.ENGLISH) in item.packageName.toLowerCase(Locale.ENGLISH) ||
                            keyword.toLowerCase(Locale.ENGLISH) in item.appInfo.label.toString().toLowerCase(Locale.ENGLISH)
                }
            }
    )

    init {
        mFilterHelper.setSearching(true)

        creatorPool.putRule(SelectableSimpleAppInfo::class.java) { inflater, parent ->
            ItemViewHolder(inflater.inflate(if (maxSelectedCount != 1) {
                R.layout.dialog_packages_selector_multi_choice_item
            } else {
                R.layout.dialog_packages_selector_single_choice_item
            }, parent, false))
        }
    }

    @JvmOverloads
    fun setAllData(items: List<SelectableSimpleAppInfo>, notify: Boolean = true) {
        allData = items.toMutableList()
        if (notify) {
            startFilter()
        }
    }

    private fun isSameItem(a: IPackageDescriptor, b: IPackageDescriptor): Boolean {
        return a.userId == b.userId && (a.packageName == b.packageName || (!a.sharedUserId.isNullOrBlank() && a.sharedUserId == b.sharedUserId))
    }

    fun <T : IPackageDescriptor> setCheckedPackages(items: List<T>) {
        allData.forEach {
            it.selected = items.any { checkedItem ->
                isSameItem(checkedItem, it)
            }
        }
        notifyDataSetChanged()
    }

    private fun setDisplayData(items: List<SelectableSimpleAppInfo>) {
        setItems(items)
        notifyDataSetChanged()
    }

    fun startFilter() {
        mFilterHelper.filter()
    }

    fun getOriginalItems(): List<SimpleAppInfo> {
        return allData.map { it.appInfo }
    }

    fun getCheckedItems(): List<SimpleAppInfo> {
        return allData.filter { it.selected }.map { it.appInfo }
    }

    fun selectAll() {
        if (maxSelectedCount > 1) {
            allData.filter { it.selectable }.forEach { it.selected = true }
            notifyItemRangeChanged(0, itemCount, PAYLOAD)
        }
    }

    fun setKeyword(keyword: String?) {
        this.keyword = keyword
        mFilterHelper.setKeyword(keyword)
    }

    fun getKeyword(): String? {
        return keyword
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            allData = it.getParcelableArrayList(STATE_PACKAGES)
                    ?: allData
            setItems(allData)
            notifyDataSetChanged()
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(STATE_PACKAGES, ArrayList(allData))
    }

    private fun onCheck(checkedItem: SelectableSimpleAppInfo, isChecked: Boolean) {
        if (!checkedItem.selectable) return

        val items = getItems<SelectableSimpleAppInfo>()

        for (item in allData) {
            if (!item.selectable) continue
            var newSelected: Boolean? = null
            if (isSameItem(checkedItem, item)) {
                newSelected = isChecked
            } else if (maxSelectedCount == 1 && !isSameItem(checkedItem, item)) {
                newSelected = false
            }
            if (newSelected != null && newSelected != item.selected) {
                item.selected = newSelected

                val index = items.indexOf(item)
                if (index != -1) {
                    notifyItemChanged(index, PAYLOAD)
                }
            }
        }
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool()
    }

    private inner class ItemViewHolder(itemView: View)
        : BaseViewHolder<SelectableSimpleAppInfo>(itemView) {

        val button: CompoundButton = itemView.findViewById(android.R.id.checkbox)
        val icon: ImageView = itemView.findViewById(android.R.id.icon)
        val title: TextView = itemView.findViewById(android.R.id.title)
        val summary: TextView = itemView.findViewById(android.R.id.summary)

        private var loadIconJob: Job? = null

        init {
            itemView.setOnClickListener {
                val newChecked = if (maxSelectedCount == 1) true else !button.isChecked
                onCheck(data, newChecked)
            }
        }

        override fun onBind() {
            loadIconJob = AppIconCache.loadIconBitmapAsync(
                    context, data.appInfo.applicationInfo!!, data.userId, icon)
            if (UserHelper.myUserId() != data.appInfo.userId) {
                title.text = context.getString(R.string.app_name_with_user, data.appInfo.label, data.appInfo.userId)
            } else {
                title.text = data.appInfo.label
            }
            if (!data.summary.isNullOrEmpty()) {
                summary.visibility = View.VISIBLE
                summary.text = data.summary
            } else {
                summary.visibility = View.GONE
            }

            updateCheckState()
        }

        override fun onBind(payloads: MutableList<Any>) {
            updateCheckState()
        }

        override fun onRecycle() {
            if (loadIconJob?.isActive == true) {
                loadIconJob?.cancel()
            }
        }

        private fun updateCheckState() {
            button.isChecked = data.selected
            button.isEnabled = data.selectable
        }

    }

}