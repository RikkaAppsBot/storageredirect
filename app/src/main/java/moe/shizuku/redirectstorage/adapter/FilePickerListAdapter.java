package moe.shizuku.redirectstorage.adapter;

import androidx.annotation.Nullable;

import java.util.List;

import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.viewholder.FilePickerItemViewHolder;
import rikka.recyclerview.BaseViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

/**
 * Created by fytho on 2017/12/12.
 */

public class FilePickerListAdapter
        extends IdBasedRecyclerViewAdapter
        implements FilePickerItemViewHolder.OnItemClickListener {

    private static final int ID_DIRECTORIES = 1;

    private Callback mCallback;

    private final BaseViewHolder.Creator<ServerFile> ITEM_CREATOR =
            FilePickerItemViewHolder.newCompactCreator(this);

    public FilePickerListAdapter() {
        super();
    }

    public void setFilePickerCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onItemClick(@Nullable String name, boolean isFile) {
        if (mCallback != null) {
            mCallback.onItemClick(name, isFile);
        }
    }

    public void updateItems(boolean isRoot, @Nullable List<ServerFile> fileItems) {
        clear();
        if (!isRoot) {
            addItem(ITEM_CREATOR, ServerFile.BACK_ITEM, ID_DIRECTORIES);
        }
        if (fileItems != null) {
            for (ServerFile item : fileItems) {
                addItem(ITEM_CREATOR, item, ID_DIRECTORIES);
            }
        }
        notifyDataSetChanged();
    }

    public interface Callback {

        void onItemClick(@Nullable String name, boolean isFile);

    }

}