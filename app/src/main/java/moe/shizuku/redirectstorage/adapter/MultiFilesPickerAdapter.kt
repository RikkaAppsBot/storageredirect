package moe.shizuku.redirectstorage.adapter

import androidx.recyclerview.widget.DiffUtil
import moe.shizuku.redirectstorage.model.SelectableServerFile
import moe.shizuku.redirectstorage.viewholder.MultiPickerItemViewHolder
import rikka.recyclerview.BaseRecyclerViewAdapter
import rikka.recyclerview.ClassCreatorPool

class MultiFilesPickerAdapter : BaseRecyclerViewAdapter<ClassCreatorPool>() {

    interface Listener : MultiPickerItemViewHolder.Listener

    init {
        setItems(emptyList<Any>())
    }

    fun updateDataWithDiffUtil(newData: List<SelectableServerFile>) {
        val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(getItems(), newData))
        setItems(newData)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateCreatorPool(): ClassCreatorPool {
        return ClassCreatorPool().apply {
            putRule(SelectableServerFile::class.java, MultiPickerItemViewHolder.CREATOR)
        }
    }

    private class DiffUtilCallback(
            private val oldList: List<Any>,
            private val newList: List<Any>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            if (oldItem is SelectableServerFile && newItem is SelectableServerFile) {
                return oldItem.contentEquals(newItem)
            }
            return areItemsTheSame(oldItemPosition, newItemPosition)
        }

    }

}