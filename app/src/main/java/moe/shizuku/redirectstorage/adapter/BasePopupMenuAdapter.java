package moe.shizuku.redirectstorage.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.R;

public class BasePopupMenuAdapter extends BaseAdapter {

    private static int measureContentWidth(Context context, ListAdapter listAdapter) {
        ViewGroup mMeasureParent = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;

        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = listAdapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = listAdapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(context);
            }

            itemView = listAdapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();

            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    protected final List<Item> items;

    public BasePopupMenuAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Nullable
    public Item findItem(int itemId) {
        for (Item item : items) {
            if (item.itemId == itemId) {
                return item;
            }
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).itemId;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type;
    }

    public int getContentWidth(Context context) {
        return measureContentWidth(context, this);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != Item.TYPE_SUBTITLE;
    }

    @Override
    public void notifyDataSetChanged() {
        updateItemsChecked();
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            switch (getItemViewType(position)) {
                case Item.TYPE_SUBTITLE: {
                    convertView = inflater.inflate(R.layout.menu_item_subtitle, parent, false);
                    break;
                }
                case Item.TYPE_NORMAL: {
                    convertView = inflater.inflate(R.layout.menu_item_layout, parent, false);
                    break;
                }
                case Item.TYPE_CHECKABLE: {
                    convertView = inflater.inflate(R.layout.menu_item_layout, parent, false);
                    ((LinearLayout) convertView).addView(inflater.inflate(R.layout.menu_item_checkbox, parent, false));
                    break;
                }
                case Item.TYPE_CHECKABLE_SINGLE: {
                    convertView = inflater.inflate(R.layout.menu_item_layout, parent, false);
                    ((LinearLayout) convertView).addView(inflater.inflate(R.layout.menu_item_radio, parent, false));
                    break;
                }
                default:
                    throw new IllegalArgumentException("No views for this item type.");
            }
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Item item = getItem(position);
        holder.titleView.setText(item.titleRes);
        if (holder.checkable != null) {
            holder.checkable.setChecked(item.checked);
        }
        return convertView;
    }

    class ViewHolder {

        @NonNull
        View rootView;
        @NonNull
        TextView titleView;
        @Nullable
        Checkable checkable;

        ViewHolder(@NonNull View rootView) {
            this.rootView = rootView;
            this.titleView = rootView.findViewById(android.R.id.text1);
            this.checkable = rootView.findViewById(android.R.id.checkbox);
        }

    }

    public void updateItemsChecked() {
    }

    public static class Item {

        public int itemId, groupId;

        /**
         * Item type
         * 0 - Menu subtitle
         * 1 - Normal item
         * 2 - Multi-checkable item
         * 3 - Single checkable item (Need set group id)
         */
        public @ItemType
        int type = TYPE_NORMAL;

        public static final int TYPE_SUBTITLE = 0, TYPE_NORMAL = 1, TYPE_CHECKABLE = 2, TYPE_CHECKABLE_SINGLE = 3;

        private boolean checked = false;

        private Group group = null;

        private @StringRes
        int titleRes;

        public Item(int itemId, int groupId, @ItemType int type, @StringRes int titleRes) {
            this.itemId = itemId;
            this.groupId = groupId;
            this.type = type;
            this.titleRes = titleRes;
        }

        public static Item createSubtitle(int itemId, @StringRes int titleRes) {
            return new Item(itemId, 0, TYPE_SUBTITLE, titleRes);
        }

        public static Item createNormalItem(int itemId, @StringRes int titleRes) {
            return new Item(itemId, 0, TYPE_NORMAL, titleRes);
        }

        public static Item createMultiCheckable(int itemId, @StringRes int titleRes) {
            return new Item(itemId, 0, TYPE_CHECKABLE, titleRes);
        }

        public static Item createSingleCheckable(int itemId, int groupId, @StringRes int titleRes) {
            return new Item(itemId, groupId, TYPE_CHECKABLE_SINGLE, titleRes);
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
            if (checked && group != null) {
                for (Item item : group.items) {
                    if (item.itemId != itemId) {
                        item.setChecked(false);
                    }
                }
            }
        }

        public MenuItemImpl getMenuItem() {
            return new MenuItemImpl();
        }

        @IntDef({TYPE_SUBTITLE, TYPE_NORMAL, TYPE_CHECKABLE, TYPE_CHECKABLE_SINGLE})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ItemType {
        }

        class MenuItemImpl implements MenuItem {

            @Override
            public int getItemId() {
                return itemId;
            }

            @Override
            public int getGroupId() {
                return groupId;
            }

            @Override
            public int getOrder() {
                return itemId;
            }

            @Override
            public boolean isCheckable() {
                return type == TYPE_CHECKABLE || type == TYPE_CHECKABLE_SINGLE;
            }

            @Override
            public MenuItem setChecked(boolean checked) {
                Item.this.setChecked(checked);
                return this;
            }

            @Override
            public boolean isChecked() {
                return Item.this.isChecked();
            }

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean isVisible() {
                return true;
            }

            /**
             * Deprecated methods.
             */


            @Override
            public MenuItem setVisible(boolean visible) {
                return this;
            }

            @Override
            public CharSequence getTitleCondensed() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setTitle(CharSequence title) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setTitle(int title) {
                return this;
            }

            @Override
            @Deprecated
            public CharSequence getTitle() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setTitleCondensed(CharSequence title) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setIcon(Drawable icon) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setIcon(int iconRes) {
                return this;
            }

            @Override
            @Deprecated
            public Drawable getIcon() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setIntent(Intent intent) {
                return this;
            }

            @Override
            @Deprecated
            public Intent getIntent() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setShortcut(char numericChar, char alphaChar) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setNumericShortcut(char numericChar) {
                return this;
            }

            @Override
            @Deprecated
            public char getNumericShortcut() {
                return 0;
            }

            @Override
            @Deprecated
            public MenuItem setAlphabeticShortcut(char alphaChar) {
                return this;
            }

            @Override
            @Deprecated
            public char getAlphabeticShortcut() {
                return 0;
            }

            @Override
            @Deprecated
            public MenuItem setCheckable(boolean checkable) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setEnabled(boolean enabled) {
                return this;
            }

            @Override
            @Deprecated
            public boolean hasSubMenu() {
                return false;
            }

            @Override
            @Deprecated
            public SubMenu getSubMenu() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
                return this;
            }

            @Override
            @Deprecated
            public ContextMenu.ContextMenuInfo getMenuInfo() {
                return null;
            }

            @Override
            @Deprecated
            public void setShowAsAction(int actionEnum) {
            }

            @Override
            @Deprecated
            public MenuItem setShowAsActionFlags(int actionEnum) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setActionView(View view) {
                return this;
            }

            @Override
            @Deprecated
            public MenuItem setActionView(int resId) {
                return this;
            }

            @Override
            @Deprecated
            public View getActionView() {
                return null;
            }

            @Override
            @Deprecated
            public MenuItem setActionProvider(ActionProvider actionProvider) {
                return this;
            }

            @Override
            @Deprecated
            public ActionProvider getActionProvider() {
                return null;
            }

            @Override
            @Deprecated
            public boolean expandActionView() {
                return false;
            }

            @Override
            @Deprecated
            public boolean collapseActionView() {
                return false;
            }

            @Override
            @Deprecated
            public boolean isActionViewExpanded() {
                return false;
            }

            @Override
            @Deprecated
            public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
                return this;
            }
        }

    }

    public static class Group {

        private int groupId;

        private List<Item> items;

        public Group(int groupId) {
            this.groupId = groupId;
            this.items = new ArrayList<>();
        }

        public Item createItem(int itemId, @StringRes int titleRes) {
            Item item = Item.createSingleCheckable(itemId, groupId, titleRes);
            item.group = this;
            items.add(item);
            return item;
        }

    }

}
