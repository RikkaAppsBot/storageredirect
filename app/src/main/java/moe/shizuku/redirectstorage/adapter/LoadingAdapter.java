package moe.shizuku.redirectstorage.adapter;

import moe.shizuku.redirectstorage.viewholder.LoadingViewHolder;
import rikka.recyclerview.BaseRecyclerViewAdapter;
import rikka.recyclerview.ClassCreatorPool;


public class LoadingAdapter extends BaseRecyclerViewAdapter<ClassCreatorPool> {

    public LoadingAdapter() {
        super();

        getCreatorPool().putRule(Object.class, LoadingViewHolder.FULLSCREEN_STYLE_CREATOR);
        getItems().add(new Object());
    }

    @Override
    public ClassCreatorPool onCreateCreatorPool() {
        return new ClassCreatorPool();
    }
}
