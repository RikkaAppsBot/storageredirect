package moe.shizuku.redirectstorage.adapter;

import androidx.annotation.StringRes;

import moe.shizuku.redirectstorage.component.detail.viewholder.AppDetailSummaryViewHolder;
import moe.shizuku.redirectstorage.viewholder.DividerViewHolder;
import moe.shizuku.redirectstorage.viewholder.SettingsDescriptionViewHolder;
import rikka.recyclerview.IdBasedRecyclerViewAdapter;

public final class RecyclerViewAdapterHelper {

    private RecyclerViewAdapterHelper() {}

    public static void addDivider(IdBasedRecyclerViewAdapter adapter, long id) {
        adapter.addItem(DividerViewHolder.CREATOR, null, id);
    }

    public static void addDivider2(IdBasedRecyclerViewAdapter adapter, long id) {
        adapter.addItem(DividerViewHolder.CREATOR2, null, id);
    }

    public static void addDescription(IdBasedRecyclerViewAdapter adapter, @StringRes int stringRes, long id) {
        adapter.addItem(SettingsDescriptionViewHolder.CREATOR, stringRes, id);
    }

    public static void addDescription(IdBasedRecyclerViewAdapter adapter, CharSequence text, long id) {
        adapter.addItem(SettingsDescriptionViewHolder.CREATOR, text, id);
    }

    public static void addSubtitle(IdBasedRecyclerViewAdapter adapter, @StringRes int stringRes, long id) {
        adapter.addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, stringRes, id);
    }

    public static void addSubtitle(IdBasedRecyclerViewAdapter adapter, CharSequence text, long id) {
        adapter.addItem(AppDetailSummaryViewHolder.SUBTITLE_CREATOR, text, id);
    }

    public static void addSubtitle2(IdBasedRecyclerViewAdapter adapter, @StringRes int stringRes, long id) {
        adapter.addItem(AppDetailSummaryViewHolder.SUBTITLE2_CREATOR, stringRes, id);
    }

    public static void addSubtitle2(IdBasedRecyclerViewAdapter adapter, CharSequence text, long id) {
        adapter.addItem(AppDetailSummaryViewHolder.SUBTITLE2_CREATOR, text, id);
    }

    public static void addInformation(IdBasedRecyclerViewAdapter adapter, @StringRes int stringRes, long id) {
        adapter.addItem(AppDetailSummaryViewHolder.INFORMATION_CREATOR, stringRes, id);
    }
}
