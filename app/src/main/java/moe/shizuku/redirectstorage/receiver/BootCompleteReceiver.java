package moe.shizuku.redirectstorage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.component.starter.service.StarterService;
import moe.shizuku.redirectstorage.utils.ForegroundService;
import moe.shizuku.redirectstorage.utils.UserHelper;


public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (!Intent.ACTION_BOOT_COMPLETED.equals(action)
                && !Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(action)) {
            return;
        }

        Log.v(AppConstants.TAG, "onReceive " + action);

        if (UserHelper.myUserId() != 0) {
            Log.w(AppConstants.TAG, "Start service in other user is not supported.");
            return;
        }

        SRManager sm = SRManager.create();
        if (sm != null) {
            Log.i(AppConstants.TAG, "Service found, our service should be running.");
            return;
        }

        ForegroundService.startOrNotify(context, new Intent(context, StarterService.class));
    }
}
