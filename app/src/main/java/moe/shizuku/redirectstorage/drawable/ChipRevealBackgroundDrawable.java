package moe.shizuku.redirectstorage.drawable;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ChipRevealBackgroundDrawable extends Drawable {

    private Context mContext;

    private int mActivatedColor;
    private int mStartX, mStartY, mEndRadius;
    private int mAnimationDuration;
    private int mAlpha = 255;
    private ColorFilter mColorFilter;
    private float mProgress = .0f;

    private @Nullable
    ValueAnimator mRevealAnimator;

    private final Paint mPaint = new Paint() {
        {
            setStyle(Paint.Style.FILL_AND_STROKE);
            setAntiAlias(true);
        }
    };

    public ChipRevealBackgroundDrawable(@NonNull Context context) {
        mContext = context;

        mAnimationDuration = mContext.getResources()
                .getInteger(android.R.integer.config_shortAnimTime);
    }

    public ChipRevealBackgroundDrawable setActivatedColor(@ColorInt int activatedColor) {
        mActivatedColor = activatedColor;
        mPaint.setColor(mActivatedColor);
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
        return this;
    }

    public ChipRevealBackgroundDrawable setAnimationDuration(int duration) {
        mAnimationDuration = duration;
        return this;
    }

    public ChipRevealBackgroundDrawable setStartX(int startX) {
        mStartX = startX;
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
        return this;
    }

    public ChipRevealBackgroundDrawable setStartY(int startY) {
        mStartY = startY;
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
        return this;
    }

    public ChipRevealBackgroundDrawable setEndRadius(int endRadius) {
        mEndRadius = endRadius;
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
        return this;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
        canvas.drawCircle(mStartX, mStartY, mEndRadius * mProgress, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        mAlpha = alpha;
        mPaint.setAlpha(mAlpha);
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mColorFilter = colorFilter;
        mPaint.setColorFilter(mColorFilter);
        if (mRevealAnimator == null || !mRevealAnimator.isRunning()) {
            invalidateSelf();
        }
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }

    public void startRevealAnimation(boolean isActivated) {
        mRevealAnimator = ValueAnimator.ofFloat(
                isActivated ? 0f : 1f, isActivated ? 1f : 0f);
        mRevealAnimator.addUpdateListener(anim -> setProgress((Float) anim.getAnimatedValue()));
        mRevealAnimator.setDuration(mAnimationDuration);
        mRevealAnimator.start();
    }

    public void setProgress(float progress) {
        mProgress = progress;
        invalidateSelf();
    }

}
