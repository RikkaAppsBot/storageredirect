package moe.shizuku.redirectstorage.viewmodel

import androidx.annotation.MainThread
import androidx.lifecycle.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@MainThread
inline fun <T> LiveData<T>.observeChanged(
        owner: LifecycleOwner,
        crossinline onChanged: (T) -> Unit
): Observer<T> {
    val wrappedObserver = Observer<T> { t -> onChanged.invoke(t) }
    Transformations.distinctUntilChanged(this).observe(owner, wrappedObserver)
    return wrappedObserver
}

@MainThread
inline fun <T> LiveData<T>.observeIgnoreInitial(
        owner: LifecycleOwner,
        crossinline onChanged: (T) -> Unit
): Observer<T> {
    val mediatorLiveData = MediatorLiveData<T>()
    mediatorLiveData.addSource(this, object : Observer<T> {
        var firstTime = true
        override fun onChanged(currentValue: T) {
            if (firstTime) {
                firstTime = false
                return
            }
            mediatorLiveData.value = currentValue
        }
    })
    val wrappedObserver = Observer<T> { t -> onChanged.invoke(t) }
    mediatorLiveData.observe(owner, wrappedObserver)
    return wrappedObserver
}

class CoroutineMapLiveData<X, Y>(
        private val context: CoroutineContext = EmptyCoroutineContext,
        private val source: LiveData<X>,
        private val mapper: (X) -> Y?)
    : LiveData<Y>() {

    private val supervisorJob = SupervisorJob(context[Job])
    private val scope = CoroutineScope(Dispatchers.Main.immediate + context + supervisorJob)

    private var job: Job? = null
    private val observer = Observer<X> { source ->
        job?.cancel()
        job = scope.launch(context) {
            mapper(source)?.let {
                postValue(it)
            }
        }
    }

    override fun onActive() {
        source.observeForever(observer)
    }

    override fun onInactive() {
        job?.cancel()
        source.removeObserver(observer)
    }
}

fun <X, Y> LiveData<X>.map(
        context: CoroutineContext = EmptyCoroutineContext,
        transformation: (X) -> Y?) = CoroutineMapLiveData(context, this, transformation)