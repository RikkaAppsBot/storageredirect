package moe.shizuku.redirectstorage.utils;

import android.system.Os;

public class UserHelper {

    private static final int UID = Os.getuid();
    private static final int USER_ID = UID / 100000;

    public static int myUserId() {
        return USER_ID;
    }

    public static int currentUserId() {
        // TODO switch user?
        return myUserId();
    }
}
