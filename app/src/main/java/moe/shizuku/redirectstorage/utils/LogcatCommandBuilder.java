package moe.shizuku.redirectstorage.utils;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class LogcatCommandBuilder {

    private List<Pair<String, Character>> tags = new ArrayList<>();
    private boolean enableTags = false;

    private String format = "brief";

    private boolean clearExistLogs = false;

    public LogcatCommandBuilder() {

    }

    public LogcatCommandBuilder addTag(String tagName, Character logLevel) {
        enableTags = true;
        tags.add(Pair.create(tagName, logLevel));
        return this;
    }

    public LogcatCommandBuilder setFormat(String format) {
        this.format = format;
        return this;
    }

    public LogcatCommandBuilder clearExistLogs() {
        this.clearExistLogs = true;
        return this;
    }

    public String build() {
        StringBuilder sb = new StringBuilder("logcat");
        if (clearExistLogs) {
            sb.append(" -c");
        }
        sb.append(" -v ").append(format);
        if (enableTags) {
            sb.append(" -s");
            for (Pair<String, Character> tag : tags) {
                sb.append(" ")
                        .append(tag.first == null ? "*" : tag.first)
                        .append(":")
                        .append(tag.second);
            }
        }
        return sb.toString();
    }

}