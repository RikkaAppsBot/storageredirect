package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.DeadObjectException;
import android.webkit.MimeTypeMap;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRFileManager;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.provider.ServerFileProvider;

public class FileBrowserUtils {

    private static final List<String> SUFFIX_DOCUMENTS = Arrays.asList(
            "txt", "doc", "docx", "log", "odt", "rtf", "pages", "wps"
    );

    public static final String MIME_TYPE_UNKNOWN = "application/octet-stream";

    public static final int SORT_BY_NAME = 0, SORT_BY_MODIFIED_TIME = 1,
            SORT_BY_LAST_ACCESS = 2;

    private static final MimeTypeMap MIME_TYPE_MAP = MimeTypeMap.getSingleton();

    @IntDef({SORT_BY_NAME, SORT_BY_MODIFIED_TIME, SORT_BY_LAST_ACCESS})
    @Retention(RetentionPolicy.SOURCE)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
    public @interface FileSort {
    }

    public static final int TYPE_UNKNOWN = 0, TYPE_IMAGES = 1, TYPE_AUDIO_ONLY = 2,
            TYPE_PDF = 3, TYPE_DOCUMENTS = 4, TYPE_VIDEO = 5;

    @IntDef({TYPE_UNKNOWN, TYPE_IMAGES, TYPE_AUDIO_ONLY, TYPE_PDF, TYPE_DOCUMENTS, TYPE_VIDEO})
    @Retention(RetentionPolicy.SOURCE)
    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
    public @interface FileTypeDef {
    }

    @Nullable
    public static String getFileSuffix(@Nullable String fileName) {
        return fileName != null && fileName.contains(".")
                ? fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase() : null;
    }

    public static String getMimeTypeBySuffix(@Nullable String suffix) {
        return MIME_TYPE_MAP.getMimeTypeFromExtension(suffix);
    }

    public static @FileTypeDef
    int getFileTypeBySuffix(@Nullable String suffix) {
        String mimeType = getMimeTypeBySuffix(suffix);
        if (mimeType == null) {
            return TYPE_UNKNOWN;
        } else if (mimeType.startsWith("video")) {
            return TYPE_VIDEO;
        } else if (mimeType.startsWith("image")) {
            return TYPE_IMAGES;
        } else if (mimeType.startsWith("audio")) {
            return TYPE_AUDIO_ONLY;
        } else if (mimeType.endsWith("pdf")) {
            return TYPE_PDF;
        } else if (mimeType.startsWith("text") || SUFFIX_DOCUMENTS.contains(suffix)) {
            return TYPE_DOCUMENTS;
        } else {
            return TYPE_UNKNOWN;
        }
    }

    public static @FileTypeDef
    int getFileTypeByName(@Nullable String fileName) {
        return getFileTypeBySuffix(getFileSuffix(fileName));
    }

    @DrawableRes
    public static int getFileIconResources(@NonNull ServerFile file) {
        return getFileIconResources(file.getName(), file.isDirectory(), file.isMountDir, getFileTypeByName(file.getName()));
    }

    @DrawableRes
    public static int getFileIconResources(@Nullable String name, boolean isDirectory, boolean isMountDir, int fileType) {
        if (name == null) {
            return R.drawable.ic_folder_open_24dp;
        } else if (isDirectory) {
            return isMountDir ? R.drawable.ic_folder_link_24dp : R.drawable.ic_folder_24dp;
        } else {
            switch (fileType) {
                case TYPE_AUDIO_ONLY:
                    return R.drawable.ic_audiotrack_24dp;
                case TYPE_IMAGES:
                    return R.drawable.ic_photo_24dp;
                case TYPE_UNKNOWN:
                    return R.drawable.ic_file_24dp;
                case TYPE_PDF:
                    return R.drawable.ic_pdf_24dp;
                case TYPE_DOCUMENTS:
                    return R.drawable.ic_description_24dp;
                case TYPE_VIDEO:
                    return R.drawable.ic_movie_24dp;
                default:
                    throw new IllegalArgumentException("Cannot read icon resources for this type.");
            }
        }
    }

    public static Flowable<ServerFile> getRedirectStorageDirs(ServerFile parent, @FileSort int sortBy) throws DeadObjectException {
        return getRedirectStorageDirs(parent, sortBy, false);
    }

    public static Flowable<ServerFile> getRedirectStorageDirs(ServerFile parent, @FileSort int sortBy, boolean addMountDirs) throws DeadObjectException {
        return Flowable.just(SRManager.createThrow())
                .map(manager -> {
                    List<String> mountDirs = new ArrayList<>();

                    if (ServerFile.TYPE_PACKAGE == parent.getType() && addMountDirs && parent.isRoot()) {
                        if (parent.isRoot()) {
                            mountDirs.addAll(manager.getMountDirsForPackage(parent.getPackageName(), parent.getUserId()));
                        }
                        mountDirs.add("Android");
                    }

                    List<ServerFile> dirs = null;
                    try {
                        dirs = manager.getFileManager().list(parent, SRFileManager.FLAG_GET_STAT);

                        if (ServerFile.TYPE_PACKAGE == parent.getType() && addMountDirs && parent.isRoot()) {
                            for (String s : mountDirs) {
                                dirs.add(ServerFile.fromStorageRoot(s, parent.getUserId()));
                            }
                        }
                    } catch (NullPointerException ignored) {
                    }

                    return sort(dirs, sortBy);
                })
                .flatMap(Flowable::fromIterable)
                .filter(ServerFile::isDirectory)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    public static Flowable<ServerFile> getRedirectStorageFiles(ServerFile parent, @FileSort int sortBy) throws DeadObjectException {
        return Flowable.just(SRManager.createThrow().getFileManager())
                .map(manager -> sort(manager.list(
                        parent,
                        SRFileManager.FLAG_GET_STAT), sortBy))
                .flatMap(Flowable::fromIterable)
                .filter(file -> !file.isDirectory())
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    private static List<ServerFile> sort(List<ServerFile> data, @FileSort int sortBy) {
        if (data != null) {
            switch (sortBy) {
                case SORT_BY_NAME:
                    Collections.sort(data, ServerFile.NAME_COMPARATOR);
                    return data;
                case SORT_BY_MODIFIED_TIME:
                    return Flowable.fromIterable(data)
                            .sorted(ServerFile.MODIFIED_TIME_COMPARATOR)
                            .toList()
                            .blockingGet();
                case SORT_BY_LAST_ACCESS:
                    return Flowable.fromIterable(data)
                            .sorted(ServerFile.LAST_ACCESS_COMPARATOR)
                            .toList()
                            .blockingGet();
                default:
                    // Just keep the original order.
                    return data;
            }
        }
        return data;
    }

    @NonNull
    public static Intent getOpenFileIntent(
            @NonNull String packageName,
            int userId,
            @NonNull String absolutePath,
            @Nullable String fileSuffix,
            @Nullable CharSequence chooserTitle) {
        return getOpenFileIntent(packageName, userId, absolutePath, fileSuffix, null, chooserTitle);
    }

    @NonNull
    public static Intent getOpenFileIntent(
            @NonNull String packageName,
            int userId,
            @NonNull String absolutePath,
            @Nullable String fileSuffix,
            @Nullable String mimeType,
            @Nullable CharSequence chooserTitle) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(ServerFileProvider.getContentUri(packageName, userId, new File(absolutePath)));
        if (mimeType != null) {
            intent.setDataAndType(intent.getData(), mimeType);
        } else if (fileSuffix != null) {
            intent.setDataAndType(intent.getData(),
                    MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileSuffix));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        return Intent.createChooser(intent, chooserTitle);
    }

    @NonNull
    public static Intent getSendToFileIntent(
            @NonNull String packageName,
            int userId,
            @NonNull String relativePath,
            @Nullable String fileSuffix,
            @Nullable String chooserTitle) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, ServerFileProvider.getContentUriRelative(packageName, userId, relativePath));
        if (fileSuffix != null) {
            intent.setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileSuffix));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        return Intent.createChooser(intent, chooserTitle);
    }

    public static String readFileToString(File file) throws IOException {
        try (FileInputStream stream = new FileInputStream(file)) {
            Reader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[8192];
            int read;
            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                builder.append(buffer, 0, read);
            }
            return builder.toString();
        }
    }

    public static void writeStringToFile(File file, String content) throws IOException {
        PrintWriter writer = new PrintWriter(new FileOutputStream(file));
        writer.write(content);
        writer.flush();
        writer.close();
    }

    public static class FileBrowserStyleHolder {

        public int folderIconTintColor;
        public int audioIconTintColor;
        public int imageIconTintColor;
        public int unknownIconTintColor;
        public int pdfIconTintColor;
        public int documentIconTintColor;
        public int videoIconTintColor;

        public FileBrowserStyleHolder(@NonNull Context context) {
            TypedArray a = context.obtainStyledAttributes(null, R.styleable.FileBrowserTheme, R.attr.fileBrowserStyle, 0);

            if (a.hasValue(0)) {
                folderIconTintColor = a.getColor(R.styleable.FileBrowserTheme_folderTint, 0);
                audioIconTintColor = a.getColor(R.styleable.FileBrowserTheme_audioFileTint, 0);
                imageIconTintColor = a.getColor(R.styleable.FileBrowserTheme_imageFileTint, 0);
                unknownIconTintColor = a.getColor(R.styleable.FileBrowserTheme_unknownFileTint, 0);
                pdfIconTintColor = a.getColor(R.styleable.FileBrowserTheme_pdfFileTint, 0);
                documentIconTintColor = a.getColor(R.styleable.FileBrowserTheme_documentFileTint, 0);
                videoIconTintColor = a.getColor(R.styleable.FileBrowserTheme_videoFileTint, 0);
            }

            a.recycle();
        }

        @ColorInt
        public int getFileTypeTintColor(ServerFile item) {
            if (item.getName() == null) {
                return folderIconTintColor;
            } else if (item.isDirectory()) {
                return folderIconTintColor;
            } else {
                return getFileTypeTintColor(getFileTypeByName(item.getName()));
            }
        }

        @ColorInt
        public int getFileTypeTintColor(@FileTypeDef int type) {
            switch (type) {
                case TYPE_AUDIO_ONLY:
                    return audioIconTintColor;
                case TYPE_IMAGES:
                    return imageIconTintColor;
                case TYPE_UNKNOWN:
                    return unknownIconTintColor;
                case TYPE_PDF:
                    return pdfIconTintColor;
                case TYPE_DOCUMENTS:
                    return documentIconTintColor;
                case TYPE_VIDEO:
                    return videoIconTintColor;
                default:
                    throw new IllegalArgumentException("Cannot read tint color for this type.");
            }
        }

    }

}
