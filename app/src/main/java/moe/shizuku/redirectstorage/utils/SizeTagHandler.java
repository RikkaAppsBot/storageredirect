package moe.shizuku.redirectstorage.utils;

import android.text.Editable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;

import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;

import rikka.html.text.HtmlCompat;

public class SizeTagHandler implements HtmlCompat.TagHandler {

    private static SizeTagHandler sInstance;

    public static SizeTagHandler getInstance() {
        if (sInstance == null) {
            sInstance = new SizeTagHandler();
        }
        return sInstance;
    }

    private static <T> T getLast(Spanned text, Class<T> kind) {
        /*
         * This knows that the last returned object from getSpans()
         * will be the most recently added.
         */
        T[] objs = text.getSpans(0, text.length(), kind);

        if (objs.length == 0) {
            return null;
        } else {
            return objs[objs.length - 1];
        }
    }

    private static void setSpanFromMark(Spannable text, Object mark, Object... spans) {
        int where = text.getSpanStart(mark);
        text.removeSpan(mark);
        int len = text.length();
        if (where != len) {
            for (Object span : spans) {
                text.setSpan(span, where, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    private static void start(Editable text, Object mark) {
        int len = text.length();
        text.setSpan(mark, len, len, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
    }

    private static void end(Editable text, Class kind, Object repl) {
        int len = text.length();
        Object obj = getLast(text, kind);
        if (obj != null) {
            setSpanFromMark(text, obj, repl);
        }
    }

    private static class Size {
        public String value;

        public Size(String value) {
            this.value = value;
        }
    }

    @Override
    public void handleTag(boolean opening, String tag, Editable output, Attributes attributes, XMLReader xmlReader) {
        if (tag.equalsIgnoreCase("size")) {
            if (opening) {
                startSize(output, attributes);
            } else {
                endSize(output);
            }
        }
    }

    private void startSize(Editable text, Attributes attributes) {
        String value = attributes.getValue("", "value");

        start(text, new Size(value));
    }

    private static void endSize(Editable text) {
        Size size = getLast(text, Size.class);
        if (size == null || TextUtils.isEmpty(size.value)) {
            return;
        }

        String string = size.value;
        int length = string.length();

        float value;
        if (string.endsWith("px") || string.endsWith("dp") || string.endsWith("sp")) {
            value = Float.valueOf(string.substring(0, length - 2));
        } else {
            value = Float.valueOf(string);
        }

        if (string.endsWith("px")) {
            setSpanFromMark(text, size, new AbsoluteSizeSpan((int) value));
        } else if (string.endsWith("dp")) {
            setSpanFromMark(text, size, new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, HtmlCompat.getContext().getResources().getDisplayMetrics())));
        } else if (string.endsWith("sp")) {
            setSpanFromMark(text, size, new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, value, HtmlCompat.getContext().getResources().getDisplayMetrics())));
        } else {
            setSpanFromMark(text, size, new RelativeSizeSpan(value));
        }
    }
}
