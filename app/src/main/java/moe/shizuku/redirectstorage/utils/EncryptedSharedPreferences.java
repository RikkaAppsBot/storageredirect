package moe.shizuku.redirectstorage.utils;

import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.nio.charset.StandardCharsets.UTF_8;

public final class EncryptedSharedPreferences implements SharedPreferences {

    private static final String NULL_VALUE = "__NULL__";

    private static final int BYTE_BYTES = 1;
    private static final int INT_BYTES = Integer.SIZE / Byte.SIZE;
    private static final int LONG_BYTES = Long.SIZE / Byte.SIZE;
    private static final int FLOAT_BYTES = Float.SIZE / Byte.SIZE;

    private final SharedPreferences mSharedPreferences;
    private final List<OnSharedPreferenceChangeListener> mListeners;
    private final String mAADForKey;
    private final boolean mAADIsBase64;

    private EncryptedSharedPreferences(@NonNull SharedPreferences sharedPreferences, @Nullable String aadForKey, boolean addIsBase64) {
        mAADForKey = aadForKey;
        mAADIsBase64 = addIsBase64;
        mSharedPreferences = sharedPreferences;
        mListeners = new ArrayList<>();
    }

    @NonNull
    public static SharedPreferences create(@NonNull SharedPreferences sharedPreferences, @NonNull String aadForKey) {
        return new EncryptedSharedPreferences(sharedPreferences, aadForKey, false);
    }

    @NonNull
    public static SharedPreferences create(@NonNull SharedPreferences sharedPreferences, @NonNull String aadForKey, boolean aadIsBase64) {
        return new EncryptedSharedPreferences(sharedPreferences, aadForKey, aadIsBase64);
    }

    @NonNull
    public static SharedPreferences createDeviceSpecific(@NonNull SharedPreferences sharedPreferences) {
        return new EncryptedSharedPreferences(sharedPreferences, null, false);
    }

    private static final class Editor implements SharedPreferences.Editor {

        private final EncryptedSharedPreferences mEncryptedSharedPreferences;
        private final SharedPreferences.Editor mEditor;
        private final List<String> mKeysChanged;
        private AtomicBoolean mClearRequested = new AtomicBoolean(false);

        Editor(EncryptedSharedPreferences encryptedSharedPreferences,
               SharedPreferences.Editor editor) {
            mEncryptedSharedPreferences = encryptedSharedPreferences;
            mEditor = editor;
            mKeysChanged = new CopyOnWriteArrayList<>();
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putString(@Nullable String key, @Nullable String value) {
            if (value == null) {
                value = NULL_VALUE;
            }
            byte[] stringBytes = value.getBytes(UTF_8);
            int stringByteLength = stringBytes.length;
            ByteBuffer buffer = ByteBuffer.allocate(INT_BYTES + INT_BYTES
                    + stringByteLength);
            buffer.putInt(EncryptedType.STRING.getId());
            buffer.putInt(stringByteLength);
            buffer.put(stringBytes);
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putStringSet(@Nullable String key,
                                                     @Nullable Set<String> values) {
            if (values == null) {
                values = new ArraySet<>();
                values.add(NULL_VALUE);
            }
            List<byte[]> byteValues = new ArrayList<>(values.size());
            int totalBytes = values.size() * INT_BYTES;
            for (String strValue : values) {
                byte[] byteValue = strValue.getBytes(UTF_8);
                byteValues.add(byteValue);
                totalBytes += byteValue.length;
            }
            totalBytes += INT_BYTES;
            ByteBuffer buffer = ByteBuffer.allocate(totalBytes);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putInt(EncryptedType.STRING_SET.getId());
            for (byte[] bytes : byteValues) {
                buffer.putInt(bytes.length);
                buffer.put(bytes);
            }
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putInt(@Nullable String key, int value) {
            ByteBuffer buffer = ByteBuffer.allocate(INT_BYTES + INT_BYTES);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putInt(EncryptedType.INT.getId());
            buffer.putInt(value);
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putLong(@Nullable String key, long value) {
            ByteBuffer buffer = ByteBuffer.allocate(INT_BYTES + LONG_BYTES);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putInt(EncryptedType.LONG.getId());
            buffer.putLong(value);
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putFloat(@Nullable String key, float value) {
            ByteBuffer buffer = ByteBuffer.allocate(INT_BYTES + FLOAT_BYTES);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putInt(EncryptedType.FLOAT.getId());
            buffer.putFloat(value);
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor putBoolean(@Nullable String key, boolean value) {
            ByteBuffer buffer = ByteBuffer.allocate(INT_BYTES + BYTE_BYTES);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putInt(EncryptedType.BOOLEAN.getId());
            buffer.put(value ? (byte) 1 : (byte) 0);
            try {
                putEncryptedObject(key, buffer.array());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor remove(@Nullable String key) {
            String encryptedKey;
            try {
                encryptedKey = mEncryptedSharedPreferences.encryptKey(key);
            } catch (Exception e) {
                e.printStackTrace();
                return this;
            }
            mEditor.remove(encryptedKey);
            mKeysChanged.remove(key);
            return this;
        }

        @Override
        @NonNull
        public SharedPreferences.Editor clear() {
            // Set the flag to clear on commit, this operation happens first on commit.
            // Cannot use underlying clear operation, it will remove the keysets and
            // break the editor.
            mClearRequested.set(true);
            return this;
        }

        @Override
        public boolean commit() {
            // Call "clear" first as per the documentation, remove all keys that haven't
            // been modified in this editor.
            if (mClearRequested.getAndSet(false)) {
                for (String key : mEncryptedSharedPreferences.getAll().keySet()) {
                    if (!mKeysChanged.contains(key)) {
                        String encryptedKey;
                        try {
                            encryptedKey = mEncryptedSharedPreferences.encryptKey(key);
                        } catch (Exception e) {
                            e.printStackTrace();
                            continue;
                        }
                        mEditor.remove(encryptedKey);
                    }
                }
            }
            try {
                return mEditor.commit();
            } finally {
                notifyListeners();
                mKeysChanged.clear();
            }
        }

        @Override
        public void apply() {
            mEditor.apply();
            notifyListeners();
        }

        private void putEncryptedObject(String key, byte[] value) throws Exception {
            mKeysChanged.add(key);
            if (key == null) {
                key = NULL_VALUE;
            }
            Pair<String, String> encryptedPair = mEncryptedSharedPreferences
                    .encryptKeyValuePair(key, value);
            mEditor.putString(encryptedPair.first, encryptedPair.second);
        }

        private void notifyListeners() {
            for (OnSharedPreferenceChangeListener listener :
                    mEncryptedSharedPreferences.mListeners) {
                for (String key : mKeysChanged) {
                    listener.onSharedPreferenceChanged(mEncryptedSharedPreferences, key);
                }
            }
        }
    }

    @Override
    @NonNull
    public Map<String, ?> getAll() {
        Map<String, ? super Object> allEntries = new HashMap<>();
        for (Map.Entry<String, ?> entry : mSharedPreferences.getAll().entrySet()) {
            if ("__aad__".equals(entry.getKey())) {
                continue;
            }
            String decryptedKey;
            Object decryptedObject;
            try {
                decryptedKey = decryptKey(entry.getKey());
                decryptedObject = getDecryptedObject(decryptedKey);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            allEntries.put(decryptedKey, decryptedObject);
        }
        return allEntries;
    }

    @Nullable
    @Override
    public String getString(@Nullable String key, @Nullable String defValue) {
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return (value instanceof String ? (String) value : defValue);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public Set<String> getStringSet(@Nullable String key, @Nullable Set<String> defValues) {
        Set<String> returnValues;
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (value instanceof Set) {
            returnValues = (Set<String>) value;
        } else {
            returnValues = new ArraySet<>();
        }
        return returnValues.size() > 0 ? returnValues : defValues;
    }

    @Override
    public int getInt(@Nullable String key, int defValue) {
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return defValue;
        }
        return (value instanceof Integer ? (Integer) value : defValue);
    }

    @Override
    public long getLong(@Nullable String key, long defValue) {
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return defValue;
        }
        return (value instanceof Long ? (Long) value : defValue);
    }

    @Override
    public float getFloat(@Nullable String key, float defValue) {
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return defValue;
        }
        return (value instanceof Float ? (Float) value : defValue);
    }

    @Override
    public boolean getBoolean(@Nullable String key, boolean defValue) {
        Object value;
        try {
            value = getDecryptedObject(key);
        } catch (Exception e) {
            e.printStackTrace();
            return defValue;
        }
        return (value instanceof Boolean ? (Boolean) value : defValue);
    }

    @Override
    public boolean contains(@Nullable String key) {
        String encryptedKey;
        try {
            encryptedKey = encryptKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return mSharedPreferences.contains(encryptedKey);
    }

    @Override
    @NonNull
    public SharedPreferences.Editor edit() {
        return new Editor(this, mSharedPreferences.edit());
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(
            @NonNull OnSharedPreferenceChangeListener listener) {
        mListeners.add(listener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(
            @NonNull OnSharedPreferenceChangeListener listener) {
        mListeners.remove(listener);
    }

    /**
     * Internal enum to set the type of encrypted data.
     */
    private enum EncryptedType {
        STRING(0),
        STRING_SET(1),
        INT(2),
        LONG(3),
        FLOAT(4),
        BOOLEAN(5);

        int mId;

        EncryptedType(int id) {
            mId = id;
        }

        public int getId() {
            return mId;
        }

        public static EncryptedType fromId(int id) {
            switch (id) {
                case 0:
                    return STRING;
                case 1:
                    return STRING_SET;
                case 2:
                    return INT;
                case 3:
                    return LONG;
                case 4:
                    return FLOAT;
                case 5:
                    return BOOLEAN;
            }
            return null;
        }
    }

    private Object getDecryptedObject(String key) throws Exception {
        if (key == null) {
            key = NULL_VALUE;
        }
        Object returnValue;
        String encryptedKey = encryptKey(key);
        String encryptedValue = mSharedPreferences.getString(encryptedKey, null);
        byte[] value;
        try {
            value = decrypt(encryptedValue, encryptedKey, false);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (value == null) {
            return null;
        }
        ByteBuffer buffer = ByteBuffer.wrap(value);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.position(0);
        int typeId = buffer.getInt();
        EncryptedType type = EncryptedType.fromId(typeId);
        if (type == null) {
            return null;
        }
        switch (type) {
            case STRING:
                int stringLength = buffer.getInt();
                ByteBuffer stringSlice = buffer.slice();
                buffer.limit(stringLength);
                String stringValue = UTF_8.decode(stringSlice).toString();
                if (stringValue.equals(NULL_VALUE)) {
                    returnValue = null;
                } else {
                    returnValue = stringValue;
                }
                break;
            case INT:
                returnValue = buffer.getInt();
                break;
            case LONG:
                returnValue = buffer.getLong();
                break;
            case FLOAT:
                returnValue = buffer.getFloat();
                break;
            case BOOLEAN:
                returnValue = buffer.get() != (byte) 0;
                break;
            case STRING_SET:
                ArraySet<String> stringSet = new ArraySet<>();
                while (buffer.hasRemaining()) {
                    int subStringLength = buffer.getInt();
                    ByteBuffer subStringSlice = buffer.slice();
                    subStringSlice.limit(subStringLength);
                    buffer.position(buffer.position() + subStringLength);
                    stringSet.add(UTF_8.decode(subStringSlice).toString());
                }
                if (stringSet.size() == 1 && NULL_VALUE.equals(stringSet.valueAt(0))) {
                    returnValue = null;
                } else {
                    returnValue = stringSet;
                }
                break;
            default:
                returnValue = null;
        }
        return returnValue;
    }

    private String encryptKey(String key) throws Exception {
        if (key == null) {
            key = NULL_VALUE;
        }
        return encrypt(key.getBytes(UTF_8), mAADForKey, mAADIsBase64);
    }

    private String decryptKey(String encryptedKey) throws Exception {
        byte[] clearText = decrypt(encryptedKey, mAADForKey, mAADIsBase64);
        String key = new String(clearText, UTF_8);
        if (key.equals(NULL_VALUE)) {
            key = null;
        }
        return key;
    }

    private Pair<String, String> encryptKeyValuePair(String key, byte[] value) throws Exception {
        String encryptedKey = encryptKey(key);
        return new Pair<>(encryptedKey, encrypt(value, encryptedKey, false));
    }

    /**
     * @param plainText plain text
     * @param aad       additional authenticated data
     * @return cipher text in base64
     */
    @SuppressWarnings("JavaJniMissingFunction")
    private native String encrypt(byte[] plainText, String aad, boolean aadIsBase64) throws Exception;

    /**
     * @param base64CipherText cipher text in base64
     * @param aad              additional authenticated data
     * @return plain text
     */
    @SuppressWarnings("JavaJniMissingFunction")
    private native byte[] decrypt(String base64CipherText, String aad, boolean aadIsBase64) throws Exception;
}
