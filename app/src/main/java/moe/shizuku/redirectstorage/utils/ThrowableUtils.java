package moe.shizuku.redirectstorage.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ThrowableUtils {

    public static String messageAndStack(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter writer = new PrintWriter(sw);
        writer.println(e.getMessage());
        writer.println();
        e.printStackTrace(writer);
        return sw.toString();
    }

    public static String stack(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter writer = new PrintWriter(sw);
        e.printStackTrace(writer);
        return sw.toString();
    }
}
