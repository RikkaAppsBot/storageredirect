package moe.shizuku.redirectstorage.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import io.reactivex.Flowable;
import io.reactivex.Single;
import moe.shizuku.redirectstorage.BuildConfig;

public final class ShareHelperListPreferences {

    private static final String PREF_NAME = "share_helper";
    private static final String KEY_LIST = "list";

    private static final Gson sGson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    private static final Intent COMMON_SHARE_INTENT = new Intent(Intent.ACTION_SEND);

    static {
        COMMON_SHARE_INTENT.setType("*/*");
        COMMON_SHARE_INTENT.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://fake_uri"));
    }

    private final SharedPreferences mSharedPref;
    private final PackageManager mPackageManager;

    public ShareHelperListPreferences(@NonNull Context context) {
        this.mSharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.mPackageManager = context.getPackageManager();
    }

    @NonNull
    public Flowable<Item> getInstalledApps() {
        return getInstalledApps(COMMON_SHARE_INTENT);
    }

    @NonNull
    public Flowable<Item> getInstalledApps(@NonNull Intent toShareIntent) {
        return Flowable.just(toShareIntent)
                .flatMap(intent -> {
                    final List<ResolveInfo> resolveInfo = mPackageManager
                            .queryIntentActivities(intent, PackageManager.GET_META_DATA);

                    final List<Item> prefs = getPreferences().toList().blockingGet();

                    return Flowable.fromIterable(resolveInfo)
                            .filter(info -> !info.activityInfo.packageName
                                    .equals(BuildConfig.APPLICATION_ID))
                            .map(info -> {
                                final Item item = new Item(
                                        info.activityInfo.packageName, info.activityInfo.name);
                                final int prefIndex = prefs.indexOf(item);
                                if (prefIndex != -1) {
                                    item.rank = prefs.get(prefIndex).rank;
                                    item.showInDirectShare = prefs.get(prefIndex).showInDirectShare;
                                }
                                item.activityInfo = info.activityInfo;
                                return item;
                            });
                })
                .sorted(Item.COMPARATOR);
    }

    @NonNull
    public Flowable<Item> getPreferences() {
        return Flowable.just(mSharedPref)
                .flatMap(pref -> {
                    final String value = pref.getString(KEY_LIST, "[]");
                    Item[] items = null;
                    try {
                        items = sGson.fromJson(value, Item[].class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (items == null) {
                        items = new Item[0];
                    }
                    return Flowable.fromArray(items);
                })
                .map(item -> {
                    ActivityInfo res = null;
                    try {
                        res = item.loadActivityInfo(mPackageManager);
                    } catch (Exception ignored) {

                    }
                    return res != null ? Optional.of(item) : Optional.<Item>empty();
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .sorted(Item.COMPARATOR);
    }

    public void setPreferences(@NonNull List<Item> items) {
        final String json = sGson.toJson(items);
        mSharedPref.edit().putString(KEY_LIST, json).apply();
    }

    public Single editPreferences(@NonNull Consumer<List<Item>> editConsumer) {
        return Single.fromCallable(() -> {
            setPreferences(getPreferences()
                    .toList()
                    .map(list -> {
                        editConsumer.accept(list);
                        return list;
                    })
                    .blockingGet());
            return true;
        });
    }

    public static class Item {

        public static final Comparator<Item> COMPARATOR = (l, r) -> {
            if (r.rank != l.rank) {
                return r.rank - l.rank;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(l.packageName, r.packageName);
        };

        @Expose
        @NonNull
        public final String packageName;
        @Expose
        @NonNull
        public final String activityName;
        @Expose
        public int rank = 0;
        @Expose
        public boolean showInDirectShare = false;

        public ActivityInfo activityInfo;

        public Item(@NonNull String packageName, @NonNull String activityName) {
            this(packageName, activityName, 0, false);
        }

        public Item(@NonNull String packageName,
                    @NonNull String activityName,
                    int rank,
                    boolean showInDirectShare) {
            this.packageName = packageName;
            this.activityName = activityName;
            this.rank = rank;
            this.showInDirectShare = showInDirectShare;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof Item)) return false;
            Item other = (Item) obj;
            return Objects.equals(packageName, other.packageName) &&
                    Objects.equals(activityName, other.activityName);
        }

        @NonNull
        public ActivityInfo loadActivityInfo(@NonNull PackageManager packageManager)
                throws PackageManager.NameNotFoundException {
            if (activityInfo == null) {
                try {
                    activityInfo = packageManager.getActivityInfo(
                            ComponentName.createRelative(packageName, activityName),
                            PackageManager.GET_META_DATA
                    );
                } catch (Exception e) {
                    throw new PackageManager.NameNotFoundException();
                }
            }
            return activityInfo;
        }

    }

}
