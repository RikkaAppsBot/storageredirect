package moe.shizuku.redirectstorage.utils;

import java.nio.charset.StandardCharsets;

/**
 * Created by rikka on 2017/12/20.
 */

public class Base64 {

    public static String encodeToString(String string) {
        return android.util.Base64.encodeToString(string.getBytes(), android.util.Base64.DEFAULT);
    }

    public static String decodeToString(String string) {
        return new String(android.util.Base64.decode(string, android.util.Base64.DEFAULT), StandardCharsets.UTF_8);
    }
}
