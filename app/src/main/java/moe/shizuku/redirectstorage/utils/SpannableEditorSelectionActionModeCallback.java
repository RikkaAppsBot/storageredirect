package moe.shizuku.redirectstorage.utils;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.Callable;

import moe.shizuku.redirectstorage.R;

/**
 * Created by fytho on 2018/1/21.
 */

public class SpannableEditorSelectionActionModeCallback extends ActionMode.Callback2 {

    private EditText mEditText;
    private @Nullable
    OnSpannableChangeListener mListener;

    private static final SpanCreator BOLD_SPAN_CREATOR = () -> new StyleSpan(Typeface.BOLD);

    private static final SpanCreator ITALIC_SPAN_CREATOR = () -> new StyleSpan(Typeface.ITALIC);

    private static final SpanCreator BOLD_ITALIC_SPAN_CREATOR = () -> new StyleSpan(Typeface.BOLD_ITALIC);

    public SpannableEditorSelectionActionModeCallback(@NonNull EditText editText, @Nullable OnSpannableChangeListener listener) {
        mEditText = editText;
        mListener = listener;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.editor_selection_actions_for_html, menu);

        iconItem(menu.findItem(R.id.action_bold));
        iconItem(menu.findItem(R.id.action_italic));
        return true;
    }

    private void iconItem(MenuItem item) {
        if (item.getIcon() != null) {
            iconItem(item, item.getIcon());
        }
    }

    private void iconItem(MenuItem item, Drawable icon) {
        icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());

        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(item.getTitle());
        builder.setSpan(new ImageSpan(icon, ImageSpan.ALIGN_BOTTOM), 0, item.getTitle().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        item.setTitle(builder);
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bold:
                setSpanForSelectedText(BOLD_SPAN_CREATOR);
                return true;
            case R.id.action_italic:
                setSpanForSelectedText(ITALIC_SPAN_CREATOR);
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }

    private void setSpanForSelectedText(@NonNull SpanCreator creator) {
        // TODO handle multiply span
        int start = mEditText.getSelectionStart(), end = mEditText.getSelectionEnd();
        if (start >= 0 && end >= 0) {
            Object[] currentSpans = mEditText.getText()
                    .getSpans(start, end, creator.call().getClass());
            boolean existSpan = false;
            for (Object s : currentSpans) {
                mEditText.getText().removeSpan(s);
                existSpan = true;
            }
            if (!existSpan) {
                for (int i = start; i < end; i++) {
                    mEditText.getText().setSpan(creator.call(),
                            i, i + 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }
                if (mEditText.getText().toString()
                        .substring(start, end).equalsIgnoreCase("rikka")) {
                    mEditText.setText(mEditText.getText().replace(start, end, "坏耶"));
                }
            }
        }
        if (mListener != null) {
            mListener.onChange();
        }
    }

    private interface SpanCreator extends Callable {
        @Override
        Object call();
    }

    public interface OnSpannableChangeListener {
        void onChange();
    }

    public static String toHtml(Spanned spanned) {
        StringBuilder sb = new StringBuilder();
        boolean isBold = false, isItalic = false;
        for (int index = 0; index < spanned.length(); index++) {
            StyleSpan[] spans = spanned.getSpans(index, index, StyleSpan.class);
            boolean nextIsBold = false, nextIsItalic = false;
            for (StyleSpan span : spans) {
                if (spanned.getSpanEnd(span) == index) continue;
                switch (span.getStyle()) {
                    case Typeface.BOLD:
                        nextIsBold = true;
                        nextIsItalic = false;
                        break;
                    case Typeface.ITALIC:
                        nextIsBold = false;
                        nextIsItalic = true;
                        break;
                    case Typeface.BOLD_ITALIC:
                        nextIsBold = true;
                        nextIsItalic = true;
                        break;
                    case Typeface.NORMAL:
                        nextIsBold = false;
                        nextIsItalic = false;
                        break;
                }
            }
            if (isBold ^ nextIsBold && !nextIsBold) {
                sb.append("</b>");
            }
            if (isItalic ^ nextIsItalic && !nextIsItalic) {
                sb.append("</i>");
            }
            if (isBold ^ nextIsBold && nextIsBold) {
                sb.append("<b>");
            }
            if (isItalic ^ nextIsItalic && nextIsItalic) {
                sb.append("<i>");
            }
            sb.append(spanned.subSequence(index, index + 1));
            isBold = nextIsBold;
            isItalic = nextIsItalic;
        }
        if (isBold) {
            if (isItalic) {
                sb.append((sb.lastIndexOf("<b>") < sb.lastIndexOf("<i>"))
                        ? "</i></b>" : "</b></i>");
            } else {
                sb.append("</b>");
            }
        } else if (isItalic) {
            sb.append("</i>");
        }
        return sb.toString();
    }

}
