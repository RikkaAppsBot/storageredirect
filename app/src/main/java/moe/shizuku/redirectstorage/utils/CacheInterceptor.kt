package moe.shizuku.redirectstorage.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import java.io.IOException

class OfflineCacheInterceptor(context: Context) : Interceptor {

    private val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private fun isNetworkAvailable(): Boolean {
        val network = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(network) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }

    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        return if (!isNetworkAvailable()) {
            val request = chain.request().newBuilder()
            request.header("Cache-Control", "public, only-if-cached, max-stale=" + Integer.MAX_VALUE)
            val response = chain.proceed(request.build())
            if (response.code == 504) {
                request.removeHeader("Cache-Control")
                chain.proceed(request.build())
            } else {
                response
            }
        } else {
            chain.proceed(chain.request())
        }
    }
}