package moe.shizuku.redirectstorage.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SharedPreferencesXmlReader {

    public static SharedPreferencesXmlReader parse(InputSource source)
            throws IOException, SAXException, ParserConfigurationException {
        return new SharedPreferencesXmlReader(source);
    }

    public static SharedPreferencesXmlReader parse(String content)
            throws IOException, SAXException, ParserConfigurationException {
        return parse(new InputSource(new StringReader(content)));
    }

    public final Map<String, Integer> intValues = new HashMap<>();
    public final Map<String, Boolean> boolValues = new HashMap<>();
    public final Map<String, Long> longValues = new HashMap<>();
    public final Map<String, String> stringValues = new HashMap<>();

    private SharedPreferencesXmlReader(InputSource source)
            throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = documentBuilder.parse(source);
        Element element = doc.getDocumentElement();

        NodeList ints = element.getElementsByTagName("int");
        for (int i = 0; i < ints.getLength(); i++) {
            final Element item = (Element) ints.item(i);
            intValues.put(item.getAttribute("name"), Integer.valueOf(item.getAttribute("value")));
        }

        NodeList longs = element.getElementsByTagName("long");
        for (int i = 0; i < longs.getLength(); i++) {
            final Element item = (Element) longs.item(i);
            longValues.put(item.getAttribute("name"), Long.valueOf(item.getAttribute("value")));
        }

        NodeList bools = element.getElementsByTagName("boolean");
        for (int i = 0; i < bools.getLength(); i++) {
            final Element item = (Element) bools.item(i);
            boolValues.put(item.getAttribute("name"), Boolean.valueOf(item.getAttribute("value")));
        }

        NodeList strs = element.getElementsByTagName("string");
        for (int i = 0; i < strs.getLength(); i++) {
            final Element item = (Element) strs.item(i);
            stringValues.put(item.getAttribute("name"), item.getTextContent());
        }
    }

}
