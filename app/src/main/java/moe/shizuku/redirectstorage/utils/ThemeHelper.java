package moe.shizuku.redirectstorage.utils;

import android.content.Context;

import androidx.annotation.StyleRes;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.app.Settings;
import rikka.core.util.ResourceUtils;

public class ThemeHelper {

    private static final String TAG = "ThemeHelper";

    private static final String THEME_DEFAULT = "DEFAULT";
    private static final String THEME_BLACK = "BLACK";
    private static final String THEME_DEFAULT_PIXEL_BLUE = "pixel";
    private static final String THEME_DEFAULT_BLUE = "dark_blue";
    private static final String THEME_AMBER = "amber";
    private static final String THEME_LIGHT_BLUE = "light_blue";
    private static final String THEME_DARK_BLUE = "dark_blue_no_white";
    private static final String THEME_PINK = "pink";

    private static final String ACCENT_COLOR_DEFAULT = "DEFAULT";
    private static final String ACCENT_COLOR_TEAL = "teal";
    private static final String ACCENT_COLOR_PINK = "pink";
    private static final String ACCENT_COLOR_DEEP_PURPLE = "deep_purple";
    private static final String ACCENT_COLOR_CYAN = "cyan";
    private static final String ACCENT_COLOR_GREEN = "green";
    private static final String ACCENT_COLOR_ORANGE = "orange";
    private static final String ACCENT_COLOR_BROWN = "brown";
    private static final String ACCENT_COLOR_BLUE_GREY = "blue_grey";
    private static final String ACCENT_COLOR_PIXEL = "pixel_blue";

    public static final String KEY_ACCENT_COLOR = "accent_color";
    public static final String KEY_LIGHT_THEME = "light_theme";
    public static final String KEY_BLACK_NIGHT_THEME = "black_night_theme";

    public static void setLightTheme(String theme) {
        Settings.getPreferences().edit().putString(KEY_LIGHT_THEME, theme).apply();
    }

    public static boolean isBlackNightTheme() {
        return Settings.getPreferences().getBoolean(KEY_BLACK_NIGHT_THEME, false);
    }

    public static String getTheme(Context context) {
        if (isBlackNightTheme()
                && ResourceUtils.isNightMode(context.getResources().getConfiguration()))
            return THEME_BLACK;

        return Settings.getPreferences().getString(KEY_LIGHT_THEME, THEME_DEFAULT_BLUE);
    }

    @StyleRes
    public static int getThemeStyleRes(Context context) {
        switch (getTheme(context)) {
            case THEME_DEFAULT_PIXEL_BLUE:
                return R.style.ThemeOverlay_Solid_Pixel;
            case THEME_DEFAULT_BLUE:
                return R.style.ThemeOverlay_Solid_DarkBlue;
            case THEME_AMBER:
                return R.style.ThemeOverlay_Colored_Amber;
            case THEME_LIGHT_BLUE:
                return R.style.ThemeOverlay_Colored_LightBlue;
            case THEME_DARK_BLUE:
                return R.style.ThemeOverlay_Colored_DarkActionBar_DarkBlue;
            case THEME_PINK:
                return R.style.ThemeOverlay_Colored_Pink;

            case THEME_BLACK:
                return R.style.ThemeOverlay_Solid_Black;
            case THEME_DEFAULT:
            default:
                return R.style.ThemeOverlay;
        }
    }

    public static String getAccentColor() {
        return Settings.getPreferences().getString(KEY_ACCENT_COLOR, ACCENT_COLOR_DEEP_PURPLE);
    }

    public static void setAccentColor(String theme) {
        Settings.getPreferences().edit().putString(KEY_ACCENT_COLOR, theme).apply();
    }

    @StyleRes
    public static int getAccentColorStyleRes() {
        switch (getAccentColor()) {
            case ACCENT_COLOR_PIXEL:
                return R.style.ThemeOverlay_PixelBlueAccent;

            case ACCENT_COLOR_PINK:
                return R.style.ThemeOverlay_PinkAccent;
            case ACCENT_COLOR_DEEP_PURPLE:
                return R.style.ThemeOverlay_DeepPurpleAccent;
            case ACCENT_COLOR_CYAN:
                return R.style.ThemeOverlay_CyanAccent;
            case ACCENT_COLOR_GREEN:
                return R.style.ThemeOverlay_GreenAccent;
            case ACCENT_COLOR_ORANGE:
                return R.style.ThemeOverlay_OrangeAccent;
            case ACCENT_COLOR_BROWN:
                return R.style.ThemeOverlay_BrownAccent;
            case ACCENT_COLOR_BLUE_GREY:
                return R.style.ThemeOverlay_BlueGreyAccent;
            case ACCENT_COLOR_TEAL:
                return R.style.ThemeOverlay_TealAccent;
            default:
                return R.style.ThemeOverlay;
        }
    }
}
