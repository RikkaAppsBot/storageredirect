package moe.shizuku.redirectstorage.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.List;

import moe.shizuku.redirectstorage.R;
import rikka.core.util.ContextUtils;

public class ShowAppInfoHelper {

    public static Intent newIntent(Context context, String packageName) {
        return newIntent(context, packageName, false);
    }

    public static Intent newIntent(Context context, String packageName, boolean check) {
        Intent intent = new Intent("android.intent.action.SHOW_APP_INFO")
                .putExtra("android.intent.extra.PACKAGE_NAME", packageName);

        List<ResolveInfo> resolveInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : new ArrayList<>(resolveInfo)) {
            if (!info.activityInfo.exported || context.getPackageName().equals(info.activityInfo.packageName)) {
                resolveInfo.remove(info);
            }
        }

        if (!check || !resolveInfo.isEmpty()) {
            return intent;
        }
        return null;
    }

    public static boolean check(Context context, String packageName) {
        return newIntent(context, packageName, true) != null;
    }

    public static boolean startActivity(Context context, String packageName) {
        Intent intent = newIntent(context, packageName);
        if (intent == null) {
            return false;
        }

        intent = Intent.createChooser(intent, context.getString(R.string.show_app_info_in));
        Activity activity = ContextUtils.getActivity(context);
        try {
            if (activity != null) {
                activity.startActivityForResult(intent, 1313);
            } else {
                context.startActivity(intent);
            }
        } catch (Throwable tr) {
            return false;
        }
        return true;
    }
}
