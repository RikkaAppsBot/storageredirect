package moe.shizuku.redirectstorage.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;

import moe.shizuku.redirectstorage.R;
import rikka.core.util.ResourceUtils;

public class NotificationHelper {

    public static int getDefaultColor(Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.uiMode = (configuration.uiMode & ~Configuration.UI_MODE_NIGHT_MASK) | Configuration.UI_MODE_NIGHT_NO;
        Context themedContext = context.createConfigurationContext(configuration);
        themedContext.getTheme().applyStyle(R.style.Theme_Light, true);
        themedContext.getTheme().applyStyle(ThemeHelper.getThemeStyleRes(context), true);

        return ResourceUtils.resolveColor(themedContext.getTheme(), R.attr.notificationColor);
    }

    public static NotificationCompat.Builder create(Context context, String channel, @StringRes int title) {
        return new NotificationCompat.Builder(context, channel)
                .setContentTitle(context.getString(title))
                .setColor(getDefaultColor(context))
                .setSmallIcon(R.drawable.ic_notification_24dp)
                .setWhen(System.currentTimeMillis())
                .setShowWhen(true);
    }

    public static NotificationCompat.Builder create(Context context, String channel, @StringRes int title, @StringRes int text) {
        return create(context, channel, title).setContentText(context.getString(text));
    }

    public static void notify(Context context, int id, String channel, @StringRes int title, @StringRes int text) {
        notify(context, id, create(context, channel, title, text));
    }

    public static void notify(Context context, int id, String channel, @StringRes int title, CharSequence text) {
        notify(context, id, create(context, channel, title).setContentText(text).setStyle(new NotificationCompat.BigTextStyle().bigText(text)));
    }

    public static void notify(Context context, int id, NotificationCompat.Builder builder) {
        final NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        if (notificationManager == null) {
            return;
        }

        notificationManager.notify(id, builder.build());
    }

    public static void cancel(Context context, int id) {
        final NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        if (notificationManager == null) {
            return;
        }

        notificationManager.cancel(id);
    }
}
