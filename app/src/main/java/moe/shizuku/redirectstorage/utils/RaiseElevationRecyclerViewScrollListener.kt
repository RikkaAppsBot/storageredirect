package moe.shizuku.redirectstorage.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.R

class RaiseElevationRecyclerViewScrollListener(
        val targetView: View
) : RecyclerView.OnScrollListener() {

    private val mContext: Context = targetView.context

    private val mStatedElevation: Map<Boolean, Float> = mapOf(
            true to mContext.resources.getDimension(R.dimen.app_bar_elevation),
            false to 0f
    )
    private val mDuration: Long = mContext.resources
            .getInteger(android.R.integer.config_mediumAnimTime).toLong()

    private var mElevationAnimator: Animator? = null
    private var mAnimatorDirection: Boolean? = null

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager
        if (layoutManager !is LinearLayoutManager) {
            // TODO Support more layout managers
            throw UnsupportedOperationException("RecyclerView's LayoutManager" +
                    " should be LinearLayoutManager.")
        }
        val shouldRaise = layoutManager.findFirstCompletelyVisibleItemPosition() > 0

        if (mAnimatorDirection == null || mAnimatorDirection != shouldRaise) {
            if (mElevationAnimator?.isRunning == true) {
                mElevationAnimator?.cancel()
            }
        }
        mAnimatorDirection = shouldRaise
        if (mElevationAnimator?.isRunning != true) {
            if (targetView.elevation != mStatedElevation[shouldRaise]) {
                mElevationAnimator = ObjectAnimator.ofFloat(
                        targetView,
                        "elevation",
                        mStatedElevation.getValue(!shouldRaise),
                        mStatedElevation.getValue(shouldRaise)
                ).also {
                    it.duration = mDuration
                    it.start()
                }
            }
        }
    }

}