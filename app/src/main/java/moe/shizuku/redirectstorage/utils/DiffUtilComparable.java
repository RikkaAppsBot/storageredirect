package moe.shizuku.redirectstorage.utils;

public interface DiffUtilComparable {

    boolean itemSamesTo(Object other);

    boolean contentSamesTo(Object other);
}
