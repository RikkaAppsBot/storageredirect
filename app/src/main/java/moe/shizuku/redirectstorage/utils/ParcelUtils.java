package moe.shizuku.redirectstorage.utils;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class ParcelUtils {

    @Nullable
    public static <T extends Parcelable> T readNullableObject(Parcel in, ClassLoader classLoader) {
        if (Boolean.TRUE.equals(booleanFromInt(in.readByte()))) {
            return in.readParcelable(classLoader);
        }
        return null;
    }

    public static void writeNullableObject(Parcel out, @Nullable Parcelable object, int flags) {
        out.writeInt(booleanToInt(object == null));
        if (object != null) {
            out.writeParcelable(object, flags);
        }
    }

    /**
     * Convert int value to nullable boolean
     *
     * @param value int value (0 -> null; 1 -> false; 2 -> true)
     * @return Nullable boolean object
     */
    @Nullable
    public static Boolean booleanFromInt(int value) {
        return value == 0 ? null : (value == 2);
    }

    /**
     * Covnert nullable boolean to int value
     *
     * @param boolObj Nullable boolean object
     * @return Int value (null -> 0; false -> 1; true -> 2)
     */
    public static int booleanToInt(@Nullable Boolean boolObj) {
        return boolObj == null ? 0 : (!boolObj ? 1 : 2);
    }
}
