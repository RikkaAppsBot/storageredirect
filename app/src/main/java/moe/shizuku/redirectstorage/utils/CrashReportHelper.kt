package moe.shizuku.redirectstorage.utils

import android.annotation.SuppressLint
import android.app.Application
import android.os.Build
import android.os.UserManager
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.microsoft.appcenter.crashes.ingestion.models.ErrorAttachmentLog
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.app.Settings.preferences
import java.util.*

@SuppressLint("ObsoleteSdkInt")
object CrashReportHelper {

    private var shouldDisableCrashReport = false

    init {
        val isUsingHijackedZygoteInit: Boolean
        try {
            throw RuntimeException()
        } catch (e: RuntimeException) {
            val trace = e.stackTrace
            isUsingHijackedZygoteInit = "com.android.internal.os.ZygoteInit" != trace[trace.size - 1].className
        }
        shouldDisableCrashReport = (BuildConfig.DEBUG // debug
                || Build.VERSION.SDK_INT < 23 // ignore min API
                || isUsingHijackedZygoteInit) // maybe xposed
    }

    fun init(context: Application) {
        if (shouldDisableCrashReport) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (context.getSystemService(UserManager::class.java)?.isUserUnlocked != true) {
                return
            }
            try {
                val secureContext = context.createDeviceProtectedStorageContext()
                context.moveSharedPreferencesFrom(secureContext, "AppCenter")
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
        if (!preferences.getBoolean("enable_crash_report", true)) {
            Crashes.setEnabled(false)
            Analytics.setEnabled(false)
        }
        AppCenter.start(context, "e8deac73-0462-45c2-88a9-589ac0500244", Analytics::class.java, Crashes::class.java)
    }

    @JvmStatic
    @JvmOverloads
    fun logException(throwable: Throwable?, message: String? = null, log: List<String?>? = null) {
        if (shouldDisableCrashReport) {
            return
        }
        Crashes.isEnabled().thenAccept { enabled: Boolean ->
            if (enabled) {
                var properties: MutableMap<String?, String?>? = null
                if (message != null) {
                    properties = HashMap()
                    properties["message"] = message
                }
                var attachments: List<ErrorAttachmentLog?>? = null
                if (log != null) {
                    val sb = StringBuilder()
                    for (line in log) {
                        sb.append(line).append('\n')
                    }
                    attachments = listOf(ErrorAttachmentLog.attachmentWithText(sb.toString().trim { it <= ' ' }, "log.txt"))
                }
                Crashes.trackError(throwable, properties, attachments)
            }
        }
    }
}