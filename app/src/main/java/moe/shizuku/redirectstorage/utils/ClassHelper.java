package moe.shizuku.redirectstorage.utils;

import java.lang.reflect.Field;

public class ClassHelper {

    private static Field classLoaderField;

    static {
        try {
            //noinspection JavaReflectionMemberAccess
            classLoaderField = Class.class.getDeclaredField("classLoader");
            classLoaderField.setAccessible(true);
        } catch (Throwable tr) {
            tr.printStackTrace();
        }
    }

    public static void setClassLoader(Class cls, ClassLoader classLoader) {
        try {
            classLoaderField.set(cls, classLoader);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}