package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.DeviceSpecificSettings;
import moe.shizuku.redirectstorage.ktx.PackageInfosKt;
import moe.shizuku.redirectstorage.license.AlipayHelper;
import moe.shizuku.redirectstorage.license.GoogleBillingHelper;
import moe.shizuku.redirectstorage.license.RedeemHelper;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import rikka.material.help.InfoHelper;

public class SRInfoHelper extends InfoHelper {

    private final Context mContext;

    public SRInfoHelper(Context context) {
        super(context);

        mContext = context;
    }

    public StringBuilder getInfo() {
        String versionName;
        long versionCode;
        try {
            PackageInfo pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            versionName = pi.versionName;
            versionCode = PackageInfosKt.getLongVersionCodeCompat(pi);
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "(null)";
            versionCode = -1;
        }

        StringBuilder sb = super.getInfo(versionName, (int) versionCode);

        SRManager srm;
        if ((srm = SRManager.create()) != null) {
            try {
                int version = srm.getVersion();
                sb.append("Service version: ").append(version).append("\n");
                ModuleStatus riru = srm.getRiruStatus();
                sb.append("Riru version: ").append(Riru.versionName(mContext, riru)).append("\n");
                ModuleStatus module = srm.getModuleStatus();
                sb.append("Enhance module version: ").append(ModuleUtils.versionName(mContext, module)).append("\n");
            } catch (Throwable ignored) {
            }
        } else {
            sb.append("Service version: service is not running").append("\n");
        }

        String googleOrderId = GoogleBillingHelper.getOrderId();
        if (!TextUtils.isEmpty(googleOrderId)) {
            sb.append("Google order id: ").append(googleOrderId).append("\n");
        }
        String alipayOriderId = AlipayHelper.getOrderId();
        if (!TextUtils.isEmpty(alipayOriderId)) {
            sb.append("Alipay order id: ").append(alipayOriderId).append("\n");
        }
        String redeemCode = RedeemHelper.getRedeemCode();
        if (!TextUtils.isEmpty(redeemCode)) {
            sb.append("Redeem code: ").append(redeemCode).append("\n");
        }
        sb.append("Instance id: ").append(DeviceSpecificSettings.getInstanceId()).append("\n");
        return sb;
    }
}
