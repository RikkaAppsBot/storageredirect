package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import rikka.internal.help.HelpEntity;
import rikka.internal.help.HelpProvider;

public class ModuleUtils {

    public static final int V18 = 18;
    public static final int V18_1 = 19;
    public static final int V19 = 20;
    public static final int V19_1 = 21;
    public static final int V19_2 = 22;
    public static final int V19_3 = 23;
    public static final int V19_4 = 24;
    public static final int V19_5 = 25;
    public static final int V19_6 = 26;
    public static final int V19_7 = 27;
    public static final int V20_0_B = 28;
    public static final int V20_0 = 29;
    public static final int V20_1 = 30;
    public static final int V20_2 = 31;
    public static final int V20_3 = 32;
    public static final int V21_0 = 33;
    public static final int V21_1 = 34;
    public static final int V22_0 = 37;
    public static final int V22_1 = 38;
    public static final int V23_0_B1 = 48;

    public static String versionName(Context context, ModuleStatus status) {
        if (status != null && !TextUtils.isEmpty(status.getVersionName())) {
            return status.getVersionName();
        }
        int versionCode = status != null ? status.getVersionCode() : -1;
        return versionName(context, versionCode);
    }

    public static String versionName(Context context, int versionCode) {
        if (versionCode == -5) {
            return context.getString(R.string.riru_unknown_version, 0);
        }

        if (versionCode <= 0) {
            return context.getString(R.string.riru_not_found);
        }

        switch (versionCode) {
            case V23_0_B1:
                return "v23.0-beta01";
            case V22_1:
                return "v22.1";
            case V22_0:
                return "v22.0";
            case 35:
            case 36:
            case V21_1:
                return "v21.1";
            case V21_0:
                return "v21.0";
            case V20_3:
                return "v20.3";
            case V20_2:
                return "v20.2";
            case V20_1:
                return "v20.1";
            case V20_0:
                return "v20.0";
            case V20_0_B:
                return "v20.0 (beta)";
            case V19_7:
                return "v19.7";
            case V19_6:
                return "v19.6";
            case V19_5:
                return "v19.5";
            case V19_4:
                return "v19.4";
            case V19_3:
                return "v19.3";
            case V19_2:
                return "v19.2";
            case V19_1:
                return "v19.1";
            case V19:
                return "v19";
            case V18_1:
                return "v18.1";
            default:
                return context.getString(R.string.riru_unknown_version, versionCode);
        }
    }

    public static boolean showDialogWhenVersionTooLow(Context context, int current, int required) {
        if (current >= required) {
            return false;
        }
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.toast_enhance_module_version_too_low, versionName(context, required)))
                .setPositiveButton(android.R.string.ok, null)
                .setNeutralButton(R.string.dialog_module_version_too_low_button_upgrade, (dialog, which) -> {
                    showInstall(context);
                })
                .setCancelable(false)
                .show();
        return true;
    }

    public static boolean showDialogWhenAndroidVersionTooLow(Context context, int required, String name) {
        if (Build.VERSION.SDK_INT >= required) {
            return false;
        }
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.toast_enhance_module_android_version_too_low, name))
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(false)
                .show();
        return true;
    }

    public static void showInstall(Context context) {
        HelpEntity entity = HelpProvider.get(context, "enhanced_mode_install");
        if (entity == null) {
            Toast.makeText(context, R.string.toast_cannot_load_help, Toast.LENGTH_SHORT).show();
        } else {
            entity.startActivity(context);
        }
    }

    public static void showWhy(Context context) {
        HelpEntity entity = HelpProvider.get(context, "enhanced_mode_why");
        if (entity == null) {
            Toast.makeText(context, R.string.toast_cannot_load_help, Toast.LENGTH_SHORT).show();
        } else {
            entity.startActivity(context);
        }
    }
}
