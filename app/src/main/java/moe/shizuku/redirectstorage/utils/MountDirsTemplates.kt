package moe.shizuku.redirectstorage.utils

import android.content.Context
import moe.shizuku.redirectstorage.MountDirsTemplate
import moe.shizuku.redirectstorage.R

fun MountDirsTemplate.getTitle(context: Context): String {
    if (title != null && title.isNotBlank())
        return title

    return when {
        MountDirsTemplate.DEFAULT_ID == id -> context.getString(R.string.mount_dirs_template_default_title)
        MountDirsTemplate.BUILT_IN_PHOTOS_ID == id -> context.getString(R.string.mount_dirs_template_built_in_photos_title)
        MountDirsTemplate.BUILT_IN_MUSIC_ID == id -> context.getString(R.string.mount_dirs_template_built_in_music_title)
        MountDirsTemplate.BUILT_IN_DOWNLOADS_ID == id -> context.getString(R.string.mount_dirs_template_built_in_downloads_title)
        MountDirsTemplate.BUILT_IN_VIDEO_ID == id -> context.getString(R.string.mount_dirs_template_built_in_video_title)
        else -> title
    }
}