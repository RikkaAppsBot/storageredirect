package moe.shizuku.redirectstorage.utils;

import android.os.Build;
import android.text.TextUtils;

import java.util.regex.Pattern;

public class RomUtils {

    private static final boolean IS_SAMSUNG = Build.MANUFACTURER.equalsIgnoreCase("Samsung");

    private static final boolean IS_MEIZU = Build.FINGERPRINT.contains("Flyme")
            || Pattern.compile("Flyme", Pattern.CASE_INSENSITIVE).matcher(Build.DISPLAY).find();

    private static final boolean IS_EMUI = !TextUtils.isEmpty(BuildUtils.getProperty("ro.build.version.emui"));

    private static final boolean IS_MIUI = !TextUtils.isEmpty(BuildUtils.getProperty("ro.miui.ui.version.name"));

    public static boolean isSamsung() {
        return IS_SAMSUNG;
    }

    public static boolean isMeizu() {
        return IS_MEIZU;
    }

    public static boolean isHuawei() {
        return IS_EMUI;
    }

    public static boolean isMiui() {
        return IS_MIUI;
    }
}
