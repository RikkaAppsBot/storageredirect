package moe.shizuku.redirectstorage.utils;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import moe.shizuku.redirectstorage.app.AppActivity;
import rikka.material.app.AppBar;
import rikka.material.app.AppBarOwner;

public class SubtitleListener extends RecyclerView.OnScrollListener {

    public static SubtitleListener attach(final RecyclerView recyclerView, Fragment fragment, CharSequence subtitle, final @Nullable Bundle savedInstanceState) {
        final SubtitleListener listener = new SubtitleListener(fragment, 0, subtitle);
        recyclerView.post(() -> {
            listener.onCreate(savedInstanceState);
            recyclerView.addOnScrollListener(listener);
            listener.updateState(recyclerView);
        });
        return listener;
    }

    private Fragment mFragment;
    private CharSequence mSubtitle;

    private boolean mShowingHeader;

    public SubtitleListener(Fragment fragment, int headerHeight, CharSequence subtitle) {
        mFragment = fragment;
        mSubtitle = subtitle;
        mShowingHeader = true;
    }

    public void setSubtitle(CharSequence subtitle) {
        mSubtitle = subtitle;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        syncViewsState();
    }

    private void applySubtitle(CharSequence subtitle) {
        AppActivity activity = ((AppActivity) mFragment.getActivity());
        if (activity != null) {
             if (activity.getSupportActionBar() != null) {
                 activity.getSupportActionBar().setSubtitle(subtitle);
             } else if (activity instanceof AppBarOwner) {
                 AppBar appBar = ((AppBarOwner) activity).getAppBar();
                 if (appBar != null) {
                     appBar.setSubtitle(subtitle);
                 }
             }
        }
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        updateState(recyclerView);
    }

    private void setShowingHeader(boolean showingHeader) {
        if (mShowingHeader != showingHeader) {
            mShowingHeader = showingHeader;
            syncViewsState();
        }
    }

    private void updateState(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            setShowingHeader(0 == ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition());
        }
    }

    private void syncViewsState() {
        applySubtitle(mShowingHeader ? null : mSubtitle);
    }

    public void onSavedInstanceState(Bundle outState) {
    }
}
