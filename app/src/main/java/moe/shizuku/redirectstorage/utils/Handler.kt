package moe.shizuku.redirectstorage.utils

import android.os.Handler
import android.os.Looper
import rikka.core.ktx.unsafeLazy

val mainHandler by unsafeLazy {
    Handler(Looper.getMainLooper())
}