package moe.shizuku.redirectstorage.utils

import moe.shizuku.redirectstorage.app.Settings
import moe.shizuku.redirectstorage.app.Settings.SortAccording.Companion.ACCORDING_INSTALL_TIME
import moe.shizuku.redirectstorage.app.Settings.SortAccording.Companion.ACCORDING_NAME
import moe.shizuku.redirectstorage.app.Settings.SortAccording.Companion.ACCORDING_UPDATE_TIME
import moe.shizuku.redirectstorage.model.AppInfo
import rikka.core.util.AppNameComparator
import java.util.*

class AppInfoComparator(
        private val mAccording: Int, private val mEnabledFirst: Boolean) : Comparator<AppInfo> {

    companion object {

        @JvmStatic
        fun createFromSRSettings(): AppInfoComparator {
            return AppInfoComparator(Settings.sortAccording, Settings.isSortEnabledFirst)
        }

    }

    private val mAppNameComparator: AppNameComparator<AppInfo>
            = AppNameComparator(AppInfoProvider())

    override fun compare(a: AppInfo, b: AppInfo): Int {
        return when {
            mEnabledFirst && a.isRedirectEnabled && !b.isRedirectEnabled -> -1
            mEnabledFirst && !a.isRedirectEnabled && b.isRedirectEnabled -> 1
            mAccording == ACCORDING_INSTALL_TIME -> b.packageInfo.firstInstallTime.compareTo(a.packageInfo.firstInstallTime)
            mAccording == ACCORDING_UPDATE_TIME -> b.packageInfo.lastUpdateTime.compareTo(a.packageInfo.lastUpdateTime)
            mAccording == ACCORDING_NAME -> mAppNameComparator.compare(a, b)
            else -> mAppNameComparator.compare(a, b)
        }
    }

    private class AppInfoProvider : AppNameComparator.InfoProvider<AppInfo> {

        override fun getTitle(item: AppInfo): CharSequence {
            return item.label?:item.packageName
        }

        override fun getPackageName(item: AppInfo): String {
            return item.packageName
        }

        override fun getUserId(item: AppInfo): Int {
            return item.userId
        }

    }

}
