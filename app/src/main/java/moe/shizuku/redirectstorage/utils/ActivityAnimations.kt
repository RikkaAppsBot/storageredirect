package moe.shizuku.redirectstorage.utils

import android.annotation.SuppressLint
import android.content.Context
import android.view.animation.Animation
import android.view.animation.AnimationUtils

object ActivityAnimations {

    private lateinit var context: Context

    private val ids = ArrayList<Int>(4)

    @JvmStatic
    @SuppressLint("ResourceType")
    fun init(context: Context) {
        this.context = context
        val a = context.obtainStyledAttributes(null, intArrayOf(
                android.R.attr.activityOpenEnterAnimation,
                android.R.attr.activityOpenExitAnimation,
                android.R.attr.activityCloseEnterAnimation,
                android.R.attr.activityCloseExitAnimation
        ), android.R.attr.windowAnimationStyle, 0)
        if (a.indexCount == 4) {
            ids.add(a.getResourceId(0, 0))
            ids.add(a.getResourceId(1, 0))
            ids.add(a.getResourceId(2, 0))
            ids.add(a.getResourceId(3, 0))
        }
        a.recycle()
    }

    private fun load(index: Int): Animation? {
        if (ids[index] == 0) {
            return null
        }
        return try {
            AnimationUtils.loadAnimation(context, ids[index])
        } catch (e: Throwable) {
            null
        }
    }

    val openEnter: Animation?
        get() {
            return load(0)
        }

    val openExit: Animation?
        get() {
            return load(1)
        }

    val closeEnter: Animation?
        get() {
            return load(2)
        }

    val closeExit: Animation?
        get() {
            return load(3)
        }
}