package moe.shizuku.redirectstorage.utils;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Process;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import moe.shizuku.redirectstorage.R;
import rikka.html.text.HtmlCompat;

public class ForegroundService {

    public static boolean checkAppOps(@NonNull Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return true;
        }

        AppOpsManager appOps = context.getSystemService(AppOpsManager.class);
        if (appOps != null) {
            int mode = AppOpsManager.MODE_ALLOWED;
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    mode = appOps.unsafeCheckOpNoThrow("android:start_foreground",
                            Process.myUid(), context.getPackageName());
                } else {
                    //noinspection deprecation
                    mode = appOps.checkOpNoThrow("android:start_foreground",
                            Process.myUid(), context.getPackageName());
                }
            } catch (Throwable ignored) {
            }
            return mode == AppOpsManager.MODE_ALLOWED;
        }
        return true;
    }

    public static void showDialog(@NonNull Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(R.string.dialog_start_foreground_denied_title)
                .setMessage(HtmlCompat.fromHtml(activity.getString(R.string.dialog_start_foreground_denied_message)))
                .setPositiveButton(R.string.exit_app, (dialogInterface, i) -> {
                    activity.finishAffinity();
                    System.exit(0);
                })
                .setCancelable(false)
                .show();
    }

    public static void startOrNotify(@NonNull Context context, Intent intent) {
        if (checkAppOps(context)) {
            try {
                ContextCompat.startForegroundService(context, intent);
            } catch (SecurityException e) {
                e.printStackTrace();

                if (e.getMessage() != null && e.getMessage().contains("app is automatically launch")) {
                    Toast.makeText(context, context.getString(R.string.toast_start_foreground_error), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            if (context instanceof Activity) {
                showDialog((Activity) context);
            } else {
                Toast.makeText(context, context.getString(R.string.toast_start_foreground_denied), Toast.LENGTH_LONG).show();
            }
        }
    }
}
