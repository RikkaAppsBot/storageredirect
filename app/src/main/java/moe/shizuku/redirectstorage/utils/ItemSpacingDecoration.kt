package moe.shizuku.redirectstorage.utils

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.RecyclerView.State

class ItemSpacingDecoration(context: Context, dp: Int = 8) : ItemDecoration() {

    private val padding: Int = (context.resources.displayMetrics.density * dp).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
        outRect.bottom = padding
        if (parent.layoutManager?.getPosition(view) == 0) {
            outRect.top = padding
        }
    }
}