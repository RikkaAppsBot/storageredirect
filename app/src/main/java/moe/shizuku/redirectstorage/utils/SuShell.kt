package moe.shizuku.redirectstorage.utils

import com.topjohnwu.superuser.CallbackList
import com.topjohnwu.superuser.Shell
import moe.shizuku.redirectstorage.suiAvailable

class SuShell(private val requestCode: Int) {

    companion object {

        @JvmStatic
        @Synchronized
        fun available(): Boolean {
            return suiAvailable || Shell.rootAccess()
        }
    }

    interface Callback {
        fun onLine(line: String?)
        fun onResult(code: Int)
    }

    private var suiShell: SuiShell? = null
    private var suShell: Shell? = null
    private var isClosed = false

    fun run(command: String, callback: Callback) {
        run(arrayOf(command), callback)
    }

    fun run(commands: List<String>, callback: Callback) {
        run(commands.toTypedArray(), callback)
    }

    fun run(commands: Array<String>, callback: Callback) {
        if (suiAvailable) {
            val shell = SuiShell(commands, requestCode)
            shell.callback(object : SuiShell.Callback {

                override fun onShouldShowRequestPermissionRationale() {
                    callback.onResult(Int.MIN_VALUE)
                }

                override fun onNewLine(line: String) {
                    callback.onLine(line)
                }

                override fun onExit(code: Int) {
                    callback.onResult(code)
                }
            })
            shell.submit()

            suiShell = shell
        } else {
            val shell = Shell.Builder.create()
                    .setFlags(Shell.FLAG_REDIRECT_STDERR)
                    .setTimeout(10)
                    .build()

            if (shell.status != Shell.ROOT_SHELL) {
                callback.onResult(Int.MIN_VALUE)
                return
            }

            shell.newJob()
                    .add(*commands)
                    .to(object : CallbackList<String>() {
                        override fun onAddElement(s: String) {
                            callback.onLine(s);
                        }
                    })
                    .submit {
                        callback.onResult(it.code);
                    }

            suShell = shell
        }

    }

    fun close() {
        if (isClosed) return

        isClosed = true
        try {
            suiShell?.close()
            suShell?.close()
        } catch (e: Throwable) {

        }
    }
}