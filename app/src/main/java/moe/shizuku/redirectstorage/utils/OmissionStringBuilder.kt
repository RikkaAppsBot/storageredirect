package moe.shizuku.redirectstorage.utils

import android.content.Context
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import moe.shizuku.redirectstorage.R

class OmissionStringBuilder(private val context: Context) {

    private var maxCount: Int = 3

    private var format: Int = R.string.omission_text_format

    private var omitted: Int = R.plurals.folder_amount_text_format

    private var empty: Int = R.string.mount_dirs_template_none_title

    private var items = ArrayList<Item>()

    fun maxCount(maxCount: Int) = apply {
        this.maxCount = maxCount
    }

    fun format(@StringRes format: Int) = apply {
        this.format = format
    }

    fun omitted(@PluralsRes omitted: Int) = apply {
        this.omitted = omitted
    }

    fun empty(@StringRes format: Int) = apply {
        this.empty = format
    }

    @JvmOverloads
    fun append(string: String, separator: String = context.getString(R.string.comma)) = apply {
        items.add(Item(string, separator))
    }

    fun build(): String {
        val size = items.size
        if (size == 0) {
            return context.getString(empty)
        }

        // show full string if items size is 1 more than max
        val maxCount = if (size == maxCount + 1) maxCount + 1 else maxCount

        val sb = StringBuilder()
        run {
            items.forEachIndexed { index, item ->
                sb.append(item.string)
                if (index == maxCount - 1 || index == size - 1) return@run
                sb.append(item.separator)
            }
        }
        val string = sb.toString()
        if (size > maxCount) {
            val other = context.resources.getQuantityString(omitted, size - maxCount, size - maxCount)
            return context.getString(format, string, other)
        }
        return string
    }

    internal data class Item(val string: String, val separator: String)
}

