package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;

/**
 * Created by fytho on 2018/1/3.
 */

public final class ViewUtils {

    private ViewUtils() {
    }

    public static void setPaddingStart(View v, @Px int paddingStart) {
        v.setPaddingRelative(paddingStart, v.getPaddingTop(), v.getPaddingEnd(), v.getPaddingBottom());
    }

    public static void setPaddingEnd(View v, @Px int paddingEnd) {
        v.setPaddingRelative(v.getPaddingStart(), v.getPaddingTop(), paddingEnd, v.getPaddingBottom());
    }

    public static void setPaddingTop(View v, @Px int paddingTop) {
        v.setPaddingRelative(v.getPaddingStart(), paddingTop, v.getPaddingEnd(), v.getPaddingBottom());
    }

    public static void setPaddingBottom(View v, @Px int paddingBottom) {
        v.setPaddingRelative(v.getPaddingStart(), v.getPaddingTop(), v.getPaddingEnd(), paddingBottom);
    }

    public static void setPaddingVertical(View v, @Px int paddingVertical) {
        v.setPaddingRelative(v.getPaddingStart(), paddingVertical, v.getPaddingEnd(), paddingVertical);
    }

    public static void setPaddingHorizontal(View v, @Px int paddingHorizontal) {
        v.setPaddingRelative(paddingHorizontal, v.getTop(), paddingHorizontal, v.getBottom());
    }

    public static void setMarginStart(@NonNull View v, @Px int marginStart) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        marginLayoutParams.setMarginStart(marginStart);
    }

    public static void setMarginEnd(@NonNull View v, @Px int marginEnd) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        marginLayoutParams.setMarginEnd(marginEnd);
    }

    public static void setMarginTop(@NonNull View v, @Px int marginTop) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        marginLayoutParams.topMargin = marginTop;
    }

    public static void setMarginBottom(@NonNull View v, @Px int marginBottom) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        marginLayoutParams.bottomMargin = marginBottom;
    }

    public static void setMarginVertical(@NonNull View v, @Px int marginVertical) {
        setMarginStart(v, marginVertical);
        setMarginEnd(v, marginVertical);
    }

    public static void setMarginHorizontal(@NonNull View v, @Px int marginHorizontal) {
        setMarginStart(v, marginHorizontal);
        setMarginEnd(v, marginHorizontal);
    }

    /**
     * Measure content width from ListAdapter
     *
     * @param context     Context
     * @param listAdapter The adapter that should be measured
     * @return Recommend popup window width
     */
    public static int measureContentWidth(Context context, ListAdapter listAdapter) {
        ViewGroup mMeasureParent = null;
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;

        final ListAdapter adapter = listAdapter;
        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = adapter.getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(context);
            }

            itemView = adapter.getView(i, itemView, mMeasureParent);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();

            if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    public static void setVisibleOrGone(@NonNull View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public static void setCompoundDrawableStartWithIntrinsicBounds(@NonNull TextView view, @Nullable Drawable newValue) {
        final Drawable[] drawables = view.getCompoundDrawablesRelative();
        view.setCompoundDrawablesRelativeWithIntrinsicBounds(newValue, drawables[1], drawables[2], drawables[3]);
    }

    public static void setCompoundDrawableStartWithIntrinsicBounds(@NonNull TextView view, @DrawableRes int newValue) {
        setCompoundDrawableStartWithIntrinsicBounds(view, view.getResources().getDrawable(newValue, view.getContext().getTheme()));
    }

    public static void hideKeyboard(@NonNull View view) {
        final InputMethodManager imm = (InputMethodManager)
                view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
