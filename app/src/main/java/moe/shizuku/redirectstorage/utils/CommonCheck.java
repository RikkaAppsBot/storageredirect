package moe.shizuku.redirectstorage.utils;

import android.os.DeadObjectException;
import android.os.Environment;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.lang.EmulatedStorageNotFoundException;
import moe.shizuku.redirectstorage.lang.InvalidLicenseException;
import moe.shizuku.redirectstorage.lang.ModuleInstallationException;
import moe.shizuku.redirectstorage.lang.NoLogDetectedException;
import moe.shizuku.redirectstorage.lang.ServerVersionMismatchException;

public class CommonCheck {

    // logcat_thread.cpp
    private static final int NO_LOG_CHECK_INTERVAL = 120;

    public static SRManager getSRManagerWithCheck() throws Exception {
        if (!Settings.isIgnoreUnsupportedStorage()) {
            String real = Environment.getExternalStorageDirectory().getAbsolutePath();
            String expect = String.format(Constants.STORAGE_PATH_FORMAT, UserHelper.myUserId());
            if (!real.equals(expect)) {
                throw new EmulatedStorageNotFoundException(expect, real);
            }
        }

        SRManager srm = SRManager.create();

        if (srm == null) {
            // other case always notify user "service not running"
            throw new DeadObjectException("service not running");
        }

        int version = srm.getVersion();
        if (version != Constants.SERVER_VERSION) {
            throw new ServerVersionMismatchException(version);
        }

        // check Riru module
        int moduleStatus = srm.getModuleStatus().getVersionCode();
        if (!Settings.isIgnoreModuleIssue()
                && (moduleStatus == SRManager.MODULE_NO_64BIT_LIB
                || moduleStatus == SRManager.MODULE_NOT_IN_MEMORY
                || moduleStatus == SRManager.MODULE_NOT_IN_64_MEMORY)) {
            throw new ModuleInstallationException(moduleStatus);
        }

        // check license
        int res = srm.isLicenseValid();
        if (res != 0) {
            throw new InvalidLicenseException(res);
        }

        if (moduleStatus == SRManager.MODULE_NOT_INSTALLED) {
            if (!Settings.isIgnoreNoLog()) {
                if (srm.isNoLogDetected()) {
                    throw new NoLogDetectedException(NO_LOG_CHECK_INTERVAL);
                }
            }
        }

        return srm;
    }
}
