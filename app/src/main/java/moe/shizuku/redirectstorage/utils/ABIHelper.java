package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;

import java.io.IOException;
import java.util.Objects;

import moe.shizuku.redirectstorage.BuildConfig;

public class ABIHelper {

    private static String ABI;

    public static String getABI(Context context) {
        if (BuildConfig.DEBUG) {
            return Build.SUPPORTED_ABIS[0];
        }
        if (ABI != null) {
            return ABI;
        }
        try {
            AssetManager assetManager = context.getAssets();
            String[] names = assetManager.list("");
            Objects.requireNonNull(names, "can't list '.' in assets");
            for (String name : names) {
                switch (name) {
                    case "armeabi-v7a":
                    case "arm64-v8a":
                    case "x86":
                    case "x86_64":
                        ABI = name;
                        return name;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        throw new IllegalStateException("can't find native files from assets");
    }
}
