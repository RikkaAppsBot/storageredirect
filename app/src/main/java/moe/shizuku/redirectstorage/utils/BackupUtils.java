package moe.shizuku.redirectstorage.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.app.Settings;

public final class BackupUtils {

    private static final String KEY_PRICE = "gp_price";
    private static final String KEY_ORDER_ID = "google_order_id";
    private static final String KEY_TOKEN = "gt"; // google_token
    private static final String KEY_SKU_TITLE = "google_unlock_title";
    private static final String KEY_SKU_DESC = "google_unlock_description";
    private static final String KEY_PURCHASE_TIME = "google_purchase_time";

    private static final List<String> SHARED_PREFS_KEY_BLACKLIST = Arrays.asList(
            KEY_PRICE,
            KEY_ORDER_ID,
            KEY_TOKEN,
            KEY_SKU_TITLE,
            KEY_SKU_DESC,
            KEY_PURCHASE_TIME,
            Settings.IGNORE_NO_LOG,
            Settings.IGNORE_UNSUPPORTED_STORAGE,
            Settings.IGNORE_UNSUPPORTED_SU
    );

    public static boolean shouldIgnoreSharedPrefsItem(@Nullable String key) {
        return SHARED_PREFS_KEY_BLACKLIST.contains(key);
    }

    public static String readString(@NonNull InputStream in) throws IOException {
        byte[] json;
        byte[] magic = new byte[AppConstants.BACKUP_MAGIC.length];

        int length;
        length = in.read(magic, 0, magic.length);
        if (length == -1 || !Arrays.equals(magic, AppConstants.BACKUP_MAGIC)) {
            throw new IllegalStateException("not a sr backup");
        }

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] b = new byte[8192];
        while ((length = in.read(b)) != -1) {
            result.write(b, 0, length);
        }

        json = result.toByteArray();
        for (int i = 0; i < json.length; i++) {
            json[i] ^= AppConstants.BACKUP_XOR;
        }

        return new String(json, StandardCharsets.UTF_8);
    }

}
