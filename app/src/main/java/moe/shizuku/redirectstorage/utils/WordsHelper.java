package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.os.RemoteException;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.List;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SRManager;

public class WordsHelper {

    @NonNull
    public static String getComma(@NonNull Context context) {
        return context.getString(R.string.comma);
    }

    @NonNull
    public static String getSemicolon(@NonNull Context context) {
        return context.getString(R.string.semicolon);
    }

    public static String getHumanReadableRedirectTargetTitle(Context context, RedirectPackageInfo info) {
        return getHumanReadableRedirectTargetTitle(context, info, true);
    }

    public static String getHumanReadableRedirectTargetTitle(Context context, RedirectPackageInfo info, boolean showFollowDefault) {
        String res;
        boolean followDefault = true;


        String target = null;
        SRManager sm = SRManager.create();
        if (sm != null) {
            try {
                target = sm.getDefaultRedirectTarget();
            } catch (RemoteException e) {
                // should never happened
            }
        }

        if (info != null && info.redirectTarget != null && !TextUtils.isEmpty(info.redirectTarget)) {
            target = info.redirectTarget;
            followDefault = false;
        }

        if (target == null) {
            target = "(unknown)";
        }

        switch (target) {
            case Constants.REDIRECT_TARGET_DATA:
                res = context.getString(R.string.redirect_target_data_title);
                break;
            case Constants.REDIRECT_TARGET_CACHE:
                res = context.getString(R.string.redirect_target_cache_title);
                break;
            default:
                res = target;
                break;
        }

        if (followDefault && showFollowDefault) {
            res = context.getString(R.string.redirect_target_default_title_format, res);
        }

        return res;
    }

    @NonNull
    public static String getFoldersAmountText(@NonNull Context context, int amount) {
        return context.getResources().getQuantityString(R.plurals.folder_amount_text_format, amount, amount);
    }

    @NonNull
    public static String buildOmissibleFoldersListText(@NonNull Context context, @NonNull List<String> items, int maxDisplayCount) {
        if (items.isEmpty()) {
            return getFoldersAmountText(context, 0);
        }
        return buildOmissibleText(context, items, context.getString(R.string.omissible_text_format, getFoldersAmountText(context, items.size()), "%s"), maxDisplayCount);
    }

    @NonNull
    public static String buildOmissibleText(@NonNull Context context, @NonNull List<String> items, @NonNull String omittedFormat, int maxDisplayCount) {
        final String comma = getComma(context);
        final StringBuilder sb = new StringBuilder();
        if (items.size() > 0) {
            for (int i = 0; i < items.size() && i < maxDisplayCount; i++) {
                sb.append(items.get(i)).append(comma);
            }
            sb.delete(sb.length() - comma.length(), sb.length());
        }
        if (items.size() > maxDisplayCount) {
            return String.format(omittedFormat, sb.toString());
        } else {
            return sb.toString();
        }
    }
}
