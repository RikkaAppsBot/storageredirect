package moe.shizuku.redirectstorage.utils

import android.content.ContentUris
import android.content.Intent
import android.content.res.AssetFileDescriptor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.IBinder
import android.provider.MediaStore.Images
import android.util.Log
import moe.shizuku.redirectstorage.AppConstants.TAG
import moe.shizuku.redirectstorage.BuildConfig
import moe.shizuku.redirectstorage.SRManager
import moe.shizuku.redirectstorage.component.gallery.model.GalleryFolderSimpleItem
import moe.shizuku.redirectstorage.component.gallery.model.PreviewModel
import moe.shizuku.redirectstorage.content.ContentProviders
import moe.shizuku.redirectstorage.model.MediaStoreImageRow
import moe.shizuku.redirectstorage.provider.RemoteProvider
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*
import kotlin.math.max

object MediaStoreUtils {

    private var original: IBinder? = null
    private var wrapped: RemoteProvider? = null

    private fun acquireProvider(srm: SRManager): RemoteProvider? {
        if (original == null || !original!!.isBinderAlive) {
            original = ContentProviders.getProviderBinder(BuildConfig.APPLICATION_ID, "media", false)
        }
        if (wrapped == null || wrapped!!.remote?.asBinder()?.isBinderAlive == false) {
            try {
                wrapped = srm.wrapContentProvider(original)
            } catch (tr: Throwable) {
                tr.printStackTrace()
            }
        }
        return wrapped
    }

    @JvmStatic
    fun listImages(userId: Int, sortOrder: String = "${Images.Media.DATE_ADDED} DESC"): List<MediaStoreImageRow>? {
        val srm: SRManager = SRManager.create() ?: return null
        val provider: RemoteProvider? = acquireProvider(srm) ?: return null
        val cursor = provider!!.query(Images.Media.EXTERNAL_CONTENT_URI, MediaStoreImageRow.PROJECTION,
                null, null, sortOrder)
        if (cursor != null) {
            try {
                val list = mutableListOf<MediaStoreImageRow>()
                if (cursor.moveToFirst()) {
                    do {
                        list.add(MediaStoreImageRow.fromCursor(cursor))
                    } while (cursor.moveToNext())
                }
                return list
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    cursor.close()
                } catch (e: Exception) {
                }
            }
        }

        /*try {
            srm.releaseContentProvider("media", "media")
        } catch (tr: Throwable) {
            tr.printStackTrace()
        }*/

        return null
    }

    @JvmStatic
    fun groupByFolder(list: List<MediaStoreImageRow>, userId: Int): List<GalleryFolderSimpleItem> {
        return list.groupBy {
            it.bucketId
        }.map { (_, list) ->
            val folderPath = list.first().path.let { it.substring(0, it.lastIndexOf('/')) }
            GalleryFolderSimpleItem(
                    folderName = list.first().bucketName ?: folderPath.split('/').last(),
                    userId = userId,
                    path = folderPath,
                    totalCount = list.size,
                    previewItems = list
            )
        }
    }

    @JvmStatic
    fun groupByDate(list: List<MediaStoreImageRow>): List<Any> {
        return list.sortedByDescending { it.dateLastModified }
                .groupBy {
                    Date(it.dateLastModified * 1000).apply {
                        hours = 0
                        minutes = 0
                        seconds = 0
                    }
                }
                .flatMap { (time, list) ->
                    mutableListOf(time) +
                            list.mapIndexed { index, item ->
                                PreviewModel(item, orderInCategory = index)
                            }
                }
    }

    @JvmStatic
    fun findSampleSize(viewSize: Int, imageWidth: Int, imageHeight: Int): Int {
        if (viewSize == 0) {
            Log.w(TAG, "findSampleSize with viewSize 0")
            return 1
        }

        val imageSize = max(imageWidth, imageHeight)
        var res = 1
        while (imageSize > viewSize * res) {
            res *= 2
        }
        return res
    }

    @JvmStatic
    fun getThumbnail(imageId: Long, previewSize: Int): Bitmap? {
        val srm: SRManager = SRManager.create() ?: return null
        val provider: RemoteProvider? = acquireProvider(srm) ?: return null

        var row: MediaStoreImageRow? = null
        val cursor = provider!!.query(Images.Thumbnails.EXTERNAL_CONTENT_URI, MediaStoreImageRow.THUMBNAILS_PROJECTION,
                Images.Thumbnails.IMAGE_ID + " = " + imageId + " AND " +
                        Images.Thumbnails.KIND + " = " + Images.Thumbnails.MINI_KIND,
                null, null)
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    row = MediaStoreImageRow.fromCursor(cursor)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    cursor.close()
                } catch (e: Exception) {
                }
            }
        } else {
            return null
        }

        var bitmap: Bitmap? = null
        var thumbUri: Uri? = null
        var pfdInput: AssetFileDescriptor? = null
        var inOpt: BitmapFactory.Options? = null

        if (row == null) {
            try {
                thumbUri = ContentUris.withAppendedId(Images.Media.EXTERNAL_CONTENT_URI, imageId)
                pfdInput = provider.openAssetFile(thumbUri!!, "r")
                pfdInput?.use {
                    val input = FileInputStream(it.fileDescriptor)
                    val outOpt = BitmapFactory.Options().apply {
                        inJustDecodeBounds = true
                    }
                    BitmapFactory.decodeStream(input, null, outOpt)
                    val sampleSize = findSampleSize(previewSize, outOpt.outWidth, outOpt.outHeight)
                    inOpt = BitmapFactory.Options().apply {
                        inSampleSize = sampleSize
                    }
                    input.close()
                }
            } catch (ex: FileNotFoundException) {
                Log.e(TAG, "couldn't open $thumbUri; $ex")
            } catch (ex: IOException) {
                Log.e(TAG, "couldn't open $thumbUri; $ex")
            } catch (ex: OutOfMemoryError) {
                Log.e(TAG, "failed to allocate memory for "
                        + thumbUri + "; " + ex)
            } catch (ex: Throwable) {
                Log.e(TAG, "couldn't open $thumbUri; $ex")
            } finally {
                try {
                    pfdInput?.close()
                } catch (e: Exception) {
                }
            }
        }

        try {
            thumbUri = if (row != null) ContentUris.withAppendedId(Images.Thumbnails.EXTERNAL_CONTENT_URI, row.id) else ContentUris.withAppendedId(Images.Media.EXTERNAL_CONTENT_URI, imageId)
            pfdInput = provider.openAssetFile(thumbUri!!, "r")

            if (pfdInput != null) {
                bitmap = BitmapFactory.decodeFileDescriptor(
                        pfdInput.fileDescriptor, null, inOpt)
            }
        } catch (ex: FileNotFoundException) {
            Log.e(TAG, "couldn't open thumbnail $thumbUri; $ex")
        } catch (ex: IOException) {
            Log.e(TAG, "couldn't open thumbnail $thumbUri; $ex")
        } catch (ex: OutOfMemoryError) {
            Log.e(TAG, "failed to allocate memory for thumbnail "
                    + thumbUri + "; " + ex)
        } catch (ex: Throwable) {
            Log.e(TAG, "couldn't open thumbnail $thumbUri; $ex")
        } finally {
            try {
                pfdInput?.close()
            } catch (e: Exception) {
            }
        }

        /*try {
            srm.releaseContentProvider("media", "media")
        } catch (tr: Throwable) {
            tr.printStackTrace()
        }*/

        return bitmap
    }

    @JvmStatic
    fun queryThumbnail(imageId: Long): MediaStoreImageRow? {
        val srm: SRManager = SRManager.create() ?: return null
        val provider: RemoteProvider? = acquireProvider(srm) ?: return null

        val cursor = provider!!.query(Images.Thumbnails.EXTERNAL_CONTENT_URI, MediaStoreImageRow.THUMBNAILS_PROJECTION,
                Images.Thumbnails.IMAGE_ID + " = " + imageId + " AND " +
                        Images.Thumbnails.KIND + " = " + Images.Thumbnails.MINI_KIND,
                null, null)
        if (cursor != null) {
            try {
                if (cursor.moveToNext()) {
                    return MediaStoreImageRow.fromCursor(cursor)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    cursor.close()
                } catch (e: Exception) {
                }
            }
        }

        /*try {
            srm.releaseContentProvider("media", "media")
        } catch (tr: Throwable) {
            tr.printStackTrace()
        }*/

        return null
    }

    @JvmStatic
    fun getViewIntent(imageId: Long): Intent {
        return Intent(Intent.ACTION_VIEW, Uri.parse("content://media/external/images/media/$imageId"))
    }

}