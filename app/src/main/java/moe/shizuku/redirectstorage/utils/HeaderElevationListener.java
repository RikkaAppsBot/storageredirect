package moe.shizuku.redirectstorage.utils;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import moe.shizuku.redirectstorage.app.AppActivity;


public class HeaderElevationListener extends RecyclerView.OnScrollListener {

    public static HeaderElevationListener attach(final RecyclerView recyclerView, Fragment fragment, CharSequence subtitle, final @Nullable Bundle savedInstanceState) {
        final HeaderElevationListener listener = new HeaderElevationListener(fragment, 0, subtitle);
        recyclerView.post(() -> {
            if (recyclerView.getChildCount() > 0 && recyclerView.computeVerticalScrollOffset() == 0) {
                listener.setHeaderHeight(recyclerView.getChildAt(0).getHeight());
                listener.onCreate(savedInstanceState);
                recyclerView.addOnScrollListener(listener);
            }
        });
        return listener;
    }

    public static HeaderElevationListener attachWithCustomView(
            final RecyclerView recyclerView,
            View customHeader,
            Fragment fragment,
            CharSequence subtitle,
            final @Nullable Bundle savedInstanceState
    ) {
        final HeaderElevationListener listener = new HeaderElevationListener(fragment, customHeader, 0, subtitle);
        recyclerView.post(() -> {
            if (recyclerView.getChildCount() > 0 && recyclerView.computeVerticalScrollOffset() == 0) {
                listener.onCreate(savedInstanceState);
                recyclerView.addOnScrollListener(listener);
            }
        });
        return listener;
    }

    private Fragment mFragment;
    private float mDefaultElevation;
    private final CharSequence mSubtitle;

    private final View mCustomHeader;

    private int mHeaderHeight;
    private boolean mShowingHeader;
    private int mScrollY;

    private static final String HEADER_HEIGHT_TAG = "headerElevationListener:headerHeight";
    private static final String SHOWING_HEADER_TAG = "headerElevationListener:showingHeader";
    private static final String SCROLL_Y_TAG = "headerElevationListener:scrollY";

    public HeaderElevationListener(Fragment fragment, int headerHeight, CharSequence subtitle) {
        this(fragment, null, headerHeight, subtitle);
    }

    public HeaderElevationListener(Fragment fragment, @Nullable View customHeader,
                                   int headerHeight, CharSequence subtitle) {
        mFragment = fragment;
        mHeaderHeight = headerHeight;
        mSubtitle = subtitle;
        mScrollY = 0;
        mShowingHeader = true;
        mCustomHeader = customHeader;
        if (mCustomHeader != null) {
            mDefaultElevation = mCustomHeader.getElevation();
        } else if (mFragment.getActivity() != null && ((AppActivity) mFragment.getActivity()).getSupportActionBar() != null) {
            mDefaultElevation = ((AppActivity) mFragment.getActivity()).getSupportActionBar().getElevation();
        } else {
            mDefaultElevation = 0;
        }

        setElevation(0);
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mHeaderHeight = savedInstanceState.getInt(HEADER_HEIGHT_TAG);
            mShowingHeader = savedInstanceState.getBoolean(SHOWING_HEADER_TAG);
        }
        syncViewsState();
    }

    private void setHeaderHeight(int headerHeight) {
        mHeaderHeight = headerHeight;
        syncViewsState();
    }

    private void setElevation(float elevation) {
        if (mCustomHeader != null) {
            mCustomHeader.setElevation(elevation);
        } else if (mFragment.getActivity() != null && ((AppActivity) mFragment.getActivity()).getSupportActionBar() != null) {
            ((AppActivity) mFragment.getActivity()).getSupportActionBar().setElevation(elevation);
        }
    }

    private void setSubtitle(CharSequence subtitle) {
        if (mFragment.getActivity() != null && ((AppActivity) mFragment.getActivity()).getSupportActionBar() != null) {
            ((AppActivity) mFragment.getActivity()).getSupportActionBar().setSubtitle(subtitle);
        }
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        mScrollY += dy;
        syncViewsState();
    }

    private void syncViewsState() {
        boolean n = mScrollY <= mHeaderHeight;
        if (n != mShowingHeader) {
            mShowingHeader = n;

            setElevation(mShowingHeader ? 0 : mDefaultElevation);
            setSubtitle(mShowingHeader ? null : mSubtitle);
        }
    }

    public void onSavedInstanceState(Bundle outState) {
        outState.putInt(HEADER_HEIGHT_TAG, mHeaderHeight);
        outState.putBoolean(SHOWING_HEADER_TAG, mShowingHeader);
    }
}
