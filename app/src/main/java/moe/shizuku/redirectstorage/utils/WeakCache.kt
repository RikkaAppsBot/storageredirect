package moe.shizuku.redirectstorage.utils

import java.lang.ref.WeakReference
import java.util.concurrent.ConcurrentHashMap

class WeakCache<K, P, V>(
        private val keyFunc: (parameter: P) -> K,
        private val valueProducer: (parameter: P) -> V
) {
    private val map = ConcurrentHashMap<K, WeakReference<V>>()

    operator fun get(parameter: P): V {
        val key = keyFunc(parameter)
        var value = map[key]?.get()
        if (value == null) {
            value = valueProducer(parameter)
            map[key] = WeakReference(value)
        }
        return value!!
    }

    operator fun set(parameter: P, value: V) {
        val key = keyFunc(parameter)
        map[key] = WeakReference(value)
    }

    fun clear() {
        map.clear()
    }
}