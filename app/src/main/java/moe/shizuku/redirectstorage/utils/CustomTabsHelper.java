package moe.shizuku.redirectstorage.utils;

import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import androidx.browser.customtabs.CustomTabsIntent;

import moe.shizuku.redirectstorage.HelpActivity;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.drawable.DrawableUtils;
import rikka.core.util.ResourceUtils;

/**
 * Created by fytho on 2017/12/15.
 */

public class CustomTabsHelper {

    public static CustomTabsIntent.Builder createBuilder(Context context) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(
                ResourceUtils.resolveColor(context.getTheme(), android.R.attr.colorPrimary));
        builder.setShowTitle(true);
        return builder;
    }

    public static boolean launchHelp(Context context, Uri uri) {
        CustomTabsIntent.Builder builder = createBuilder(context);

        Drawable directoryIcon = context.getDrawable(R.drawable.ic_list_24dp).mutate();
        directoryIcon.setTint(ResourceUtils.resolveColor(
                context.getTheme(), android.R.attr.actionMenuTextColor));
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0,
                new Intent(context, HelpActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK),
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setActionButton(
                DrawableUtils.toBitmap(directoryIcon),
                context.getResources().getString(R.string.action_help_directory),
                pendingIntent,
                true);

        Uri.Builder uriBuilder = uri.buildUpon();
        if ((context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_YES) > 0) {
            uriBuilder.appendQueryParameter("night", "1");
        }

        return launchUrl(context, builder.build(), uriBuilder.build());
    }

    public static boolean launchUrl(Context context, Uri uri) {
        return launchUrl(context, createBuilder(context).build(), uri);
    }

    private static boolean launchUrl(Context context, CustomTabsIntent customTabsIntent, Uri uri) {
        try {
            customTabsIntent.launchUrl(context, uri);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}