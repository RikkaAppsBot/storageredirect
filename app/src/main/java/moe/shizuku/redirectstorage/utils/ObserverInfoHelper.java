package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.text.TextUtils;

import moe.shizuku.redirectstorage.ObserverInfo;
import rikka.core.util.ResourceUtils;

/**
 * Created by rikka on 2017/11/24.
 */

public class ObserverInfoHelper {

    public static String loadDescription(Context context, ObserverInfo oi) {
        if (TextUtils.isEmpty(oi.summary)) {
            return "";
        }

        return ResourceUtils.resolveString(context.getResources(), "description_" + oi.summary, oi.summary);
    }
}
