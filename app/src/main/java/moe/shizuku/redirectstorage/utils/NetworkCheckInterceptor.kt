package moe.shizuku.redirectstorage.utils

import moe.shizuku.redirectstorage.app.Settings
import okhttp3.Interceptor
import okhttp3.Response

class NetworkCheckInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            //throw java.net.ConnectException("Failed to connect to api.rikka.app/127.0.0.1:443")
            return chain.proceed(chain.request())
        } catch (e: Exception) {
            if (e.message?.contains("${chain.request().url.host}/127.") == true
                    || e.message?.contains("${chain.request().url.host}/0.") == true) {
                Settings.preferences.edit().putBoolean("db", true).apply()
            }
            throw e
        }
    }
}