package moe.shizuku.redirectstorage.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;

import static android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
import static android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED;
import static android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED_USER;
import static android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
import static moe.shizuku.redirectstorage.AppConstants.TAG;

public class PackageManagerUtils {

    public static boolean isPackageEnabled(Context context, String packageName) {
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(packageName, 0);
            return ai.enabled;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isComponentEnabled(@NonNull Context context,
                                             @NonNull ComponentName componentName) {
        try {
            final int res = context.getPackageManager().getComponentEnabledSetting(componentName);
            return res != COMPONENT_ENABLED_STATE_DISABLED &&
                    res != COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED &&
                    res != COMPONENT_ENABLED_STATE_DISABLED_USER;
        } catch (Exception e) {
            return false;
        }
    }

    public static void setComponentEnabled(@NonNull Context context,
                                           @NonNull ComponentName componentName,
                                           boolean enabled, int flags) {
        try {
            context.getPackageManager().setComponentEnabledSetting(componentName,
                    enabled ? COMPONENT_ENABLED_STATE_ENABLED : COMPONENT_ENABLED_STATE_DISABLED,
                    flags);
        } catch (Exception e) {
            Log.e(TAG,
                    "Failed to set up component enabled for " + componentName.flattenToString(), e);
        }
    }

    public static void setComponentEnabled(@NonNull Context context,
                                           @NonNull ComponentName componentName,
                                           boolean enable) {
        setComponentEnabled(context, componentName, enable, PackageManager.DONT_KILL_APP);
    }
}
