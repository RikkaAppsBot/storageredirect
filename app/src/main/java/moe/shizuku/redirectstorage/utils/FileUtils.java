package moe.shizuku.redirectstorage.utils;

import java.io.File;

public class FileUtils {

    public static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    public static boolean isChildOf(String parent, String child) {
        if (parent.equals(child)) {
            return true;
        }
        if (!parent.endsWith("/")) {
            parent += "/";
        }
        if (!child.endsWith("/")) {
            child += "/";
        }
        return canonicalize(child).startsWith(canonicalize(parent));
    }
}
