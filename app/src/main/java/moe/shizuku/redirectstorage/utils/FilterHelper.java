package moe.shizuku.redirectstorage.utils;

import android.util.SparseBooleanArray;
import android.util.SparseIntArray;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class FilterHelper<T> {

    private Filter mFilter;

    private List<T> mFilteredItems;

    private String mKeyword;
    private boolean mIsSearching;
    private SparseIntArray mIntKeys;
    private SparseBooleanArray mBooleanKeys;

    private FilterItemsProvider<T> mCallback;

    public FilterHelper(@NonNull FilterItemsProvider<T> callback, @NonNull Filter filter) {
        mCallback = callback;
        mFilter = filter;
        mIntKeys = new SparseIntArray();
        mBooleanKeys = new SparseBooleanArray();

        mFilteredItems = new ArrayList<>();
    }

    public List<T> getFilteredItems() {
        return mFilteredItems;
    }

    public void setKeyword(String keyword) {
        if (mKeyword != null && mKeyword.equals(keyword)) {
            return;
        }

        mKeyword = keyword;
        filter();
    }

    public void setSearching(boolean searching) {
        mIsSearching = searching;
        filter();
        mCallback.onFilterFinish(mFilteredItems);
    }

    public void putKey(int key, boolean value) {
        putKey(key, value, true);
    }

    public void putKey(int key, boolean value, boolean notify) {
        mBooleanKeys.put(key, value);
        filter(notify);
    }

    public void putKey(int key, int value) {
        putKey(key, value, true);
    }

    public void putKey(int key, int value, boolean notify) {
        mIntKeys.put(key, value);
        filter(notify);
    }

    public List<T> filter() {
        return filter(true);
    }

    @SuppressWarnings("unchecked")
    public List<T> filter(boolean notify) {
        mFilteredItems.clear();

        final List<T> originalItems = mCallback.onFilterStart();
        for (int position = 0; position < originalItems.size(); position++) {
            T obj = originalItems.get(position);

            boolean add = true;
            for (int i = 0; i < mIntKeys.size(); i++) {
                int key = mIntKeys.keyAt(i);
                if (!mFilter.filter(key, mIntKeys.get(key), obj)) {
                    add = false;
                    break;
                }
            }
            for (int i = 0; i < mBooleanKeys.size(); i++) {
                int key = mBooleanKeys.keyAt(i);
                if (!mFilter.filter(key, mBooleanKeys.get(key), obj)) {
                    add = false;
                    break;
                }
            }
            if (!add) {
                continue;
            }

            if (mIsSearching && !mFilter.contains(mKeyword, obj)) {
                continue;
            }

            mFilteredItems.add(obj);
        }

        if (notify) {
            mCallback.onFilterFinish(mFilteredItems);
        }
        return mFilteredItems;
    }

    public static class Filter<T> {

        public boolean contains(String keyword, T obj) {
            return true;
        }

        public boolean filter(int key, int value, T obj) {
            return true;
        }

        public boolean filter(int key, boolean value, T obj) {
            return true;
        }
    }

    public interface FilterItemsProvider<T> {

        List<T> onFilterStart();

        void onFilterFinish(List<T> items);

    }
}
