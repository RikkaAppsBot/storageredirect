package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.system.ErrnoException;
import android.system.Os;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.ktx.ContextKt;

public class ServerBootHelper {

    private static String PATH;
    private static String STARTER_PATH;

    private static final String STARTER_NAME = "starter";
    private static final String DAEMON_NAME = "daemon";

    private static File getParent(Context context) {
        return ContextKt.createDeviceProtectedStorageContextCompatWhenLocked(context).getFilesDir();
    }

    private static boolean cleanOldFiles(File parent, List<String> excludeFileNames) {
        Log.v(AppConstants.TAG, "Cleaning old server dex");

        final File[] children = parent.listFiles((dir, name) ->
                name.endsWith(".dex") || name.startsWith("daemon-v"));
        for (File file : Optional.ofNullable(children).orElse(new File[0])) {
            if (excludeFileNames.stream().anyMatch(e -> file.getName().equals(e))) {
                continue;
            }
            if (!file.delete()) {
                Log.v(AppConstants.TAG, "Failed to clean " + file.getAbsolutePath() + ". Ignored.");
                // return false;
            }
        }

        return true;
    }

    private static boolean writeAsset(Context context, String source, File target) {
        Log.v(AppConstants.TAG, "Writing " + target);

        try {
            InputStream is = context.getAssets().open(source);
            OutputStream os = new FileOutputStream(target);

            rikka.core.os.FileUtils.copy(is, os);

            os.flush();
            os.close();
            is.close();

            try {
                Os.chmod(target.getAbsolutePath(), 0755);
            } catch (ErrnoException e) {
                Log.w(AppConstants.TAG, "Can't chmod 0755 " + target, e);
            }

            return true;
        } catch (IOException e) {
            Log.w(AppConstants.TAG, "Can't write " + target, e);

            return false;
        }
    }

    private static boolean writeLib(Context context, String name, File target) {
        Log.v(AppConstants.TAG, "Writing " + target);

        File source = new File(context.getApplicationInfo().nativeLibraryDir, name);
        if (!source.exists()) {
            Log.e(AppConstants.TAG, "Lib not exists " + source);
            return false;
        }

        try {
            InputStream is = new FileInputStream(source);
            OutputStream os = new FileOutputStream(target);

            rikka.core.os.FileUtils.copy(is, os);

            os.flush();
            os.close();
            is.close();

            try {
                Os.chmod(target.getAbsolutePath(), 0755);
            } catch (ErrnoException e) {
                Log.w(AppConstants.TAG, "Can't chmod 0755 " + target, e);
            }

            return true;
        } catch (IOException e) {
            Log.w(AppConstants.TAG, "Can't write " + target, e);
            return false;
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static boolean copyFiles(Context context) {
        File parent = getParent(context);

        PATH = parent.getAbsolutePath();
        STARTER_PATH = PATH + "/starter";

        if (BuildConfig.DEBUG) {
            Log.d(Constants.TAG, "cmd: " + getCmd(false)[0]);
        }

        // write files
        boolean res = true;
        res &= cleanOldFiles(parent, Collections.singletonList(DAEMON_NAME));

        String abi = ABIHelper.getABI(context);
        res &= writeAsset(context, abi + File.separator + STARTER_NAME, new File(parent, STARTER_NAME));
        res &= writeAsset(context, abi + File.separator + DAEMON_NAME, new File(parent, DAEMON_NAME));
        res &= writeAsset(context, abi + File.separator + "libhelper.so", new File(parent, "libhelper.so"));
        res &= writeAsset(context, "main.dex", new File(parent, "main.dex"));

        return res;
    }

    public static String[] getCmd(boolean fromUI) {
        return new String[]{
                String.format("%s --bin-path=%s%s", ServerBootHelper.STARTER_PATH, ServerBootHelper.PATH, fromUI ? " --from-ui" : "")};
    }
}
