package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.text.TextUtils;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.ModuleStatus;

public class Riru {

    private static final int V19_6 = 28;
    private static final int V19_5 = 27;
    private static final int V19_4 = 26;
    private static final int V19_3 = 25;
    private static final int V19_2 = 24;
    private static final int V19_1 = 22;
    private static final int V19 = 20;
    private static final int V18 = 19;
    private static final int V17_1 = 18;

    public static String versionName(Context context, ModuleStatus status) {
        if (status != null && !TextUtils.isEmpty(status.getVersionName())) {
            return status.getVersionName();
        }

        int versionCode = status != null ? status.getVersionCode() : -1;

        if (versionCode == -5) {
            return context.getString(R.string.riru_unknown_version, 0);
        }

        if (versionCode <= 0) {
            return context.getString(R.string.riru_not_found);
        }

        switch (versionCode) {
            case V19_6:
                return "v19.6";
            case V19_5:
                return "v19.5";
            case V19_4:
                return "v19.4";
            case V19_3:
                return "v19.3";
            case V19_2:
                return "v19.2";
            case V19_1:
                return "v19.1";
            case V19:
                return "v19";
            case V18:
                return "v18";
            case V17_1:
                return "v17.1";
            default:
                return context.getString(R.string.riru_unknown_version, versionCode);
        }
    }
}
