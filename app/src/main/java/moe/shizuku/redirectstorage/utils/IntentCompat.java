package moe.shizuku.redirectstorage.utils;

public final class IntentCompat {

    private IntentCompat() {}

    public static final String ACTION_SHOW_APP_INFO = "android.intent.action.SHOW_APP_INFO";

    public static final String EXTRA_PACKAGE_NAME = "android.intent.extra.PACKAGE_NAME";

}
