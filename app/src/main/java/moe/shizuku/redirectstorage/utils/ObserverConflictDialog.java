package moe.shizuku.redirectstorage.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.dialog.AlertDialogFragment;
import rikka.html.text.HtmlCompat;

public class ObserverConflictDialog {

    private static String loadLabel(Context context, String packageName, int userId) {
        SRManager srm = SRManager.create();
        if (srm == null)
            return packageName;

        try {
            ApplicationInfo ai = srm.getApplicationInfo(packageName, 0, userId);
            if (ai != null) {
                return ai.loadLabel(context.getPackageManager()).toString();
            }
        } catch (Throwable tr) {
            tr.printStackTrace();
        }
        return packageName;
    }

    public static void showSourceTargetConflict(@NonNull Context context, @NonNull FragmentManager fragmentManager, @NonNull ObserverInfo s, @NonNull ObserverInfo t) {
        new AlertDialogFragment.Builder()
                .setTitle(context.getString(R.string.data_verifier_conflict_detected_title))
                .setMessage(HtmlCompat.fromHtml(context.getString(R.string.data_verifier_conflict_detected_source_and_target, s.source, s.target, loadLabel(context, t.packageName, t.userId), t.source, t.target), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(context.getString(android.R.string.ok), null)
                .build()
                .show(fragmentManager);
    }

    public static void showSourceConflict(@NonNull Context context, @NonNull FragmentManager fragmentManager, @NonNull ObserverInfo s, @NonNull ObserverInfo t) {
        new AlertDialogFragment.Builder()
                .setTitle(context.getString(R.string.data_verifier_conflict_detected_title))
                .setMessage(HtmlCompat.fromHtml(context.getString(R.string.data_verifier_conflict_detected_source, s.source, loadLabel(context, t.packageName, t.userId), t.source, t.target), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(context.getString(android.R.string.ok), null)
                .build()
                .show(fragmentManager);
    }

    public static void showTargetConflict(@NonNull Context activity, @NonNull FragmentManager fragmentManager, @NonNull ObserverInfo s, @NonNull ObserverInfo t) {
        new AlertDialogFragment.Builder()
                .setTitle(activity.getString(R.string.data_verifier_conflict_detected_title))
                .setMessage(HtmlCompat.fromHtml(activity.getString(R.string.data_verifier_conflict_detected_target, s.target, loadLabel(activity, t.packageName, t.userId), t.source, t.target), HtmlCompat.FROM_HTML_OPTION_TRIM_WHITESPACE))
                .setPositiveButton(activity.getString(android.R.string.ok), null)
                .build()
                .show(fragmentManager);
    }
}
