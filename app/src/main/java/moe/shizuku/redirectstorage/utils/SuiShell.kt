package moe.shizuku.redirectstorage.utils

import android.content.pm.PackageManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import rikka.shizuku.Shizuku
import rikka.shizuku.ShizukuRemoteProcess
import java.io.Closeable

class SuiShell(private val commands: Array<String>, private val requestCode: Int) : Closeable {

    interface Callback {

        fun onShouldShowRequestPermissionRationale()

        fun onNewLine(line: String)

        fun onExit(code: Int)
    }

    private var callback: Callback? = null

    fun callback(callback: Callback) = apply {
        this.callback = callback
    }

    fun submit() {
        if (Shizuku.checkSelfPermission() != PackageManager.PERMISSION_GRANTED) {
            if (Shizuku.shouldShowRequestPermissionRationale()) {
                callback?.onShouldShowRequestPermissionRationale()
                return
            }

            Shizuku.requestPermission(requestCode)
            Shizuku.addRequestPermissionResultListener(object : Shizuku.OnRequestPermissionResultListener {
                override fun onRequestPermissionResult(requestCode: Int, grantResult: Int) {
                    if (requestCode == this@SuiShell.requestCode) {
                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            runAsync()
                        } else {
                            callback?.onShouldShowRequestPermissionRationale()
                        }
                        Shizuku.removeRequestPermissionResultListener(this)
                    }
                }
            })
            return
        }

        runAsync()
    }

    private fun runAsync() {
        GlobalScope.launch(Dispatchers.IO) {
            runShell()
        }
    }

    private var shell: ShizukuRemoteProcess? = null

    private fun runShell() {
        val shell = Shizuku.newProcess(arrayOf("sh"), null, null)
        val input = shell.inputStream
        val output = shell.outputStream

        this.shell = shell

        commands.forEach {
            output.write(it.toByteArray())
        }
        output.close()

        input.bufferedReader().use {
            while (true) {
                val line = it.readLine() ?: break
                callback?.onNewLine(line)
            }
        }
        input.close()

        val code = shell.waitFor()
        callback?.onExit(code)
        shell.destroy()
    }

    override fun close() {
        shell?.destroy()
    }
}