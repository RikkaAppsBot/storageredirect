package moe.shizuku.redirectstorage.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.File;
import java.util.Objects;

import moe.shizuku.redirectstorage.AppConstants;
import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SRManager;
import moe.shizuku.redirectstorage.app.BaseIntentService;
import moe.shizuku.redirectstorage.app.Settings;
import moe.shizuku.redirectstorage.drawable.DrawableUtils;
import moe.shizuku.redirectstorage.event.Events;
import moe.shizuku.redirectstorage.utils.BuildUtils;
import moe.shizuku.redirectstorage.utils.FileBrowserUtils;
import moe.shizuku.redirectstorage.utils.ForegroundService;
import moe.shizuku.redirectstorage.utils.NotificationHelper;
import moe.shizuku.redirectstorage.utils.UserHandleUtils;
import moe.shizuku.redirectstorage.utils.UserHelper;

import static moe.shizuku.redirectstorage.AppConstants.ACTION_ADD_TO_DOWNLOADS;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_PACKAGE_NAME;
import static moe.shizuku.redirectstorage.AppConstants.EXTRA_USER_ID;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_DOWNLOAD;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_PACKAGE_ADDED;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_CHANNEL_WORK_SERVICE;
import static moe.shizuku.redirectstorage.AppConstants.NOTIFICATION_ID_WORKING;
import static moe.shizuku.redirectstorage.AppConstants.TAG;
import static moe.shizuku.redirectstorage.Constants.ACTION_PACKAGE_ADDED;
import static moe.shizuku.redirectstorage.Constants.ACTION_PACKAGE_REMOVED;
import static moe.shizuku.redirectstorage.Constants.EXTRA_MIN_COMPATIBLE_VERSION;
import static moe.shizuku.redirectstorage.Constants.EXTRA_PACKAGE_INFO;
import static moe.shizuku.redirectstorage.Constants.EXTRA_PATH;
import static moe.shizuku.redirectstorage.app.ServiceKtxKt.startForegroundOrNotify;
import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class WorkService extends BaseIntentService {

    public static void start(Context context, Intent originalIntent) {
        ForegroundService.startOrNotify(context, new Intent(originalIntent)
                .setComponent(ComponentName.createRelative(Constants.APPLICATION_ID, WorkService.class.getName())));
    }

    private static boolean checkServerVersion(Intent intent, String reason) {
        int version = intent.getIntExtra(EXTRA_MIN_COMPATIBLE_VERSION, -1);
        if (version == -1 || version > Constants.MIN_COMPATIBLE_VERSION) {
            LOGGER.w("%s: server compatible version %d dose not match client %s", reason, version, Constants.MIN_COMPATIBLE_VERSION);
            return false;
        }
        return true;
    }

    public static final String ACTION_APPLY_APP_CONFIG = BuildConfig.APPLICATION_ID + ".action.APPLY_APP_CONFIG";

    public WorkService() {
        super("WorkService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_WORK_SERVICE, getString(R.string.notification_channel_work_service), NotificationManager.IMPORTANCE_MIN);
                channel.setShowBadge(false);
                if (BuildUtils.atLeast29()) {
                    channel.setAllowBubbles(false);
                }

                notificationManager.createNotificationChannel(channel);
            }
        }

        NotificationCompat.Builder notification = NotificationHelper
                .create(this, NOTIFICATION_CHANNEL_WORK_SERVICE, R.string.work_service_notification_title)
                .setOngoing(true);

        startForegroundOrNotify(this, NOTIFICATION_ID_WORKING, notification.build());
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_ADD_TO_DOWNLOADS.equals(action)) {
                handleAddToDownloads(intent);
            } else if (ACTION_PACKAGE_ADDED.equals(action)) {
                handlePackageAdded(intent);
            } else if (ACTION_PACKAGE_REMOVED.equals(action)) {
                handlePackageRemoved(intent);
            } else if ((NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action) || Intent.ACTION_VIEW.equals(action)) &&
                    (intent.getData() != null && "storage-redirect-client".equals(intent.getData().getScheme()))) {
                handleStorageRedirectClientUri(intent);
            } else if (ACTION_APPLY_APP_CONFIG.equals(action)) {
                handleApplyAppConfig(intent);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopForeground(true);
    }

    private void handlePackageRemoved(Intent intent) {
        if (!checkServerVersion(intent, "handlePackageRemoved")) {
            return;
        }

        String packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);
        if (packageName == null) {
            LOGGER.w("handlePackageRemoved: packageName is null");
            return;
        }

        int userId = intent.getIntExtra(EXTRA_USER_ID, -1);
        if (userId == -1) {
            LOGGER.w("handlePackageRemoved: userId is null");
            return;
        }

        Events.postPackageInstallationEvent(ev -> {
            ev.onPackageRemoved(packageName, userId);
            return null;
        });

        Context context = this;
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        if (notificationManager == null) {
            LOGGER.w("handlePackageRemoved: can't get NotificationManager");
            return;
        }

        int code = Objects.hash(packageName, userId);
        notificationManager.cancel("package_added", code);
    }

    private void handlePackageAdded(Intent intent) {
        if (!checkServerVersion(intent, "handlePackageAdded")) {
            return;
        }

        Context context = this;

        PackageInfo pi = intent.getParcelableExtra(EXTRA_PACKAGE_INFO);
        if (pi == null) {
            LOGGER.w("handleAddToDownloads: packageInfo is null");
            return;
        }
        if (pi.applicationInfo == null) {
            LOGGER.w("handleAddToDownloads: applicationInfo is null");
            return;
        }

        String packageName = pi.packageName;
        int userId = UserHandleUtils.getUserId(pi.applicationInfo.uid);

        SRManager sm = SRManager.create();
        if (sm == null) {
            LOGGER.w("handleAddToDownloads: can't access service");
            return;
        }

        RedirectPackageInfo info;
        try {
            info = sm.getRedirectPackageInfo(packageName, SRManager.MATCH_ALL_PACKAGES | SRManager.GET_PACKAGE_INFO, userId);
        } catch (Throwable e) {
            LOGGER.w(e, "handleAddToDownloads: getRedirectPackageInfo");
            return;
        }

        if (info == null) {
            LOGGER.w("handleAddToDownloads: can't get info for %d:%s", userId, packageName);
            return;
        }

        Events.postPackageInstallationEvent(ev -> {
            ev.onPackageAdded(info);
            return null;
        });

        if (info.enabled) {
            LOGGER.v("%d:%s is already enabled, don't show notification", userId, packageName);
        }


        boolean showNotification = intent.getBooleanExtra(Constants.EXTRA_ENABLE_NOTIFICATION, true);
        if (showNotification && !info.enabled) {
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager == null) {
                Log.w(Constants.TAG, "Can't get NotificationManager");
                return;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_PACKAGE_ADDED, getString(R.string.channel_package_added), NotificationManager.IMPORTANCE_HIGH);
                channel.setSound(null, null);
                channel.setShowBadge(false);
                channel.setBypassDnd(true);
                if (BuildUtils.atLeast29()) {
                    channel.setAllowBubbles(false);
                }

                notificationManager.createNotificationChannel(channel);
            }

            String label;
            try {
                label = pi.applicationInfo.loadLabel(context.getPackageManager()).toString();
            } catch (Throwable tr) {
                Log.w(Constants.TAG, "loadLabel " + packageName + " " + userId);

                label = packageName;
            }

            if (UserHelper.myUserId() != userId) {
                label = getString(R.string.app_name_with_user, label, userId);
            }

            Drawable iconDrawable = null;
            try {
                iconDrawable = pi.applicationInfo.loadIcon(context.getPackageManager());
            } catch (Throwable tr) {
                Log.w(Constants.TAG, "loadIcon " + packageName + " " + userId);
            }
            Bitmap iconBitmap = null;
            if (iconDrawable != null) {
                try {
                    iconBitmap = DrawableUtils.toBitmap(iconDrawable);
                } catch (Exception e) {
                    Log.w(TAG, "Cannot get icon bitmap: " + packageName, e);
                }
            }

            int color = NotificationHelper.getDefaultColor(context);
            int code = Objects.hash(packageName, userId);

            PendingIntent contentIntent = PendingIntent.getActivity(context, code,
                    new Intent("android.intent.action.SHOW_APP_INFO")
                            .addCategory(Intent.CATEGORY_DEFAULT)
                            .setPackage(Constants.APPLICATION_ID)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                            .putExtra("android.intent.extra.PACKAGE_NAME", packageName)
                            .putExtra(AppConstants.EXTRA_USER_ID, userId),
                    PendingIntent.FLAG_ONE_SHOT);

            CharSequence text;
            /*if (info.enabled || info.mountDirs != null || info.redirectTarget != null) {
                text = getString(R.string.notification_old_config_restored);
            } else {
                text = getString(R.string.notification_tap_for_detail);
            }*/
            text = getString(R.string.notification_tap_for_detail);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_PACKAGE_ADDED)
                    .setContentTitle(getString(R.string.notification_app_installed, label))
                    .setContentText(text)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setColor(color)
                    .setLargeIcon(iconBitmap)
                    .setSmallIcon(R.drawable.ic_notification_24dp)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)
                    .setContentIntent(contentIntent)
                    .setAutoCancel(true);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                builder.setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSound(Uri.EMPTY)
                        .setVibrate(new long[0]);
            }

            notificationManager.notify("package_added", code, builder.build());
        }
    }

    private void handleAddToDownloads(Intent intent) {
        if (!checkServerVersion(intent, "handleAddToDownloads")) {
            return;
        }

        Context context = this;

        PackageInfo pi = intent.getParcelableExtra(EXTRA_PACKAGE_INFO);
        if (pi == null) {
            LOGGER.w("handleAddToDownloads: packageInfo is null");
            return;
        }
        if (pi.applicationInfo == null) {
            LOGGER.w("handleAddToDownloads: applicationInfo is null");
            return;
        }
        String filePath = intent.getStringExtra(EXTRA_PATH);
        if (filePath == null) {
            LOGGER.w("handleAddToDownloads: filePath is null");
            return;
        }

        File file = new File(filePath);
        String packageName = pi.packageName;
        int userId = UserHandleUtils.getUserId(pi.applicationInfo.uid);
        ApplicationInfo ai = pi.applicationInfo;

        CharSequence label;
        try {
            label = ai.loadLabel(getPackageManager());
        } catch (Throwable tr) {
            Log.w(Constants.TAG, "loadLabel " + packageName + " " + userId);

            label = packageName;
        }

        if (UserHelper.myUserId() != userId) {
            label = context.getString(R.string.app_name_with_user, label, userId);
        }

        String mimeType = null;
        String extension = null;
        String name = file.getName();
        int index = name.lastIndexOf('.');
        if (index != -1) {
            extension = name.substring(index + 1);
        }

        if (extension != null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        if (notificationManager == null) {
            Log.w(Constants.TAG, "Can't get NotificationManager");
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_DOWNLOAD, getString(R.string.channel_download), NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(null, null);
            channel.setShowBadge(false);
            channel.setBypassDnd(true);
            if (BuildUtils.atLeast29()) {
                channel.setAllowBubbles(false);
            }

            notificationManager.createNotificationChannel(channel);
        }

        int color = NotificationHelper.getDefaultColor(context);
        int code = Objects.hash(packageName, userId, file.getName(), mimeType);

        CharSequence subText;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            subText = new SpannableString(label);
            ((SpannableString) subText).setSpan(new ForegroundColorSpan(color), 0, subText.length(), 0);
        } else {
            // do not color for P+, because P don't color app name in notification
            subText = label;
        }

        PendingIntent contentIntent = PendingIntent.getActivity(context, code,
                FileBrowserUtils.getOpenFileIntent("sdcard", userId, file.getAbsolutePath(), null, mimeType, context.getString(R.string.dialog_open_file_chooser_title)),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_DOWNLOAD)
                .setContentTitle(getString(R.string.notification_downloaded, file.getName()))
                .setContentText(getString(R.string.notification_tap_to_open))
                .setColor(color)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setWhen(System.currentTimeMillis())
                .setShowWhen(true)
                .setSubText(subText)
                .setContentIntent(contentIntent)
                .setAutoCancel(false);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            builder.setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSound(Uri.EMPTY)
                    .setVibrate(new long[0]);
        }

        notificationManager.notify("add_to_downloads", code, builder.build());
    }

    private void handleStorageRedirectClientUri(@NonNull Intent intent) {
        final Uri uri = Objects.requireNonNull(intent.getData());

        /*if ("app-configuration".equals(uri.getAuthority())) {
            try {
                final AppConfiguration received = new Gson().fromJson(Base64.decodeToString(uri.getLastPathSegment()), AppConfiguration.class);

                final NotificationManager notificationManager = getSystemService(NotificationManager.class);
                if (notificationManager == null) {
                    Log.w(Constants.TAG, "Can't get NotificationManager");
                    return;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    final NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_RECEIVED_FROM_OTHER_DEVICES, getString(R.string.channel_received_from_other_devices), NotificationManager.IMPORTANCE_HIGH);
                    channel.setShowBadge(true);
                    if (BuildUtils.isQBeta4()) {
                        channel.setAllowBubbles(false);
                    }

                    notificationManager.createNotificationChannel(channel);
                }

                final Intent contentIntent = new Intent(this, ReceiverActivity.class);
                contentIntent.setAction(ACTION_APPLY_APP_CONFIG);
                contentIntent.putExtra(EXTRA_DATA, received);
                final PendingIntent pi = PendingIntent.getActivity(this, Objects.hashCode(received.getPackageName()), contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                // TODO How to select target user?

                int color = NotificationHelper.getDefaultColor(this);
                final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_RECEIVED_FROM_OTHER_DEVICES)
                        .setContentTitle(getString(R.string.share_configuration_notification_received_title, received.getPackageName()))
                        .setContentText(getString(R.string.share_configuration_notification_received_text))
                        .setColor(color)
                        .setSmallIcon(R.drawable.ic_notification_24dp)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pi)
                        .setShowWhen(true)
                        .setAutoCancel(true);

                notificationManager.notify("received_app_cfg", Objects.hashCode(received.getPackageName()), builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }

    private void handleApplyAppConfig(@NonNull Intent intent) {
        /*final AppConfiguration received = intent.getParcelableExtra(EXTRA_DATA);
        final int userId = intent.getIntExtra(EXTRA_USER_ID, UserHelper.myUserId());
        try {
            final SRManager srm = SRManager.createThrow();
            //noinspection ConstantConditions
            if (received.getRecommendedMountDirs() != null) {
                final RedirectPackageInfo info = srm.getRedirectPackageInfo(received.getPackageName(), 0, userId);
                final MountDirsConfig config = info != null ? info.mountDirsConfig : new MountDirsConfig();
                config.flags = MountDirsConfig.FLAG_ALLOW_CUSTOM;
                config.customDirs = new HashSet<>(received.getRecommendedMountDirs());
                srm.setMountDirsForPackage(config, received.getPackageName(), userId);
            }
            srm.setRedirectTargetForPackage(received.getRecommendedRedirectTarget(), received.getPackageName(), userId);
            for (ObserverInfo oi : Optional.safeList(received.getObservers())) {
                oi.userId = userId;
                oi.packageName = received.getPackageName();
                try {
                    srm.updateObserver(oi);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            new Handler(getMainLooper()).post(() -> {
                Toast.makeText(this, getString(R.string.toast_apply_configuration_success), Toast.LENGTH_LONG).show();
            });
        } catch (Exception e) {
            new Handler(getMainLooper()).post(() -> {
                Toast.makeText(this, getString(R.string.toast_apply_configuration_failed) + " " + e.getMessage(), Toast.LENGTH_LONG).show();
            });
        }*/
    }

}
