package moe.shizuku.redirectstorage.widget;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class DefaultRVAdapterDataObserver extends RecyclerView.AdapterDataObserver {

    @Nullable
    private Runnable mOnChangedCallback;

    public DefaultRVAdapterDataObserver() {
        mOnChangedCallback = null;
    }

    public DefaultRVAdapterDataObserver(@Nullable Runnable onChangedCallback) {
        mOnChangedCallback = onChangedCallback;
    }

    @Override
    public void onChanged() {
        if (mOnChangedCallback != null) {
            mOnChangedCallback.run();
        }
    }

    @Override
    public void onItemRangeChanged(int positionStart, int itemCount) {
        onChanged();
    }

    @Override
    public void onItemRangeInserted(int positionStart, int itemCount) {
        onChanged();
    }

    @Override
    public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
        onChanged();
    }

    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount) {
        onChanged();
    }

}
