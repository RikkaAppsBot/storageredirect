package moe.shizuku.redirectstorage.widget;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.BreadcrumbItem;
import moe.shizuku.redirectstorage.utils.ViewUtils;
import rikka.core.util.ResourceUtils;

class BreadcrumbsAdapter extends RecyclerView.Adapter<BreadcrumbsAdapter.ItemHolder> {

    private final int DROPDOWN_OFFSET_Y_FIX;

    private ArrayList<BreadcrumbItem> items;
    private BreadcrumbsCallback callback;

    private BreadcrumbsView parent;

    private int mPopupThemeId = -1;

    public BreadcrumbsAdapter(BreadcrumbsView parent) {
        this(parent, new ArrayList<>());
    }

    public BreadcrumbsAdapter(BreadcrumbsView parent, ArrayList<BreadcrumbItem> items) {
        this.parent = parent;
        this.items = items;
        DROPDOWN_OFFSET_Y_FIX = parent.getResources().getDimensionPixelOffset(R.dimen.dropdown_offset_y_fix_value);
    }

    public @NonNull
    ArrayList<BreadcrumbItem> getItems() {
        return this.items;
    }

    public void setItems(@NonNull ArrayList<BreadcrumbItem> items) {
        this.items = items;
    }

    public void setCallback(@Nullable BreadcrumbsCallback callback) {
        this.callback = callback;
    }

    public @Nullable
    BreadcrumbsCallback getCallback() {
        return this.callback;
    }

    public void setPopupThemeId(@IdRes int popupThemeId) {
        this.mPopupThemeId = popupThemeId;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == R.layout.breadcrumbs_view_item_arrow) {
            return new ArrowIconHolder(inflater.inflate(viewType, parent, false));
        } else if (viewType == R.layout.breadcrumbs_view_item_text) {
            return new BreadcrumbItemHolder(inflater.inflate(viewType, parent, false));
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        int viewType = getItemViewType(position);
        int truePos = viewType == R.layout.breadcrumbs_view_item_arrow ? ((position - 1) / 2) + 1 : position / 2;
        holder.setItem(items.get(truePos));
    }

    @Override
    public int getItemCount() {
        return (items != null && !items.isEmpty()) ? (items.size() * 2 - 1) : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 1 ? R.layout.breadcrumbs_view_item_arrow : R.layout.breadcrumbs_view_item_text;
    }

    class BreadcrumbItemHolder extends ItemHolder<BreadcrumbItem> {

        TextView button;

        BreadcrumbItemHolder(View itemView) {
            super(itemView);
            button = (TextView) itemView;
            button.setOnClickListener(view -> {
                if (callback != null) {
                    callback.onItemClick(parent, getAdapterPosition() / 2);
                }
            });
        }

        @Override
        public void setItem(@NonNull BreadcrumbItem item) {
            super.setItem(item);
            button.setText(item.getSelectedItem());
            button.setTextColor(
                    ResourceUtils.resolveColor(getContext().getTheme(),
                            getAdapterPosition() == getItemCount() - 1 ? android.R.attr.textColorPrimary : android.R.attr.textColorSecondary)
            );
        }

    }

    class ArrowIconHolder extends ItemHolder<BreadcrumbItem> {

        ImageButton imageButton;
        ListPopupWindow popupWindow;

        ArrowIconHolder(View itemView) {
            super(itemView);
            imageButton = (ImageButton) itemView;
            imageButton.setOnClickListener(view -> {
                if (item.hasMoreSelect()) {
                    try {
                        popupWindow.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            createPopupWindow();
        }

        @Override
        public void setItem(@NonNull BreadcrumbItem item) {
            super.setItem(item);
            imageButton.setClickable(item.hasMoreSelect());
            if (item.hasMoreSelect()) {
                List<Map<String, String>> list = new ArrayList<>();
                for (Object obj : item.getItems()) {
                    Map<String, String> map = new HashMap<>();
                    map.put("text", obj.toString());
                    list.add(map);
                }
                // Kotlin: item.getItems().map { "text" to it.toString() }
                ListAdapter adapter = new SimpleAdapter(getPopupThemedContext(), list, R.layout.breadcrumbs_view_dropdown_item, new String[]{"text"}, new int[]{android.R.id.text1});
                popupWindow.setAdapter(adapter);
                popupWindow.setWidth(ViewUtils.measureContentWidth(getPopupThemedContext(), adapter));
                imageButton.setOnTouchListener(popupWindow.createDragToOpenListener(imageButton));
            } else {
                imageButton.setOnTouchListener(null);
            }
        }

        private void createPopupWindow() {
            popupWindow = new ListPopupWindow(getPopupThemedContext());
            popupWindow.setAnchorView(imageButton);
            popupWindow.setOnItemClickListener((adapterView, view, i, l) -> {
                if (callback != null) {
                    callback.onItemChange(parent, getAdapterPosition() / 2, getItems().get(getAdapterPosition() / 2 + 1).getItems().get(i));
                    popupWindow.dismiss();
                }
            });
            imageButton.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    popupWindow.setVerticalOffset(-imageButton.getMeasuredHeight() + DROPDOWN_OFFSET_Y_FIX);
                    imageButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }

    }

    class ItemHolder<T> extends RecyclerView.ViewHolder {

        T item;

        ItemHolder(View itemView) {
            super(itemView);
        }

        public void setItem(@NonNull T item) {
            this.item = item;
        }

        Context getContext() {
            return itemView.getContext();
        }

        Context getPopupThemedContext() {
            return mPopupThemeId != -1 ? new ContextThemeWrapper(getContext(), mPopupThemeId) : getContext();
        }

    }

}
