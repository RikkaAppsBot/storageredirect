package moe.shizuku.redirectstorage.widget

import android.animation.*
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Property
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import moe.shizuku.redirectstorage.R

class AnimatedStarView
@JvmOverloads
constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {

        private const val START_COLOR = -0xa8de
        private const val END_COLOR = -0x3ef9

        private const val DOTS_COUNT = 7
        private const val OUTER_DOTS_POSITION_ANGLE = 360 / DOTS_COUNT

        private const val COLOR_1 = -0x3ef9
        private const val COLOR_2 = -0x6800
        private const val COLOR_3 = -0xa8de
        private const val COLOR_4 = -0xbbcca

        private val DECELERATE_INTERPOLATOR = DecelerateInterpolator()
        private val ACCELERATE_DECELERATE_INTERPOLATOR = AccelerateDecelerateInterpolator()
        private val OVERSHOOT_INTERPOLATOR = OvershootInterpolator(4F)

        private fun mapValueFromRangeToRange(value: Float, fromLow: Float,
                                     fromHigh: Float, toLow: Float, toHigh: Float): Float {
            return toLow + (value - fromLow) / (fromHigh - fromLow) * (toHigh - toLow)
        }

        private fun clamp(value: Float, low: Float, high: Float): Float {
            return Math.min(Math.max(value, low), high)
        }

        @JvmStatic
        val INNER_CIRCLE_RADIUS_PROGRESS: Property<CircleView, Float> =
                object : Property<CircleView, Float>(Float::class.java, "innerCircleRadiusProgress") {
                    override fun get(view: CircleView): Float? {
                        return view.innerCircleRadiusProgress
                    }

                    override fun set(view: CircleView, value: Float?) {
                        view.innerCircleRadiusProgress = value!!
                    }
                }

        @JvmStatic
        val OUTER_CIRCLE_RADIUS_PROGRESS: Property<CircleView, Float> =
                object : Property<CircleView, Float>(Float::class.java, "outerCircleRadiusProgress") {
                    override fun get(view: CircleView): Float? {
                        return view.outerCircleRadiusProgress
                    }

                    override fun set(view: CircleView, value: Float?) {
                        view.outerCircleRadiusProgress = value!!
                    }
                }

        @JvmStatic
        val DOTS_PROGRESS: Property<DotsView, Float> =
                object : Property<DotsView, Float>(Float::class.java, "dotsProgress") {
                    override fun get(view: DotsView): Float {
                        return view.currentProgress
                    }

                    override fun set(view: DotsView, value: Float?) {
                        view.currentProgress = value!!
                    }
                }

    }

    val imageView: ImageView
    val dotsView: DotsView
    val circleView: CircleView

    private var isChecked: Boolean = false
    private var animatorSet: AnimatorSet? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_animated_star_view, this, true)
        imageView = findViewById(R.id.image_view)
        dotsView = findViewById(R.id.dots_view)
        circleView = findViewById(R.id.circle_view)
    }

    fun setChecked(bool: Boolean) {
        isChecked = bool
        imageView.setImageResource(if (isChecked) R.drawable.ic_star_activated_24dp else R.drawable.ic_star_text_secondary_color_24dp)

        if (animatorSet != null) {
            animatorSet!!.cancel()
        }

        if (isChecked) {
            imageView.animate().cancel()
            imageView.scaleX = 0F
            imageView.scaleY = 0F
            circleView.innerCircleRadiusProgress = 0F
            circleView.outerCircleRadiusProgress = 0F
            dotsView.currentProgress = 0F

            val outerCircleAnimator = ObjectAnimator.ofFloat(circleView, OUTER_CIRCLE_RADIUS_PROGRESS, 0.1f, 1f)
            outerCircleAnimator.duration = 250
            outerCircleAnimator.interpolator = DECELERATE_INTERPOLATOR

            val innerCircleAnimator = ObjectAnimator.ofFloat(circleView, INNER_CIRCLE_RADIUS_PROGRESS, 0.1f, 1f)
            innerCircleAnimator.duration = 200
            innerCircleAnimator.startDelay = 200
            innerCircleAnimator.interpolator = DECELERATE_INTERPOLATOR

            val starScaleYAnimator = ObjectAnimator.ofFloat(imageView, ImageView.SCALE_Y, 0.2f, 1f)
            starScaleYAnimator.duration = 350
            starScaleYAnimator.startDelay = 250
            starScaleYAnimator.interpolator = OVERSHOOT_INTERPOLATOR

            val starScaleXAnimator = ObjectAnimator.ofFloat(imageView, ImageView.SCALE_X, 0.2f, 1f)
            starScaleXAnimator.duration = 350
            starScaleXAnimator.startDelay = 250
            starScaleXAnimator.interpolator = OVERSHOOT_INTERPOLATOR

            val dotsAnimator = ObjectAnimator.ofFloat(dotsView, DOTS_PROGRESS, 0f, 1f)
            dotsAnimator.duration = 900
            dotsAnimator.startDelay = 50
            dotsAnimator.interpolator = ACCELERATE_DECELERATE_INTERPOLATOR

            animatorSet = AnimatorSet()
            animatorSet!!.apply {
                playTogether(
                        outerCircleAnimator,
                        innerCircleAnimator,
                        starScaleYAnimator,
                        starScaleXAnimator,
                        dotsAnimator
                )
                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationCancel(animation: Animator) {
                        circleView.innerCircleRadiusProgress = 0F
                        circleView.outerCircleRadiusProgress = 0F
                        dotsView.currentProgress = 0F
                        imageView.scaleX = 1F
                        imageView.scaleY = 1F
                    }
                })
            }.start()
        }
    }

    fun toggle() {
        setChecked(!isChecked)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                imageView.animate()
                        .scaleX(0.7f)
                        .scaleY(0.7f)
                        .setDuration(150)
                        .also {
                            it.interpolator = DECELERATE_INTERPOLATOR
                        }
                isPressed = true
            }

            MotionEvent.ACTION_MOVE -> {
                val x = event.x
                val y = event.y
                val isInside = x > 0 && x < width && y > 0 && y < height
                if (isPressed != isInside) {
                    isPressed = isInside
                }
            }

            MotionEvent.ACTION_UP -> {
                imageView.animate()
                        .scaleX(1F)
                        .scaleY(1F)
                        .also {
                            it.interpolator = DECELERATE_INTERPOLATOR
                        }
                if (isPressed) {
                    performClick()
                    isPressed = false
                }
            }

            MotionEvent.ACTION_CANCEL -> {
                imageView.animate()
                        .scaleX(1F)
                        .scaleY(1F)
                        .also {
                            it.interpolator = DECELERATE_INTERPOLATOR
                        }
                isPressed = false
            }
        }
        return true
    }

    fun isChecked(): Boolean {
        return isChecked
    }

    class DotsView @JvmOverloads constructor(
            context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : View(context, attrs, defStyleAttr) {

        private val circlePaints = Array(4) {
            Paint().also {
                it.style = Paint.Style.FILL
            }
        }

        private var centerX: Int = 0
        private var centerY: Int = 0

        private var maxOuterDotsRadius: Float = 0f
        private var maxInnerDotsRadius: Float = 0f
        private var maxDotSize: Float = 0f

        var currentProgress = 0f
            set(value) {
                field = value

                updateInnerDotsPosition()
                updateOuterDotsPosition()
                updateDotsPaints()
                updateDotsAlpha()

                postInvalidate()
            }

        private var currentRadius1 = 0f
        private var currentDotSize1 = 0f

        private var currentDotSize2 = 0f
        private var currentRadius2 = 0f

        private val argbEvaluator = ArgbEvaluator()

        override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
            super.onSizeChanged(w, h, oldw, oldh)
            centerX = w / 2
            centerY = h / 2
            maxDotSize = 5f
            maxOuterDotsRadius = w / 2 - maxDotSize * 2
            maxInnerDotsRadius = 0.8f * maxOuterDotsRadius
        }

        override fun onDraw(canvas: Canvas) {
            drawOuterDotsFrame(canvas)
            drawInnerDotsFrame(canvas)
        }

        private fun drawOuterDotsFrame(canvas: Canvas) {
            for (i in 0 until DOTS_COUNT) {
                val cX = (centerX + currentRadius1 * Math.cos(i.toDouble() * OUTER_DOTS_POSITION_ANGLE.toDouble() * Math.PI / 180)).toFloat()
                val cY = (centerY + currentRadius1 * Math.sin(i.toDouble() * OUTER_DOTS_POSITION_ANGLE.toDouble() * Math.PI / 180)).toFloat()
                canvas.drawCircle(cX, cY, currentDotSize1, circlePaints[i % circlePaints.size])
            }
        }

        private fun drawInnerDotsFrame(canvas: Canvas) {
            for (i in 0 until DOTS_COUNT) {
                val cX = (centerX + currentRadius2 * Math.cos((i * OUTER_DOTS_POSITION_ANGLE - 10) * Math.PI / 180)).toFloat()
                val cY = (centerY + currentRadius2 * Math.sin((i * OUTER_DOTS_POSITION_ANGLE - 10) * Math.PI / 180)).toFloat()
                canvas.drawCircle(cX, cY, currentDotSize2, circlePaints[(i + 1) % circlePaints.size])
            }
        }

        private fun updateInnerDotsPosition() {
            if (currentProgress < 0.3f) {
                this.currentRadius2 = mapValueFromRangeToRange(currentProgress,
                        0f, 0.3f, 0f, maxInnerDotsRadius)
            } else {
                this.currentRadius2 = maxInnerDotsRadius
            }

            when {
                currentProgress < 0.2 -> this.currentDotSize2 = maxDotSize
                currentProgress < 0.5 -> this.currentDotSize2 = mapValueFromRangeToRange(currentProgress,
                        0.2f, 0.5f, maxDotSize, 0.3f * maxDotSize)
                else -> this.currentDotSize2 = mapValueFromRangeToRange(currentProgress,
                        0.5f, 1f, maxDotSize * 0.3f, 0f)
            }
        }

        private fun updateOuterDotsPosition() {
            if (currentProgress < 0.3f) {
                this.currentRadius1 = mapValueFromRangeToRange(currentProgress,
                        0.0f, 0.3f, 0f, maxOuterDotsRadius * 0.8f)
            } else {
                this.currentRadius1 = mapValueFromRangeToRange(currentProgress,
                        0.3f, 1f, 0.8f * maxOuterDotsRadius, maxOuterDotsRadius)
            }

            if (currentProgress < 0.7) {
                this.currentDotSize1 = maxDotSize
            } else {
                this.currentDotSize1 = mapValueFromRangeToRange(currentProgress,
                        0.7f, 1f, maxDotSize, 0f)
            }
        }

        private fun updateDotsPaints() {
            if (currentProgress < 0.5f) {
                val progress = mapValueFromRangeToRange(currentProgress,
                        0f, 0.5f, 0f, 1f)
                circlePaints[0].color = argbEvaluator.evaluate(progress, COLOR_1, COLOR_2) as Int
                circlePaints[1].color = argbEvaluator.evaluate(progress, COLOR_2, COLOR_3) as Int
                circlePaints[2].color = argbEvaluator.evaluate(progress, COLOR_3, COLOR_4) as Int
                circlePaints[3].color = argbEvaluator.evaluate(progress, COLOR_4, COLOR_1) as Int
            } else {
                val progress = mapValueFromRangeToRange(currentProgress,
                        0.5f, 1f, 0f, 1f)
                circlePaints[0].color = argbEvaluator.evaluate(progress, COLOR_2, COLOR_3) as Int
                circlePaints[1].color = argbEvaluator.evaluate(progress, COLOR_3, COLOR_4) as Int
                circlePaints[2].color = argbEvaluator.evaluate(progress, COLOR_4, COLOR_1) as Int
                circlePaints[3].color = argbEvaluator.evaluate(progress, COLOR_1, COLOR_2) as Int
            }
        }

        private fun updateDotsAlpha() {
            val progress = clamp(currentProgress, 0.6f, 1f)
            val alpha = mapValueFromRangeToRange(progress,
                    0.6f, 1f, 255f, 0f).toInt()
            circlePaints[0].alpha = alpha
            circlePaints[1].alpha = alpha
            circlePaints[2].alpha = alpha
            circlePaints[3].alpha = alpha
        }

    }

    class CircleView @JvmOverloads constructor(
            context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : View(context, attrs, defStyleAttr) {

        private val argbEvaluator = ArgbEvaluator()

        private val circlePaint = Paint()
        private val maskPaint = Paint()

        private lateinit var tempBitmap: Bitmap
        private lateinit var tempCanvas: Canvas

        var outerCircleRadiusProgress = 0f
            set(value) {
                field = value
                updateCircleColor()
                postInvalidate()
            }
        var innerCircleRadiusProgress = 0f
            set(value) {
                field = value
                postInvalidate()
            }

        private var maxCircleSize: Int = 0

        init {
            circlePaint.style = Paint.Style.FILL
            maskPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }

        override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
            super.onSizeChanged(w, h, oldw, oldh)
            maxCircleSize = w / 2
            tempBitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888)
            tempCanvas = Canvas(tempBitmap)
        }

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            tempCanvas.drawColor(0xffffff, PorterDuff.Mode.CLEAR)
            tempCanvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(),
                    outerCircleRadiusProgress * maxCircleSize, circlePaint)
            tempCanvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(),
                    innerCircleRadiusProgress * maxCircleSize, maskPaint)
            canvas.drawBitmap(tempBitmap, 0F, 0F, null)
        }

        private fun updateCircleColor() {
            var colorProgress = clamp(outerCircleRadiusProgress, 0.5f, 1.0f)
            colorProgress = mapValueFromRangeToRange(colorProgress, 0.5f, 1.0f, 0.0f, 1.0f)
            this.circlePaint.color = argbEvaluator.evaluate(colorProgress, START_COLOR, END_COLOR) as Int
        }

    }

}