package moe.shizuku.redirectstorage.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import moe.shizuku.redirectstorage.R

class EditButton
@JvmOverloads
constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
        defStyleRes: Int = R.style.Widget_EditButton
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {

    private val mTitle: TextView
    private val mContent: TextView

    init {
        LayoutInflater.from(context).inflate(R.layout.edit_button_widget_layout, this, true)

        mTitle = findViewById(android.R.id.title)
        mContent = findViewById(android.R.id.text1)

        if (attrs != null) {
            val a = context.obtainStyledAttributes(
                    attrs, R.styleable.EditButton, defStyleAttr, defStyleRes)
            setTitle(a.getString(R.styleable.EditButton_title))
            setContentText(a.getString(R.styleable.EditButton_android_text))
            setContentHint(a.getString(R.styleable.EditButton_android_hint))
            setIconDrawable(a.getDrawable(R.styleable.EditButton_android_icon))
            if (a.hasValue(R.styleable.EditButton_android_maxLines)) {
                a.getInt(R.styleable.EditButton_android_maxLines, 1)
            }
            a.recycle()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        mContent.isEnabled = enabled
    }

    fun setTitle(title: CharSequence?) {
        mTitle.text = title
    }

    fun setTitle(@StringRes titleRes: Int) {
        mTitle.setText(titleRes)
    }

    fun setContentText(text: CharSequence?) {
        mContent.text = text
    }

    fun setContentText(@StringRes textRes: Int) {
        mContent.setText(textRes)
    }

    fun setContentHint(hint: CharSequence?) {
        mContent.hint = hint
    }

    fun setContentHint(@StringRes hintRes: Int) {
        mContent.setHint(hintRes)
    }

    fun setIconDrawable(@DrawableRes drawableRes: Int) {
        mContent.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, drawableRes, 0)
    }

    fun setIconDrawable(drawable: Drawable?) {
        mContent.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null)
    }

    var maxLines
        get() = mContent.maxLines
        set(value) {
            mContent.maxLines = value
        }

    override fun onSaveInstanceState(): Parcelable? {
        val state = ViewSavedState(super.onSaveInstanceState())
        state.title = mTitle.text.toString()
        state.content = mContent.text.toString()
        state.hint = mContent.hint.toString()
        return state
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state is ViewSavedState) {
            super.onRestoreInstanceState(state.superState)
            setTitle(state.title)
            setContentText(state.content)
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    internal class ViewSavedState : BaseSavedState {

        companion object {

            @JvmField
            val CREATOR = object : Parcelable.Creator<ViewSavedState> {
                override fun createFromParcel(source: Parcel): ViewSavedState {
                    return ViewSavedState(source)
                }

                override fun newArray(size: Int): Array<ViewSavedState?> {
                    return arrayOfNulls(size)
                }
            }

        }

        var title: String? = null
        var content: String? = null
        var hint: String? = null

        private constructor(src: Parcel) : super(src) {
            title = src.readString()
            content = src.readString()
            hint = src.readString()
        }

        constructor(superState: Parcelable?) : super(superState)

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeString(title)
            out.writeString(content)
            out.writeString(hint)
        }

    }

}
