package moe.shizuku.redirectstorage.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import moe.shizuku.redirectstorage.R;

public class RaisedLinearLayout extends LinearLayout implements RaisedView {

    private static final int[] CHECKED_STATE_SET = new int[]{R.attr.state_raised};

    private boolean mRaised;

    public RaisedLinearLayout(Context context) {
        super(context);
    }

    public RaisedLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RaisedLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RaisedLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isRaised()) {
            View.mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }

    @Override
    public boolean isRaised() {
        return mRaised;
    }

    @Override
    public void setRaised(boolean raised) {
        if (mRaised != raised) {
            mRaised = raised;
            refreshDrawableState();
        }
    }
}
