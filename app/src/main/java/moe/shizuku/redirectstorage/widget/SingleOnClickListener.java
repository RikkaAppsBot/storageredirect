package moe.shizuku.redirectstorage.widget;

import android.view.View;

import androidx.annotation.CallSuper;

public class SingleOnClickListener implements View.OnClickListener {

    @CallSuper
    @Override
    public void onClick(View v) {
        v.setOnClickListener(null);
    }
}
