package moe.shizuku.redirectstorage.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import moe.shizuku.redirectstorage.R
import rikka.core.res.resolveColor

class DialogRecyclerView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0)
    : RecyclerView(context, attrs, defStyle) {

    private var isTopBorderShowed = false
    private var isBottomBorderShowed = false

    private val mBorderLinePaint: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL_AND_STROKE
            strokeWidth = context.resources.getDimension(R.dimen.dialog_list_view_border_width)
            color = context.theme.resolveColor(android.R.attr.textColorTertiary)
            alpha = (255 * 0.2f).toInt()
        }
    }

    override fun onScrolled(dx: Int, dy: Int) {
        val offset = computeVerticalScrollOffset()
        val range = computeVerticalScrollRange() - computeVerticalScrollExtent()
        val isTopBorderShowed: Boolean
        val isBottomBorderShowed: Boolean
        if (range != 0) {
            isTopBorderShowed = offset > 0
            isBottomBorderShowed = offset < range - 1
        } else {
            isBottomBorderShowed = false
            isTopBorderShowed = isBottomBorderShowed
        }
        if (this.isTopBorderShowed != isTopBorderShowed
                || this.isBottomBorderShowed != isBottomBorderShowed) {
            this.isTopBorderShowed = isTopBorderShowed
            this.isBottomBorderShowed = isBottomBorderShowed
            postInvalidate()
        }
    }

    override fun onDrawForeground(canvas: Canvas) {
        super.onDrawForeground(canvas)
        if (isBottomBorderShowed) {
            canvas.drawLine(
                    0f,
                    canvas.height - (mBorderLinePaint.strokeWidth + 1) / 2,
                    canvas.width.toFloat(),
                    canvas.height - (mBorderLinePaint.strokeWidth + 1) / 2,
                    mBorderLinePaint
            )
        }
        if (isTopBorderShowed) {
            canvas.drawLine(
                    0f,
                    (mBorderLinePaint.strokeWidth + 1) / 2,
                    canvas.width.toFloat(),
                    (mBorderLinePaint.strokeWidth + 1) / 2,
                    mBorderLinePaint
            )
        }
    }
}