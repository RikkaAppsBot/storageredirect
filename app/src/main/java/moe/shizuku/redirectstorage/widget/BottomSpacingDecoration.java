package moe.shizuku.redirectstorage.widget;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class BottomSpacingDecoration extends RecyclerView.ItemDecoration {

    private static final int PADDING_DP = 8;
    private int mPadding;

    public BottomSpacingDecoration(Context context) {
        this(Math.round(PADDING_DP * context.getResources().getDisplayMetrics().density));
    }

    public BottomSpacingDecoration(int padding) {
        this.mPadding = padding;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
            outRect.bottom = mPadding;
        }
    }
}
