package moe.shizuku.redirectstorage.widget

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import androidx.recyclerview.widget.RecyclerView

class BackgroundClippedRecyclerView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : RecyclerView(context, attrs, defStyleAttr) {

    init {
        clipToOutline = true
        outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                when (background) {
                    is GradientDrawable -> background.getOutline(outline)
                    is ShapeDrawable -> background.getOutline(outline)
                    else -> throw IllegalStateException(
                            "Unsupported get clip outline from " + background.javaClass.simpleName)
                }
            }
        }
    }

}
