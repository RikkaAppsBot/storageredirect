package moe.shizuku.redirectstorage.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Fung Gwo on 2018/1/27.
 */

class AnimatedRecyclerView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0)
    : RecyclerView(context, attrs, defStyle) {

    override fun setLayoutManager(layoutManager: LayoutManager?) {
        if (layoutManager !is LinearLayoutManager) {
            throw IllegalArgumentException(
                    "AnimatedRecyclerView supports only LinearLayoutManager currently.")
        }
        super.setLayoutManager(layoutManager)
    }

    override fun attachLayoutAnimationParameters(child: View, params: ViewGroup.LayoutParams,
                                                 index: Int, count: Int) {
        if (adapter != null && layoutManager is LinearLayoutManager) {
            var animationParameters = params.layoutAnimationParameters
            if (animationParameters == null) {
                animationParameters = LayoutAnimationController.AnimationParameters()
                params.layoutAnimationParameters = animationParameters
            }
            animationParameters.index = index
            animationParameters.count = count
        } else {
            super.attachLayoutAnimationParameters(child, params, index, count)
        }
    }
}
