package moe.shizuku.redirectstorage.widget;

public interface RaisedView {

    boolean isRaised();
    void setRaised(boolean raised);
}