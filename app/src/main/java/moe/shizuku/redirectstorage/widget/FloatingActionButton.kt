package moe.shizuku.redirectstorage.widget

import android.animation.AnimatorInflater.loadStateListAnimator
import android.content.Context
import android.graphics.Outline
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageButton

import moe.shizuku.redirectstorage.R

/**
 * Created by Fung Gwo on 2018/2/21.
 */

class FloatingActionButton
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ImageButton(context, attrs, defStyleAttr) {

    init {
        outlineProvider = RoundOutlineProvider()
        clipToOutline = true
        stateListAnimator = loadStateListAnimator(context, R.animator.fab_state_list_animator)
        elevation = resources.getDimension(R.dimen.fab_resting_elevation)
        val a = context.obtainStyledAttributes(intArrayOf(android.R.attr.selectableItemBackground))
        foreground = a.getDrawable(0)
        a.recycle()
        scaleType = ScaleType.CENTER
        isClickable = true
        isFocusable = true
    }

    private class RoundOutlineProvider : ViewOutlineProvider() {

        override fun getOutline(view: View, outline: Outline) {
            outline.setOval(0, 0, view.width, view.height)
        }

    }

}
