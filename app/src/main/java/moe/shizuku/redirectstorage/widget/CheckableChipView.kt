package moe.shizuku.redirectstorage.widget

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Outline
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.*
import moe.shizuku.redirectstorage.R
import moe.shizuku.redirectstorage.drawable.ChipRevealBackgroundDrawable

class CheckableChipView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : FrameLayout(context, attrs, defStyleAttr), Checkable {

    private val mChipLayout: View
    private val mTitle: TextView
    private val mCheckIcon: ImageView
    private val mCheckIconDot: ImageView
    private val mRightSpace: View
    private val mIconSpace: View
    private val mWidgetContainer: LinearLayout

    private var isChecked = false
    private var isCheckable = false
    private var mLastCheckable = false

    private var isBroadcasting = false

    private var mChipBackgroundTintNormal: Int = 0
    private var mChipBackgroundTintActivated: Int = 0
    private var mChipTextColorNormal: Int = 0
    private var mChipTextColorActivated: Int = 0
    private var mChipIconTintNormal: Int = 0
    private var mChipIconTintActivated: Int = 0
    private val mAnimationDuration: Int
    private val mIconSpaceWidth: Int
    private val mMinLeftSpaceWidth: Int

    private val mRevealBackground: ChipRevealBackgroundDrawable

    private val mStateAnimator: Animator? = null
    private var mIconSpaceWidthAnimator: Animator? = null

    private var mOnCheckedChangeListener: OnCheckedChangeListener? = null

    init {
        val rootView = LayoutInflater.from(context).inflate(
                R.layout.checkable_chip_widget_layout, this, false)
        mChipLayout = rootView.findViewById(R.id.chip_layout)
        mTitle = rootView.findViewById(android.R.id.title)
        mCheckIcon = rootView.findViewById(R.id.check_icon)
        mCheckIconDot = rootView.findViewById(R.id.check_icon_dot)
        mRightSpace = rootView.findViewById(R.id.chip_title_right_space)
        mIconSpace = rootView.findViewById(R.id.chip_icon_space)
        mWidgetContainer = rootView.findViewById(R.id.chip_widget_container)
        addView(rootView, LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT))

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.ChipView, defStyleAttr, 0)
            mChipBackgroundTintNormal = a.getColor(R.styleable.ChipView_chipBackgroundTintNormal, 0)
            mChipBackgroundTintActivated = a.getColor(R.styleable.ChipView_chipBackgroundTintActivated, 0)
            mChipTextColorNormal = a.getColor(R.styleable.ChipView_chipTextColorNormal, 0)
            mChipTextColorActivated = a.getColor(R.styleable.ChipView_chipTextColorActivated, 0)
            mChipIconTintNormal = a.getColor(R.styleable.ChipView_chipRightIconTintNormal, 0)
            mChipIconTintActivated = a.getColor(R.styleable.ChipView_chipRightIconTintActivated, 0)
            if (a.hasValue(R.styleable.ChipView_title)) {
                mTitle.text = a.getString(R.styleable.ChipView_title)
            }
            a.recycle()
        }

        mAnimationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)
        mIconSpaceWidth = resources.getDimensionPixelSize(R.dimen.chip_icon_space)
        mMinLeftSpaceWidth = resources.getDimensionPixelSize(R.dimen.chip_margin_start)

        mRevealBackground = ChipRevealBackgroundDrawable(getContext())
                .setActivatedColor(mChipBackgroundTintActivated)
                .setStartX(mIconSpaceWidth / 2)
                .setStartY(mIconSpaceWidth / 2)
        mWidgetContainer.addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            mRevealBackground.setEndRadius(Math.abs(right - left))
        }
        mWidgetContainer.background = mRevealBackground
        mWidgetContainer.setOnClickListener {
            if (isCheckable) {
                toggle()
            }
        }

        mChipLayout.clipToOutline = true
        mChipLayout.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                outline.setRoundRect(
                        view.left, view.top, view.right, view.bottom,
                        resources.getDimensionPixelSize(R.dimen.chip_corner_radius).toFloat()
                )
            }
        }

        updateViewState(false)
    }

    fun updateViewState(animate: Boolean) {
        mChipLayout.isClickable = isCheckable

        mTitle.visibility = View.VISIBLE

        if (isCheckable) {
            mCheckIconDot.visibility = View.VISIBLE
        } else {
            mCheckIconDot.visibility = View.GONE
        }

        mChipLayout.backgroundTintList = ColorStateList.valueOf(mChipBackgroundTintNormal)

        if (animate) {
            if (isCheckable != mLastCheckable) {
                if (isCheckable) {
                    if (isChecked) {
                        mCheckIcon.animate().scaleX(1f).scaleY(1f)
                                .setDuration(mAnimationDuration.toLong())
                                .start()
                    }
                    mCheckIconDot.animate().scaleX(1f).scaleY(1f)
                            .setDuration(mAnimationDuration.toLong())
                            .start()
                    val widthAnimator = ValueAnimator.ofInt(mMinLeftSpaceWidth, mIconSpaceWidth)
                    widthAnimator.addUpdateListener { animator ->
                        mIconSpace.layoutParams.width = animator.animatedValue as Int
                        mChipLayout.invalidateOutline()
                        mIconSpace.requestLayout()
                    }
                    widthAnimator.duration = mAnimationDuration.toLong()
                    widthAnimator.start()
                    mIconSpaceWidthAnimator = widthAnimator
                } else {
                    mCheckIcon.animate().scaleX(0f).scaleY(0f)
                            .setDuration(mAnimationDuration.toLong())
                            .start()
                    mCheckIconDot.animate().scaleX(0f).scaleY(0f)
                            .setDuration(mAnimationDuration.toLong())
                            .start()

                    val widthAnimator = ValueAnimator.ofInt(mIconSpaceWidth, mMinLeftSpaceWidth)
                    widthAnimator.addUpdateListener { animator ->
                        mIconSpace.layoutParams.width = animator.animatedValue as Int
                        mChipLayout.invalidateOutline()
                        mIconSpace.requestLayout()
                    }
                    widthAnimator.duration = mAnimationDuration.toLong()
                    widthAnimator.start()
                    mIconSpaceWidthAnimator = widthAnimator
                }
            }

            if (mChipLayout.isSelected != isChecked) {
                mCheckIcon.animate()
                        .scaleX(if (isChecked && isCheckable) 1f else 0f)
                        .scaleY(if (isChecked && isCheckable) 1f else 0f)
                        .setDuration(mAnimationDuration.toLong())
                        .start()
                animateColorWithView(mTitle, "textColor",
                        if (isChecked) mChipTextColorNormal else mChipTextColorActivated,
                        if (isChecked) mChipTextColorActivated else mChipTextColorNormal)
                animateColorWithView(mCheckIcon,
                        if (isChecked) mChipIconTintNormal else mChipIconTintActivated,
                        if (isChecked) mChipIconTintActivated else mChipIconTintNormal
                ) { mCheckIcon.imageTintList = ColorStateList.valueOf(it.animatedValue as Int) }
                mRevealBackground.startRevealAnimation(isChecked)
            }
        } else if (mStateAnimator?.isRunning != false && mIconSpaceWidthAnimator?.isRunning != true) {
            mIconSpace.layoutParams.width = if (isCheckable) mIconSpaceWidth else mMinLeftSpaceWidth
            mIconSpace.requestLayout()
            mCheckIcon.scaleX = if (isCheckable && isChecked) 1f else 0f
            mCheckIcon.scaleY = if (isCheckable && isChecked) 1f else 0f
            mTitle.setTextColor(if (isChecked) mChipTextColorActivated else mChipTextColorNormal)
            mCheckIcon.imageTintList = ColorStateList.valueOf(
                    if (isChecked) mChipIconTintActivated else mChipIconTintNormal)
            mRevealBackground.setProgress(if (isChecked) 1f else 0f)
        }

        mChipLayout.isSelected = isChecked

        mLastCheckable = isCheckable
    }

    fun setTitle(title: String) {
        mTitle.text = title
    }

    override fun setChecked(checked: Boolean) {
        setChecked(checked, notify = true, animate = true)
    }

    fun setChecked(checked: Boolean, notify: Boolean, animate: Boolean) {
        if (isChecked != checked) {
            isChecked = checked
            updateViewState(animate)

            if (isBroadcasting || !notify) {
                return
            }

            isBroadcasting = true
            mOnCheckedChangeListener?.onCheckedChanged(this, isChecked)
            isBroadcasting = false
        }
    }

    override fun isChecked(): Boolean {
        return isChecked
    }

    override fun toggle() {
        setChecked(!isChecked())
    }

    @JvmOverloads
    fun setCheckable(checkable: Boolean, notify: Boolean = true) {
        if (isCheckable == checkable) return
        isCheckable = checkable
        if (notify) {
            updateViewState(true)
        }
    }

    fun isCheckable(): Boolean {
        return isCheckable
    }

    fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        mOnCheckedChangeListener = listener
    }

    fun setOnCheckedChangeListener(listener: (view: CheckableChipView, isChecked: Boolean) -> Unit) {
        mOnCheckedChangeListener = object : OnCheckedChangeListener {
            override fun onCheckedChanged(view: CheckableChipView, isChecked: Boolean) {
                listener(view, isChecked)
            }
        }
    }

    private fun animateColorWithView(target: View, propertyName: String,
                                     from: Int, to: Int): ObjectAnimator {
        return ObjectAnimator.ofArgb(target, propertyName, from, to).apply {
            duration = target.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
            start()
        }
    }

    private fun animateColorWithView(target: View, from: Int, to: Int,
                                     consumer: (ValueAnimator) -> Unit): ValueAnimator {
        return ValueAnimator.ofArgb(from, to).apply {
            duration = target.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
            addUpdateListener { consumer(it) }
            start()
        }
    }

    interface OnCheckedChangeListener {

        fun onCheckedChanged(view: CheckableChipView, isChecked: Boolean)

    }

}
