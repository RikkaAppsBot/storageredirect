package moe.shizuku.redirectstorage.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import moe.shizuku.redirectstorage.BuildConfig;
import moe.shizuku.redirectstorage.R;
import moe.shizuku.redirectstorage.model.BreadcrumbItem;
import moe.shizuku.redirectstorage.utils.ViewUtils;
import rikka.core.util.ResourceUtils;

public class BreadcrumbsView extends FrameLayout {

    /**
     * Internal implement of BreadcrumbsView
     */
    private RecyclerView mRecyclerView;
    private BreadcrumbsAdapter mAdapter;

    /**
     * Popup Menu Theme Id
     */
    private int mPopupThemeId = -1;

    private int mPaddingStart = 0;
    private int mPaddingEnd = 0;

    private static final String KEY_SUPER_STATES = BuildConfig.APPLICATION_ID + ".superStates";
    private static final String KEY_BREADCRUMBS = BuildConfig.APPLICATION_ID + ".breadcrumbs";

    public BreadcrumbsView(Context context) {
        this(context, null);
    }

    public BreadcrumbsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BreadcrumbsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BreadcrumbsView, defStyleAttr, 0);
            mPopupThemeId = a.getResourceId(R.styleable.BreadcrumbsView_android_popupTheme, -1);
            mPaddingStart = a.getDimensionPixelSize(R.styleable.BreadcrumbsView_paddingStart, 0);
            mPaddingEnd = a.getDimensionPixelSize(R.styleable.BreadcrumbsView_paddingEnd, 0);
            a.recycle();
        }

        init();
    }

    /**
     * Init BreadcrumbsView
     */
    private void init() {
        // Init RecyclerView
        if (mRecyclerView == null) {
            ViewGroup.LayoutParams rvLayoutParams = new ViewGroup.LayoutParams(-1, -1);
            rvLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            mRecyclerView = new RecyclerView(getContext());

            ViewUtils.setPaddingStart(mRecyclerView, mPaddingStart);
            ViewUtils.setPaddingEnd(mRecyclerView, mPaddingEnd);
            mRecyclerView.setClipToPadding(false);

            // Create Horizontal LinearLayoutManager
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    getContext(), LinearLayoutManager.HORIZONTAL, ResourceUtils.isRtl(getContext().getResources().getConfiguration()));
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setOverScrollMode(OVER_SCROLL_NEVER);

            // Add RecyclerView
            addView(mRecyclerView, rvLayoutParams);
        }
        // Init Adapter
        if (mAdapter == null) {
            mAdapter = new BreadcrumbsAdapter(this);
            if (mPopupThemeId != -1) {
                mAdapter.setPopupThemeId(mPopupThemeId);
            }
        }
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Get breadcrumb items list
     *
     * @return Breadcrumb Items
     */
    public @Nullable
    ArrayList<BreadcrumbItem> getItems() {
        return mAdapter.getItems();
    }

    /**
     * Get current breadcrumb item
     *
     * @return Current item
     */
    public @Nullable
    BreadcrumbItem getCurrentItem() {
        if (mAdapter.getItems().size() <= 0) {
            return null;
        }
        return mAdapter.getItems().get(mAdapter.getItems().size() - 1);
    }

    /**
     * Set breadcrumb items list
     *
     * @param items Target list
     */
    public void setItems(@Nullable ArrayList<BreadcrumbItem> items) {
        mAdapter.setItems(items);
        mAdapter.notifyDataSetChanged();
        postDelayed(() -> mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1), 500);
    }

    /**
     * Notify which item has been changed to update text view
     *
     * @param index The item position
     */
    public void notifyItemChanged(int index) {
        mAdapter.notifyItemChanged(index * 2);
    }

    /**
     * Add a new item
     *
     * @param item New item
     */
    public void addItem(@NonNull BreadcrumbItem item) {
        int oldSize = mAdapter.getItemCount();
        mAdapter.getItems().add(item);
        mAdapter.notifyItemRangeInserted(oldSize, 2);
        mAdapter.notifyItemChanged(oldSize - 1);
        postDelayed(() -> mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1), 500);
    }

    /**
     * Remove items after a position
     *
     * @param afterPos The first position of the removing range
     */
    public void removeItemAfter(final int afterPos) {
        if (afterPos <= mAdapter.getItems().size() - 1) {
            int oldSize = mAdapter.getItemCount();
            while (mAdapter.getItems().size() > afterPos) {
                mAdapter.getItems().remove(mAdapter.getItems().size() - 1);
            }
            mAdapter.notifyItemRangeRemoved(afterPos * 2 - 1, oldSize - afterPos);
            /* Add delay time to fix animation */
            postDelayed(() -> {
                int currentPos = afterPos * 2 - 1 - 1;
                mAdapter.notifyItemChanged(currentPos);
                mRecyclerView.smoothScrollToPosition(currentPos);
            }, 100);
        }
    }

    /**
     * Remove last item
     */
    public void removeLastItem() {
        removeItemAfter(mAdapter.getItems().size() - 1);
    }

    /**
     * Set BreadcrumbsView callback (Recommend to use DefaultBreadcrumbsCallback)
     *
     * @param callback Callback should be set
     * @see BreadcrumbsCallback
     * @see DefaultBreadcrumbsCallback
     */
    public void setCallback(@Nullable BreadcrumbsCallback callback) {
        mAdapter.setCallback(callback);
    }

    /**
     * Get callback
     *
     * @return Callback
     * @see BreadcrumbsCallback
     */
    public @Nullable
    BreadcrumbsCallback getCallback() {
        return mAdapter.getCallback();
    }

    // Save/Restore View Instance State
    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_SUPER_STATES, super.onSaveInstanceState());
        bundle.putParcelableArrayList(KEY_BREADCRUMBS, mAdapter.getItems());
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            super.onRestoreInstanceState(bundle.getParcelable(KEY_SUPER_STATES));
            setItems(bundle.getParcelableArrayList(KEY_BREADCRUMBS));
            return;
        }
        super.onRestoreInstanceState(BaseSavedState.EMPTY_STATE);
    }

}
