package moe.shizuku.redirectstorage.widget

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

import moe.shizuku.redirectstorage.R

class ExpandableLayout
@JvmOverloads
constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    private var mHeight: Int = 0
    private var mAnimHeight: Int = 0
    private var mLastAnimHeight: Int = 0
    private var mExpanded: Boolean = false
    private var mAnimating: Boolean = false
    private var mValueAnimator: ValueAnimator? = null

    private var mOnHeightUpdatedListener: OnHeightUpdatedListener? = null

    var isExpanded: Boolean
        get() = mExpanded
        set(expanded) = setExpanded(expanded, true)

    init {
        val a = context.obtainStyledAttributes(attrs,
                R.styleable.ExpandableLayout, defStyleAttr, 0)
        mExpanded = a.getBoolean(R.styleable.ExpandableLayout_isExpanded, false)
        a.recycle()
    }

    fun setOnHeightUpdatedListener(onHeightUpdatedListener: OnHeightUpdatedListener) {
        mOnHeightUpdatedListener = onHeightUpdatedListener
    }

    fun setOnHeightUpdatedListener(
            onHeightUpdatedListener: (v: ExpandableLayout, height: Int, changed: Int) -> Unit) {
        mOnHeightUpdatedListener = object : OnHeightUpdatedListener {
            override fun onHeightUpdate(v: ExpandableLayout, height: Int, changed: Int) {
                onHeightUpdatedListener(v, height, changed)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        if (isInEditMode) {
            return
        }

        mHeight = measuredHeight

        if (mAnimating) {
            setMeasuredDimension(measuredWidth, mAnimHeight)
        } else if (!mExpanded) {
            setMeasuredDimension(measuredWidth, 0)
        }
    }

    fun toggle() {
        isExpanded = !mExpanded
    }

    fun setExpanded(expanded: Boolean, animate: Boolean) {
        if (mExpanded == expanded) {
            return
        }

        mExpanded = expanded

        if (mAnimating) {
            mValueAnimator?.cancel()
        }

        if (!animate) {
            mAnimating = false
            requestLayout()
            return
        }

        val from: Int
        val to: Int
        if (mExpanded) {
            from = 0
            to = mHeight
        } else {
            from = mHeight
            to = 0
        }

        mLastAnimHeight = from
        mValueAnimator = ValueAnimator.ofInt(from, to).apply {
            duration = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
            addUpdateListener {
                mAnimHeight = it.animatedValue as Int
                requestLayout()

                mOnHeightUpdatedListener?.onHeightUpdate(
                        this@ExpandableLayout, mAnimHeight, mAnimHeight - mLastAnimHeight)

                mLastAnimHeight = mAnimHeight
            }
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    mAnimating = true
                }

                override fun onAnimationEnd(animation: Animator) {
                    mOnHeightUpdatedListener?.onHeightUpdate(
                            this@ExpandableLayout, mHeight, mHeight - mLastAnimHeight)

                    mAnimating = false
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            start()
        }
    }

    interface OnHeightUpdatedListener {
        fun onHeightUpdate(v: ExpandableLayout, height: Int, changed: Int)
    }

}
