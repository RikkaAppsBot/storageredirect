#ifndef APPLICATION_H
#define APPLICATION_H

#include <jni.h>

namespace Application {

    int registerNatives(JNIEnv *env);

    void setNames(const char *className, char *obfuscatedName,
                  size_t count, const char *names[], const char *obfuscatedNames[], const char *signatures[]);
}

#endif //APPLICATION_H
