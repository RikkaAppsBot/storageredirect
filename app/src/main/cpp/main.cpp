#include <jni.h>
#include <cassert>
#include <unistd.h>
#include <misc/android.h>
#include <sys/system_properties.h>
#include <xposed-detector.h>
#include <cstdlib>
#include "logging.h"
#include "signing/signing_block.h"
#include "signing/pm.h"
#include "EncryptedSharedPreferences.h"
#include "EncryptedSharedPreferencesLegacy.h"
#include "JNIHelper.h"
#include "Application.h"
#include "License.h"
#include "bypass.h"

static int sdk;
static int preview_sdk;

static void init() {
    char prop[PROP_VALUE_MAX] = {0};

    __system_property_get("ro.build.version.sdk", prop);
    sdk = (int) strtol(prop, nullptr, 10);

    __system_property_get("ro.build.version.preview_sdk", prop);
    preview_sdk = (int) strtol(prop, nullptr, 10);

    LOGD("sdk: %d (preview %d)", sdk, preview_sdk);
}

static int parseSigningBlock(JNIEnv *env) {
    char *path = getPath(env, getuid());
    LOGD("path=%s", path);
    int res = parse_signing_block(path);
    if (path) free(path);
    return res;
}

extern "C" JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {
    LOGD("JNI_OnLoad");
    JNIEnv *env = nullptr;

    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK)
        return JNI_ERR;

    init();

    if (sdk >= 30 || (sdk == 29 && preview_sdk > 0)) {
        bypass::Bypass(env);
    }

#ifndef NO_SIG_CHECK
    if (parseSigningBlock(env) != 0)
        return JNI_ERR;
#endif

    if (EncryptedSharedPreferences::registerNatives(env) != 0 ||
        EncryptedSharedPreferencesLegacy::registerNatives(env) != 0 ||
        Application::registerNatives(env) != 0 ||
        License::registerNatives(env))
        return JNI_ERR;

    auto xposed = get_xposed_status(env, sdk);
    switch (xposed) {
        case ANTIED_XPOSED:
            LOGD("anti-ed xposed");
            break;
        case FOUND_XPOSED:
            LOGD("found xposed");
            break;
        case NO_XPOSED:
            LOGD("no xposed");
            break;
        case CAN_NOT_ANTI_XPOSED:
            LOGD("cannot anti xposed");
            return JNI_ERR;
    }

    return JNI_VERSION_1_6;
}