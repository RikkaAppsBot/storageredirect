#include <cstring>
#include <cstdlib>
#include <jni.h>
#include <JNIHelper.h>
#include "logging.h"

namespace License {

// sync with IService.aidl
#define DESCRIPTOR "moe.shizuku.redirectstorage.IService"

    static const inline int methodCount = 1;
    static char sClassName[256];
    static JNINativeMethod *sMethods;

    static inline int my_strcmp(const char *s1, const char *s2) {
        while (*s1 == *s2++)
            if (*s1++ == 0)
                return (0);
        return (*(unsigned char *) s1 - *(unsigned char *) --s2);
    }

    static inline char *my_strcpy(const char *s) {
        size_t len = strlen(s);
        char *res = new char[len + 1];
        memcpy(res, s, len);
        res[len] = '\0';
        return res;
    }

    static int my_strlen(const char *str) {
        int i = 0;
        while (str[i] != '\0')
            i++;
        return i;
    }

    static char license[4][1024] = {{0},
                                    {0},
                                    {0},
                                    {0}};

    static int readInt(const unsigned char *bytes) {
        int res;
        *((unsigned char *) (&res) + 3) = bytes[0];
        *((unsigned char *) (&res) + 2) = bytes[1];
        *((unsigned char *) (&res) + 1) = bytes[2];
        *((unsigned char *) (&res)) = bytes[3];
        return res;
    }

    static bool isValid(int type) {
        return my_strlen(license[type]) > (type == 0 ? 24 : 0);
    }

    static bool isValid() {
        return isValid(0) || isValid(1) || isValid(2);
    }

    static jmethodID transactMethod;

    static jclass parcelClass;
    static jmethodID obtainMethod;
    static jmethodID recycleMethod;
    static jmethodID writeInterfaceTokenMethod;
    static jmethodID writeIntMethod;
    static jmethodID writeStringMethod;
    static jmethodID readExceptionMethod;

    static jclass deadObjectExceptionClass;

    static void init(JNIEnv *env) {
        static bool init = false;
        if (init) return;
        init = true;

        // IBinder
        jclass iBinderClass = env->FindClass("android/os/IBinder");
        transactMethod = env->GetMethodID(iBinderClass, "transact", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z");

        // Parcel
        auto parcelClass_ = env->FindClass("android/os/Parcel");
        if (parcelClass_) parcelClass = (jclass) env->NewGlobalRef(parcelClass_);
        obtainMethod = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
        recycleMethod = env->GetMethodID(parcelClass, "recycle", "()V");
        writeInterfaceTokenMethod = env->GetMethodID(parcelClass, "writeInterfaceToken", "(Ljava/lang/String;)V");
        writeIntMethod = env->GetMethodID(parcelClass, "writeInt", "(I)V");
        writeStringMethod = env->GetMethodID(parcelClass, "writeString", "(Ljava/lang/String;)V");
        readExceptionMethod = env->GetMethodID(parcelClass, "readException", "()V");

        auto deadObjectExceptionClass_ = env->FindClass("android/os/DeadObjectException");
        if (deadObjectExceptionClass_) deadObjectExceptionClass = (jclass) env->NewGlobalRef(deadObjectExceptionClass_);
    }

    static jobject binder;

    static jboolean tryTransact(JNIEnv *env, jint code, jobject data, jobject reply, jint flags) {
        if (!binder) {
            LOGD("binder is null");
            return JNI_FALSE;
        }

        auto res = env->CallBooleanMethod(binder, transactMethod, code, data, reply, flags);
        auto exception = env->ExceptionOccurred();
        if (exception) {
            env->ExceptionClear();
        }
        return res;
    }

    static bool setLicenseRemote(JNIEnv *env, int type) {
        /*
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
            _data.writeInterfaceToken(DESCRIPTOR);
            _data.writeInt(type);
            _data.writeString(value);
            boolean _status = mRemote.transact(Stub.TRANSACTION_updateLicense, _data, _reply, 0);
            if (!_status && getDefaultImpl() != null) {
                getDefaultImpl().updateLicense(type, value);
                return;
            }
            _reply.readException();
        }
        finally {
                _reply.recycle();
                _data.recycle();
        }
        */

        init(env);

        jstring descriptor, value;
        jobject data;

        data = env->CallStaticObjectMethod(parcelClass, obtainMethod);

        descriptor = env->NewStringUTF(DESCRIPTOR);
        env->CallVoidMethod(data, writeInterfaceTokenMethod, descriptor);
        env->CallVoidMethod(data, writeIntMethod, type);
        value = license[type][0] != '\0' ? env->NewStringUTF(license[type]) : nullptr;
        env->CallVoidMethod(data, writeStringMethod, value);

        auto res = tryTransact(env, 65, data, nullptr, 1 /* FLAG_ONEWAY */);
        LOGD("update license remote: res=%s, type=%d, valid=%s", res ? "true" : "false", type, isValid(type) ? "true" : "false");

        env->CallVoidMethod(data, recycleMethod);

        env->DeleteLocalRef(value);
        env->DeleteLocalRef(descriptor);
        env->DeleteLocalRef(data);

        return res;
    }

    void setLicense(JNIEnv *env, int type, const unsigned char *bytes) {
        bool changed = false;
        if (bytes) {
            int data_type = readInt(bytes);
            if (data_type != 0) {
#ifdef DEBUG
                LOGE("not string");
#endif
                return;
            }

            int length = readInt(bytes + 4);
            if (length < 0) {
#ifdef DEBUG
                LOGE("bad string");
#endif
                return;
            }

            for (int i = 0; i < length; ++i) {
                if (license[type][i] != (char) *(bytes + 8 + i)) {
                    license[type][i] = (char) *(bytes + 8 + i);
                    changed = true;
                }
            }
            license[type][length] = '\0';
        } else {
            if (license[type][0] != '\0') {
                license[type][0] = '\0';
                changed = true;
            }
        }

        if (changed) {
            LOGD("license updated: type=%d, valid=%s", type, isValid() ? "true" : "false");
        }

        if (changed) {
            if (!setLicenseRemote(env, type)) {
                license[type][0] = '\0';
            }
        }
    }

    static void doAction(JNIEnv *env, jobject object, jint action, jobject arg) {
        switch (action) {
            case 0: {
                LOGD("reset remote_status");
                license[0][0] = license[1][0] = license[2][0] = license[3][0] = '\0';

                if (binder) {
                    env->DeleteGlobalRef(binder);
                    binder = nullptr;
                }
                if (arg) {
                    LOGD("doAction get binder");
                    binder = env->NewGlobalRef(arg);
                } else {
                    LOGD("doAction get null binder");
                }
                break;
            }
        }
    }

    void setNames(const char *className, char *obfuscatedName, size_t count, const char *names[], const char *obfuscatedNames[],
                  const char *signatures[]) {
        if (my_strcmp(className, "moe/shizuku/redirectstorage/license/License") != 0) return;

        memcpy(sClassName, obfuscatedName, strlen(className));
        sClassName[strlen(obfuscatedName)] = '\0';

        sMethods = new JNINativeMethod[methodCount];

        for (size_t i = 0; i < count; i++) {
            void *func;
            if (my_strcmp(names[i], "native_doAction") == 0
                && my_strcmp(signatures[i], "(ILjava/lang/Object;)V") == 0)
                func = (void *) doAction;
            else
                continue;

            sMethods[i] = {my_strcpy(obfuscatedNames[i]), my_strcpy(signatures[i]), func};
        }
    }


    int registerNatives(JNIEnv *env) {
#ifdef DEBUG
        jclass cls = JNI_FindClass(env, "moe/shizuku/redirectstorage/license/License")
        if (!cls) return 1;

        JNINativeMethod methods[methodCount] = {
                {"native_doAction", "(ILjava/lang/Object;)V", (void *) doAction},
        };

        jint res = JNI_RegisterNatives(env, cls, methods, methodCount);
        if (res != JNI_OK) return 1;
#else
        jclass cls = JNI_FindClass(env, sClassName);
        if (!cls) return 1;

        jint res = JNI_RegisterNatives(env, cls, sMethods, methodCount);

        for (size_t i = 0; i < methodCount; ++i) {
            delete[]sMethods[i].name;
            delete[]sMethods[i].signature;
        }
        delete[]sMethods;
        if (res != JNI_OK) return 1;
#endif

        return 0;
    }
}