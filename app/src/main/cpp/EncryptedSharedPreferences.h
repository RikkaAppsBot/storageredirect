#ifndef OBFUSCATED_PREFERENCE_H
#define OBFUSCATED_PREFERENCE_H

#include <jni.h>

namespace EncryptedSharedPreferences {
    int registerNatives(JNIEnv *env);

    void setNames(const char *className, char *obfuscatedName,
                  size_t count, const char *names[], const char *obfuscatedNames[], const char *signatures[]);
}
#endif // OBFUSCATED_PREFERENCE_H
