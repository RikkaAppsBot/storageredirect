#ifndef AES_H
#define AES_H


unsigned char *aes_256_gcm_encrypt(unsigned char *plaintext, size_t plaintext_len,
                                   unsigned char *aad, size_t aad_len,
                                   unsigned char *key,
                                   unsigned char *iv,
                                   size_t *ciphertext_len);

unsigned char *aes_256_gcm_decrypt(unsigned char *ciphertext, size_t ciphertext_len,
                                   unsigned char *aad, size_t aad_len,
                                   unsigned char *key,
                                   size_t *plaintext_len);

#endif // AES_H
