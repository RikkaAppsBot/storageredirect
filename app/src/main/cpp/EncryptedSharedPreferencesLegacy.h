#ifndef OBFUSCATED_PREFERENCE_LEGACY_H
#define OBFUSCATED_PREFERENCE_LEGACY_H

#include <jni.h>

namespace EncryptedSharedPreferencesLegacy {
    int registerNatives(JNIEnv *env);

    void setNames(const char *className, char *obfuscatedName,
                  size_t count, const char *names[], const char *obfuscatedNames[], const char *signatures[]);
}
#endif // OBFUSCATED_PREFERENCE_LEGACY_H
