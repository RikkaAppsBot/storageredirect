#ifndef JNIHELPER_H
#define JNIHELPER_H

#include <jni.h>
#include "logging.h"

#ifdef __cplusplus
extern "C" {
#endif
static int ClearException(JNIEnv *env) {
    jthrowable exception = env->ExceptionOccurred();
    if (exception != nullptr) {
        env->ExceptionDescribe();
        env->ExceptionClear();
        return true;
    }
    return false;
}

#ifdef DEBUG

static inline void PrintJavaObject(JNIEnv *env, const char *format, jobject object) {
    jclass classObject = env->FindClass("java/lang/Object");
    jmethodID objectToString = env->GetMethodID(classObject, "toString", "()Ljava/lang/String;");
    if (object == nullptr) {
        LOGI(format, nullptr);
    } else {
        jstring string = (jstring) env->CallObjectMethod(object, objectToString);
        const char *value = env->GetStringUTFChars(string, nullptr);
        LOGI(format, value);
        env->ReleaseStringUTFChars(string, value);
        env->DeleteLocalRef(string);
    }
    env->DeleteLocalRef(classObject);
}

#else
#define PrintJavaObject(x, y, z) do {} while(0);
#endif

#define JNI_FindClass(env, name) \
    env->FindClass(name); \
    if (ClearException(env)) LOGE("FindClass " #name);

#define JNI_GetObjectClass(env, obj) \
    env->GetObjectClass(obj); \
    if (ClearException(env)) LOGE("GetObjectClass " #obj);

#define JNI_GetFieldID(env, class, name, sig) \
    env->GetFieldID(class, name, sig); \
    if (ClearException(env)) LOGE("GetFieldID " #name);

#define JNI_GetObjectField(env, class, fieldId) \
    env->GetObjectField(class, fieldId); \
    if (ClearException(env)) LOGE("GetObjectField " #fieldId);

#define JNI_GetMethodID(env, class, name, sig) \
    env->GetMethodID(class, name, sig); \
    if (ClearException(env)) LOGE("GetMethodID " #name);

#define JNI_CallObjectMethod(env, obj, ...) \
    env->CallObjectMethod(obj, __VA_ARGS__); \
    if (ClearException(env)) LOGE("CallObjectMethod " #obj " " #__VA_ARGS__);

#define JNI_CallVoidMethod(env, obj, ...) \
    env->CallVoidMethod(obj, __VA_ARGS__); \
    if (ClearException(env)) LOGE("CallVoidMethod " #obj " " #__VA_ARGS__);

#define JNI_GetStaticFieldID(env, class, name, sig) \
    env->GetStaticFieldID(class, name, sig); \
    if (ClearException(env)) LOGE("GetStaticFieldID " #name " " #sig);

#define JNI_GetStaticBooleanField(env, class, fieldId) \
    env->GetStaticBooleanField(class, fieldId); \
    if (ClearException(env)) LOGE("JNI_GetStaticBooleanField " #fieldId);

#define JNI_GetStaticObjectField(env, class, fieldId) \
    env->GetStaticObjectField(class, fieldId); \
    if (ClearException(env)) LOGE("GetStaticObjectField " #fieldId);

#define JNI_GetStaticMethodID(env, class, name, sig) \
    env->GetStaticMethodID(class, name, sig); \
    if (ClearException(env)) LOGE("GetStaticMethodID " #name);

#define JNI_CallStaticVoidMethod(env, obj, ...) \
    env->CallStaticVoidMethod(obj, __VA_ARGS__); \
    if (ClearException(env)) LOGE("CallStaticVoidMethod " #obj " " #__VA_ARGS__);

#define JNI_CallStaticObjectMethod(env, obj, ...) \
    env->CallStaticObjectMethod(obj, __VA_ARGS__); \
    if (ClearException(env)) LOGE("CallStaticObjectMethod " #obj " " #__VA_ARGS__);

#define JNI_GetArrayLength(env, array) \
    env->GetArrayLength(array); \
    if (ClearException(env)) LOGE("GetArrayLength " #array);

#define JNI_NewObject(env, class, ...) \
    env->NewObject(class, __VA_ARGS__); \
    if (ClearException(env)) LOGE("NewObject " #class " " #__VA_ARGS__);

#define JNI_RegisterNatives(env, class, methods, size) \
    env->RegisterNatives(class, methods, size); \
    if (ClearException(env)) LOGE("RegisterNatives " #class);

#define JNI_PushLocalFrame(env, capacity) \
    env->PushLocalFrame(capacity); \
    if (ClearException(env)) LOGE("PushLocalFrame " #capacity);

#define JNI_PopLocalFrame(env, result) \
    env->PopLocalFrame(result); \
    if (ClearException(env)) LOGE("PushLocalFrame");

#define JNI_SetObjectArrayElement(env, array, index, value) \
    env->SetObjectArrayElement(array, index, value); \
    if (ClearException(env)) LOGE("SetObjectArrayElement");

#define JNI_SetStaticIntField(env, class, fieldId, value) \
    env->SetStaticIntField(class, fieldId, value); \
    if (ClearException(env)) LOGE("SetStaticIntField");

#ifdef __cplusplus
};
#endif
#endif // JNIHELPER_H

