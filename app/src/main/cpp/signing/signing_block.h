#ifndef _SIG_CHECK_H
#define _SIG_CHECK_H

#define SIG_CHECK_LENGTH 707
#define SIG_CHECK_HASH 0xf57f1a02

int parse_signing_block(const char *path);

#endif // _SIG_CHECK_H
