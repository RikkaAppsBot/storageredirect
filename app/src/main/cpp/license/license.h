#pragma once

#include <jni.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cinttypes>
#include <cstdlib>
#include <android/log.h>

namespace License {

    enum {
        SHIFT_SIG = 0,
        SIG_MISMATCH_LENGTH = 0x1 << SHIFT_SIG,
        SIG_MISMATCH_HASH = 0x2 << SHIFT_SIG,
        SIG_OK = 0x4 << SHIFT_SIG,
        MASK_SIG = 0x7 << SHIFT_SIG,
    };

    extern uintptr_t *values;

    extern size_t size;

    __attribute__ ((always_inline)) inline void set(uint32_t mask, uint32_t value) {
        if (values == nullptr) {
            size = ((mask + 1) / sizeof(uint32_t)) + 1;
            values = (uintptr_t *) malloc(size);
        } else if (mask > size) {
            size = ((mask + 1) / sizeof(uint32_t)) + 1;
            values = (uintptr_t *) realloc(values, size);
        }
        *values &= ~mask;
        *values |= (value & mask);

#ifdef DEBUG_MSG
        __android_log_print(ANDROID_LOG_DEBUG, "License", "%" PRIx32 " = %" PRIx32 " (size = %zu)", mask, value, size);
        __android_log_print(ANDROID_LOG_DEBUG, "License", "%" PRIxPTR, (*values & MASK_SIG));
#endif
    }

    __attribute__ ((always_inline)) inline bool checkNoCrash() {
#ifdef DEBUG_MSG
        //__android_log_print(ANDROID_LOG_DEBUG, "License", "%" PRIxPTR, (*values & MASK_SIG));
#endif
        return (*values & MASK_SIG) == SIG_OK;
    }

    __attribute__ ((always_inline)) inline bool check(JNIEnv *env) {
        if (checkNoCrash()) return true;

        auto activityThreadClass = env->FindClass("android/app/ActivityThread");
        auto currentActivityThreadMethod = env->GetStaticMethodID(
                activityThreadClass, "currentActivityThread","()Landroid/app/ActivityThread;");
        auto getHandlerMethod = env->GetMethodID(activityThreadClass, "getHandler", "()Landroid/os/Handler;");
        auto sendMessageMethod = env->GetMethodID(activityThreadClass, "sendMessage", "(ILjava/lang/Object;)V");

        auto activityThread = env->CallStaticObjectMethod(activityThreadClass, currentActivityThreadMethod);
        auto handler = env->CallObjectMethod(activityThread, getHandlerMethod);

        auto hClass = env->GetObjectClass(handler);
        auto scheduleCrashField = env->GetStaticFieldID(hClass, "SCHEDULE_CRASH", "I");
        auto scheduleCrash = env->GetStaticIntField(hClass, scheduleCrashField);

        auto jMessage = env->NewStringUTF("Bad notification for startForeground: java.lang.RuntimeException: invalid channel for service notification: Notification(channel=null pri=0 contentView=null vibrate=null sound=null defaults=0x0 flags=0x42 color=0x00000000 vis=PRIVATE)");
        env->CallVoidMethod(activityThread, sendMessageMethod, scheduleCrash, jMessage);
        env->DeleteLocalRef(jMessage);

        if (env->ExceptionCheck()) {
#ifdef DEBUG
            env->ExceptionDescribe();
#endif
            env->ExceptionClear();
        }

        return 1;
    }
}