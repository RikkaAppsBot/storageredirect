#include <jni.h>
#include <cstring>
#include <climits>
#include <sys/system_properties.h>
#include <fcntl.h>
#include <unistd.h>
#include <obfuscate.h>
#include <malloc.h>
#include "misc/opentat.h"
#include "misc/android.h"
#include "crypto/digests.h"
#include "crypto/aes.h"
#include "crypto/base64.h"
#include "JNIHelper.h"
#include "logging.h"
#include "License.h"
#include "license/license.h"

static inline int my_strcmp(const char *s1, const char *s2) {
    while (*s1 == *s2++)
        if (*s1++ == 0)
            return (0);
    return (*(unsigned char *) s1 - *(unsigned char *) --s2);
}

static inline char *my_strcpy(const char *s) {
    size_t len = strlen(s);
    char *res = new char[len + 1];
    memcpy(res, s, len);
    res[len] = '\0';
    return res;
}

namespace EncryptedSharedPreferencesLegacy {

    static auto preference_ase_key = OBF("t0GE4etr4UgJbXRU1j0UecdnjVVQprts");
    static auto preference_base64_table = OBF("jq0snSa+9ymWZVIAE7xUoCeHBg8pRfP4Mruid_kwYlhQFtJONc2vb15LX3Tz6KGD");
    static auto preference_key_google_token = OBF("2VgAwGthzMqFFpHs");
    static auto preference_key_alipay_order = OBF("T8yQS4LatKY0DlGe");
    static auto preference_key_redeem_code = OBF("33tpAXJTbMGV8P0C");
    static auto preference_key_instance_id = OBF("XXRpKf5RijI3x258");

    static const inline int methodCount = 3;

    static const inline int TYPE_GOOGLE = 0;
    static const inline int TYPE_ALIPAY = 1;
    static const inline int TYPE_REDEEM = 2;
    static const inline int TYPE_INSTANCE_ID = 3;

    static char encrypted_key_google[1024] = {0},
            encrypted_key_alipay[1024] = {0},
            encrypted_key_redeem[1024] = {0},
            encrypted_key_instance_id[1024] = {0};

    static unsigned char *createDeviceIdSha256(JNIEnv *env) {
        const char *androidId = android::getAndroidId(env);

        ssize_t maxSize = strlen(androidId) + PROP_VALUE_MAX * 2 + 4096;
        unsigned char deviceId[maxSize];
        unsigned char *pos = deviceId;

        memcpy(pos, androidId, strlen(androidId));
        pos += strlen(androidId);

        LOGD("androidId %s", androidId);

        char prop[PROP_VALUE_MAX] = {0};

        int len;
        if ((len = __system_property_get("ro.product.model", prop)) > 0) {
            memcpy(pos, prop, (size_t) len);
            pos += len;

            LOGD("ro.product.model %s", prop);
        }

        if ((len = __system_property_get("ro.product.manufacturer", prop)) > 0) {
            memcpy(pos, prop, (size_t) len);
            pos += len;
            LOGD("ro.product.manufacturer %s", prop);
        }

        uid_t uid = getuid();
        *pos = (unsigned char) ((uid >> 24) & 0xff);
        *(pos + 1) = (unsigned char) ((uid >> 16) & 0xff);
        *(pos + 2) = (unsigned char) ((uid >> 8) & 0xff);
        *(pos + 3) = (unsigned char) ((uid) & 0xff);
        pos += 4;
        LOGD("uid %d", uid);

        auto *hash = (unsigned char *) malloc(32);
        unsigned int hash_len;
        sha256(deviceId, (pos - deviceId), hash, &hash_len);

        LOGD("deviceId %s", deviceId);
#ifdef DEBUG
        LOGD("android id: %s", androidId);

        size_t base64_len;
        unsigned char *base64 = base64_encode(hash, 32, &base64_len, true,
                                              (const unsigned char *) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        LOGD("device id hash base64: %s", base64);
#endif
        return hash;
    }

    static unsigned char *getDeviceIdHash(JNIEnv *env) {
        static unsigned char *deviceId = nullptr;
        if (!deviceId) {
            deviceId = createDeviceIdSha256(env);
        }
        return deviceId;
    }

    static void generate_aad(const char *in, size_t in_len, unsigned char *out, size_t *out_len) {
        if (in_len > 32) in_len = 32;
        memcpy(out, in, in_len);
        *out_len = in_len;
    }

    static inline void generate_iv(unsigned char *iv, const unsigned char *plaintext, size_t plaintext_len) {
        int i = 0;
        while (i < 12) {
            iv[i] = plaintext[i % plaintext_len] ^ (unsigned char) (i % 12 + 1);
            i++;
        }
    }

    static unsigned char *encrypt_to_base64(unsigned char *plaintext, size_t plaintext_len, unsigned char *aad, size_t aad_len) {
        unsigned char iv[12];
        generate_iv(iv, plaintext, plaintext_len);

        size_t ciphertext_len;
        unsigned char *ciphertext = aes_256_gcm_encrypt(
                plaintext, plaintext_len,
                aad, aad_len,
                (unsigned char *) preference_ase_key.get(),
                iv, &ciphertext_len);

        if (ciphertext) {
            size_t base64_len;
            unsigned char *base64 = base64_encode(ciphertext, ciphertext_len, &base64_len, true,
                                                  (const unsigned char *) preference_base64_table.get());
            free(ciphertext);
            return base64;
        }
        return nullptr;
    }

    static jstring encrypt(JNIEnv *env, jobject object, jbyteArray jPlainText, jstring jAAD, jboolean jAADIsBase64) {
        auto plaintext_len = (size_t) env->GetArrayLength(jPlainText);
        unsigned char plaintext[plaintext_len];
        env->GetByteArrayRegion(jPlainText, 0, (jsize) plaintext_len, (jbyte *) &plaintext);

        License::check(env);

        const char *aad_in;
        size_t aad_in_len = 0;
        if (jAAD) {
            if (jAADIsBase64) {
                const char *aad_base64 = env->GetStringUTFChars(jAAD, nullptr);
                aad_in_len = static_cast<size_t>(env->GetStringLength(jAAD));
                aad_in = (const char *) base64_decode((const unsigned char *) aad_base64, strlen(aad_base64), &aad_in_len, true,
                                                      (const unsigned char *) preference_base64_table.get());
            } else {
                aad_in = env->GetStringUTFChars(jAAD, nullptr);
                aad_in_len = static_cast<size_t>(env->GetStringLength(jAAD));
            }
        } else {
            aad_in = (const char *) getDeviceIdHash(env);
            aad_in_len = 32;
        }

        unsigned char aad[32];
        size_t aad_len;
        generate_aad(aad_in, aad_in_len, aad, &aad_len);

        if (jAAD) env->ReleaseStringUTFChars(jAAD, aad_in);

        auto base64 = encrypt_to_base64(plaintext, plaintext_len, aad, aad_len);
        if (base64) {
            jstring res = env->NewStringUTF((const char *) base64);
            free(base64);
            return res;
        }

        env->ThrowNew(env->FindClass("java/lang/Exception"), "failed to encrypt");
        return nullptr;
    }

    static void init_license_keys(JNIEnv *env) {
        const char *aad_in = (const char *) getDeviceIdHash(env);
        size_t aad_in_len = 32;
        unsigned char aad[32];
        size_t aad_len;
        generate_aad(aad_in, aad_in_len, aad, &aad_len);

        unsigned char *base64;
        base64 = encrypt_to_base64((unsigned char *) preference_key_google_token.get(), strlen(preference_key_google_token.get()), aad, aad_len);
        memcpy(encrypted_key_google, base64, 1024);
        if (base64) free(base64);

        base64 = encrypt_to_base64((unsigned char *) preference_key_alipay_order.get(), strlen(preference_key_alipay_order.get()), aad, aad_len);
        memcpy(encrypted_key_alipay, base64, 1024);
        if (base64) free(base64);

        base64 = encrypt_to_base64((unsigned char *) preference_key_redeem_code.get(), strlen(preference_key_redeem_code.get()), aad, aad_len);
        memcpy(encrypted_key_redeem, base64, 1024);
        if (base64) free(base64);

        base64 = encrypt_to_base64((unsigned char *) preference_key_instance_id.get(), strlen(preference_key_instance_id.get()), aad, aad_len);
        memcpy(encrypted_key_instance_id, base64, 1024);
        if (base64) free(base64);

        LOGD("google=%s", encrypted_key_google);
        LOGD("alipay=%s", encrypted_key_alipay);
        LOGD("redeem=%s", encrypted_key_redeem);
        LOGD("instance_id=%s", encrypted_key_instance_id);
    }

    static jbyteArray decrypt(JNIEnv *env, jobject object, jstring jBase64CipherText, jstring jAAD, jboolean jAADIsBase64) {
        int license_type = -1;

        const char *aad_in;
        size_t aad_in_len = 0;
        if (jAAD) {
            if (jAADIsBase64) {
                const char *aad_base64 = env->GetStringUTFChars(jAAD, nullptr);
                aad_in_len = static_cast<size_t>(env->GetStringLength(jAAD));
                aad_in = (const char *) base64_decode((const unsigned char *) aad_base64, strlen(aad_base64), &aad_in_len, true,
                                                      (const unsigned char *) preference_base64_table.get());
            } else {
                aad_in = env->GetStringUTFChars(jAAD, nullptr);
                aad_in_len = static_cast<size_t>(env->GetStringLength(jAAD));
            }

            static bool license_keys_initialized = false;
            if (!license_keys_initialized) {
                init_license_keys(env);
                license_keys_initialized = true;
            }

            if (my_strcmp(aad_in, encrypted_key_google) == 0) {
                license_type = TYPE_GOOGLE;
            } else if (my_strcmp(aad_in, encrypted_key_alipay) == 0) {
                license_type = TYPE_ALIPAY;
            } else if (my_strcmp(aad_in, encrypted_key_redeem) == 0) {
                license_type = TYPE_REDEEM;
            } else if (my_strcmp(aad_in, encrypted_key_instance_id) == 0) {
                license_type = TYPE_INSTANCE_ID;
            }
        } else {
            aad_in = (const char *) getDeviceIdHash(env);
            aad_in_len = 32;
        }

        if (!jBase64CipherText) {
            if (license_type != -1) {
                License::setLicense(env, license_type, nullptr);
            }
            return nullptr;
        }

        const char *base64 = env->GetStringUTFChars(jBase64CipherText, nullptr);
        auto base64_len = static_cast<size_t>(env->GetStringLength(jBase64CipherText));

        unsigned char aad[32];
        size_t aad_len;
        generate_aad(aad_in, aad_in_len, aad, &aad_len);

        if (jAAD) env->ReleaseStringUTFChars(jAAD, aad_in);

        size_t ciphertext_len = 0;
        unsigned char *ciphertext = base64_decode((const unsigned char *) base64, base64_len, &ciphertext_len, true,
                                                  (const unsigned char *) preference_base64_table.get());

        env->ReleaseStringUTFChars(jBase64CipherText, base64);

        size_t plaintext_len = 0;
        unsigned char *plaintext;
        if (ciphertext) {
            plaintext = aes_256_gcm_decrypt(
                    ciphertext, ciphertext_len,
                    aad, aad_len,
                    (unsigned char *) preference_ase_key.get(),
                    &plaintext_len);

            free(ciphertext);

            if (plaintext) {
                jbyteArray res = env->NewByteArray((jsize) plaintext_len);
                env->SetByteArrayRegion(res, 0, (jsize) plaintext_len, (const jbyte *) plaintext);

                if (license_type != -1) {
                    License::setLicense(env, license_type, plaintext);
                }

                free(plaintext);
                return res;
            }
        }

        env->ThrowNew(env->FindClass("java/lang/Exception"), "failed to decrypt");
        return nullptr;
    }

    static inline unsigned char *get_default_add(JNIEnv *env, size_t *base64_len) {
        unsigned char *deivceIdHash = getDeviceIdHash(env);
        unsigned char *base64 = base64_encode(deivceIdHash, 32, base64_len, true, (const unsigned char *) preference_base64_table.get());
        return base64;
    }

    static jstring getDefaultAAD(JNIEnv *env, jobject object) {
        size_t base64_len;
        unsigned char *base64 = get_default_add(env, &base64_len);
        jstring res = env->NewStringUTF((const char *) base64);
        free(base64);
        return res;
    }

    static char sClassName[256];
    static JNINativeMethod *sMethods;

    void setNames(const char *className, char *obfuscatedName, size_t count, const char *names[], const char *obfuscatedNames[],
                  const char *signatures[]) {
        if (my_strcmp(className, "moe/shizuku/redirectstorage/utils/EncryptedSharedPreferencesLegacy") != 0) return;

        memcpy(sClassName, obfuscatedName, strlen(className));
        sClassName[strlen(obfuscatedName)] = '\0';

        sMethods = new JNINativeMethod[methodCount];

        for (size_t i = 0; i < count; i++) {
            void *func;
            if (my_strcmp(names[i], "encrypt") == 0
                && my_strcmp(signatures[i], "([BLjava/lang/String;Z)Ljava/lang/String;") == 0)
                func = (void *) encrypt;
            else if (my_strcmp(names[i], "decrypt") == 0
                     && my_strcmp(signatures[i], "(Ljava/lang/String;Ljava/lang/String;Z)[B") == 0)
                func = (void *) decrypt;
            else if (my_strcmp(names[i], "getDefaultAAD") == 0
                     && my_strcmp(signatures[i], "()Ljava/lang/String;") == 0)
                func = (void *) getDefaultAAD;
            else
                continue;

            sMethods[i] = {my_strcpy(obfuscatedNames[i]), my_strcpy(signatures[i]), func};
        }
    }

    int registerNatives(JNIEnv *env) {
#ifdef DEBUG
        jclass cls = JNI_FindClass(env, "moe/shizuku/redirectstorage/utils/EncryptedSharedPreferencesLegacy")
        if (!cls) return 1;

        JNINativeMethod methods[methodCount] = {
                {"encrypt",       "([BLjava/lang/String;Z)Ljava/lang/String;", (void *) encrypt},
                {"decrypt",       "(Ljava/lang/String;Ljava/lang/String;Z)[B", (void *) decrypt},
                {"getDefaultAAD", "()Ljava/lang/String;",                      (void *) getDefaultAAD},
        };

        jint res = JNI_RegisterNatives(env, cls, methods, methodCount)
        if (res != JNI_OK) return 1;
#else
        jclass cls = JNI_FindClass(env, sClassName);
        if (!cls) return 1;

        jint res = JNI_RegisterNatives(env, cls, sMethods, methodCount);

        for (size_t i = 0; i < methodCount; ++i) {
            delete[]sMethods[i].name;
            delete[]sMethods[i].signature;
        }
        delete[]sMethods;
        if (res != JNI_OK) return 1;
#endif

        return 0;
    }
}