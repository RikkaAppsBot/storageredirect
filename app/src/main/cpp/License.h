#ifndef LICENSE_H
#define LICENSE_H

namespace License {

    int registerNatives(JNIEnv *env);

    void setNames(const char *className, char *obfuscatedName,
                  size_t count, const char *names[], const char *obfuscatedNames[], const char *signatures[]);

    void setLicense(JNIEnv *env, int type, const unsigned char *bytes);
}
#endif // LICENSE_H
