#include <jni.h>
#include <JNIHelper.h>
#include <misc/android.h>
#include <cstring>
#include <license/license.h>

namespace Application {

    static const inline int methodCount = 1;
    static char sClassName[256];
    static JNINativeMethod *sMethods;

    static inline int my_strcmp(const char *s1, const char *s2) {
        while (*s1 == *s2++)
            if (*s1++ == 0)
                return (0);
        return (*(unsigned char *) s1 - *(unsigned char *) --s2);
    }

    static inline char *my_strcpy(const char *s) {
        size_t len = strlen(s);
        char *res = new char[len + 1];
        memcpy(res, s, len);
        res[len] = '\0';
        return res;
    }

    static void attachBaseContext(JNIEnv *env, jobject object) {
        android::getAndroidId(env, object);
    }

    void setNames(const char *className, char *obfuscatedName, size_t count, const char *names[], const char *obfuscatedNames[],
                  const char *signatures[]) {
        if (my_strcmp(className, "moe/shizuku/redirectstorage/SRApplication") != 0) return;

        memcpy(sClassName, obfuscatedName, strlen(className));
        sClassName[strlen(obfuscatedName)] = '\0';

        sMethods = new JNINativeMethod[methodCount];

        for (size_t i = 0; i < count; i++) {
            void *func;
            if (my_strcmp(names[i], "attachBaseContext") == 0 && my_strcmp(signatures[i], "()V") == 0)
                func = (void *) attachBaseContext;
            else
                continue;

            sMethods[i] = {my_strcpy(obfuscatedNames[i]), my_strcpy(signatures[i]), func};
        }
    }

    int registerNatives(JNIEnv *env) {
#ifdef DEBUG
        jclass cls = JNI_FindClass(env, "moe/shizuku/redirectstorage/SRApplication")
        if (!cls) return 1;

        JNINativeMethod methods[methodCount] = {
                {"attachBaseContext", "()V", (void *) attachBaseContext},
        };

        jint res = JNI_RegisterNatives(env, cls, methods, methodCount);
        if (res != JNI_OK) return 1;
#else
        jclass cls = JNI_FindClass(env, sClassName);
        if (!cls) return 1;

        jint res = JNI_RegisterNatives(env, cls, sMethods, methodCount);

        for (size_t i = 0; i < methodCount; ++i) {
            delete[]sMethods[i].name;
            delete[]sMethods[i].signature;
        }
        delete[]sMethods;
        if (res != JNI_OK) return 1;
#endif

        License::check(env);

        return 0;
    }
}