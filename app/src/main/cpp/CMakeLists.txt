cmake_minimum_required(VERSION 3.4.1)

set(LIBRARY_NAME "sr")

set(LINKER_FLAGS "-Wl,--hash-style=both")
set(C_FLAGS "-Werror=format -fno-exceptions -fno-rtti")

if (ANDROID_ABI STREQUAL "arm64-v8a")
    set(LINKER_FLAGS "${LINKER_FLAGS} -ffixed-x18")
    set(C_FLAGS "${C_FLAGS} -ffixed-x18")
endif ()

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    message("Builing Release...")

    set(LINKER_FLAGS "${LINKER_FLAGS} -Wl,-exclude-libs,ALL -Wl,--gc-sections")
    set(C_FLAGS "${C_FLAGS} -fvisibility=hidden -fvisibility-inlines-hidden")

    if (DEFINED TOOLCHAIN_LLVM_HIKARI)
        message("Use Hikari...")
        set(C_FLAGS "${C_FLAGS} -O0")
        set(C_FLAGS "${C_FLAGS} -mllvm -enable-bcfobf")
        set(C_FLAGS "${C_FLAGS} -mllvm -enable-cffobf")
        set(C_FLAGS "${C_FLAGS} -mllvm -enable-strcry")
    else ()
        set(C_FLAGS "${C_FLAGS} -O2")
        #add_definitions(-DDEBUG_MSG)
    endif ()
elseif (CMAKE_BUILD_TYPE STREQUAL "Debug")
    message("Builing Debug...")

    add_definitions(-DDEBUG)
    add_definitions(-DDEBUG_MSG)
    set(C_FLAGS "${C_FLAGS} -O0")
endif ()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${C_FLAGS}")

set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${LINKER_FLAGS}")
set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${LINKER_FLAGS}")

include_directories(include)
include_directories(.)

find_package(curl REQUIRED CONFIG)
find_package(xposeddetector REQUIRED CONFIG)

add_library(${LIBRARY_NAME} SHARED
        main.cpp
        signing/pm.c
        signing/signing_block.cpp
        misc/opentat.c
        misc/misc.cpp
        misc/android.cpp
        crypto/base64.c
        crypto/aes.cpp
        crypto/digests.cpp
        EncryptedSharedPreferences.cpp
        EncryptedSharedPreferencesLegacy.cpp
        Application.cpp
        License.cpp
        bypass.cpp
        misc/pmparser.c
        license/license.cpp
        tinynew.cpp)

find_library(log-lib log)
target_link_libraries(
        ${LIBRARY_NAME}
        ${log-lib}
        curl::curl
        boringssl::crypto
        boringssl::ssl
        xposeddetector::xposed_detector)

add_custom_command(TARGET ${LIBRARY_NAME} POST_BUILD
        COMMAND ${CMAKE_STRIP} --remove-section=.comment "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/lib${LIBRARY_NAME}.so")
