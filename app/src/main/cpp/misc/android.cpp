#include <jni.h>
#include <JNIHelper.h>
#include <cstring>
#include <sys/system_properties.h>
#include <climits>
#include <fcntl.h>
#include <unistd.h>
#include "logging.h"
#include "misc.h"

namespace android {

#ifdef DEBUG

    static inline void debug(JNIEnv *env, const char *prefix, jobject object) {
        jclass classObject = env->FindClass("java/lang/Object");
        jmethodID objectToString = env->GetMethodID(classObject, "toString", "()Ljava/lang/String;");
        if (object == nullptr) {
            LOGI(prefix, "(null)");
        } else {
            auto string = (jstring) env->CallObjectMethod(object, objectToString);
            const char *value = env->GetStringUTFChars(string, nullptr);
            LOGI(prefix, value);
            env->ReleaseStringUTFChars(string, value);
            env->DeleteLocalRef(string);
        }
        env->DeleteLocalRef(classObject);
    }

#else
#define debug(x, y, z) do {} while(0);
#endif

    static const char *androidId = nullptr;

    static jobject getApplicationContext(JNIEnv *env) {
        jclass activityThread = JNI_FindClass(env, "android/app/ActivityThread")
        jmethodID currentActivityThread = JNI_GetStaticMethodID(env, activityThread, "currentActivityThread", "()Landroid/app/ActivityThread;")
        jobject at = JNI_CallStaticObjectMethod(env, activityThread, currentActivityThread)
        debug(env, "currentActivityThread: %s", at);

        jmethodID getApplication = JNI_GetMethodID(env, activityThread, "getApplication", "()Landroid/app/Application;")
        jobject context = JNI_CallObjectMethod(env, at, getApplication)
        debug(env, "getApplication: %s", context);

        return context;
    }

    static jobject getSystemContext(JNIEnv *env) {
        jclass activityThread = JNI_FindClass(env, "android/app/ActivityThread")
        jmethodID currentActivityThread = JNI_GetStaticMethodID(env, activityThread, "currentActivityThread", "()Landroid/app/ActivityThread;")
        jobject at = JNI_CallStaticObjectMethod(env, activityThread, currentActivityThread)
        debug(env, "currentActivityThread: %s", at);

        jmethodID getSystemContext = JNI_GetMethodID(env, activityThread, "getSystemContext", "()Landroid/app/ContextImpl;")
        jobject context = JNI_CallObjectMethod(env, at, getSystemContext)
        debug(env, "getSystemContext: %s", context);

        return context;
    }

    const char *getAndroidId(JNIEnv *env, jobject context = nullptr) {
        if (androidId) {
            return androidId;
        }

        if (context == nullptr) {
            context = getApplicationContext(env);
        }
        debug(env, "context: %s", context);

        if (context) {
            jclass contextClass = JNI_GetObjectClass(env, context)
            jmethodID getContentResolver = JNI_GetMethodID(env, contextClass, "getContentResolver", "()Landroid/content/ContentResolver;")
            jobject contentResolver = env->CallObjectMethod(context, getContentResolver);
            debug(env, "contentResolver: %s", contentResolver);

            jclass settingsClass = JNI_FindClass(env, "android/provider/Settings$Secure")
            jmethodID methodId = JNI_GetStaticMethodID(env, settingsClass, "getString",
                                                       "(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;")

            jstring jANDROID_ID = env->NewStringUTF("android_id");
            auto jAndroidId = (jstring) JNI_CallStaticObjectMethod(env, settingsClass, methodId, contentResolver, jANDROID_ID)
            debug(env, "jAndroidId: %s", jAndroidId);

            env->DeleteLocalRef(jANDROID_ID);

            if (jAndroidId) {
                auto cAndroidId = env->GetStringUTFChars(jAndroidId, nullptr);
                androidId = strdup(cAndroidId);
                env->ReleaseStringUTFChars(jAndroidId, cAndroidId);
            }
        }

        ClearException(env);

        if (androidId) {
            return androidId;
        }
        return "__NULL__";
    }

    int GetApiLevel() {
        static int apiLevel = 0;
        if (apiLevel > 0) return apiLevel;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("ro.build.version.sdk", buf) > 0)
            apiLevel = atoi(buf);

        return apiLevel;
    }

    int GetPreviewApiLevel() {
        static int previewApiLevel = 0;
        if (previewApiLevel > 0) return previewApiLevel;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("ro.build.version.preview_sdk", buf) > 0)
            previewApiLevel = atoi(buf);

        return previewApiLevel;
    }

    bool IsFilesystemSupported(const char *name) {
        int fd = open("/proc/filesystems", O_RDONLY);
        if (fd == -1) return false;

        bool res = false;
        char buf[PATH_MAX];
        while (fdgets(buf, PATH_MAX, fd) > 0) {
            if (strstr(buf, name)) {
                res = true;
                break;
            }
        }
        close(fd);
        return res;
    }

    bool IsSdcardfsUsed() {
        if (GetApiLevel() < 30) return true;

        static bool res = false;
        static bool init = false;
        if (init) return res;
        init = true;

        char buf[PROP_VALUE_MAX + 1];
        if (__system_property_get("external_storage.sdcardfs.enabled", buf) <= 0)
            strcpy(buf, "true");

        res = IsFilesystemSupported("sdcardfs") && strcmp("true", buf) == 0;
        return res;
    }
}