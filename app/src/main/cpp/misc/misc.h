#ifndef NOPEEKING_MISC_H
#define NOPEEKING_MISC_H

ssize_t fdgets(char *buf, const size_t size, int fd);

int read_full(int fd, void *buf, size_t count);

int write_full(int fd, const void *buf, size_t count);

uintptr_t memsearch(const uintptr_t start, const uintptr_t end, const void *value, size_t size);

#endif //NOPEEKING_MISC_H
