#ifndef ANDROID_H
#define ANDROID_H

#include <jni.h>

namespace android {

    const char *getAndroidId(JNIEnv *env, jobject context = nullptr);

    bool IsSdcardfsUsed();
}
#endif //ANDROID_H
