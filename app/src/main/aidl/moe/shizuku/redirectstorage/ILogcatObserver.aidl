package moe.shizuku.redirectstorage;

import moe.shizuku.redirectstorage.model.LogcatItem;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

interface ILogcatObserver {

    oneway void onNewLines(in ParceledListSlice items);
}