package moe.shizuku.redirectstorage;

import moe.shizuku.redirectstorage.ILogcatObserver;
import moe.shizuku.redirectstorage.model.LogcatItem;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

interface ILogcatService {

    boolean isRunning();

    void setIndex(int index);

    void registerObserver(in ILogcatObserver observer);

    void unregisterObserver(in ILogcatObserver observer);

}
