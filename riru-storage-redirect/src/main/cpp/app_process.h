#ifndef HOOK_H
#define HOOK_H

#include <jni.h>
#include "dex_file.h"

namespace AppProcess {

    void main(
            JNIEnv *env, const char *package_name, int user,
            int enable_file_monitor, int enable_fix_rename,
            int enable_dex_inject, DexFile *dexFile, jobjectArray args);
}

#endif // HOOK_H
