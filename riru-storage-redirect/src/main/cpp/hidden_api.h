#pragma once

#include <jni.h>
#include <stddef.h>

namespace HiddenApi {

    void init(JNIEnv *env);
    void setHiddenApiExemptions(JNIEnv *env, const char** signaturePrefixes, size_t size);
}