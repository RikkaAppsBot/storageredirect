#include <jni.h>
#include "logging.h"
#include "bridge_service.h"

namespace BridgeService {

// sync with StorageIsolationBridgeService.java
#define BRIDGE_SERVICE_DESCRIPTOR "android.app.IActivityManager"
#define BRIDGE_SERVICE_NAME "activity"
#define BRIDGE_ACTION_GET_BINDER 2

// sync with IService.aidl
#define DESCRIPTOR "moe.shizuku.redirectstorage.IService"
#define TRANSACTION_insertFileMonitor 10001
#define TRANSACTION_requestCheckProcess 10002

    static jclass serviceManagerClass;
    static jmethodID getServiceMethod;

    static jmethodID transactMethod;

    static jclass parcelClass;
    static jmethodID obtainMethod;
    static jmethodID recycleMethod;
    static jmethodID writeInterfaceTokenMethod;
    static jmethodID writeIntMethod;
    static jmethodID writeStringMethod;
    static jmethodID readExceptionMethod;
    static jmethodID readStrongBinderMethod;
    static jmethodID createStringArray;

    static jclass deadObjectExceptionClass;

    static bool ok = false;

    void init(JNIEnv *env) {
        static bool init = false;
        if (init) return;
        init = true;

        // ServiceManager
        auto serviceManagerClass_ = env->FindClass("android/os/ServiceManager");
        if (serviceManagerClass_) {
            serviceManagerClass = (jclass) env->NewGlobalRef(serviceManagerClass_);
        } else {
            env->ExceptionClear();
            return;
        }
        getServiceMethod = env->GetStaticMethodID(serviceManagerClass, "getService", "(Ljava/lang/String;)Landroid/os/IBinder;");
        if (!getServiceMethod) {
            env->ExceptionClear();
            return;
        }

        // IBinder
        jclass iBinderClass = env->FindClass("android/os/IBinder");
        transactMethod = env->GetMethodID(iBinderClass, "transact", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z");

        // Parcel
        auto parcelClass_ = env->FindClass("android/os/Parcel");
        if (parcelClass_) parcelClass = (jclass) env->NewGlobalRef(parcelClass_);
        obtainMethod = env->GetStaticMethodID(parcelClass, "obtain", "()Landroid/os/Parcel;");
        recycleMethod = env->GetMethodID(parcelClass, "recycle", "()V");
        writeInterfaceTokenMethod = env->GetMethodID(parcelClass, "writeInterfaceToken", "(Ljava/lang/String;)V");
        writeIntMethod = env->GetMethodID(parcelClass, "writeInt", "(I)V");
        writeStringMethod = env->GetMethodID(parcelClass, "writeString", "(Ljava/lang/String;)V");
        readExceptionMethod = env->GetMethodID(parcelClass, "readException", "()V");
        readStrongBinderMethod = env->GetMethodID(parcelClass, "readStrongBinder", "()Landroid/os/IBinder;");
        createStringArray = env->GetMethodID(parcelClass, "createStringArray", "()[Ljava/lang/String;");

        auto deadObjectExceptionClass_ = env->FindClass("android/os/DeadObjectException");
        if (deadObjectExceptionClass_) deadObjectExceptionClass = (jclass) env->NewGlobalRef(deadObjectExceptionClass_);

        ok = serviceManagerClass != nullptr;
    }

    static jobject storageIsolationBinder = nullptr;

    static jobject requireBinder(JNIEnv *env, bool force = false) {
        /*
         * private static IBinder requireBinder() {
            if (storageIsolationBinder != null) return storageIsolationBinder;

            IBinder binder = ServiceManager.getService(BRIDGE_SERVICE_NAME);
            if (binder == null) return null;

            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            try {
                data.writeInterfaceToken(BRIDGE_SERVICE_DESCRIPTOR);
                data.writeInt(ACTION_GET_BINDER);
                binder.transact(BRIDGE_TRANSACTION_CODE, data, reply, 0);
                reply.readException();
                IBinder received = reply.readStrongBinder();
                if (received != null) {
                    storageIsolationBinder = received;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                data.recycle();
                reply.recycle();
            }
            return storageIsolationBinder;
        }
        */

        if (storageIsolationBinder && !force) return storageIsolationBinder;

        jstring bridgeServiceName, descriptor;
        jobject bridgeService, data, reply, service;
        jboolean res;

        bridgeServiceName = env->NewStringUTF(BRIDGE_SERVICE_NAME);
        bridgeService = env->CallStaticObjectMethod(serviceManagerClass, getServiceMethod, bridgeServiceName);
        if (!bridgeService) {
            LOGD("can't get %s", BRIDGE_SERVICE_NAME);
            goto clean_bridgeServiceName;
        }

        data = env->CallStaticObjectMethod(parcelClass, obtainMethod);
        reply = env->CallStaticObjectMethod(parcelClass, obtainMethod);

        descriptor = env->NewStringUTF(BRIDGE_SERVICE_DESCRIPTOR);
        env->CallVoidMethod(data, writeInterfaceTokenMethod, descriptor);
        env->CallVoidMethod(data, writeIntMethod, BRIDGE_ACTION_GET_BINDER);

        res = env->CallBooleanMethod(bridgeService, transactMethod, BRIDGE_TRANSACTION_CODE, data, reply, 0);

        if (env->ExceptionCheck()) {
            env->ExceptionClear();
            goto clean;
        }

        if (res) {
            env->CallVoidMethod(reply, readExceptionMethod);
            if (env->ExceptionCheck()) {
                env->ExceptionClear();
                goto clean;
            }
            service = env->CallObjectMethod(reply, readStrongBinderMethod);
            if (service != nullptr) {
                if (storageIsolationBinder) env->DeleteGlobalRef(storageIsolationBinder);
                storageIsolationBinder = env->NewGlobalRef(service);
            }
        } else {
            LOGD("no reply");
        }

        if (env->ExceptionCheck()) {
            env->ExceptionClear();
            goto clean;
        }

        clean:
        env->CallVoidMethod(data, recycleMethod);
        env->CallVoidMethod(reply, recycleMethod);

        env->DeleteLocalRef(descriptor);
        env->DeleteLocalRef(reply);
        env->DeleteLocalRef(data);
        env->DeleteLocalRef(bridgeService);

        clean_bridgeServiceName:
        env->DeleteLocalRef(bridgeServiceName);

        return storageIsolationBinder;
    }

    static jboolean tryTransact(JNIEnv *env, jint code, jobject data, jobject reply, jint flags, bool retry = true) {
        auto binder = requireBinder(env);
        if (!binder) {
            LOGE("binder is null");
            return JNI_FALSE;
        }

        auto res = env->CallBooleanMethod(binder, transactMethod, code, data, reply, flags);

        auto exception = env->ExceptionOccurred();
        if (exception) {
            env->ExceptionClear();
            auto isDeadObjectException = env->IsInstanceOf(exception, deadObjectExceptionClass);
            env->DeleteLocalRef(exception);

            if (isDeadObjectException && retry) {
                res = tryTransact(env, code, data, reply, flags, false);
            }
        }

        return res;
    }

    void insertFileMonitor(JNIEnv *env, const char *packageName, const char *func, const char *path) {
        /*
         *  IBinder binder = requireBinder();
            Parcel data = Parcel.obtain();
            try {
                data.writeInterfaceToken(DESCRIPTOR);
                data.writeString(Contexts.getPackageName());
                data.writeString(path);
                data.writeString(func);
                binder.transact(TRANSACTION_insertFileMonitor, data, null, IBinder.FLAG_ONEWAY);
            } catch (Throwable e) {
                LOGGER.w(e, "insertFileMonitor");
            } finally {
                data.recycle();
            }
         */

        if (!ok) return;

        // TODO reflect context

        jstring descriptor, jPackageName, jFunc, jPath;
        jobject data, reply = nullptr;

        data = env->CallStaticObjectMethod(parcelClass, obtainMethod);

        descriptor = env->NewStringUTF(DESCRIPTOR);
        env->CallVoidMethod(data, writeInterfaceTokenMethod, descriptor);
        jPackageName = packageName ? env->NewStringUTF(packageName) : nullptr;
        if (env->ExceptionCheck()) {
            env->ExceptionClear();
        }
        env->CallVoidMethod(data, writeStringMethod, jPackageName);
        jPath = env->NewStringUTF(path);
        env->CallVoidMethod(data, writeStringMethod, jPath);
        jFunc = env->NewStringUTF(func);
        env->CallVoidMethod(data, writeStringMethod, jFunc);

        tryTransact(env, TRANSACTION_insertFileMonitor, data, reply, 1 /* FLAG_ONEWAY */);

        env->CallVoidMethod(data, recycleMethod);

        env->DeleteLocalRef(jFunc);
        env->DeleteLocalRef(jPath);
        env->DeleteLocalRef(jPackageName);
        env->DeleteLocalRef(descriptor);
        env->DeleteLocalRef(data);
    }

    jobjectArray requestCheckProcess(JNIEnv *env, const char *packageName) {
        /*
         *  IBinder binder = requireBinder();
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            try {
                data.writeInterfaceToken(DESCRIPTOR);
                data.writeString(Contexts.getPackageName());
                binder.transact(TRANSACTION_requestCheckProcess, data, reply, 0);
                reply.readException();
                reply.createStringArray();
            } catch (Throwable e) {
                LOGGER.w(e, "insertFileMonitor");
            } finally {
                data.recycle();
                reply.recycle();
            }
         */

        if (!ok) return nullptr;

        jstring descriptor, jPackageName;
        jobject data, reply;
        jobjectArray array = nullptr;

        data = env->CallStaticObjectMethod(parcelClass, obtainMethod);
        reply = env->CallStaticObjectMethod(parcelClass, obtainMethod);

        descriptor = env->NewStringUTF(DESCRIPTOR);
        env->CallVoidMethod(data, writeInterfaceTokenMethod, descriptor);
        env->CallVoidMethod(data, writeIntMethod, RIRU_MODULE_VERSION);
        jPackageName = packageName ? env->NewStringUTF(packageName) : nullptr;
        if (env->ExceptionCheck()) {
            env->ExceptionClear();
        }
        env->CallVoidMethod(data, writeStringMethod, jPackageName);

        auto res = tryTransact(env, TRANSACTION_requestCheckProcess, data, reply, 0);

        if (res) {
            env->CallVoidMethod(reply, readExceptionMethod);
            if (env->ExceptionCheck()) {
                env->ExceptionClear();
                goto clean;
            }
            array = (jobjectArray) env->CallObjectMethod(reply, createStringArray);
            if (env->ExceptionCheck()) {
                env->ExceptionClear();
                goto clean;
            }
        } else {
            LOGD("no reply");
        }

        clean:
        env->CallVoidMethod(data, recycleMethod);
        env->CallVoidMethod(reply, recycleMethod);

        env->DeleteLocalRef(jPackageName);
        env->DeleteLocalRef(descriptor);
        env->DeleteLocalRef(reply);
        env->DeleteLocalRef(data);

        return array;
    }
}