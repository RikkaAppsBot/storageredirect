#define LOG_TAG    "StorageRedirectStarter"

#include <cstring>
#include <cerrno>
#include <android/log.h>
#include <sys/system_properties.h>
#include <dirent.h>
#include <zconf.h>
#include <sys/stat.h>
#include <asm-generic/fcntl.h>
#include <fcntl.h>
#include "cgroup.h"
#include "misc.h"
#include "logging.h"

#define BIN_PATH    "/data/adb/storage-isolation/bin"

int main(int argc, char **argv) {
    daemon(0, 0);

    pid_t pid = fork();
    if (pid > 0)
        return 0;
    else if (pid < 0)
        return 1;

    LOGD("waiting for zygote");

#ifdef __LP64__
    static const char* zygote_name = "zygote64";
#else
    static const char* zygote_name = "zygote";
#endif

    while (true) {
        static pid_t zygote_pid = -1;
        foreach_proc([](pid_t pid) -> bool {
            if (pid == getpid()) return false;

            char buf[64];
            snprintf(buf, 64, "/proc/%d/cmdline", pid);

            int fd = open(buf, O_RDONLY);
            if (fd > 0) {
                memset(buf, 0, 64);
                if (read(fd, buf, 64) > 0 && strcmp(zygote_name, buf) == 0) {
                    zygote_pid = pid;
                }
                close(fd);
            }
            return zygote_pid != -1;
        });

        if (zygote_pid != -1) {
            LOGD("found %s %d", zygote_name, zygote_pid);
            break;
        }

        LOGD("%s not started, wait 1s...", zygote_name);
        sleep(1);
    }

    int daemon_version = 0;
    char daemon_path[PATH_MAX] = {0};

    DIR *dir;
    struct dirent *entry;

    while (true) {
        if ((dir = opendir(BIN_PATH)) == nullptr) {
            LOGW("%s not exists, wait 1s", BIN_PATH);
            sleep(1);
            continue;
        }

        while ((entry = readdir(dir))) {
            if (entry->d_type != DT_REG) {
                continue;
            }
            int version;
            if (sscanf(entry->d_name, "daemon-v%d", &version) == 1) {
                if (version > daemon_version) {
                    daemon_version = version;
                    snprintf(daemon_path, PATH_MAX, "%s/%s", BIN_PATH, entry->d_name);
                    LOGI("found %s", daemon_path);
                }
            }
        }

        closedir(dir);

        if (daemon_version) {
            break;
        }
    }

    LOGI("execute %s...", daemon_path);

    if (0 != execl(daemon_path, daemon_path, "--from-module", nullptr)) {
        PLOGE("excel %s", daemon_path);
        return 1;
    }
    return 0;
}