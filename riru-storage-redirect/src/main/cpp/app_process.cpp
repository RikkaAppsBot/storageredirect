#include <cstdio>
#include <cstring>
#include <chrono>
#include <fcntl.h>
#include <unistd.h>
#include <sys/vfs.h>
#include <sys/stat.h>
#include <dirent.h>
#include <xhook.h>
#include <jni.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <mntent.h>
#include <sys/mount.h>
#include <sys/sendfile.h>
#include <dlfcn.h>

#include "logging.h"
#include "riru.h"
#include "misc.h"
#include "socket.h"
#include "bridge_service.h"
#include "dex_file.h"
#include "android.h"
#include "hidden_api.h"

namespace AppProcess {

    static JavaVM *javaVm;

    static const char *pkg;
    static int fix_rename;
    static int log_file_access;

    static jclass mainClass = nullptr;
    static jmethodID transactNativeMethodId = nullptr;


#define NEW_FUNC_DEF(ret, func, ...) \
    static ret (*old_##func)(__VA_ARGS__); \
    static ret new_##func(__VA_ARGS__)

#define CALL(func, ...) \
    if (old_##func) { \
        res = (old_##func)(__VA_ARGS__); \
    } else { \
        res = func(__VA_ARGS__); \
        LOGW("%s is null", #func); \
    }

#define NEW_FUNC_IMPL(ret, func, ...) \
    ret res; \
    CALL(func, __VA_ARGS__); \
    log(#func, path); \
    return res;

    static void log(const char *func, const char *path) {
        if (strncmp("/storage", path, strlen("/storage")) != 0)
            return;

        JNIEnv *env;
        auto res = javaVm->GetEnv((void **) &env, JNI_VERSION_1_6);
        if (res == JNI_EDETACHED) {
            if (javaVm->AttachCurrentThread(&env, nullptr) != JNI_OK) {
                LOGD("unable to attach JVM");
                return;
            }
        } else if (res == JNI_OK) {
        } else if (res == JNI_EVERSION) {
            LOGD("JNI_EVERSION");
            return;
        }

        BridgeService::insertFileMonitor(env, pkg, func, path);

        if (env->ExceptionCheck()) {
            env->ExceptionDescribe();
        }
    }

    NEW_FUNC_DEF(int, open, const char *__path, int __flags, ...) {
        mode_t mode = 0;
        int res;

        va_list ap;
        va_start(ap, __flags);
        mode = static_cast<mode_t>(va_arg(ap, int));
        if (mode) {
            CALL(open, __path, __flags, mode);
        } else {
            CALL(open, __path, __flags);
        }
        va_end(ap);

        log("open", __path);
        return res;
    }

    NEW_FUNC_DEF(int, open64, const char *__path, int __flags, ...) {
        mode_t mode = 0;
        int res;

        va_list ap;
        va_start(ap, __flags);
        mode = static_cast<mode_t>(va_arg(ap, int));
        if (mode) {
            CALL(open64, __path, __flags, mode);
        } else {
            CALL(open64, __path, __flags);
        }
        va_end(ap);

        log("open64", __path);
        return res;
    }

    NEW_FUNC_DEF(int, openat, int __dir_fd, const char *__path, int __flags, ...) {
        mode_t mode = 0;
        int res;

        va_list ap;
        va_start(ap, __flags);
        mode = static_cast<mode_t>(va_arg(ap, int));
        if (mode) {
            CALL(openat, __dir_fd, __path, __flags, mode);
        } else {
            CALL(openat, __dir_fd, __path, __flags);
        }
        va_end(ap);

        log("openat", __path);
        return res;
    }

    NEW_FUNC_DEF(int, openat64, int __dir_fd, const char *__path, int __flags, ...) {
        mode_t mode = 0;
        int res;

        va_list ap;
        va_start(ap, __flags);
        mode = static_cast<mode_t>(va_arg(ap, int));
        if (mode) {
            CALL(openat64, __dir_fd, __path, __flags, mode);
        } else {
            CALL(openat64, __dir_fd, __path, __flags);
        }
        va_end(ap);

        log("openat64", __path);
        return res;
    }

    NEW_FUNC_DEF(int, creat, const char *path, mode_t mode) {
        NEW_FUNC_IMPL(int, creat, path, mode)
    }

    NEW_FUNC_DEF(int, creat64, const char *path, mode_t mode) {
        NEW_FUNC_IMPL(int, creat64, path, mode)
    }

    NEW_FUNC_DEF(int, mkdir, const char *path, mode_t mode) {
        NEW_FUNC_IMPL(int, mkdir, path, mode)
    }

    NEW_FUNC_DEF(int, mkdirat, int dir_fd, const char *path, mode_t mode) {
        NEW_FUNC_IMPL(int, mkdirat, dir_fd, path, mode)
    }

    NEW_FUNC_DEF(int, rename, const char *__old_path, const char *__new_path) {
        int res;
        CALL(rename, __old_path, __new_path);

        if (fix_rename && res == -1 && errno == EXDEV
            && strncmp("/storage/emulated/", __new_path, strlen("/storage/emulated/")) == 0) {
            if (copyfile(__old_path, __new_path) == 0) {
                unlink(__old_path);

                LOGI("rename %s %s fixed", __old_path, __new_path);
                errno = 0;
                return 0;
            }
            errno = EXDEV;
        }

        if (log_file_access)
            log("rename", __new_path);

        return res;
    }

    NEW_FUNC_DEF(int, renameat, int __old_dir_fd, const char *__old_path, int __new_dir_fd,
                 const char *__new_path) {
        int res;
        CALL(renameat, __old_dir_fd, __old_path, __new_dir_fd, __new_path);

        if (fix_rename && res == -1 && errno == EXDEV
            && strncmp("/storage/emulated/", __new_path, strlen("/storage/emulated/")) == 0) {
            if (copyfileat(__old_dir_fd, __old_path, __new_dir_fd, __new_path) == 0) {
                unlinkat(__old_dir_fd, __old_path, 0);

                LOGI("renameat %d %s %d %s fixed", __old_dir_fd, __old_path, __new_dir_fd,
                     __new_path);
                errno = 0;
                return 0;
            }
            errno = EXDEV;
        }

        if (log_file_access)
            log("renameat", __new_path);

        return res;
    }

#define XHOOK_REGISTER(PATH_REGEX, NAME) \
    if (xhook_register(PATH_REGEX, #NAME, (void*) new_##NAME, (void **) &old_##NAME) != 0) { \
        LOGE("failed to register hook " #NAME "."); \
    }

    using jniRegisterNativeMethods_t = int(
            JNIEnv *env, const char *className, const JNINativeMethod *methods, int numMethods);

    using transactNative_t = jboolean(
            JNIEnv *env, jobject obj, jint code, jobject dataObj, jobject replyObj, jint flags);

    static transactNative_t *original_transactNative;

    static bool installDex(JNIEnv *env, DexFile *dexFile, jobjectArray args) {
        if (android::GetApiLevel() >= 26) {
            dexFile->createInMemoryDexClassLoader(env);
        } else {
            return false;
        }

        mainClass = dexFile->findClass(env, "moe/shizuku/redirectstorage/AppProcess");
        if (!mainClass) {
            LOGE("unable to find main class");
            return false;
        }
        mainClass = (jclass) env->NewGlobalRef(mainClass);

        auto mainMethod = env->GetStaticMethodID(mainClass, "main", "([Ljava/lang/String;)V");
        if (!mainMethod) {
            LOGE("unable to find main method");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        transactNativeMethodId = env->GetStaticMethodID(mainClass, "transactNative",
                                                        "(Landroid/os/IBinder;ILandroid/os/Parcel;Landroid/os/Parcel;I)Z");
        if (!transactNativeMethodId) {
            LOGE("unable to find transactNative method");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        env->CallStaticVoidMethod(mainClass, mainMethod, args);
        if (env->ExceptionCheck()) {
            LOGE("unable to call main method");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        return true;
    }

    static void install_hook() {
        const char *path_regex;
        if (android::GetApiLevel() >= 24)
            path_regex = ".*\\libopenjdk.so$";
        else
            path_regex = ".*\\.so$";

        XHOOK_REGISTER(path_regex, open);
        XHOOK_REGISTER(path_regex, open64);
        XHOOK_REGISTER(path_regex, openat);
        XHOOK_REGISTER(path_regex, openat64);
        XHOOK_REGISTER(path_regex, creat);
        XHOOK_REGISTER(path_regex, creat64);
        XHOOK_REGISTER(path_regex, mkdir);
        XHOOK_REGISTER(path_regex, mkdirat);
        XHOOK_REGISTER(path_regex, rename);
        XHOOK_REGISTER(path_regex, renameat);

        xhook_ignore(".*/linker$", nullptr);
        xhook_ignore(".*/linker64$", nullptr);
        xhook_ignore(".*/libwebviewchromium.so$", nullptr);

        if (xhook_refresh(0) == 0)
            xhook_clear();
        else
            LOGE("failed to refresh hook");

        LOGV("install hook finished");
    }

    const uint32_t FLAG_CALL_ORIGINAL = 1 << 10;

    static jboolean new_transactNative(
            JNIEnv *env, jobject obj, jint code, jobject dataObj, jobject replyObj, jint flags) {

        if (flags & FLAG_CALL_ORIGINAL) {
            return original_transactNative(env, obj, code, dataObj, replyObj, flags & ~FLAG_CALL_ORIGINAL);
        }

        jboolean consumed = env->CallStaticBooleanMethod(mainClass, transactNativeMethodId, obj, code, dataObj, replyObj, flags);
        if (env->ExceptionCheck()) {
#ifdef DEBUG
            env->ExceptionDescribe();
#endif
            env->ExceptionClear();
            return original_transactNative(env, obj, code, dataObj, replyObj, flags);
        }

        if (consumed) {
            return JNI_TRUE;
        } else {
            return original_transactNative(env, obj, code, dataObj, replyObj, flags);
        }
    }

    static const char *bpClassName = "android/os/BinderProxy";

    static JNINativeMethod transactNativeMethod = {
            "transactNative", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z",
            (void *) new_transactNative
    };

    static const JNINativeMethod binderProxyMethods[] = {transactNativeMethod};

    static int registerNativeMethods(JNIEnv *env, const char *className,
                                     JNINativeMethod *methods, int numMethods) {
        jclass clazz = env->FindClass(className);
        if (clazz == nullptr) {
            env->ExceptionClear();
            LOGE("can't find class %s", className);
            return JNI_FALSE;
        }

        int res = env->RegisterNatives(clazz, methods, numMethods);
        if (res < 0) {
            env->ExceptionClear();
            return JNI_FALSE;
        }

        return JNI_TRUE;
    }

    static void hook_binder(JNIEnv *env) {
        original_transactNative = nullptr/*(transactNative_t *) riru_get_native_method_func(
                    bpClassName, transactNativeMethod.name, transactNativeMethod.signature)*/;

        if (!original_transactNative) {
            LOGW("can't get address of android.os.BinderProxy#transactNative");
            return;
        }

        void *handle = dlopen("libnativehelper.so", RTLD_LOCAL | RTLD_LAZY);
        if (!handle) {
            LOGW("can't dlopen libnativehelper.so: %s", dlerror());
            return;
        }

        auto *jniRegisterNativeMethods = (jniRegisterNativeMethods_t *) dlsym(handle, "jniRegisterNativeMethods");
        if (!jniRegisterNativeMethods) {
            LOGW("can't find jniRegisterNativeMethods: %s", dlerror());
            return;
        }

        transactNativeMethod.fnPtr = (void *) new_transactNative;
        jniRegisterNativeMethods(env, bpClassName, binderProxyMethods, 1);

        LOGD("hook binder finished");
    }

    void main(JNIEnv *env, const char *package_name, int user,
              int enable_file_monitor, int enable_fix_rename,
              int enable_dex_inject, DexFile *dexFile, jobjectArray args) {

        if (!package_name || !strlen(package_name))
            return;

        env->GetJavaVM(&javaVm);

        pkg = strdup(package_name);

        log_file_access = enable_file_monitor;
        fix_rename = enable_fix_rename;

        LOGV("main: package=%s, user=%d", package_name, user);

        if (log_file_access) {
            install_hook();
        } else {
            LOGV("main: log file access is disabled");
        }

        if (android::GetApiLevel() >= 26 && enable_dex_inject) {
            LOGV("install dex");

            static const char *exemptions[] = {
                    "Landroid/content/ContentProviderProxy",
                    "Landroid/content/ContentProviderNative",
                    "Landroid/content/IContentProvider",
            };

            HiddenApi::setHiddenApiExemptions(env, exemptions, 3);

            if (installDex(env, dexFile, args)) {
                LOGV("install dex finished");
                hook_binder(env);
            }
        }
    }
}