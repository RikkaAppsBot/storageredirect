#ifndef ART_H
#define ART_H

enum EnforcementPolicy {
    kNoChecks = 0,
    kJustWarn = 1,  // keep checks enabled, but allow everything (enables logging)
    kDarkGreyAndBlackList = 2,  // ban dark grey & blacklist
    kBlacklistOnly = 3,  // ban blacklist violations only
    kMax = kBlacklistOnly,
};

template<typename T>
constexpr int CTZ(T x) {
    static_assert(std::is_integral<T>::value, "T must be integral");
    // It is not unreasonable to ask for trailing zeros in a negative number. As such, do not check
    // that T is an unsigned type.
    static_assert(sizeof(T) == sizeof(uint64_t) || sizeof(T) <= sizeof(uint32_t),
                  "Unsupported sizeof(T)");
    //DCHECK_NE(x, static_cast<T>(0));
    return (sizeof(T) == sizeof(uint64_t)) ? __builtin_ctzll(x) : __builtin_ctz(x);
}

#endif // ART_H
