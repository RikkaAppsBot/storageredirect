#include <jni.h>
#include "logging.h"

namespace HiddenApi {

    static jclass runtimeClass;
    static jmethodID getRuntimeMethod, setHiddenApiExemptionsMethod;

    static bool ok = false;

    void init(JNIEnv *env) {
        static bool init = false;
        if (init) return;
        init = true;

        auto runtimeClass_ = env->FindClass("dalvik/system/VMRuntime");
        if (runtimeClass_) {
            runtimeClass = (jclass) env->NewGlobalRef(runtimeClass_);
        } else {
            env->ExceptionClear();
            return;
        }

        getRuntimeMethod = env->GetStaticMethodID(runtimeClass_, "getRuntime", "()Ldalvik/system/VMRuntime;");
        if (!getRuntimeMethod) {
            env->ExceptionClear();
            return;
        }

        setHiddenApiExemptionsMethod = env->GetMethodID(runtimeClass_, "setHiddenApiExemptions", "([Ljava/lang/String;)V");
        if (!setHiddenApiExemptionsMethod) {
            if (env->ExceptionCheck()) env->ExceptionClear();
            return;
        }

        ok = true;
    }

    void setHiddenApiExemptions(JNIEnv *env, const char** signaturePrefixes, size_t size) {
        if (!ok) {
            LOGD("reflection failed");
            return;
        }

        auto runtime = env->CallStaticObjectMethod(runtimeClass, getRuntimeMethod);
        if (!runtime) {
            env->ExceptionClear();
            return;
        }

        auto array = env->NewObjectArray(size, env->FindClass("java/lang/String"), nullptr);
        for (auto i = 0; i < size; i++) {
            if (!signaturePrefixes[i]) continue;
            env->SetObjectArrayElement(array, i, env->NewStringUTF(signaturePrefixes[i]));
        }
        env->CallVoidMethod(runtime, setHiddenApiExemptionsMethod, array);
        if (env->ExceptionCheck()) env->ExceptionClear();
    }
}
