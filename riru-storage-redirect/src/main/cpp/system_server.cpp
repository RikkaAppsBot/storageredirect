#include <cstdio>
#include <cstring>
#include <chrono>
#include <fcntl.h>
#include <unistd.h>
#include <sys/vfs.h>
#include <sys/stat.h>
#include <dirent.h>
#include <xhook.h>
#include <jni.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <mntent.h>
#include <sys/mount.h>
#include <sys/sendfile.h>
#include <dlfcn.h>
#include <cinttypes>

#include "android.h"
#include "logging.h"
#include "riru.h"
#include "misc.h"
#include "dex_file.h"
#include "bridge_service.h"
#include "binder_hook.h"

namespace SystemServer {

    static jclass mainClass = nullptr;
    static jmethodID my_execTransactMethodID;

    /*using jniRegisterNativeMethods_t = int(
            JNIEnv *env, const char *className, const JNINativeMethod *methods, int numMethods);

    using transactNative_t = jboolean(
            JNIEnv *env, jobject obj, jint code, jobject dataObj, jobject replyObj, jint flags);

    static transactNative_t *original_transactNative;*/

    static bool installDex(JNIEnv *env, DexFile *dexFile) {
        if (android::GetApiLevel() >= 26) {
            dexFile->createInMemoryDexClassLoader(env);
        } else {
            dexFile->createDexClassLoader(env, "/data/system/storage-isolation", "main.dex", "/data/system/storage-isolation/oat");
        }

        mainClass = dexFile->findClass(env, "moe/shizuku/redirectstorage/SystemProcess");
        if (!mainClass) {
            LOGE("unable to find main class");
            return false;
        }
        mainClass = (jclass) env->NewGlobalRef(mainClass);

        auto mainMethod = env->GetStaticMethodID(mainClass, "main", "([Ljava/lang/String;)V");
        if (!mainMethod) {
            LOGE("unable to find main method");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        my_execTransactMethodID = env->GetStaticMethodID(mainClass, "execTransact", "(IJJI)Z");
        if (!my_execTransactMethodID) {
            LOGE("unable to find execTransact");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        auto args = env->NewObjectArray(1, env->FindClass("java/lang/String"), nullptr);

        char buf[64];
        sprintf(buf, "--version-code=%d", RIRU_MODULE_VERSION);
        env->SetObjectArrayElement(args, 0, env->NewStringUTF(buf));

        env->CallStaticVoidMethod(mainClass, mainMethod, args);
        if (env->ExceptionCheck()) {
            LOGE("unable to call main method");
            env->ExceptionDescribe();
            env->ExceptionClear();
            return false;
        }

        return true;
    }

    /*const uint32_t FLAG_CALL_ORIGINAL = 1 << 10;

    static jboolean new_transactNative(
            JNIEnv *env, jobject obj, jint code, jobject dataObj, jobject replyObj, jint flags) {

        if (!dex.binderHookClass || !dex.transactNativeMethod) {
            if (!dex.binderHookClass) {
                LOGE("binderHookClass is null");
            }
            if (!dex.transactNativeMethod) {
                LOGE("transactNativePreMethod is null");
            }
            return original_transactNative(env, obj, code, dataObj, replyObj, flags);
        }

        if (flags & FLAG_CALL_ORIGINAL) {
            return original_transactNative(env, obj, code, dataObj, replyObj, flags & ~FLAG_CALL_ORIGINAL);
        } else {
            jboolean consumed = JNI_CallStaticBooleanMethod(
                    env, dex.binderHookClass, dex.transactNativeMethod, obj, code, dataObj, replyObj, flags);
            if (consumed) {
                return JNI_TRUE;
            }
            return original_transactNative(env, obj, code, dataObj, replyObj, flags);
        }
    }

    static const char *bpClassName = "android/os/BinderProxy";

    static JNINativeMethod transactNativeMethod = {
            "transactNative", "(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z",
            (void *) new_transactNative
    };

    static const JNINativeMethod binderProxyMethods[] = {transactNativeMethod};

    static int registerNativeMethods(JNIEnv *env, const char *className,
                                     JNINativeMethod *methods, int numMethods) {
        jclass clazz = JNI_FindClass(env, className);
        if (clazz == nullptr) {
            LOGE("can't find class %s", className);
            return JNI_FALSE;
        }

        int res = JNI_RegisterNatives(env, clazz, methods, numMethods);
        if (res < 0) {
            return JNI_FALSE;
        }

        return JNI_TRUE;
    }

    static void hook_binder(JNIEnv *env) {
        if (!dex.binderHookClass) {
            LOGW("required dex not loaded");
            return;
        }

        if (!dex.transactNativeMethod) {
            LOGW("required methods not found in dex");
            return;
        }

        if (NativeMethod::getOffset() != -1) {
            LOGV("use offset, offset=%d", NativeMethod::getOffset());

            jclass cls = env->FindClass(bpClassName);
            original_transactNative = (transactNative_t *) NativeMethod::getMethodAddress(
                    env, cls, transactNativeMethod.name, transactNativeMethod.signature);
            env->RegisterNatives(cls, binderProxyMethods, 1);
        } else {
            LOGV("use Riru");

            original_transactNative = (transactNative_t *) riru_get_native_method_func(
                    bpClassName, transactNativeMethod.name, transactNativeMethod.signature);

            if (!original_transactNative) {
                LOGW("can't get address of android.os.BinderProxy#transactNative");
                return;
            }

            void *handle = dlopen("libnativehelper.so", RTLD_LOCAL | RTLD_LAZY);
            if (!handle) {
                LOGW("can't dlopen libnativehelper.so: %s", dlerror());
                return;
            }

            auto *jniRegisterNativeMethods = (jniRegisterNativeMethods_t *) dlsym(handle, "jniRegisterNativeMethods");
            if (!jniRegisterNativeMethods) {
                LOGW("can't find jniRegisterNativeMethods: %s", dlerror());
                return;
            }

            transactNativeMethod.fnPtr = (void *) new_transactNative;
            jniRegisterNativeMethods(env, bpClassName, binderProxyMethods, 1);

            riru_set_native_method_func(
                    bpClassName, transactNativeMethod.name, transactNativeMethod.signature,
                    binderProxyMethods->fnPtr);V
        }
    }*/

    static bool ExecTransact(jboolean *res, JNIEnv *env, jobject obj, va_list args) {
        jint code;

        va_list copy;
        va_copy(copy, args);
        code = va_arg(copy, jint);
        va_end(copy);

        if (code == BridgeService::BRIDGE_TRANSACTION_CODE) {
            *res = env->CallStaticBooleanMethodV(mainClass, my_execTransactMethodID, args);
            return true;
        }

        return false;
    }

    void main(JNIEnv *env, DexFile *dexFile) {
        LOGD("dex size=%" PRIdPTR, dexFile->getSize());

        if (!dexFile->getBytes()) {
            LOGE("no dex");
            return;
        }

        LOGV("main: system server");

        LOGV("install dex");

        if (!installDex(env, dexFile)) {
            LOGE("can't install dex");
            return;
        }

        LOGV("install dex finished");

        JavaVM *javaVm;
        env->GetJavaVM(&javaVm);

        BinderHook::Install(javaVm, env, ExecTransact);
    }
}