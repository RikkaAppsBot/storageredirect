#include <dlfcn.h>
#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include <jni.h>
#include <cstring>
#include <cstdlib>
#include <cinttypes>
#include <sys/mman.h>
#include <array>
#include <thread>
#include <vector>
#include <utility>
#include <climits>
#include <string>
#include <mntent.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/system_properties.h>
#include <nativehelper/scoped_utf_chars.h>
#include <nativehelper/scoped_utf_chars.h>
#include "riru.h"

#include "android.h"
#include "misc.h"
#include "logging.h"
#include "app_process.h"
#include "socket.h"
#include "art.h"
#include "system_server.h"
#include "dex_file.h"
#include "bridge_service.h"
#include "hidden_api.h"

#define ROOT_PATH "/data/adb/storage-isolation"

static char saved_package_name[256] = {0};
static int saved_uid;
static int saved_mount_external;
static int saved_is_child_zygote;

#ifdef DEBUG
static char saved_process_name[256] = {0};
#endif

static DexFile *dexFile = nullptr;

static void PrepareDex() {
    if (dexFile && dexFile->getBytes()) return;
    dexFile = new DexFile(ROOT_PATH "/bin/main.dex");
}

static void
appProcessPre(JNIEnv *env, jint *uid, jint *mountExternal, jstring *jNiceName, jstring *jAppDataDir, jboolean *isChildZygote) {

    saved_is_child_zygote = *isChildZygote;

    BridgeService::init(env);
    HiddenApi::init(env);

    PrepareDex();

    saved_uid = *uid;
    saved_mount_external = *mountExternal;

#ifdef DEBUG
    memset(saved_process_name, 0, 256);

    if (*jNiceName) {
        ScopedUtfChars nice_name(env, *jNiceName);
        sprintf(saved_process_name, "%s", nice_name.c_str());
    }
#endif

    memset(saved_package_name, 0, 256);

    if (*jAppDataDir) {
        ScopedUtfChars appDataDir(env, *jAppDataDir);
        int user = 0;

        // /data/user/<user_id>/<package>
        if (sscanf(appDataDir.c_str(), "/data/%*[^/]/%d/%s", &user, saved_package_name) == 2)
            goto found;

        // /mnt/expand/<id>/user/<user_id>/<package>
        if (sscanf(appDataDir.c_str(), "/mnt/expand/%*[^/]/%*[^/]/%d/%s", &user, saved_package_name) == 2)
            goto found;

        // /data/data/<package>
        if (sscanf(appDataDir.c_str(), "/data/%*[^/]/%s", saved_package_name) == 1)
            goto found;

        // nothing found
        saved_package_name[0] = '\0';

        found:;
    }


    // TODO add simple option (for external sdcard resist)
    /*if (saved_should_redirect) {
        *mountExternal = 1; // MOUNT_EXTERNAL_DEFAULT
    }*/

    // TODO load dex only for media process
}

static void appProcessPost(
        JNIEnv *env, const char *from, const char *package_name, jint uid, jint mount_external, bool is_child_zygote) {

    LOGD("%s: uid=%d, package=%s, process=%s, is_child_zygote=%s",
         from, uid, package_name, saved_process_name, is_child_zygote ? "true" : "false");

    if (!mount_external || is_child_zygote) {
        return;
    }

    auto reply = BridgeService::requestCheckProcess(env, package_name);

    bool enable_file_monitor = false;
    bool enable_fix_rename = false;
    bool enable_dex_inject = false;

    if (reply) {
        auto length = env->GetArrayLength(reply);
        for (int i = 0; i < length; i++) {
            auto jArg = (jstring) (env->GetObjectArrayElement(reply, i));
            if (!jArg) continue;

            ScopedUtfChars s_arg(env, jArg);
            auto arg = s_arg.c_str();

            LOGD("arg: %s", arg);

            if (strcmp(arg, "--enable-file-monitor") == 0) {
                enable_file_monitor = true;
            } else if (strcmp(arg, "--enable-fix-rename") == 0) {
                enable_fix_rename = true;
            } else if (strcmp(arg, "--enable-dex-inject") == 0) {
                enable_dex_inject = true;
            }
        }
    }

    if (enable_file_monitor || enable_fix_rename || enable_dex_inject) {
        AppProcess::main(
                env, package_name, saved_uid / 100000, enable_file_monitor, enable_fix_rename,
                enable_dex_inject, dexFile, reply);
    } else {
        riru_set_unload_allowed(true);
    }
}

static void forkAndSpecializePre(
        JNIEnv *env, jclass clazz, jint *uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jintArray *fdsToClose, jintArray *fdsToIgnore, jboolean *is_child_zygote,
        jstring *instructionSet, jstring *appDataDir, jboolean *isTopApp, jobjectArray *pkgDataInfoList,
        jobjectArray *whitelistedDataInfoList, jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {

    appProcessPre(env, uid, mountExternal, niceName, appDataDir, is_child_zygote);
}

static void forkAndSpecializePost(JNIEnv *env, jclass clazz, jint res) {

    if (res == 0) {
        appProcessPost(env, "forkAndSpecialize", saved_package_name, saved_uid, saved_mount_external, saved_is_child_zygote);
    }
}

static void specializeAppProcessPre(
        JNIEnv *env, jclass clazz, jint *uid, jint *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jint *mountExternal, jstring *seInfo, jstring *niceName,
        jboolean *startChildZygote, jstring *instructionSet, jstring *appDataDir,
        jboolean *isTopApp, jobjectArray *pkgDataInfoList, jobjectArray *whitelistedDataInfoList,
        jboolean *bindMountAppDataDirs, jboolean *bindMountAppStorageDirs) {

    appProcessPre(env, uid, mountExternal, niceName, appDataDir, startChildZygote);
}

static void specializeAppProcessPost(JNIEnv *env, jclass clazz) {

    appProcessPost(env, "specializeAppProcess", saved_package_name, saved_uid, saved_mount_external, saved_is_child_zygote);
}

static void forkSystemServerPre(
        JNIEnv *env, jclass clazz, uid_t *uid, gid_t *gid, jintArray *gids, jint *runtimeFlags,
        jobjectArray *rlimits, jlong *permittedCapabilities, jlong *effectiveCapabilities) {

    PrepareDex();
}

static void forkSystemServerPost(JNIEnv *env, jclass clazz, jint res) {
    if (res == 0) {
        LOGV("nativeForkSystemServerPost");

        SystemServer::main(env, dexFile);
    }
}

static int shouldSkipUid(int uid) {
    return false;
}

static void onModuleLoaded() {
    //NativeMethod::init();
    PrepareDex();
}

extern "C" {
int riru_api_version;
const char *riru_magisk_module_path = nullptr;
int *riru_allow_unload = nullptr;

static auto module = RiruVersionedModuleInfo{
        .moduleApiVersion = RIRU_MODULE_API_VERSION,
        .moduleInfo= RiruModuleInfo{
                .supportHide = true,
                .version = RIRU_MODULE_VERSION,
                .versionName = RIRU_MODULE_VERSION_NAME,
                .onModuleLoaded = onModuleLoaded,
                .forkAndSpecializePre = forkAndSpecializePre,
                .forkAndSpecializePost = forkAndSpecializePost,
                .forkSystemServerPre = forkSystemServerPre,
                .forkSystemServerPost = forkSystemServerPost,
                .specializeAppProcessPre = specializeAppProcessPre,
                .specializeAppProcessPost = specializeAppProcessPost
        }
};

RiruVersionedModuleInfo *init(Riru *riru) {
    static int step = 0;
    step += 1;

    switch (step) {
        case 1: {
            auto core_max_api_version = riru->riruApiVersion;
            riru_api_version = core_max_api_version <= RIRU_MODULE_API_VERSION ? core_max_api_version : RIRU_MODULE_API_VERSION;
            if (riru_api_version < 25) {
                module.moduleInfo.unused = (void *) shouldSkipUid;
            }
            if (riru_api_version >= 25) {
                riru_allow_unload = riru->allowUnload;
            }
            if (riru_api_version >= 24) {
                module.moduleApiVersion = riru_api_version;
                riru_magisk_module_path = strdup(riru->magiskModulePath);
                return &module;
            } else {
                return (RiruVersionedModuleInfo *) &riru_api_version;
            }
        }
        case 2: {
            return (RiruVersionedModuleInfo *) &module.moduleInfo;
        }
        case 3:
        default: {
            return nullptr;
        }
    }
}
}