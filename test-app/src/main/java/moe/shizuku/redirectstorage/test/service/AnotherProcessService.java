package moe.shizuku.redirectstorage.test.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import moe.shizuku.redirectstorage.test.common.TestUtils;

import static moe.shizuku.redirectstorage.test.TestApplication.TAG;

public class AnotherProcessService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "AnotherProcessService onCreate");
        TestUtils.writeStringToFile(
                "/sdcard/.aaa", ".anotherprocess", "Created by AnotherProcessService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
