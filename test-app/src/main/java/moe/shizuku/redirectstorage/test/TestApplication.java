package moe.shizuku.redirectstorage.test;

import android.app.Application;
import android.content.Context;
import android.os.IVold;

import moe.shizuku.redirectstorage.test.binder.TransactionCodeExporter;
import moe.shizuku.redirectstorage.test.util.HiddenApi;

import static moe.shizuku.redirectstorage.test.util.Logger.LOGGER;

public class TestApplication extends Application {

    public static final String TAG = "SRTest";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        /*Parcel parcel = Parcel.obtain();
        parcel.writeInterfaceToken("test");
        Log.i("?!", Arrays.toString(parcel.marshall()));
        //Log.i("?!", new String(parcel.marshall()));
        parcel.setDataPosition(0);
        Log.i("?!", parcel.readInt() + "");
        if (Build.VERSION.SDK_INT >= 29) {
            Log.i("?!", parcel.readInt() + "");
        }
        Log.i("?!", parcel.readString() + "");
        parcel.recycle();*/

        super.attachBaseContext(base);

        HiddenApi.exemptAll();

        try {
            TransactionCodeExporter exporter = new TransactionCodeExporter(IVold.Stub.class);
            LOGGER.i("remountUid=" + exporter.export("remountUid", void.class, int.class, int.class));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}