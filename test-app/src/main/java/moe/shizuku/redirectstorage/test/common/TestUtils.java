package moe.shizuku.redirectstorage.test.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class TestUtils {

    public static boolean writeStringToFile(String pathName, String fileName, String content) {
        return writeStringToFile(new File(pathName), fileName, content);
    }

    public static boolean writeStringToFile(File path, String fileName, String content) {
        if (!path.exists()) {
            if (!path.mkdirs()) {
                return false;
            }
        }
        File file = new File(path, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
