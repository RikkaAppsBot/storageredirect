package moe.shizuku.redirectstorage.test.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import moe.shizuku.redirectstorage.test.R;

public class ForegroundService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = getSystemService(NotificationManager.class);
            nm.createNotificationChannel(new NotificationChannel("test", "test", NotificationManager.IMPORTANCE_DEFAULT));
        }

        startForeground(123, new NotificationCompat.Builder(this, "test")
                .setContentTitle("test")
                .setSmallIcon(R.drawable.ic_android_24)
                .build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
