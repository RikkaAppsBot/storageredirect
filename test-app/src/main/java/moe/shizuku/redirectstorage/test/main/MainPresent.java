package moe.shizuku.redirectstorage.test.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rikka.storageisolation.StorageIsolationManager;
import moe.shizuku.redirectstorage.test.common.TestUtils;

@SuppressLint("SdCardPath")
public class MainPresent {

    private final MainActivity mActivity;

    MainPresent(@NonNull MainActivity activity) {
        mActivity = activity;
    }

    public void requestStoragePermission() {
        if (!mActivity.isStoragePermissionGranted()) {
            mActivity.requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 0);
        }
    }

    public void listSdcard() {
        List<String> list = Collections.emptyList();
        try {
            list = Arrays.asList(new File("/sdcard").list());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final StringBuilder sb = new StringBuilder();
        for (String item : list) {
            sb.append("File name: ").append(item).append("\n");
        }
        new AlertDialog.Builder(mActivity)
                .setTitle("List " + list.size() + " files")
                .setMessage(sb.toString())
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    public void listStorage() {
        List<File> list = Collections.emptyList();
        try {
            list = Arrays.asList(Environment.getExternalStorageDirectory().listFiles());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final StringBuilder sb = new StringBuilder();
        for (File item : list) {
            sb.append(item.getAbsolutePath()).append("\n");
        }
        new AlertDialog.Builder(mActivity)
                .setTitle("List " + list.size() + " files")
                .setMessage(sb.toString())
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    public void listAndroidData() {
        List<File> list = Collections.emptyList();
        try {
            list = Arrays.asList(new File(Environment.getExternalStorageDirectory(), "Android/data").listFiles());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final StringBuilder sb = new StringBuilder();
        for (File item : list) {
            sb.append(item.getAbsolutePath()).append("\n");
        }
        new AlertDialog.Builder(mActivity)
                .setTitle("List " + list.size() + " files")
                .setMessage(sb.toString())
                .setPositiveButton(android.R.string.ok, null)
                .show();

        ContentResolver cr = mActivity.getContentResolver();

        try (Cursor cursor = cr.query(MediaStore.Files.getContentUri("external"), null, null, null, null)) {
            if (cursor == null || !cursor.moveToFirst())
                return;

            int image_path_index = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            int display_name_index = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
            int bucket_display_name_index = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            do {
                String path = cursor.getString(image_path_index);
                String display = cursor.getString(display_name_index);
                String bucket = cursor.getString(bucket_display_name_index);

                Log.i("SRTest", path + " display=" + display + " bucket=" + bucket);
            } while (cursor.moveToNext());
        }
    }


    public void createFile() {
        try {
            if (TestUtils.writeStringToFile(
                    "/sdcard", "test", "Test")) {
                mActivity.toast("Done.");
            } else {
                mActivity.toast("Failed to write test file.");
            }

            if (TestUtils.writeStringToFile(
                    "/sdcard/Documents", "test", "Test")) {
                mActivity.toast("Done.");
            } else {
                mActivity.toast("Failed to write test file.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createFileInExternalData(View view) {
        Context context = view.getContext();
        try {
            if (TestUtils.writeStringToFile(
                    context.getExternalFilesDir(null), "test", "Test")) {
                mActivity.toast("Done.");
            } else {
                mActivity.toast("Failed to write test file.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openFileWithFileUri() {
        TestUtils.writeStringToFile("/sdcard", "Nanahira.mp3", "Nanahira");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3"));
        intent.setDataAndType(Uri.fromFile(new File("/sdcard/Nanahira.mp3")),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3"));
        mActivity.startActivity(intent/*Intent.createChooser(intent, "Choose app to open")*/);
    }

    public void shareFileWithFileUri() {
        TestUtils.writeStringToFile("/sdcard", "Nanahira.mp3", "Nanahira");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3"));
        intent.putExtra(Intent.EXTRA_STREAM,
                Uri.fromFile(new File("/sdcard/Nanahira.mp3")));
        mActivity.startActivity(Intent.createChooser(intent, "Choose app to share"));
    }

    public void createFakePublicFile() {
        try {
            if (TestUtils.writeStringToFile(
                    "/sdcard/FakePublic", "test", "Test")) {
                mActivity.toast("Done.");
            } else {
                mActivity.toast("Failed to write test file.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createPictures() {
        try {
            if (TestUtils.writeStringToFile(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "1.png", "1.png")) {
                mActivity.toast("Done.");
            } else {
                mActivity.toast("Failed to write test file.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void listImages(View view) {
        /*ContentResolver cr = view.getContext().getContentResolver();

        try (Cursor cursor = MediaStore.Images.Media.query(cr, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null)) {
            if (cursor == null || !cursor.moveToFirst())
                return;

            int image_path_index = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            int display_name_index = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
            int bucket_display_name_index = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            do {
                String path = cursor.getString(image_path_index);
                String display = cursor.getString(display_name_index);
                String bucket = cursor.getString(bucket_display_name_index);

                Log.i("SRTest", path + " display=" + display + " bucket=" + bucket);

                try {
                    Log.i("SRTest", "access R_OK " + Os.access(path, OsConstants.R_OK));
                } catch (ErrnoException e) {
                    Log.e("SRTest", "access", e);
                }
                try {
                    StructStat stat = Os.stat(path);
                    Log.i("SRTest", "mode " + Integer.toOctalString(stat.st_mode));
                } catch (ErrnoException e) {
                    Log.e("SRTest", "stat", e);
                }
                try {
                    FileInputStream is = new FileInputStream(path);
                    int r = is.read();
                    Log.i("SRTest", "FileInputStream read: " + r);
                    is.close();
                } catch (Throwable tr) {
                    Log.e("SRTest", "", tr);
                }
            } while (cursor.moveToNext());
        }*/

    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void insertImage(View view) {
        /*File image = new File(view.getContext().getFilesDir(), "test");
        try {
            image.createNewFile();

            ContentResolver cr = view.getContext().getContentResolver();

            ContentValues values;
            values = new ContentValues();
            values.put(MediaStore.Images.ImageColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + "/test");
            values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, Long.toString(System.currentTimeMillis()));
            values.put(MediaStore.Images.ImageColumns.IS_PENDING, true);
            Uri uri = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            if (uri == null) {
                return;
            }

            InputStream is = new FileInputStream(image);
            OutputStream os = cr.openOutputStream(uri, "rw");
            byte[] b = new byte[8192];
            for (int r; (r = is.read(b)) != -1; ) {
                os.write(b, 0, r);
            }
            os.flush();
            os.close();
            is.close();

            values = new ContentValues();
            values.put(MediaStore.Images.ImageColumns.IS_PENDING, false);
            cr.update(uri, values, null, null);
        } catch (Throwable tr) {
            Log.e("SRTest",  "", tr);
        }*/
        try {
            ContentResolver cr = view.getContext().getContentResolver();
            ContentValues values;
            values = new ContentValues();
            values.put("description", "");
            values.put("mime_type", "image/jpeg");
            values.put("title", "/storage/emulated/0/DCIM/Camera/1577278744024");
            Uri uri = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            if (uri == null) {
                return;
            }
            AssetFileDescriptor afd = cr.openAssetFile(uri, "w", null);
            OutputStream os = afd.createOutputStream();
            os.write(1);
            os.close();
        } catch (Throwable tr) {
            Log.e("SRTest", "", tr);
        }
    }

    public void queryDownloadManager(View view) {
        queryDownloadManager(view.getContext());
    }

    private void queryDownloadManager(Context context, long... ids) {
        DownloadManager.Query query = new DownloadManager.Query();
        if (ids.length > 0) {
            query.setFilterById(ids);
        }

        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Cursor cursor = downloadManager.query(query);
        if (cursor == null || !cursor.moveToFirst()) {
            return;
        }

        do {
            long id = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID));
            String uri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            String hint = cursor.getString(cursor.getColumnIndex("hint"));

            String exists = "?";
            if (uri != null) {
                File file = new File(Uri.parse(uri).getPath());
                exists = file.exists() ? "exists" : "not exists";
            }
            Log.i("SRTest", "query: id=" + id + ", uri=" + uri + ", hint=" + hint + ", status=" + status + ", exists=" + exists);
        } while (cursor.moveToNext());
    }

    public void insertDownloadManager(View view) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png"))
                .setTitle("Dummy File")
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(Uri.fromFile(new File("/storage/emulated/0/test/1")))
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true);

        DownloadManager downloadManager = (DownloadManager) view.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        long id = downloadManager.enqueue(request);
        queryDownloadManager(view.getContext(), id);

        view.getContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Fetching the download id received with the broadcast
                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                queryDownloadManager(context, id);

            }
        }, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void testApi() {
        StorageIsolationManager srm = StorageIsolationManager.create();
        if (srm == null) {
            Log.e("SRTest", "srm is null");
            return;
        }

        int userId = Process.myUid() / 100000;
        Log.i("SRTest", "getRedirectPackages: " + srm.getRedirectPackages(0, userId).toString());
        Log.i("SRTest", "getObservers: " + srm.getObservers(userId).toString());
        Log.i("SRTest", "getSimpleMounts: " + srm.getSimpleMounts(userId).toString());
        Log.i("SRTest", "getMountDirsTemplates: " + srm.getMountDirsTemplates().toString());
    }

    public void insertFile(View view) {
        Context context = view.getContext();
        ContentResolver cr = context.getContentResolver();
        ContentValues values;

        try {
            File file = new File(context.getFilesDir(), "1234568");
            file.createNewFile();
            try (OutputStream os = new FileOutputStream(file)) {
                os.write("test".getBytes());
            }

            values = new ContentValues();

            String displayName = "test_" + System.currentTimeMillis() + ".txt";
            if (Build.VERSION.SDK_INT >= 29) {
                values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS + "/test");
                values.put(MediaStore.MediaColumns.IS_PENDING, true);
            } else {
                values.put(MediaStore.MediaColumns.DATA, Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_DOWNLOADS + "/test/" + displayName);
            }
            values.put(MediaStore.MediaColumns.DISPLAY_NAME, displayName);

            Uri target;
            if (Build.VERSION.SDK_INT >= 29) {
                target = MediaStore.Downloads.EXTERNAL_CONTENT_URI;
            } else {
                target = MediaStore.Files.getContentUri("external");
            }
            Uri uri = cr.insert(target, values);
            if (uri == null) {
                return;
            }

            InputStream is = new FileInputStream(file);
            OutputStream os = cr.openOutputStream(uri, "rw");
            byte[] b = new byte[8192];
            for (int r; (r = is.read(b)) != -1; ) {
                os.write(b, 0, r);
            }
            os.flush();
            os.close();
            is.close();

            if (Build.VERSION.SDK_INT >= 29) {
                values = new ContentValues();
                values.put(MediaStore.Images.ImageColumns.IS_PENDING, false);
                cr.update(uri, values, null, null);
            }
        } catch (Throwable tr) {
        }
    }
}