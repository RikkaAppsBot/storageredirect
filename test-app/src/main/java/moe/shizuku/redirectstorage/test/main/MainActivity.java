package moe.shizuku.redirectstorage.test.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.system.ErrnoException;
import android.system.Os;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.io.FileNotFoundException;
import java.io.IOException;

import moe.shizuku.redirectstorage.test.R;
import moe.shizuku.redirectstorage.test.common.TestUtils;
import moe.shizuku.redirectstorage.test.databinding.ActivityMainBinding;

@SuppressLint("SdCardPath")
public class MainActivity extends Activity {

    private ActivityMainBinding mBinding;
    private final MainPresent mPresent = new MainPresent(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if ("android.intent.action.SEND".equals(getIntent().getAction())) {
            Uri uri = getIntent().getParcelableExtra(Intent.EXTRA_STREAM);

            try {
                Os.stat(uri.getPath());
                Toast.makeText(this, uri.getPath() + " is accessible", Toast.LENGTH_LONG).show();
            } catch (ErrnoException e) {
                e.printStackTrace();
                Toast.makeText(this, uri.getPath() + "\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else if ("android.intent.action.VIEW".equals(getIntent().getAction())) {
            try {
                getContentResolver().openInputStream(getIntent().getData()).close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else */{
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            mBinding.setPresent(mPresent);
            //TestUtils.writeStringToFile("/storage/emulated/0/.Android", "1.baiduyun.p.download", "Created by MainActivity");
        }
    }

    void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    boolean isStoragePermissionGranted() {
        return checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }
}
