package moe.shizuku.redirectstorage.test.binder;

import android.os.IBinder;

public interface IProxyBinder<T> extends IBinder {

    T getOriginal();

    boolean isTransactionReplaced(int code);
}
