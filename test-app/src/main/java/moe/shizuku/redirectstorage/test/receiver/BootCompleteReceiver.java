package moe.shizuku.redirectstorage.test.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import moe.shizuku.redirectstorage.test.service.ForegroundService;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(intent.getAction())) {
            return;
        }

        ContextCompat.startForegroundService(context, new Intent(context, ForegroundService.class));
    }
}
