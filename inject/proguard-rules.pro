-repackageclasses moe.shizuku.redirectstorage

-keep class moe.shizuku.redirectstorage.AppProcess { public <methods>;}
-keep class moe.shizuku.redirectstorage.SystemProcess { public <methods>;}

-assumenosideeffects class android.util.Log {
    public static *** d(...);
}

-keepattributes LineNumberTable