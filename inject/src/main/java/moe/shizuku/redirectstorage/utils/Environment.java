package moe.shizuku.redirectstorage.utils;

import java.io.File;

public class Environment {

    private static String externalStoragePath;

    public static String getExternalStoragePath() {
        if (externalStoragePath == null) {
            externalStoragePath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return externalStoragePath;
    }

    public static String getExternalStoragePrefix() {
        return getExternalStoragePath() + File.separator;
    }
}
