package moe.shizuku.redirectstorage.utils;

public class StringUtils {

    public static boolean startWithIgnoreCase(String in, String prefix) {
        if (in == null || prefix == null)
            return false;

        return in.toLowerCase().startsWith(prefix.toLowerCase());
    }

    public static String replaceFirstNoRegex(String in, CharSequence target, CharSequence replacement) {
        if (in == null || target == null || replacement == null)
            return in;

        StringBuilder sb = new StringBuilder(in);

        String replacementStr = replacement.toString();
        String targetStr = target.toString();
        int targetLen = targetStr.length();
        int index = in.indexOf(targetStr);
        if (index != -1) {
            sb.replace(index, index + targetLen, replacementStr);
        }
        return sb.toString();
    }
}
