package moe.shizuku.redirectstorage.utils;

import android.database.Cursor;
import android.database.MatrixCursor;

public class CursorUtils {

    public interface OnAddRowListener {
        boolean onAddRow(Object[] row);
    }

    public static Cursor clone(Cursor oldCursor, OnAddRowListener listener) {
        if (oldCursor == null)
            return null;

        MatrixCursor newCursor = new MatrixCursor(oldCursor.getColumnNames(), oldCursor.getCount());

        int oldPosition = oldCursor.getPosition();
        int numColumns = oldCursor.getColumnCount();

        int position;
        if (oldCursor.moveToFirst() && oldCursor.getCount() > 0) {
            position = 0;

            int[] columnTypes = new int[numColumns];
            for (int i = 0; i < numColumns; i++) {
                columnTypes[i] = oldCursor.getType(i);
            }
            do {
                Object[] row = new Object[numColumns];
                for (int i = 0; i < numColumns; i++) {
                    int fieldType = columnTypes[i];
                    if (fieldType == Cursor.FIELD_TYPE_BLOB)
                        row[i] = oldCursor.getBlob(i);
                    else if (fieldType == Cursor.FIELD_TYPE_FLOAT)
                        row[i] = oldCursor.getDouble(i);
                    else if (fieldType == Cursor.FIELD_TYPE_INTEGER)
                        row[i] = oldCursor.getLong(i);
                    else if (fieldType == Cursor.FIELD_TYPE_STRING)
                        row[i] = oldCursor.getString(i);
                    else if (fieldType == Cursor.FIELD_TYPE_NULL)
                        row[i] = null;
                }

                if (listener != null && !listener.onAddRow(row)) {
                    if (oldPosition != -1 && oldPosition >= position)
                        oldPosition --;
                    continue;
                }

                newCursor.addRow(row);
                position ++;
            } while (oldCursor.moveToNext());
        }

        oldCursor.moveToPosition(oldPosition);
        newCursor.moveToPosition(oldPosition);
        return newCursor;
    }
}
