package moe.shizuku.redirectstorage.utils;

import android.net.Uri;
import android.util.Pair;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class FileUtils {

    private static final String PREFIX_SDCARD = File.separator + "sdcard" + File.separator;
    private static final String SDCARD = File.separator + "sdcard";

    public static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    public static boolean isExternalPublicFile(String file) {
        if (file == null)
            return false;

        return StringUtils.startWithIgnoreCase(file, PREFIX_SDCARD) || StringUtils.startWithIgnoreCase(file, Environment.getExternalStoragePath() + File.separator);
    }

    public static String getRelativePathOfExternalStorage(String path) {
        if (StringUtils.startWithIgnoreCase(path, Environment.getExternalStoragePrefix()))
            return path.substring(Environment.getExternalStoragePrefix().length());
        else if (StringUtils.startWithIgnoreCase(path, PREFIX_SDCARD))
            return path.substring(PREFIX_SDCARD.length());
        else
            return null;
    }

    public static Uri getFixedUri(Uri uri, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        return getRealFileUri(uri, target, mounts, observers);
    }

    private static Uri getRealFileUri(Uri uri, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        String path = getRealFilePath(uri.getPath(), target, mounts, observers);
        if (path != null) {
            return Uri.fromFile(new File(path));
        }
        return null;
    }

    public static String getRealFilePath(String _path, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        String path, pathRelatedOfStorage, prefix;

        path = canonicalize(_path);

        // check if in /storage
        if (StringUtils.startWithIgnoreCase(path, Environment.getExternalStoragePrefix())) {
            prefix = Environment.getExternalStoragePath();
            pathRelatedOfStorage = path.substring(Environment.getExternalStoragePrefix().length());
        } else if (StringUtils.startWithIgnoreCase(path, PREFIX_SDCARD)) {
            prefix = SDCARD;
            pathRelatedOfStorage = path.substring(PREFIX_SDCARD.length());
        } else {
            return null;
        }

        //Log.i(TAG, "path in storage " + pathInStorage);

        // check if in redirect target path
        if (StringUtils.startWithIgnoreCase(pathRelatedOfStorage + File.separator, target + File.separator))
            return null;

        // check if in observers
        for (Pair<String, String> observer : observers) {
            if (observer.first == null || observer.second == null) {
                continue;
            }

            if (StringUtils.startWithIgnoreCase(pathRelatedOfStorage + File.separator, observer.first + File.separator))
                return prefix + File.separator + observer.second + File.separator + pathRelatedOfStorage.substring((observer.first + File.separator).length());
        }

        // check if in mounts
        for (Pair<String, String> mount : mounts) {
            if (mount.first == null || mount.second == null) {
                continue;
            }

            if (Objects.equals(mount.first, mount.second)) {
                // direct mount
                if (StringUtils.startWithIgnoreCase(pathRelatedOfStorage + File.separator, mount.first + File.separator))
                    return null;
            } else {
                // simple mount
                if (StringUtils.startWithIgnoreCase(pathRelatedOfStorage + File.separator, mount.second + File.separator))
                    return prefix + File.separator + mount.first + File.separator + pathRelatedOfStorage.substring((mount.second + File.separator).length());
            }
        }
        return prefix + File.separator + target + File.separator + pathRelatedOfStorage;
    }
}
