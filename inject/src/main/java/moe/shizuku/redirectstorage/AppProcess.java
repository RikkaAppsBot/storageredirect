package moe.shizuku.redirectstorage;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import moe.shizuku.redirectstorage.app.StorageRedirectInfo;
import moe.shizuku.redirectstorage.binder.CallOriginalBinder;
import moe.shizuku.redirectstorage.proxy.provider.ContentProviderProxy;
import moe.shizuku.redirectstorage.utils.ParcelUtils;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

@SuppressWarnings("unused")
public final class AppProcess {

    private AppProcess() {
    }

    public static boolean transactNative(IBinder binder, int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        String descriptor;
        try {
            data.setDataPosition(0);
            descriptor = ParcelUtils.readInterfaceDescriptor(data);
            data.setDataPosition(0);
        } catch (Throwable tr) {
            LOGGER.e(tr, "transactNative");
            return false;
        }

        IBinder wrapped = new CallOriginalBinder(binder);
        if (descriptor != null) {
            if (ContentProviderProxy.DESCRIPTION.equals(descriptor)) {
                return ContentProviderProxy.transact(wrapped, descriptor, code, data, reply, flags);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        LOGGER.d("args=%s", Arrays.toString(args));

        String name = null, target = "Android/data/%s/sdcard";
        List<Pair<String, String>> mounts = new ArrayList<>();
        List<Pair<String, String>> observers = new ArrayList<>();

        if (args == null || args.length == 0) {
            StorageRedirectInfo.asRedirectedApp(name, target, mounts, observers);
            return;
        }

        int index = 0;
        boolean isMounts = false, isObservers = false;
        while (index < args.length) {
            if ("--name".equals(args[index])) {
                name = args[index + 1];
                index += 2;
            } else if ("--target".equals(args[index])) {
                target = args[index + 1];
                index += 2;
            } else if ("--mounts".equals(args[index])) {
                index += 1;
                isMounts = true;
                isObservers = false;
            } else if ("--observers".equals(args[index])) {
                index += 1;
                isMounts = false;
                isObservers = true;
            } else if (isMounts) {
                mounts.add(new Pair<>(args[index], args[index + 1]));
                index += 2;
            } else if (isObservers) {
                observers.add(new Pair<>(args[index], args[index + 1]));
                index += 2;
            } else {
                index += 1;
            }
        }

        LOGGER.d("name=%s", name);
        LOGGER.d("target=%s", target);
        LOGGER.d("mounts=%s", mounts);
        LOGGER.d("observers=%s", observers);

        StorageRedirectInfo.asRedirectedApp(name, target, mounts, observers);
    }
}
