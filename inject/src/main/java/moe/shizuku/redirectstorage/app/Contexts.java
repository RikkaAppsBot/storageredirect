package moe.shizuku.redirectstorage.app;

import android.app.ActivityThread;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import moe.shizuku.redirectstorage.app.StorageRedirectInfo;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class Contexts {

    private static final String SR_APP_PACKAGE = "moe.shizuku.redirectstorage";

    public static Application getApplication() {
        ActivityThread activityThread = ActivityThread.currentActivityThread();
        if (activityThread == null)
            throw new NullPointerException("ActivityThread.currentActivityThread() returns null");

        return activityThread.getApplication();
    }

    public static Context getApplicationContext() {
        return getApplication();
    }

    public static String getPackageName() {
        Application application = getApplication();
        if (application == null)
            throw new NullPointerException("ActivityThread.currentActivityThread().getApplication() returns null");

        return application.getPackageName();
    }

    public static Context getContextForStorageRedirectApp() {
        Application application = getApplication();
        if (application == null)
            return null;

        Context base = application.getBaseContext();

        try {
            Context context = base.createPackageContext(SR_APP_PACKAGE, 0);
            if (StorageRedirectInfo.self().getLocale() != null) {
                Configuration configuration = new Configuration(context.getResources().getConfiguration());
                configuration.setLocale(StorageRedirectInfo.self().getLocale());
                return context.createConfigurationContext(configuration);
            } else {
                return context;
            }
        } catch (Throwable tr) {
            LOGGER.e("getContextForStorageRedirectApp", tr);
            return null;
        }
    }
}
