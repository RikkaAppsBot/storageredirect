package moe.shizuku.redirectstorage.app;

import android.text.TextUtils;
import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StorageRedirectInfo {

    private static StorageRedirectInfo instance;

    private boolean isRedirectedApp;
    private String name;
    private String target;
    private boolean targetFormatted;
    private List<Pair<String, String>> mounts;
    private List<Pair<String, String>> observers;
    private Locale locale;
    private boolean showToast;

    public static void asRedirectedApp(String name, String target, List<Pair<String, String>> mounts, List<Pair<String, String>> observers) {
        instance = new StorageRedirectInfo();
        instance.isRedirectedApp = true;
        instance.name = name;
        instance.target = target;
        instance.mounts = mounts;
        instance.observers = observers;
    }

    public static StorageRedirectInfo self() {
        return instance;
    }

    public String getName() {
        if (TextUtils.isEmpty(name)) {
            return Contexts.getPackageName();
        }
        return name;
    }

    public String getTarget() {
        if (!targetFormatted) {
            if (target.contains("%s")) {
                target = String.format(target, Contexts.getPackageName());
            }
            targetFormatted = true;
        }
        return target;
    }

    public List<Pair<String, String>> getMounts() {
        return mounts;
    }

    public List<Pair<String, String>> getObservers() {
        return observers;
    }

    public Locale getLocale() {
        return Locale.getDefault();
    }

    @NonNull
    @Override
    public String toString() {
        return "StorageRedirectInfo{" +
                "isRedirectedApp=" + isRedirectedApp +
                ", target='" + target + '\'' +
                ", mounts=" + mounts.toString() +
                ", observers=" + observers.toString() +
                '}';
    }
}
