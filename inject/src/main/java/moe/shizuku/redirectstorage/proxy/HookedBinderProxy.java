package moe.shizuku.redirectstorage.proxy;

import android.os.IBinder;

public interface HookedBinderProxy<T> extends IBinder {

    T getOriginal();

    boolean isTransactionReplaced(int code);
}
