package moe.shizuku.redirectstorage.proxy.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

import moe.shizuku.redirectstorage.app.StorageRedirectInfo;
import moe.shizuku.redirectstorage.utils.CursorUtils;
import moe.shizuku.redirectstorage.utils.FileUtils;
import moe.shizuku.redirectstorage.utils.StringUtils;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class DownloadProviderProxy {

    private static String getRedirectedFile(String file) {
        if (!FileUtils.isExternalPublicFile(file))
            return file;

        StorageRedirectInfo info = StorageRedirectInfo.self();
        return FileUtils.getRealFilePath(file, info.getTarget(), info.getMounts(), info.getObservers());
    }

    public static ContentValues insert(Uri uri, ContentValues initialValues) {
        LOGGER.v("insert: uri=" + uri
                + ", initialValues=" + initialValues);

        String uriString = initialValues.getAsString("hint"); // *Downloads.Impl.COLUMN_FILE_NAME_HINT
        if (uriString != null) {
            String file = Uri.parse(uriString).getPath();
            String redirected = getRedirectedFile(file);
            if (redirected != null && !Objects.equals(file, getRedirectedFile(file))) {
                LOGGER.v("insert: %s -> %s", file, redirected);
                initialValues.put("hint", "file://" + redirected);
            }
        }

        return initialValues;
    }

    public static Cursor query(Cursor res, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (true) {
            return res;
        }

        if (res == null) {
            return null;
        }

        LOGGER.v("query: uri=" + uri
                + ", projection=" + (projection != null ? Arrays.toString(projection) : "null")
                + ", selection=" + selection
                + ", selectionArgs=" + (selectionArgs != null ? Arrays.toString(selectionArgs) : "null")
                + ", sortOrder=" + sortOrder
                + ", resultCount=" + res.getCount()
        );

        if (!"/my_downloads".equals(uri.getPath())) {
            return res;
        }

        final StorageRedirectInfo info = StorageRedirectInfo.self();
        final String[] isolatedStoragePaths = new String[]{
                String.format("Android/data/%s/sdcard", info.getName()),
                String.format("Android/data/%s/cache/sdcard", info.getName()),
        };
        final int dataIndex = res.getColumnIndex("_data");
        final int localFileNameIndex = res.getColumnIndex("local_filename");
        final int pathIndex = dataIndex == -1 ? localFileNameIndex : dataIndex;
        final int hintIndex = res.getColumnIndex("hint");
        if (hintIndex == -1) {
            return res;
        }

        CursorUtils.OnAddRowListener listener = row -> {
            if (!(row[hintIndex] instanceof String)) {
                return true;
            }

            String hintString = (String) row[hintIndex];
            Uri hintUri = Uri.parse(hintString);
            if (!"file".equals(hintUri.getScheme())) {
                return true;
            }

            String path = hintUri.getPath();
            String pathInStorage = FileUtils.getRelativePathOfExternalStorage(path);
            if (TextUtils.isEmpty(pathInStorage))
                return true;

            for (String isolatedStoragePath : isolatedStoragePaths) {
                if (StringUtils.startWithIgnoreCase(pathInStorage + File.separator, isolatedStoragePath + File.separator)) {
                    String newHint = StringUtils.replaceFirstNoRegex(hintString, isolatedStoragePath + File.separator, "");
                    LOGGER.v("query: hint %s -> %s", hintString, newHint);
                    row[hintIndex] = newHint;

                    if (pathIndex != -1 && row[pathIndex] instanceof String) {
                        String newPath = StringUtils.replaceFirstNoRegex(path, isolatedStoragePath + File.separator, "");
                        LOGGER.v("query: _data %s -> %s", path, newPath);
                        row[pathIndex] = newPath;
                    }
                    break;
                }
            }
            return true;
        };

        try {
            Cursor newCursor = CursorUtils.clone(res, listener);
            res.close();

            return newCursor;
        } catch (Throwable tr) {
            LOGGER.e("CursorUtils.clone", tr);
            //ToastHelper.showText("CursorUtils#clone: " + tr.getMessage());
            return null;
        }
    }
}
