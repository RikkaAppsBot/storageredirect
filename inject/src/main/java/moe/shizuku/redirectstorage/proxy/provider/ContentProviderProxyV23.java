package moe.shizuku.redirectstorage.proxy.provider;

import android.content.ContentProviderNative;
import android.content.ContentValues;
import android.content.IContentProvider;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ICancellationSignal;
import android.os.RemoteException;

import java.util.Set;

import moe.shizuku.redirectstorage.proxy.HookedBinderProxy;
import moe.shizuku.redirectstorage.binder.Transaction;
import moe.shizuku.redirectstorage.binder.TransactionCodeExporter;

public class ContentProviderProxyV23 extends ContentProviderNative implements HookedBinderProxy<IContentProvider> {

    private static final Set<Integer> CODES = TransactionCodeExporter.exportAll(ContentProviderNative.class, ContentProviderProxyV23.class);

    private IContentProvider mOriginal;

    public ContentProviderProxyV23(IContentProvider original) {
        mOriginal = original;
    }

    @Override
    public IContentProvider getOriginal() {
        return mOriginal;
    }

    @Override
    public boolean isTransactionReplaced(int code) {
        return CODES.contains(code);
    }

    @Override
    public String getProviderName() {
        return "";
    }

    @Transaction
    @Override
    public Cursor query(String callingPkg, Uri url, String[] projection, String selection, String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal) throws RemoteException {
        Cursor res = mOriginal.query(callingPkg, url, projection, selection, selectionArgs, sortOrder, cancellationSignal);

        if (url != null && "media".equals(url.getAuthority())) {
            return MediaProviderProxy.query(res, url, projection, selection, selectionArgs, sortOrder);
        } else if (url != null && "downloads".equals(url.getAuthority())) {
            return DownloadProviderProxy.query(res, url, projection, selection, selectionArgs, sortOrder);
        } else {
            return res;
        }
    }

    @Transaction
    @Override
    public Uri insert(String callingPkg, Uri url, ContentValues initialValues) throws RemoteException {
        if (url == null || url.getAuthority() == null) {
            return mOriginal.insert(callingPkg, url, initialValues);
        } else if ("media".equals(url.getAuthority())) {
            initialValues = MediaProviderProxy.insert(url, initialValues);
            if (initialValues == null) {
                return null;
            }

            return mOriginal.insert(callingPkg, url, initialValues);
        } else if ("downloads".equals(url.getAuthority())) {
            initialValues = DownloadProviderProxy.insert(url, initialValues);
            if (initialValues == null) {
                return null;
            }

            return mOriginal.insert(callingPkg, url, initialValues);
        } else {
            return mOriginal.insert(callingPkg, url, initialValues);
        }
    }

    @Transaction
    @Override
    public int bulkInsert(String callingPkg, Uri url, ContentValues[] initialValues) throws RemoteException {
        if (url == null || !"media".equals(url.getAuthority())) {
            return mOriginal.bulkInsert(callingPkg, url, initialValues);
        }

        initialValues = MediaProviderProxy.bulkInsert(url, initialValues);

        return mOriginal.bulkInsert(callingPkg, url, initialValues);
    }

    @Transaction
    @Override
    public int delete(String callingPkg, Uri url, String selection, String[] selectionArgs) throws RemoteException {
        if (url == null || !"media".equals(url.getAuthority())) {
            return mOriginal.delete(callingPkg, url, selection, selectionArgs);
        }

        if (MediaProviderProxy.delete(url, selection, selectionArgs)) {
            return mOriginal.delete(callingPkg, url, selection, selectionArgs);
        }

        return 0;
    }

    @Override
    public Cursor query(String callingPkg, Uri url, String[] projection, Bundle queryArgs, ICancellationSignal cancellationSignal) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
