package moe.shizuku.redirectstorage.proxy.provider;

import android.content.ContentProviderNative;
import android.content.IContentProvider;
import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

import moe.shizuku.redirectstorage.proxy.HookedBinderProxy;

public class ContentProviderProxy {

    public static final String DESCRIPTION = "android.content.IContentProvider";

    private static HookedBinderProxy<?> create(IBinder binder) {
        IContentProvider provider = ContentProviderNative.asInterface(binder);
        if (provider == null) {
            return null;
        }

        if (Build.VERSION.SDK_INT >= 30) {
            return new ContentProviderProxyV30(provider);
        } else if (Build.VERSION.SDK_INT >= 26) {
            return new ContentProviderProxyV26(provider);
        } else if (Build.VERSION.SDK_INT >= 23) {
            return new ContentProviderProxyV23(provider);
        } else {
            return null;
        }
    }

    public static boolean transact(IBinder original, String descriptor, int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        HookedBinderProxy<?> proxyBinder = create(original);
        if (proxyBinder == null) {
            return false;
        }
        if (proxyBinder.isTransactionReplaced(code)) {
            proxyBinder.transact(code, data, reply, flags);
            return true;
        }
        return false;
    }
}
