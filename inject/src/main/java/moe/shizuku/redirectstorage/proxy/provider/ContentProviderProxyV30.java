package moe.shizuku.redirectstorage.proxy.provider;

import android.content.ContentProviderNative;
import android.content.ContentValues;
import android.content.IContentProvider;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.RemoteException;

import androidx.annotation.RequiresApi;

import java.lang.reflect.Constructor;
import java.util.Set;

import moe.shizuku.redirectstorage.proxy.HookedBinderProxy;
import moe.shizuku.redirectstorage.binder.Transaction;
import moe.shizuku.redirectstorage.binder.TransactionCodeExporter;

@RequiresApi(api = 30)
public class ContentProviderProxyV30 extends ContentProviderNative implements HookedBinderProxy<IContentProvider> {

    private static final Set<Integer> CODES = TransactionCodeExporter.exportAll("IContentProvider", (binder) -> {
        try {
            Class<?> cls = Class.forName("android.content.ContentProviderProxy");
            Constructor<?> constructor = cls.getConstructor(IBinder.class);
            constructor.setAccessible(true);
            return constructor.newInstance(binder);
        } catch (Throwable tr) {
            throw new RuntimeException(tr);
        }
    }, ContentProviderProxyV30.class);

    private final IContentProvider mOriginal;

    public ContentProviderProxyV30(IContentProvider original) {
        mOriginal = original;
    }

    @Override
    public IContentProvider getOriginal() {
        return mOriginal;
    }

    @Override
    public boolean isTransactionReplaced(int code) {
        return CODES.contains(code);
    }

    @Override
    public String getProviderName() {
        return "";
    }

    @Transaction
    @Override
    public Cursor query(String callingPkg, String featureId, Uri url, String[] projection, Bundle queryArgs, ICancellationSignal cancellationSignal) throws RemoteException {
        Cursor res = mOriginal.query(callingPkg, featureId, url, projection, queryArgs, cancellationSignal);

        if (url != null && "media".equals(url.getAuthority())) {
            String selection = null;
            String[] selectionArgs = null;
            String sortOrder = null;
            if (queryArgs != null) {
                selection = queryArgs.getString("android:query-arg-sql-selection");
                selectionArgs = queryArgs.getStringArray("android:query-arg-sql-selection-args");
                sortOrder = queryArgs.getString("android:query-arg-sql-sort-order");
            }

            return MediaProviderProxy.query(res, url, projection, selection, selectionArgs, sortOrder);
        } else if (url != null && "downloads".equals(url.getAuthority())) {
            String selection = null;
            String[] selectionArgs = null;
            String sortOrder = null;
            if (queryArgs != null) {
                selection = queryArgs.getString("android:query-arg-sql-selection");
                selectionArgs = queryArgs.getStringArray("android:query-arg-sql-selection-args");
                sortOrder = queryArgs.getString("android:query-arg-sql-sort-order");
            }

            return DownloadProviderProxy.query(res, url, projection, selection, selectionArgs, sortOrder);
        } else {
            return res;
        }
    }

    @Transaction
    @Override
    public Uri insert(String callingPkg, String featureId, Uri url, ContentValues values, Bundle extras) throws RemoteException {
        if (url == null || url.getAuthority() == null) {
            return mOriginal.insert(callingPkg, featureId, url, values, extras);
        } else if ("media".equals(url.getAuthority())) {
            values = MediaProviderProxy.insert(url, values);
            if (values == null) {
                return null;
            }

            return mOriginal.insert(callingPkg, featureId, url, values, extras);
        } else if ("downloads".equals(url.getAuthority())) {
            values = DownloadProviderProxy.insert(url, values);
            if (values == null) {
                return null;
            }

            return mOriginal.insert(callingPkg, featureId, url, values, extras);
        } else {
            return mOriginal.insert(callingPkg, featureId, url, values, extras);
        }
    }

    @Transaction
    @Override
    public int bulkInsert(String callingPkg, String featureId, Uri url, ContentValues[] initialValues) throws RemoteException {
        if (url == null || !"media".equals(url.getAuthority())) {
            return mOriginal.bulkInsert(callingPkg, featureId, url, initialValues);
        }

        initialValues = MediaProviderProxy.bulkInsert(url, initialValues);

        return mOriginal.bulkInsert(callingPkg, featureId, url, initialValues);
    }

    @Transaction
    @Override
    public int delete(String callingPkg, String featureId, Uri url, Bundle extras) throws RemoteException {
        if (url == null || !"media".equals(url.getAuthority())) {
            return mOriginal.delete(callingPkg, featureId, url, extras);
        }

        String selection = null;
        String[] selectionArgs = null;
        if (extras != null) {
            selection = extras.getString("android:query-arg-sql-selection");
            selectionArgs = extras.getStringArray("android:query-arg-sql-selection-args");
        }
        if (MediaProviderProxy.delete(url, selection, selectionArgs)) {
            return mOriginal.delete(callingPkg, featureId, url, extras);
        }

        return 0;
    }
}
