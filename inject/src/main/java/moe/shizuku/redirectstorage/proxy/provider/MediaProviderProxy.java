package moe.shizuku.redirectstorage.proxy.provider;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Pair;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.app.StorageRedirectInfo;
import moe.shizuku.redirectstorage.utils.CursorUtils;
import moe.shizuku.redirectstorage.utils.FileUtils;
import moe.shizuku.redirectstorage.utils.StringUtils;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class MediaProviderProxy {

    private static final int IMAGES_MEDIA = 1;
    private static final int IMAGES_MEDIA_ID = 2;
    private static final int IMAGES_THUMBNAILS = 3;
    private static final int IMAGES_THUMBNAILS_ID = 4;
    private static final int AUDIO_MEDIA = 100;
    private static final int AUDIO_MEDIA_ID = 101;
    private static final int AUDIO_MEDIA_ID_GENRES = 102;
    private static final int AUDIO_MEDIA_ID_GENRES_ID = 103;
    private static final int AUDIO_MEDIA_ID_PLAYLISTS = 104;
    private static final int AUDIO_MEDIA_ID_PLAYLISTS_ID = 105;
    private static final int AUDIO_GENRES = 106;
    private static final int AUDIO_GENRES_ID = 107;
    private static final int AUDIO_GENRES_ID_MEMBERS = 108;
    private static final int AUDIO_GENRES_ALL_MEMBERS = 109;
    private static final int AUDIO_PLAYLISTS = 110;
    private static final int AUDIO_PLAYLISTS_ID = 111;
    private static final int AUDIO_PLAYLISTS_ID_MEMBERS = 112;
    private static final int AUDIO_PLAYLISTS_ID_MEMBERS_ID = 113;
    private static final int AUDIO_ARTISTS = 114;
    private static final int AUDIO_ARTISTS_ID = 115;
    private static final int AUDIO_ALBUMS = 116;
    private static final int AUDIO_ALBUMS_ID = 117;
    private static final int AUDIO_ARTISTS_ID_ALBUMS = 118;
    private static final int AUDIO_ALBUMART = 119;
    private static final int AUDIO_ALBUMART_ID = 120;
    private static final int AUDIO_ALBUMART_FILE_ID = 121;
    private static final int VIDEO_MEDIA = 200;
    private static final int VIDEO_MEDIA_ID = 201;
    private static final int VIDEO_THUMBNAILS = 202;
    private static final int VIDEO_THUMBNAILS_ID = 203;

    private static final int VOLUMES = 300;
    private static final int VOLUMES_ID = 301;
    private static final int AUDIO_SEARCH_LEGACY = 400;
    private static final int AUDIO_SEARCH_BASIC = 401;
    private static final int AUDIO_SEARCH_FANCY = 402;

    private static final int MEDIA_SCANNER = 500;
    private static final int FS_ID = 600;
    private static final int VERSION = 601;
    private static final int FILES = 700;
    private static final int FILES_ID = 701;
    // Used only by the MTP implementation
    private static final int MTP_OBJECTS = 702;
    private static final int MTP_OBJECTS_ID = 703;
    private static final int MTP_OBJECT_REFERENCES = 704;
    // UsbReceiver calls insert() and delete() with this URI to tell us
    // when MTP is connected and disconnected
    private static final int MTP_CONNECTED = 705;
    // Used only to invoke special logic for directories
    private static final int FILES_DIRECTORY = 706;

    private static final UriMatcher URI_MATCHER =
            new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI("media", "*/images/media", IMAGES_MEDIA);
        URI_MATCHER.addURI("media", "*/images/media/#", IMAGES_MEDIA_ID);
        URI_MATCHER.addURI("media", "*/images/thumbnails", IMAGES_THUMBNAILS);
        URI_MATCHER.addURI("media", "*/images/thumbnails/#", IMAGES_THUMBNAILS_ID);
        URI_MATCHER.addURI("media", "*/audio/media", AUDIO_MEDIA);
        URI_MATCHER.addURI("media", "*/audio/media/#", AUDIO_MEDIA_ID);
        URI_MATCHER.addURI("media", "*/audio/media/#/genres", AUDIO_MEDIA_ID_GENRES);
        URI_MATCHER.addURI("media", "*/audio/media/#/genres/#", AUDIO_MEDIA_ID_GENRES_ID);
        URI_MATCHER.addURI("media", "*/audio/media/#/playlists", AUDIO_MEDIA_ID_PLAYLISTS);
        URI_MATCHER.addURI("media", "*/audio/media/#/playlists/#", AUDIO_MEDIA_ID_PLAYLISTS_ID);
        URI_MATCHER.addURI("media", "*/audio/genres", AUDIO_GENRES);
        URI_MATCHER.addURI("media", "*/audio/genres/#", AUDIO_GENRES_ID);
        URI_MATCHER.addURI("media", "*/audio/genres/#/members", AUDIO_GENRES_ID_MEMBERS);
        URI_MATCHER.addURI("media", "*/audio/genres/all/members", AUDIO_GENRES_ALL_MEMBERS);
        URI_MATCHER.addURI("media", "*/audio/playlists", AUDIO_PLAYLISTS);
        URI_MATCHER.addURI("media", "*/audio/playlists/#", AUDIO_PLAYLISTS_ID);
        URI_MATCHER.addURI("media", "*/audio/playlists/#/members", AUDIO_PLAYLISTS_ID_MEMBERS);
        URI_MATCHER.addURI("media", "*/audio/playlists/#/members/#", AUDIO_PLAYLISTS_ID_MEMBERS_ID);
        URI_MATCHER.addURI("media", "*/audio/artists", AUDIO_ARTISTS);
        URI_MATCHER.addURI("media", "*/audio/artists/#", AUDIO_ARTISTS_ID);
        URI_MATCHER.addURI("media", "*/audio/artists/#/albums", AUDIO_ARTISTS_ID_ALBUMS);
        URI_MATCHER.addURI("media", "*/audio/albums", AUDIO_ALBUMS);
        URI_MATCHER.addURI("media", "*/audio/albums/#", AUDIO_ALBUMS_ID);
        URI_MATCHER.addURI("media", "*/audio/albumart", AUDIO_ALBUMART);
        URI_MATCHER.addURI("media", "*/audio/albumart/#", AUDIO_ALBUMART_ID);
        URI_MATCHER.addURI("media", "*/audio/media/#/albumart", AUDIO_ALBUMART_FILE_ID);
        URI_MATCHER.addURI("media", "*/video/media", VIDEO_MEDIA);
        URI_MATCHER.addURI("media", "*/video/media/#", VIDEO_MEDIA_ID);
        URI_MATCHER.addURI("media", "*/video/thumbnails", VIDEO_THUMBNAILS);
        URI_MATCHER.addURI("media", "*/video/thumbnails/#", VIDEO_THUMBNAILS_ID);
        URI_MATCHER.addURI("media", "*/media_scanner", MEDIA_SCANNER);
        URI_MATCHER.addURI("media", "*/fs_id", FS_ID);
        URI_MATCHER.addURI("media", "*/version", VERSION);
        URI_MATCHER.addURI("media", "*/mtp_connected", MTP_CONNECTED);
        URI_MATCHER.addURI("media", "*", VOLUMES_ID);
        URI_MATCHER.addURI("media", null, VOLUMES);
        // Used by MTP implementation
        URI_MATCHER.addURI("media", "*/file", FILES);
        URI_MATCHER.addURI("media", "*/file/#", FILES_ID);
        URI_MATCHER.addURI("media", "*/object", MTP_OBJECTS);
        URI_MATCHER.addURI("media", "*/object/#", MTP_OBJECTS_ID);
        URI_MATCHER.addURI("media", "*/object/#/references", MTP_OBJECT_REFERENCES);
        // Used only to trigger special logic for directories
        URI_MATCHER.addURI("media", "*/dir", FILES_DIRECTORY);

        /*
          use the 'basic' or 'fancy' search Uris instead
         */
        URI_MATCHER.addURI("media", "*/audio/" + SearchManager.SUGGEST_URI_PATH_QUERY,
                AUDIO_SEARCH_LEGACY);
        URI_MATCHER.addURI("media", "*/audio/" + SearchManager.SUGGEST_URI_PATH_QUERY + "/*",
                AUDIO_SEARCH_LEGACY);
        // used for search suggestions
        URI_MATCHER.addURI("media", "*/audio/search/" + SearchManager.SUGGEST_URI_PATH_QUERY,
                AUDIO_SEARCH_BASIC);
        URI_MATCHER.addURI("media", "*/audio/search/" + SearchManager.SUGGEST_URI_PATH_QUERY +
                "/*", AUDIO_SEARCH_BASIC);
        // used by the music app's search activity
        URI_MATCHER.addURI("media", "*/audio/search/fancy", AUDIO_SEARCH_FANCY);
        URI_MATCHER.addURI("media", "*/audio/search/fancy/*", AUDIO_SEARCH_FANCY);
    }

    private static String getVolumeName(Uri uri) {
        final List<String> segments = uri.getPathSegments();
        if (segments != null && segments.size() > 0) {
            return segments.get(0);
        } else {
            return null;
        }
    }

    private static List<Pair<String, String>> getMounts() {
        return StorageRedirectInfo.self().getMounts();
    }

    public static Cursor query(Cursor res, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (res == null) {
            return null;
        }

        if (!"external".equals(getVolumeName(uri)))
            return res;

        LOGGER.v("query: uri=" + uri
                + ", projection=" + (projection != null ? Arrays.toString(projection) : "null")
                + ", selection=" + selection
                + ", selectionArgs=" + (selectionArgs != null ? Arrays.toString(selectionArgs) : "null")
                + ", sortOrder=" + sortOrder
                + ", resultCount=" + res.getCount()
        );

        switch (URI_MATCHER.match(uri)) {
            case IMAGES_MEDIA:
            case IMAGES_THUMBNAILS:
            case AUDIO_MEDIA:
            case VIDEO_MEDIA:
            case VIDEO_THUMBNAILS:
            case FILES:
                break;
            default:
                return res;
        }

        final int pathIndex = res.getColumnIndex(MediaStore.MediaColumns.DATA);
        if (pathIndex == -1) {
            return res;
        }

        CursorUtils.OnAddRowListener listener = row -> {
            if (!(row[pathIndex] instanceof String)) {
                return true;
            }

            String path = (String) row[pathIndex];
            String pathInStorage = FileUtils.getRelativePathOfExternalStorage(path);

            if (TextUtils.isEmpty(pathInStorage))
                return true;

            pathInStorage = FileUtils.canonicalize(pathInStorage);

            //LOGGER.v("pathInStorage: %s", pathInStorage);

                /*if (StringUtils.startWithIgnoreCase(pathInStorage, "Android/sandbox"))
                    return true;*/

            boolean skip = true;
            for (Pair<String, String> mount : getMounts()) {
                if (mount.first == null || mount.second == null) {
                    continue;
                }

                if (Objects.equals(mount.first, mount.second)) {
                    if (StringUtils.startWithIgnoreCase(pathInStorage + File.separator, mount.first + File.separator)) {
                        skip = false;
                        break;
                    }
                }
            }
            if (skip) {
                LOGGER.v("query: remove %s from result", path);
                return false;
            }

            return true;
        };

        try {
            Cursor newCursor = CursorUtils.clone(res, listener);
            res.close();

            //ToastHelper.showMediaFiltered();

            return newCursor;
        } catch (Throwable tr) {
            LOGGER.e("CursorUtils.clone", tr);
            //ToastHelper.showText("CursorUtils#clone: " + tr.getMessage());
            return null;
        }
    }

    private static String getRedirectedFile(String file) {
        if (!FileUtils.isExternalPublicFile(file))
            return file;

        StorageRedirectInfo info = StorageRedirectInfo.self();
        return FileUtils.getRealFilePath(file, info.getTarget(), info.getMounts(), info.getObservers());
    }

    public static ContentValues insert(Uri uri, ContentValues initialValues) {
        LOGGER.v("insert: uri=" + uri
                + ", initialValues=" + initialValues);

        String file = initialValues.getAsString("_data");
        if (file != null) {
            String redirected = getRedirectedFile(file);
            if (redirected != null && !Objects.equals(file, getRedirectedFile(file))) {
                LOGGER.v("insert: %s -> %s", file, redirected);
                initialValues.put("_data", redirected);
            }
        }

        return initialValues;
    }

    public static ContentValues[] bulkInsert(Uri uri, ContentValues[] initialValues) {
        LOGGER.v("insert: uri=" + uri
                + ", initialValues=" + Arrays.toString(initialValues));

        List<ContentValues> newValues = new ArrayList<>();
        for (ContentValues values : initialValues) {
            String file = values.getAsString("_data");
            if (file != null) {
                String redirected = getRedirectedFile(file);
                if (redirected != null && !Objects.equals(file, getRedirectedFile(file))) {
                    LOGGER.v("insert: %s -> %s", file, redirected);
                    values.put("_data", redirected);
                }
            }
            newValues.add(values);
        }

        return newValues.toArray(new ContentValues[0]);
    }

    public static boolean delete(Uri uri, String selection, String[] selectionArgs) {
        LOGGER.v("delete: uri=" + uri
                + ", selection=" + selection
                + ", selectionArgs=" + Arrays.toString(selectionArgs));

        return true;
    }
}
