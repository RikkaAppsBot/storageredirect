package moe.shizuku.redirectstorage.proxy.vold;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.IBinder;
import android.os.IVold;
import android.os.Parcel;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import moe.shizuku.redirectstorage.proxy.HookedBinderProxy;
import moe.shizuku.redirectstorage.binder.IBinderWrapper;
import moe.shizuku.redirectstorage.server.StorageIsolationBridgeService;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class VoldProxy extends IBinderWrapper {

    private static final Map<Integer, String> MOUNT_MODES = new LinkedHashMap<>();

    static {
        try {
            @SuppressLint("PrivateApi")
            Class<?> cls = Class.forName("com.android.internal.os.Zygote");
            for (Field field : cls.getDeclaredFields()) {
                if ((field.getModifiers() & Modifier.STATIC) == 0) {
                    continue;
                }

                String name = field.getName();
                switch (name) {
                    case "MOUNT_EXTERNAL_NONE":
                    case "MOUNT_EXTERNAL_DEFAULT":
                    case "MOUNT_EXTERNAL_READ":
                    case "MOUNT_EXTERNAL_WRITE":
                    case "MOUNT_EXTERNAL_LEGACY":
                    case "MOUNT_EXTERNAL_INSTALLER":
                    case "MOUNT_EXTERNAL_FULL":
                        MOUNT_MODES.put(field.getInt(null), name);
                        break;
                }

            }
        } catch (ClassNotFoundException | IllegalAccessException ignored) {
        }

        for (Map.Entry<Integer, String> entry : MOUNT_MODES.entrySet()) {
            LOGGER.d("%s is %d", entry.getValue(), entry.getKey());
        }
    }

    private static HookedBinderProxy<?> create(IBinder binder) {
        if (Build.VERSION.SDK_INT < 28) {
            return null;
        }

        IVold vold = IVold.Stub.asInterface(binder);
        if (vold == null) {
            return null;
        }
        return new VoldProxyV28(vold);
    }

    private final HookedBinderProxy<?> proxyBinder;

    public VoldProxy(IBinder original) {
        super(original);
        proxyBinder = create(original);
    }

    @Override
    public boolean transact(int code, @NonNull Parcel data, Parcel reply, int flags) throws RemoteException {
        if (proxyBinder == null || !proxyBinder.isTransactionReplaced(code)) {
            return super.transact(code, data, reply, flags);
        }

        proxyBinder.transact(code, data, reply, flags);
        return true;
    }

    protected static boolean remountUid(int uid, int remountMode) {
        String name = MOUNT_MODES.get(remountMode);
        if (name == null) {
            name = String.format(Locale.ENGLISH, "unknown (%d)", remountMode);
        }
        LOGGER.v("remountUid: uid=%d, remountMode=%s", uid, name);

        boolean block = StorageIsolationBridgeService.getIsolatedUids().contains(uid);
        if (block) {
            LOGGER.i("remountUid: isolation is enabled for uid %d, skip", uid);
            return false;
        } else {
            LOGGER.v("remountUid: isolation is not enabled for uid %d", uid);
        }
        return true;
    }
}
