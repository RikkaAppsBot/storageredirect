package moe.shizuku.redirectstorage.proxy.vold;

import android.os.IVold;

import java.util.Set;

import moe.shizuku.redirectstorage.proxy.HookedBinderProxy;
import moe.shizuku.redirectstorage.binder.Transaction;
import moe.shizuku.redirectstorage.binder.TransactionCodeExporter;

public class VoldProxyV28 extends IVold.Stub implements HookedBinderProxy<IVold> {

    private static final Set<Integer> CODES = TransactionCodeExporter.exportAll(IVold.Stub.class, VoldProxyV28.class);

    private final IVold mOriginal;

    public VoldProxyV28(IVold original) {
        mOriginal = original;
    }

    @Override
    public IVold getOriginal() {
        return mOriginal;
    }

    @Override
    public boolean isTransactionReplaced(int code) {
        return CODES.contains(code);
    }

    @Transaction
    @Override
    public void remountUid(int uid, int remountMode) {
        if (VoldProxy.remountUid(uid, remountMode)) {
            mOriginal.remountUid(uid, remountMode);
        }
    }
}
