package moe.shizuku.redirectstorage;

import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import moe.shizuku.redirectstorage.binder.ServiceManagerProxy;
import moe.shizuku.redirectstorage.proxy.vold.VoldProxy;
import moe.shizuku.redirectstorage.server.PackageReceiver;
import moe.shizuku.redirectstorage.server.ServiceWaiter;
import moe.shizuku.redirectstorage.server.StorageIsolationBridgeService;
import moe.shizuku.redirectstorage.server.StorageIsolationBridgeServiceV23;
import moe.shizuku.redirectstorage.server.StorageIsolationBridgeServiceV26;
import moe.shizuku.redirectstorage.utils.ParcelUtils;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public final class SystemProcess extends ServiceManagerProxy {

    private static final StorageIsolationBridgeService SERVICE = new StorageIsolationBridgeService();

    private static final List<String> REQUIRED_SERVICES;

    static {
        REQUIRED_SERVICES = new ArrayList<>();
        REQUIRED_SERVICES.add("package");
        REQUIRED_SERVICES.add("user");
        REQUIRED_SERVICES.add("activity");
        REQUIRED_SERVICES.add("appops");
        REQUIRED_SERVICES.add("mount");
        REQUIRED_SERVICES.add("input");
        REQUIRED_SERVICES.add("launcherapps");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            REQUIRED_SERVICES.add("permissionmgr");
        }
    }

    private static final int V23_0 = 49;

    private final int versionCode;

    private IBinder activityService;

    private List<String> addedServices = new ArrayList<>();

    private SystemProcess(int versionCode) {
        this.versionCode = versionCode;
    }

    @Override
    protected IBinder addServicePre(String name, IBinder service) {
        LOGGER.d("add service %s", name);

        if ("activity".equals(name)) {
            try {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(PackageReceiver::register);
            } catch (Throwable e) {
                LOGGER.w("post", e);
            }

            if (versionCode >= V23_0) return service;

            LOGGER.d("addService %s %s", name, service);
            activityService = service;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                LOGGER.d("StorageIsolationBridgeServiceV26");
                return new StorageIsolationBridgeServiceV26(SERVICE, service);
            } else {
                LOGGER.d("StorageIsolationBridgeServiceV23");
                return new StorageIsolationBridgeServiceV23(SERVICE, service);
            }
        }
        return service;
    }

    @Override
    protected void addServicePost(String name, IBinder service) {
        if (addedServices == null) return;

        if (REQUIRED_SERVICES.contains(name)) {
            addedServices.add(name);

            if (addedServices.containsAll(REQUIRED_SERVICES)) {
                addedServices = null;
                LOGGER.v("waiting for service...");
                ServiceWaiter.startWait();
                /*Handler handler = new Handler(Looper.getMainLooper());
                handler.post(ServiceWaiter::startWait);*/
            }
        }
    }

    @Override
    protected IBinder getServicePre(String name, IBinder service) {
        if ("vold".equals(name)) {
            LOGGER.d("getService %s %s", name, service);
            return new VoldProxy(service);
        }

        if (versionCode < V23_0) {
            if ("activity".equals(name)) {
                return activityService;
            }
        }
        return service;
    }

    public static boolean execTransact(int code, long dataObj, long replyObj, int flags) {
        Parcel data = ParcelUtils.fromNativePointer(dataObj);
        Parcel reply = ParcelUtils.fromNativePointer(replyObj);

        boolean res = false;

        if (data == null) {
            return res;
        }

        try {
            String descriptor = ParcelUtils.readInterfaceDescriptor(data);
            data.setDataPosition(0);

            if (SERVICE.isServiceDescriptor(descriptor)) {
                res = SERVICE.onTransact(code, data, reply, flags);
            }
        } catch (Exception e) {
            if ((flags & IBinder.FLAG_ONEWAY) != 0) {
                LOGGER.w(e, "Caught a Exception from the binder stub implementation.");
            } else {
                reply.setDataPosition(0);
                reply.writeException(e);
            }
            res = true;
        }

        if (res) {
            if (data != null) data.recycle();
            if (reply != null) reply.recycle();
        }

        return res;
    }

    public static void main(String[] args) {
        LOGGER.d("main: %s", Arrays.toString(args));

        int versionCode = -1;
        if (args != null) {
            for (String arg : args) {
                if (arg.startsWith("--version-code=")) {
                    try {
                        versionCode = Integer.parseInt(arg.substring("--version-code=".length()));
                    } catch (Throwable ignored) {
                    }
                }
            }
        }

        LOGGER.d("version code %d", versionCode);

        try {
            new SystemProcess(versionCode).install();
            LOGGER.i("install");
        } catch (Throwable e) {
            LOGGER.w(e, "install failed");
        }
    }
}
