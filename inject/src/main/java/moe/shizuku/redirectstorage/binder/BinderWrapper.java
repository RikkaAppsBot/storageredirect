package moe.shizuku.redirectstorage.binder;

import android.os.Binder;
import android.os.IBinder;

public class BinderWrapper extends Binder {

    private final IBinder mOriginal;

    public BinderWrapper(IBinder original) {
        this.mOriginal = original;
    }

    public IBinder getOriginal() {
        return mOriginal;
    }
}
