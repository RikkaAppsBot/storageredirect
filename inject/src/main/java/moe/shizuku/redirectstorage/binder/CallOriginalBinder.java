package moe.shizuku.redirectstorage.binder;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

import androidx.annotation.NonNull;

public class CallOriginalBinder extends IBinderWrapper {

    private static final int FLAG_CALL_ORIGINAL = 1 << 10;

    public CallOriginalBinder(IBinder original) {
        super(original);
    }

    @Override
    public boolean transact(int code, @NonNull Parcel data, Parcel reply, int flags) throws RemoteException {
        return getOriginal().transact(code, data, reply, flags | FLAG_CALL_ORIGINAL);
    }
}
