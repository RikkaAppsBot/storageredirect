package moe.shizuku.redirectstorage.binder;

import android.annotation.SuppressLint;
import android.os.IBinder;
import android.os.IServiceManager;
import android.os.ServiceManager;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ServiceManagerProxy implements InvocationHandler {

    private IServiceManager original;

    @SuppressWarnings("JavaReflectionMemberAccess")
    @SuppressLint("DiscouragedPrivateApi")
    public synchronized void install() throws ReflectiveOperationException {
        if (original != null) return;

        Method method = ServiceManager.class.getDeclaredMethod("getIServiceManager");
        Field field = ServiceManager.class.getDeclaredField("sServiceManager");

        method.setAccessible(true);
        field.setAccessible(true);

        original = (IServiceManager) method.invoke(null);

        field.set(null, Proxy.newProxyInstance(
                ServiceManagerProxy.class.getClassLoader(),
                new Class[]{IServiceManager.class},
                this
        ));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        switch (method.getName()) {
            case "addService": {
                if (args.length < 2) return method.invoke(original, args);
                if (!(args[0] instanceof String)) return method.invoke(original, args);
                if (!(args[1] instanceof IBinder)) return method.invoke(original, args);

                final String name = (String) args[0];
                final IBinder service = (IBinder) args[1];

                args[1] = addServicePre(name, service);
                Object res = method.invoke(original, args);
                addServicePost(name, (IBinder) args[1]);
                return res;
            }
            case "getService":
                if (args.length < 1) return method.invoke(original, args);
                if (!(args[0] instanceof String)) return method.invoke(original, args);

                final String n = (String) args[0];
                final Object s = method.invoke(original, args);

                if (!(s instanceof IBinder)) return s;

                return getServicePre(n, (IBinder) s);
        }

        return method.invoke(original, args);
    }

    protected IBinder addServicePre(String name, IBinder service) {
        return service;
    }

    protected void addServicePost(String name, IBinder service) {
    }

    protected IBinder getServicePre(String name, IBinder service) {
        return service;
    }
}
