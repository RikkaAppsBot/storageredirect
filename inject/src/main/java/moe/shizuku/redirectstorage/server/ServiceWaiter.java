package moe.shizuku.redirectstorage.server;

import android.util.Log;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class ServiceWaiter {

    public static void startWait() {
        int count = 20;

        while (count > 0) {
            if (StorageIsolationBridgeService.getStorageIsolationBinder() != null
                    && StorageIsolationBridgeService.isStorageIsolationStarted()) {
                LOGGER.i("service is ready");
                break;
            }

            if (StorageIsolationBridgeService.getStorageIsolationBinder() != null) {
                LOGGER.w("service is not ready, %d retries left...", count);
            } else {
                LOGGER.w("service is not started, %d retries left...", count);
            }

            count--;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                LOGGER.e(Log.getStackTraceString(e));
            }
        }
    }
}
