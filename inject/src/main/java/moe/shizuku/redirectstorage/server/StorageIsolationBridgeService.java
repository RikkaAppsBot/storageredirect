package moe.shizuku.redirectstorage.server;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.ArraySet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class StorageIsolationBridgeService {

    private static final String DESCRIPTOR = "android.app.IActivityManager";
    private static final int TRANSACTION = ('_' << 24) | ('_' << 16) | ('S' << 8) | 'R';

    private static final int ACTION_SEND_BINDER = 1;
    private static final int ACTION_GET_BINDER = ACTION_SEND_BINDER + 1;
    private static final int ACTION_NOTIFY_FINISHED = ACTION_SEND_BINDER + 2;
    private static final int ACTION_SET_UID_ISOLATED = ACTION_SEND_BINDER + 3;
    private static final int ACTION_SET_ISOLATED_UID_LIST = ACTION_SEND_BINDER + 4;

    private static final Set<Integer> ISOLATED_UID = Collections.synchronizedSet(new ArraySet<>());

    private static IBinder storageIsolationBinder;
    private static boolean storageIsolationStarted;

    public static IBinder getStorageIsolationBinder() {
        return storageIsolationBinder;
    }

    public static Set<Integer> getIsolatedUids() {
        return ISOLATED_UID;
    }

    public static boolean isStorageIsolationStarted() {
        return storageIsolationStarted;
    }

    private void sendBinder(IBinder binder) {
        LOGGER.d("sendBinder: %s", binder);

        if (binder != null) {
            storageIsolationBinder = binder;
            try {
                storageIsolationBinder.linkToDeath(() -> {
                    storageIsolationBinder = null;
                    LOGGER.i("service is dead");
                }, 0);
            } catch (RemoteException ignored) {
            }
        }
    }

    public boolean isServiceDescriptor(String descriptor) {
        return Objects.equals(DESCRIPTOR, descriptor);
    }

    public boolean isServiceTransaction(int code) {
        return code == TRANSACTION;
    }

    public boolean onTransact(int code, @NonNull Parcel data, @Nullable Parcel reply, int flags) {
        data.enforceInterface(DESCRIPTOR);

        int action = data.readInt();
        LOGGER.d("sr from am: action=%d, callingUid=%d, callingPid=%d", action, Binder.getCallingUid(), Binder.getCallingPid());

        switch (action) {
            case ACTION_SEND_BINDER: {
                if (Binder.getCallingUid() == 0) {
                    sendBinder(data.readStrongBinder());
                    if (reply != null) {
                        reply.writeNoException();
                    }
                    return true;
                }
                break;
            }
            case ACTION_GET_BINDER: {
                if (reply != null) {
                    reply.writeNoException();
                    LOGGER.d("saved binder is %s", storageIsolationBinder);
                    reply.writeStrongBinder(storageIsolationBinder);
                }
                return true;
            }
            case ACTION_NOTIFY_FINISHED: {
                if (Binder.getCallingUid() == 0) {
                    storageIsolationStarted = true;

                    if (reply != null) {
                        reply.writeNoException();
                    }
                    return true;
                }
            }
            case ACTION_SET_UID_ISOLATED: {
                if (Binder.getCallingUid() == 0) {
                    int uid = data.readInt();
                    boolean isolated = data.readByte() != 0;

                    if (uid > 0) {
                        if (isolated) {
                            ISOLATED_UID.add(uid);
                            LOGGER.d("add isolated uid %d", uid);
                        } else {
                            ISOLATED_UID.remove(uid);
                            LOGGER.d("remove isolated uid %d", uid);
                        }
                    }

                    if (reply != null) {
                        reply.writeNoException();
                    }
                    return true;
                }
            }
            case ACTION_SET_ISOLATED_UID_LIST: {
                if (Binder.getCallingUid() == 0) {
                    int[] list = data.createIntArray();
                    if (list != null) {
                        for (int it : list) {
                            ISOLATED_UID.add(it);
                            LOGGER.d("add isolated uid %d", it);
                        }
                    }

                    if (reply != null) {
                        reply.writeNoException();
                    }
                    return true;
                }
            }
        }
        return false;
    }
}
