package moe.shizuku.redirectstorage.server;

import android.annotation.SuppressLint;
import android.app.ActivityThread;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.UserHandle;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class PackageReceiver {

    private static final BroadcastReceiver RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Uri uri = intent.getData();
            String pkgName = (uri != null) ? uri.getSchemeSpecificPart() : null;
            int uid = intent.getIntExtra(Intent.EXTRA_UID, -1);
            boolean replacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);
            LOGGER.d("%s: %s (%d)", intent.getAction(), pkgName, uid);
            if (!replacing) {
                StorageIsolationBridge.packageChanged(intent.getAction(), pkgName, uid);
            }
        }
    };

    public static void register() {
        ActivityThread activityThread = ActivityThread.currentActivityThread();
        if (activityThread == null) {
            LOGGER.w("ActivityThread is null");
            return;
        }
        Context context = activityThread.getSystemContext();
        if (context == null) {
            LOGGER.w("context is null");
            return;
        }

        UserHandle userHandleAll;
        try {
            //noinspection JavaReflectionMemberAccess
            Field field = UserHandle.class.getDeclaredField("ALL");
            userHandleAll = (UserHandle) field.get(null);
        } catch (Throwable e) {
            LOGGER.w("UserHandle.ALL", e);
            return;
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme("package");

        HandlerThread thread = new HandlerThread("sr-PackageReceiver");
        thread.start();
        Handler handler = new Handler(thread.getLooper());

        try {
            //noinspection JavaReflectionMemberAccess
            @SuppressLint("DiscouragedPrivateApi")
            Method method = Context.class.getDeclaredMethod("registerReceiverAsUser", BroadcastReceiver.class, UserHandle.class, IntentFilter.class, String.class, Handler.class);
            method.invoke(context, RECEIVER, userHandleAll, intentFilter, null, handler);
            LOGGER.d("register package receiver");
        } catch (Throwable e) {
            LOGGER.w("registerReceiver failed", e);
        }
    }
}
