package moe.shizuku.redirectstorage.server;

import android.app.IActivityManager;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StorageIsolationBridgeServiceV26 extends IActivityManager.Stub {

    private final StorageIsolationBridgeService impl;
    private final IBinder original;

    public StorageIsolationBridgeServiceV26(StorageIsolationBridgeService impl, IBinder original) {
        this.impl = impl;
        this.original = original;
    }

    @Override
    protected boolean onTransact(int code, @NonNull Parcel data, @Nullable Parcel reply, int flags) throws RemoteException {
        if (impl.isServiceTransaction(code) && impl.onTransact(code, data, reply, flags)) {
            return true;
        }
        return original.transact(code, data, reply, flags);
    }
}
