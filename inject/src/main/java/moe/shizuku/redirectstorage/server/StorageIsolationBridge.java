package moe.shizuku.redirectstorage.server;

import android.os.IBinder;
import android.os.Parcel;

import static moe.shizuku.redirectstorage.utils.Logger.LOGGER;

public class StorageIsolationBridge {

    // sync with IService.aidl
    private static final String DESCRIPTOR = "moe.shizuku.redirectstorage.IService";
    private static final int TRANSACTION_packageChanged = 10004;

    public static void packageChanged(String action, String packageName, int uid) {
        IBinder binder = StorageIsolationBridgeService.getStorageIsolationBinder();
        if (binder == null) {
            LOGGER.d("binder is null");
            return;
        }

        Parcel data = Parcel.obtain();
        try {
            data.writeInterfaceToken(DESCRIPTOR);
            data.writeString(action);
            data.writeString(packageName);
            data.writeInt(uid);
            binder.transact(TRANSACTION_packageChanged, data, null, IBinder.FLAG_ONEWAY);
        } catch (Throwable e) {
            LOGGER.w(e, "transact");
        } finally {
            data.recycle();
        }
    }
}
