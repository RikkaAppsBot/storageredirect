package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.SRManager;

public class ObserverInfoList extends ArrayList<ObserverInfo> {

    public ObserverInfoList(int initialCapacity) {
        super(initialCapacity);
    }

    public ObserverInfoList() {
    }

    public ObserverInfoList(@NonNull Collection<? extends ObserverInfo> c) {
        super(c);
    }

    @Nullable
    public ObserverInfo get(@NonNull String packageName, int userId) {
        for (ObserverInfo item : this) {
            if (Objects.equals(item.packageName, packageName) && item.userId == userId) {
                return item;
            }
        }
        return null;
    }

    public boolean contains(String packageName, int userId) {
        return get(packageName, userId) != null;
    }
}
