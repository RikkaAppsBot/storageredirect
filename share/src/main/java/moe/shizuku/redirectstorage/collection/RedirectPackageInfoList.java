package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import moe.shizuku.redirectstorage.RedirectPackageInfo;

public class RedirectPackageInfoList extends ArrayList<RedirectPackageInfo> {

    public RedirectPackageInfoList(int initialCapacity) {
        super(initialCapacity);
    }

    public RedirectPackageInfoList() {
    }

    public RedirectPackageInfoList(@NonNull Collection<? extends RedirectPackageInfo> c) {
        super(c);
    }

    @Nullable
    public RedirectPackageInfo get(String name, int userId) {
        for (RedirectPackageInfo item : this) {
            if (Objects.equals(item.packageName, name) && item.userId == userId) {
                return item;
            }
        }
        return null;
    }

    public boolean contains(String packageName, int userId) {
        return get(packageName, userId) != null;
    }

    public RedirectPackageInfo remove(String packageName, int userId) {
        for (int i = 0; i < size(); i++) {
            RedirectPackageInfo item = get(i);
            if (Objects.equals(item.packageName, packageName) && item.userId == userId) {
                return remove(i);
            }
        }
        return null;
    }
}
