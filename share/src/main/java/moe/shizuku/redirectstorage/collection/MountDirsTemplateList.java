package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import moe.shizuku.redirectstorage.MountDirsTemplate;

public class MountDirsTemplateList extends ArrayList<MountDirsTemplate> {

    public MountDirsTemplateList(int initialCapacity) {
        super(initialCapacity);
    }

    public MountDirsTemplateList() {
    }

    public MountDirsTemplateList(@NonNull Collection<? extends MountDirsTemplate> c) {
        super(c);
    }

    @Nullable
    public MountDirsTemplate get(String id) {
        for (MountDirsTemplate item : this) {
            if (Objects.equals(item.id, id)) {
                return item;
            }
        }
        return null;
    }

    public boolean contains(String id) {
        return get(id) != null;
    }
}
