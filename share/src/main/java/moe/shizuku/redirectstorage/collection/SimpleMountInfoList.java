package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import moe.shizuku.redirectstorage.SimpleMountInfo;

public class SimpleMountInfoList extends ArrayList<SimpleMountInfo> {

    public SimpleMountInfoList(int initialCapacity) {
        super(initialCapacity);
    }

    public SimpleMountInfoList() {
    }

    public SimpleMountInfoList(@NonNull Collection<? extends SimpleMountInfo> c) {
        super(c);
    }

    @Nullable
    public SimpleMountInfo get(String id) {
        for (SimpleMountInfo item : this) {
            if (Objects.equals(item.id, id)) {
                return item;
            }
        }
        return null;
    }

    public boolean contains(String id) {
        return get(id) != null;
    }
}
