package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.HashSet;

import moe.shizuku.redirectstorage.model.ServerFile;

public class ServerFileSet extends HashSet<ServerFile> {

    public ServerFileSet() {
    }

    public ServerFileSet(@NonNull Collection<? extends ServerFile> c) {
        super(c);
    }

    public ServerFileSet(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public ServerFileSet(int initialCapacity) {
        super(initialCapacity);
    }

    public void add(String path, boolean isDirectory, String packageName, int userId) {
        ServerFile file = null;
        boolean add = true;
        for (ServerFile f : this) {
            if (f.isDirectory() == isDirectory && f.getRelativePath().equalsIgnoreCase(path)) {
                file = f;
                add = false;
                break;
            }
        }
        if (file == null) {
            file = new ServerFile(path, ServerFile.TYPE_STORAGE_ROOT, userId);
        }
        file.setIsDirectory(isDirectory);
        if (packageName == null) {
            file.setStorageRootType();
        } else {
            file.setPackageName(packageName);
        }
        if (add) {
            add(file);
        }
    }
}
