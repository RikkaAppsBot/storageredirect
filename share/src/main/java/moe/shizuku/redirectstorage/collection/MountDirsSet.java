package moe.shizuku.redirectstorage.collection;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MountDirsSet extends HashSet<String> {

    private static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    private static boolean isChildOf(String parent, String child) {
        parent = parent.toLowerCase();
        child = child.toLowerCase();

        if (parent.equals(child)) {
            return true;
        }
        if (!parent.endsWith("/")) {
            parent += "/";
        }
        if (!child.endsWith("/")) {
            child += "/";
        }
        return canonicalize(child).startsWith(canonicalize(parent));
    }

    private Set<String> mLowerCased;

    public MountDirsSet() {
    }

    public MountDirsSet(Collection<? extends String> collection) {
        super(collection);
        for (String s : collection) {
            getLowerCased().add(canonicalize(s).toLowerCase());
        }
    }

    private Set<String> getLowerCased() {
        if (mLowerCased == null) {
            mLowerCased = new HashSet<>();
        }
        return mLowerCased;
    }

    @Override
    public boolean add(String path) {
        Objects.requireNonNull(path, "path must not be null");

        path = canonicalize(path);

        for (String parent : this) {
            if (isChildOf(parent, path) && !Objects.equals(parent, path)) {
                return false;
            }
        }
        for (String child : this) {
            if (isChildOf(path, child) && !Objects.equals(child, path)) {
                remove(child);
                return super.add(path);
            }
        }

        getLowerCased().add(path.toLowerCase());
        return super.add(path);
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends String> collection) {
        return super.addAll(collection);
    }

    @Override
    public boolean contains(Object o) {
        if (!(o instanceof String)) {
            return false;
        }
        String o1 = canonicalize((String) o).toLowerCase();
        return getLowerCased().contains(o1);
    }

    public boolean containsChild(Object o) {
        if (!(o instanceof String)) {
            return false;
        }
        String path = canonicalize((String) o).toLowerCase();
        for (String parent : this) {
            if (Objects.equals(parent, path) || isChildOf(parent, path)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof String)) {
            return false;
        }
        String o1 = canonicalize((String) o).toLowerCase();
        if (getLowerCased().remove(o1)) {
            for (String path : new ArrayList<>(this)) {
                if (o1.equals(path.toLowerCase())) {
                    return super.remove(path);
                }
            }
        }
        return false;
    }

    @Override
    public void clear() {
        getLowerCased().clear();
        super.clear();
    }

    public void addDefaultDirs(String packageName, boolean data, boolean media, boolean obb) {
        if (data) add(String.format("Android/data/%s", packageName));
        if (media) add(String.format("Android/media/%s", packageName));
        if (obb) add(String.format("Android/obb/%s", packageName));
    }
}
