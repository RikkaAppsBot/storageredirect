package moe.shizuku.redirectstorage.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface IServerFile<T extends IServerFile<T>> extends Comparable<IServerFile> {

    boolean isStorageRoot();

    void setStorageRootType();

    void setRelativePath(@NonNull String relativePath);

    @NonNull
    String getRelativePath();

    void setPackageName(@NonNull String packageName);

    @NonNull
    String getPackageName();

    void setUserId(int userId);

    int getUserId();

    void setIsDirectory(boolean isDirectory);

    void setStat(@Nullable ParcelStructStat stat);

    @Nullable
    ParcelStructStat getStat();

    void setName(@NonNull String newName);

    @Nullable
    String getName();

    @Nullable
    String getNameSuffix();

    @ServerFile.Type
    int getType();

    boolean isRoot();

    boolean isDirectory();

    boolean isBackItem();

    boolean isParentOf(IServerFile other);

    T getParent();

}
