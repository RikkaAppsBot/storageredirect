package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Comparator;
import java.util.Objects;

public class ServerFile implements Parcelable, IServerFile<ServerFile> {

    public static ServerFile fromStorageRoot(@NonNull String relativePath, int userId) {
        return new ServerFile(relativePath, TYPE_STORAGE_ROOT, userId);
    }

    public static ServerFile fromRedirectTarget(@NonNull String relativePath, @NonNull String packageName, int userId) {
        final ServerFile file = new ServerFile(relativePath, TYPE_PACKAGE, userId);
        file.setPackageName(packageName);
        return file;
    }

    public static final Comparator<IServerFile> NAME_COMPARATOR = new NameComparator();
    public static final Comparator<IServerFile> CREATE_TIME_COMPARATOR = new CreateTimeComparator();
    public static final Comparator<IServerFile> MODIFIED_TIME_COMPARATOR = new ModifiedTimeComparator();
    public static final Comparator<IServerFile> LAST_ACCESS_COMPARATOR = new LastAccessComparator();

    public static final ServerFile BACK_ITEM = new ServerFile();

    public static final int TYPE_STORAGE_ROOT = 1;
    public static final int TYPE_PACKAGE = 2;

    @IntDef({TYPE_STORAGE_ROOT, TYPE_PACKAGE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    private String relativePath;
    private int type;
    private String packageName;
    private int userId;

    private boolean isDirectory = false;
    private ParcelStructStat stat = null;

    public transient boolean isMountDir = false;

    private ServerFile() {
        this.relativePath = null;
        this.userId = 0;
        this.type = 0;
        this.packageName = null;
    }

    public ServerFile(@NonNull String relativePath, int type, int userId) {
        this.relativePath = Objects.requireNonNull(relativePath);
        this.type = type;
        this.userId = userId;
    }

    public ServerFile(@NonNull ServerFile src) {
        this.relativePath = src.relativePath;
        this.type = src.type;
        this.packageName = src.packageName;
        this.userId = src.userId;
        this.isDirectory = src.isDirectory;
        this.stat = src.stat;
    }

    public ServerFile(@NonNull ServerFile parent, @NonNull String child) {
        this.relativePath = parent.relativePath;
        if (parent.relativePath.length() > 0) {
            this.relativePath += "/";
        }
        this.relativePath += Objects.requireNonNull(child);
        this.type = parent.type;
        this.userId = parent.userId;
        this.packageName = parent.packageName;
    }

    private ServerFile(Parcel in) {
        this.relativePath = in.readString();
        this.type = in.readInt();
        this.packageName = in.readString();
        this.userId = in.readInt();
        this.isDirectory = in.readByte() != 0;
        this.stat = in.readParcelable(ParcelStructStat.class.getClassLoader());
    }

    @Override
    public boolean isStorageRoot() {
        return this.type == TYPE_STORAGE_ROOT;
    }

    @Override
    public void setRelativePath(@NonNull String relativePath) {
        this.relativePath = relativePath;
    }

    @Override
    public void setPackageName(@NonNull String packageName) {
        this.type = TYPE_PACKAGE;
        this.packageName = packageName;
    }

    @Override
    public void setStorageRootType() {
        this.type = TYPE_STORAGE_ROOT;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public void setIsDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    @Override
    public void setStat(ParcelStructStat stat) {
        this.stat = stat;
    }

    @NonNull
    @Override
    public String getRelativePath() {
        return relativePath;
    }

    @Override
    public ServerFile getParent() {
        final ServerFile parent = new ServerFile(this);
        if (TextUtils.isEmpty(relativePath)) {
            return null;
        }
        final int lastSlashIndex = relativePath.lastIndexOf("/");
        if (lastSlashIndex != -1) {
            parent.relativePath = relativePath.substring(0, lastSlashIndex);
        } else {
            parent.relativePath = "";
        }
        parent.stat = null;
        return parent;
    }

    @Override
    public String getName() {
        if (TextUtils.isEmpty(relativePath)) {
            return null;
        }
        if (!relativePath.contains("/")) {
            return relativePath;
        }
        return relativePath.substring(relativePath.lastIndexOf("/") + 1);
    }

    @Nullable
    @Override
    public String getNameSuffix() {
        final String name = getName();
        if (name == null || !name.contains(".")) {
            return null;
        }
        return name.substring(name.indexOf(".") + 1);
    }

    @Override
    public void setName(String newName) {
        if (!relativePath.contains("/")) {
            relativePath = newName;
        } else {
            relativePath = relativePath.substring(0, relativePath.lastIndexOf("/")) + "/" + newName;
        }
    }

    @Type
    @Override
    public int getType() {
        return type;
    }

    @Nullable
    @Override
    public String getPackageName() {
        if (isStorageRoot()) {
            return "sdcard";
        }
        return packageName;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public boolean isRoot() {
        return TextUtils.isEmpty(relativePath);
    }

    @Override
    public boolean isDirectory() {
        return isDirectory;
    }

    @Override
    public boolean isBackItem() {
        return BACK_ITEM.equals(this);
    }

    @Override
    public boolean isParentOf(@NonNull IServerFile other) {
        final String otherPath = other.getRelativePath();
        return !otherPath.equals(this.relativePath) && otherPath.startsWith(this.relativePath + File.separator);
    }

    @Nullable
    @Override
    public ParcelStructStat getStat() {
        return stat;
    }

    @Override
    public int compareTo(IServerFile other) {
        return NAME_COMPARATOR.compare(this, other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageName, relativePath, type, userId);
    }

    @NonNull
    @Override
    public String toString() {
        return "ServerFile{" +
                "relativePath='" + relativePath + '\'' +
                ", type=" + type +
                ", packageName='" + packageName + '\'' +
                ", userId=" + userId +
                ", isDirectory=" + isDirectory +
                ", stat=" + stat +
                ", isMountDir=" + isMountDir +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof ServerFile)) {
            return false;
        }
        final ServerFile other = (ServerFile) o;
        return userId == other.userId &&
                type == other.type &&
                Objects.equals(packageName, other.packageName) &&
                Objects.equals(relativePath, other.relativePath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(relativePath);
        dest.writeInt(type);
        dest.writeString(packageName);
        dest.writeInt(userId);
        dest.writeByte(isDirectory ? (byte) 1 : (byte) 0);
        dest.writeParcelable(stat, flags);
    }

    public static final Creator<ServerFile> CREATOR = new Creator<ServerFile>() {
        @Override
        public ServerFile createFromParcel(Parcel in) {
            return new ServerFile(in);
        }

        @Override
        public ServerFile[] newArray(int size) {
            return new ServerFile[size];
        }
    };

    private static class NameComparator implements Comparator<IServerFile> {

        @Override
        public int compare(IServerFile a, IServerFile b) {
            return String.CASE_INSENSITIVE_ORDER.compare(a.getName(), b.getName());
        }

    }

    private static abstract class ParcelStructStatFieldComparator<T extends Comparable<T>> extends NameComparator {

        @Override
        public int compare(IServerFile a, IServerFile b) {
            final T fieldA = invokeField(a.getStat());
            final T fieldB = invokeField(b.getStat());
            if (fieldA.equals(fieldB)) {
                return super.compare(a, b);
            } else {
                return fieldA.compareTo(fieldB);
            }
        }

        public abstract T invokeField(ParcelStructStat stat);

    }

    private static class CreateTimeComparator extends ParcelStructStatFieldComparator<Long> {

        @Override
        public Long invokeField(ParcelStructStat stat) {
            return stat.st_ctime;
        }

    }

    private static class ModifiedTimeComparator extends ParcelStructStatFieldComparator<Long> {

        @Override
        public Long invokeField(ParcelStructStat stat) {
            return stat.st_mtime;
        }

    }

    private static class LastAccessComparator extends ParcelStructStatFieldComparator<Long> {

        @Override
        public Long invokeField(ParcelStructStat stat) {
            return stat.st_atime;
        }

    }

}
