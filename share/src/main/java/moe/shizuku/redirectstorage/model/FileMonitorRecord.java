package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class FileMonitorRecord implements Parcelable {

    public long id;
    public int user;
    public String packageName;
    public String path;
    public String func;
    public long timestamp;
    public transient int count;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeInt(this.user);
        dest.writeString(this.packageName);
        dest.writeString(this.path);
        dest.writeString(this.func);
        dest.writeLong(this.timestamp);
    }

    public FileMonitorRecord() {
    }

    protected FileMonitorRecord(Parcel in) {
        this.id = in.readLong();
        this.user = in.readInt();
        this.packageName = in.readString();
        this.path = in.readString();
        this.func = in.readString();
        this.timestamp = in.readLong();
    }

    public static final Parcelable.Creator<FileMonitorRecord> CREATOR = new Parcelable.Creator<FileMonitorRecord>() {
        @Override
        public FileMonitorRecord createFromParcel(Parcel source) {
            return new FileMonitorRecord(source);
        }

        @Override
        public FileMonitorRecord[] newArray(int size) {
            return new FileMonitorRecord[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "FileMonitorRecord{" +
                "id=" + id +
                ", user=" + user +
                ", packageName='" + packageName + '\'' +
                ", path='" + path + '\'' +
                ", func=" + func +
                ", timestamp=" + timestamp +
                '}';
    }
}
