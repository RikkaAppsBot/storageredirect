package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class DebugInfo implements Parcelable {

    public String mediaPathFromNative;
    public boolean configFromFile;
    public long lastConfigSaved;
    public List<String> exceptions;

    public void addException(Throwable tr) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        exceptions.add(sw.toString());
    }

    public DebugInfo() {
        this.exceptions = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "DebugInfo{" +
                "mediaPathFromNative='" + mediaPathFromNative + '\'' +
                ", configFromFile=" + configFromFile +
                ", lastConfigSaved=" + lastConfigSaved +
                ", exceptions=" + exceptions +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mediaPathFromNative);
        dest.writeByte(this.configFromFile ? (byte) 1 : (byte) 0);
        dest.writeLong(this.lastConfigSaved);
        dest.writeStringList(this.exceptions);
    }

    protected DebugInfo(Parcel in) {
        this.mediaPathFromNative = in.readString();
        this.configFromFile = in.readByte() == 1;
        this.lastConfigSaved = in.readLong();
        this.exceptions = in.createStringArrayList();
    }

    public static final Parcelable.Creator<DebugInfo> CREATOR = new Parcelable.Creator<DebugInfo>() {
        @Override
        public DebugInfo createFromParcel(Parcel source) {
            return new DebugInfo(source);
        }

        @Override
        public DebugInfo[] newArray(int size) {
            return new DebugInfo[size];
        }
    };
}
