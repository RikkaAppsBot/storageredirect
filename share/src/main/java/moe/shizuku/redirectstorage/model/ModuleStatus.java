package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Objects;

public class ModuleStatus implements Parcelable {

    private int versionCode;
    private String versionName;

    public ModuleStatus(int versionCode, String versionName) {
        this.versionCode = versionCode;
        this.versionName = versionName;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.versionCode);
        dest.writeString(this.versionName);
    }

    protected ModuleStatus(Parcel in) {
        this.versionCode = in.readInt();
        this.versionName = in.readString();
    }

    public static final Parcelable.Creator<ModuleStatus> CREATOR = new Parcelable.Creator<ModuleStatus>() {
        @Override
        public ModuleStatus createFromParcel(Parcel source) {
            return new ModuleStatus(source);
        }

        @Override
        public ModuleStatus[] newArray(int size) {
            return new ModuleStatus[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModuleStatus that = (ModuleStatus) o;
        return versionCode == that.versionCode &&
                Objects.equals(versionName, that.versionName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(versionCode, versionName);
    }

    @NonNull
    @Override
    public String toString() {
        return "ModuleStatus{" +
                "versionCode=" + versionCode +
                ", versionName='" + versionName + '\'' +
                '}';
    }
}
