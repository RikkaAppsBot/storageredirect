package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

public class FolderSizeEntry implements Parcelable, Comparable<FolderSizeEntry> {

    public static final Comparator<FolderSizeEntry> SIZE_COMPARATOR = new SizeComparator();

    private final ServerFile mFile;
    private final AtomicLong mSize;

    public FolderSizeEntry(@NonNull ServerFile file) {
        mFile = file;
        mSize = new AtomicLong(0);
    }

    private FolderSizeEntry(Parcel in) {
        mFile = in.readParcelable(ServerFile.class.getClassLoader());
        mSize = new AtomicLong(in.readLong());
    }

    @NonNull
    public ServerFile getFile() {
        return mFile;
    }

    public long getSize() {
        return mSize.get();
    }

    public void setSize(long size) {
        mSize.set(size);
    }

    public long addSize(long addSize) {
        return mSize.addAndGet(addSize);
    }

    @Override
    public int compareTo(@NonNull FolderSizeEntry other) {
        return SIZE_COMPARATOR.compare(this, other);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FolderSizeEntry) {
            FolderSizeEntry other = (FolderSizeEntry) o;
            return other.mFile.equals(this.mFile);
        }
        return false;
    }

    @Override
    public String toString() {
        return "FolderSizeEntry[file=" + mFile + ", size=" + mSize + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(mFile, flags);
        parcel.writeLong(mSize.get());
    }

    public static final Creator<FolderSizeEntry> CREATOR = new Creator<FolderSizeEntry>() {
        @Override
        public FolderSizeEntry createFromParcel(Parcel in) {
            return new FolderSizeEntry(in);
        }

        @Override
        public FolderSizeEntry[] newArray(int size) {
            return new FolderSizeEntry[size];
        }
    };

    private static class SizeComparator implements Comparator<FolderSizeEntry> {

        @Override
        public int compare(FolderSizeEntry o1, FolderSizeEntry o2) {
            return Long.compare(o1.getSize(), o2.getSize());
        }

    }

}
