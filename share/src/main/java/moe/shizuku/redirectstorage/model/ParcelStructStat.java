package moe.shizuku.redirectstorage.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.system.StructStat;

public class ParcelStructStat implements Parcelable {

    public final long st_atime;
    public final long st_blksize;
    public final long st_blocks;
    public final long st_ctime;
    public final long st_dev;
    public final int st_gid;
    public final long st_ino;
    public final int st_mode;
    public final long st_mtime;
    public final long st_nlink;
    public final long st_rdev;
    public final long st_size;
    public final int st_uid;

    public ParcelStructStat(StructStat stat) {
        this.st_atime = stat.st_atime;
        this.st_blksize = stat.st_blksize;
        this.st_blocks = stat.st_blocks;
        this.st_ctime = stat.st_ctime;
        this.st_dev = stat.st_dev;
        this.st_gid = stat.st_gid;
        this.st_ino = stat.st_ino;
        this.st_mode = stat.st_mode;
        this.st_mtime = stat.st_mtime;
        this.st_nlink = stat.st_nlink;
        this.st_rdev = stat.st_rdev;
        this.st_size = stat.st_size;
        this.st_uid = stat.st_uid;
    }

    private ParcelStructStat(Parcel in) {
        st_atime = in.readLong();
        st_blksize = in.readLong();
        st_blocks = in.readLong();
        st_ctime = in.readLong();
        st_dev = in.readLong();
        st_gid = in.readInt();
        st_ino = in.readLong();
        st_mode = in.readInt();
        st_mtime = in.readLong();
        st_nlink = in.readLong();
        st_rdev = in.readLong();
        st_size = in.readLong();
        st_uid = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(st_atime);
        parcel.writeLong(st_blksize);
        parcel.writeLong(st_blocks);
        parcel.writeLong(st_ctime);
        parcel.writeLong(st_dev);
        parcel.writeInt(st_gid);
        parcel.writeLong(st_ino);
        parcel.writeInt(st_mode);
        parcel.writeLong(st_mtime);
        parcel.writeLong(st_nlink);
        parcel.writeLong(st_rdev);
        parcel.writeLong(st_size);
        parcel.writeInt(st_uid);
    }

    public static final Creator<ParcelStructStat> CREATOR = new Creator<ParcelStructStat>() {
        @Override
        public ParcelStructStat createFromParcel(Parcel in) {
            return new ParcelStructStat(in);
        }

        @Override
        public ParcelStructStat[] newArray(int size) {
            return new ParcelStructStat[size];
        }
    };

}
