package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

public class BasePackageInfo implements Parcelable {

    public String packageName;
    public int userId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageName);
        dest.writeInt(this.userId);
    }

    protected BasePackageInfo(Parcel in) {
        this.packageName = in.readString();
        this.userId = in.readInt();
    }

    public static final Parcelable.Creator<BasePackageInfo> CREATOR = new Parcelable.Creator<BasePackageInfo>() {
        @Override
        public BasePackageInfo createFromParcel(Parcel source) {
            return new BasePackageInfo(source);
        }

        @Override
        public BasePackageInfo[] newArray(int size) {
            return new BasePackageInfo[size];
        }
    };
}
