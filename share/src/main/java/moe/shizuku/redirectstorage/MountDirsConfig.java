package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import moe.shizuku.redirectstorage.collection.MountDirsTemplateList;
import moe.shizuku.redirectstorage.collection.MountDirsSet;

public class MountDirsConfig implements Parcelable {

    @NonNull
    public static Set<String> getDirsForPackage(@Nullable MountDirsConfig config, String packageName, MountDirsTemplateList templates) {
        MountDirsSet mountDirs = new MountDirsSet();
        if (packageName != null) {
            mountDirs.add(String.format("Android/data/%s", packageName));
            mountDirs.add(String.format("Android/media/%s", packageName));
            mountDirs.add(String.format("Android/obb/%s", packageName));
        }
        if (config == null) {
            return mountDirs;
        }
        if (config.isTemplatesAllowed()) {
            if (config.templatesIds != null) {
                for (String id : config.templatesIds) {
                    MountDirsTemplate template;
                    if ((template = templates.get(id)) != null) {
                        mountDirs.addAll(template.list);
                    }
                }
            }
        }
        if (config.isCustomAllowed()) {
            if (config.customDirs != null) {
                mountDirs.addAll(config.customDirs);
            }
        }
        return mountDirs;
    }

    public static final int FLAG_NONE = 0;
    public static final int FLAG_ALLOW_TEMPLATES = 1;
    public static final int FLAG_ALLOW_CUSTOM = 1 << 1;

    @SerializedName(value = "flags", alternate = "type")
    public int flags;

    @Nullable
    @SerializedName(value = "template_ids", alternate = "templatesIds")
    public Set<String> templatesIds;

    @Nullable
    @SerializedName(value = "custom_dirs", alternate = "customDirs")
    public Set<String> customDirs;

    public MountDirsConfig() {
        this.flags = FLAG_NONE;
    }

    public MountDirsConfig(MountDirsConfig that) {
        this.flags = that.flags;
        this.templatesIds = that.templatesIds == null ? null : new HashSet<>(that.templatesIds);
        this.customDirs = that.customDirs == null ? null : new HashSet<>(that.customDirs);
    }

    public boolean isTemplatesAllowed() {
        return (flags & FLAG_ALLOW_TEMPLATES) > 0;
    }

    public boolean isCustomAllowed() {
        return (flags & FLAG_ALLOW_CUSTOM) > 0;
    }

    public boolean isNone() {
        return (flags & (FLAG_ALLOW_TEMPLATES | FLAG_ALLOW_CUSTOM)) == 0;
    }

    public void setCustomAllowed(boolean allowed) {
        if (allowed) {
            flags |= FLAG_ALLOW_CUSTOM;
        } else {
            flags &= ~FLAG_ALLOW_CUSTOM;
        }
    }

    public void setTemplatesAllowed(boolean allowed) {
        if (allowed) {
            flags |= FLAG_ALLOW_TEMPLATES;
        } else {
            flags &= ~FLAG_ALLOW_TEMPLATES;
        }
    }

    public void plus(MountDirsConfig b) {
        if (b.isTemplatesAllowed() && b.templatesIds != null) {
            setTemplatesAllowed(true);
            if (this.templatesIds == null) {
                this.templatesIds = new HashSet<>();
            }
            this.templatesIds.addAll(b.templatesIds);
        }
        if (b.isCustomAllowed() && b.customDirs != null) {
            setCustomAllowed(true);
            if (customDirs == null) {
                customDirs = new HashSet<>();
            }
            this.customDirs.addAll(b.customDirs);
        }
    }

    private String getTypeCodeName() {
        if (isNone()) {
            return "NONE";
        } else if (isTemplatesAllowed() && isCustomAllowed()) {
            return "TEMPLATES | CUSTOM";
        } else if (isTemplatesAllowed()) {
            return "TEMPLATES";
        } else if (isCustomAllowed()) {
            return "CUSTOM";
        } else {
            return "UNKNOWN";
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "MountDirsConfig{" +
                "type=" + getTypeCodeName() +
                ", templatesIds=" + templatesIds +
                ", customDirs=" + customDirs +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MountDirsConfig that = (MountDirsConfig) o;
        return flags == that.flags &&
                Objects.equals(templatesIds, that.templatesIds) &&
                Objects.equals(customDirs, that.customDirs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flags, templatesIds, customDirs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.flags);
        dest.writeStringList(this.templatesIds == null ? null : new ArrayList<>(this.templatesIds));
        dest.writeStringList(this.customDirs == null ? null : new ArrayList<>(this.customDirs));
    }

    protected MountDirsConfig(Parcel in) {
        this.flags = in.readInt();
        this.templatesIds = createStringSet(in);
        this.customDirs = createStringSet(in);
    }

    private static Set<String> createStringSet(Parcel in) {
        List<String> list = in.createStringArrayList();
        if (list != null) {
            return new HashSet<>(list);
        } else {
            return null;
        }
    }

    public static final Creator<MountDirsConfig> CREATOR = new Creator<MountDirsConfig>() {
        @Override
        public MountDirsConfig createFromParcel(Parcel source) {
            return new MountDirsConfig(source);
        }

        @Override
        public MountDirsConfig[] newArray(int size) {
            return new MountDirsConfig[size];
        }
    };
}
