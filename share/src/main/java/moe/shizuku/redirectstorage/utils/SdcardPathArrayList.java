package moe.shizuku.redirectstorage.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class SdcardPathArrayList extends ArrayList<String> {

    public SdcardPathArrayList() {
    }

    private static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    private static boolean isChildOf(String parent, String child) {
        parent = parent.toLowerCase();
        child = child.toLowerCase();

        if (parent.equals(child)) {
            return true;
        }
        if (!parent.endsWith("/")) {
            parent += "/";
        }
        if (!child.endsWith("/")) {
            child += "/";
        }
        return canonicalize(child).startsWith(canonicalize(parent));
    }

    /*public SdcardPathArrayList(Collection<? extends String> collection) {
        super(collection);
    }*/

    @Override
    public boolean add(String o) {
        Objects.requireNonNull(o, "path must not be null");
        if (contains(o)) {
            return false;
        }
        return super.add(o);
    }

    @Override
    public boolean addAll(Collection<? extends String> collection) {
        return super.addAll(collection);
    }

    @Override
    public int indexOf(Object o) {
        if (!(o instanceof String)) {
            return -1;
        }
        String path = canonicalize((String) o);
        for (int i = 0; i < size(); i++) {
            if (get(i).equalsIgnoreCase(path)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return indexOf(o);
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }
}
