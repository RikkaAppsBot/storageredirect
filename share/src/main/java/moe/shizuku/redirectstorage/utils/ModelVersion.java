package moe.shizuku.redirectstorage.utils;

import android.os.Binder;
import android.util.ArrayMap;

import java.util.Map;

public class ModelVersion {

    public static final int MAX_SDK_VERSION = 2;
    public static final int MANAGER_APP = -1;

    private static final Map<Long, Integer> CLIENT_PUBLIC_SDK_VERSION = new ArrayMap<>();
    private static boolean isServer;

    public static boolean isServer() {
        return isServer;
    }

    public static void setServer(boolean isServer) {
        ModelVersion.isServer = isServer;
    }

    public static int getCallingProcessSdkVersion() {
        int uid = Binder.getCallingUid();
        int pid = Binder.getCallingPid();
        long key = (long) uid << 32 + pid;
        Integer value = CLIENT_PUBLIC_SDK_VERSION.get(key);
        return value == null ? -1 : value;
    }

    public static void setCallingProcessSdkVersion(int sdkVersion) {
        int uid = Binder.getCallingUid();
        int pid = Binder.getCallingPid();
        long key = (long) uid << 32 + pid;
        CLIENT_PUBLIC_SDK_VERSION.put(key, sdkVersion);
    }

    public static int getSdkVersion() {
        if (ModelVersion.isServer()) {
            return ModelVersion.getCallingProcessSdkVersion();
        }
        return MANAGER_APP;
    }
}
