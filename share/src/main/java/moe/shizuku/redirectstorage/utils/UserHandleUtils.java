package moe.shizuku.redirectstorage.utils;

public class UserHandleUtils {

    public static final int PER_USER_RANGE = 100000;

    public static int getUserId(int uid) {
        return uid / PER_USER_RANGE;
    }

    public static int getAppId(int uid) {
        return uid % PER_USER_RANGE;
    }

    public static boolean isRegularApp(int uid) {
        return getAppId(uid) >= 10000 && getAppId(uid) <= 19999;
    }
}
