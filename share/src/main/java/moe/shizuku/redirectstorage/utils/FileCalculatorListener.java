package moe.shizuku.redirectstorage.utils;

import android.os.RemoteException;

import java.util.List;

import moe.shizuku.redirectstorage.IFileCalculatorListener;
import moe.shizuku.redirectstorage.model.FolderSizeEntry;

public abstract class FileCalculatorListener extends IFileCalculatorListener.Stub {

    public abstract void onFinished(List<FolderSizeEntry> list);

    @Override
    public final void onFinished(ParceledListSlice list) throws RemoteException {
        if (list != null) {
            //noinspection unchecked
            onFinished(list.getList());
        } else {
            onFinished((List<FolderSizeEntry>) null);
        }
    }
}
