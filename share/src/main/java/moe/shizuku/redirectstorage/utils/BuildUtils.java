package moe.shizuku.redirectstorage.utils;

import android.os.Build;
import android.os.SystemProperties;

public class BuildUtils {

    public static String getProperty(String key) {
        return SystemProperties.get(key);
    }

    public static boolean isFuse() {
        return Build.VERSION.SDK_INT >= 30 && SystemProperties.getBoolean("persist.sys.fuse", true);
    }

    public static boolean atLeast31() {
        return Build.VERSION.SDK_INT >= 31 || Build.VERSION.SDK_INT == 30 && Build.VERSION.PREVIEW_SDK_INT > 0;
    }

    public static boolean atLeast30() {
        return Build.VERSION.SDK_INT >= 30;
    }

    public static boolean atLeast29() {
        return Build.VERSION.SDK_INT >= 29;
    }

    public static boolean atLeast28() {
        return Build.VERSION.SDK_INT >= 28;
    }

    public static boolean atLeast26() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static boolean atLeast24() {
        return Build.VERSION.SDK_INT >= 24;
    }
}
