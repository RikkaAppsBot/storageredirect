package moe.shizuku.redirectstorage.utils;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.Constants;
import moe.shizuku.redirectstorage.ObserverInfo;

public class DataVerifier {

    public static final class ObserverInfoConflict {

        public static final int NONE = 0;
        public static final int SOURCE = 1;
        public static final int TARGET = 2;

    }

    public static boolean isPathInStandardFolders(@NonNull List<String> standardFolders,
                                                  @Nullable String runtimeStoragePath,
                                                  @NonNull String path) {
        String relativePath;
        if (runtimeStoragePath != null && !runtimeStoragePath.startsWith(Constants.STORAGE_PATH)) {
            relativePath = path.replaceFirst(runtimeStoragePath, "");
            if (relativePath.startsWith("/")) {
                relativePath = relativePath.substring(1);
            }
        } else {
            relativePath = path.replaceFirst(Constants.STORAGE_PATH, "");
            if (relativePath.startsWith("/")) {
                relativePath = relativePath.substring(1);
            }
            String[] pathSegments = relativePath.split("/");
            if (pathSegments.length > 0 && TextUtils.isDigitsOnly(pathSegments[0])) {
                relativePath = relativePath.substring(relativePath.indexOf("/") + 1);
            }
        }
        for (String standardFolder : standardFolders) {
            if (relativePath.startsWith(standardFolder)) {
                return true;
            } else {
                // Fix bugs: "/storage", "/storage/emulated", "/storage/emulated/0" return false
                if (runtimeStoragePath != null) {
                    final String standardFolderAbsPath = runtimeStoragePath + "/" + standardFolder;
                    if (relativePath.length() <= standardFolderAbsPath.length()) {
                        String[] standardFolderPathSegs = standardFolderAbsPath.split("/", -1);
                        String[] pathSegments = path.split("/", -1);
                        if (pathSegments.length <= standardFolderPathSegs.length &&
                                Arrays.asList(standardFolderPathSegs)
                                        .containsAll(Arrays.asList(pathSegments))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean isValidRedirectTarget(String path) {
        return Constants.REDIRECT_TARGET_DATA.equals(path)
                || Constants.REDIRECT_TARGET_CACHE.equals(path);
    }

    private static boolean isChildOf(String parent, String child) {
        if (parent.startsWith("/")) {
            parent = parent.substring(1);
        }
        if (child.startsWith("/")) {
            child = parent.substring(1);
        }
        if (child.startsWith(parent)) {
            return child.equals(parent) || child.charAt(parent.length()) == '/';
        }
        return false;
    }

    public static boolean isValidMountDirs(Collection<String> dirs) {
        if (dirs == null) {
            return true;
        }
        List<String> tmp = new ArrayList<>();
        for (String s : dirs) {
            tmp.add(s.toLowerCase());
        }

        if (tmp.contains("Android".toLowerCase())) {
            return false;
        }
        for (String s1 : tmp) {
            for (String s2 : tmp) {
                if (!s1.equals(s2) && isChildOf(s2, s1)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check if observer infos conflict and returns conflict code
     * You can get what parts of observer infos need a change, and tell users how to fix.
     *
     * @param a First observer info
     * @param b Second observer info
     * @return Conflict bits (1 = Source; 2 = Target)
     */
    public static int checkIfObserverInfoConflict(@NonNull ObserverInfo a, @NonNull ObserverInfo b) {
        int result = ObserverInfoConflict.NONE;
        if (a.userId != b.userId) {
            return result;
        }
        String as = a.packageName + a.source.toLowerCase() + File.separator, bs = b.packageName + b.source.toLowerCase() + File.separator;
        String at = a.target.toLowerCase() + File.separator, bt = b.target.toLowerCase() + File.separator;
        if (Objects.equals(as, bs) || as.startsWith(bs) || bs.startsWith(as)) {
            result |= ObserverInfoConflict.SOURCE;
        }
        if (Objects.equals(at, bt) || at.startsWith(bt) || bt.startsWith(at)) {
            result |= ObserverInfoConflict.TARGET;
        }
        return result;
    }

    /**
     * Check if two collections mutually subset.
     *
     * @param left  First collection
     * @param right Second collection
     * @param <E>   The type of element in collections
     * @return Result
     */
    public static <E> boolean isSameCollections(@Nullable Collection<E> left,
                                                @Nullable Collection<E> right) {
        if (left == null || right == null) {
            // Returns false even if both are null.
            return false;
        }
        return left.containsAll(right) && right.containsAll(left);
    }

    public static boolean isValidSimpleMountDirs(Collection<String> dirs) {
        return dirs != null;
    }
}
