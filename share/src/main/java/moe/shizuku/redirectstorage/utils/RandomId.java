package moe.shizuku.redirectstorage.utils;

import java.security.SecureRandom;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RandomId {

    private static final long START = -12219292800000L; // 15 Oct 1582

    private static final AtomicLong LAST_NANOS = new AtomicLong(0);
    private static final SecureRandom RANDOM = new SecureRandom();

    public static String next() {
        long newNanos = (System.currentTimeMillis() - START) * 10000; // -> 100ns
        while (true) {
            long lastNanos = LAST_NANOS.get();
            if (newNanos > lastNanos) {
                if (LAST_NANOS.compareAndSet(lastNanos, newNanos)) break;
            } else {
                newNanos = LAST_NANOS.incrementAndGet();
                break;
            }
        }

        long msb = 0L;
        long lsb = RANDOM.nextLong();

        msb |= (0x00000000ffffffffL & newNanos) << 32;
        msb |= (0x0000ffff00000000L & newNanos) >>> 16;
        msb |= (0xffff000000000000L & newNanos) >>> 48;
        msb |= 0x0000000000001000L;

        return new UUID(msb, lsb).toString();
    }
}
