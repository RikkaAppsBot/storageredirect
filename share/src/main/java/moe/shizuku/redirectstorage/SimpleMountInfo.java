package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.utils.DataVerifier;

import static java.util.Objects.requireNonNull;

public class SimpleMountInfo implements Parcelable {

    private static String canonicalize(String path) {
        return path
                .replace('\\', File.separatorChar)
                .replaceAll("[" + File.separator + "]{2,}", File.separator);
    }

    private static boolean isChildOf(String parent, String child) {
        parent = parent.toLowerCase();
        child = child.toLowerCase();

        if (parent.equals(child)) {
            return true;
        }
        if (!parent.endsWith("/")) {
            parent += "/";
        }
        if (!child.endsWith("/")) {
            child += "/";
        }
        return canonicalize(child).startsWith(canonicalize(parent));
    }

    @SerializedName("id")
    public String id;

    @SerializedName(value = "user_id", alternate = "userId")
    public int userId;

    @SerializedName("source_package")
    public String sourcePackage;

    @SerializedName("target_package")
    public String targetPackage;

    @SerializedName("title")
    public String title;

    @SerializedName("paths")
    public final List<String> paths;

    @SerializedName("enabled")
    public boolean enabled;

    public SimpleMountInfo(int userId, String sourcePackage, String targetPackage) {
        this.userId = userId;
        this.sourcePackage = sourcePackage;
        this.targetPackage = targetPackage;
        this.paths = new ArrayList<>();
    }

    public SimpleMountInfo(int userId, String sourcePackage,
                           String targetPackage, List<String> paths) {
        this.userId = userId;
        this.sourcePackage = sourcePackage;
        this.targetPackage = targetPackage;
        this.paths = requireNonNull(paths);
    }

    public SimpleMountInfo(SimpleMountInfo that) {
        this.id = that.id;
        this.userId = that.userId;
        this.sourcePackage = that.sourcePackage;
        this.targetPackage = that.targetPackage;
        this.paths = that.paths;
        this.enabled = that.enabled;
        this.title = that.title;
    }

    public List<String> getDirectPaths(Collection<String> packages) {
        List<String> res = new ArrayList<>();
        for (String it : paths) {
            if (it.equalsIgnoreCase("Android")
                    || it.equalsIgnoreCase("Android/data")
                    || it.equalsIgnoreCase("Android/media")
                    || it.equalsIgnoreCase("Android/obb")
                    || isChildOf(String.format("Android/data/%s", sourcePackage), it)) { // for shared-xxx
                res.add(it);
            } else {
                for (String pkg : packages) {
                    if (isChildOf(String.format("Android/data/%s", pkg), it)
                            || isChildOf(String.format("Android/media/%s", pkg), it)
                            || isChildOf(String.format("Android/obb/%s", pkg), it)) {
                        res.add(it);
                        break;
                    }
                }
            }
        }
        return res;
    }

    public List<String> getRedirectedPaths(Collection<String> packages) {
        List<String> res = new ArrayList<>();
        for (String it : paths) {
            if (it.equalsIgnoreCase("Android")
                    || it.equalsIgnoreCase("Android/data")
                    || it.equalsIgnoreCase("Android/media")
                    || it.equalsIgnoreCase("Android/obb")
                    || isChildOf(String.format("Android/data/%s", sourcePackage), it)) { // for shared-xxx
                continue;
            } else {
                boolean skip = false;
                for (String pkg : packages) {
                    if (isChildOf(String.format("Android/data/%s", pkg), it)
                            || isChildOf(String.format("Android/media/%s", pkg), it)
                            || isChildOf(String.format("Android/obb/%s", pkg), it)) {
                        skip = true;
                        break;
                    }
                }
                if (skip) {
                    continue;
                }
            }
            res.add(it);
        }
        return res;
    }

    public boolean valid() {
        return !TextUtils.isEmpty(sourcePackage)
                && !TextUtils.isEmpty(targetPackage)
                && DataVerifier.isValidSimpleMountDirs(paths)
                && !TextUtils.isEmpty(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleMountInfo that = (SimpleMountInfo) o;
        return userId == that.userId &&
                Objects.equals(id, that.id);
    }

    public boolean equalsAllFields(Object o) {
        if (!equals(o)) return false;
        return equalsContent(o);
    }

    public boolean equalsContent(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final SimpleMountInfo that = (SimpleMountInfo) o;
        if (!Objects.equals(this.sourcePackage, that.sourcePackage) ||
                !Objects.equals(this.targetPackage, that.targetPackage)) {
            return false;
        }
        if (this.paths != null && that.paths != null) {
            if (this.paths.size() != that.paths.size()) {
                return false;
            }
            for (String a : this.paths) {
                boolean contains = false;
                for (String b : that.paths) {
                    if (a.equalsIgnoreCase(b)) {
                        contains = true;
                        break;
                    }
                }
                if (!contains) {
                    return false;
                }
            }
            return true;
        } else return (this.paths == null || this.paths.isEmpty()) && (that.paths == null || that.paths.isEmpty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, sourcePackage, targetPackage, paths);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.userId);
        dest.writeString(this.sourcePackage);
        dest.writeString(this.targetPackage);
        dest.writeStringList(this.paths);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
        dest.writeString(this.title);
    }

    protected SimpleMountInfo(Parcel in) {
        this.id = in.readString();
        this.userId = in.readInt();
        this.sourcePackage = in.readString();
        this.targetPackage = in.readString();
        this.paths = in.createStringArrayList();
        this.enabled = in.readByte() == 1;
        this.title = in.readString();
    }

    public static final Creator<SimpleMountInfo> CREATOR = new Creator<SimpleMountInfo>() {
        @Override
        public SimpleMountInfo createFromParcel(Parcel source) {
            return new SimpleMountInfo(source);
        }

        @Override
        public SimpleMountInfo[] newArray(int size) {
            return new SimpleMountInfo[size];
        }
    };
}
