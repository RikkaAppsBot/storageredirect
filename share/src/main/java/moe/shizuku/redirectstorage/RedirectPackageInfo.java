package moe.shizuku.redirectstorage;

import android.content.pm.PackageInfo;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Until;

import java.util.List;

import moe.shizuku.redirectstorage.utils.ModelVersion;

public class RedirectPackageInfo implements Parcelable {

    public static boolean isSharedUserId(@NonNull String packageName) {
        return packageName.startsWith(SHARED_USER_PREFIX);
    }

    public static String getRawSharedUserId(@NonNull String packageName) {
        return packageName.substring(SHARED_USER_PREFIX.length());
    }

    public static final int PERMISSION_READ = 1;
    public static final int PERMISSION_WRITE = 1 << 1;

    public static final String SHARED_USER_PREFIX = "shared-";

    @SerializedName(value = "package_name", alternate = "packageName")
    public String packageName;

    @SerializedName(value = "user_id", alternate = "userId")
    public int userId;

    @SerializedName(value = "enabled")
    public boolean enabled;

    @SerializedName(value = "redirect_target", alternate = "redirectTarget")
    public String redirectTarget;

    @SerializedName(value = "mount_dirs", alternate = "mountDirsConfig")
    public MountDirsConfig mountDirsConfig;

    public transient PackageInfo packageInfo;
    public transient boolean systemApp;
    public transient byte storagePermission;
    public transient boolean hidden;

    @Deprecated
    @Until(9)
    public String mountDirsTemplateId;
    @Deprecated
    @Until(9)
    public List<String> mountDirs;

    @SuppressWarnings("CopyConstructorMissesField")
    public RedirectPackageInfo(RedirectPackageInfo info) {
        this.packageName = info.packageName;
        this.userId = info.userId;
        this.enabled = info.enabled;
        this.redirectTarget = info.redirectTarget;
        this.packageInfo = info.packageInfo;
        this.systemApp = info.systemApp;
        this.hidden = info.hidden;
        this.storagePermission = info.storagePermission;
        this.mountDirsConfig = new MountDirsConfig(info.mountDirsConfig);
    }

    public RedirectPackageInfo(String packageName, int userId) {
        this.packageName = packageName;
        this.userId = userId;
        this.redirectTarget = null;
        this.packageInfo = null;
        this.systemApp = false;
        this.hidden = false;
        this.storagePermission = 0;
        this.mountDirsConfig = new MountDirsConfig();
    }

    public boolean isSharedUserId() {
        return packageName != null && packageName.startsWith(SHARED_USER_PREFIX);
    }

    @Nullable
    public String getSharedUserId() {
        return isSharedUserId() ? packageName.substring(SHARED_USER_PREFIX.length()) : null;
    }

    public boolean readPermissionOnly() {
        return this.storagePermission == PERMISSION_READ;
    }

    public boolean readPermission() {
        return (this.storagePermission & PERMISSION_READ) > 0;
    }

    public boolean writePermission() {
        return (this.storagePermission & PERMISSION_WRITE) > 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "RedirectPackageInfo{" +
                "packageName='" + packageName + '\'' +
                ", userId=" + userId +
                ", enabled=" + enabled +
                ", redirectTarget='" + redirectTarget + '\'' +
                ", mountDirsConfig=" + mountDirsConfig +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RedirectPackageInfo that = (RedirectPackageInfo) o;

        return userId == that.userId
                && packageName.equals(that.packageName);
    }

    @Override
    public int hashCode() {
        int result = packageName.hashCode();
        result = 31 * result + userId;
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        switch (ModelVersion.getSdkVersion()) {
            case 1: {
                dest.writeString(this.packageName);
                dest.writeInt(this.userId);
                dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
                dest.writeString(this.redirectTarget);
                dest.writeStringList(this.mountDirs);
                dest.writeParcelable(this.packageInfo, flags);
                dest.writeByte(this.systemApp ? (byte) 1 : (byte) 0);
                dest.writeByte(this.storagePermission);
                dest.writeByte((byte) 0);
                dest.writeString(this.mountDirsTemplateId);
                break;
            }
            case ModelVersion.MANAGER_APP:
            default: {
                dest.writeString(this.packageName);
                dest.writeInt(this.userId);
                dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
                dest.writeString(this.redirectTarget);
                dest.writeParcelable(this.packageInfo, flags);
                dest.writeByte(this.systemApp ? (byte) 1 : (byte) 0);
                dest.writeByte(this.hidden ? (byte) 1 : (byte) 0);
                dest.writeByte(this.storagePermission);
                dest.writeParcelable(this.mountDirsConfig, flags);
                break;
            }
        }
    }

    protected RedirectPackageInfo(Parcel in) {
        this.packageName = in.readString();
        this.userId = in.readInt();
        this.enabled = in.readByte() != 0;
        this.redirectTarget = in.readString();
        this.packageInfo = in.readParcelable(PackageInfo.class.getClassLoader());
        this.systemApp = in.readByte() != 0;
        this.hidden = in.readByte() != 0;
        this.storagePermission = in.readByte();
        this.mountDirsConfig = in.readParcelable(MountDirsConfig.class.getClassLoader());
    }

    public static final Creator<RedirectPackageInfo> CREATOR = new Creator<RedirectPackageInfo>() {
        @Override
        public RedirectPackageInfo createFromParcel(Parcel source) {
            return new RedirectPackageInfo(source);
        }

        @Override
        public RedirectPackageInfo[] newArray(int size) {
            return new RedirectPackageInfo[size];
        }
    };

}
