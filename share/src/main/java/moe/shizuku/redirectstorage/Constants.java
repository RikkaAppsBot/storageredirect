package moe.shizuku.redirectstorage;

public class Constants {

    public static final String TAG = "StorageRedirect";

    // #define VERSION_NAME
    public static final int SERVER_VERSION = 295;
    public static final int MIN_COMPATIBLE_VERSION = 267; // only affect WorkService

    public static final String APPLICATION_ID = "moe.shizuku.redirectstorage";

    public static final String MANAGER_PERMISSION = "moe.shizuku.redirectstorage.permission.MANAGER";
    public static final String SDK_GET_CONFIG_PERMISSION = "moe.shizuku.redirectstorage.permission.GET_CONFIGURATION";

    public static final String STORAGE_PATH;
    public static final String STORAGE_PATH_FORMAT;

    public static final String REDIRECT_TARGET_DATA = "Android/data/%s/sdcard";
    public static final String REDIRECT_TARGET_CACHE = "Android/data/%s/cache/sdcard";

    static {
        STORAGE_PATH = "/storage/emulated";
        STORAGE_PATH_FORMAT = STORAGE_PATH + "/%d";
    }

    public static final String ACTION_PACKAGE_ADDED = Constants.APPLICATION_ID + ".action.PACKAGE_ADDED";
    public static final String ACTION_PACKAGE_REMOVED = Constants.APPLICATION_ID + ".action.PACKAGE_REMOVED";

    public static final String EXTRA_PACKAGE_INFO = APPLICATION_ID + ".extra.PACKAGE_INFO";
    public static final String EXTRA_PATH = APPLICATION_ID + ".extra.PATH";
    public static final String EXTRA_MIN_COMPATIBLE_VERSION = APPLICATION_ID + ".extra.MIN_COMPATIBLE_VERSION";
    public static final String EXTRA_ENABLE_NOTIFICATION = APPLICATION_ID + ".extra.ENABLE_NOTIFICATION";

}
