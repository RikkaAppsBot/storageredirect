package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import moe.shizuku.redirectstorage.utils.DataVerifier;
import moe.shizuku.redirectstorage.utils.RandomId;

import static java.util.Objects.requireNonNull;

public class MountDirsTemplate implements Parcelable {

    public static final String DEFAULT_ID = "DEFAULT";
    public static final String BUILT_IN_PHOTOS_ID = "BUILT_IN_PHOTOS";
    public static final String BUILT_IN_MUSIC_ID = "BUILT_IN_MUSIC";
    public static final String BUILT_IN_VIDEO_ID = "BUILT_IN_VIDEO";
    public static final String BUILT_IN_DOWNLOADS_ID = "BUILT_IN_DOWNLOADS";

    public static MountDirsTemplate createWithId(String id) {
        return createWithId(id, new ArrayList<>());
    }

    public static MountDirsTemplate createWithId(String id, List<String> list) {
        return new MountDirsTemplate(id, "", new ArrayList<>(list));
    }

    @SerializedName("id")
    public String id;

    @SerializedName("title")
    public String title;

    @SerializedName("list")
    public final List<String> list;

    @Nullable
    public transient List<RedirectPackageInfo> packages;

    public MountDirsTemplate() {
        this.id = null;
        this.title = "";
        this.list = new ArrayList<>();
        this.packages = null;
    }

    public MountDirsTemplate(@NonNull String title, @NonNull List<String> list) {
        this.id = null;
        this.title = requireNonNull(title);
        this.list = requireNonNull(list);
        this.packages = null;
    }

    public MountDirsTemplate(@NonNull MountDirsTemplate original) {
        this.id = original.id;
        this.title = original.title;
        this.list = new ArrayList<>(original.list);
        this.packages = original.packages;
    }

    protected MountDirsTemplate(@NonNull String id,
                                @NonNull String title,
                                @NonNull List<String> list) {
        this.id = requireNonNull(id);
        this.title = requireNonNull(title);
        this.list = requireNonNull(list);
        this.packages = null;
    }

    private MountDirsTemplate(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.list = in.createStringArrayList();
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof MountDirsTemplate)) {
            return false;
        }
        final MountDirsTemplate other = (MountDirsTemplate) o;
        return Objects.equals(this.id, other.id);
    }

    @NonNull
    @Override
    public String toString() {
        return "MountDirsTemplate[" +
                "id=" + id + ", " +
                "title=" + title + ", " +
                "list=" + list + ", " +
                "packages=" + packages +
                "]";
    }

    public boolean contentEquals(MountDirsTemplate other) {
        if (other == null) {
            return false;
        }
        return DataVerifier.isSameCollections(this.list, other.list)
                && DataVerifier.isSameCollections(this.packages, other.packages);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeStringList(list);
    }

    public static final Creator<MountDirsTemplate> CREATOR = new Creator<MountDirsTemplate>() {
        @Override
        public MountDirsTemplate createFromParcel(Parcel in) {
            return new MountDirsTemplate(in);
        }

        @Override
        public MountDirsTemplate[] newArray(int size) {
            return new MountDirsTemplate[size];
        }
    };

}
