package moe.shizuku.redirectstorage;

import android.os.DeadObjectException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import moe.shizuku.redirectstorage.model.ParcelStructStat;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.utils.FileCalculatorListener;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

public class SRFileManager {

    public static final int FLAG_GET_STAT = 1 << 1;

    public static final int FLAG_AS_PACKAGE = 1 << 2;

    private static RuntimeException rethrowFromSystemServer(RemoteException e) throws RemoteException {
        if (e instanceof DeadObjectException) {
            throw new RemoteException("system dead?");
        } else {
            throw e;
        }
    }

    private final IService mService;

    SRFileManager(SRManager srm) {
        this.mService = srm.mService;
    }

    public boolean isAlive() {
        return mService.asBinder().isBinderAlive();
    }

    @Nullable
    public List<ServerFile> list(@NonNull ServerFile parent, int flags) throws RemoteException {
        try {
            //noinspection unchecked
            final ParceledListSlice<ServerFile> slice = mService.fileManager_list(parent, flags);
            if (slice == null) {
                return null;
            } else {
                return slice.getList();
            }
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @NonNull
    public String getAbsolutePath(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_getAbsolutePath(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @NonNull
    public ParcelFileDescriptor openFileDescriptor(@NonNull ServerFile file, @NonNull String mode) throws RemoteException {
        try {
            return mService.fileManager_openFileDescriptor(file, mode);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public long length(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_length(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean delete(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_delete(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public long getLastModified(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_getLastModified(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @NonNull
    public ParcelStructStat stat(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_stat(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean move(@NonNull ServerFile src, @NonNull ServerFile dst) throws RemoteException {
        try {
            return mService.fileManager_move(src, dst);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean copy(@NonNull ServerFile src, @NonNull ServerFile dst) throws RemoteException {
        try {
            return mService.fileManager_copy(src, dst);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean create(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_create(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean isDirectory(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_isDirectory(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public void calculateFoldersSizeInRedirectTarget(
            @NonNull String packageName, int userId, FileCalculatorListener listener) throws RemoteException {
        try {
            mService.calculateFoldersSizeInRedirectTarget(packageName, userId, listener);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public boolean exists(@NonNull ServerFile file) throws RemoteException {
        try {
            return mService.fileManager_exists(file);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }
}
