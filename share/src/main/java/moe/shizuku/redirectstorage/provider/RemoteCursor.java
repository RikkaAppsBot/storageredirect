package moe.shizuku.redirectstorage.provider;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public class RemoteCursor implements Cursor, Parcelable {

    private static RuntimeException rethrowFromSystemServer(RemoteException e) throws RuntimeException {
        if (e instanceof DeadObjectException) {
            throw new RuntimeException("remote dead");
        } else {
            throw new RuntimeException(e);
        }
    }

    private IRemoteCursor mRemote;

    RemoteCursor(IRemoteCursor remote) {
        mRemote = remote;
    }

    @Override
    public int getCount() {
        try {
            return mRemote.getCount();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getPosition() {
        try {
            return mRemote.getPosition();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean move(int offset) {
        try {
            return mRemote.move(offset);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean moveToPosition(int position) {
        try {
            return mRemote.moveToPosition(position);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean moveToFirst() {
        try {
            return mRemote.moveToFirst();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean moveToLast() {
        try {
            return mRemote.moveToLast();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean moveToNext() {
        try {
            return mRemote.moveToNext();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean moveToPrevious() {
        try {
            return mRemote.moveToPrevious();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isFirst() {
        try {
            return mRemote.isFirst();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isLast() {
        try {
            return mRemote.isLast();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isBeforeFirst() {
        try {
            return mRemote.isBeforeFirst();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isAfterLast() {
        try {
            return mRemote.isAfterLast();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getColumnIndex(String columnName) {
        try {
            return mRemote.getColumnIndex(columnName);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        try {
            return mRemote.getColumnIndexOrThrow(columnName);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        try {
            return mRemote.getColumnName(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public String[] getColumnNames() {
        try {
            return mRemote.getColumnNames();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getColumnCount() {
        try {
            return mRemote.getColumnCount();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public byte[] getBlob(int columnIndex) {
        try {
            return mRemote.getBlob(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public String getString(int columnIndex) {
        try {
            return mRemote.getString(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
    }

    @Override
    public short getShort(int columnIndex) {
        try {
            return (short) mRemote.getShort(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getInt(int columnIndex) {
        try {
            return mRemote.getInt(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public long getLong(int columnIndex) {
        try {
            return mRemote.getLong(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public float getFloat(int columnIndex) {
        try {
            return mRemote.getFloat(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public double getDouble(int columnIndex) {
        try {
            return mRemote.getDouble(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public int getType(int columnIndex) {
        try {
            return mRemote.getType(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isNull(int columnIndex) {
        try {
            return mRemote.isNull(columnIndex);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public void deactivate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean requery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        try {
            mRemote.close();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean isClosed() {
        try {
            return mRemote.isClosed();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public void registerContentObserver(ContentObserver observer) {

    }

    @Override
    public void unregisterContentObserver(ContentObserver observer) {

    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void setNotificationUri(ContentResolver cr, Uri uri) {

    }

    @Override
    public Uri getNotificationUri() {
        try {
            return mRemote.getNotificationUri();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public boolean getWantsAllOnMoveCalls() {
        try {
            return mRemote.getWantsAllOnMoveCalls();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public void setExtras(Bundle extras) {
        try {
            mRemote.setExtras(extras);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public Bundle getExtras() {
        try {
            return mRemote.getExtras();
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    @Override
    public Bundle respond(Bundle extras) {
        try {
            return mRemote.respond(extras);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    private RemoteCursor(Parcel in) {
        mRemote = IRemoteCursor.Stub.asInterface(in.readStrongBinder());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStrongBinder(mRemote.asBinder());
    }

    public static final Creator<RemoteCursor> CREATOR = new Creator<RemoteCursor>() {
        @Override
        public RemoteCursor createFromParcel(Parcel in) {
            return new RemoteCursor(in);
        }

        @Override
        public RemoteCursor[] newArray(int size) {
            return new RemoteCursor[size];
        }
    };
}
