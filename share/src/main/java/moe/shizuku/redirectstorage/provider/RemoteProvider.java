package moe.shizuku.redirectstorage.provider;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.DeadObjectException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public class RemoteProvider implements Parcelable {

    private static RuntimeException rethrowFromSystemServer(RemoteException e) throws RuntimeException {
        if (e instanceof DeadObjectException) {
            throw new RuntimeException("remote dead");
        } else {
            throw new RuntimeException(e);
        }
    }

    private IRemoteProvider mRemote;

    public RemoteProvider(IRemoteProvider remote) {
        mRemote = remote;
    }

    public RemoteCursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        IRemoteCursor cursor;
        try {
            cursor = mRemote.query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
        if (cursor == null)
            return null;

        return new RemoteCursor(cursor);
    }

    public AssetFileDescriptor openAssetFile(Uri url, String mode) {
        try {
            return mRemote.openAssetFile(url, mode);
        } catch (RemoteException e) {
            throw rethrowFromSystemServer(e);
        }
    }

    public IRemoteProvider getRemote() {
        return mRemote;
    }

    private RemoteProvider(Parcel in) {
        mRemote = IRemoteProvider.Stub.asInterface(in.readStrongBinder());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStrongBinder(mRemote.asBinder());
    }

    public static final Creator<RemoteProvider> CREATOR = new Creator<RemoteProvider>() {
        @Override
        public RemoteProvider createFromParcel(Parcel in) {
            return new RemoteProvider(in);
        }

        @Override
        public RemoteProvider[] newArray(int size) {
            return new RemoteProvider[size];
        }
    };
}
