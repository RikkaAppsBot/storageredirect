package moe.shizuku.redirectstorage;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

import moe.shizuku.redirectstorage.collection.MountDirsSet;
import moe.shizuku.redirectstorage.model.DebugInfo;
import moe.shizuku.redirectstorage.model.FileMonitorRecord;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.provider.BinderProvider;
import moe.shizuku.redirectstorage.provider.IRemoteProvider;
import moe.shizuku.redirectstorage.provider.RemoteProvider;
import moe.shizuku.redirectstorage.utils.DataVerifier;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

@SuppressWarnings({"WeakerAccess"})
public class SRManager {

    public final static int USER_ALL = -1;

    public final static int FLAG_KILL_PACKAGE = 1 << 3;
    public final static int FLAG_UPDATE_ON_EXISTS = 1 << 4;
    public final static int FLAG_FORCE_ENABLE_REDIRECT = 1 << 5;
    public final static int FLAG_NO_PERMISSION_CHECK = 1 << 6;
    public final static int FLAG_TOGGLE_ENABLED_ONLY = 1 << 7;
    public final static int FLAG_DONNOT_EXPAND_SHARED_USER = 1 << 8;

    /**
     * Match packages with redirect enabled or custom configs set (even if it's not enabled).
     */
    public final static int MATCH_DEFAULT = 0;

    /**
     * Match packages with redirect enabled only.
     */
    public final static int MATCH_ENABLED_ONLY = 1 << 1;

    /**
     * Match all installed packages.
     */
    public final static int MATCH_ALL_PACKAGES = 1 << 2;

    /**
     * Request {@link android.content.pm.PackageInfo}.
     */
    public final static int GET_PACKAGE_INFO = 1 << 3;

    public final static int GET_INCLUDE_SOURCE = 1 << 4;

    // module.h
    /**
     * No module files found
     */
    public final static int MODULE_NOT_INSTALLED = -1;

    /**
     * Only 32-bit module found on 64-bit device
     */
    public final static int MODULE_NO_64BIT_LIB = -2;

    /**
     * Module files exists, but not in zygote process memory
     */
    public final static int MODULE_NOT_IN_MEMORY = -3;

    /**
     * Module files exists, but not in zygote64 process memory
     */
    public final static int MODULE_NOT_IN_64_MEMORY = -4;

    private static boolean sLogBinderCalls;

    public static boolean logBinderCalls() {
        return sLogBinderCalls;
    }

    public static void setLogBinderCalls(boolean debug) {
        sLogBinderCalls = debug;
    }

    private static final int BRIDGE_TRANSACTION_CODE = ('_' << 24) | ('_' << 16) | ('S' << 8) | 'R';
    private static final String BRIDGE_SERVICE_DESCRIPTOR = "android.app.IActivityManager";
    private static final String BRIDGE_SERVICE_NAME = "activity";
    private static final int BRIDGE_ACTION_GET_BINDER = 2;

    private static IService storageIsolationService;
    private static IBinder storageIsolationBinder;

    private static final Handler MAIN_HANDLER = new Handler(Looper.getMainLooper());

    private static final CopyOnWriteArraySet<ReceivedListener> RECEIVED_LISTENERS = new CopyOnWriteArraySet<>();
    private static final CopyOnWriteArraySet<DeathListener> DEATH_LISTENERS = new CopyOnWriteArraySet<>();

    public interface ReceivedListener {
        void onServiceBinderRecevied();
    }

    public interface DeathListener {
        void onServiceDead();
    }

    private static IBinder requestBinderFromBridge() {
        IBinder binder = ServiceManager.getService(BRIDGE_SERVICE_NAME);
        if (binder == null) return null;

        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        try {
            data.writeInterfaceToken(BRIDGE_SERVICE_DESCRIPTOR);
            data.writeInt(BRIDGE_ACTION_GET_BINDER);
            binder.transact(BRIDGE_TRANSACTION_CODE, data, reply, 0);
            reply.readException();
            IBinder received = reply.readStrongBinder();
            if (received != null) {
                return received;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            data.recycle();
            reply.recycle();
        }
        return null;
    }

    private static IBinder requestBinderFromServiceManager() {
        try {
            return ServiceManager.getService("storage_redirect");
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private static IBinder requestBinderFromContentProvider() {
        return BinderProvider.getBinder();
    }

    public static IBinder requestBinder() {
        if (storageIsolationBinder != null) {
            return storageIsolationBinder;
        }

        IBinder binder = requestBinderFromBridge();
        if (binder == null) {
            binder = requestBinderFromContentProvider();
        }
        if (binder == null) {
            binder = requestBinderFromServiceManager();
        }

        if (binder == null || !binder.pingBinder()) {
            return null;
        }

        try {
            binder.linkToDeath(() -> {
                storageIsolationService = null;
                storageIsolationBinder = null;

                MAIN_HANDLER.post(() -> {
                    for (DeathListener listener : DEATH_LISTENERS) {
                        listener.onServiceDead();
                    }
                });
            }, 0);
        } catch (RemoteException ignored) {
            return null;
        }

        storageIsolationBinder = binder;

        MAIN_HANDLER.post(() -> {
            for (ReceivedListener listener : RECEIVED_LISTENERS) {
                listener.onServiceBinderRecevied();
            }
        });

        return binder;
    }

    @Nullable
    private static IService requestService() {
        if (storageIsolationService != null) {
            return storageIsolationService;
        }

        IBinder binder = requestBinder();

        IService service = IService.Stub.asInterface(binder);
        try {
            if (service == null || service.getVersion() <= 0) {
                return null;
            }
        } catch (Throwable tr) {
            tr.printStackTrace();
            return null;
        }

        if (logBinderCalls()) {
            storageIsolationService = (IService) Proxy.newProxyInstance(service.getClass().getClassLoader(), new Class[]{IService.class}, (proxy, method, args) -> {
                Log.v("IService", method.getName() + " from thread " + Thread.currentThread().getName(), new Exception("Stack trace"));
                return method.invoke(service, args);
            });
        } else {
            storageIsolationService = service;
        }

        return storageIsolationService;
    }

    public static void addDeathListener(@NonNull DeathListener listener) {
        if (listener == null) return;
        DEATH_LISTENERS.add(listener);
    }

    public static void removeDeathListener(@NonNull DeathListener listener) {
        if (listener == null) return;
        DEATH_LISTENERS.remove(listener);
    }

    public static void addReceivedListener(@NonNull ReceivedListener listener) {
        if (listener == null) return;
        RECEIVED_LISTENERS.add(listener);
    }

    public static void removeReceivedListener(@NonNull ReceivedListener listener) {
        if (listener == null) return;
        RECEIVED_LISTENERS.remove(listener);
    }

    @Nullable
    public static SRManager create() {
        IService remote = requestService();
        if (remote == null) {
            return null;
        }
        return new SRManager(remote);
    }

    @NonNull
    public static SRManager createThrow() throws DeadObjectException {
        IService remote = requestService();
        if (remote == null) {
            throw new DeadObjectException("service not running");
        }
        return new SRManager(remote);
    }

    private static RuntimeException rethrow(RemoteException e) throws RemoteException {
        if (e instanceof DeadObjectException) {
            throw new RemoteException("system dead?");
        } else {
            throw e;
        }
    }

    private static RuntimeException rethrowFromSystemServer(RemoteException e) {
        throw new RuntimeException(e);
    }

    IService mService;

    private SRFileManager mFileManagerInstance;

    private SRManager(IService service) {
        mService = service;
    }

    @NonNull
    public SRFileManager getFileManager() {
        if (mFileManagerInstance == null) {
            mFileManagerInstance = new SRFileManager(this);
        }
        return mFileManagerInstance;
    }

    public boolean isAlive() {
        return mService.asBinder().isBinderAlive();
    }

    public int getVersion() throws RemoteException {
        try {
            return mService.getVersion();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<RedirectPackageInfo> getRedirectPackages(int flags, int userId) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getRedirectPackages(flags, userId).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @Nullable
    public RedirectPackageInfo getRedirectPackageInfo(@NonNull String packageName, int flags, int userId) throws RemoteException {
        try {
            return mService.getRedirectPackageInfo(packageName, flags, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void addRedirectPackage(@NonNull RedirectPackageInfo info, int flags) throws RemoteException {
        try {
            mService.addRedirectPackage(info, flags);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /*/**
     * @throws IllegalArgumentException redirectTarget / mountDirs invalid
     * @throws IllegalStateException a file move action is running
     */
    /*public void updateRedirectPackage(RedirectPackageInfo info, int flags) throws RemoteException, IllegalArgumentException, IllegalStateException {
        try {
            mService.updateRedirectPackage(info, flags);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }*/

    @Nullable
    public String getRedirectTargetForPackage(@NonNull String packageName, int userId, boolean raw, boolean format) throws RemoteException {
        RedirectPackageInfo info = getRedirectPackageInfo(packageName, SRManager.FLAG_NO_PERMISSION_CHECK, userId);
        if (raw) {
            return info == null ? null : info.redirectTarget;
        }
        if (info == null || !DataVerifier.isValidRedirectTarget(info.redirectTarget)) {
            String defaultTarget = getDefaultRedirectTarget();
            return format ? String.format(defaultTarget, packageName) : defaultTarget;
        }
        return format ? String.format(info.redirectTarget, packageName) : info.redirectTarget;
    }

    /**
     * @return updated and require file move
     * @throws IllegalArgumentException redirectTarget invalid
     * @throws IllegalStateException    a file move action is running
     */
    public boolean setRedirectTargetForPackage(@Nullable String target, @NonNull String packageName, int userId) throws RemoteException, IllegalArgumentException, IllegalStateException {
        try {
            return mService.setRedirectTarget(target, packageName, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public MountDirsSet getMountDirsForPackage(@NonNull String packageName, int userId) throws RemoteException {
        return getMountDirsForPackage(packageName, userId, true);
    }

    @NonNull
    public MountDirsSet getMountDirsForPackage(@NonNull String packageName, int userId, boolean includeDefault) throws RemoteException {
        try {
            return new MountDirsSet(mService.getMountDirsForPackage(packageName, userId, includeDefault));
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<String> getMountDirsForConfig(@NonNull MountDirsConfig config) throws RemoteException {
        try {
            return mService.getMountDirsForConfig(config);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalArgumentException mountDirs invalid
     */
    public boolean setMountDirsForPackage(@NonNull MountDirsConfig config, @NonNull String packageName, int userId) throws RemoteException, IllegalArgumentException {
        try {
            return mService.setMountDirsForPackage(config, packageName, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void removeRedirectPackage(@NonNull String packageName, int userId) throws RemoteException {
        try {
            mService.removeRedirectPackage(packageName, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public int getNonStandardFilesCount(@NonNull String packageName, int userId) throws RemoteException {
        try {
            return mService.getNonStandardFilesCount(packageName, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<ObserverInfo> getObservers() throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getObservers(null, SRManager.USER_ALL).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<ObserverInfo> getObservers(String packageName, int userId) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getObservers(packageName, userId).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException    not unlocked / source or target path used
     * @throws IllegalArgumentException mask invalid
     */
    @NonNull
    public ObserverInfo.Result addObserver(@NonNull ObserverInfo observerInfo) throws RemoteException, IllegalStateException, IllegalArgumentException {
        try {
            return mService.addObserver(observerInfo);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException    not unlocked / source or target path used
     * @throws IllegalArgumentException mask invalid
     */
    @NonNull
    public ObserverInfo.Result updateObserver(@NonNull ObserverInfo observerInfo) throws RemoteException, IllegalStateException, IllegalArgumentException {
        try {
            return mService.updateObserver(observerInfo, 0);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException not unlocked
     */
    public boolean removeObserver(@NonNull ObserverInfo info) throws RemoteException, IllegalStateException {
        try {
            return mService.removeObserver(info);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public String getDefaultRedirectTarget() throws RemoteException {
        try {
            return mService.getDefaultRedirectTarget();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isOngoingFileMoveExists() throws RemoteException {
        try {
            return mService.isOngoingFileMoveExists();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalArgumentException invalid data
     * @throws IllegalStateException    a file move action is running
     */
    public boolean setDefaultRedirectTarget(@NonNull String path) throws RemoteException, IllegalArgumentException, IllegalStateException {
        try {
            return mService.setDefaultRedirectTarget(path);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isFileMonitorEnabled() throws RemoteException {
        try {
            return mService.isFileMonitorEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException IO-related exception
     */
    public void setFileMonitorEnabled(boolean enabled) throws RemoteException, IllegalStateException {
        try {
            mService.setFileMonitorEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isFixRenameEnabled() throws RemoteException {
        try {
            return mService.isFixRenameEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException IO-related exception
     */
    public void setFixRenameEnabled(boolean enabled) throws RemoteException, IllegalStateException {
        try {
            mService.setFixRenameEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isKillMediaStorageOnStartEnabled() throws RemoteException {
        try {
            return mService.isKillMediaStorageOnStartEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException IO-related exception
     */
    public void setKillMediaStorageOnStartEnabled(boolean enabled) throws RemoteException, IllegalStateException {
        try {
            mService.setKillMediaStorageOnStartEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public ModuleStatus getModuleStatus() throws RemoteException {
        try {
            return mService.getModuleStatus();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public ModuleStatus getRiruStatus() throws RemoteException {
        try {
            return mService.getRiruStatus();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException SQL exception
     */
    @NonNull
    public List<FileMonitorRecord> queryFileMonitor(
            @Nullable String selection,
            @Nullable String[] selectionArgs,
            @Nullable String orderBy,
            @Nullable String limit
    ) throws RemoteException, IllegalStateException {
        try {
            //noinspection unchecked
            return mService.queryFileMonitor(selection, selectionArgs, orderBy, limit).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException SQL exception
     */
    public void deleteFileMonitor(@Nullable String whereClause, @Nullable String[] whereArgs) throws RemoteException, IllegalStateException {
        try {
            mService.deleteFileMonitor(whereClause, whereArgs);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void registerFileMoveListener(@NonNull IFileMoveListener listener) throws RemoteException {
        try {
            mService.registerFileMoveListener(listener);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void unregisterFileMoveListener(@NonNull IFileMoveListener listener) throws RemoteException {
        try {
            mService.unregisterFileMoveListener(listener);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public int isLicenseValid() throws RemoteException {
        try {
            return mService.isLicenseValid();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void exit(int requestCode) throws RemoteException {
        try {
            mService.exit(requestCode);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public DebugInfo getDebugInfo(int flags) throws RemoteException {
        try {
            return mService.getDebugInfo(flags);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public String getSavedMediaPath() throws RemoteException {
        try {
            return mService.getSavedMediaPath();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void setSavedMediaPath(@Nullable String path) throws RemoteException {
        try {
            mService.setSavedMediaPath(path);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<MountDirsTemplate> getMountDirsTemplates() throws RemoteException {
        try {
            //noinspection unchecked
            ParceledListSlice<MountDirsTemplate> slice = mService.getMountDirsTemplates();
            List<MountDirsTemplate> templates = slice.getList();
            if (templates != null) {
                for (MountDirsTemplate template : templates) {
                    template.packages = getPackagesForMountDirsTemplate(template.id);
                }
                return templates;
            } else {
                return new ArrayList<>();
            }
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public MountDirsTemplate getMountDirsTemplate(@NonNull String id) throws RemoteException {
        try {
            MountDirsTemplate template = mService.getMountDirsTemplate(id);
            if (template != null) {
                template.packages = getPackagesForMountDirsTemplate(template.id);
            }
            return template;
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public List<RedirectPackageInfo> getPackagesForMountDirsTemplate(String id) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getPackagesForMountDirsTemplate(id).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void updatePackagesForMountDirsTemplate(String id, List<RedirectPackageInfo> packages) throws RemoteException {
        try {
            mService.updatePackagesForMountDirsTemplate(id, new ParceledListSlice<>(packages));
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public String addMountDirsTemplate(@NonNull MountDirsTemplate data) throws RemoteException {
        String id;
        try {
            id = mService.addMountDirsTemplate(data);
        } catch (RemoteException e) {
            throw rethrow(e);
        }

        updatePackagesForMountDirsTemplate(data.id, data.packages);
        return id;
    }

    public void updateMountDirsTemplate(@NonNull MountDirsTemplate data) throws RemoteException {
        try {
            mService.updateMountDirsTemplate(data);
        } catch (RemoteException e) {
            throw rethrow(e);
        }

        if (data.packages != null) {
            updatePackagesForMountDirsTemplate(data.id, data.packages);
        }
    }

    public void removeMountDirsTemplate(@NonNull String id) throws RemoteException {
        try {
            mService.removeMountDirsTemplate(id);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<SimpleMountInfo> getSimpleMounts() throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getSimpleMounts(null, 0, SRManager.USER_ALL).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<SimpleMountInfo> getSimpleMounts(@NonNull String packageName, int userId, int flags) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getSimpleMounts(packageName, flags, userId).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean addSimpleMount(@NonNull SimpleMountInfo info, int flags) throws RemoteException, IllegalArgumentException {
        info.enabled = true;

        try {
            return mService.addSimpleMount(info, flags);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean updateSimpleMount(@NonNull SimpleMountInfo info, int flags) throws RemoteException {
        try {
            return mService.addSimpleMount(info, flags | SRManager.FLAG_UPDATE_ON_EXISTS);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean removeSimpleMount(@NonNull SimpleMountInfo info) throws RemoteException {
        try {
            return mService.removeSimpleMount(info);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isNoLogDetected() throws RemoteException {
        try {
            return mService.isNoLogDetected();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isFixInteractionEnabled() throws RemoteException {
        try {
            return mService.isFixInteractionEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    /**
     * @throws IllegalStateException IO-related exception
     */
    public void setFixInteractionEnabled(boolean enabled) throws RemoteException, IllegalStateException {
        try {
            mService.setFixInteractionEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public long getRedirectedProcessCount() throws RemoteException {
        try {
            return mService.getRedirectedProcessCount();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public RemoteProvider wrapContentProvider(IBinder binder) throws RemoteException {
        IRemoteProvider provider;
        try {
            provider = mService.wrapContentProvider(binder);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
        if (provider == null)
            return null;

        return new RemoteProvider(provider);
    }

    public boolean isFixInteractionEnabledForPackage(String packageName, int userId) throws RemoteException {
        try {
            return mService.isFixInteractionEnabledForPackage(packageName, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void setFixInteractionEnabledForPackage(String packageName, int userId, boolean enabled) throws RemoteException {
        try {
            mService.setFixInteractionEnabledForPackage(packageName, userId, enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isBlockRemountEnabled() throws RemoteException {
        try {
            return mService.isBlockRemountEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void setBlockRemountEnabled(boolean enabled) throws RemoteException {
        try {
            mService.setBlockRemountEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isDisableExportNotificationEnabled() throws RemoteException {
        try {
            return mService.isDisableExportNotificationEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void setDisableExportNotificationEnabled(boolean enabled) throws RemoteException {
        try {
            mService.setDisableExportNotificationEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public String[] getPackagesForUid(int uid) throws RemoteException {
        try {
            return mService.getPackagesForUid(uid);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public int getUidForSharedUser(String sharedUserId, int userId) throws RemoteException {
        try {
            return mService.getUidForSharedUser(sharedUserId, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<PackageInfo> getInstalledPackages(int flags, int userId) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getInstalledPackages(flags, userId).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @Nullable
    public PackageInfo getPackageInfo(@NonNull String packageName, int flags, int userId) throws RemoteException {
        try {
            return mService.getPackageInfo(packageName, flags, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @NonNull
    public List<ApplicationInfo> getInstalledApplications(int flags, int userId) throws RemoteException {
        try {
            //noinspection unchecked
            return mService.getInstalledApplications(flags, userId).getList();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    @Nullable
    public ApplicationInfo getApplicationInfo(@NonNull String packageName, int flags, int userId) throws RemoteException {
        try {
            return mService.getApplicationInfo(packageName, flags, userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isSdcardfsUsed() throws RemoteException {
        try {
            return mService.isSdcardfsUsed();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isNewAppNotificationEnabled() throws RemoteException {
        try {
            return mService.isNewAppNotificationEnabled();
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public void setNewAppNotificationEnabled(boolean enabled) throws RemoteException {
        try {
            mService.setNewAppNotificationEnabled(enabled);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

    public boolean isUserStorageAvailable(int userId) throws RemoteException {
        try {
            return mService.isUserStorageAvailable(userId);
        } catch (RemoteException e) {
            throw rethrow(e);
        }
    }

}
