package moe.shizuku.redirectstorage;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.regex.Pattern;

public class ObserverInfo implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName(value = "package_name", alternate = "packageName")
    public String packageName;

    @SerializedName(value = "summary", alternate = {"title", "title_key", "description"})
    public String summary;

    /**
     * source path, from /sdcard/Android/data/[package]/sdcard
     */
    @SerializedName("source")
    public final String source;

    /**
     * target path, from /sdcard
     */
    @SerializedName("target")
    public final String target;

    @SerializedName("call_media_scan")
    public final boolean callMediaScan;

    @SerializedName("add_to_downloads")
    public final boolean showNotification;

    /**
     * allow folders in source path
     */
    @SerializedName("allow_child")
    public final boolean allowChild;

    /**
     * allow file end with .tmp or .temp
     */
    @SerializedName("allow_temp")
    public final boolean allowTemp;

    /**
     * file need match this
     **/
    @SerializedName("mask")
    public final String mask;

    @SerializedName(value = "user_id", alternate = "userId")
    public int userId;

    @SerializedName("enabled")
    public boolean enabled;

    public transient Pattern pattern;

    public ObserverInfo(String packageName, int userId, boolean enabled, String summary, String source, String target, boolean callMediaScan, boolean showNotification, boolean allowChild, boolean allowTemp, String mask) {
        this.packageName = packageName;
        this.userId = userId;
        this.enabled = enabled;
        this.summary = summary;
        this.source = source;
        this.target = target;
        this.callMediaScan = callMediaScan;
        this.showNotification = showNotification;
        this.allowChild = allowChild;
        this.allowTemp = allowTemp;
        this.mask = mask;
    }

    public ObserverInfo(ObserverInfo oi) {
        this.id = oi.id;
        this.mask = oi.mask;
        this.callMediaScan = oi.callMediaScan;
        this.showNotification = oi.showNotification;
        this.allowTemp = oi.allowTemp;
        this.allowChild = oi.allowChild;
        this.packageName = oi.packageName;
        this.userId = oi.userId;
        this.source = oi.source;
        this.target = oi.target;
        this.summary = oi.summary;
        this.enabled = oi.enabled;
    }

    public ObserverInfo(String mask, boolean callMediaScan, boolean showNotification,
                         boolean allowTemp, boolean allowChild, String packageName, int userId,
                         String source, String target, String summary) {
        this.mask = mask;
        this.callMediaScan = callMediaScan;
        this.showNotification = showNotification;
        this.allowTemp = allowTemp;
        this.allowChild = allowChild;
        this.packageName = packageName;
        this.userId = userId;
        this.source = source;
        this.target = target;
        this.summary = summary;
        this.enabled = false;
    }

    @NonNull
    @Override
    public String toString() {
        return "ObserverInfo{" +
                "id='" + id + '\'' +
                ", summary='" + summary + '\'' +
                ", enabled='" + enabled + '\'' +
                ", mask='" + mask + '\'' +
                ", allowChild=" + allowChild +
                ", allowTemp=" + allowTemp +
                ", callMediaScan=" + callMediaScan +
                ", showNotification=" + showNotification +
                ", packageName='" + packageName + '\'' +
                ", userId=" + userId +
                ", source='" + source + '\'' +
                ", target='" + target + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObserverInfo that = (ObserverInfo) o;
        boolean result = Objects.equals(packageName, that.packageName);
        // Source as id
        result &= Objects.equals(source, that.source);
        // Check if same user
        if (that.userId != -1 && userId != -1) {
            result &= that.userId == userId;
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mask, callMediaScan, showNotification, packageName, source, target, allowChild, allowTemp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected ObserverInfo(Parcel in) {
        this.id = in.readString();
        this.mask = in.readString();
        this.callMediaScan = in.readByte() != 0;
        this.showNotification = in.readByte() != 0;
        this.packageName = in.readString();
        this.userId = in.readInt();
        this.source = in.readString();
        this.target = in.readString();
        this.enabled = in.readByte() != 0;
        this.summary = in.readString();
        this.allowChild = in.readByte() != 0;
        this.allowTemp = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.mask);
        dest.writeByte(this.callMediaScan ? (byte) 1 : (byte) 0);
        dest.writeByte(this.showNotification ? (byte) 1 : (byte) 0);
        dest.writeString(this.packageName);
        dest.writeInt(this.userId);
        dest.writeString(this.source);
        dest.writeString(this.target);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
        dest.writeString(this.summary);
        dest.writeByte(this.allowChild ? (byte) 1 : (byte) 0);
        dest.writeByte(this.allowTemp ? (byte) 1 : (byte) 0);
    }

    public static final Creator<ObserverInfo> CREATOR = new Creator<ObserverInfo>() {
        @Override
        public ObserverInfo createFromParcel(Parcel source) {
            return new ObserverInfo(source);
        }

        @Override
        public ObserverInfo[] newArray(int size) {
            return new ObserverInfo[size];
        }
    };

    public static class Result implements Parcelable {

        public boolean updated;
        public int conflict;
        public ObserverInfo conflictObserver;

        public Result(boolean updated) {
            this.updated = updated;
        }

        public Result(int conflict, ObserverInfo conflictObserver) {
            this.updated = false;
            this.conflict = conflict;
            this.conflictObserver = conflictObserver;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte(this.updated ? (byte) 1 : (byte) 0);
            dest.writeInt(this.conflict);
            dest.writeParcelable(this.conflictObserver, flags);
        }

        protected Result(Parcel in) {
            this.updated = in.readByte() != 0;
            this.conflict = in.readInt();
            this.conflictObserver = in.readParcelable(ObserverInfo.class.getClassLoader());
        }

        public static final Creator<Result> CREATOR = new Creator<Result>() {
            @Override
            public Result createFromParcel(Parcel source) {
                return new Result(source);
            }

            @Override
            public Result[] newArray(int size) {
                return new Result[size];
            }
        };
    }
}
