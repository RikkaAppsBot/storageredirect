package moe.shizuku.redirectstorage;

interface IFileMoveListener {

    void onProgress(String packageName, int userId, int progress, int max);
}