package moe.shizuku.redirectstorage.content;

interface IOnAppsChangedListener {

    oneway void onPackageRemoved(int userId, in String packageName) = 0;

    oneway void onPackageAdded(int userId, in String packageName) = 1;

    oneway void onPackageChanged(in UserHandle user, String packageName) = 2;

    //oneway void onPackagesAvailable(in UserHandle user, in String[] packageNames, boolean replacing);
    //oneway void onPackagesUnavailable(in UserHandle user, in String[] packageNames, boolean replacing);
    //oneway void onPackagesSuspended(in UserHandle user, in String[] packageNames,
    //        in Bundle launcherExtras);
    //oneway void onPackagesUnsuspended(in UserHandle user, in String[] packageNames);
    //oneway void onShortcutChanged(in UserHandle user, String packageName, in ParceledListSlice shortcuts);
}
