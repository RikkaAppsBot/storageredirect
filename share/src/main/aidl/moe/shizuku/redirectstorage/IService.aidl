package moe.shizuku.redirectstorage;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import moe.shizuku.redirectstorage.IFileCalculatorListener;
import moe.shizuku.redirectstorage.IFileMoveListener;
import moe.shizuku.redirectstorage.MountDirsConfig;
import moe.shizuku.redirectstorage.MountDirsTemplate;
import moe.shizuku.redirectstorage.ObserverInfo;
import moe.shizuku.redirectstorage.RedirectPackageInfo;
import moe.shizuku.redirectstorage.SimpleMountInfo;
import moe.shizuku.redirectstorage.model.DebugInfo;
import moe.shizuku.redirectstorage.model.FileMonitorRecord;
import moe.shizuku.redirectstorage.model.ModuleStatus;
import moe.shizuku.redirectstorage.model.ParcelStructStat;
import moe.shizuku.redirectstorage.model.ServerFile;
import moe.shizuku.redirectstorage.provider.IRemoteProvider;
import moe.shizuku.redirectstorage.utils.ParceledListSlice;

interface IService {

    int getVersion() = 0;
    
    ParceledListSlice getRedirectPackages(int flags, int userId) = 1;
    
    RedirectPackageInfo getRedirectPackageInfo(String packageName, int flags, int userId) = 2;

    void addRedirectPackage(in RedirectPackageInfo info, int flags) = 3;

    List<String> getMountDirsForPackage(String packageName, int userId, boolean includeDefault) = 65;

    boolean setMountDirsForPackage(in MountDirsConfig config,String packageName, int userId) = 66;

    boolean setRedirectTarget(in String target, String packageName, int userId) = 67;
    
    void removeRedirectPackage(String packageName, int userId) = 5;
    
    ParceledListSlice getObservers(String packageName, int userId) = 6;
    
    ObserverInfo.Result addObserver(in ObserverInfo info) = 7;
    
    boolean removeObserver(in ObserverInfo info) = 8;
    
    int getNonStandardFilesCount(String packageName, int userId) = 9;
    
    String getDefaultRedirectTarget() = 10;
    
    boolean setDefaultRedirectTarget(String path) = 11;

    boolean isModuleExists() = 12;
    
    boolean isFileMonitorEnabled() = 13;
    
    void setFileMonitorEnabled(boolean enabled) = 14;
    
    ModuleStatus getModuleStatus() = 15;
    
    ParceledListSlice queryFileMonitor(String selection, in String[] selectionArgs, String orderBy, String limit) = 16;
    
    int deleteFileMonitor(String whereClause, in String[] whereArgs) = 17;

    boolean isOngoingFileMoveExists() = 18;

    void registerFileMoveListener(in IFileMoveListener listener) = 19;

    void unregisterFileMoveListener(in IFileMoveListener listener) = 20;

    int isLicenseValid() = 21;

    ObserverInfo.Result updateObserver(in ObserverInfo info, int flags) = 22;

    void exit(int requestCode) = 23;

    DebugInfo getDebugInfo(int flags) = 24;

    String getSavedMediaPath() = 25;

    void setSavedMediaPath(String path) = 26;

    ParceledListSlice getMountDirsTemplates() = 37;

    String addMountDirsTemplate(in MountDirsTemplate template) = 38;

    void updateMountDirsTemplate(in MountDirsTemplate template) = 39;

    void removeMountDirsTemplate(String id) = 40;

    MountDirsTemplate getMountDirsTemplate(String id) = 41;

    ParceledListSlice getSimpleMounts(String packageName, int flags, int userId) = 42;

    boolean addSimpleMount(in SimpleMountInfo info, int flags) = 43;

    boolean removeSimpleMount(in SimpleMountInfo info) = 44;

    boolean isFixRenameEnabled() = 49;

    void setFixRenameEnabled(boolean enabled) = 50;

    boolean isKillMediaStorageOnStartEnabled() = 51;

    void setKillMediaStorageOnStartEnabled(boolean enabled) = 52;

    boolean isNoLogDetected() = 53;

    boolean isFixInteractionEnabled() = 54;

    void setFixInteractionEnabled(boolean enabled) = 55;

    long getRedirectedProcessCount() = 58;

    int setPublicSdkVersion(int sdk) = 60;

    IRemoteProvider wrapContentProvider(in IBinder provider) = 61;

    oneway void calculateFoldersSizeInRedirectTarget(String packageName, int userId, in IFileCalculatorListener listener) = 63;

    List<String> getMountDirsForConfig(in MountDirsConfig config) = 68;

    boolean isFixInteractionEnabledForPackage(String packageName, int userId) = 69;

    void setFixInteractionEnabledForPackage(String packageName, int userId, boolean enabled) = 70;

    boolean isBlockRemountEnabled() = 71;

    void setBlockRemountEnabled(boolean enabled) = 72;

    ParceledListSlice getPackagesForMountDirsTemplate(in String id) = 75;

    void updatePackagesForMountDirsTemplate(in String id, in ParceledListSlice packages) = 76;
    
    ModuleStatus getRiruStatus() = 77;

    boolean isDisableExportNotificationEnabled() = 78;

    void setDisableExportNotificationEnabled(boolean enabled) = 79;

    boolean isSdcardfsUsed() = 80;

    boolean isNewAppNotificationEnabled() = 81;

    void setNewAppNotificationEnabled(boolean enabled) = 82;

    boolean isUserStorageAvailable(int userId) = 83;

    ParceledListSlice fileManager_list(in ServerFile parent, int flags) = 1001;

    String fileManager_getAbsolutePath(in ServerFile file) = 1002;

    ParcelFileDescriptor fileManager_openFileDescriptor(in ServerFile file, String mode) = 1003;

    long fileManager_length(in ServerFile file) = 1004;

    boolean fileManager_delete(in ServerFile file) = 1005;

    long fileManager_getLastModified(in ServerFile file) = 1006;

    ParcelStructStat fileManager_stat(in ServerFile file) = 1007;

    boolean fileManager_move(in ServerFile src, in ServerFile dst) = 1008;

    boolean fileManager_copy(in ServerFile src, in ServerFile dest) = 1009;

    boolean fileManager_create(in ServerFile file) = 1010;

    boolean fileManager_isDirectory(in ServerFile file) = 1011;

    boolean fileManager_exists(in ServerFile file) = 1012;

    String[] getPackagesForUid(int uid) = 2000;

    int getUidForSharedUser(in String sharedUserId, int userId) = 2001;

    ParceledListSlice getInstalledPackages(int flags, int userId) = 2002;

    PackageInfo getPackageInfo(String packageName, int flags, int userId) = 2003;

    ParceledListSlice getInstalledApplications(int flags, int userId) = 2004;

    ApplicationInfo getApplicationInfo(String packageName, int flags, int userId) = 2005;

    oneway void updateLicense(int type, String value) = 64;

    oneway void insertFileMonitor(in String callingPackage, in String path, in String func) = 10000;

    String[] requestCheckProcess(int moduleVersion, in String callingPackage) = 10001;

    boolean shouldBlockRemountForUid(int uid) = 10002;

    oneway void packageChanged(in String action, in String packageName, int uid) = 10003;

    boolean isIsolationEnabledForUid(int uid) = 20000;
}
