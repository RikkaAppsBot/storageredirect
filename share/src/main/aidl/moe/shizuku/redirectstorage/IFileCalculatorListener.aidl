package moe.shizuku.redirectstorage;

import moe.shizuku.redirectstorage.utils.ParceledListSlice;

interface IFileCalculatorListener {

    oneway void onFinished(in ParceledListSlice list);
}