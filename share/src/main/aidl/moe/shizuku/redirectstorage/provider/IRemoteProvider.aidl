package moe.shizuku.redirectstorage.provider;

import moe.shizuku.redirectstorage.provider.IRemoteCursor;

interface IRemoteProvider {

    IRemoteCursor query(in Uri uri, in String[] projection, in String selection,
                        in String[] selectionArgs, in String sortOrder) = 0;

    AssetFileDescriptor openAssetFile(in Uri url, in String mode) = 7;
}