package moe.shizuku.redirectstorage;

import com.google.gson.Gson;

import org.junit.Test;

import moe.shizuku.redirectstorage.collection.RedirectPackageInfoList;

import static org.junit.Assert.assertEquals;

public class JsonTest {

    private static final Gson GSON = new Gson();

    @Test
    public void serialize() throws Exception {
        RedirectPackageInfoList list = new RedirectPackageInfoList();
        list.add(new RedirectPackageInfo("com.example", 0));
        assertEquals("[{\"package_name\":\"com.example\",\"user_id\":0,\"enabled\":false,\"mount_dirs\":{\"flags\":0}}]", GSON.toJson(list));
    }

    @Test
    public void deserialize() throws Exception {
        RedirectPackageInfoList list = GSON.fromJson("[{\"package_name\":\"com.example\",\"user_id\":0,\"enabled\":false,\"mount_dirs\":{\"flags\":0}}]", RedirectPackageInfoList.class);
        assertEquals(1, list.size());
        assertEquals("com.example", list.get(0).packageName);
    }
}
